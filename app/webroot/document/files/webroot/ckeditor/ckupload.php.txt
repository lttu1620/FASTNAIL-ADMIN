<?php
error_reporting(1);
date_default_timezone_set('Asia/Saigon');
header ("Expires: Mon, 26 Jul 2009 05:00:00 GMT");
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate, no-store, post-check=0, pre-check=0");
header ("Pragma: no-cache");
ob_start();
require_once 'base.php';
require_once ROOT_DIR . '/conf/config.php';
set_include_path (
	ROOT_DIR .
	PATH_SEPARATOR . ROOT_LIB . '/' .
	PATH_SEPARATOR . ROOT_LIB . '/Common/' .				
	PATH_SEPARATOR . get_include_path()
);   
require_once 'Zend/Loader/Autoloader.php';
$loader = Zend_Loader_Autoloader::getInstance();
$loader->setFallbackAutoloader(true);
$objConf = new Zend_Config_Ini(ROOT_LIB . '/Conf/config.ini');
$arrConf = $objConf->toArray();		

/** Db **/
$objDb = Zend_Db::factory($arrConf['db']['adapter'], $arrConf['db']['connect']);
$objDb->query("SET NAMES 'UTF8'");
Zend_Registry::set('db', $objDb);

/** Session **/
$AppUI = Session::start();
Zend_Registry::set('AppUI', $AppUI); 
		
/**Cache**/
$objCache = new Cache($arrConf['cache']);
Zend_Registry::set('objCache', $objCache);

/**Clear cache**/
$objClear = new Clear($objCache);
Zend_Registry::set('objClear', $objClear);

$strUrl = $strMessage = '';
$arrImageType = array('image/pjpeg', 'image/jpg', 'image/jpeg', 'image/gif', 'image/png');
if (($_FILES['upload'] == "none") OR (empty($_FILES['upload']['name']))) {
   $strMessage = "No file uploaded.";
} elseif ($_FILES['upload']["size"] == 0) {
   $strMessage = "The file is of zero length.";
} elseif (!in_array($_FILES['upload']['type'], $arrImageType)) {
   $strMessage = "The image must be in either GIF, JPG or PNG format. Please upload a GIF, JPG or PNG instead.";
} else if (!is_uploaded_file($_FILES['upload']["tmp_name"])) {
   $strMessage = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
}
if ($strMessage == '') {	
	$strUploadFolder = ROOT_UPLOAD_IMAGE;
	$strUrl = HOST_UPLOAD_IMAGE;
	if (isset($_FILES['upload']) && $_FILES['upload']['name']) {	
		$arrUploadResult = Upload::process($_FILES['upload'], $strUploadFolder, 10*1024*1024);
		if ($arrUploadResult['error'] == '') {
			$objCommon = new Common();
			$objCommon->addFile($AppUI->id, $arrUploadResult['filename']);
			Functions::thumbImage($arrUploadResult['fullpath'], $arrUploadResult['fullpath'], 600);
			$strUrl = $strUrl . '/' . $arrUploadResult['filename'];		
		}
	}
}
$funcNum = $_GET['CKEditorFuncNum'];
echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$strUrl', '$strMessage');</script>";
ob_end_flush();
?>
