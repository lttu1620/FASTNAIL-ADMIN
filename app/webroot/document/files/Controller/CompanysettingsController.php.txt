<?php

/**
 * CompanySettingsController class of CompanySettings Controller
 *
 * @package	Controller
 * @copyright Oceanize INC
 */
class CompanySettingsController extends AppController{

    /**
     * Initializes components for CompaniesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view update Companies.
     * 
     * @param integer $company_id ID value of Company. Default value is 0.
     * @return void
     */
    public function index($company_id = 0) {
        include ('Companysettings/index.php'); 
    }
}

