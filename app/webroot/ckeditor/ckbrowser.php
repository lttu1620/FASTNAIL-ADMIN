<?php
error_reporting(1);
date_default_timezone_set('Asia/Saigon');
header ("Expires: Mon, 26 Jul 2009 05:00:00 GMT");
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate, no-store, post-check=0, pre-check=0");
header ("Pragma: no-cache");
ob_start();
require_once 'base.php';
require_once ROOT_DIR . '/conf/config.php';
set_include_path (
	ROOT_DIR .
	PATH_SEPARATOR . ROOT_LIB . '/' .
	PATH_SEPARATOR . ROOT_LIB . '/Common/' .				
	PATH_SEPARATOR . get_include_path()
);
require_once 'Zend/Loader/Autoloader.php';
$loader = Zend_Loader_Autoloader::getInstance();
$loader->setFallbackAutoloader(true);
$objConf = new Zend_Config_Ini(ROOT_LIB . '/Conf/config.ini');
$arrConf = $objConf->toArray();		

/** Db **/
$objDb = Zend_Db::factory($arrConf['db']['adapter'], $arrConf['db']['connect']);
$objDb->query("SET NAMES 'UTF8'");
Zend_Registry::set('db', $objDb);

/** Session **/
$AppUI = Session::start();
Zend_Registry::set('AppUI', $AppUI); 
		
/**Cache**/
$objCache = new Cache($arrConf['cache']);
Zend_Registry::set('objCache', $objCache);

/**Clear cache**/
$objClear = new Clear($objCache);
Zend_Registry::set('objClear', $objClear);

$intYearFr	= 2011;
$intYearTo	= date('Y');
$y = isset($_GET['y']) ? $_GET['y'] : 0;
$m = isset($_GET['m']) ? $_GET['m'] : 0;
$d = isset($_GET['d']) ? $_GET['d'] : 0;
if ($y && $m && $d) {
	$strDate 	= "$y-$m-$d";
	$intMonth 	= $m;
	$intDay		= $d;
} else {
	$strDate 	= date('Y-m-d');
	$intMonth	= date('m');
	$intDay		= date('d');
}
$arrYear 	= Functions::arrayNumber($intYearFr, $intYearTo);
$arrMonth 	= Functions::arrayNumber(1, 12);
$arrDay 	= Functions::arrayNumber(1, 31);
$strUrl 	= HOST_UPLOAD_IMAGE;
$objCommon	= new Common();
$arrImage	= $objCommon->getAllFile($AppUI->id, $strDate);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Browse</title>
<base href="<?php echo HOST_ADMIN; ?>"/>
<link href="<?php echo HOST_ADMIN; ?>/css/setup.css" rel="stylesheet" type="text/css" />
<link href="<?php echo HOST_ADMIN; ?>/css/ckbrowser.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function select_image(img) {
	var CKEditorFuncNum = <?php echo $_GET['CKEditorFuncNum']; ?>;
	window.parent.opener.CKEDITOR.tools.callFunction(CKEditorFuncNum, img, '' );
	self.close();
}
</script>
</head>
<body>

<div style="padding:5px;">	
<form id="frmBrowser" name="frmBrowser" method="GET" action="">
	<input type="hidden" id="CKEditor" name="CKEditor" value="<?php echo $_GET['CKEditor'];?>"/>
	<input type="hidden" id="CKEditorFuncNum" name="CKEditorFuncNum" value="<?php echo $_GET['CKEditorFuncNum'];?>"/>
	<input type="hidden" id="langCode" name="langCode" value="<?php echo $_GET['langCode'];?>"/>
	<select style="float:left;text-align:center;margin-right:2px;height:22px;" id="y" name="y">
	<?php foreach($arrYear as $i => $year) { ?>
		<option value="<?php echo $year;?>" <?php if ($year == $intYearTo) echo "selected=\"selected\"";?> ><?php echo $year;?></option>
	<?php } ?>
	</select>
	<select style="float:left;text-align:center;margin-right:2px;height:22px;" id="m" name="m">
	<?php foreach($arrMonth as $i => $month) { ?>
		<option value="<?php echo Functions::dFormat($month,2);?>" <?php if ($month == intval($intMonth)) echo "selected=\"selected\"";?> ><?php echo Functions::dFormat($month, 2);?></option>
	<?php } ?>
	</select>
	<select style="float:left;text-align:center;margin-right:2px;height:22px;" id="d" name="d">
	<?php foreach($arrDay as $i => $day) { ?>
		<option value="<?php echo Functions::dFormat($day,2);?>" <?php if ($day == intval($intDay)) echo "selected=\"selected\"";?> ><?php echo Functions::dFormat($day, 2);?></option>
	<?php } ?>
	</select>
	<a href="javascript:void(0)" onclick="document.frmBrowser.submit();" class="bntS1"><span>Xem</span></a>	
	
</form>
</div>	
<div class="space"></div>
<hr/>
<div class="listImage">
<ul>
<?php foreach($arrImage as $i => $row) { ?>
	<li><a title="" href="javascript:select_image('<?php echo HOST_UPLOAD . '/' . $row['path'] . '/' . $row['file']?>');"><img src="<?php echo $strUrl . '/' . $row['file']?>"/></a></li>
<?php } ?>
</ul>
</div>
</body>
</html>
<?php ob_end_flush(); ?>