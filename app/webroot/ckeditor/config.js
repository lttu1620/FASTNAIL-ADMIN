﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
		
	config.ImageClick = function(){
		alert(1);
	}
	config.LinkClick = function(){
		alert(1);
	}
	
};

CKEDITOR.on( 'dialogDefinition', function( ev )
{
  // Take the dialog name and its definition from the event data.
  var dialogName = ev.data.name;
  var dialogDefinition = ev.data.definition;
  // Check if the definition is from the dialog we're
  // interested in (the 'link' dialog).
	if (dialogName == 'image') {	
		// Remove the 'Target' and 'Advanced' tabs from the 'Link' dialog.
		//dialogDefinition.removeContents( 'target' );
		//dialogDefinition.removeContents( 'advanced' );
		//dialogDefinition.removeContents( 'target' );
		dialogDefinition.removeContents('Link');
		dialogDefinition.removeContents('advanced');
		 // Get a reference to the 'Link Info' tab.
		//var infoTab = dialogDefinition.getContents( 'info' );
	
		 // Remove unnecessary widgets from the 'Link Info' tab.         
		 //infoTab.remove( 'linkType');
		 //infoTab.remove( 'protocol');
	}
	
});
CKEDITOR.config.smiley_path=CKEDITOR.basePath+'plugins/smiley/ab/';
CKEDITOR.config.smiley_images = ['blush.gif','braces.gif','giggle.gif','grin.gif','happy.gif','hypnotised.gif','ill.gif','nerd.gif','sad.gif','scared.gif','scream.gif','thumbsup.gif','tut_tut.gif'];
CKEDITOR.config.smiley_descriptions= [];


