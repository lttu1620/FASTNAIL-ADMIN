<?php

App::uses('AppController', 'Controller');

/**
 * Tags of controller
 * @package Controller
 * @created 2015-04-01
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class DevicesController extends AppController {
    
    /**
     * Construct
     * 
     * @author Hoang Gia Thong 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Action: index
     * 
     * @author Hoang Gia Thong
     * @return void
     */
    public function index() {
        include ('Devices/index.php');
    }

    /**
     * Action: update
     * 
     * @author Hoang Gia Thong
     * @return void
     */
    public function update($id = 0) {
        include ('Devices/update.php');
    }
}

