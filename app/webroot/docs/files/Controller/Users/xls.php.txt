<?php

$modelName = $this->User->name;

$param = $this->getParams(array('page' => 1, 'limit' => 10000));
list($total, $data) = Api::call(Configure::read('API.url_users_list'), $param, false, array(0, array()));
$this->PhpExcel->createWorksheet()
        ->setDefaultFont('Calibri', 12);
// define table cells
$table = array(
    array('label' => __('ID')),
    array('label' => __('Code')),
    array('label' => __('Email')),
    array('label' => __('Name')),
    array('label' => __('Kana')),
    array('label' => __('Gender')),
    array('label' => __('Phone')),
    array('label' => __('Birthday')),
    array('label' => __('Created'))
);
// add heading with different font and bold text
$this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));

// add data
$genders = Configure::read('Config.searchGender');
foreach ($data as $value) {
    $gender = isset($genders[$value['sex']]) ? $genders[$value['sex']] : '';
    $birthday = $this->Common->dateFormat($value['birthday']) != false ? $this->Common->dateFormat($value['birthday']) : '';
    $created = $this->Common->dateFormat($value['created']) != false ? $this->Common->dateFormat($value['created']) : '';
    $this->PhpExcel->addTableRow(array(
        $value['id'],
        $value['code'],
        $value['email'],
        $value['name'],
        $value['kana'],
        $gender,
        $value['phone'],
        $birthday,
        $created,
    ));
}
// close table and output
$fileName = 'users-' . date('YmdHis') . '.xls';
$this->PhpExcel->addTableFooter()->output($fileName, 'Excel5');

