<?php

App::uses('AppController', 'Controller');

/**
 * Tags of controller
 * @package Controller
 * @created 2015-06-17
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class BeautiesController extends AppController {
    
    /**
     * Construct
     * 
     * @author diennvt 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Action: index
     * 
     * @author diennvt
     * @return void
     */
    public function index() {
        include ('Beauties/index.php');
    }

    /**
     * Action: update
     * 
     * @author diennvt
     * @return void
     */
    public function update($id = 0) {
        include ('Beauties/update.php');
    }
}

