<?php

$modelName = $this->Userselectednaillog->name;

// create breadcrumb
$pageTitle = __('User selected nail log list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'    => 'nail_customid',
        'label' => __('Nail ID'),
        'type' => 'text'
    ))
    ->addElement(array(
            'id' => 'name',
            'label' => __('User name')
        ))
        ->addElement(array(
        'id'       => 'date_from',
        'label'    => __('From date'),
        'type'     => 'text',
        'calendar' => true
    ))
    ->addElement(array(
        'id'       => 'date_to',
        'label'    => __('To date'),
        'type'     => 'text',
        'calendar' => true
    ))
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty' => __('All')
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'created-asc' => __('Created Asc'),
            'created-desc' => __('Created Desc'),
            'name-asc' => __('Name Asc'),
            'name-desc' => __('Name Desc')
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
if(isset($param['nail_customid'])){
    $param['nail_id'] = $param['nail_customid'];
} 
list($total, $data) = Api::call(Configure::read('API.url_userselectednaillogs_list'), $param, false, array(0, array()));

$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'image_url',
        'type'  => 'image',
        'title' => __('Image'),
        'src'   => '{image_url}',
        'width' => '60'
    ))
    ->addColumn(array(
        'id' => 'name',
        'title' => __('User name'),
    ))
    ->addColumn(array(
        'id' => 'created',
        'type' => 'date',
        'title' => __('Created'),
        'width' => 120
    ))
    ->setDataset($data);

