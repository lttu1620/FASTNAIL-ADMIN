<?php

if(!empty($shopId) && is_numeric($shopId)){
    $result = Api::call(Configure::read('API.url_shops_detail'), array('id' => $shopId));
    $this->Common->handleException(Api::getError());
    if(!empty($result)){
        $this->AppUI->shop_group_id = $result['shop_group_id'];        
        $this->AppUI->shop_area_id = $result['area_id'];
        $this->AppUI->shop_reservation_interval = $result['reservation_interval'];
        $this->AppUI->shop_id = $shopId;
        $this->AppUI->shop_name = $result['name'];
        $this->AppUI->shop_phone = $result['phone'];
        $this->AppUI->shop_address = $result['address'];
        $this->AppUI->shop_is_designate = $result['is_designate'];
        $this->AppUI->shop_open_time = $result['open_time'];
        $this->AppUI->shop_close_time = $result['close_time'];
        $this->AppUI->shop_is_plus = $result['is_plus'];
        $this->AppUI->shop_disable = $result['disable'];
        $this->AppUI->shop_max_seat = $result['max_seat'];
        $this->AppUI->shop_hp_max_seat = $result['hp_max_seat'];
        $this->AppUI->total_max_seat = $result['max_seat'] + $result['hp_max_seat'];
        
        $this->createLoginSession((array)$this->AppUI);   
        AppCache::delete(Configure::read('shops_all')->key);
        AppCache::delete(Configure::read('nailists_all')->key);
        AppCache::delete(Configure::read('devices_all')->key);
    }
}
return $this->redirect(Controller::referer());
