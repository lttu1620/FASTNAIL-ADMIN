<?php
$data = array();
if (!empty($this->request->query['order_id'])) { //if have order_id -> update/copy
	if(!empty($this->request->query['is_copy']) ){
		$data['reservation']['is_copy'] = $this->request->query['is_copy'];
	}
    $data['reservation']['order_id'] = $this->request->query['order_id'];
}else { // add new
    //get data from dasboard
    if (!empty($this->request->data['reservation'])) {
        $data['reservation'] = $this->request->data['reservation'];
    } else {
        $data['reservation']['start_at_epoch'] = time();
        $data['reservation']['duration'] = 3600;
        $data['reservation']['stylist_ids'] = array();
    }
}

// return if data is empty
if(empty($data)){
    $info = array();
}else{
    //get available nailist
    $nailist = MasterData::nailists_all($this->AppUI->shop_id);
    $nailist = $this->Common->generateArrPrefixValue($nailist, 'id', 'n_', array('id', 'name'));
    //get available devicelist
    $devicelist = MasterData::devices_all($this->AppUI->shop_id);
    $devicelist = $this->Common->generateArrPrefixValue($devicelist, 'id', 'd_', array('id', 'name'));
    // generate available list
    if(!empty($this->request->query['is_seat']) ){
    	$available_list = array($devicelist);
    }else{
    	$available_list = array($devicelist, $nailist);
    }
    //get services list
    $servicelist = MasterData::services_all();
    
    //if have order_id -> update
    if (!empty($data['reservation']['order_id'])) {
        $order_id = $data['reservation']['order_id'];
        $param['order_id'] = $order_id;
        $order = Api::call(Configure::read('API.url_orders_detailforcalendar'), $param);
        if (Api::getError()) {
            AppLog::info("API.url_orders_detailforcalendar", __METHOD__, $param);
            return $this->Common->setFlashErrorMessage(Api::getError());
        }
        
        $reservation_date = date('Y-m-d H:i', $order['reservation_date']);
        $reservation_date_timestamp = $order['reservation_date'];
        $reservation_duration = $order['order_end_date'] - $order['order_start_date'];
        
        //generate selected list
        $selected_services = !empty($order['services']) ? $order['services'] : array();
        
        $nailist_arr = $this->Common->arrayFilter($order['nailists'], 'disable', 0);
        $selected_nailist_ids = $this->Common->arrayValues($nailist_arr, 'nailist_id');
        foreach($selected_nailist_ids as &$n_id){
            $n_id = 'n_'.$n_id;
        }
        $device_arr = $this->Common->arrayFilter($order['devices'], 'disable', 0);
        $selected_device_ids = $this->Common->arrayValues($device_arr, 'device_id');
        foreach($selected_device_ids as &$d_id){
            $d_id = 'd_'.$d_id;
        }
        if(!empty($this->request->query['is_seat']) ){
        	$seat_id = !empty($order['seat_id']) ? $order['seat_id'] : ''; 
        	$selected_ids = !empty($selected_device_ids) ? array($selected_device_ids) : array(array(''));
        }else{
        	$selected_ids = array($selected_device_ids,$selected_nailist_ids);
        }
        $visit_section = $order['visit_section'];
        $visit_element = $order['visit_element'];  
        $is_action = 'update';
        if(!empty($data['reservation']['is_copy'] )){
        	echo json_encode(array('name'=>$order['user_name'], 'phone'=>$order['phone'],'email'=>$order['email'], 'sex'=> $order['sex']));exit;
        }
     }else { // add new
        //get data from dasboard
        $reservation = $data['reservation'];

        if (!is_numeric($reservation['start_at_epoch'])) {
            $reservation['start_at_epoch'] = time();
        }

        $reservation_date = date('Y-m-d H:i', $reservation['start_at_epoch']);
        $reservation_date_timestamp = $reservation['start_at_epoch'];
        $reservation_duration = 0;
        
       	$selected_device_ids = array();
       	$selected_nailist_ids = array();
       	foreach($reservation['stylist_ids'] as $id){
       		if(substr($id, 0,1) == 'd'){
       			$selected_device_ids[] = $id;
       		}else{
       			$selected_nailist_ids[] = $id;
       		}
       	}
       	$selected_ids = array($selected_device_ids,$selected_nailist_ids);
        
        $visit_section = 0;
        $visit_element = 0;  
        $is_action = 'new';
    }

    $visit_element_list = Configure::read('Config.VisitElement');
    unset($visit_element_list[0]);
    $visit_section_list = Configure::read('Config.VisitSection');
    unset($visit_section_list[0]);
    
    //prepare data for booking dialog
    $info = array(
        'reservation_date' => $reservation_date, //date('Y-m-d H:i:s O'),
        'startAtTimeStamp' => $reservation_date_timestamp,
        'reservation_duration' => Configure::read('Config.BookingDuration'),
        'duration' => $reservation_duration,
        'reservation_element' => $visit_element_list,
        'reservation_section' => $visit_section_list,
    	'reservation_type' => Configure::read('Config.ReservationType'),	
    	'reserve_type' => !empty($order['reservation_type']) ? $order['reservation_type'] : '',	
    	'service_list' => !empty($servicelist) ? $servicelist : array(),	
        'available_list' => $available_list,	
        'selected_ids' => $selected_ids,
    	'selected_services' => !empty($selected_services) ? $selected_services : array(),	
        'visit_section' => $visit_section,
        'visit_element' => $visit_element,
    	'request' => !empty($order['request']) ? $order['request'] : '', 	
        'shop_id' => !empty($this->AppUI->shop_id) ? $this->AppUI->shop_id : 0,
        'user_id' => 0,
        'order_id' => !empty($order_id) ? $order_id : '',
        'user_name' => !empty($order['user_name']) ? $order['user_name'] : '',
        'phone' => !empty($order['phone']) ? $order['phone']: '',
        'email' => !empty($order['email']) ? $order['email']  : '',
        'sex' => !empty($order['sex']) ? $order['sex'] : '',
    	'is_action' => $is_action,	
    	'is_seat' => !empty($this->request->query['is_seat']) ? $this->request->query['is_seat'] : 0,
    	'estimate_duration' => 	!empty($order['estimate_time']) ? $order['estimate_time'] : 0,
    );
}

$this->set('info', $info);



