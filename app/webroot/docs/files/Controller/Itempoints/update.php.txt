<?php

$modelName = $this->Itempoint->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Item Point');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_item_points_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Item unavailable", __METHOD__, $param);
        throw new NotFoundException("Item Point unavailable", __METHOD__, $param);
    }
    $pageTitle = __('Edit Item Point');
}
$this->setPageTitle($pageTitle);
$listItem = $this->Common->arrayKeyValue(MasterData::items_all(), 'id', 'name');
$listService = $this->Common->arrayKeyValue(MasterData::services_all(), 'id', 'name');

// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));
// create Update form 
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id'    => 'id',
        'type'  => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id'       => 'item_type',
        'label'    => __('Item type'),
        'options'  => Configure::read('Config.itemPoint'),
        'empty'    => Configure::read('Config.StrChooseOne'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'item_id',
        'label'    => __('Items'),
        'options'  => $listItem,
        'empty'    => Configure::read('Config.StrChooseOne'),
        'required' => true,
        'before' => '<div id="div_item_id">',
        'after' => '</div>'
    )) 
    ->addElement(array(
        'id'       => 'service_id',
        'label'    => __('Service'),
        'options'  => $listService,
        'empty'    => Configure::read('Config.StrChooseOne'),
        'required' => true,
        'before' => '<div id="div_service_id">',
        'after' => '</div>'
    ))  
    ->addElement(array(
        'id'       => 'name',
        'label'    => __('Name'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'description',
        'label'    => __('Description'),
        'type'     => 'text',
        'rows'     => 6,
        'required' => true
    ))
    ->addElement(array(
        'id'    => 'image_url',
        'label' => __('Image url')
    ))
    ->addElement(array(
        'id'       => 'point',
        'label'    => __('Point'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'start_date',
        'type'     => 'text',
        'calendar' => true,
        'label'    => __('Start date'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'end_date',
        'type'     => 'text',
        'calendar' => true,
        'label'    => __('End date'),
        'required' => true
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type'    => 'submit',
        'value'   => __('Cancel'),
        'class'   => 'btn pull-left',
        'onclick' => 'return back();'
    ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        $id = Api::call(Configure::read('API.url_item_points_add_update'), $data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not update item point", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
    } else {
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
