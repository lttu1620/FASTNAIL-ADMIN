<?php

/**
 * User of model.
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class User extends AppModel {

    public $name = 'User';
    public $table = 'users';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author thailh
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateUserInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'code' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 64),
                    'message' => __('Code must be no larger then 64 character long')
                ),                
            ),
            'name' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 40),
                    'message' => __('Name must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                )
            ),
            'kana' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 40),
                    'message' => __('Name Kana must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name Kana can not empty'),
                )
            ),
            'phone' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Phone can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxlength', 15),
                    'message' => __('Phone must be no larger then 13 character long'),
                ),                
                'format' => array(
                    'rule' => array('phone', '/^\d{2,5}\-\d{3,4}\-\d{3,4}+$/'),
                    'message' => __('Phone is invalid'),
                ),
                'between' => array(
                    'rule'      => array('lengthBetween', 12, 13),
                    'message'   => __("The phone number must contain 10 - 11 digits"),
                )
            ),
            'email' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 40),
                    'message' => __('Email must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'rule3' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address'))
            ),
            'image_url' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadImage_url", array()),
                ),
            ),
            'sex' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Sex can not empty'),
                ),
            ),
        );
        
        if (empty($data[$this->name]['id'])) {
           $this->validate['password'] = array(
                'maxLength' => array(
                    'rule' => array('between', 4, 40),
                    'message' => __('Your password must be not null or must be between 4 and 40 characters.'),
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('password can not empty'),
                ),
            );
           $this->validate['password_confirm'] = array(
                'maxLength' => array(
                    'rule' => array('between', 4, 40),
                    'message' => __('Your password confirm must be not null or must be between 4 and 40 characters.'),
                ),
                'compare' => array(
                    'rule' => array('validate_passwords'),
                    'message' => __('The password confirm you entered do not match'),
                )
            );
        }

        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to upload image_url.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadImage_url() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile("User.image_url");
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

    /**
     * Validate password.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function validate_passwords() {
        if ($this->data[$this->name]['password'] != '') {
            return $this->data[$this->name]['password'] === $this->data[$this->name]['password_confirm'];
        } else {
            return true;
        }
    }

    /**
     * Validate password to user profile.
     *
     * @author thailh
     * @param array $date Input array
     * @return bool Returns the boolean.
     */
    public function validate_passwords_user_profile($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'password' => array(
                'maxLength' => array(
                    'allowEmpty' => false,
                    'rule' => array('between', 8, 40),
                    'message' => __('Your password must be not null or must be between 8 and 40 characters'),
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('password can not empty'),
                ),
            ),
            'password_confirm' => array(
                'maxLength' => array(
                    'rule' => array('between', 8, 40),
                    'message' => __('Your password confirm must be not null or must be between 8 and 40 characters'),
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('password can not empty'),
                ),
                'compare' => array(
                    'rule' => array('validate_passwords'),
                    'message' => __('The password confirm you entered do not match'),
                )
            )
        );
        if ($this->validates()) {
            return true; //$data[$this->name]['password'] === $data[$this->name]['password_confirm'];
        } else {
            return false;
        }
    }

    /**
     * Validate data recruiter before insert or update.
     *
     * @author thailh
     * @param array $date Input array
     * @return bool Returns the boolean.
     */
    public function validateRecruiterInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'thumbnail_img' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadThumbnail_img", array()),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Check upload thumbnail of image.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadThumbnail_img() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile('User.thumbnail_img');
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

    /**
     * Validate data company before insert or update.
     *
     * @author thailh
     * @param array $date Input array
     * @return bool Returns the boolean.
     */
    public function validateCompanyInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 40),
                    'message' => __('Company name must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Company name can not empty'),
                )
            ),
            'thumbnail_img' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadCompany_thumbnail_img", array()),
                ),
            ),
            'background_img' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadBackground_img", array()),
                ),
            )
            ,
            'introduction_movie' => array(
                'checkUpload' => array(
                    'rule' => array("checkUploadIntroduction_movie", array()),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Check data thumbnail image of company before upload.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadCompany_thumbnail_img() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile("User.thumbnail_img");
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

    /**
     * Check data background image before upload.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadBackground_img() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile("User.background_img");
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

    /**
     * Check the data introduction movie before upload.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function checkUploadIntroduction_movie() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Video = new VideoComponent(new ComponentCollection());
        $file = $this->Common->getFile("User.introduction_movie");
        if (!empty($file['name'])) {
            if (!$this->Video->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Video->errorMsg);
            }
        }
        return true;
    }

    /**
     * Check the data register user before upload.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function validateRegister($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('First name can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxlength', 64),
                    'message' => __('First name must be no larger then %d character long'),
                ),
            ),
            'kana' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name kana can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxlength', 64),
                    'message' => __('Name kana must be no larger then %d character long'),
                ),
                'kata' => array(
                    'rule' => 'katakana',
                    'message' => __('Name kana must be katakana'),
                ),
            ),
            'email' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'maxLength' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 255),
                    'message' => __('Email must be no larger then %d character long'),
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => __('Email is invalid'),
                ),
            ),
            'birthday' => array(
                'date' => array(
                    'allowEmpty' => true,
                    'rule' => 'date',
                    'message' => __('Birthday is invalid'),
                ),
            ),
            'phone' => array(
                'maxLength' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxlength', 64),
                    'message' => __('Phone must be no larger then %d character long'),
                ),
                'format' => array(
                    'rule' => array('phone', '/^\d+$/'),
                    'message' => __('Phone is invalid'),
                ),
            ),
            'address1' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Address 1 can not empty'),
                ),
                'maxLength' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxlength', 256),
                    'message' => __('Address 1 must be no larger then %d character long'),
                ),
            ),
            'address2' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Address 2 can not empty'),
                ),
                'maxLength' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxlength', 256),
                    'message' => __('Address 2 must be no larger then %d character long'),
                ),
            ),
            'password' => array(
//                'notEmpty' => array(
//                    'rule' => 'notEmpty',
//                    'message' => __('Password can not empty'),
//                ),
                'maxLength' => array(
                    'allowEmpty' => true,
                    'rule' => array('between', 6, 40),
                    'message' => __('Password must be not null or must be between %d and %d characters'),
                ),
            ),
            'password_confirm' => array(
//                'notEmpty' => array(
//                    'rule' => 'notEmpty',
//                    'message' => __('Confirm password can not empty'),
//                ),
                'compare' => array(
                    'rule' => array('validate_passwords'),
                    'message' => __('The password confirm you entered do not match'),
                )
            )
        );

        if ($this->validates()) {
            return true;
        } else {
            return false;
        }
    }

}

