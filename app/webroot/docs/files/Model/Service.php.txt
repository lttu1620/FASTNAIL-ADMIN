<?php

/**
 * Services of model
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Service extends AppModel {

    public $name = 'Service';
    public $table = 'services';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Truongnn
     * @param array $data Input array.
     * @return bool Returns True if verify or otherwise.
     */
    public function validateInsertOrUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 2, 45),
                    'message' => __('Name must be between 2 to 45 characters')
                ),
            ),
        );
        return $this->validates();
    }

}

