$(document).ready(function() {
//////////////////////////////// Stop watch ////////////////////////////////////////
    function getDateTimeZone(offset) {
        // create Date object for current location
        d = new Date();

        // convert to msec
        // add local time zone offset
        // get UTC time in msec
        utc = d.getTime() + (d.getTimezoneOffset() * 60000);

        // create new Date object for different city
        // using supplied offset
        nd = new Date(utc + (3600000*offset));

        // return date
        return nd;
    }
	
	function covertTimeStampZone(timestamp, offset) {
        // create Date object for current location
        d = new Date(timestamp*1000);

        // convert to msec
        // add local time zone offset
        // get UTC time in msec
        utc = d.getTime() + (d.getTimezoneOffset() * 60000);

        // create new Date object for different city
        // using supplied offset
        nd = new Date(utc + (3600000*offset));

        // return timestamp
        return (nd.getTime()/1000);
    }

    function setCookie(cname,cvalue,exdays) {
        //var d = new Date();
        var d = getDateTimeZone('+9');
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname+"="+cvalue+"; "+expires;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function showTime(timestamp){
     	var d = getDateTimeZone('+9');
     	cur_timestamp = Math.floor(d.getTime()/1000) + 1;
		cookie_timestamp = timestamp;
        balance_seconds = cur_timestamp - cookie_timestamp;
        hour_time = parseInt(balance_seconds/3600);
        min_time = parseInt((balance_seconds - (hour_time*3600))/60);
        second_time =  balance_seconds - (hour_time*3600) -  (min_time*60);
		if(  hour_time > 0){
			if(hour_time < 10){
				hour_time = '0' + hour_time+":";
			}else{
				hour_time = hour_time+":";
			}
		}else{
			hour_time = "";
		}
        if(min_time < 10){
             min_time= '0'+ min_time;
        } else if (min_time > 59){
        	min_time= '00';
        }
        if(second_time <10) second_time= '0'+ second_time;
        time = hour_time + min_time +':'+second_time;
        return time;
    }

    function timeOrder() {
        var cookie_order=getCookie(order);
        if (cookie_order != "") {
			time = showTime(cookie_order);
            $('.timeFrom .hours_large').html(time);
            $('.running-time').html(time);
        } else {
        	   //var d = new Date();
               var d = getDateTimeZone('+9');
               //time = d.getHours() +':'+ d.getMinutes()+':'+d.getSeconds();
    		   var time = Math.floor(d.getTime()/1000);
	           setCookie(order, time, 1);
               $('.timeFrom .hours_large').html('00:01');
               $('.running-time').html('00:01');
        }
    }
    
    function f_timeOrder() {
        var cookie_order=getCookie(f_order);
        if (cookie_order != "") {
			time = showTime(cookie_order);
            $('.f_timeFrom .hours_large').html(time);
            $('.running-time').html(time);
        } else {
        	   //var d = new Date();
               var d = getDateTimeZone('+9');
               //time = d.getHours() +':'+ d.getMinutes()+':'+d.getSeconds();
    		   var time = Math.floor(d.getTime()/1000);
	           setCookie(f_order, time, 1);
               $('.f_timeFrom .hours_large').html('00:01');
               $('.running-time').html('00:01');
        }
    }
    function timeService() {
        var cookie_service=getCookie(service);
        if (cookie_service != "") {
        	time = showTime(cookie_service);
            $('.timeTo .hours_large').html(time);
            $('.running-time').html(time);
        } else {
        	   //var d = new Date();
               var d = getDateTimeZone('+9');
               //time = d.getHours() +':'+ d.getMinutes()+':'+d.getSeconds();
               var time = Math.floor(d.getTime()/1000);
               setCookie(service, time, 1);
               $('.timeTo .hours_large').html('00:01');
               $('.running-time').html('00:01');
        }
    }
    
    function f_timeService() {
        var cookie_service=getCookie(f_service);
        if (cookie_service != "") {
        	time = showTime(cookie_service);
            $('.f_timeTo .hours_large').html(time);
            $('.running-time').html(time);
        } else {
        	   //var d = new Date();
               var d = getDateTimeZone('+9');
               //time = d.getHours() +':'+ d.getMinutes()+':'+d.getSeconds();
               var time = Math.floor(d.getTime()/1000);
               setCookie(f_service, time, 1);
               $('.f_timeTo .hours_large').html('00:01');
               $('.running-time').html('00:01');
        }
    }
    function timeConsult() {
    	var cookie_consult=getCookie(consult);
        if (cookie_consult != "") {
            time = showTime(cookie_consult);
            $('.timeConsult .hours_large').html(time);
            $('.running-time').html(time);
        } else {
            //var d = new Date();
            var d = getDateTimeZone('+9');
            //time = d.getHours() +':'+ d.getMinutes()+':'+d.getSeconds();
            var time = Math.floor(d.getTime()/1000);
            setCookie(consult, time, 1);
            $('.timeConsult .hours_large').html('00:01');
            $('.running-time').html('00:01');
        }
    }

    function resetOrder(){
        setCookie(order, "", -1);
    }
    function resetService(){
        setCookie(service, "", -1);
    }
    function f_resetOrder(){
        setCookie(f_order, "", -1);
    }
    function f_resetService(){
        setCookie(f_service, "", -1);
    }
    function resetConsult(){
        setCookie(consult, "", -1);
    }
    function clockOrder(){
        timeOrder();
        valOrder= setInterval(timeOrder, 1000);
    }
    function clockService(){
        timeService();
        valService = setInterval(timeService, 1000);
    }
    function f_clockOrder(){
        f_timeOrder();
        f_valOrder= setInterval(f_timeOrder, 1000);
    }
    function f_clockService(){
        f_timeService();
        f_valService = setInterval(f_timeService, 1000);
    }
    function clockConsult(){
        timeConsult();
        valConsult = setInterval(timeConsult, 1000);
    }
    function stopOrder() {
        clearInterval(valOrder);
    }
    function stopService() {
        clearInterval(valService);
    }
    function f_stopOrder() {
        clearInterval(f_valOrder);
    }
    function f_stopService() {
        clearInterval(f_valService);
    }
    function stopConsult() {
        clearInterval(valConsult);
    }
    function onLoadTime(){
        //Remember tab Hand | Foot when click
        if(nailTab != "") {
            showSubOrderTab(nailTab);
        }
        
        var cookie_order=getCookie(order);
        if (cookie_order != "") {
        	time = showTime(cookie_order);
            $('.timeFrom .hours_large').html(time);
            $('.running-time').html(time);
            $('.nail_off a').html('終了');
            $('.nail_off a').data('on',0);
            clockOrder();
        }
        
        var cookie_service=getCookie(service);
        if (cookie_service != "") {
        	time = showTime(cookie_service);
            $('.timeTo .hours_large').html(time);
            $('.running-time').html(time);
            $('.service_off a').html('終了');
            $('.service_off a').data('on',0);
            clockService();
        }
        
        var f_cookie_order=getCookie(f_order);
        if (f_cookie_order != "") {
        	time = showTime(f_cookie_order);
            $('.f_timeFrom .hours_large').html(time);
            $('.running-time').html(time);
            $('.f_nail_off a').html('終了');
            $('.f_nail_off a').data('on',0);
            f_clockOrder();
        }
        
        var f_cookie_service=getCookie(f_service);
        if (f_cookie_service != "") {
        	time = showTime(f_cookie_service);
            $('.f_timeTo .hours_large').html(time);
            $('.running-time').html(time);
            $('.f_service_off a').html('終了');
            $('.f_service_off a').data('on',0);
            f_clockService();
        }
        
        var cookie_consult=getCookie(consult);
        if (cookie_consult != "") {
        	time = showTime(cookie_consult);
            $('.timeConsult .hours_large').html(time);
            $('.running-time').html(time);
            $('.consult_off a').html('終了');
            $('.consult_off a').data('on',0);
            clockConsult();
        }
    }

    onLoadTime();
    // click start date
    $('.popupContent .bgSalmonDark a').click(function() {
    	var existCookie = document.cookie.indexOf(start);
    	var nailist_id = $('.a_nailists').attr('data-id');
        if ($(this).hasClass('disable')) return false;
    	setCookie(start, 'done', 1);
        $(this).addClass('disable');
        $(this).html('受付済');
        nailist_name = $('.a_nailists').attr('data-name');
        $("#txt_start_nailist").text(nailist_name);
        //enable undo button
        //$('.popupContent .bgOrange a').removeClass('disable');
	    showUndoBox();
        $.ajax({
            url: baseUrl + 'ajax/stopwatchdialog?order_id=' + $(this).data('id')+'&start_date=1&nailist_id='+nailist_id,
            type: 'post',
            success: function(data) {
            }
        });
    });
    
    // click paid button
    $('.popupContent .bgSalmon a').click(function() {
    	var existCookie = document.cookie.indexOf(paid);
    	var nailist_id = $('.a_nailists').attr('data-id');
        if ($(this).hasClass('disable')) return false;
        if(nailist_id==''){
    		alert('ネイリストを選択してください');
    		return false;
    	}
        var in_case='';
	   	var nail_off = $('.nail_off a');
	    var service_off = $('.service_off a');
	    var consult_off = $('.consult_off a');
    	if($('.popupContent .bgSalmonDark a').hasClass("disable")!==true){
         	$('.popupContent .bgSalmonDark a').addClass("disable");
        }
         if(consult_off.text()=='開始' && nail_off.text()=='開始' && service_off.text()=='開始'){
            in_case = 'is_paid_no_start_time';
         }else if(consult_off.text()=='終了' && consult_off.hasClass('disable') === false) {
        	in_case = 'is_paid_consult_start';
         }else if(nail_off.text()=='終了' && nail_off.hasClass('disable') === false) {
        	in_case = 'is_paid_nail_start';
         }else if(service_off.text()=='終了' && service_off.hasClass('disable') === false) {
        	in_case = 'is_paid_service_start';
         }
        nail_off.addClass('disable');
       	service_off.addClass('disable');
       	consult_off.addClass('disable');

        $(this).addClass('disable');
        $(this).html('会計済');
        nailist_name = $('.a_nailists').attr('data-name');
        $("#txt_paid_nailist").text(nailist_name);
        //enable undo button
        //$('.popupContent .bgOrange a').removeClass('disable');
	    showUndoBox();
        $.ajax({
            url: baseUrl + 'ajax/stopwatchdialog?order_id=' + $(this).data('id')+'&is_paid=1&nailist_id='+nailist_id+"&in_case="+in_case,
            type: 'post',
            success: function(data) {
            	if(in_case == 'is_paid_no_start_time'){

                 }else if(in_case == 'is_paid_consult_start') {
                	resetConsult();
                    stopConsult();
                    $('.consult_right').html(data);
                 }else if(in_case == 'is_paid_nail_start') {
                	resetOrder();
                    stopOrder();
                    $('.order_right').html(data);
                 }else if(in_case == 'is_paid_service_start') {
                	resetService();
                    stopService();
                    $('.service_right').html(data);
                 }
            }
        });
    });
   
    //click  consult button
    $('.consult_off a').click(function() {
    	var existCookie = document.cookie.indexOf(consult);
    	var nailist_id = $('.a_nailists').attr('data-id'); 
        if ($(this).hasClass('disable')) return false;	
    	if(nailist_id == ''){
    		alert('ネイリストを選択してください');
    		return false;
    	}
    	
        data_on = $(this).data('on');
        if ( data_on == 1 ) {
            $(this).data('on',0);
            $(this).attr('data-on',0);
            $(this).html('終了');
            if($('.popupContent .bgSalmonDark a').hasClass("disable")!==true){
             	$('.popupContent .bgSalmonDark a').addClass("disable");
             }
        } else {
            $(this).data('on',1);
            $(this).addClass('disable');
        }
        nailist_name = $('.a_nailists').attr('data-name');
        $("#txt_consult_nailist").text(nailist_name);
        //enable undo button
        //$('.popupContent .bgOrange a').removeClass('disable');
        showUndoBox();
        $.ajax({
            url: baseUrl + 'ajax/stopwatchdialog?order_id=' + $(this).data('id')+'&is_consult='+data_on+'&nailist_id='+nailist_id,
            type: 'post',
            success: function(data) {
                if (data_on == 1) {
                    $('.consult_left').html(data);
                    clockConsult();
                }else {
                    $('.consult_right').html(data);
                    resetConsult();
					stopConsult();
                    $('.running-time').html('00:00');
                }
            }
        });
    });
    
    //click nail button
    $('.nail_off a').click(function() {
    	var existCookie = document.cookie.indexOf(order);
    	var nailist_id = $('.a_nailists').attr('data-id'); 
    	if ($(this).hasClass('disable')) return false;
        
        if (has_suborder == 1) {
            if (($('.f_nail_off a').text() == '開始' && $('.f_service_off a').text() == '開始')
                    || ($('.f_nail_off a').hasClass('disable') && $('.f_service_off a').hasClass('disable'))) {
            } else {
                alert('先にフットストップウォッチを止めてください。');
                return false;
            }
        }
        
    	if(nailist_id==''){
    		alert('ネイリストを選択してください');
    		return false;
    	}
        data_on = $(this).data('on');
        var in_case='';
        consult_off = $('.consult_off a');
        if ( data_on == 1 ) {
            $(this).data('on',0);
            $(this).attr('data-on',0);
            $(this).html('終了');
            if($('.popupContent .bgSalmonDark a').hasClass("disable")!==true){
            	$('.popupContent .bgSalmonDark a').addClass("disable");
            }
            if(consult_off.text()=='開始'){
            	in_case = 'nail_no_start_time';
            }else if(consult_off.text()=='終了' && consult_off.hasClass('disable') === false) {
               in_case = 'nail_consult_start';
            }
            consult_off.addClass('disable');
        } else {
            $(this).data('on',1);
            $(this).addClass('disable');
        }
        nailist_name = $('.a_nailists').attr('data-name');
        $("#txt_nail_nailist").text(nailist_name);
        //enable undo button
        //$('.popupContent .bgOrange a').removeClass('disable');
	    showUndoBox();
        $.ajax({
            url: baseUrl + 'ajax/stopwatchdialog?order_id=' + $(this).data('id')+'&is_order='+data_on+'&nailist_id='+nailist_id+"&in_case="+in_case,
            type: 'post',
            success: function(data) {
            	if (data_on == 1){
                     $('.order_left').html(data);
                     clockOrder();
                     if(in_case == 'nail_no_start_time'){

                     }else if(in_case == 'nail_consult_start') {
                        resetConsult();
                        stopConsult();
                        $('.consult_right').html(data);
                     }
                } else {
                    $('.order_right').html(data);
                    resetOrder();
                    stopOrder();
                    $('.running-time').html('00:00');
               }
            }
        });
    });
    
    //click foot nail button
    $('.f_nail_off a').click(function() {
    	var existCookie = document.cookie.indexOf(f_order);
    	var nailist_id = $('.a_nailists').attr('data-id'); 
    	if ($(this).hasClass('disable')) return false;
        
        if(($('.nail_off a').text() == '開始' && $('.service_off a').text() == '開始')
                || ($('.nail_off a').hasClass('disable') && $('.service_off a').hasClass('disable'))) {
        }else {
            alert('先にハンドストップウォッチを止めてください。');
    		return false;
        }
        
    	if(nailist_id==''){
    		alert('ネイリストを選択してください');
    		return false;
    	}
        data_on = $(this).data('on');
        var in_case='';
        consult_off = $('.consult_off a');
        if ( data_on == 1 ) {
            $(this).data('on',0);
            $(this).attr('data-on',0);
            $(this).html('終了');
            if($('.popupContent .bgSalmonDark a').hasClass("disable")!==true){
            	$('.popupContent .bgSalmonDark a').addClass("disable");
            }
            if(consult_off.text()=='開始'){
            	in_case = 'nail_no_start_time';
            }else if(consult_off.text()=='終了' && consult_off.hasClass('disable') === false) {
               in_case = 'nail_consult_start';
            }
            consult_off.addClass('disable');
        } else {
            $(this).data('on',1);
            $(this).addClass('disable');
        }
        nailist_name = $('.a_nailists').attr('data-name');
        $("#txt_nail_nailist").text(nailist_name);
        //enable undo button
	    showUndoBox();
        $.ajax({
            url: baseUrl + 'ajax/stopwatchdialog?order_id=' + $(this).data('id')+'&f_is_order='+data_on+'&nailist_id='+nailist_id+"&in_case="+in_case,
            type: 'post',
            success: function(data) {
            	if (data_on == 1){
                     $('.f_order_left').html(data);
                     f_clockOrder();
                     if(in_case == 'nail_no_start_time'){

                     }else if(in_case == 'nail_consult_start') {
                        resetConsult();
                        stopConsult();
                        $('.consult_right').html(data);
                     }
                } else {
                    $('.f_order_right').html(data);
                    f_resetOrder();
                    f_stopOrder();
                    $('.running-time').html('00:00');
               }
            }
        });
    });
    
    //click service button
    $('.service_off a').click(function() {
    	var existCookie = document.cookie.indexOf(service);
    	var nailist_id = $('.a_nailists').attr('data-id'); 
    	if ($(this).hasClass('disable')) return false;
        
        if (has_suborder == 1) {
            if(($('.f_nail_off a').text() == '開始' && $('.f_service_off a').text() == '開始')
                    || ($('.f_nail_off a').hasClass('disable') && $('.f_service_off a').hasClass('disable'))) {
            }else {
                alert('先にフットストップウォッチを止めてください。');
                return false;
            }
        }
        
    	if(nailist_id==''){
    		alert('ネイリストを選択してください');
    		return false;
    	}
        data_on = $(this).data('on');
        str = 'sr_start_date';
        var in_case='';
	   	var nail_off = $('.nail_off a');
	    var service_off = $('.service_off a');
	    var consult_off = $('.consult_off a');

        if ( data_on == 1 ) {
            $(this).data('on',0);
            $(this).attr('data-on',0);
            $(this).html('終了');
            str = 'sr_start_date';
            if($('.popupContent .bgSalmonDark a').hasClass("disable")!==true){
             	$('.popupContent .bgSalmonDark a').addClass("disable");
             }

             if(consult_off.text()=='開始' && nail_off.text()=='開始'){
            	in_case = 'service_no_start_time';
            }else if(consult_off.text()=='終了' && consult_off.hasClass('disable') === false) {
            	in_case = 'service_consult_start';
            }else if(nail_off.text()=='終了' && nail_off.hasClass('disable') === false) {
            	in_case = 'service_nail_start';
            }
         	nail_off.addClass('disable');
        	consult_off.addClass('disable');
        } else {
            $(this).data('on',1);
            $(this).addClass('disable');
            str = 'sr_end_date';
        }
        nailist_name = $('.a_nailists').attr('data-name');
        $("#txt_service_nailist").text(nailist_name);
        //enable undo button
	    showUndoBox();
        $.ajax({
            url: baseUrl + 'ajax/stopwatchdialog?order_id=' + $(this).data('id')+'&is_service='+data_on+'&nailist_id='+nailist_id + "&in_case="+in_case,
            type: 'post',
            success: function(data) {
            	if (data_on == 1) {
                    $('.service_left').html(data);
                    clockService();
                    if(in_case == 'service_no_start_time'){

                    }else if(in_case == 'service_consult_start') {
                    	resetConsult();
                        stopConsult();
                        $('.consult_right').html(data);
                    }else if(in_case == 'service_nail_start') {
                    	resetOrder();
                        stopOrder();
                        $('.order_right').html(data);
                    }
                }
                else {
                    $('.service_right').html(data);
                    resetService();
                    stopService();
                    $('.running-time').html('00:00');
                }
            }
        });
    });
    
    //click foot service button
    $('.f_service_off a').click(function() {
    	var existCookie = document.cookie.indexOf(f_service);
    	var nailist_id = $('.a_nailists').attr('data-id'); 
    	if ($(this).hasClass('disable')) return false;
        if(($('.nail_off a').text() == '開始' && $('.service_off a').text() == '開始')
                || ($('.nail_off a').hasClass('disable') && $('.service_off a').hasClass('disable'))) {
        }else {
            alert('先にハンドストップウォッチを止めてください。');
    		return false;
        }
    	if(nailist_id==''){
    		alert('ネイリストを選択してください');
    		return false;
    	}
        data_on = $(this).data('on');
        str = 'sr_start_date';
        var in_case='';
	   	var f_nail_off = $('.f_nail_off a');
	    var f_service_off = $('.f_service_off a');
	    var consult_off = $('.consult_off a');

        if ( data_on == 1 ) {
            $(this).data('on',0);
            $(this).attr('data-on',0);
            $(this).html('終了');
            str = 'sr_start_date';
            if($('.popupContent .bgSalmonDark a').hasClass("disable")!==true){
             	$('.popupContent .bgSalmonDark a').addClass("disable");
             }

             if(consult_off.text()=='開始' && f_nail_off.text()=='開始'){
            	in_case = 'service_no_start_time';
            }else if(consult_off.text()=='終了' && consult_off.hasClass('disable') === false) {
            	in_case = 'service_consult_start';
            }else if(f_nail_off.text()=='終了' && f_nail_off.hasClass('disable') === false) {
            	in_case = 'service_nail_start';
            }
         	f_nail_off.addClass('disable');
        	consult_off.addClass('disable');
        } else {
            $(this).data('on',1);
            $(this).addClass('disable');
            str = 'sr_end_date';
        }
        nailist_name = $('.a_nailists').attr('data-name');
        $("#txt_service_nailist").text(nailist_name);
        //enable undo button
	    showUndoBox();
        $.ajax({
            url: baseUrl + 'ajax/stopwatchdialog?order_id=' + $(this).data('id')+'&f_is_service='+data_on+'&nailist_id='+nailist_id + "&in_case="+in_case,
            type: 'post',
            success: function(data) {
            	if (data_on == 1) {
                    $('.f_service_left').html(data);
                    f_clockService();
                    if(in_case == 'service_no_start_time'){

                    }else if(in_case == 'service_consult_start') {
                    	f_resetConsult();
                        f_stopConsult();
                        $('.f_consult_right').html(data);
                    }else if(in_case == 'service_nail_start') {
                    	f_resetOrder();
                        f_stopOrder();
                        $('.f_order_right').html(data);
                    }
                }
                else {
                    $('.f_service_right').html(data);
                    f_resetService();
                    f_stopService();
                    $('.running-time').html('00:00');
                }
            }
        });
    });

	  $('#orderLogsButton').click(function () {
        if($('#popupWrapperLog').html() != ''){
            $("#popupWrapperLog, .bgBlack").addClass("active");
            return false;
        }
	    order_id = $(this).attr('data-order-id');
	        $.ajax({
	            type: "POST",
	            url: baseUrl + 'ajax/orderslog/' + order_id,
	            data: {},
	            success: function (response) {
	                $('#popupWrapperLog').html(response);
	            }
	        });
	    });
    // 変更画面はpopupで表示します。項目の並び順は名前順でお願いします。
     $('.nailInfo div.fLeft li div.title i').click(function() {
        var _attribute = $(this).data('object');
        var _nailTtype = $(this).data('nailtype');
         $.ajax({
                type: "POST",
                url: baseUrl + 'ajax/ordernails',
                data: {
                    attribute_id    : _attribute,
                    order_id        : $('#order_id').val(),
                    nail_id         : (_nailTtype == 'foot') ? $('#f_nail_id').val() : $('#nail_id').val()
                },
                beforeSend: function() {
                    $('#largeModal .modal-body').html('');
                },
                success: function (response) {
                    $('#largeModal .modal-body').html(response);
                    $('#largeModal').modal('show');
                }
        });
    });

      // 変更画面はpopupで表示します。項目の並び順は名前順でお願いします。
     $('#nailItem li div.itemContent').click(function() {
     $('.result_sumary').addClass('hidden');
        var _item = $(this).find('i').data('object');
        var _checked = [];
         $.each($("#item_list li span.item_price"), function() {
            _checked.push($(this).attr('id'));
        })
         //console.log(_checked);
        // return false;
         $.ajax({
                type: "POST",
                url: baseUrl + 'ajax/orderitems',
                data: {
                    item_id    : _item,
                    order_id        : $('#order_id').val(),
                    checked  : _checked
                },
                beforeSend: function() {
                    $('#largeModal .modal-body').html('');
                },
                success: function (response) {
                    $('#largeModal .modal-body').html(response);
                    $('#largeModal').modal('show');
                }
        });
    });
     
    /*Undo box*/
    var undo_interval='';
    var duration = 15; //15 seconds
    var display = $('#undo-medical-box .undo-time');
    var time_effect = 1000;
     startTimer =  function (duration, display) {//duration: seconds
    	    var timer = duration, minutes, seconds;
    	    undo_interval = setInterval(function () {
    	        minutes = parseInt(timer / 60, 10)
    	        seconds = parseInt(timer % 60, 10);

    	        minutes = minutes < 10 ? "0" + minutes : minutes;
    	        //seconds = seconds < 10 ? "0" + seconds : seconds;
    	        seconds = seconds < 10 ? seconds : seconds;

    	        //display.text(minutes + ":" + seconds);
    	        display.text(seconds);

    	        if (--timer < 0) {
    	            clearInterval(undo_interval);
    	           	$("#undo-medical-box").hide(time_effect);
    	        }
    	    }, 1000);
    	}
     
    showUndoBox = function(){
    	if(undo_interval) {
    		clearInterval(undo_interval);
    		$("#undo-medical-box").hide();
    	}
    	duration = 15; //15 seconds
    	startTimer(duration, display);
    	$("#undo-medical-box").show(time_effect);
    } 
 
    $(".undo-close a").click(function(){
    	clearInterval(undo_interval);
    	$("#undo-medical-box").hide(time_effect);
    }); 
    
    $(".undo-med-btn").click(function(){
    	
    	if ($(this).text() == '処理中...') return false;
    	$(this).text('処理中...');
    	var order_id = $(this).data('id');
    	clearInterval(undo_interval);
    	//console.log(undo_interval);
        $.ajax({
            url: baseUrl + 'ajax/undostopwatchdialog?order_id=' + order_id,
            type: 'post',
            success: function(response) {
            	res = JSON.parse(response);
                if(res.msg == 'FAILED'){
            		alert('システムエラー');
            		$("#undo-medical-box").hide(time_effect);
            		$(".undo-med-btn").text('取り消す');
            		return false;
            	}
           	 	setCookie(start, '', -1);
        	 	setCookie(paid, '', -1);
            	if(res.before.undo == 'consult_end_date'){
            		continue_time = covertTimeStampZone(parseInt(res.before.consult_start_date), '+9');
            		setCookie(consult, continue_time , 1);
            	}else if(res.before.undo == 'consult_start_date'){
            	   	resetConsult();
            	    stopConsult();
              	}else if(res.before.undo == 'off_end_date'){
                    continue_time = covertTimeStampZone(parseInt(res.before.off_start_date), '+9');
            		setCookie(order, continue_time, 1);
            	}else if(res.before.undo == 'off_start_date'){
                    resetOrder();
            	    stopOrder();
					if(res.after.consult_end_date==''){
                        if(res.after.consult_start_date != ''){
                            continue_time = covertTimeStampZone(parseInt(res.after.consult_start_date), '+9');
                            setCookie(consult, continue_time , 1);
                        }
					}
              	}else if(res.before.undo == 'f_off_end_date'){
                    continue_time = covertTimeStampZone(parseInt(res.before.f_off_start_date), '+9');
            		setCookie(f_order, continue_time, 1);
            	}else if(res.before.undo == 'f_off_start_date'){
                    f_resetOrder();
            	    f_stopOrder();
					if(res.after.consult_end_date==''){
                        if(res.after.consult_start_date != ''){
                            continue_time = covertTimeStampZone(parseInt(res.after.consult_start_date), '+9');
                            setCookie(consult, continue_time , 1);
                        }
					}
              	}else if(res.before.undo == 'sr_end_date'){
					continue_time = covertTimeStampZone(parseInt(res.before.sr_start_date), '+9');
            		setCookie(service, continue_time, 1);
            	}else if(res.before.undo == 'sr_start_date'){
	                resetService();
	                stopService();
					if(res.after.consult_end_date==''){
                        if(res.after.consult_start_date !=''){
                            continue_time = covertTimeStampZone(parseInt(res.after.consult_start_date), '+9');
                            setCookie(consult, continue_time, 1);
                        }
					}else if(res.after.off_end_date==''){
                        if(res.after.off_start_date != ''){
                            continue_time = covertTimeStampZone(parseInt(res.after.off_start_date),'+9');
                            setCookie(order, continue_time, 1);
                        }
					}
            	}else if(res.before.undo == 'f_sr_end_date'){
					continue_time = covertTimeStampZone(parseInt(res.before.f_sr_start_date), '+9');
            		setCookie(f_service, continue_time, 1);
            	}else if(res.before.undo == 'f_sr_start_date'){
	                f_resetService();
	                f_stopService();
					if(res.after.consult_end_date==''){
                        if(res.after.consult_start_date !=''){
                            continue_time = covertTimeStampZone(parseInt(res.after.consult_start_date), '+9');
                            setCookie(consult, continue_time, 1);
                        }
					}else if(res.after.f_off_end_date==''){
                        if(res.after.f_off_start_date != ''){
                            continue_time = covertTimeStampZone(parseInt(res.after.f_off_start_date),'+9');
                            setCookie(f_order, continue_time, 1);
                        }
					}
            	}else if(res.before.undo == 'is_paid'){
                    resetConsult();
                    stopConsult();
                    resetOrder();
                    stopOrder();
                    resetService();
                    stopService();
                    if(res.after.consult_start_date == ''){
                        //do nothing
                    }else if(res.after.consult_end_date ==''){
                        continue_time = covertTimeStampZone(parseInt(res.after.consult_start_date), '+9');
                        setCookie(consult, continue_time, 1);
                    }else if(res.after.off_end_date ==''){
                        continue_time = covertTimeStampZone(parseInt(res.after.off_start_date),'+9');
                        setCookie(order, continue_time, 1);
                    }else if(res.after.sr_end_date ==''){
                        continue_time = covertTimeStampZone(parseInt(res.after.sr_start_date),'+9');
                        setCookie(service, continue_time, 1);
                    }
                }
 
            	$.ajax({
                     url: baseUrl +  'ajax/medicalchartdialog?orderId=' + order_id + '&undo=1',
                     type: 'get',
                     success: function(data) {     
                    	 $('body').html(data);          	 
                     }
                 });
            }
        });
    });
    
    /*Dropdown nailist*/
    $('.drop-nailist').on('click',function(){
        $('#orders_nailists').toggle();
    });
    
    $("#orders_nailists").on("click", ".user .nailists", function() {
        $('.a_nailists').html($(this).html());
        $('.a_nailists').attr('data-id',$(this).data('id'));
        $('.a_nailists').attr('data-name',$(this).data('name'));
        $('.a_nailists').parent().addClass('user');
    });
    
});

showGroupAttr = function() {
    var attribute_id = $('#attribute_id').val();
    $('.group-attribute').hide();
    $('#attribute_' + attribute_id).show();
    
}

saveAttribute = function(order_id, nail_id) {
    var attribute_id = $('#attribute_id').val();
    var attribute_value = [];      
    var prefix = '';
    if($('#tab-foot-btn').length > 0 && $('#tab-foot-btn').hasClass('btn-active')) {
        prefix = 'f_';
    }
    
    $('#attribute_' + attribute_id + ' input').each(function(index) {
        if ($(this).is(':checked')) {
            attribute_value.push($(this).val());               
        }
    }); 
    var data = {            
        order_id: order_id,
        attribute_value: attribute_value.join(),
        attribute_id: attribute_id,
        nail_id : nail_id
    };
    $.ajax({
        type: "POST",
        url: baseUrl + 'ajax/ordernails',
        data: data,
        success: function (json) {              
            var response = JSON.parse(json); 
            //$('#popup_message').html(response['message']);
            $('.result_sumary').show().delay(5000).fadeOut();
            if ($('#' + prefix + attribute_id).length > 0 && response['error'] === 0) {
                $('#'+ prefix + attribute_id).html(response['data']);
                if (!$('#'+ prefix + attribute_id).hasClass('is_change')) {
                    $('#'+ prefix + attribute_id).addClass('is_change');
                }
            }
        }
    });
    return false;        
}

saveItems =  function(order_id) {
     var attribute_id = $('#attribute_id').val();
     $('#attribute_' + attribute_id + ' input').each(function(index) {
        if ($(this).is(':checked')) {
        	$('.result_sumary').show().delay(5000).fadeOut();
            var theOptionVal = $(this).val();
            if (theOptionVal != '' && $('#item_' + theOptionVal).length == 0) {
                $.ajax({
                    type: "get",
                    url: baseUrl + 'ajax/getiteminfo',
                    data: {id:theOptionVal},
                    success: function (response) {
                        if (response != '') {
                            $('#item_list').append(response);
                            updateTotalPrice();
                            $('#select_items option[value="' + theOptionVal + '"]').remove();
                        }
                    }
                });
                $('#select_items option[value="0"]').attr('selected','selected');
            }
        }else{
			removeItem($(this).val());
		}
    }); 
    
 return false; 
    
}

// sub order functions
showSubOrderTab = function(tab) {
    if(tab == 'hand') {
        $('.tab-detail-hand').show();
        $('.tab-detail-foot').hide();
        $('#tab-hand-btn').addClass("btn-active");
        $('#tab-foot-btn').removeClass("btn-active");
    }else if (tab == 'foot'){
        $('.tab-detail-hand').hide();
        $('.tab-detail-foot').show();
        $('#tab-hand-btn').removeClass("btn-active");
        $('#tab-foot-btn').addClass("btn-active");
    }
    //Remember tab Hand | Foot when click
    if (history.pushState) {
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?tab='+tab;
        window.history.pushState({path:newurl},'',newurl);
    }
}