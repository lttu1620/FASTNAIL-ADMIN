$(document).ready(function(){
    $(".time").click(function(){
        $("#popupWrapper, .bgBlack").addClass("active");
    });
    $(".close").click(function(){
        $("#popupWrapper, #popupWrapperLog, .bgBlack").removeClass("active");
    	return false;
    });
    
    $(".list").click(function(){
        $("#popupWrapperLog, .bgBlack").addClass("active");
    });
    
    //For medical chart form -----------
    $(document).on('click', '#btnSubmitMedicalChart', function(e) {
        //updateListFingers();
        updateListOfItems();
        /*
        if ( $('.rec33.bgSalmon').length != $('#sel_nail_add_length').find(":selected").val()) {
            alert ('You choose not enough nails');
            return false;
        }*/
        
        if ($('#formInitVal').val() != $("#MedicalChartForm").serialize()) {
            $('#formInitVal').val($("#MedicalChartForm").serialize());
            $('.loadingWrapper .loading').addClass('active');
            //submit
            $.ajax({
                type: "POST",
                url: $("#MedicalChartForm").attr('action'),
                data: $('#MedicalChartForm').serialize(),
                success: function (response) {
                    $('.messages').remove();
                    var res = $.parseJSON(response);
                    $.each( res, function( key, value ) {
                        if (key != 'succedMsg') {
                            $("input[name='data[Order][" + key + "]']").after('<p class="messages error ' + key + '">' + value + '</p>');
                        }
                    });
                    $('.loadingWrapper .loading').removeClass('active');
                }
            });
        }
    });
    
    $('#sel_problem').on('change', function(e) {
        changeProblem();
    });
    
    $('#f_sel_problem').on('change', function(e) {
        f_changeProblem();
    });
    
    changeProblem();
    f_changeProblem();
    
    //validate nail add length
    $('.rec33').click(function(){
        if ($(this).hasClass('bgSalmon')) {
            $(this).removeClass('bgSalmon');
        } else {
            if ( $('.rec33.bgSalmon').length >= $('#sel_nail_add_length').find(":selected").val()) {
                alert('Please choose only ' + $('#sel_nail_add_length').find(":selected").val() + ' nail(s)');
            } else {
                $(this).addClass('bgSalmon');
            }
        }
    });
    
    var currentNailAddLength;
    $('#sel_nail_add_length').focus(function(){
        currentNailAddLength = $('#sel_nail_add_length').find(":selected").val();
    });
    
    $('#sel_nail_add_length').change(function(e){
        if ( $('.rec33.bgSalmon').length > $('#sel_nail_add_length').find(":selected").val()) {
            alert("Cannot change because you've chosen " + $('.rec33.bgSalmon').length + ' nail(s)');
            $('#sel_nail_add_length option[value=' + currentNailAddLength + ']').attr('selected','selected');
        }
    });
    
    var f_currentNailAddLength;
    $('#f_sel_nail_add_length').focus(function(){
        f_currentNailAddLength = $('#f_sel_nail_add_length').find(":selected").val();
    });
    
    $('#f_sel_nail_add_length').change(function(e){
        if ( $('.rec33.bgSalmon').length > $('#f_sel_nail_add_length').find(":selected").val()) {
            alert("Cannot change because you've chosen " + $('.rec33.bgSalmon').length + ' nail(s)');
            $('#f_sel_nail_add_length option[value=' + f_currentNailAddLength + ']').attr('selected','selected');
        }
    });
    
    if($('#MedicalChartForm').length > 0){
        var medicalIdList = new cookieList("medicalIdList");
        medicalIdList.add("" + $('#order_id').val());
        if ($('#succeedContent').html() != '') {
            $('#succeedPopup').removeClass("Hidden");
            $('#bgPopup').removeClass("Hidden");
        }
        
        //updateListFingers();
        updateListOfItems();
        $('#formInitVal').val($("#MedicalChartForm").serialize());
        
        setInterval(function(){
            //submit form
            $('#btnSubmitMedicalChart').click();
        }, 10 * 1000);//10 seconds
        
        $("input").blur(function() {
            if ($(this).val() == '') {
                $('#btnSubmitMedicalChart').click();
            } else {
                var parts = $(this).attr('name').split(/\[|\]/);
                parts = parts.filter(function(v){return v!==''});
                var key = parts[parts.length - 1];
                
                $('.messages.error.' + key).remove();
            }
        });
    }
    
    $(document).on('click','*[data-event=close]',function(e){
        e.preventDefault();
        var target = $(this).data('target');
        $(target).addClass('Hidden');
        $('#bgPopup').addClass('Hidden');
    });
    
        
    $(document).on('click', '#choose_nail', function(e) {
        $('#btnSubmitMedicalChart').click();
        window.location.href = feBaseUrl + 'orders?token=' + $('#secret_key').val() + $('#secret_time').val() + '&order_id=' + $('#order_id').val() + '&link=1';
        /*
        var url = feBaseUrl + 'ajax/setadminorderid?key=' + $('#secret_key').val() + '&time=' + $('#secret_time').val() + '&order_id=' + $('#order_id').val();
        var W = $( window ).width();
        var H = $( window ).height();
        window.open(url, "popupWindow", "width=" + W + ",height=" + H + ",scrollbars=yes");
        */
    });
    
    $(document).on('click', '#choose_nail_hand', function(e) {
        $('#btnSubmitMedicalChart').click();
        window.location.href = feBaseUrl + 'orders?token=' + $('#secret_key').val() + $('#secret_time').val() + '&order_id=' + $('#order_id').val() + '&link=1&hf_section=1';
    });
    
    $(document).on('click', '#choose_nail_foot', function(e) {
        $('#btnSubmitMedicalChart').click();
        window.location.href = feBaseUrl + 'orders?token=' + $('#secret_key').val() + $('#secret_time').val() + '&order_id=' + $('#order_id').val() + '&link=1&hf_section=2';
    });
    
    $('#select_items').on('change', function(e) {
        var theOptionVal = $(this).val();
        if ($(this).val() != '' && $('#item_' + $(this).val()).length == 0) {
            $.ajax({
                type: "get",
                url: baseUrl + 'ajax/getiteminfo',
                data: {id: $(this).val()},
                success: function (response) {
                    if (response != '') {
                        $('#item_list').append(response);
                        updateTotalPrice();
                        $('#select_items option[value="' + theOptionVal + '"]').remove();
                    }
                }
            });
            $('#select_items option[value="0"]').attr('selected','selected');
        }
    });
    
    $('.ordered_item').on('blur', function(e) {
        updateTotalPrice();
    });
    
    $('#fade').popup({
        transition: 'all 0.3s',
        scrolllock: true
    });
    
    var updateNailInterval;
    $('#choose_nail_by_qrcode').on('click', function() {
        $('#fade').html('');
        var data_n = feBaseUrl + 'orders?token=' + $('#secret_key').val() + $('#secret_time').val() + '&order_id=' + $('#order_id').val();        
        var qrcode_n = new QRCode("fade");
        qrcode_n.makeCode(data_n); 
        
        var qrcodeStartTime = new Date(); 
        var qrcodeStartTimeInSeconds = qrcodeStartTime.getHours() * 60 * 60 + qrcodeStartTime.getMinutes() * 60 + qrcodeStartTime.getSeconds();
        
        updateNailInterval = setInterval(function(){
            var now     = new Date(); 
            var nowInSeconds = now.getHours() * 60 * 60 + now.getMinutes() * 60 + now.getSeconds();
        
            if (nowInSeconds - qrcodeStartTimeInSeconds >= adminOrderTimeout) {
                clearInterval(updateNailInterval);
            }
//            if ($('#fade_wrapper').css('display') == 'none') {
//                clearInterval(updateNailInterval);
//            }
            $.ajax({
                type: "get",
                url: baseUrl + 'ajax/orderupdatenail',
                data: {order_id: $('#order_id').val(), nail_id: $('#nail_id').val()},
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    if (data['status'] == 'OK') {
                        //clearInterval(updateNailInterval);
                        $('.picDetail').click();
                        $('#nail_id').val(data['nail_id']);
                        
                        $('#user_name').text(data['display_user_name']);
                        $('#user_code').attr('class', 'readonly-textbox W200');
                        $('#user_code').text(data['user_code']);
                        $('.registerButton').remove();
                        
                        $('#sel_nail_off option[value="2"]').text(data['nailOffText']);
                        
                        var price = parseInt(data['nail_price']);
                        price = price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        price = price.replace(/\.00$/, '');
                        $('#nail-price').html(price);
                        updateTotalPrice();
                        
                        if ($('#order_nail_img').length > 0) {
                            $('#order_nail_img').attr('src', data['nail_image_url']);
                            $('.picDetail .picNum').html('写真番号' + data['nail_photo_cd']);
                        } else {
                            $('#choose_nail a').html('編集');
                            $('#choose_nail').before('<div class="picNum">写真番号' + data['nail_photo_cd'] + '</div>');
                            $('#choose_nail').before('<div class="thumbnail"><img id="order_nail_img" src="' + data['nail_image_url'] + '" alt="" /></div>');
                            $('#choose_nail').attr('class', 'btn  bgSalmon W150 aCenter');
                        }
                    }
                }
            });
        }, 5 * 1000);//5 seconds
    });
    
    $('#choose_nail_hand_by_qrcode').on('click', function() {
        $('#fade').html('');
        var data_h = feBaseUrl + 'orders?token=' + $('#secret_key').val() + $('#secret_time').val() + '&order_id=' + $('#order_id').val() + '&hf_section=1';        
        var qrcode_h = new QRCode("fade");
        qrcode_h.makeCode(data_h);    

        var qrcodeStartTime = new Date(); 
        var qrcodeStartTimeInSeconds = qrcodeStartTime.getHours() * 60 * 60 + qrcodeStartTime.getMinutes() * 60 + qrcodeStartTime.getSeconds();
        
        updateNailInterval = setInterval(function(){
            var now     = new Date(); 
            var nowInSeconds = now.getHours() * 60 * 60 + now.getMinutes() * 60 + now.getSeconds();
        
            if (nowInSeconds - qrcodeStartTimeInSeconds >= adminOrderTimeout) {
                clearInterval(updateNailInterval);
            }
//            if ($('#fade_wrapper').css('display') == 'none') {
//                clearInterval(updateNailInterval);
//            }
            $.ajax({
                type: "get",
                url: baseUrl + 'ajax/orderupdatenail',
                data: {order_id: $('#order_id').val(), nail_id: $('#nail_id').val()},
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    if (data['status'] == 'OK') {
                        //clearInterval(updateNailInterval);
                        $('.picDetail').click();
                        $('#nail_id').val(data['nail_id']);
                        
                        $('#user_name').text(data['display_user_name']);
                        $('#user_code').attr('class', 'readonly-textbox W200');
                        $('#user_code').text(data['user_code']);
                        $('.registerButton').remove();
                        
                        $('#sel_nail_off option[value="2"]').text(data['nailOffText']);
                        
                        var price = parseInt(data['nail_price']);
                        price = price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        price = price.replace(/\.00$/, '');
                        $('#nail-price').html(price);
                        updateTotalPrice();
                        
                        if ($('#order_nail_img').length > 0) {
                            $('#order_nail_img').attr('src', data['nail_image_url']);
                            $('.picDetail .picNum').html('写真番号' + data['nail_photo_cd']);
                        } else {
                            $('#choose_nail_hand a').html('編集');
                            $('#choose_nail_hand').before('<div class="picNum">写真番号' + data['nail_photo_cd'] + '</div>');
                            $('#choose_nail_hand').before('<div class="thumbnail"><img id="order_nail_img" src="' + data['nail_image_url'] + '" alt="" /></div>');
                            $('#choose_nail_hand').attr('class', 'btn  bgSalmon W150 aCenter');
                        }
                    }
                }
            });
        }, 5 * 1000);//5 seconds
    });
    
    var f_updateNailInterval;
    $('#choose_nail_foot_by_qrcode').on('click', function() {
        $('#fade').html('');
        var data_f = feBaseUrl + 'orders?token=' + $('#secret_key').val() + $('#secret_time').val() + '&order_id=' + $('#order_id').val() + '&hf_section=2';        
        var qrcode_f = new QRCode("fade");
        qrcode_f.makeCode(data_f); 
        
        var qrcodeStartTime = new Date(); 
        var qrcodeStartTimeInSeconds = qrcodeStartTime.getHours() * 60 * 60 + qrcodeStartTime.getMinutes() * 60 + qrcodeStartTime.getSeconds();
        
        f_updateNailInterval = setInterval(function(){
            var now     = new Date(); 
            var nowInSeconds = now.getHours() * 60 * 60 + now.getMinutes() * 60 + now.getSeconds();
        
            if (nowInSeconds - qrcodeStartTimeInSeconds >= adminOrderTimeout) {
                clearInterval(f_updateNailInterval);
            }

            $.ajax({
                type: "get",
                url: baseUrl + 'ajax/orderupdatenail',
                data: {order_id: $('#order_id').val(), f_nail_id: $('#f_nail_id').val()},
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    if (data['status'] == 'OK') {
                        //clearInterval(updateNailInterval);
                        $('.picDetail').click();
                        $('#f_nail_id').val(data['nail_id']);
                        
                        $('#user_name').text(data['display_user_name']);
                        $('#user_code').attr('class', 'readonly-textbox W200');
                        $('#user_code').text(data['user_code']);
                        $('.registerButton').remove();
                        
                        $('#f_sel_nail_off option[value="2"]').text(data['nailOffText']);
                        
                        var price = parseInt(data['nail_price']);
                        price = price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                        price = price.replace(/\.00$/, '');
                        $('#f-nail-price').html(price);
                        updateTotalPrice();
                        
                        if ($('#order_nail_img').length > 0) {
                            $('#order_nail_img').attr('src', data['nail_image_url']);
                            $('.picDetail .picNum').html('写真番号' + data['nail_photo_cd']);
                        } else {
                            $('#choose_nail_foot a').html('編集');
                            $('#choose_nail_foot').before('<div class="picNum">写真番号' + data['nail_photo_cd'] + '</div>');
                            $('#choose_nail_foot').before('<div class="thumbnail"><img id="order_nail_img" src="' + data['nail_image_url'] + '" alt="" /></div>');
                            $('#choose_nail_foot').attr('class', 'btn  bgSalmon W150 aCenter');
                        }
                    }
                }
            });
        }, 5 * 1000);//5 seconds
    });
    
    $('.registerButton').remove();
    
    //makeQRCode();
    
});//On Ready

$(document).on("load",function(){
  alert('medical chart');
        exit();
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
        $("#headerWrapper").addClass("fix-top");
    } else {
        $("#headerWrapper").removeClass("fix-top");
    }
});

$(window).on('beforeunload', function() {
    if($('#MedicalChartForm').length > 0) {
        if ($('#formInitVal').val() != $("#MedicalChartForm").serialize()) {
            $('#btnSubmitMedicalChart').click();
            return 'Are you sure to leave?';
        }
    }
});

$(window).on('unload', function() {
    if($('#MedicalChartForm').length > 0) {
        //$.removeCookie("medicalid", { path: "/"});
        var medicalIdList = new cookieList("medicalIdList");
        medicalIdList.remove("" + $('#order_id').val());
    }
});

function changeProblem() {
     if ($('#sel_problem').find(":selected").val() == 1) {
        $("#txt_problem").removeAttr("disabled");
    } else {
        $("#txt_problem").attr("disabled","disabled");
    }
}

function f_changeProblem() {
     if ($('#f_sel_problem').find(":selected").val() == 1) {
        $("#f_txt_problem").removeAttr("disabled");
    } else {
        $("#f_txt_problem").attr("disabled","disabled");
    }
}

function updateListFingers() {
    $('#listFingers').html('');
    //var arrRightFingers = [];
    $('.rightHand.bgSalmon').each(function(index) {
        //arrRightFingers.push($(this).attr('id'));
        $('#listFingers').append('<input type="hidden" name="data[Order][rightNails][]" value="' + $(this).attr('id') + '"/>');
    });

    //var arrLeftFingers = [];
    $('.leftHand.bgSalmon').each(function(index) {
        //arrLeftFingers.push($(this).attr('id'));
        $('#listFingers').append('<input type="hidden" name="data[Order][leftNails][]" value="' + $(this).attr('id') + '"/>');
    });
}

function updateListOfItems() {
    $('#listOrderedItems').html('');
    $('.ordered_item').each(function(index) {
        if($(this).attr('id')){
            var itemIdParts = $(this).attr('id').split('_');
            $('#listOrderedItems').append('<input type="hidden" name="data[Order][items_id][]" value="' + itemIdParts[1] + '"/>');
            $('#listOrderedItems').append('<input type="hidden" name="data[Order][items_quantity][]" value="' + itemIdParts[1] + '_' + $(this).val() + '"/>');
        }
    });
    updateTotalPrice();
}

function removeItem(id) {
	
	if($('#item_' + id).closest('li').length === 0){
		return false;
	}
    $('#item_' + id).closest('li').remove();
    var previousOption = parseInt(id);
    var isBreak = 0;
    
    $("#select_items > option").each(function() {
        if (isBreak == 0) {
            if ($(this).val() > parseInt(id)) {
                isBreak = 1;
            } else {
                previousOption = $(this).val();
            }
        }
    });
    
    $('#select_items option[value="' + previousOption + '"]').after('<option value="' + id + '">' + $('#hidden_item_' + id).val() + '</option>');
    updateTotalPrice();
}

function updateTotalPrice() {
    var total_price = 0;
    if(has_suborder == 1) {
        total_price = parseInt($('#nail-price').html().replace(',', ''));
        total_price += parseInt($('#f-nail-price').html().replace(',', ''));
    }else {
        total_price = parseInt($('#nail-price').html().replace(',', ''));
    }
    
    $('.item_price').each(function() {
        total_price += parseInt($(this).html().replace(',', '')) * $('#item_' + $(this).attr('id')).val();
    });
    var total_price_tax = total_price + parseInt(total_price*itemTax);
    
    var total_price_point = parseInt(total_price_tax) - parseInt($("input[name*='r_point']").val()) - parseInt($("input[name*='fn_point']").val());
    total_price_point = total_price_point.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    total_price_point = total_price_point.replace(/\.00$/, '');
    
    total_price = total_price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    total_price = total_price.replace(/\.00$/, '');
    
    total_price_tax = total_price_tax.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    total_price_tax = total_price_tax.replace(/\.00$/, '');
    
    //$('#total_price').html(total_price + '円 ' + '('+total_price_tax+'円)');
    $('#total_price').html(total_price + '(税抜)');
    $('#total_price_point').html(total_price_point + '円(税込)');
}

function upQtyClick(id) {
    var newVal = $('#item_' + id).val();
    if (isNaN(newVal) || newVal == null || newVal == '') {
        newVal = 0;
    }
    newVal = parseInt(newVal) + 1;
    if (newVal <= 50) {
        $('#item_' + id).val(newVal);
        updateTotalPrice();
    }
}

function downQtyClick(id) {
    var newVal = $('#item_' + id).val();
    
    if (isNaN(newVal) || newVal == null || newVal == '') {
        newVal = 2;
    }
    newVal = parseInt(newVal) - 1;
    
    if (newVal > 0) {
        $('#item_' + id).val(newVal);
        updateTotalPrice();
    }
    
}

//define class numberOnly for fields which accept only number
$(document).on('keydown', '.onlyNumber', function(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) || 
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||         
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

$('.onlyNumber').bind("paste",function(e) {
    e.preventDefault();
});

function orderQRCode(order_id, nail_id) {
    $.ajax({
        url: baseUrl + 'ajax/addnailfororder',
        type: 'post',
        data: {
            order_id: order_id,
            nail_id: nail_id
        },
        success: function(response) {
            if (data['status'] == 'OK') {
                $('#user_name').text(data['user_name']);
                $('#user_code').attr('class', 'readonly-textbox W200');
                if (data['user_code'] != '') {
                    $('#user_code').text(data['user_code']);
                }
                
                $('.registerButton').remove();
                    
                if ($('#order_nail_img').length > 0) {
                    $('#order_nail_img').attr('src', data['image_url']);
                } else {
                    $('#choose_nail a').html('編集');
                    $('#choose_nail').before('<div class="picNum">写真番号' + data['photo_cd'] + '</div>');
                    $('#choose_nail').before('<div class="thumbnail"><img id="order_nail_img" src="' + data['image_url'] + '" alt="" /></div>');
                    $('#choose_nail').attr('class', 'btn  bgSalmon W150 aCenter');
                }
                $("html, body").animate({ scrollTop: 0 }, "fast");
            } else {
                alert(data['message']);
            }
        }
    });
} 
function qrCodeScan() {
    var href = window.location.href;
    var ptr = href.lastIndexOf("#");
    if (ptr > 0) {
        href = href.substr(0, ptr);
    }
    if (navigator.appVersion.indexOf('iPhone') > -1) {
        var link = 'itmss://itunes.apple.com/en/app/barcodes-scanner/id417257150?mt=8';
    } else if (navigator.userAgent.indexOf('Android') > -1) {
        var link = "intent://scan/?ret=" + escape(href + "#{CODE}") + "#Intent;scheme=zxing;package=com.google.zxing.client.android;end";
    }
    window.location.href = "zxing://scan/?ret=" + escape(href + "#{CODE}");
    if (link) {
        setTimeout(function qrCodeScan() {window.location.href = link;}, 5);
    }	

}

function hashChange() {      
    var hash = window.location.hash.substr(1);   
    if (typeof hash == 'undefined' || hash == '') {
        return false;
    }
    var nail_id = unescape(hash);
    orderQRCode($('#order_id').val(), nail_id);
    return true;
}       

$(function(){
    $(window).bind('hashchange', function() {
       hashChange();
   });			        
});

var qrcode;

function makeQRCode () { 
    var data = feBaseUrl + 'orders?token=' + $('#secret_key').val() + $('#secret_time').val() + '&order_id=' + $('#order_id').val();
    //var data = 'http://10.10.8.57/oceanize/fastnail/frontend/ajax/setadminorderid?token=' + $('#secret_key').val() + '&time=' + $('#secret_time').val() + '&order_id=' + $('#order_id').val();
    var qrcode = new QRCode("fade");
    qrcode.makeCode(data);    
}
