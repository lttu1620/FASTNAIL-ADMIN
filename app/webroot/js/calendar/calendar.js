if (function(t, e) {
        function n(t) {
            var e = t.length,
                n = ue.type(t);
            return ue.isWindow(t) ? !1 : 1 === t.nodeType && e ? !0 : "array" === n || "function" !== n && (0 === e || "number" == typeof e && e > 0 && e - 1 in t)
        }

        function i(t) {
            var e = Te[t] = {};
            return ue.each(t.match(he) || [], function(t, n) {
                e[n] = !0
            }), e
        }

        function o(t, n, i, o) {
            if (ue.acceptData(t)) {
                var r, s, a = ue.expando,
                    l = t.nodeType,
                    c = l ? ue.cache : t,
                    u = l ? t[a] : t[a] && a;
                if (u && c[u] && (o || c[u].data) || i !== e || "string" != typeof n) return u || (u = l ? t[a] = ee.pop() || ue.guid++ : a), c[u] || (c[u] = l ? {} : {
                    toJSON: ue.noop
                }), ("object" == typeof n || "function" == typeof n) && (o ? c[u] = ue.extend(c[u], n) : c[u].data = ue.extend(c[u].data, n)), s = c[u], o || (s.data || (s.data = {}), s = s.data), i !== e && (s[ue.camelCase(n)] = i), "string" == typeof n ? (r = s[n], null == r && (r = s[ue.camelCase(n)])) : r = s, r
            }
        }

        function r(t, e, n) {
            if (ue.acceptData(t)) {
                var i, o, r = t.nodeType,
                    s = r ? ue.cache : t,
                    l = r ? t[ue.expando] : ue.expando;
                if (s[l]) {
                    if (e && (i = n ? s[l] : s[l].data)) {
                        ue.isArray(e) ? e = e.concat(ue.map(e, ue.camelCase)) : e in i ? e = [e] : (e = ue.camelCase(e), e = e in i ? [e] : e.split(" ")), o = e.length;
                        for (; o--;) delete i[e[o]];
                        if (n ? !a(i) : !ue.isEmptyObject(i)) return
                    }(n || (delete s[l].data, a(s[l]))) && (r ? ue.cleanData([t], !0) : ue.support.deleteExpando || s != s.window ? delete s[l] : s[l] = null)
                }
            }
        }

        function s(t, n, i) {
            if (i === e && 1 === t.nodeType) {
                var o = "data-" + n.replace(Se, "-$1").toLowerCase();
                if (i = t.getAttribute(o), "string" == typeof i) {
                    try {
                        i = "true" === i ? !0 : "false" === i ? !1 : "null" === i ? null : +i + "" === i ? +i : $e.test(i) ? ue.parseJSON(i) : i
                    } catch (r) {}
                    ue.data(t, n, i)
                } else i = e
            }
            return i
        }

        function a(t) {
            var e;
            for (e in t)
                if (("data" !== e || !ue.isEmptyObject(t[e])) && "toJSON" !== e) return !1;
            return !0
        }

        function l() {
            return !0
        }

        function c() {
            return !1
        }

        function u() {
            try {
                return G.activeElement
            } catch (t) {}
        }

        function d(t, e) {
            do t = t[e]; while (t && 1 !== t.nodeType);
            return t
        }

        function h(t, e, n) {
            if (ue.isFunction(e)) return ue.grep(t, function(t, i) {
                return !!e.call(t, i, t) !== n
            });
            if (e.nodeType) return ue.grep(t, function(t) {
                return t === e !== n
            });
            if ("string" == typeof e) {
                if (qe.test(e)) return ue.filter(e, t, n);
                e = ue.filter(e, t)
            }
            return ue.grep(t, function(t) {
                return ue.inArray(t, e) >= 0 !== n
            })
        }

        function p(t) {
            var e = Ye.split("|"),
                n = t.createDocumentFragment();
            if (n.createElement)
                for (; e.length;) n.createElement(e.pop());
            return n
        }

        function f(t, e) {
            return ue.nodeName(t, "table") && ue.nodeName(1 === e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
        }

        function m(t) {
            return t.type = (null !== ue.find.attr(t, "type")) + "/" + t.type, t
        }

        function g(t) {
            var e = rn.exec(t.type);
            return e ? t.type = e[1] : t.removeAttribute("type"), t
        }

        function v(t, e) {
            for (var n, i = 0; null != (n = t[i]); i++) ue._data(n, "globalEval", !e || ue._data(e[i], "globalEval"))
        }

        function b(t, e) {
            if (1 === e.nodeType && ue.hasData(t)) {
                var n, i, o, r = ue._data(t),
                    s = ue._data(e, r),
                    a = r.events;
                if (a) {
                    delete s.handle, s.events = {};
                    for (n in a)
                        for (i = 0, o = a[n].length; o > i; i++) ue.event.add(e, n, a[n][i])
                }
                s.data && (s.data = ue.extend({}, s.data))
            }
        }

        function y(t, e) {
            var n, i, o;
            if (1 === e.nodeType) {
                if (n = e.nodeName.toLowerCase(), !ue.support.noCloneEvent && e[ue.expando]) {
                    o = ue._data(e);
                    for (i in o.events) ue.removeEvent(e, i, o.handle);
                    e.removeAttribute(ue.expando)
                }
                "script" === n && e.text !== t.text ? (m(e).text = t.text, g(e)) : "object" === n ? (e.parentNode && (e.outerHTML = t.outerHTML), ue.support.html5Clone && t.innerHTML && !ue.trim(e.innerHTML) && (e.innerHTML = t.innerHTML)) : "input" === n && en.test(t.type) ? (e.defaultChecked = e.checked = t.checked, e.value !== t.value && (e.value = t.value)) : "option" === n ? e.defaultSelected = e.selected = t.defaultSelected : ("input" === n || "textarea" === n) && (e.defaultValue = t.defaultValue)
            }
        }

        function _(t, n) {
            var i, o, r = 0,
                s = typeof t.getElementsByTagName !== U ? t.getElementsByTagName(n || "*") : typeof t.querySelectorAll !== U ? t.querySelectorAll(n || "*") : e;
            if (!s)
                for (s = [], i = t.childNodes || t; null != (o = i[r]); r++) !n || ue.nodeName(o, n) ? s.push(o) : ue.merge(s, _(o, n));
            return n === e || n && ue.nodeName(t, n) ? ue.merge([t], s) : s
        }

        function w(t) {
            en.test(t.type) && (t.defaultChecked = t.checked)
        }

        function x(t, e) {
            if (e in t) return e;
            for (var n = e.charAt(0).toUpperCase() + e.slice(1), i = e, o = Tn.length; o--;)
                if (e = Tn[o] + n, e in t) return e;
            return i
        }

        function C(t, e) {
            return t = e || t, "none" === ue.css(t, "display") || !ue.contains(t.ownerDocument, t)
        }

        function k(t, e) {
            for (var n, i, o, r = [], s = 0, a = t.length; a > s; s++) i = t[s], i.style && (r[s] = ue._data(i, "olddisplay"), n = i.style.display, e ? (r[s] || "none" !== n || (i.style.display = ""), "" === i.style.display && C(i) && (r[s] = ue._data(i, "olddisplay", j(i.nodeName)))) : r[s] || (o = C(i), (n && "none" !== n || !o) && ue._data(i, "olddisplay", o ? n : ue.css(i, "display"))));
            for (s = 0; a > s; s++) i = t[s], i.style && (e && "none" !== i.style.display && "" !== i.style.display || (i.style.display = e ? r[s] || "" : "none"));
            return t
        }

        function T(t, e, n) {
            var i = bn.exec(e);
            return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : e
        }

        function $(t, e, n, i, o) {
            for (var r = n === (i ? "border" : "content") ? 4 : "width" === e ? 1 : 0, s = 0; 4 > r; r += 2) "margin" === n && (s += ue.css(t, n + kn[r], !0, o)), i ? ("content" === n && (s -= ue.css(t, "padding" + kn[r], !0, o)), "margin" !== n && (s -= ue.css(t, "border" + kn[r] + "Width", !0, o))) : (s += ue.css(t, "padding" + kn[r], !0, o), "padding" !== n && (s += ue.css(t, "border" + kn[r] + "Width", !0, o)));
            return s
        }

        function S(t, e, n) {
            var i = !0,
                o = "width" === e ? t.offsetWidth : t.offsetHeight,
                r = dn(t),
                s = ue.support.boxSizing && "border-box" === ue.css(t, "boxSizing", !1, r);
            if (0 >= o || null == o) {
                if (o = hn(t, e, r), (0 > o || null == o) && (o = t.style[e]), yn.test(o)) return o;
                i = s && (ue.support.boxSizingReliable || o === t.style[e]), o = parseFloat(o) || 0
            }
            return o + $(t, e, n || (s ? "border" : "content"), i, r) + "px"
        }

        function j(t) {
            var e = G,
                n = wn[t];
            return n || (n = D(t, e), "none" !== n && n || (un = (un || ue("<iframe frameborder='0' width='0' height='0'/>").css("cssText", "display:block !important")).appendTo(e.documentElement), e = (un[0].contentWindow || un[0].contentDocument).document, e.write("<!doctype html><html><body>"), e.close(), n = D(t, e), un.detach()), wn[t] = n), n
        }

        function D(t, e) {
            var n = ue(e.createElement(t)).appendTo(e.body),
                i = ue.css(n[0], "display");
            return n.remove(), i
        }

        function E(t, e, n, i) {
            var o;
            if (ue.isArray(e)) ue.each(e, function(e, o) {
                n || Sn.test(t) ? i(t, o) : E(t + "[" + ("object" == typeof o ? e : "") + "]", o, n, i)
            });
            else if (n || "object" !== ue.type(e)) i(t, e);
            else
                for (o in e) E(t + "[" + o + "]", e[o], n, i)
        }

        function P(t) {
            return function(e, n) {
                "string" != typeof e && (n = e, e = "*");
                var i, o = 0,
                    r = e.toLowerCase().match(he) || [];
                if (ue.isFunction(n))
                    for (; i = r[o++];) "+" === i[0] ? (i = i.slice(1) || "*", (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n)
            }
        }

        function A(t, n, i, o) {
            function r(l) {
                var c;
                return s[l] = !0, ue.each(t[l] || [], function(t, l) {
                    var u = l(n, i, o);
                    return "string" != typeof u || a || s[u] ? a ? !(c = u) : e : (n.dataTypes.unshift(u), r(u), !1)
                }), c
            }
            var s = {},
                a = t === Vn;
            return r(n.dataTypes[0]) || !s["*"] && r("*")
        }

        function N(t, n) {
            var i, o, r = ue.ajaxSettings.flatOptions || {};
            for (o in n) n[o] !== e && ((r[o] ? t : i || (i = {}))[o] = n[o]);
            return i && ue.extend(!0, t, i), t
        }

        function M(t, n, i) {
            for (var o, r, s, a, l = t.contents, c = t.dataTypes;
                "*" === c[0];) c.shift(), r === e && (r = t.mimeType || n.getResponseHeader("Content-Type"));
            if (r)
                for (a in l)
                    if (l[a] && l[a].test(r)) {
                        c.unshift(a);
                        break
                    }
            if (c[0] in i) s = c[0];
            else {
                for (a in i) {
                    if (!c[0] || t.converters[a + " " + c[0]]) {
                        s = a;
                        break
                    }
                    o || (o = a)
                }
                s = s || o
            }
            return s ? (s !== c[0] && c.unshift(s), i[s]) : e
        }

        function H(t, e, n, i) {
            var o, r, s, a, l, c = {},
                u = t.dataTypes.slice();
            if (u[1])
                for (s in t.converters) c[s.toLowerCase()] = t.converters[s];
            for (r = u.shift(); r;)
                if (t.responseFields[r] && (n[t.responseFields[r]] = e), !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = r, r = u.shift())
                    if ("*" === r) r = l;
                    else if ("*" !== l && l !== r) {
                if (s = c[l + " " + r] || c["* " + r], !s)
                    for (o in c)
                        if (a = o.split(" "), a[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                            s === !0 ? s = c[o] : c[o] !== !0 && (r = a[0], u.unshift(a[1]));
                            break
                        }
                if (s !== !0)
                    if (s && t["throws"]) e = s(e);
                    else try {
                        e = s(e)
                    } catch (d) {
                        return {
                            state: "parsererror",
                            error: s ? d : "No conversion from " + l + " to " + r
                        }
                    }
            }
            return {
                state: "success",
                data: e
            }
        }

        function I() {
            try {
                return new t.XMLHttpRequest
            } catch (e) {}
        }

        function z() {
            try {
                return new t.ActiveXObject("Microsoft.XMLHTTP")
            } catch (e) {}
        }

        function O() {
            return setTimeout(function() {
                Zn = e
            }), Zn = ue.now()
        }

        function L(t, e, n) {
            for (var i, o = (ri[e] || []).concat(ri["*"]), r = 0, s = o.length; s > r; r++)
                if (i = o[r].call(n, e, t)) return i
        }

        function F(t, e, n) {
            var i, o, r = 0,
                s = oi.length,
                a = ue.Deferred().always(function() {
                    delete l.elem
                }),
                l = function() {
                    if (o) return !1;
                    for (var e = Zn || O(), n = Math.max(0, c.startTime + c.duration - e), i = n / c.duration || 0, r = 1 - i, s = 0, l = c.tweens.length; l > s; s++) c.tweens[s].run(r);
                    return a.notifyWith(t, [c, r, n]), 1 > r && l ? n : (a.resolveWith(t, [c]), !1)
                },
                c = a.promise({
                    elem: t,
                    props: ue.extend({}, e),
                    opts: ue.extend(!0, {
                        specialEasing: {}
                    }, n),
                    originalProperties: e,
                    originalOptions: n,
                    startTime: Zn || O(),
                    duration: n.duration,
                    tweens: [],
                    createTween: function(e, n) {
                        var i = ue.Tween(t, c.opts, e, n, c.opts.specialEasing[e] || c.opts.easing);
                        return c.tweens.push(i), i
                    },
                    stop: function(e) {
                        var n = 0,
                            i = e ? c.tweens.length : 0;
                        if (o) return this;
                        for (o = !0; i > n; n++) c.tweens[n].run(1);
                        return e ? a.resolveWith(t, [c, e]) : a.rejectWith(t, [c, e]), this
                    }
                }),
                u = c.props;
            for (W(u, c.opts.specialEasing); s > r; r++)
                if (i = oi[r].call(c, t, u, c.opts)) return i;
            return ue.map(u, L, c), ue.isFunction(c.opts.start) && c.opts.start.call(t, c), ue.fx.timer(ue.extend(l, {
                elem: t,
                anim: c,
                queue: c.opts.queue
            })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
        }

        function W(t, e) {
            var n, i, o, r, s;
            for (n in t)
                if (i = ue.camelCase(n), o = e[i], r = t[n], ue.isArray(r) && (o = r[1], r = t[n] = r[0]), n !== i && (t[i] = r, delete t[n]), s = ue.cssHooks[i], s && "expand" in s) {
                    r = s.expand(r), delete t[i];
                    for (n in r) n in t || (t[n] = r[n], e[n] = o)
                } else e[i] = o
        }

        function q(t, e, n) {
            var i, o, r, s, a, l, c = this,
                u = {},
                d = t.style,
                h = t.nodeType && C(t),
                p = ue._data(t, "fxshow");
            n.queue || (a = ue._queueHooks(t, "fx"), null == a.unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function() {
                a.unqueued || l()
            }), a.unqueued++, c.always(function() {
                c.always(function() {
                    a.unqueued--, ue.queue(t, "fx").length || a.empty.fire()
                })
            })), 1 === t.nodeType && ("height" in e || "width" in e) && (n.overflow = [d.overflow, d.overflowX, d.overflowY], "inline" === ue.css(t, "display") && "none" === ue.css(t, "float") && (ue.support.inlineBlockNeedsLayout && "inline" !== j(t.nodeName) ? d.zoom = 1 : d.display = "inline-block")), n.overflow && (d.overflow = "hidden", ue.support.shrinkWrapBlocks || c.always(function() {
                d.overflow = n.overflow[0], d.overflowX = n.overflow[1], d.overflowY = n.overflow[2]
            }));
            for (i in e)
                if (o = e[i], ei.exec(o)) {
                    if (delete e[i], r = r || "toggle" === o, o === (h ? "hide" : "show")) continue;
                    u[i] = p && p[i] || ue.style(t, i)
                }
            if (!ue.isEmptyObject(u)) {
                p ? "hidden" in p && (h = p.hidden) : p = ue._data(t, "fxshow", {}), r && (p.hidden = !h), h ? ue(t).show() : c.done(function() {
                    ue(t).hide()
                }), c.done(function() {
                    var e;
                    ue._removeData(t, "fxshow");
                    for (e in u) ue.style(t, e, u[e])
                });
                for (i in u) s = L(h ? p[i] : 0, i, c), i in p || (p[i] = s.start, h && (s.end = s.start, s.start = "width" === i || "height" === i ? 1 : 0))
            }
        }

        function R(t, e, n, i, o) {
            return new R.prototype.init(t, e, n, i, o)
        }

        function V(t, e) {
            var n, i = {
                    height: t
                },
                o = 0;
            for (e = e ? 1 : 0; 4 > o; o += 2 - e) n = kn[o], i["margin" + n] = i["padding" + n] = t;
            return e && (i.opacity = i.width = t), i
        }

        function B(t) {
            return ue.isWindow(t) ? t : 9 === t.nodeType ? t.defaultView || t.parentWindow : !1
        }
        var Y, Q, U = typeof e,
            X = t.location,
            G = t.document,
            J = G.documentElement,
            K = t.jQuery,
            Z = t.$,
            te = {},
            ee = [],
            ne = "1.10.1",
            ie = ee.concat,
            oe = ee.push,
            re = ee.slice,
            se = ee.indexOf,
            ae = te.toString,
            le = te.hasOwnProperty,
            ce = ne.trim,
            ue = function(t, e) {
                return new ue.fn.init(t, e, Q)
            },
            de = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
            he = /\S+/g,
            pe = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
            fe = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
            me = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
            ge = /^[\],:{}\s]*$/,
            ve = /(?:^|:|,)(?:\s*\[)+/g,
            be = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
            ye = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,
            _e = /^-ms-/,
            we = /-([\da-z])/gi,
            xe = function(t, e) {
                return e.toUpperCase()
            },
            Ce = function(t) {
                (G.addEventListener || "load" === t.type || "complete" === G.readyState) && (ke(), ue.ready())
            },
            ke = function() {
                G.addEventListener ? (G.removeEventListener("DOMContentLoaded", Ce, !1), t.removeEventListener("load", Ce, !1)) : (G.detachEvent("onreadystatechange", Ce), t.detachEvent("onload", Ce))
            };
        ue.fn = ue.prototype = {
                jquery: ne,
                constructor: ue,
                init: function(t, n, i) {
                    var o, r;
                    if (!t) return this;
                    if ("string" == typeof t) {
                        if (o = "<" === t.charAt(0) && ">" === t.charAt(t.length - 1) && t.length >= 3 ? [null, t, null] : fe.exec(t), !o || !o[1] && n) return !n || n.jquery ? (n || i).find(t) : this.constructor(n).find(t);
                        if (o[1]) {
                            if (n = n instanceof ue ? n[0] : n, ue.merge(this, ue.parseHTML(o[1], n && n.nodeType ? n.ownerDocument || n : G, !0)), me.test(o[1]) && ue.isPlainObject(n))
                                for (o in n) ue.isFunction(this[o]) ? this[o](n[o]) : this.attr(o, n[o]);
                            return this
                        }
                        if (r = G.getElementById(o[2]), r && r.parentNode) {
                            if (r.id !== o[2]) return i.find(t);
                            this.length = 1, this[0] = r
                        }
                        return this.context = G, this.selector = t, this
                    }
                    return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : ue.isFunction(t) ? i.ready(t) : (t.selector !== e && (this.selector = t.selector, this.context = t.context), ue.makeArray(t, this))
                },
                selector: "",
                length: 0,
                toArray: function() {
                    return re.call(this)
                },
                get: function(t) {
                    return null == t ? this.toArray() : 0 > t ? this[this.length + t] : this[t]
                },
                pushStack: function(t) {
                    var e = ue.merge(this.constructor(), t);
                    return e.prevObject = this, e.context = this.context, e
                },
                each: function(t, e) {
                    return ue.each(this, t, e)
                },
                ready: function(t) {
                    return ue.ready.promise().done(t), this
                },
                slice: function() {
                    return this.pushStack(re.apply(this, arguments))
                },
                first: function() {
                    return this.eq(0)
                },
                last: function() {
                    return this.eq(-1)
                },
                eq: function(t) {
                    var e = this.length,
                        n = +t + (0 > t ? e : 0);
                    return this.pushStack(n >= 0 && e > n ? [this[n]] : [])
                },
                map: function(t) {
                    return this.pushStack(ue.map(this, function(e, n) {
                        return t.call(e, n, e)
                    }))
                },
                end: function() {
                    return this.prevObject || this.constructor(null)
                },
                push: oe,
                sort: [].sort,
                splice: [].splice
            }, ue.fn.init.prototype = ue.fn, ue.extend = ue.fn.extend = function() {
                var t, n, i, o, r, s, a = arguments[0] || {},
                    l = 1,
                    c = arguments.length,
                    u = !1;
                for ("boolean" == typeof a && (u = a, a = arguments[1] || {}, l = 2), "object" == typeof a || ue.isFunction(a) || (a = {}), c === l && (a = this, --l); c > l; l++)
                    if (null != (r = arguments[l]))
                        for (o in r) t = a[o], i = r[o], a !== i && (u && i && (ue.isPlainObject(i) || (n = ue.isArray(i))) ? (n ? (n = !1, s = t && ue.isArray(t) ? t : []) : s = t && ue.isPlainObject(t) ? t : {}, a[o] = ue.extend(u, s, i)) : i !== e && (a[o] = i));
                return a
            }, ue.extend({
                expando: "jQuery" + (ne + Math.random()).replace(/\D/g, ""),
                noConflict: function(e) {
                    return t.$ === ue && (t.$ = Z), e && t.jQuery === ue && (t.jQuery = K), ue
                },
                isReady: !1,
                readyWait: 1,
                holdReady: function(t) {
                    t ? ue.readyWait++ : ue.ready(!0)
                },
                ready: function(t) {
                    if (t === !0 ? !--ue.readyWait : !ue.isReady) {
                        if (!G.body) return setTimeout(ue.ready);
                        ue.isReady = !0, t !== !0 && --ue.readyWait > 0 || (Y.resolveWith(G, [ue]), ue.fn.trigger && ue(G).trigger("ready").off("ready"))
                    }
                },
                isFunction: function(t) {
                    return "function" === ue.type(t)
                },
                isArray: Array.isArray || function(t) {
                    return "array" === ue.type(t)
                },
                isWindow: function(t) {
                    return null != t && t == t.window
                },
                isNumeric: function(t) {
                    return !isNaN(parseFloat(t)) && isFinite(t)
                },
                type: function(t) {
                    return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? te[ae.call(t)] || "object" : typeof t
                },
                isPlainObject: function(t) {
                    var n;
                    if (!t || "object" !== ue.type(t) || t.nodeType || ue.isWindow(t)) return !1;
                    try {
                        if (t.constructor && !le.call(t, "constructor") && !le.call(t.constructor.prototype, "isPrototypeOf")) return !1
                    } catch (i) {
                        return !1
                    }
                    if (ue.support.ownLast)
                        for (n in t) return le.call(t, n);
                    for (n in t);
                    return n === e || le.call(t, n)
                },
                isEmptyObject: function(t) {
                    var e;
                    for (e in t) return !1;
                    return !0
                },
                error: function(t) {
                    throw Error(t)
                },
                parseHTML: function(t, e, n) {
                    if (!t || "string" != typeof t) return null;
                    "boolean" == typeof e && (n = e, e = !1), e = e || G;
                    var i = me.exec(t),
                        o = !n && [];
                    return i ? [e.createElement(i[1])] : (i = ue.buildFragment([t], e, o), o && ue(o).remove(), ue.merge([], i.childNodes))
                },
                parseJSON: function(n) {
                    return t.JSON && t.JSON.parse ? t.JSON.parse(n) : null === n ? n : "string" == typeof n && (n = ue.trim(n), n && ge.test(n.replace(be, "@").replace(ye, "]").replace(ve, ""))) ? Function("return " + n)() : (ue.error("Invalid JSON: " + n), e)
                },
                parseXML: function(n) {
                    var i, o;
                    if (!n || "string" != typeof n) return null;
                    try {
                        t.DOMParser ? (o = new DOMParser, i = o.parseFromString(n, "text/xml")) : (i = new ActiveXObject("Microsoft.XMLDOM"), i.async = "false", i.loadXML(n))
                    } catch (r) {
                        i = e
                    }
                    return i && i.documentElement && !i.getElementsByTagName("parsererror").length || ue.error("Invalid XML: " + n), i
                },
                noop: function() {},
                globalEval: function(e) {
                    e && ue.trim(e) && (t.execScript || function(e) {
                        t.eval.call(t, e)
                    })(e)
                },
                camelCase: function(t) {
                    return t.replace(_e, "ms-").replace(we, xe)
                },
                nodeName: function(t, e) {
                    return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
                },
                each: function(t, e, i) {
                    var o, r = 0,
                        s = t.length,
                        a = n(t);
                    if (i) {
                        if (a)
                            for (; s > r && (o = e.apply(t[r], i), o !== !1); r++);
                        else
                            for (r in t)
                                if (o = e.apply(t[r], i), o === !1) break
                    } else if (a)
                        for (; s > r && (o = e.call(t[r], r, t[r]), o !== !1); r++);
                    else
                        for (r in t)
                            if (o = e.call(t[r], r, t[r]), o === !1) break; return t
                },
                trim: ce && !ce.call("\ufeff\xa0") ? function(t) {
                    return null == t ? "" : ce.call(t)
                } : function(t) {
                    return null == t ? "" : (t + "").replace(pe, "")
                },
                makeArray: function(t, e) {
                    var i = e || [];
                    return null != t && (n(Object(t)) ? ue.merge(i, "string" == typeof t ? [t] : t) : oe.call(i, t)), i
                },
                inArray: function(t, e, n) {
                    var i;
                    if (e) {
                        if (se) return se.call(e, t, n);
                        for (i = e.length, n = n ? 0 > n ? Math.max(0, i + n) : n : 0; i > n; n++)
                            if (n in e && e[n] === t) return n
                    }
                    return -1
                },
                merge: function(t, n) {
                    var i = n.length,
                        o = t.length,
                        r = 0;
                    if ("number" == typeof i)
                        for (; i > r; r++) t[o++] = n[r];
                    else
                        for (; n[r] !== e;) t[o++] = n[r++];
                    return t.length = o, t
                },
                grep: function(t, e, n) {
                    var i, o = [],
                        r = 0,
                        s = t.length;
                    for (n = !!n; s > r; r++) i = !!e(t[r], r), n !== i && o.push(t[r]);
                    return o
                },
                map: function(t, e, i) {
					if (t == undefined) {
						return false;
					}
                    var o, r = 0,
                        s = t.length,
                        a = n(t),
                        l = [];
                    if (a)
                        for (; s > r; r++) o = e(t[r], r, i), null != o && (l[l.length] = o);
                    else
                        for (r in t) o = e(t[r], r, i), null != o && (l[l.length] = o);
                    return ie.apply([], l)
                },
                guid: 1,
                proxy: function(t, n) {
                    var i, o, r;
                    return "string" == typeof n && (r = t[n], n = t, t = r), ue.isFunction(t) ? (i = re.call(arguments, 2), o = function() {
                        return t.apply(n || this, i.concat(re.call(arguments)))
                    }, o.guid = t.guid = t.guid || ue.guid++, o) : e
                },
                access: function(t, n, i, o, r, s, a) {
                    var l = 0,
                        c = t.length,
                        u = null == i;
                    if ("object" === ue.type(i)) {
                        r = !0;
                        for (l in i) ue.access(t, n, l, i[l], !0, s, a)
                    } else if (o !== e && (r = !0, ue.isFunction(o) || (a = !0), u && (a ? (n.call(t, o), n = null) : (u = n, n = function(t, e, n) {
                            return u.call(ue(t), n)
                        })), n))
                        for (; c > l; l++) n(t[l], i, a ? o : o.call(t[l], l, n(t[l], i)));
                    return r ? t : u ? n.call(t) : c ? n(t[0], i) : s
                },
                now: function() {
                    return (new Date).getTime()
                },
                swap: function(t, e, n, i) {
                    var o, r, s = {};
                    for (r in e) s[r] = t.style[r], t.style[r] = e[r];
                    o = n.apply(t, i || []);
                    for (r in e) t.style[r] = s[r];
                    return o
                }
            }), ue.ready.promise = function(e) {
                if (!Y)
                    if (Y = ue.Deferred(), "complete" === G.readyState) setTimeout(ue.ready);
                    else if (G.addEventListener) G.addEventListener("DOMContentLoaded", Ce, !1), t.addEventListener("load", Ce, !1);
                else {
                    G.attachEvent("onreadystatechange", Ce), t.attachEvent("onload", Ce);
                    var n = !1;
                    try {
                        n = null == t.frameElement && G.documentElement
                    } catch (i) {}
                    n && n.doScroll && function o() {
                        if (!ue.isReady) {
                            try {
                                n.doScroll("left")
                            } catch (t) {
                                return setTimeout(o, 50)
                            }
                            ke(), ue.ready()
                        }
                    }()
                }
                return Y.promise(e)
            }, ue.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(t, e) {
                te["[object " + e + "]"] = e.toLowerCase()
            }), Q = ue(G),
            function(t, e) {
                function n(t, e, n, i) {
                    var o, r, s, a, l, c, u, d, h, p;
                    if ((e ? e.ownerDocument || e : V) !== I && H(e), e = e || I, n = n || [], !t || "string" != typeof t) return n;
                    if (1 !== (a = e.nodeType) && 9 !== a) return [];
                    if (O && !i) {
                        if (o = Ce.exec(t))
                            if (s = o[1]) {
                                if (9 === a) {
                                    if (r = e.getElementById(s), !r || !r.parentNode) return n;
                                    if (r.id === s) return n.push(r), n
                                } else if (e.ownerDocument && (r = e.ownerDocument.getElementById(s)) && q(e, r) && r.id === s) return n.push(r), n
                            } else {
                                if (o[2]) return oe.apply(n, e.getElementsByTagName(t)), n;
                                if ((s = o[3]) && S.getElementsByClassName && e.getElementsByClassName) return oe.apply(n, e.getElementsByClassName(s)), n
                            }
                        if (S.qsa && (!L || !L.test(t))) {
                            if (d = u = R, h = e, p = 9 === a && t, 1 === a && "object" !== e.nodeName.toLowerCase()) {
                                for (c = m(t), (u = e.getAttribute("id")) ? d = u.replace($e, "\\$&") : e.setAttribute("id", d), d = "[id='" + d + "'] ", l = c.length; l--;) c[l] = d + g(c[l]);
                                h = ve.test(t) && e.parentNode || e, p = c.join(",")
                            }
                            if (p) try {
                                return oe.apply(n, h.querySelectorAll(p)), n
                            } catch (f) {} finally {
                                u || e.removeAttribute("id")
                            }
                        }
                    }
                    return k(t.replace(fe, "$1"), e, n, i)
                }

                function i(t) {
                    return xe.test(t + "")
                }

                function o() {
                    function t(n, i) {
                        return e.push(n += " ") > D.cacheLength && delete t[e.shift()], t[n] = i
                    }
                    var e = [];
                    return t
                }

                function r(t) {
                    return t[R] = !0, t
                }

                function s(t) {
                    var e = I.createElement("div");
                    try {
                        return !!t(e)
                    } catch (n) {
                        return !1
                    } finally {
                        e.parentNode && e.parentNode.removeChild(e), e = null
                    }
                }

                function a(t, e, n) {
                    t = t.split("|");
                    for (var i, o = t.length, r = n ? null : e; o--;)(i = D.attrHandle[t[o]]) && i !== e || (D.attrHandle[t[o]] = r)
                }

                function l(t, e) {
                    var n = t.getAttributeNode(e);
                    return n && n.specified ? n.value : t[e] === !0 ? e.toLowerCase() : null
                }

                function c(t, e) {
                    return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
                }

                function u(t) {
                    return "input" === t.nodeName.toLowerCase() ? t.defaultValue : e
                }

                function d(t, e) {
                    var n = e && t,
                        i = n && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || Z) - (~t.sourceIndex || Z);
                    if (i) return i;
                    if (n)
                        for (; n = n.nextSibling;)
                            if (n === e) return -1;
                    return t ? 1 : -1
                }

                function h(t) {
                    return function(e) {
                        var n = e.nodeName.toLowerCase();
                        return "input" === n && e.type === t
                    }
                }

                function p(t) {
                    return function(e) {
                        var n = e.nodeName.toLowerCase();
                        return ("input" === n || "button" === n) && e.type === t
                    }
                }

                function f(t) {
                    return r(function(e) {
                        return e = +e, r(function(n, i) {
                            for (var o, r = t([], n.length, e), s = r.length; s--;) n[o = r[s]] && (n[o] = !(i[o] = n[o]))
                        })
                    })
                }

                function m(t, e) {
                    var i, o, r, s, a, l, c, u = U[t + " "];
                    if (u) return e ? 0 : u.slice(0);
                    for (a = t, l = [], c = D.preFilter; a;) {
                        (!i || (o = me.exec(a))) && (o && (a = a.slice(o[0].length) || a), l.push(r = [])), i = !1, (o = ge.exec(a)) && (i = o.shift(), r.push({
                            value: i,
                            type: o[0].replace(fe, " ")
                        }), a = a.slice(i.length));
                        for (s in D.filter) !(o = we[s].exec(a)) || c[s] && !(o = c[s](o)) || (i = o.shift(), r.push({
                            value: i,
                            type: s,
                            matches: o
                        }), a = a.slice(i.length));
                        if (!i) break
                    }
                    return e ? a.length : a ? n.error(t) : U(t, l).slice(0)
                }

                function g(t) {
                    for (var e = 0, n = t.length, i = ""; n > e; e++) i += t[e].value;
                    return i
                }

                function v(t, e, n) {
                    var i = e.dir,
                        o = n && "parentNode" === i,
                        r = Y++;
                    return e.first ? function(e, n, r) {
                        for (; e = e[i];)
                            if (1 === e.nodeType || o) return t(e, n, r)
                    } : function(e, n, s) {
                        var a, l, c, u = B + " " + r;
                        if (s) {
                            for (; e = e[i];)
                                if ((1 === e.nodeType || o) && t(e, n, s)) return !0
                        } else
                            for (; e = e[i];)
                                if (1 === e.nodeType || o)
                                    if (c = e[R] || (e[R] = {}), (l = c[i]) && l[0] === u) {
                                        if ((a = l[1]) === !0 || a === j) return a === !0
                                    } else if (l = c[i] = [u], l[1] = t(e, n, s) || j, l[1] === !0) return !0
                    }
                }

                function b(t) {
                    return t.length > 1 ? function(e, n, i) {
                        for (var o = t.length; o--;)
                            if (!t[o](e, n, i)) return !1;
                        return !0
                    } : t[0]
                }

                function y(t, e, n, i, o) {
                    for (var r, s = [], a = 0, l = t.length, c = null != e; l > a; a++)(r = t[a]) && (!n || n(r, i, o)) && (s.push(r), c && e.push(a));
                    return s
                }

                function _(t, e, n, i, o, s) {
                    return i && !i[R] && (i = _(i)), o && !o[R] && (o = _(o, s)), r(function(r, s, a, l) {
                        var c, u, d, h = [],
                            p = [],
                            f = s.length,
                            m = r || C(e || "*", a.nodeType ? [a] : a, []),
                            g = !t || !r && e ? m : y(m, h, t, a, l),
                            v = n ? o || (r ? t : f || i) ? [] : s : g;
                        if (n && n(g, v, a, l), i)
                            for (c = y(v, p), i(c, [], a, l), u = c.length; u--;)(d = c[u]) && (v[p[u]] = !(g[p[u]] = d));
                        if (r) {
                            if (o || t) {
                                if (o) {
                                    for (c = [], u = v.length; u--;)(d = v[u]) && c.push(g[u] = d);
                                    o(null, v = [], c, l)
                                }
                                for (u = v.length; u--;)(d = v[u]) && (c = o ? se.call(r, d) : h[u]) > -1 && (r[c] = !(s[c] = d))
                            }
                        } else v = y(v === s ? v.splice(f, v.length) : v), o ? o(null, s, v, l) : oe.apply(s, v)
                    })
                }

                function w(t) {
                    for (var e, n, i, o = t.length, r = D.relative[t[0].type], s = r || D.relative[" "], a = r ? 1 : 0, l = v(function(t) {
                            return t === e
                        }, s, !0), c = v(function(t) {
                            return se.call(e, t) > -1
                        }, s, !0), u = [function(t, n, i) {
                            return !r && (i || n !== N) || ((e = n).nodeType ? l(t, n, i) : c(t, n, i))
                        }]; o > a; a++)
                        if (n = D.relative[t[a].type]) u = [v(b(u), n)];
                        else {
                            if (n = D.filter[t[a].type].apply(null, t[a].matches), n[R]) {
                                for (i = ++a; o > i && !D.relative[t[i].type]; i++);
                                return _(a > 1 && b(u), a > 1 && g(t.slice(0, a - 1).concat({
                                    value: " " === t[a - 2].type ? "*" : ""
                                })).replace(fe, "$1"), n, i > a && w(t.slice(a, i)), o > i && w(t = t.slice(i)), o > i && g(t))
                            }
                            u.push(n)
                        }
                    return b(u)
                }

                function x(t, e) {
                    var i = 0,
                        o = e.length > 0,
                        s = t.length > 0,
                        a = function(r, a, l, c, u) {
                            var d, h, p, f = [],
                                m = 0,
                                g = "0",
                                v = r && [],
                                b = null != u,
                                _ = N,
                                w = r || s && D.find.TAG("*", u && a.parentNode || a),
                                x = B += null == _ ? 1 : Math.random() || .1;
                            for (b && (N = a !== I && a, j = i); null != (d = w[g]); g++) {
                                if (s && d) {
                                    for (h = 0; p = t[h++];)
                                        if (p(d, a, l)) {
                                            c.push(d);
                                            break
                                        }
                                    b && (B = x, j = ++i)
                                }
                                o && ((d = !p && d) && m--, r && v.push(d))
                            }
                            if (m += g, o && g !== m) {
                                for (h = 0; p = e[h++];) p(v, f, a, l);
                                if (r) {
                                    if (m > 0)
                                        for (; g--;) v[g] || f[g] || (f[g] = ne.call(c));
                                    f = y(f)
                                }
                                oe.apply(c, f), b && !r && f.length > 0 && m + e.length > 1 && n.uniqueSort(c)
                            }
                            return b && (B = x, N = _), v
                        };
                    return o ? r(a) : a
                }

                function C(t, e, i) {
                    for (var o = 0, r = e.length; r > o; o++) n(t, e[o], i);
                    return i
                }

                function k(t, e, n, i) {
                    var o, r, s, a, l, c = m(t);
                    if (!i && 1 === c.length) {
                        if (r = c[0] = c[0].slice(0), r.length > 2 && "ID" === (s = r[0]).type && S.getById && 9 === e.nodeType && O && D.relative[r[1].type]) {
                            if (e = (D.find.ID(s.matches[0].replace(Se, je), e) || [])[0], !e) return n;
                            t = t.slice(r.shift().value.length)
                        }
                        for (o = we.needsContext.test(t) ? 0 : r.length; o-- && (s = r[o], !D.relative[a = s.type]);)
                            if ((l = D.find[a]) && (i = l(s.matches[0].replace(Se, je), ve.test(r[0].type) && e.parentNode || e))) {
                                if (r.splice(o, 1), t = i.length && g(r), !t) return oe.apply(n, i), n;
                                break
                            }
                    }
                    return A(t, c)(i, e, !O, n, ve.test(t)), n
                }

                function T() {}
                var $, S, j, D, E, P, A, N, M, H, I, z, O, L, F, W, q, R = "sizzle" + -new Date,
                    V = t.document,
                    B = 0,
                    Y = 0,
                    Q = o(),
                    U = o(),
                    X = o(),
                    G = !1,
                    J = function() {
                        return 0
                    },
                    K = typeof e,
                    Z = 1 << 31,
                    te = {}.hasOwnProperty,
                    ee = [],
                    ne = ee.pop,
                    ie = ee.push,
                    oe = ee.push,
                    re = ee.slice,
                    se = ee.indexOf || function(t) {
                        for (var e = 0, n = this.length; n > e; e++)
                            if (this[e] === t) return e;
                        return -1
                    },
                    ae = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                    le = "[\\x20\\t\\r\\n\\f]",
                    ce = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                    de = ce.replace("w", "w#"),
                    he = "\\[" + le + "*(" + ce + ")" + le + "*(?:([*^$|!~]?=)" + le + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + de + ")|)|)" + le + "*\\]",
                    pe = ":(" + ce + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + he.replace(3, 8) + ")*)|.*)\\)|)",
                    fe = RegExp("^" + le + "+|((?:^|[^\\\\])(?:\\\\.)*)" + le + "+$", "g"),
                    me = RegExp("^" + le + "*," + le + "*"),
                    ge = RegExp("^" + le + "*([>+~]|" + le + ")" + le + "*"),
                    ve = RegExp(le + "*[+~]"),
                    be = RegExp("=" + le + "*([^\\]'\"]*)" + le + "*\\]", "g"),
                    ye = RegExp(pe),
                    _e = RegExp("^" + de + "$"),
                    we = {
                        ID: RegExp("^#(" + ce + ")"),
                        CLASS: RegExp("^\\.(" + ce + ")"),
                        TAG: RegExp("^(" + ce.replace("w", "w*") + ")"),
                        ATTR: RegExp("^" + he),
                        PSEUDO: RegExp("^" + pe),
                        CHILD: RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + le + "*(even|odd|(([+-]|)(\\d*)n|)" + le + "*(?:([+-]|)" + le + "*(\\d+)|))" + le + "*\\)|)", "i"),
                        bool: RegExp("^(?:" + ae + ")$", "i"),
                        needsContext: RegExp("^" + le + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + le + "*((?:-\\d)?\\d*)" + le + "*\\)|)(?=[^-]|$)", "i")
                    },
                    xe = /^[^{]+\{\s*\[native \w/,
                    Ce = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                    ke = /^(?:input|select|textarea|button)$/i,
                    Te = /^h\d$/i,
                    $e = /'|\\/g,
                    Se = RegExp("\\\\([\\da-f]{1,6}" + le + "?|(" + le + ")|.)", "ig"),
                    je = function(t, e, n) {
                        var i = "0x" + e - 65536;
                        return i !== i || n ? e : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(55296 | i >> 10, 56320 | 1023 & i)
                    };
                try {
                    oe.apply(ee = re.call(V.childNodes), V.childNodes), ee[V.childNodes.length].nodeType
                } catch (De) {
                    oe = {
                        apply: ee.length ? function(t, e) {
                            ie.apply(t, re.call(e))
                        } : function(t, e) {
                            for (var n = t.length, i = 0; t[n++] = e[i++];);
                            t.length = n - 1
                        }
                    }
                }
                P = n.isXML = function(t) {
                    var e = t && (t.ownerDocument || t).documentElement;
                    return e ? "HTML" !== e.nodeName : !1
                }, S = n.support = {}, H = n.setDocument = function(t) {
                    var n = t ? t.ownerDocument || t : V,
                        o = n.parentWindow;
                    return n !== I && 9 === n.nodeType && n.documentElement ? (I = n, z = n.documentElement, O = !P(n), o && o.frameElement && o.attachEvent("onbeforeunload", function() {
                        H()
                    }), S.attributes = s(function(t) {
                        return t.innerHTML = "<a href='#'></a>", a("type|href|height|width", c, "#" === t.firstChild.getAttribute("href")), a(ae, l, null == t.getAttribute("disabled")), t.className = "i", !t.getAttribute("className")
                    }), S.input = s(function(t) {
                        return t.innerHTML = "<input>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
                    }), a("value", u, S.attributes && S.input), S.getElementsByTagName = s(function(t) {
                        return t.appendChild(n.createComment("")), !t.getElementsByTagName("*").length
                    }), S.getElementsByClassName = s(function(t) {
                        return t.innerHTML = "<div class='a'></div><div class='a i'></div>", t.firstChild.className = "i", 2 === t.getElementsByClassName("i").length
                    }), S.getById = s(function(t) {
                        return z.appendChild(t).id = R, !n.getElementsByName || !n.getElementsByName(R).length
                    }), S.getById ? (D.find.ID = function(t, e) {
                        if (typeof e.getElementById !== K && O) {
                            var n = e.getElementById(t);
                            return n && n.parentNode ? [n] : []
                        }
                    }, D.filter.ID = function(t) {
                        var e = t.replace(Se, je);
                        return function(t) {
                            return t.getAttribute("id") === e
                        }
                    }) : (delete D.find.ID, D.filter.ID = function(t) {
                        var e = t.replace(Se, je);
                        return function(t) {
                            var n = typeof t.getAttributeNode !== K && t.getAttributeNode("id");
                            return n && n.value === e
                        }
                    }), D.find.TAG = S.getElementsByTagName ? function(t, n) {
                        return typeof n.getElementsByTagName !== K ? n.getElementsByTagName(t) : e
                    } : function(t, e) {
                        var n, i = [],
                            o = 0,
                            r = e.getElementsByTagName(t);
                        if ("*" === t) {
                            for (; n = r[o++];) 1 === n.nodeType && i.push(n);
                            return i
                        }
                        return r
                    }, D.find.CLASS = S.getElementsByClassName && function(t, n) {
                        return typeof n.getElementsByClassName !== K && O ? n.getElementsByClassName(t) : e
                    }, F = [], L = [], (S.qsa = i(n.querySelectorAll)) && (s(function(t) {
                        t.innerHTML = "<select><option selected=''></option></select>", t.querySelectorAll("[selected]").length || L.push("\\[" + le + "*(?:value|" + ae + ")"), t.querySelectorAll(":checked").length || L.push(":checked")
                    }), s(function(t) {
                        var e = n.createElement("input");
                        e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("t", ""), t.querySelectorAll("[t^='']").length && L.push("[*^$]=" + le + "*(?:''|\"\")"), t.querySelectorAll(":enabled").length || L.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), L.push(",.*:")
                    })), (S.matchesSelector = i(W = z.webkitMatchesSelector || z.mozMatchesSelector || z.oMatchesSelector || z.msMatchesSelector)) && s(function(t) {
                        S.disconnectedMatch = W.call(t, "div"), W.call(t, "[s!='']:x"), F.push("!=", pe)
                    }), L = L.length && RegExp(L.join("|")), F = F.length && RegExp(F.join("|")), q = i(z.contains) || z.compareDocumentPosition ? function(t, e) {
                        var n = 9 === t.nodeType ? t.documentElement : t,
                            i = e && e.parentNode;
                        return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)))
                    } : function(t, e) {
                        if (e)
                            for (; e = e.parentNode;)
                                if (e === t) return !0;
                        return !1
                    }, S.sortDetached = s(function(t) {
                        return 1 & t.compareDocumentPosition(n.createElement("div"))
                    }), J = z.compareDocumentPosition ? function(t, e) {
                        if (t === e) return G = !0, 0;
                        var i = e.compareDocumentPosition && t.compareDocumentPosition && t.compareDocumentPosition(e);
                        return i ? 1 & i || !S.sortDetached && e.compareDocumentPosition(t) === i ? t === n || q(V, t) ? -1 : e === n || q(V, e) ? 1 : M ? se.call(M, t) - se.call(M, e) : 0 : 4 & i ? -1 : 1 : t.compareDocumentPosition ? -1 : 1
                    } : function(t, e) {
                        var i, o = 0,
                            r = t.parentNode,
                            s = e.parentNode,
                            a = [t],
                            l = [e];
                        if (t === e) return G = !0, 0;
                        if (!r || !s) return t === n ? -1 : e === n ? 1 : r ? -1 : s ? 1 : M ? se.call(M, t) - se.call(M, e) : 0;
                        if (r === s) return d(t, e);
                        for (i = t; i = i.parentNode;) a.unshift(i);
                        for (i = e; i = i.parentNode;) l.unshift(i);
                        for (; a[o] === l[o];) o++;
                        return o ? d(a[o], l[o]) : a[o] === V ? -1 : l[o] === V ? 1 : 0
                    }, n) : I
                }, n.matches = function(t, e) {
                    return n(t, null, null, e)
                }, n.matchesSelector = function(t, e) {
                    if ((t.ownerDocument || t) !== I && H(t), e = e.replace(be, "='$1']"), !(!S.matchesSelector || !O || F && F.test(e) || L && L.test(e))) try {
                        var i = W.call(t, e);
                        if (i || S.disconnectedMatch || t.document && 11 !== t.document.nodeType) return i
                    } catch (o) {}
                    return n(e, I, null, [t]).length > 0
                }, n.contains = function(t, e) {
                    return (t.ownerDocument || t) !== I && H(t), q(t, e)
                }, n.attr = function(t, n) {
                    (t.ownerDocument || t) !== I && H(t);
                    var i = D.attrHandle[n.toLowerCase()],
                        o = i && te.call(D.attrHandle, n.toLowerCase()) ? i(t, n, !O) : e;
                    return o === e ? S.attributes || !O ? t.getAttribute(n) : (o = t.getAttributeNode(n)) && o.specified ? o.value : null : o
                }, n.error = function(t) {
                    throw Error("Syntax error, unrecognized expression: " + t)
                }, n.uniqueSort = function(t) {
                    var e, n = [],
                        i = 0,
                        o = 0;
                    if (G = !S.detectDuplicates, M = !S.sortStable && t.slice(0), t.sort(J), G) {
                        for (; e = t[o++];) e === t[o] && (i = n.push(o));
                        for (; i--;) t.splice(n[i], 1)
                    }
                    return t
                }, E = n.getText = function(t) {
                    var e, n = "",
                        i = 0,
                        o = t.nodeType;
                    if (o) {
                        if (1 === o || 9 === o || 11 === o) {
                            if ("string" == typeof t.textContent) return t.textContent;
                            for (t = t.firstChild; t; t = t.nextSibling) n += E(t)
                        } else if (3 === o || 4 === o) return t.nodeValue
                    } else
                        for (; e = t[i]; i++) n += E(e);
                    return n
                }, D = n.selectors = {
                    cacheLength: 50,
                    createPseudo: r,
                    match: we,
                    attrHandle: {},
                    find: {},
                    relative: {
                        ">": {
                            dir: "parentNode",
                            first: !0
                        },
                        " ": {
                            dir: "parentNode"
                        },
                        "+": {
                            dir: "previousSibling",
                            first: !0
                        },
                        "~": {
                            dir: "previousSibling"
                        }
                    },
                    preFilter: {
                        ATTR: function(t) {
                            return t[1] = t[1].replace(Se, je), t[3] = (t[4] || t[5] || "").replace(Se, je), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
                        },
                        CHILD: function(t) {
                            return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || n.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && n.error(t[0]), t
                        },
                        PSEUDO: function(t) {
                            var n, i = !t[5] && t[2];
                            return we.CHILD.test(t[0]) ? null : (t[3] && t[4] !== e ? t[2] = t[4] : i && ye.test(i) && (n = m(i, !0)) && (n = i.indexOf(")", i.length - n) - i.length) && (t[0] = t[0].slice(0, n), t[2] = i.slice(0, n)), t.slice(0, 3))
                        }
                    },
                    filter: {
                        TAG: function(t) {
                            var e = t.replace(Se, je).toLowerCase();
                            return "*" === t ? function() {
                                return !0
                            } : function(t) {
                                return t.nodeName && t.nodeName.toLowerCase() === e
                            }
                        },
                        CLASS: function(t) {
                            var e = Q[t + " "];
                            return e || (e = RegExp("(^|" + le + ")" + t + "(" + le + "|$)")) && Q(t, function(t) {
                                return e.test("string" == typeof t.className && t.className || typeof t.getAttribute !== K && t.getAttribute("class") || "")
                            })
                        },
                        ATTR: function(t, e, i) {
                            return function(o) {
                                var r = n.attr(o, t);
                                return null == r ? "!=" === e : e ? (r += "", "=" === e ? r === i : "!=" === e ? r !== i : "^=" === e ? i && 0 === r.indexOf(i) : "*=" === e ? i && r.indexOf(i) > -1 : "$=" === e ? i && r.slice(-i.length) === i : "~=" === e ? (" " + r + " ").indexOf(i) > -1 : "|=" === e ? r === i || r.slice(0, i.length + 1) === i + "-" : !1) : !0
                            }
                        },
                        CHILD: function(t, e, n, i, o) {
                            var r = "nth" !== t.slice(0, 3),
                                s = "last" !== t.slice(-4),
                                a = "of-type" === e;
                            return 1 === i && 0 === o ? function(t) {
                                return !!t.parentNode
                            } : function(e, n, l) {
                                var c, u, d, h, p, f, m = r !== s ? "nextSibling" : "previousSibling",
                                    g = e.parentNode,
                                    v = a && e.nodeName.toLowerCase(),
                                    b = !l && !a;
                                if (g) {
                                    if (r) {
                                        for (; m;) {
                                            for (d = e; d = d[m];)
                                                if (a ? d.nodeName.toLowerCase() === v : 1 === d.nodeType) return !1;
                                            f = m = "only" === t && !f && "nextSibling"
                                        }
                                        return !0
                                    }
                                    if (f = [s ? g.firstChild : g.lastChild], s && b) {
                                        for (u = g[R] || (g[R] = {}), c = u[t] || [], p = c[0] === B && c[1], h = c[0] === B && c[2], d = p && g.childNodes[p]; d = ++p && d && d[m] || (h = p = 0) || f.pop();)
                                            if (1 === d.nodeType && ++h && d === e) {
                                                u[t] = [B, p, h];
                                                break
                                            }
                                    } else if (b && (c = (e[R] || (e[R] = {}))[t]) && c[0] === B) h = c[1];
                                    else
                                        for (;
                                            (d = ++p && d && d[m] || (h = p = 0) || f.pop()) && ((a ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++h || (b && ((d[R] || (d[R] = {}))[t] = [B, h]), d !== e)););
                                    return h -= o, h === i || 0 === h % i && h / i >= 0
                                }
                            }
                        },
                        PSEUDO: function(t, e) {
                            var i, o = D.pseudos[t] || D.setFilters[t.toLowerCase()] || n.error("unsupported pseudo: " + t);
                            return o[R] ? o(e) : o.length > 1 ? (i = [t, t, "", e], D.setFilters.hasOwnProperty(t.toLowerCase()) ? r(function(t, n) {
                                for (var i, r = o(t, e), s = r.length; s--;) i = se.call(t, r[s]), t[i] = !(n[i] = r[s])
                            }) : function(t) {
                                return o(t, 0, i)
                            }) : o
                        }
                    },
                    pseudos: {
                        not: r(function(t) {
                            var e = [],
                                n = [],
                                i = A(t.replace(fe, "$1"));
                            return i[R] ? r(function(t, e, n, o) {
                                for (var r, s = i(t, null, o, []), a = t.length; a--;)(r = s[a]) && (t[a] = !(e[a] = r))
                            }) : function(t, o, r) {
                                return e[0] = t, i(e, null, r, n), !n.pop()
                            }
                        }),
                        has: r(function(t) {
                            return function(e) {
                                return n(t, e).length > 0
                            }
                        }),
                        contains: r(function(t) {
                            return function(e) {
                                return (e.textContent || e.innerText || E(e)).indexOf(t) > -1
                            }
                        }),
                        lang: r(function(t) {
                            return _e.test(t || "") || n.error("unsupported lang: " + t), t = t.replace(Se, je).toLowerCase(),
                                function(e) {
                                    var n;
                                    do
                                        if (n = O ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return n = n.toLowerCase(), n === t || 0 === n.indexOf(t + "-");
                                    while ((e = e.parentNode) && 1 === e.nodeType);
                                    return !1
                                }
                        }),
                        target: function(e) {
                            var n = t.location && t.location.hash;
                            return n && n.slice(1) === e.id
                        },
                        root: function(t) {
                            return t === z
                        },
                        focus: function(t) {
                            return t === I.activeElement && (!I.hasFocus || I.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                        },
                        enabled: function(t) {
                            return t.disabled === !1
                        },
                        disabled: function(t) {
                            return t.disabled === !0
                        },
                        checked: function(t) {
                            var e = t.nodeName.toLowerCase();
                            return "input" === e && !!t.checked || "option" === e && !!t.selected
                        },
                        selected: function(t) {
                            return t.parentNode && t.parentNode.selectedIndex, t.selected === !0
                        },
                        empty: function(t) {
                            for (t = t.firstChild; t; t = t.nextSibling)
                                if (t.nodeName > "@" || 3 === t.nodeType || 4 === t.nodeType) return !1;
                            return !0
                        },
                        parent: function(t) {
                            return !D.pseudos.empty(t)
                        },
                        header: function(t) {
                            return Te.test(t.nodeName)
                        },
                        input: function(t) {
                            return ke.test(t.nodeName)
                        },
                        button: function(t) {
                            var e = t.nodeName.toLowerCase();
                            return "input" === e && "button" === t.type || "button" === e
                        },
                        text: function(t) {
                            var e;
                            return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || e.toLowerCase() === t.type)
                        },
                        first: f(function() {
                            return [0]
                        }),
                        last: f(function(t, e) {
                            return [e - 1]
                        }),
                        eq: f(function(t, e, n) {
                            return [0 > n ? n + e : n]
                        }),
                        even: f(function(t, e) {
                            for (var n = 0; e > n; n += 2) t.push(n);
                            return t
                        }),
                        odd: f(function(t, e) {
                            for (var n = 1; e > n; n += 2) t.push(n);
                            return t
                        }),
                        lt: f(function(t, e, n) {
                            for (var i = 0 > n ? n + e : n; --i >= 0;) t.push(i);
                            return t
                        }),
                        gt: f(function(t, e, n) {
                            for (var i = 0 > n ? n + e : n; e > ++i;) t.push(i);
                            return t
                        })
                    }
                };
                for ($ in {
                        radio: !0,
                        checkbox: !0,
                        file: !0,
                        password: !0,
                        image: !0
                    }) D.pseudos[$] = h($);
                for ($ in {
                        submit: !0,
                        reset: !0
                    }) D.pseudos[$] = p($);
                A = n.compile = function(t, e) {
                    var n, i = [],
                        o = [],
                        r = X[t + " "];
                    if (!r) {
                        for (e || (e = m(t)), n = e.length; n--;) r = w(e[n]), r[R] ? i.push(r) : o.push(r);
                        r = X(t, x(o, i))
                    }
                    return r
                }, D.pseudos.nth = D.pseudos.eq, T.prototype = D.filters = D.pseudos, D.setFilters = new T, S.sortStable = R.split("").sort(J).join("") === R, H(), [0, 0].sort(J), S.detectDuplicates = G, ue.find = n, ue.expr = n.selectors, ue.expr[":"] = ue.expr.pseudos, ue.unique = n.uniqueSort, ue.text = n.getText, ue.isXMLDoc = n.isXML, ue.contains = n.contains
            }(t);
        var Te = {};
        ue.Callbacks = function(t) {
            t = "string" == typeof t ? Te[t] || i(t) : ue.extend({}, t);
            var n, o, r, s, a, l, c = [],
                u = !t.once && [],
                d = function(e) {
                    for (o = t.memory && e, r = !0, a = l || 0, l = 0, s = c.length, n = !0; c && s > a; a++)
                        if (c[a].apply(e[0], e[1]) === !1 && t.stopOnFalse) {
                            o = !1;
                            break
                        }
                    n = !1, c && (u ? u.length && d(u.shift()) : o ? c = [] : h.disable())
                },
                h = {
                    add: function() {
                        if (c) {
                            var e = c.length;
                            ! function i(e) {
                                ue.each(e, function(e, n) {
                                    var o = ue.type(n);
                                    "function" === o ? t.unique && h.has(n) || c.push(n) : n && n.length && "string" !== o && i(n)
                                })
                            }(arguments), n ? s = c.length : o && (l = e, d(o))
                        }
                        return this
                    },
                    remove: function() {
                        return c && ue.each(arguments, function(t, e) {
                            for (var i;
                                (i = ue.inArray(e, c, i)) > -1;) c.splice(i, 1), n && (s >= i && s--, a >= i && a--)
                        }), this
                    },
                    has: function(t) {
                        return t ? ue.inArray(t, c) > -1 : !(!c || !c.length)
                    },
                    empty: function() {
                        return c = [], s = 0, this
                    },
                    disable: function() {
                        return c = u = o = e, this
                    },
                    disabled: function() {
                        return !c
                    },
                    lock: function() {
                        return u = e, o || h.disable(), this
                    },
                    locked: function() {
                        return !u
                    },
                    fireWith: function(t, e) {
                        return e = e || [], e = [t, e.slice ? e.slice() : e], !c || r && !u || (n ? u.push(e) : d(e)), this
                    },
                    fire: function() {
                        return h.fireWith(this, arguments), this
                    },
                    fired: function() {
                        return !!r
                    }
                };
            return h
        }, ue.extend({
            Deferred: function(t) {
                var e = [
                        ["resolve", "done", ue.Callbacks("once memory"), "resolved"],
                        ["reject", "fail", ue.Callbacks("once memory"), "rejected"],
                        ["notify", "progress", ue.Callbacks("memory")]
                    ],
                    n = "pending",
                    i = {
                        state: function() {
                            return n
                        },
                        always: function() {
                            return o.done(arguments).fail(arguments), this
                        },
                        then: function() {
                            var t = arguments;
                            return ue.Deferred(function(n) {
                                ue.each(e, function(e, r) {
                                    var s = r[0],
                                        a = ue.isFunction(t[e]) && t[e];
                                    o[r[1]](function() {
                                        var t = a && a.apply(this, arguments);
                                        t && ue.isFunction(t.promise) ? t.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[s + "With"](this === i ? n.promise() : this, a ? [t] : arguments)
                                    })
                                }), t = null
                            }).promise()
                        },
                        promise: function(t) {
                            return null != t ? ue.extend(t, i) : i
                        }
                    },
                    o = {};
                return i.pipe = i.then, ue.each(e, function(t, r) {
                    var s = r[2],
                        a = r[3];
                    i[r[1]] = s.add, a && s.add(function() {
                        n = a
                    }, e[1 ^ t][2].disable, e[2][2].lock), o[r[0]] = function() {
                        return o[r[0] + "With"](this === o ? i : this, arguments), this
                    }, o[r[0] + "With"] = s.fireWith
                }), i.promise(o), t && t.call(o, o), o
            },
            when: function(t) {
                var e, n, i, o = 0,
                    r = re.call(arguments),
                    s = r.length,
                    a = 1 !== s || t && ue.isFunction(t.promise) ? s : 0,
                    l = 1 === a ? t : ue.Deferred(),
                    c = function(t, n, i) {
                        return function(o) {
                            n[t] = this, i[t] = arguments.length > 1 ? re.call(arguments) : o, i === e ? l.notifyWith(n, i) : --a || l.resolveWith(n, i)
                        }
                    };
                if (s > 1)
                    for (e = Array(s), n = Array(s), i = Array(s); s > o; o++) r[o] && ue.isFunction(r[o].promise) ? r[o].promise().done(c(o, i, r)).fail(l.reject).progress(c(o, n, e)) : --a;
                return a || l.resolveWith(i, r), l.promise()
            }
        }), ue.support = function(e) {
            var n, i, o, r, s, a, l, c, u, d = G.createElement("div");
            if (d.setAttribute("className", "t"), d.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = d.getElementsByTagName("*") || [], i = d.getElementsByTagName("a")[0], !i || !i.style || !n.length) return e;
            r = G.createElement("select"), a = r.appendChild(G.createElement("option")), o = d.getElementsByTagName("input")[0], i.style.cssText = "top:1px;float:left;opacity:.5", e.getSetAttribute = "t" !== d.className, e.leadingWhitespace = 3 === d.firstChild.nodeType, e.tbody = !d.getElementsByTagName("tbody").length, e.htmlSerialize = !!d.getElementsByTagName("link").length, e.style = /top/.test(i.getAttribute("style")), e.hrefNormalized = "/a" === i.getAttribute("href"), e.opacity = /^0.5/.test(i.style.opacity), e.cssFloat = !!i.style.cssFloat, e.checkOn = !!o.value, e.optSelected = a.selected, e.enctype = !!G.createElement("form").enctype, e.html5Clone = "<:nav></:nav>" !== G.createElement("nav").cloneNode(!0).outerHTML, e.inlineBlockNeedsLayout = !1, e.shrinkWrapBlocks = !1, e.pixelPosition = !1, e.deleteExpando = !0, e.noCloneEvent = !0, e.reliableMarginRight = !0, e.boxSizingReliable = !0, o.checked = !0, e.noCloneChecked = o.cloneNode(!0).checked, r.disabled = !0, e.optDisabled = !a.disabled;
            try {
                delete d.test
            } catch (h) {
                e.deleteExpando = !1
            }
            o = G.createElement("input"), o.setAttribute("value", ""), e.input = "" === o.getAttribute("value"), o.value = "t", o.setAttribute("type", "radio"), e.radioValue = "t" === o.value, o.setAttribute("checked", "t"), o.setAttribute("name", "t"), s = G.createDocumentFragment(), s.appendChild(o), e.appendChecked = o.checked, e.checkClone = s.cloneNode(!0).cloneNode(!0).lastChild.checked, d.attachEvent && (d.attachEvent("onclick", function() {
                e.noCloneEvent = !1
            }), d.cloneNode(!0).click());
            for (u in {
                    submit: !0,
                    change: !0,
                    focusin: !0
                }) d.setAttribute(l = "on" + u, "t"), e[u + "Bubbles"] = l in t || d.attributes[l].expando === !1;
            d.style.backgroundClip = "content-box", d.cloneNode(!0).style.backgroundClip = "", e.clearCloneStyle = "content-box" === d.style.backgroundClip;
            for (u in ue(e)) break;
            return e.ownLast = "0" !== u, ue(function() {
                var n, i, o, r = "padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;",
                    s = G.getElementsByTagName("body")[0];
                s && (n = G.createElement("div"), n.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", s.appendChild(n).appendChild(d), d.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", o = d.getElementsByTagName("td"), o[0].style.cssText = "padding:0;margin:0;border:0;display:none", c = 0 === o[0].offsetHeight, o[0].style.display = "", o[1].style.display = "none", e.reliableHiddenOffsets = c && 0 === o[0].offsetHeight, d.innerHTML = "", d.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;", ue.swap(s, null != s.style.zoom ? {
                    zoom: 1
                } : {}, function() {
                    e.boxSizing = 4 === d.offsetWidth
                }), t.getComputedStyle && (e.pixelPosition = "1%" !== (t.getComputedStyle(d, null) || {}).top, e.boxSizingReliable = "4px" === (t.getComputedStyle(d, null) || {
                    width: "4px"
                }).width, i = d.appendChild(G.createElement("div")), i.style.cssText = d.style.cssText = r, i.style.marginRight = i.style.width = "0", d.style.width = "1px", e.reliableMarginRight = !parseFloat((t.getComputedStyle(i, null) || {}).marginRight)), typeof d.style.zoom !== U && (d.innerHTML = "", d.style.cssText = r + "width:1px;padding:1px;display:inline;zoom:1", e.inlineBlockNeedsLayout = 3 === d.offsetWidth, d.style.display = "block", d.innerHTML = "<div></div>", d.firstChild.style.width = "5px", e.shrinkWrapBlocks = 3 !== d.offsetWidth, e.inlineBlockNeedsLayout && (s.style.zoom = 1)), s.removeChild(n), n = d = o = i = null)
            }), n = r = s = a = i = o = null, e
        }({});
        var $e = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
            Se = /([A-Z])/g;
        ue.extend({
            cache: {},
            noData: {
                applet: !0,
                embed: !0,
                object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
            },
            hasData: function(t) {
                return t = t.nodeType ? ue.cache[t[ue.expando]] : t[ue.expando], !!t && !a(t)
            },
            data: function(t, e, n) {
                return o(t, e, n)
            },
            removeData: function(t, e) {
                return r(t, e)
            },
            _data: function(t, e, n) {
                return o(t, e, n, !0)
            },
            _removeData: function(t, e) {
                return r(t, e, !0)
            },
            acceptData: function(t) {
                if (t.nodeType && 1 !== t.nodeType && 9 !== t.nodeType) return !1;
                var e = t.nodeName && ue.noData[t.nodeName.toLowerCase()];
                return !e || e !== !0 && t.getAttribute("classid") === e
            }
        }), ue.fn.extend({
            data: function(t, n) {
                var i, o, r = null,
                    a = 0,
                    l = this[0];
                if (t === e) {
                    if (this.length && (r = ue.data(l), 1 === l.nodeType && !ue._data(l, "parsedAttrs"))) {
                        for (i = l.attributes; i.length > a; a++) o = i[a].name, 0 === o.indexOf("data-") && (o = ue.camelCase(o.slice(5)), s(l, o, r[o]));
                        ue._data(l, "parsedAttrs", !0)
                    }
                    return r
                }
                return "object" == typeof t ? this.each(function() {
                    ue.data(this, t)
                }) : arguments.length > 1 ? this.each(function() {
                    ue.data(this, t, n)
                }) : l ? s(l, t, ue.data(l, t)) : null
            },
            removeData: function(t) {
                return this.each(function() {
                    ue.removeData(this, t)
                })
            }
        }), ue.extend({
            queue: function(t, n, i) {
                var o;
                return t ? (n = (n || "fx") + "queue", o = ue._data(t, n), i && (!o || ue.isArray(i) ? o = ue._data(t, n, ue.makeArray(i)) : o.push(i)), o || []) : e
            },
            dequeue: function(t, e) {
                e = e || "fx";
                var n = ue.queue(t, e),
                    i = n.length,
                    o = n.shift(),
                    r = ue._queueHooks(t, e),
                    s = function() {
                        ue.dequeue(t, e)
                    };
                "inprogress" === o && (o = n.shift(), i--), o && ("fx" === e && n.unshift("inprogress"), delete r.stop, o.call(t, s, r)), !i && r && r.empty.fire()
            },
            _queueHooks: function(t, e) {
                var n = e + "queueHooks";
                return ue._data(t, n) || ue._data(t, n, {
                    empty: ue.Callbacks("once memory").add(function() {
                        ue._removeData(t, e + "queue"), ue._removeData(t, n)
                    })
                })
            }
        }), ue.fn.extend({
            queue: function(t, n) {
                var i = 2;
                return "string" != typeof t && (n = t, t = "fx", i--), i > arguments.length ? ue.queue(this[0], t) : n === e ? this : this.each(function() {
                    var e = ue.queue(this, t, n);
                    ue._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && ue.dequeue(this, t)
                })
            },
            dequeue: function(t) {
                return this.each(function() {
                    ue.dequeue(this, t)
                })
            },
            delay: function(t, e) {
                return t = ue.fx ? ue.fx.speeds[t] || t : t, e = e || "fx", this.queue(e, function(e, n) {
                    var i = setTimeout(e, t);
                    n.stop = function() {
                        clearTimeout(i)
                    }
                })
            },
            clearQueue: function(t) {
                return this.queue(t || "fx", [])
            },
            promise: function(t, n) {
                var i, o = 1,
                    r = ue.Deferred(),
                    s = this,
                    a = this.length,
                    l = function() {
                        --o || r.resolveWith(s, [s])
                    };
                for ("string" != typeof t && (n = t, t = e), t = t || "fx"; a--;) i = ue._data(s[a], t + "queueHooks"), i && i.empty && (o++, i.empty.add(l));
                return l(), r.promise(n)
            }
        });
        var je, De, Ee = /[\t\r\n\f]/g,
            Pe = /\r/g,
            Ae = /^(?:input|select|textarea|button|object)$/i,
            Ne = /^(?:a|area)$/i,
            Me = /^(?:checked|selected)$/i,
            He = ue.support.getSetAttribute,
            Ie = ue.support.input;
        ue.fn.extend({
            attr: function(t, e) {
                return ue.access(this, ue.attr, t, e, arguments.length > 1)
            },
            removeAttr: function(t) {
                return this.each(function() {
                    ue.removeAttr(this, t)
                })
            },
            prop: function(t, e) {
                return ue.access(this, ue.prop, t, e, arguments.length > 1)
            },
            removeProp: function(t) {
                return t = ue.propFix[t] || t, this.each(function() {
                    try {
                        this[t] = e, delete this[t]
                    } catch (n) {}
                })
            },
            addClass: function(t) {
                var e, n, i, o, r, s = 0,
                    a = this.length,
                    l = "string" == typeof t && t;
                if (ue.isFunction(t)) return this.each(function(e) {
                    ue(this).addClass(t.call(this, e, this.className))
                });
                if (l)
                    for (e = (t || "").match(he) || []; a > s; s++)
                        if (n = this[s], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Ee, " ") : " ")) {
                            for (r = 0; o = e[r++];) 0 > i.indexOf(" " + o + " ") && (i += o + " ");
                            n.className = ue.trim(i)
                        }
                return this
            },
            removeClass: function(t) {
                var e, n, i, o, r, s = 0,
                    a = this.length,
                    l = 0 === arguments.length || "string" == typeof t && t;
                if (ue.isFunction(t)) return this.each(function(e) {
                    ue(this).removeClass(t.call(this, e, this.className))
                });
                if (l)
                    for (e = (t || "").match(he) || []; a > s; s++)
                        if (n = this[s], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Ee, " ") : "")) {
                            for (r = 0; o = e[r++];)
                                for (; i.indexOf(" " + o + " ") >= 0;) i = i.replace(" " + o + " ", " ");
                            n.className = t ? ue.trim(i) : ""
                        }
                return this
            },
            toggleClass: function(t, e) {
                var n = typeof t,
                    i = "boolean" == typeof e;
                return this.each(ue.isFunction(t) ? function(n) {
                    ue(this).toggleClass(t.call(this, n, this.className, e), e)
                } : function() {
                    if ("string" === n)
                        for (var o, r = 0, s = ue(this), a = e, l = t.match(he) || []; o = l[r++];) a = i ? a : !s.hasClass(o), s[a ? "addClass" : "removeClass"](o);
                    else(n === U || "boolean" === n) && (this.className && ue._data(this, "__className__", this.className), this.className = this.className || t === !1 ? "" : ue._data(this, "__className__") || "")
                })
            },
            hasClass: function(t) {
                for (var e = " " + t + " ", n = 0, i = this.length; i > n; n++)
                    if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(Ee, " ").indexOf(e) >= 0) return !0;
                return !1
            },
            val: function(t) {
                var n, i, o, r = this[0];
                return arguments.length ? (o = ue.isFunction(t), this.each(function(n) {
                    var r;
                    1 === this.nodeType && (r = o ? t.call(this, n, ue(this).val()) : t, null == r ? r = "" : "number" == typeof r ? r += "" : ue.isArray(r) && (r = ue.map(r, function(t) {
                        return null == t ? "" : t + ""
                    })), i = ue.valHooks[this.type] || ue.valHooks[this.nodeName.toLowerCase()], i && "set" in i && i.set(this, r, "value") !== e || (this.value = r))
                })) : r ? (i = ue.valHooks[r.type] || ue.valHooks[r.nodeName.toLowerCase()], i && "get" in i && (n = i.get(r, "value")) !== e ? n : (n = r.value, "string" == typeof n ? n.replace(Pe, "") : null == n ? "" : n)) : void 0
            }
        }), ue.extend({
            valHooks: {
                option: {
                    get: function(t) {
                        var e = ue.find.attr(t, "value");
                        return null != e ? e : t.text
                    }
                },
                select: {
                    get: function(t) {
                        for (var e, n, i = t.options, o = t.selectedIndex, r = "select-one" === t.type || 0 > o, s = r ? null : [], a = r ? o + 1 : i.length, l = 0 > o ? a : r ? o : 0; a > l; l++)
                            if (n = i[l], !(!n.selected && l !== o || (ue.support.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && ue.nodeName(n.parentNode, "optgroup"))) {
                                if (e = ue(n).val(), r) return e;
                                s.push(e)
                            }
                        return s
                    },
                    set: function(t, e) {
                        for (var n, i, o = t.options, r = ue.makeArray(e), s = o.length; s--;) i = o[s], (i.selected = ue.inArray(ue(i).val(), r) >= 0) && (n = !0);
                        return n || (t.selectedIndex = -1), r
                    }
                }
            },
            attr: function(t, n, i) {
                var o, r, s = t.nodeType;
                return t && 3 !== s && 8 !== s && 2 !== s ? typeof t.getAttribute === U ? ue.prop(t, n, i) : (1 === s && ue.isXMLDoc(t) || (n = n.toLowerCase(), o = ue.attrHooks[n] || (ue.expr.match.bool.test(n) ? De : je)), i === e ? o && "get" in o && null !== (r = o.get(t, n)) ? r : (r = ue.find.attr(t, n), null == r ? e : r) : null !== i ? o && "set" in o && (r = o.set(t, i, n)) !== e ? r : (t.setAttribute(n, i + ""), i) : (ue.removeAttr(t, n), e)) : void 0
            },
            removeAttr: function(t, e) {
                var n, i, o = 0,
                    r = e && e.match(he);
                if (r && 1 === t.nodeType)
                    for (; n = r[o++];) i = ue.propFix[n] || n, ue.expr.match.bool.test(n) ? Ie && He || !Me.test(n) ? t[i] = !1 : t[ue.camelCase("default-" + n)] = t[i] = !1 : ue.attr(t, n, ""), t.removeAttribute(He ? n : i)
            },
            attrHooks: {
                type: {
                    set: function(t, e) {
                        if (!ue.support.radioValue && "radio" === e && ue.nodeName(t, "input")) {
                            var n = t.value;
                            return t.setAttribute("type", e), n && (t.value = n), e
                        }
                    }
                }
            },
            propFix: {
                "for": "htmlFor",
                "class": "className"
            },
            prop: function(t, n, i) {
                var o, r, s, a = t.nodeType;
                return t && 3 !== a && 8 !== a && 2 !== a ? (s = 1 !== a || !ue.isXMLDoc(t), s && (n = ue.propFix[n] || n, r = ue.propHooks[n]), i !== e ? r && "set" in r && (o = r.set(t, i, n)) !== e ? o : t[n] = i : r && "get" in r && null !== (o = r.get(t, n)) ? o : t[n]) : void 0
            },
            propHooks: {
                tabIndex: {
                    get: function(t) {
                        var e = ue.find.attr(t, "tabindex");
                        return e ? parseInt(e, 10) : Ae.test(t.nodeName) || Ne.test(t.nodeName) && t.href ? 0 : -1
                    }
                }
            }
        }), De = {
            set: function(t, e, n) {
                return e === !1 ? ue.removeAttr(t, n) : Ie && He || !Me.test(n) ? t.setAttribute(!He && ue.propFix[n] || n, n) : t[ue.camelCase("default-" + n)] = t[n] = !0, n
            }
        }, ue.each(ue.expr.match.bool.source.match(/\w+/g), function(t, n) {
            var i = ue.expr.attrHandle[n] || ue.find.attr;
            ue.expr.attrHandle[n] = Ie && He || !Me.test(n) ? function(t, n, o) {
                var r = ue.expr.attrHandle[n],
                    s = o ? e : (ue.expr.attrHandle[n] = e) != i(t, n, o) ? n.toLowerCase() : null;
                return ue.expr.attrHandle[n] = r, s
            } : function(t, n, i) {
                return i ? e : t[ue.camelCase("default-" + n)] ? n.toLowerCase() : null
            }
        }), Ie && He || (ue.attrHooks.value = {
            set: function(t, n, i) {
                return ue.nodeName(t, "input") ? (t.defaultValue = n, e) : je && je.set(t, n, i)
            }
        }), He || (je = {
            set: function(t, n, i) {
                var o = t.getAttributeNode(i);
                return o || t.setAttributeNode(o = t.ownerDocument.createAttribute(i)), o.value = n += "", "value" === i || n === t.getAttribute(i) ? n : e
            }
        }, ue.expr.attrHandle.id = ue.expr.attrHandle.name = ue.expr.attrHandle.coords = function(t, n, i) {
            var o;
            return i ? e : (o = t.getAttributeNode(n)) && "" !== o.value ? o.value : null
        }, ue.valHooks.button = {
            get: function(t, n) {
                var i = t.getAttributeNode(n);
                return i && i.specified ? i.value : e
            },
            set: je.set
        }, ue.attrHooks.contenteditable = {
            set: function(t, e, n) {
                je.set(t, "" === e ? !1 : e, n)
            }
        }, ue.each(["width", "height"], function(t, n) {
            ue.attrHooks[n] = {
                set: function(t, i) {
                    return "" === i ? (t.setAttribute(n, "auto"), i) : e
                }
            }
        })), ue.support.hrefNormalized || ue.each(["href", "src"], function(t, e) {
            ue.propHooks[e] = {
                get: function(t) {
                    return t.getAttribute(e, 4)
                }
            }
        }), ue.support.style || (ue.attrHooks.style = {
            get: function(t) {
                return t.style.cssText || e
            },
            set: function(t, e) {
                return t.style.cssText = e + ""
            }
        }), ue.support.optSelected || (ue.propHooks.selected = {
            get: function(t) {
                var e = t.parentNode;
                return e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex), null
            }
        }), ue.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
            ue.propFix[this.toLowerCase()] = this
        }), ue.support.enctype || (ue.propFix.enctype = "encoding"), ue.each(["radio", "checkbox"], function() {
            ue.valHooks[this] = {
                set: function(t, n) {
                    return ue.isArray(n) ? t.checked = ue.inArray(ue(t).val(), n) >= 0 : e
                }
            }, ue.support.checkOn || (ue.valHooks[this].get = function(t) {
                return null === t.getAttribute("value") ? "on" : t.value
            })
        });
        var ze = /^(?:input|select|textarea)$/i,
            Oe = /^key/,
            Le = /^(?:mouse|contextmenu)|click/,
            Fe = /^(?:focusinfocus|focusoutblur)$/,
            We = /^([^.]*)(?:\.(.+)|)$/;
        ue.event = {
            global: {},
            add: function(t, n, i, o, r) {
                var s, a, l, c, u, d, h, p, f, m, g, v = ue._data(t);
                if (v) {
                    for (i.handler && (c = i, i = c.handler, r = c.selector), i.guid || (i.guid = ue.guid++), (a = v.events) || (a = v.events = {}), (d = v.handle) || (d = v.handle = function(t) {
                            return typeof ue === U || t && ue.event.triggered === t.type ? e : ue.event.dispatch.apply(d.elem, arguments)
                        }, d.elem = t), n = (n || "").match(he) || [""], l = n.length; l--;) s = We.exec(n[l]) || [], f = g = s[1], m = (s[2] || "").split(".").sort(), f && (u = ue.event.special[f] || {}, f = (r ? u.delegateType : u.bindType) || f, u = ue.event.special[f] || {}, h = ue.extend({
                        type: f,
                        origType: g,
                        data: o,
                        handler: i,
                        guid: i.guid,
                        selector: r,
                        needsContext: r && ue.expr.match.needsContext.test(r),
                        namespace: m.join(".")
                    }, c), (p = a[f]) || (p = a[f] = [], p.delegateCount = 0, u.setup && u.setup.call(t, o, m, d) !== !1 || (t.addEventListener ? t.addEventListener(f, d, !1) : t.attachEvent && t.attachEvent("on" + f, d))), u.add && (u.add.call(t, h), h.handler.guid || (h.handler.guid = i.guid)), r ? p.splice(p.delegateCount++, 0, h) : p.push(h), ue.event.global[f] = !0);
                    t = null
                }
            },
            remove: function(t, e, n, i, o) {
                var r, s, a, l, c, u, d, h, p, f, m, g = ue.hasData(t) && ue._data(t);
                if (g && (u = g.events)) {
                    for (e = (e || "").match(he) || [""], c = e.length; c--;)
                        if (a = We.exec(e[c]) || [], p = m = a[1], f = (a[2] || "").split(".").sort(), p) {
                            for (d = ue.event.special[p] || {}, p = (i ? d.delegateType : d.bindType) || p, h = u[p] || [], a = a[2] && RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = r = h.length; r--;) s = h[r], !o && m !== s.origType || n && n.guid !== s.guid || a && !a.test(s.namespace) || i && i !== s.selector && ("**" !== i || !s.selector) || (h.splice(r, 1), s.selector && h.delegateCount--, d.remove && d.remove.call(t, s));
                            l && !h.length && (d.teardown && d.teardown.call(t, f, g.handle) !== !1 || ue.removeEvent(t, p, g.handle), delete u[p])
                        } else
                            for (p in u) ue.event.remove(t, p + e[c], n, i, !0);
                    ue.isEmptyObject(u) && (delete g.handle, ue._removeData(t, "events"))
                }
            },
            trigger: function(n, i, o, r) {
                var s, a, l, c, u, d, h, p = [o || G],
                    f = le.call(n, "type") ? n.type : n,
                    m = le.call(n, "namespace") ? n.namespace.split(".") : [];
                if (l = d = o = o || G, 3 !== o.nodeType && 8 !== o.nodeType && !Fe.test(f + ue.event.triggered) && (f.indexOf(".") >= 0 && (m = f.split("."), f = m.shift(), m.sort()), a = 0 > f.indexOf(":") && "on" + f, n = n[ue.expando] ? n : new ue.Event(f, "object" == typeof n && n), n.isTrigger = r ? 2 : 3, n.namespace = m.join("."), n.namespace_re = n.namespace ? RegExp("(^|\\.)" + m.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, n.result = e, n.target || (n.target = o), i = null == i ? [n] : ue.makeArray(i, [n]), u = ue.event.special[f] || {}, r || !u.trigger || u.trigger.apply(o, i) !== !1)) {
                    if (!r && !u.noBubble && !ue.isWindow(o)) {
                        for (c = u.delegateType || f, Fe.test(c + f) || (l = l.parentNode); l; l = l.parentNode) p.push(l), d = l;
                        d === (o.ownerDocument || G) && p.push(d.defaultView || d.parentWindow || t)
                    }
                    for (h = 0;
                        (l = p[h++]) && !n.isPropagationStopped();) n.type = h > 1 ? c : u.bindType || f, s = (ue._data(l, "events") || {})[n.type] && ue._data(l, "handle"), s && s.apply(l, i), s = a && l[a], s && ue.acceptData(l) && s.apply && s.apply(l, i) === !1 && n.preventDefault();
                    if (n.type = f, !r && !n.isDefaultPrevented() && (!u._default || u._default.apply(p.pop(), i) === !1) && ue.acceptData(o) && a && o[f] && !ue.isWindow(o)) {
                        d = o[a], d && (o[a] = null), ue.event.triggered = f;
                        try {
                            o[f]()
                        } catch (g) {}
                        ue.event.triggered = e, d && (o[a] = d)
                    }
                    return n.result
                }
            },
            dispatch: function(t) {
                t = ue.event.fix(t);
                var n, i, o, r, s, a = [],
                    l = re.call(arguments),
                    c = (ue._data(this, "events") || {})[t.type] || [],
                    u = ue.event.special[t.type] || {};
                if (l[0] = t, t.delegateTarget = this, !u.preDispatch || u.preDispatch.call(this, t) !== !1) {
                    for (a = ue.event.handlers.call(this, t, c), n = 0;
                        (r = a[n++]) && !t.isPropagationStopped();)
                        for (t.currentTarget = r.elem, s = 0;
                            (o = r.handlers[s++]) && !t.isImmediatePropagationStopped();)(!t.namespace_re || t.namespace_re.test(o.namespace)) && (t.handleObj = o, t.data = o.data, i = ((ue.event.special[o.origType] || {}).handle || o.handler).apply(r.elem, l), i !== e && (t.result = i) === !1 && (t.preventDefault(), t.stopPropagation()));
                    return u.postDispatch && u.postDispatch.call(this, t), t.result
                }
            },
            handlers: function(t, n) {
                var i, o, r, s, a = [],
                    l = n.delegateCount,
                    c = t.target;
                if (l && c.nodeType && (!t.button || "click" !== t.type))
                    for (; c != this; c = c.parentNode || this)
                        if (1 === c.nodeType && (c.disabled !== !0 || "click" !== t.type)) {
                            for (r = [], s = 0; l > s; s++) o = n[s], i = o.selector + " ", r[i] === e && (r[i] = o.needsContext ? ue(i, this).index(c) >= 0 : ue.find(i, this, null, [c]).length), r[i] && r.push(o);
                            r.length && a.push({
                                elem: c,
                                handlers: r
                            })
                        }
                return n.length > l && a.push({
                    elem: this,
                    handlers: n.slice(l)
                }), a
            },
            fix: function(t) {
                if (t[ue.expando]) return t;
                var e, n, i, o = t.type,
                    r = t,
                    s = this.fixHooks[o];
                for (s || (this.fixHooks[o] = s = Le.test(o) ? this.mouseHooks : Oe.test(o) ? this.keyHooks : {}), i = s.props ? this.props.concat(s.props) : this.props, t = new ue.Event(r), e = i.length; e--;) n = i[e], t[n] = r[n];
                return t.target || (t.target = r.srcElement || G), 3 === t.target.nodeType && (t.target = t.target.parentNode), t.metaKey = !!t.metaKey, s.filter ? s.filter(t, r) : t
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function(t, e) {
                    return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function(t, n) {
                    var i, o, r, s = n.button,
                        a = n.fromElement;
                    return null == t.pageX && null != n.clientX && (o = t.target.ownerDocument || G, r = o.documentElement, i = o.body, t.pageX = n.clientX + (r && r.scrollLeft || i && i.scrollLeft || 0) - (r && r.clientLeft || i && i.clientLeft || 0), t.pageY = n.clientY + (r && r.scrollTop || i && i.scrollTop || 0) - (r && r.clientTop || i && i.clientTop || 0)), !t.relatedTarget && a && (t.relatedTarget = a === t.target ? n.toElement : a), t.which || s === e || (t.which = 1 & s ? 1 : 2 & s ? 3 : 4 & s ? 2 : 0), t
                }
            },
            special: {
                load: {
                    noBubble: !0
                },
                focus: {
                    trigger: function() {
                        if (this !== u() && this.focus) try {
                            return this.focus(), !1
                        } catch (t) {}
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        return this === u() && this.blur ? (this.blur(), !1) : e
                    },
                    delegateType: "focusout"
                },
                click: {
                    trigger: function() {
                        return ue.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : e
                    },
                    _default: function(t) {
                        return ue.nodeName(t.target, "a")
                    }
                },
                beforeunload: {
                    postDispatch: function(t) {
                        t.result !== e && (t.originalEvent.returnValue = t.result)
                    }
                }
            },
            simulate: function(t, e, n, i) {
                var o = ue.extend(new ue.Event, n, {
                    type: t,
                    isSimulated: !0,
                    originalEvent: {}
                });
                i ? ue.event.trigger(o, null, e) : ue.event.dispatch.call(e, o), o.isDefaultPrevented() && n.preventDefault()
            }
        }, ue.removeEvent = G.removeEventListener ? function(t, e, n) {
            t.removeEventListener && t.removeEventListener(e, n, !1)
        } : function(t, e, n) {
            var i = "on" + e;
            t.detachEvent && (typeof t[i] === U && (t[i] = null), t.detachEvent(i, n))
        }, ue.Event = function(t, n) {
            return this instanceof ue.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || t.returnValue === !1 || t.getPreventDefault && t.getPreventDefault() ? l : c) : this.type = t, n && ue.extend(this, n), this.timeStamp = t && t.timeStamp || ue.now(), this[ue.expando] = !0, e) : new ue.Event(t, n)
        }, ue.Event.prototype = {
            isDefaultPrevented: c,
            isPropagationStopped: c,
            isImmediatePropagationStopped: c,
            preventDefault: function() {
                var t = this.originalEvent;
                this.isDefaultPrevented = l, t && (t.preventDefault ? t.preventDefault() : t.returnValue = !1)
            },
            stopPropagation: function() {
                var t = this.originalEvent;
                this.isPropagationStopped = l, t && (t.stopPropagation && t.stopPropagation(), t.cancelBubble = !0)
            },
            stopImmediatePropagation: function() {
                this.isImmediatePropagationStopped = l, this.stopPropagation()
            }
        }, ue.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout"
        }, function(t, e) {
            ue.event.special[t] = {
                delegateType: e,
                bindType: e,
                handle: function(t) {
                    var n, i = this,
                        o = t.relatedTarget,
                        r = t.handleObj;
                    return (!o || o !== i && !ue.contains(i, o)) && (t.type = r.origType, n = r.handler.apply(this, arguments), t.type = e), n
                }
            }
        }), ue.support.submitBubbles || (ue.event.special.submit = {
            setup: function() {
                return ue.nodeName(this, "form") ? !1 : (ue.event.add(this, "click._submit keypress._submit", function(t) {
                    var n = t.target,
                        i = ue.nodeName(n, "input") || ue.nodeName(n, "button") ? n.form : e;
                    i && !ue._data(i, "submitBubbles") && (ue.event.add(i, "submit._submit", function(t) {
                        t._submit_bubble = !0
                    }), ue._data(i, "submitBubbles", !0))
                }), e)
            },
            postDispatch: function(t) {
                t._submit_bubble && (delete t._submit_bubble, this.parentNode && !t.isTrigger && ue.event.simulate("submit", this.parentNode, t, !0))
            },
            teardown: function() {
                return ue.nodeName(this, "form") ? !1 : (ue.event.remove(this, "._submit"), e)
            }
        }), ue.support.changeBubbles || (ue.event.special.change = {
            setup: function() {
                return ze.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (ue.event.add(this, "propertychange._change", function(t) {
                    "checked" === t.originalEvent.propertyName && (this._just_changed = !0)
                }), ue.event.add(this, "click._change", function(t) {
                    this._just_changed && !t.isTrigger && (this._just_changed = !1), ue.event.simulate("change", this, t, !0)
                })), !1) : (ue.event.add(this, "beforeactivate._change", function(t) {
                    var e = t.target;
                    ze.test(e.nodeName) && !ue._data(e, "changeBubbles") && (ue.event.add(e, "change._change", function(t) {
                        !this.parentNode || t.isSimulated || t.isTrigger || ue.event.simulate("change", this.parentNode, t, !0)
                    }), ue._data(e, "changeBubbles", !0))
                }), e)
            },
            handle: function(t) {
                var n = t.target;
                return this !== n || t.isSimulated || t.isTrigger || "radio" !== n.type && "checkbox" !== n.type ? t.handleObj.handler.apply(this, arguments) : e
            },
            teardown: function() {
                return ue.event.remove(this, "._change"), !ze.test(this.nodeName)
            }
        }), ue.support.focusinBubbles || ue.each({
            focus: "focusin",
            blur: "focusout"
        }, function(t, e) {
            var n = 0,
                i = function(t) {
                    ue.event.simulate(e, t.target, ue.event.fix(t), !0)
                };
            ue.event.special[e] = {
                setup: function() {
                    0 === n++ && G.addEventListener(t, i, !0)
                },
                teardown: function() {
                    0 === --n && G.removeEventListener(t, i, !0)
                }
            }
        }), ue.fn.extend({
            on: function(t, n, i, o, r) {
                var s, a;
                if ("object" == typeof t) {
                    "string" != typeof n && (i = i || n, n = e);
                    for (s in t) this.on(s, n, i, t[s], r);
                    return this
                }
                if (null == i && null == o ? (o = n, i = n = e) : null == o && ("string" == typeof n ? (o = i, i = e) : (o = i, i = n, n = e)), o === !1) o = c;
                else if (!o) return this;
                return 1 === r && (a = o, o = function(t) {
                    return ue().off(t), a.apply(this, arguments)
                }, o.guid = a.guid || (a.guid = ue.guid++)), this.each(function() {
                    ue.event.add(this, t, o, i, n)
                })
            },
            one: function(t, e, n, i) {
                return this.on(t, e, n, i, 1)
            },
            off: function(t, n, i) {
                var o, r;
                if (t && t.preventDefault && t.handleObj) return o = t.handleObj, ue(t.delegateTarget).off(o.namespace ? o.origType + "." + o.namespace : o.origType, o.selector, o.handler), this;
                if ("object" == typeof t) {
                    for (r in t) this.off(r, n, t[r]);
                    return this
                }
                return (n === !1 || "function" == typeof n) && (i = n, n = e), i === !1 && (i = c), this.each(function() {
                    ue.event.remove(this, t, i, n)
                })
            },
            trigger: function(t, e) {
                return this.each(function() {
                    ue.event.trigger(t, e, this)
                })
            },
            triggerHandler: function(t, n) {
                var i = this[0];
                return i ? ue.event.trigger(t, n, i, !0) : e
            }
        });
        var qe = /^.[^:#\[\.,]*$/,
            Re = /^(?:parents|prev(?:Until|All))/,
            Ve = ue.expr.match.needsContext,
            Be = {
                children: !0,
                contents: !0,
                next: !0,
                prev: !0
            };
        ue.fn.extend({
            find: function(t) {
                var e, n = [],
                    i = this,
                    o = i.length;
                if ("string" != typeof t) return this.pushStack(ue(t).filter(function() {
                    for (e = 0; o > e; e++)
                        if (ue.contains(i[e], this)) return !0
                }));
                for (e = 0; o > e; e++) ue.find(t, i[e], n);
                return n = this.pushStack(o > 1 ? ue.unique(n) : n), n.selector = this.selector ? this.selector + " " + t : t, n
            },
            has: function(t) {
                var e, n = ue(t, this),
                    i = n.length;
                return this.filter(function() {
                    for (e = 0; i > e; e++)
                        if (ue.contains(this, n[e])) return !0
                })
            },
            not: function(t) {
                return this.pushStack(h(this, t || [], !0))
            },
            filter: function(t) {
                return this.pushStack(h(this, t || [], !1))
            },
            is: function(t) {
                return !!h(this, "string" == typeof t && Ve.test(t) ? ue(t) : t || [], !1).length
            },
            closest: function(t, e) {
                for (var n, i = 0, o = this.length, r = [], s = Ve.test(t) || "string" != typeof t ? ue(t, e || this.context) : 0; o > i; i++)
                    for (n = this[i]; n && n !== e; n = n.parentNode)
                        if (11 > n.nodeType && (s ? s.index(n) > -1 : 1 === n.nodeType && ue.find.matchesSelector(n, t))) {
                            n = r.push(n);
                            break
                        }
                return this.pushStack(r.length > 1 ? ue.unique(r) : r)
            },
            index: function(t) {
                return t ? "string" == typeof t ? ue.inArray(this[0], ue(t)) : ue.inArray(t.jquery ? t[0] : t, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
            },
            add: function(t, e) {
                var n = "string" == typeof t ? ue(t, e) : ue.makeArray(t && t.nodeType ? [t] : t),
                    i = ue.merge(this.get(), n);
                return this.pushStack(ue.unique(i))
            },
            addBack: function(t) {
                return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
            }
        }), ue.each({
            parent: function(t) {
                var e = t.parentNode;
                return e && 11 !== e.nodeType ? e : null
            },
            parents: function(t) {
                return ue.dir(t, "parentNode")
            },
            parentsUntil: function(t, e, n) {
                return ue.dir(t, "parentNode", n)
            },
            next: function(t) {
                return d(t, "nextSibling")
            },
            prev: function(t) {
                return d(t, "previousSibling")
            },
            nextAll: function(t) {
                return ue.dir(t, "nextSibling")
            },
            prevAll: function(t) {
                return ue.dir(t, "previousSibling")
            },
            nextUntil: function(t, e, n) {
                return ue.dir(t, "nextSibling", n)
            },
            prevUntil: function(t, e, n) {
                return ue.dir(t, "previousSibling", n)
            },
            siblings: function(t) {
                return ue.sibling((t.parentNode || {}).firstChild, t)
            },
            children: function(t) {
                return ue.sibling(t.firstChild)
            },
            contents: function(t) {
                return ue.nodeName(t, "iframe") ? t.contentDocument || t.contentWindow.document : ue.merge([], t.childNodes)
            }
        }, function(t, e) {
            ue.fn[t] = function(n, i) {
                var o = ue.map(this, e, n);
                return "Until" !== t.slice(-5) && (i = n), i && "string" == typeof i && (o = ue.filter(i, o)), this.length > 1 && (Be[t] || (o = ue.unique(o)), Re.test(t) && (o = o.reverse())), this.pushStack(o)
            }
        }), ue.extend({
            filter: function(t, e, n) {
                var i = e[0];
                return n && (t = ":not(" + t + ")"), 1 === e.length && 1 === i.nodeType ? ue.find.matchesSelector(i, t) ? [i] : [] : ue.find.matches(t, ue.grep(e, function(t) {
                    return 1 === t.nodeType
                }))
            },
            dir: function(t, n, i) {
                for (var o = [], r = t[n]; r && 9 !== r.nodeType && (i === e || 1 !== r.nodeType || !ue(r).is(i));) 1 === r.nodeType && o.push(r), r = r[n];
                return o
            },
            sibling: function(t, e) {
                for (var n = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && n.push(t);
                return n
            }
        });
        var Ye = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
            Qe = / jQuery\d+="(?:null|\d+)"/g,
            Ue = RegExp("<(?:" + Ye + ")[\\s/>]", "i"),
            Xe = /^\s+/,
            Ge = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
            Je = /<([\w:]+)/,
            Ke = /<tbody/i,
            Ze = /<|&#?\w+;/,
            tn = /<(?:script|style|link)/i,
            en = /^(?:checkbox|radio)$/i,
            nn = /checked\s*(?:[^=]|=\s*.checked.)/i,
            on = /^$|\/(?:java|ecma)script/i,
            rn = /^true\/(.*)/,
            sn = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
            an = {
                option: [1, "<select multiple='multiple'>", "</select>"],
                legend: [1, "<fieldset>", "</fieldset>"],
                area: [1, "<map>", "</map>"],
                param: [1, "<object>", "</object>"],
                thead: [1, "<table>", "</table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                _default: ue.support.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
            },
            ln = p(G),
            cn = ln.appendChild(G.createElement("div"));
        an.optgroup = an.option, an.tbody = an.tfoot = an.colgroup = an.caption = an.thead, an.th = an.td, ue.fn.extend({
            text: function(t) {
                return ue.access(this, function(t) {
                    return t === e ? ue.text(this) : this.empty().append((this[0] && this[0].ownerDocument || G).createTextNode(t))
                }, null, t, arguments.length)
            },
            append: function() {
                return this.domManip(arguments, function(t) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var e = f(this, t);
                        e.appendChild(t)
                    }
                })
            },
            prepend: function() {
                return this.domManip(arguments, function(t) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var e = f(this, t);
                        e.insertBefore(t, e.firstChild)
                    }
                })
            },
            before: function() {
                return this.domManip(arguments, function(t) {
                    this.parentNode && this.parentNode.insertBefore(t, this)
                })
            },
            after: function() {
                return this.domManip(arguments, function(t) {
                    this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
                })
            },
            remove: function(t, e) {
                for (var n, i = t ? ue.filter(t, this) : this, o = 0; null != (n = i[o]); o++) e || 1 !== n.nodeType || ue.cleanData(_(n)), n.parentNode && (e && ue.contains(n.ownerDocument, n) && v(_(n, "script")), n.parentNode.removeChild(n));
                return this
            },
            empty: function() {
                for (var t, e = 0; null != (t = this[e]); e++) {
                    for (1 === t.nodeType && ue.cleanData(_(t, !1)); t.firstChild;) t.removeChild(t.firstChild);
                    t.options && ue.nodeName(t, "select") && (t.options.length = 0)
                }
                return this
            },
            clone: function(t, e) {
                return t = null == t ? !1 : t, e = null == e ? t : e, this.map(function() {
                    return ue.clone(this, t, e)
                })
            },
            html: function(t) {
                return ue.access(this, function(t) {
                    var n = this[0] || {},
                        i = 0,
                        o = this.length;
                    if (t === e) return 1 === n.nodeType ? n.innerHTML.replace(Qe, "") : e;
                    if (!("string" != typeof t || tn.test(t) || !ue.support.htmlSerialize && Ue.test(t) || !ue.support.leadingWhitespace && Xe.test(t) || an[(Je.exec(t) || ["", ""])[1].toLowerCase()])) {
                        t = t.replace(Ge, "<$1></$2>");
                        try {
                            for (; o > i; i++) n = this[i] || {}, 1 === n.nodeType && (ue.cleanData(_(n, !1)), n.innerHTML = t);
                            n = 0
                        } catch (r) {}
                    }
                    n && this.empty().append(t)
                }, null, t, arguments.length)
            },
            replaceWith: function() {
                var t = ue.map(this, function(t) {
                        return [t.nextSibling, t.parentNode]
                    }),
                    e = 0;
                return this.domManip(arguments, function(n) {
                    var i = t[e++],
                        o = t[e++];
                    o && (i && i.parentNode !== o && (i = this.nextSibling), ue(this).remove(), o.insertBefore(n, i))
                }, !0), e ? this : this.remove()
            },
            detach: function(t) {
                return this.remove(t, !0)
            },
            domManip: function(t, e, n) {
                t = ie.apply([], t);
                var i, o, r, s, a, l, c = 0,
                    u = this.length,
                    d = this,
                    h = u - 1,
                    p = t[0],
                    f = ue.isFunction(p);
                if (f || !(1 >= u || "string" != typeof p || ue.support.checkClone) && nn.test(p)) return this.each(function(i) {
                    var o = d.eq(i);
                    f && (t[0] = p.call(this, i, o.html())), o.domManip(t, e, n)
                });
                if (u && (l = ue.buildFragment(t, this[0].ownerDocument, !1, !n && this), i = l.firstChild, 1 === l.childNodes.length && (l = i), i)) {
                    for (s = ue.map(_(l, "script"), m), r = s.length; u > c; c++) o = l, c !== h && (o = ue.clone(o, !0, !0), r && ue.merge(s, _(o, "script"))), e.call(this[c], o, c);
                    if (r)
                        for (a = s[s.length - 1].ownerDocument, ue.map(s, g), c = 0; r > c; c++) o = s[c], on.test(o.type || "") && !ue._data(o, "globalEval") && ue.contains(a, o) && (o.src ? ue._evalUrl(o.src) : ue.globalEval((o.text || o.textContent || o.innerHTML || "").replace(sn, "")));
                    l = i = null
                }
                return this
            }
        }), ue.each({
            appendTo: "append",
            prependTo: "prepend",
            insertBefore: "before",
            insertAfter: "after",
            replaceAll: "replaceWith"
        }, function(t, e) {
            ue.fn[t] = function(t) {
                for (var n, i = 0, o = [], r = ue(t), s = r.length - 1; s >= i; i++) n = i === s ? this : this.clone(!0), ue(r[i])[e](n), oe.apply(o, n.get());
                return this.pushStack(o)
            }
        }), ue.extend({
            clone: function(t, e, n) {
                var i, o, r, s, a, l = ue.contains(t.ownerDocument, t);
                if (ue.support.html5Clone || ue.isXMLDoc(t) || !Ue.test("<" + t.nodeName + ">") ? r = t.cloneNode(!0) : (cn.innerHTML = t.outerHTML, cn.removeChild(r = cn.firstChild)), !(ue.support.noCloneEvent && ue.support.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || ue.isXMLDoc(t)))
                    for (i = _(r), a = _(t), s = 0; null != (o = a[s]); ++s) i[s] && y(o, i[s]);
                if (e)
                    if (n)
                        for (a = a || _(t), i = i || _(r), s = 0; null != (o = a[s]); s++) b(o, i[s]);
                    else b(t, r);
                return i = _(r, "script"), i.length > 0 && v(i, !l && _(t, "script")), i = a = o = null, r
            },
            buildFragment: function(t, e, n, i) {
                for (var o, r, s, a, l, c, u, d = t.length, h = p(e), f = [], m = 0; d > m; m++)
                    if (r = t[m], r || 0 === r)
                        if ("object" === ue.type(r)) ue.merge(f, r.nodeType ? [r] : r);
                        else if (Ze.test(r)) {
                    for (a = a || h.appendChild(e.createElement("div")), l = (Je.exec(r) || ["", ""])[1].toLowerCase(), u = an[l] || an._default, a.innerHTML = u[1] + r.replace(Ge, "<$1></$2>") + u[2], o = u[0]; o--;) a = a.lastChild;
                    if (!ue.support.leadingWhitespace && Xe.test(r) && f.push(e.createTextNode(Xe.exec(r)[0])), !ue.support.tbody)
                        for (r = "table" !== l || Ke.test(r) ? "<table>" !== u[1] || Ke.test(r) ? 0 : a : a.firstChild, o = r && r.childNodes.length; o--;) ue.nodeName(c = r.childNodes[o], "tbody") && !c.childNodes.length && r.removeChild(c);
                    for (ue.merge(f, a.childNodes), a.textContent = ""; a.firstChild;) a.removeChild(a.firstChild);
                    a = h.lastChild
                } else f.push(e.createTextNode(r));
                for (a && h.removeChild(a), ue.support.appendChecked || ue.grep(_(f, "input"), w), m = 0; r = f[m++];)
                    if ((!i || -1 === ue.inArray(r, i)) && (s = ue.contains(r.ownerDocument, r), a = _(h.appendChild(r), "script"), s && v(a), n))
                        for (o = 0; r = a[o++];) on.test(r.type || "") && n.push(r);
                return a = null, h
            },
            cleanData: function(t, e) {
                for (var n, i, o, r, s = 0, a = ue.expando, l = ue.cache, c = ue.support.deleteExpando, u = ue.event.special; null != (n = t[s]); s++)
                    if ((e || ue.acceptData(n)) && (o = n[a], r = o && l[o])) {
                        if (r.events)
                            for (i in r.events) u[i] ? ue.event.remove(n, i) : ue.removeEvent(n, i, r.handle);
                        l[o] && (delete l[o], c ? delete n[a] : typeof n.removeAttribute !== U ? n.removeAttribute(a) : n[a] = null, ee.push(o))
                    }
            },
            _evalUrl: function(t) {
                return ue.ajax({
                    url: t,
                    type: "GET",
                    dataType: "script",
                    async: !1,
                    global: !1,
                    "throws": !0
                })
            }
        }), ue.fn.extend({
            wrapAll: function(t) {
                if (ue.isFunction(t)) return this.each(function(e) {
                    ue(this).wrapAll(t.call(this, e))
                });
                if (this[0]) {
                    var e = ue(t, this[0].ownerDocument).eq(0).clone(!0);
                    this[0].parentNode && e.insertBefore(this[0]), e.map(function() {
                        for (var t = this; t.firstChild && 1 === t.firstChild.nodeType;) t = t.firstChild;
                        return t
                    }).append(this)
                }
                return this
            },
            wrapInner: function(t) {
                return this.each(ue.isFunction(t) ? function(e) {
                    ue(this).wrapInner(t.call(this, e))
                } : function() {
                    var e = ue(this),
                        n = e.contents();
                    n.length ? n.wrapAll(t) : e.append(t)
                })
            },
            wrap: function(t) {
                var e = ue.isFunction(t);
                return this.each(function(n) {
                    ue(this).wrapAll(e ? t.call(this, n) : t)
                })
            },
            unwrap: function() {
                return this.parent().each(function() {
                    ue.nodeName(this, "body") || ue(this).replaceWith(this.childNodes)
                }).end()
            }
        });
        var un, dn, hn, pn = /alpha\([^)]*\)/i,
            fn = /opacity\s*=\s*([^)]*)/,
            mn = /^(top|right|bottom|left)$/,
            gn = /^(none|table(?!-c[ea]).+)/,
            vn = /^margin/,
            bn = RegExp("^(" + de + ")(.*)$", "i"),
            yn = RegExp("^(" + de + ")(?!px)[a-z%]+$", "i"),
            _n = RegExp("^([+-])=(" + de + ")", "i"),
            wn = {
                BODY: "block"
            },
            xn = {
                position: "absolute",
                visibility: "hidden",
                display: "block"
            },
            Cn = {
                letterSpacing: 0,
                fontWeight: 400
            },
            kn = ["Top", "Right", "Bottom", "Left"],
            Tn = ["Webkit", "O", "Moz", "ms"];
        ue.fn.extend({
            css: function(t, n) {
                return ue.access(this, function(t, n, i) {
                    var o, r, s = {},
                        a = 0;
                    if (ue.isArray(n)) {
                        for (r = dn(t), o = n.length; o > a; a++) s[n[a]] = ue.css(t, n[a], !1, r);
                        return s
                    }
                    return i !== e ? ue.style(t, n, i) : ue.css(t, n)
                }, t, n, arguments.length > 1)
            },
            show: function() {
                return k(this, !0)
            },
            hide: function() {
                return k(this)
            },
            toggle: function(t) {
                var e = "boolean" == typeof t;
                return this.each(function() {
                    (e ? t : C(this)) ? ue(this).show(): ue(this).hide()
                })
            }
        }), ue.extend({
            cssHooks: {
                opacity: {
                    get: function(t, e) {
                        if (e) {
                            var n = hn(t, "opacity");
                            return "" === n ? "1" : n
                        }
                    }
                }
            },
            cssNumber: {
                columnCount: !0,
                fillOpacity: !0,
                fontWeight: !0,
                lineHeight: !0,
                opacity: !0,
                orphans: !0,
                widows: !0,
                zIndex: !0,
                zoom: !0
            },
            cssProps: {
                "float": ue.support.cssFloat ? "cssFloat" : "styleFloat"
            },
            style: function(t, n, i, o) {
                if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                    var r, s, a, l = ue.camelCase(n),
                        c = t.style;
                    if (n = ue.cssProps[l] || (ue.cssProps[l] = x(c, l)), a = ue.cssHooks[n] || ue.cssHooks[l], i === e) return a && "get" in a && (r = a.get(t, !1, o)) !== e ? r : c[n];
                    if (s = typeof i, "string" === s && (r = _n.exec(i)) && (i = (r[1] + 1) * r[2] + parseFloat(ue.css(t, n)), s = "number"), !(null == i || "number" === s && isNaN(i) || ("number" !== s || ue.cssNumber[l] || (i += "px"), ue.support.clearCloneStyle || "" !== i || 0 !== n.indexOf("background") || (c[n] = "inherit"), a && "set" in a && (i = a.set(t, i, o)) === e))) try {
                        c[n] = i
                    } catch (u) {}
                }
            },
            css: function(t, n, i, o) {
                var r, s, a, l = ue.camelCase(n);
                return n = ue.cssProps[l] || (ue.cssProps[l] = x(t.style, l)), a = ue.cssHooks[n] || ue.cssHooks[l], a && "get" in a && (s = a.get(t, !0, i)), s === e && (s = hn(t, n, o)), "normal" === s && n in Cn && (s = Cn[n]), "" === i || i ? (r = parseFloat(s), i === !0 || ue.isNumeric(r) ? r || 0 : s) : s
            }
        }), t.getComputedStyle ? (dn = function(e) {
            return t.getComputedStyle(e, null)
        }, hn = function(t, n, i) {
            var o, r, s, a = i || dn(t),
                l = a ? a.getPropertyValue(n) || a[n] : e,
                c = t.style;
            return a && ("" !== l || ue.contains(t.ownerDocument, t) || (l = ue.style(t, n)), yn.test(l) && vn.test(n) && (o = c.width, r = c.minWidth, s = c.maxWidth, c.minWidth = c.maxWidth = c.width = l, l = a.width, c.width = o, c.minWidth = r, c.maxWidth = s)), l
        }) : G.documentElement.currentStyle && (dn = function(t) {
            return t.currentStyle
        }, hn = function(t, n, i) {
            var o, r, s, a = i || dn(t),
                l = a ? a[n] : e,
                c = t.style;
            return null == l && c && c[n] && (l = c[n]), yn.test(l) && !mn.test(n) && (o = c.left, r = t.runtimeStyle, s = r && r.left, s && (r.left = t.currentStyle.left), c.left = "fontSize" === n ? "1em" : l, l = c.pixelLeft + "px", c.left = o, s && (r.left = s)), "" === l ? "auto" : l
        }), ue.each(["height", "width"], function(t, n) {
            ue.cssHooks[n] = {
                get: function(t, i, o) {
                    return i ? 0 === t.offsetWidth && gn.test(ue.css(t, "display")) ? ue.swap(t, xn, function() {
                        return S(t, n, o)
                    }) : S(t, n, o) : e
                },
                set: function(t, e, i) {
                    var o = i && dn(t);
                    return T(t, e, i ? $(t, n, i, ue.support.boxSizing && "border-box" === ue.css(t, "boxSizing", !1, o), o) : 0)
                }
            }
        }), ue.support.opacity || (ue.cssHooks.opacity = {
            get: function(t, e) {
                return fn.test((e && t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : e ? "1" : ""
            },
            set: function(t, e) {
                var n = t.style,
                    i = t.currentStyle,
                    o = ue.isNumeric(e) ? "alpha(opacity=" + 100 * e + ")" : "",
                    r = i && i.filter || n.filter || "";
                n.zoom = 1, (e >= 1 || "" === e) && "" === ue.trim(r.replace(pn, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === e || i && !i.filter) || (n.filter = pn.test(r) ? r.replace(pn, o) : r + " " + o)
            }
        }), ue(function() {
            ue.support.reliableMarginRight || (ue.cssHooks.marginRight = {
                get: function(t, n) {
                    return n ? ue.swap(t, {
                        display: "inline-block"
                    }, hn, [t, "marginRight"]) : e
                }
            }), !ue.support.pixelPosition && ue.fn.position && ue.each(["top", "left"], function(t, n) {
                ue.cssHooks[n] = {
                    get: function(t, i) {
                        return i ? (i = hn(t, n), yn.test(i) ? ue(t).position()[n] + "px" : i) : e
                    }
                }
            })
        }), ue.expr && ue.expr.filters && (ue.expr.filters.hidden = function(t) {
            return 0 >= t.offsetWidth && 0 >= t.offsetHeight || !ue.support.reliableHiddenOffsets && "none" === (t.style && t.style.display || ue.css(t, "display"))
        }, ue.expr.filters.visible = function(t) {
            return !ue.expr.filters.hidden(t)
        }), ue.each({
            margin: "",
            padding: "",
            border: "Width"
        }, function(t, e) {
            ue.cssHooks[t + e] = {
                expand: function(n) {
                    for (var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++) o[t + kn[i] + e] = r[i] || r[i - 2] || r[0];
                    return o
                }
            }, vn.test(t) || (ue.cssHooks[t + e].set = T)
        });
        var $n = /%20/g,
            Sn = /\[\]$/,
            jn = /\r?\n/g,
            Dn = /^(?:submit|button|image|reset|file)$/i,
            En = /^(?:input|select|textarea|keygen)/i;
        ue.fn.extend({
            serialize: function() {
                return ue.param(this.serializeArray())
            },
            serializeArray: function() {
                return this.map(function() {
                    var t = ue.prop(this, "elements");
                    return t ? ue.makeArray(t) : this
                }).filter(function() {
                    var t = this.type;
                    return this.name && !ue(this).is(":disabled") && En.test(this.nodeName) && !Dn.test(t) && (this.checked || !en.test(t))
                }).map(function(t, e) {
                    var n = ue(this).val();
                    return null == n ? null : ue.isArray(n) ? ue.map(n, function(t) {
                        return {
                            name: e.name,
                            value: t.replace(jn, "\r\n")
                        }
                    }) : {
                        name: e.name,
                        value: n.replace(jn, "\r\n")
                    }
                }).get()
            }
        }), ue.param = function(t, n) {
            var i, o = [],
                r = function(t, e) {
                    e = ue.isFunction(e) ? e() : null == e ? "" : e, o[o.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
                };
            if (n === e && (n = ue.ajaxSettings && ue.ajaxSettings.traditional), ue.isArray(t) || t.jquery && !ue.isPlainObject(t)) ue.each(t, function() {
                r(this.name, this.value)
            });
            else
                for (i in t) E(i, t[i], n, r);
            return o.join("&").replace($n, "+")
        }, ue.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(t, e) {
            ue.fn[e] = function(t, n) {
                return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e)
            }
        }), ue.fn.extend({
            hover: function(t, e) {
                return this.mouseenter(t).mouseleave(e || t)
            },
            bind: function(t, e, n) {
                return this.on(t, null, e, n)
            },
            unbind: function(t, e) {
                return this.off(t, null, e)
            },
            delegate: function(t, e, n, i) {
                return this.on(e, t, n, i)
            },
            undelegate: function(t, e, n) {
                return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n)
            }
        });
        var Pn, An, Nn = ue.now(),
            Mn = /\?/,
            Hn = /#.*$/,
            In = /([?&])_=[^&]*/,
            zn = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
            On = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
            Ln = /^(?:GET|HEAD)$/,
            Fn = /^\/\//,
            Wn = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
            qn = ue.fn.load,
            Rn = {},
            Vn = {},
            Bn = "*/".concat("*");
        try {
            An = X.href
        } catch (Yn) {
            An = G.createElement("a"), An.href = "", An = An.href
        }
        Pn = Wn.exec(An.toLowerCase()) || [], ue.fn.load = function(t, n, i) {
            if ("string" != typeof t && qn) return qn.apply(this, arguments);
            var o, r, s, a = this,
                l = t.indexOf(" ");
            return l >= 0 && (o = t.slice(l, t.length), t = t.slice(0, l)), ue.isFunction(n) ? (i = n, n = e) : n && "object" == typeof n && (s = "POST"), a.length > 0 && ue.ajax({
                url: t,
                type: s,
                dataType: "html",
                data: n
            }).done(function(t) {
                r = arguments, a.html(o ? ue("<div>").append(ue.parseHTML(t)).find(o) : t)
            }).complete(i && function(t, e) {
                a.each(i, r || [t.responseText, e, t])
            }), this
        }, ue.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(t, e) {
            ue.fn[e] = function(t) {
                return this.on(e, t)
            }
        }), ue.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: An,
                type: "GET",
                isLocal: On.test(Pn[1]),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": Bn,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript"
                },
                contents: {
                    xml: /xml/,
                    html: /html/,
                    json: /json/
                },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText",
                    json: "responseJSON"
                },
                converters: {
                    "* text": String,
                    "text html": !0,
                    "text json": ue.parseJSON,
                    "text xml": ue.parseXML
                },
                flatOptions: {
                    url: !0,
                    context: !0
                }
            },
            ajaxSetup: function(t, e) {
                return e ? N(N(t, ue.ajaxSettings), e) : N(ue.ajaxSettings, t)
            },
            ajaxPrefilter: P(Rn),
            ajaxTransport: P(Vn),
            ajax: function(t, n) {
                function i(t, n, i, o) {
                    var r, d, b, y, w, C = n;
                    2 !== _ && (_ = 2, l && clearTimeout(l), u = e, a = o || "", x.readyState = t > 0 ? 4 : 0, r = t >= 200 && 300 > t || 304 === t, i && (y = M(h, x, i)), y = H(h, y, x, r), r ? (h.ifModified && (w = x.getResponseHeader("Last-Modified"), w && (ue.lastModified[s] = w), w = x.getResponseHeader("etag"), w && (ue.etag[s] = w)), 204 === t || "HEAD" === h.type ? C = "nocontent" : 304 === t ? C = "notmodified" : (C = y.state, d = y.data, b = y.error, r = !b)) : (b = C, (t || !C) && (C = "error", 0 > t && (t = 0))), x.status = t, x.statusText = (n || C) + "", r ? m.resolveWith(p, [d, C, x]) : m.rejectWith(p, [x, C, b]), x.statusCode(v), v = e, c && f.trigger(r ? "ajaxSuccess" : "ajaxError", [x, h, r ? d : b]), g.fireWith(p, [x, C]), c && (f.trigger("ajaxComplete", [x, h]), --ue.active || ue.event.trigger("ajaxStop")))
                }
                "object" == typeof t && (n = t, t = e), n = n || {};
                var o, r, s, a, l, c, u, d, h = ue.ajaxSetup({}, n),
                    p = h.context || h,
                    f = h.context && (p.nodeType || p.jquery) ? ue(p) : ue.event,
                    m = ue.Deferred(),
                    g = ue.Callbacks("once memory"),
                    v = h.statusCode || {},
                    b = {},
                    y = {},
                    _ = 0,
                    w = "canceled",
                    x = {
                        readyState: 0,
                        getResponseHeader: function(t) {
                            var e;
                            if (2 === _) {
                                if (!d)
                                    for (d = {}; e = zn.exec(a);) d[e[1].toLowerCase()] = e[2];
                                e = d[t.toLowerCase()]
                            }
                            return null == e ? null : e
                        },
                        getAllResponseHeaders: function() {
                            return 2 === _ ? a : null
                        },
                        setRequestHeader: function(t, e) {
                            var n = t.toLowerCase();
                            return _ || (t = y[n] = y[n] || t, b[t] = e), this
                        },
                        overrideMimeType: function(t) {
                            return _ || (h.mimeType = t), this
                        },
                        statusCode: function(t) {
                            var e;
                            if (t)
                                if (2 > _)
                                    for (e in t) v[e] = [v[e], t[e]];
                                else x.always(t[x.status]);
                            return this
                        },
                        abort: function(t) {
                            var e = t || w;
                            return u && u.abort(e), i(0, e), this
                        }
                    };
                if (m.promise(x).complete = g.add, x.success = x.done, x.error = x.fail, h.url = ((t || h.url || An) + "").replace(Hn, "").replace(Fn, Pn[1] + "//"), h.type = n.method || n.type || h.method || h.type, h.dataTypes = ue.trim(h.dataType || "*").toLowerCase().match(he) || [""], null == h.crossDomain && (o = Wn.exec(h.url.toLowerCase()), h.crossDomain = !(!o || o[1] === Pn[1] && o[2] === Pn[2] && (o[3] || ("http:" === o[1] ? "80" : "443")) === (Pn[3] || ("http:" === Pn[1] ? "80" : "443")))), h.data && h.processData && "string" != typeof h.data && (h.data = ue.param(h.data, h.traditional)), A(Rn, h, n, x), 2 === _) return x;
                c = h.global, c && 0 === ue.active++ && ue.event.trigger("ajaxStart"), h.type = h.type.toUpperCase(), h.hasContent = !Ln.test(h.type), s = h.url, h.hasContent || (h.data && (s = h.url += (Mn.test(s) ? "&" : "?") + h.data, delete h.data), h.cache === !1 && (h.url = In.test(s) ? s.replace(In, "$1_=" + Nn++) : s + (Mn.test(s) ? "&" : "?") + "_=" + Nn++)), h.ifModified && (ue.lastModified[s] && x.setRequestHeader("If-Modified-Since", ue.lastModified[s]), ue.etag[s] && x.setRequestHeader("If-None-Match", ue.etag[s])), (h.data && h.hasContent && h.contentType !== !1 || n.contentType) && x.setRequestHeader("Content-Type", h.contentType), x.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + Bn + "; q=0.01" : "") : h.accepts["*"]);
                for (r in h.headers) x.setRequestHeader(r, h.headers[r]);
                if (h.beforeSend && (h.beforeSend.call(p, x, h) === !1 || 2 === _)) return x.abort();
                w = "abort";
                for (r in {
                        success: 1,
                        error: 1,
                        complete: 1
                    }) x[r](h[r]);
                if (u = A(Vn, h, n, x)) {
                    x.readyState = 1, c && f.trigger("ajaxSend", [x, h]), h.async && h.timeout > 0 && (l = setTimeout(function() {
                        x.abort("timeout")
                    }, h.timeout));
                    try {
                        _ = 1, u.send(b, i)
                    } catch (C) {
                        if (!(2 > _)) throw C;
                        i(-1, C)
                    }
                } else i(-1, "No Transport");
                return x
            },
            getJSON: function(t, e, n) {
                return ue.get(t, e, n, "json")
            },
            getScript: function(t, n) {
                return ue.get(t, e, n, "script")
            }
        }), ue.each(["get", "post"], function(t, n) {
            ue[n] = function(t, i, o, r) {
                return ue.isFunction(i) && (r = r || o, o = i, i = e), ue.ajax({
                    url: t,
                    type: n,
                    dataType: r,
                    data: i,
                    success: o
                })
            }
        }), ue.ajaxSetup({
            accepts: {
                script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
            },
            contents: {
                script: /(?:java|ecma)script/
            },
            converters: {
                "text script": function(t) {
                    return ue.globalEval(t), t
                }
            }
        }), ue.ajaxPrefilter("script", function(t) {
            t.cache === e && (t.cache = !1), t.crossDomain && (t.type = "GET", t.global = !1)
        }), ue.ajaxTransport("script", function(t) {
            if (t.crossDomain) {
                var n, i = G.head || ue("head")[0] || G.documentElement;
                return {
                    send: function(e, o) {
                        n = G.createElement("script"), n.async = !0, t.scriptCharset && (n.charset = t.scriptCharset), n.src = t.url, n.onload = n.onreadystatechange = function(t, e) {
                            (e || !n.readyState || /loaded|complete/.test(n.readyState)) && (n.onload = n.onreadystatechange = null, n.parentNode && n.parentNode.removeChild(n), n = null, e || o(200, "success"))
                        }, i.insertBefore(n, i.firstChild)
                    },
                    abort: function() {
                        n && n.onload(e, !0)
                    }
                }
            }
        });
        var Qn = [],
            Un = /(=)\?(?=&|$)|\?\?/;
        ue.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function() {
                var t = Qn.pop() || ue.expando + "_" + Nn++;
                return this[t] = !0, t
            }
        }), ue.ajaxPrefilter("json jsonp", function(n, i, o) {
            var r, s, a, l = n.jsonp !== !1 && (Un.test(n.url) ? "url" : "string" == typeof n.data && !(n.contentType || "").indexOf("application/x-www-form-urlencoded") && Un.test(n.data) && "data");
            return l || "jsonp" === n.dataTypes[0] ? (r = n.jsonpCallback = ue.isFunction(n.jsonpCallback) ? n.jsonpCallback() : n.jsonpCallback, l ? n[l] = n[l].replace(Un, "$1" + r) : n.jsonp !== !1 && (n.url += (Mn.test(n.url) ? "&" : "?") + n.jsonp + "=" + r), n.converters["script json"] = function() {
                return a || ue.error(r + " was not called"), a[0]
            }, n.dataTypes[0] = "json", s = t[r], t[r] = function() {
                a = arguments
            }, o.always(function() {
                t[r] = s, n[r] && (n.jsonpCallback = i.jsonpCallback, Qn.push(r)), a && ue.isFunction(s) && s(a[0]), a = s = e
            }), "script") : e
        });
        var Xn, Gn, Jn = 0,
            Kn = t.ActiveXObject && function() {
                var t;
                for (t in Xn) Xn[t](e, !0)
            };
        ue.ajaxSettings.xhr = t.ActiveXObject ? function() {
            return !this.isLocal && I() || z()
        } : I, Gn = ue.ajaxSettings.xhr(), ue.support.cors = !!Gn && "withCredentials" in Gn, Gn = ue.support.ajax = !!Gn, Gn && ue.ajaxTransport(function(n) {
            if (!n.crossDomain || ue.support.cors) {
                var i;
                return {
                    send: function(o, r) {
                        var s, a, l = n.xhr();
                        if (n.username ? l.open(n.type, n.url, n.async, n.username, n.password) : l.open(n.type, n.url, n.async), n.xhrFields)
                            for (a in n.xhrFields) l[a] = n.xhrFields[a];
                        n.mimeType && l.overrideMimeType && l.overrideMimeType(n.mimeType), n.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest");
                        try {
                            for (a in o) l.setRequestHeader(a, o[a])
                        } catch (c) {}
                        l.send(n.hasContent && n.data || null), i = function(t, o) {
                            var a, c, u, d;
                            try {
                                if (i && (o || 4 === l.readyState))
                                    if (i = e, s && (l.onreadystatechange = ue.noop, Kn && delete Xn[s]), o) 4 !== l.readyState && l.abort();
                                    else {
                                        d = {}, a = l.status, c = l.getAllResponseHeaders(), "string" == typeof l.responseText && (d.text = l.responseText);
                                        try {
                                            u = l.statusText
                                        } catch (h) {
                                            u = ""
                                        }
                                        a || !n.isLocal || n.crossDomain ? 1223 === a && (a = 204) : a = d.text ? 200 : 404
                                    }
                            } catch (p) {
                                o || r(-1, p)
                            }
                            d && r(a, u, d, c)
                        }, n.async ? 4 === l.readyState ? setTimeout(i) : (s = ++Jn, Kn && (Xn || (Xn = {}, ue(t).unload(Kn)), Xn[s] = i), l.onreadystatechange = i) : i()
                    },
                    abort: function() {
                        i && i(e, !0)
                    }
                }
            }
        });
        var Zn, ti, ei = /^(?:toggle|show|hide)$/,
            ni = RegExp("^(?:([+-])=|)(" + de + ")([a-z%]*)$", "i"),
            ii = /queueHooks$/,
            oi = [q],
            ri = {
                "*": [function(t, e) {
                    var n = this.createTween(t, e),
                        i = n.cur(),
                        o = ni.exec(e),
                        r = o && o[3] || (ue.cssNumber[t] ? "" : "px"),
                        s = (ue.cssNumber[t] || "px" !== r && +i) && ni.exec(ue.css(n.elem, t)),
                        a = 1,
                        l = 20;
                    if (s && s[3] !== r) {
                        r = r || s[3], o = o || [], s = +i || 1;
                        do a = a || ".5", s /= a, ue.style(n.elem, t, s + r); while (a !== (a = n.cur() / i) && 1 !== a && --l)
                    }
                    return o && (s = n.start = +s || +i || 0, n.unit = r, n.end = o[1] ? s + (o[1] + 1) * o[2] : +o[2]), n
                }]
            };
        ue.Animation = ue.extend(F, {
            tweener: function(t, e) {
                ue.isFunction(t) ? (e = t, t = ["*"]) : t = t.split(" ");
                for (var n, i = 0, o = t.length; o > i; i++) n = t[i], ri[n] = ri[n] || [], ri[n].unshift(e)
            },
            prefilter: function(t, e) {
                e ? oi.unshift(t) : oi.push(t)
            }
        }), ue.Tween = R, R.prototype = {
            constructor: R,
            init: function(t, e, n, i, o, r) {
                this.elem = t, this.prop = n, this.easing = o || "swing", this.options = e, this.start = this.now = this.cur(), this.end = i, this.unit = r || (ue.cssNumber[n] ? "" : "px")
            },
            cur: function() {
                var t = R.propHooks[this.prop];
                return t && t.get ? t.get(this) : R.propHooks._default.get(this)
            },
            run: function(t) {
                var e, n = R.propHooks[this.prop];
                return this.pos = e = this.options.duration ? ue.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : R.propHooks._default.set(this), this
            }
        }, R.prototype.init.prototype = R.prototype, R.propHooks = {
            _default: {
                get: function(t) {
                    var e;
                    return null == t.elem[t.prop] || t.elem.style && null != t.elem.style[t.prop] ? (e = ue.css(t.elem, t.prop, ""), e && "auto" !== e ? e : 0) : t.elem[t.prop]
                },
                set: function(t) {
                    ue.fx.step[t.prop] ? ue.fx.step[t.prop](t) : t.elem.style && (null != t.elem.style[ue.cssProps[t.prop]] || ue.cssHooks[t.prop]) ? ue.style(t.elem, t.prop, t.now + t.unit) : t.elem[t.prop] = t.now
                }
            }
        }, R.propHooks.scrollTop = R.propHooks.scrollLeft = {
            set: function(t) {
                t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
            }
        }, ue.each(["toggle", "show", "hide"], function(t, e) {
            var n = ue.fn[e];
            ue.fn[e] = function(t, i, o) {
                return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(V(e, !0), t, i, o)
            }
        }), ue.fn.extend({
            fadeTo: function(t, e, n, i) {
                return this.filter(C).css("opacity", 0).show().end().animate({
                    opacity: e
                }, t, n, i)
            },
            animate: function(t, e, n, i) {
                var o = ue.isEmptyObject(t),
                    r = ue.speed(e, n, i),
                    s = function() {
                        var e = F(this, ue.extend({}, t), r);
                        (o || ue._data(this, "finish")) && e.stop(!0)
                    };
                return s.finish = s, o || r.queue === !1 ? this.each(s) : this.queue(r.queue, s)
            },
            stop: function(t, n, i) {
                var o = function(t) {
                    var e = t.stop;
                    delete t.stop, e(i)
                };
                return "string" != typeof t && (i = n, n = t, t = e), n && t !== !1 && this.queue(t || "fx", []), this.each(function() {
                    var e = !0,
                        n = null != t && t + "queueHooks",
                        r = ue.timers,
                        s = ue._data(this);
                    if (n) s[n] && s[n].stop && o(s[n]);
                    else
                        for (n in s) s[n] && s[n].stop && ii.test(n) && o(s[n]);
                    for (n = r.length; n--;) r[n].elem !== this || null != t && r[n].queue !== t || (r[n].anim.stop(i), e = !1, r.splice(n, 1));
                    (e || !i) && ue.dequeue(this, t)
                })
            },
            finish: function(t) {
                return t !== !1 && (t = t || "fx"), this.each(function() {
                    var e, n = ue._data(this),
                        i = n[t + "queue"],
                        o = n[t + "queueHooks"],
                        r = ue.timers,
                        s = i ? i.length : 0;
                    for (n.finish = !0, ue.queue(this, t, []), o && o.stop && o.stop.call(this, !0), e = r.length; e--;) r[e].elem === this && r[e].queue === t && (r[e].anim.stop(!0), r.splice(e, 1));
                    for (e = 0; s > e; e++) i[e] && i[e].finish && i[e].finish.call(this);
                    delete n.finish
                })
            }
        }), ue.each({
            slideDown: V("show"),
            slideUp: V("hide"),
            slideToggle: V("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(t, e) {
            ue.fn[t] = function(t, n, i) {
                return this.animate(e, t, n, i)
            }
        }), ue.speed = function(t, e, n) {
            var i = t && "object" == typeof t ? ue.extend({}, t) : {
                complete: n || !n && e || ue.isFunction(t) && t,
                duration: t,
                easing: n && e || e && !ue.isFunction(e) && e
            };
            return i.duration = ue.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in ue.fx.speeds ? ue.fx.speeds[i.duration] : ue.fx.speeds._default, (null == i.queue || i.queue === !0) && (i.queue = "fx"), i.old = i.complete, i.complete = function() {
                ue.isFunction(i.old) && i.old.call(this), i.queue && ue.dequeue(this, i.queue)
            }, i
        }, ue.easing = {
            linear: function(t) {
                return t
            },
            swing: function(t) {
                return .5 - Math.cos(t * Math.PI) / 2
            }
        }, ue.timers = [], ue.fx = R.prototype.init, ue.fx.tick = function() {
            var t, n = ue.timers,
                i = 0;
            for (Zn = ue.now(); n.length > i; i++) t = n[i], t() || n[i] !== t || n.splice(i--, 1);
            n.length || ue.fx.stop(), Zn = e
        }, ue.fx.timer = function(t) {
            t() && ue.timers.push(t) && ue.fx.start()
        }, ue.fx.interval = 13, ue.fx.start = function() {
            ti || (ti = setInterval(ue.fx.tick, ue.fx.interval))
        }, ue.fx.stop = function() {
            clearInterval(ti), ti = null
        }, ue.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, ue.fx.step = {}, ue.expr && ue.expr.filters && (ue.expr.filters.animated = function(t) {
            return ue.grep(ue.timers, function(e) {
                return t === e.elem
            }).length
        }), ue.fn.offset = function(t) {
            if (arguments.length) return t === e ? this : this.each(function(e) {
                ue.offset.setOffset(this, t, e)
            });
            var n, i, o = {
                    top: 0,
                    left: 0
                },
                r = this[0],
                s = r && r.ownerDocument;
            return s ? (n = s.documentElement, ue.contains(n, r) ? (typeof r.getBoundingClientRect !== U && (o = r.getBoundingClientRect()), i = B(s), {
                top: o.top + (i.pageYOffset || n.scrollTop) - (n.clientTop || 0),
                left: o.left + (i.pageXOffset || n.scrollLeft) - (n.clientLeft || 0)
            }) : o) : void 0
        }, ue.offset = {
            setOffset: function(t, e, n) {
                var i = ue.css(t, "position");
                "static" === i && (t.style.position = "relative");
                var o, r, s = ue(t),
                    a = s.offset(),
                    l = ue.css(t, "top"),
                    c = ue.css(t, "left"),
                    u = ("absolute" === i || "fixed" === i) && ue.inArray("auto", [l, c]) > -1,
                    d = {},
                    h = {};
                u ? (h = s.position(), o = h.top, r = h.left) : (o = parseFloat(l) || 0, r = parseFloat(c) || 0), ue.isFunction(e) && (e = e.call(t, n, a)), null != e.top && (d.top = e.top - a.top + o), null != e.left && (d.left = e.left - a.left + r), "using" in e ? e.using.call(t, d) : s.css(d)
            }
        }, ue.fn.extend({
            position: function() {
                if (this[0]) {
                    var t, e, n = {
                            top: 0,
                            left: 0
                        },
                        i = this[0];
                    return "fixed" === ue.css(i, "position") ? e = i.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), ue.nodeName(t[0], "html") || (n = t.offset()), n.top += ue.css(t[0], "borderTopWidth", !0), n.left += ue.css(t[0], "borderLeftWidth", !0)), {
                        top: e.top - n.top - ue.css(i, "marginTop", !0),
                        left: e.left - n.left - ue.css(i, "marginLeft", !0)
                    }
                }
            },
            offsetParent: function() {
                return this.map(function() {
                    for (var t = this.offsetParent || J; t && !ue.nodeName(t, "html") && "static" === ue.css(t, "position");) t = t.offsetParent;
                    return t || J
                })
            }
        }), ue.each({
            scrollLeft: "pageXOffset",
            scrollTop: "pageYOffset"
        }, function(t, n) {
            var i = /Y/.test(n);
            ue.fn[t] = function(o) {
                return ue.access(this, function(t, o, r) {
                    var s = B(t);
                    return r === e ? s ? n in s ? s[n] : s.document.documentElement[o] : t[o] : (s ? s.scrollTo(i ? ue(s).scrollLeft() : r, i ? r : ue(s).scrollTop()) : t[o] = r, e)
                }, t, o, arguments.length, null)
            }
        }), ue.each({
            Height: "height",
            Width: "width"
        }, function(t, n) {
            ue.each({
                padding: "inner" + t,
                content: n,
                "": "outer" + t
            }, function(i, o) {
                ue.fn[o] = function(o, r) {
                    var s = arguments.length && (i || "boolean" != typeof o),
                        a = i || (o === !0 || r === !0 ? "margin" : "border");
                    return ue.access(this, function(n, i, o) {
                        var r;
                        return ue.isWindow(n) ? n.document.documentElement["client" + t] : 9 === n.nodeType ? (r = n.documentElement, Math.max(n.body["scroll" + t], r["scroll" + t], n.body["offset" + t], r["offset" + t], r["client" + t])) : o === e ? ue.css(n, i, a) : ue.style(n, i, o, a)
                    }, n, s ? o : e, s, null)
                }
            })
        }), ue.fn.size = function() {
            return this.length
        }, ue.fn.andSelf = ue.fn.addBack, "object" == typeof module && module && "object" == typeof module.exports ? module.exports = ue : (t.jQuery = t.$ = ue, "function" == typeof define && define.amd && define("jquery", [], function() {
            return ue
        }))
    }(window), function(t, e) {
        t.rails !== e && t.error("jquery-ujs has already been loaded!");
        var n, i = t(document);
        t.rails = n = {
            linkClickSelector: "a[data-confirm], a[data-method], a[data-remote], a[data-disable-with]",
            buttonClickSelector: "button[data-remote]",
            inputChangeSelector: "select[data-remote], input[data-remote], textarea[data-remote]",
            formSubmitSelector: "form",
            formInputClickSelector: "form input[type=submit], form input[type=image], form button[type=submit], form button:not([type])",
            disableSelector: "input[data-disable-with], button[data-disable-with], textarea[data-disable-with]",
            enableSelector: "input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled",
            requiredInputSelector: "input[name][required]:not([disabled]),textarea[name][required]:not([disabled])",
            fileInputSelector: "input[type=file]",
            linkDisableSelector: "a[data-disable-with]",
            CSRFProtection: function(e) {
                var n = t('meta[name="csrf-token"]').attr("content");
                n && e.setRequestHeader("X-CSRF-Token", n)
            },
            fire: function(e, n, i) {
                var o = t.Event(n);
                return e.trigger(o, i), o.result !== !1
            },
            confirm: function(t) {
                return confirm(t)
            },
            ajax: function(e) {
                return t.ajax(e)
            },
            href: function(t) {
                return t.attr("href")
            },
            handleRemote: function(i) {
                var o, r, s, a, l, c, u, d;
                if (n.fire(i, "ajax:before")) {
                    if (a = i.data("cross-domain"), l = a === e ? null : a, c = i.data("with-credentials") || null, u = i.data("type") || t.ajaxSettings && t.ajaxSettings.dataType, i.is("form")) {
                        o = i.attr("method"), r = i.attr("action"), s = i.serializeArray();
                        var h = i.data("ujs:submit-button");
                        h && (s.push(h), i.data("ujs:submit-button", null))
                    } else i.is(n.inputChangeSelector) ? (o = i.data("method"), r = i.data("url"), s = i.serialize(), i.data("params") && (s = s + "&" + i.data("params"))) : i.is(n.buttonClickSelector) ? (o = i.data("method") || "get", r = i.data("url"), s = i.serialize(), i.data("params") && (s = s + "&" + i.data("params"))) : (o = i.data("method"), r = n.href(i), s = i.data("params") || null);
                    d = {
                        type: o || "GET",
                        data: s,
                        dataType: u,
                        beforeSend: function(t, o) {
                            return o.dataType === e && t.setRequestHeader("accept", "*/*;q=0.5, " + o.accepts.script), n.fire(i, "ajax:beforeSend", [t, o])
                        },
                        success: function(t, e, n) {
                            i.trigger("ajax:success", [t, e, n])
                        },
                        complete: function(t, e) {
                            i.trigger("ajax:complete", [t, e])
                        },
                        error: function(t, e, n) {
                            i.trigger("ajax:error", [t, e, n])
                        },
                        crossDomain: l
                    }, c && (d.xhrFields = {
                        withCredentials: c
                    }), r && (d.url = r);
                    var p = n.ajax(d);
                    return i.trigger("ajax:send", p), p
                }
                return !1
            },
            handleMethod: function(i) {
                var o = n.href(i),
                    r = i.data("method"),
                    s = i.attr("target"),
                    a = t("meta[name=csrf-token]").attr("content"),
                    l = t("meta[name=csrf-param]").attr("content"),
                    c = t('<form method="post" action="' + o + '"></form>'),
                    u = '<input name="_method" value="' + r + '" type="hidden" />';
                l !== e && a !== e && (u += '<input name="' + l + '" value="' + a + '" type="hidden" />'), s && c.attr("target", s), c.hide().append(u).appendTo("body"), c.submit()
            },
            disableFormElements: function(e) {
                e.find(n.disableSelector).each(function() {
                    var e = t(this),
                        n = e.is("button") ? "html" : "val";
                    e.data("ujs:enable-with", e[n]()), e[n](e.data("disable-with")), e.prop("disabled", !0)
                })
            },
            enableFormElements: function(e) {
                e.find(n.enableSelector).each(function() {
                    var e = t(this),
                        n = e.is("button") ? "html" : "val";
                    e.data("ujs:enable-with") && e[n](e.data("ujs:enable-with")), e.prop("disabled", !1)
                })
            },
            allowAction: function(t) {
                var e, i = t.data("confirm"),
                    o = !1;
                return i ? (n.fire(t, "confirm") && (o = n.confirm(i), e = n.fire(t, "confirm:complete", [o])), o && e) : !0
            },
            blankInputs: function(e, n, i) {
                var o, r, s = t(),
                    a = n || "input,textarea",
                    l = e.find(a);
                return l.each(function() {
                    if (o = t(this), r = o.is("input[type=checkbox],input[type=radio]") ? o.is(":checked") : o.val(), !r == !i) {
                        if (o.is("input[type=radio]") && l.filter('input[type=radio]:checked[name="' + o.attr("name") + '"]').length) return !0;
                        s = s.add(o)
                    }
                }), s.length ? s : !1
            },
            nonBlankInputs: function(t, e) {
                return n.blankInputs(t, e, !0)
            },
            stopEverything: function(e) {
                return t(e.target).trigger("ujs:everythingStopped"), e.stopImmediatePropagation(), !1
            },
            disableElement: function(t) {
                t.data("ujs:enable-with", t.html()), t.html(t.data("disable-with")), t.bind("click.railsDisable", function(t) {
                    return n.stopEverything(t)
                })
            },
            enableElement: function(t) {
                t.data("ujs:enable-with") !== e && (t.html(t.data("ujs:enable-with")), t.removeData("ujs:enable-with")), t.unbind("click.railsDisable")
            }
        }, n.fire(i, "rails:attachBindings") && (t.ajaxPrefilter(function(t, e, i) {
            t.crossDomain || n.CSRFProtection(i)
        }), i.delegate(n.linkDisableSelector, "ajax:complete", function() {
            n.enableElement(t(this))
        }), i.delegate(n.linkClickSelector, "click.rails", function(i) {
            var o = t(this),
                r = o.data("method"),
                s = o.data("params");
            if (!n.allowAction(o)) return n.stopEverything(i);
            if (o.is(n.linkDisableSelector) && n.disableElement(o), o.data("remote") !== e) {
                if (!(!i.metaKey && !i.ctrlKey || r && "GET" !== r || s)) return !0;
                var a = n.handleRemote(o);
                return a === !1 ? n.enableElement(o) : a.error(function() {
                    n.enableElement(o)
                }), !1
            }
            return o.data("method") ? (n.handleMethod(o), !1) : void 0
        }), i.delegate(n.buttonClickSelector, "click.rails", function(e) {
            var i = t(this);
            return n.allowAction(i) ? (n.handleRemote(i), !1) : n.stopEverything(e)
        }), i.delegate(n.inputChangeSelector, "change.rails", function(e) {
            var i = t(this);
            return n.allowAction(i) ? (n.handleRemote(i), !1) : n.stopEverything(e)
        }), i.delegate(n.formSubmitSelector, "submit.rails", function(i) {
            var o = t(this),
                r = o.data("remote") !== e,
                s = n.blankInputs(o, n.requiredInputSelector),
                a = n.nonBlankInputs(o, n.fileInputSelector);
            if (!n.allowAction(o)) return n.stopEverything(i);
            if (s && o.attr("novalidate") == e && n.fire(o, "ajax:aborted:required", [s])) return n.stopEverything(i);
            if (r) {
                if (a) {
                    setTimeout(function() {
                        n.disableFormElements(o)
                    }, 13);
                    var l = n.fire(o, "ajax:aborted:file", [a]);
                    return l || setTimeout(function() {
                        n.enableFormElements(o)
                    }, 13), l
                }
                return n.handleRemote(o), !1
            }
            setTimeout(function() {
                n.disableFormElements(o)
            }, 13)
        }), i.delegate(n.formInputClickSelector, "click.rails", function(e) {
            var i = t(this);
            if (!n.allowAction(i)) return n.stopEverything(e);
            var o = i.attr("name"),
                r = o ? {
                    name: o,
                    value: i.val()
                } : null;
            i.closest("form").data("ujs:submit-button", r)
        }), i.delegate(n.formSubmitSelector, "ajax:beforeSend.rails", function(e) {
            this == e.target && n.disableFormElements(t(this))
        }), i.delegate(n.formSubmitSelector, "ajax:complete.rails", function(e) {
            this == e.target && n.enableFormElements(t(this))
        }), t(function() {
            var e = t("meta[name=csrf-token]").attr("content"),
                n = t("meta[name=csrf-param]").attr("content");
            t('form input[name="' + n + '"]').val(e)
        }))
    }(jQuery),
    /*!
     * jQuery UI Core 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/category/ui-core/
     */
    function(t, e) {
        function n(e, n) {
            var o, r, s, a = e.nodeName.toLowerCase();
            return "area" === a ? (o = e.parentNode, r = o.name, e.href && r && "map" === o.nodeName.toLowerCase() ? (s = t("img[usemap=#" + r + "]")[0], !!s && i(s)) : !1) : (/input|select|textarea|button|object/.test(a) ? !e.disabled : "a" === a ? e.href || n : n) && i(e)
        }

        function i(e) {
            return t.expr.filters.visible(e) && !t(e).parents().addBack().filter(function() {
                return "hidden" === t.css(this, "visibility")
            }).length
        }
        var o = 0,
            r = /^ui-id-\d+$/;
        t.ui = t.ui || {}, t.extend(t.ui, {
            version: "1.10.3",
            keyCode: {
                BACKSPACE: 8,
                COMMA: 188,
                DELETE: 46,
                DOWN: 40,
                END: 35,
                ENTER: 13,
                ESCAPE: 27,
                HOME: 36,
                LEFT: 37,
                PAGE_DOWN: 34,
                PAGE_UP: 33,
                PERIOD: 190,
                RIGHT: 39,
                SPACE: 32,
                TAB: 9,
                UP: 38
            }
        }), t.fn.extend({
            focus: function(e) {
                return function(n, i) {
                    return "number" == typeof n ? this.each(function() {
                        var e = this;
                        setTimeout(function() {
                            t(e).focus(), i && i.call(e)
                        }, n)
                    }) : e.apply(this, arguments)
                }
            }(t.fn.focus),
            scrollParent: function() {
                var e;
                return e = t.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function() {
                    return /(relative|absolute|fixed)/.test(t.css(this, "position")) && /(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
                }).eq(0) : this.parents().filter(function() {
                    return /(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
                }).eq(0), /fixed/.test(this.css("position")) || !e.length ? t(this[0].ownerDocument || document) : e
            },
            uniqueId: function() {
                return this.each(function() {
                    this.id || (this.id = "ui-id-" + ++o)
                })
            },
            removeUniqueId: function() {
                return this.each(function() {
                    r.test(this.id) && t(this).removeAttr("id")
                })
            }
        }), t.extend(t.expr[":"], {
            data: t.expr.createPseudo ? t.expr.createPseudo(function(e) {
                return function(n) {
                    return !!t.data(n, e)
                }
            }) : function(e, n, i) {
                return !!t.data(e, i[3])
            },
            focusable: function(e) {
                return n(e, !isNaN(t.attr(e, "tabindex")))
            },
            tabbable: function(e) {
                var i = t.attr(e, "tabindex"),
                    o = isNaN(i);
                return (o || i >= 0) && n(e, !o)
            }
        }), t("<a>").outerWidth(1).jquery || t.each(["Width", "Height"], function(n, i) {
            function o(e, n, i, o) {
                return t.each(r, function() {
                    n -= parseFloat(t.css(e, "padding" + this)) || 0, i && (n -= parseFloat(t.css(e, "border" + this + "Width")) || 0), o && (n -= parseFloat(t.css(e, "margin" + this)) || 0)
                }), n
            }
            var r = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"],
                s = i.toLowerCase(),
                a = {
                    innerWidth: t.fn.innerWidth,
                    innerHeight: t.fn.innerHeight,
                    outerWidth: t.fn.outerWidth,
                    outerHeight: t.fn.outerHeight
                };
            t.fn["inner" + i] = function(n) {
                return n === e ? a["inner" + i].call(this) : this.each(function() {
                    t(this).css(s, o(this, n) + "px")
                })
            }, t.fn["outer" + i] = function(e, n) {
                return "number" != typeof e ? a["outer" + i].call(this, e) : this.each(function() {
                    t(this).css(s, o(this, e, !0, n) + "px")
                })
            }
        }), t.fn.addBack || (t.fn.addBack = function(t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }), t("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (t.fn.removeData = function(e) {
            return function(n) {
                return arguments.length ? e.call(this, t.camelCase(n)) : e.call(this)
            }
        }(t.fn.removeData)), t.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), t.support.selectstart = "onselectstart" in document.createElement("div"), t.fn.extend({
            disableSelection: function() {
                return this.bind((t.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function(t) {
                    t.preventDefault()
                })
            },
            enableSelection: function() {
                return this.unbind(".ui-disableSelection")
            },
            zIndex: function(n) {
                if (n !== e) return this.css("zIndex", n);
                if (this.length)
                    for (var i, o, r = t(this[0]); r.length && r[0] !== document;) {
                        if (i = r.css("position"), ("absolute" === i || "relative" === i || "fixed" === i) && (o = parseInt(r.css("zIndex"), 10), !isNaN(o) && 0 !== o)) return o;
                        r = r.parent()
                    }
                return 0
            }
        }), t.ui.plugin = {
            add: function(e, n, i) {
                var o, r = t.ui[e].prototype;
                for (o in i) r.plugins[o] = r.plugins[o] || [], r.plugins[o].push([n, i[o]])
            },
            call: function(t, e, n, i) {
                var o, r = t.plugins[e];
                if (r && (i || t.element[0].parentNode && 11 !== t.element[0].parentNode.nodeType))
                    for (o = 0; o < r.length; o++) t.options[r[o][0]] && r[o][1].apply(t.element, n)
            }
        }
    }(jQuery),
    /*!
     * jQuery UI Widget 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/jQuery.widget/
     */
    function(t, e) {
        var n = 0,
            i = Array.prototype.slice,
            o = t.cleanData;
        t.cleanData = function(e) {
            for (var n, i = 0; null != (n = e[i]); i++) try {
                t(n).triggerHandler("remove")
            } catch (r) {}
            o(e)
        }, t.widget = function(e, n, i) {
            var o, r, s, a, l = {},
                c = e.split(".")[0];
            return e = e.split(".")[1], o = c + "-" + e, i || (i = n, n = t.Widget), t.expr[":"][o.toLowerCase()] = function(e) {
                return !!t.data(e, o)
            }, t[c] = t[c] || {}, r = t[c][e], s = t[c][e] = function(t, e) {
                return this._createWidget ? void(arguments.length && this._createWidget(t, e)) : new s(t, e)
            }, t.extend(s, r, {
                version: i.version,
                _proto: t.extend({}, i),
                _childConstructors: []
            }), a = new n, a.options = t.widget.extend({}, a.options), t.each(i, function(e, i) {
                return t.isFunction(i) ? void(l[e] = function() {
                    var t = function() {
                            return n.prototype[e].apply(this, arguments)
                        },
                        o = function(t) {
                            return n.prototype[e].apply(this, t)
                        };
                    return function() {
                        var e, n = this._super,
                            r = this._superApply;
                        return this._super = t, this._superApply = o, e = i.apply(this, arguments), this._super = n, this._superApply = r, e
                    }
                }()) : void(l[e] = i)
            }), s.prototype = t.widget.extend(a, {
                widgetEventPrefix: r ? a.widgetEventPrefix || e : e
            }, l, {
                constructor: s,
                namespace: c,
                widgetName: e,
                widgetFullName: o
            }), r ? (t.each(r._childConstructors, function(e, n) {
                var i = n.prototype;
                t.widget(i.namespace + "." + i.widgetName, s, n._proto)
            }), delete r._childConstructors) : n._childConstructors.push(s), t.widget.bridge(e, s), s
        }, t.widget.extend = function(n) {
            for (var o, r, s = i.call(arguments, 1), a = 0, l = s.length; l > a; a++)
                for (o in s[a]) r = s[a][o], s[a].hasOwnProperty(o) && r !== e && (n[o] = t.isPlainObject(r) ? t.isPlainObject(n[o]) ? t.widget.extend({}, n[o], r) : t.widget.extend({}, r) : r);
            return n
        }, t.widget.bridge = function(n, o) {
            var r = o.prototype.widgetFullName || n;
            t.fn[n] = function(s) {
                var a = "string" == typeof s,
                    l = i.call(arguments, 1),
                    c = this;
                return s = !a && l.length ? t.widget.extend.apply(null, [s].concat(l)) : s, this.each(a ? function() {
                    var i, o = t.data(this, r);
                    return "instance" === s ? (c = o, !1) : o ? t.isFunction(o[s]) && "_" !== s.charAt(0) ? (i = o[s].apply(o, l), i !== o && i !== e ? (c = i && i.jquery ? c.pushStack(i.get()) : i, !1) : void 0) : t.error("no such method '" + s + "' for " + n + " widget instance") : t.error("cannot call methods on " + n + " prior to initialization; attempted to call method '" + s + "'")
                } : function() {
                    var e = t.data(this, r);
                    e ? e.option(s || {})._init() : t.data(this, r, new o(s, this))
                }), c
            }
        }, t.Widget = function() {}, t.Widget._childConstructors = [], t.Widget.prototype = {
            widgetName: "widget",
            widgetEventPrefix: "",
            defaultElement: "<div>",
            options: {
                disabled: !1,
                create: null
            },
            _createWidget: function(e, i) {
                i = t(i || this.defaultElement || this)[0], this.element = t(i), this.uuid = n++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = t.widget.extend({}, this.options, this._getCreateOptions(), e), this.bindings = t(), this.hoverable = t(), this.focusable = t(), i !== this && (t.data(i, this.widgetFullName, this), this._on(!0, this.element, {
                    remove: function(t) {
                        t.target === i && this.destroy()
                    }
                }), this.document = t(i.style ? i.ownerDocument : i.document || i), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
            },
            _getCreateOptions: t.noop,
            _getCreateEventData: t.noop,
            _create: t.noop,
            _init: t.noop,
            destroy: function() {
                this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
            },
            _destroy: t.noop,
            widget: function() {
                return this.element
            },
            option: function(n, i) {
                var o, r, s, a = n;
                if (0 === arguments.length) return t.widget.extend({}, this.options);
                if ("string" == typeof n)
                    if (a = {}, o = n.split("."), n = o.shift(), o.length) {
                        for (r = a[n] = t.widget.extend({}, this.options[n]), s = 0; s < o.length - 1; s++) r[o[s]] = r[o[s]] || {}, r = r[o[s]];
                        if (n = o.pop(), i === e) return r[n] === e ? null : r[n];
                        r[n] = i
                    } else {
                        if (i === e) return this.options[n] === e ? null : this.options[n];
                        a[n] = i
                    }
                return this._setOptions(a), this
            },
            _setOptions: function(t) {
                var e;
                for (e in t) this._setOption(e, t[e]);
                return this
            },
            _setOption: function(t, e) {
                return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!e), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
            },
            enable: function() {
                return this._setOptions({
                    disabled: !1
                })
            },
            disable: function() {
                return this._setOptions({
                    disabled: !0
                })
            },
            _on: function(e, n, i) {
                var o, r = this;
                "boolean" != typeof e && (i = n, n = e, e = !1), i ? (n = o = t(n), this.bindings = this.bindings.add(n)) : (i = n, n = this.element, o = this.widget()), t.each(i, function(i, s) {
                    function a() {
                        return e || r.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof s ? r[s] : s).apply(r, arguments) : void 0
                    }
                    "string" != typeof s && (a.guid = s.guid = s.guid || a.guid || t.guid++);
                    var l = i.match(/^(\w+)\s*(.*)$/),
                        c = l[1] + r.eventNamespace,
                        u = l[2];
                    u ? o.delegate(u, c, a) : n.bind(c, a)
                })
            },
            _off: function(t, e) {
                e = (e || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, t.unbind(e).undelegate(e)
            },
            _delay: function(t, e) {
                function n() {
                    return ("string" == typeof t ? i[t] : t).apply(i, arguments)
                }
                var i = this;
                return setTimeout(n, e || 0)
            },
            _hoverable: function(e) {
                this.hoverable = this.hoverable.add(e), this._on(e, {
                    mouseenter: function(e) {
                        t(e.currentTarget).addClass("ui-state-hover")
                    },
                    mouseleave: function(e) {
                        t(e.currentTarget).removeClass("ui-state-hover")
                    }
                })
            },
            _focusable: function(e) {
                this.focusable = this.focusable.add(e), this._on(e, {
                    focusin: function(e) {
                        t(e.currentTarget).addClass("ui-state-focus")
                    },
                    focusout: function(e) {
                        t(e.currentTarget).removeClass("ui-state-focus")
                    }
                })
            },
            _trigger: function(e, n, i) {
                var o, r, s = this.options[e];
                if (i = i || {}, n = t.Event(n), n.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), n.target = this.element[0], r = n.originalEvent)
                    for (o in r) o in n || (n[o] = r[o]);
                return this.element.trigger(n, i), !(t.isFunction(s) && s.apply(this.element[0], [n].concat(i)) === !1 || n.isDefaultPrevented())
            }
        }, t.each({
            show: "fadeIn",
            hide: "fadeOut"
        }, function(e, n) {
            t.Widget.prototype["_" + e] = function(i, o, r) {
                "string" == typeof o && (o = {
                    effect: o
                });
                var s, a = o ? o === !0 || "number" == typeof o ? n : o.effect || n : e;
                o = o || {}, "number" == typeof o && (o = {
                    duration: o
                }), s = !t.isEmptyObject(o), o.complete = r, o.delay && i.delay(o.delay), s && t.effects && t.effects.effect[a] ? i[e](o) : a !== e && i[a] ? i[a](o.duration, o.easing, r) : i.queue(function(n) {
                    t(this)[e](), r && r.call(i[0]), n()
                })
            }
        })
    }(jQuery),
    /*!
     * jQuery UI Mouse 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/mouse/
     *
     * Depends:
     *	jquery.ui.widget.js
     */
    function(t) {
        var e = !1;
        t(document).mouseup(function() {
            e = !1
        }), t.widget("ui.mouse", {
            version: "1.10.3",
            options: {
                cancel: "input,textarea,button,select,option",
                distance: 1,
                delay: 0
            },
            _mouseInit: function() {
                var e = this;
                this.element.bind("mousedown." + this.widgetName, function(t) {
                    return e._mouseDown(t)
                }).bind("click." + this.widgetName, function(n) {
                    return !0 === t.data(n.target, e.widgetName + ".preventClickEvent") ? (t.removeData(n.target, e.widgetName + ".preventClickEvent"), n.stopImmediatePropagation(), !1) : void 0
                }), this.started = !1
            },
            _mouseDestroy: function() {
                this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
            },
            _mouseDown: function(n) {
                if (!e) {
                    this._mouseStarted && this._mouseUp(n), this._mouseDownEvent = n;
                    var i = this,
                        o = 1 === n.which,
                        r = "string" == typeof this.options.cancel && n.target.nodeName ? t(n.target).closest(this.options.cancel).length : !1;
                    return o && !r && this._mouseCapture(n) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
                        i.mouseDelayMet = !0
                    }, this.options.delay)), this._mouseDistanceMet(n) && this._mouseDelayMet(n) && (this._mouseStarted = this._mouseStart(n) !== !1, !this._mouseStarted) ? (n.preventDefault(), !0) : (!0 === t.data(n.target, this.widgetName + ".preventClickEvent") && t.removeData(n.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function(t) {
                        return i._mouseMove(t)
                    }, this._mouseUpDelegate = function(t) {
                        return i._mouseUp(t)
                    }, this.document.bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), n.preventDefault(), e = !0, !0)) : !0
                }
            },
            _mouseMove: function(e) {
                return t.ui.ie && (!document.documentMode || document.documentMode < 9) && !e.button ? this._mouseUp(e) : this._mouseStarted ? (this._mouseDrag(e), e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, e) !== !1, this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted)
            },
            _mouseUp: function(n) {
                return this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, n.target === this._mouseDownEvent.target && t.data(n.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(n)), e = !1, !1
            },
            _mouseDistanceMet: function(t) {
                return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance
            },
            _mouseDelayMet: function() {
                return this.mouseDelayMet
            },
            _mouseStart: function() {},
            _mouseDrag: function() {},
            _mouseStop: function() {},
            _mouseCapture: function() {
                return !0
            }
        })
    }(jQuery),
    /*!
     * jQuery UI Position 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/position/
     */
    function(t, e) {
        function n(t, e, n) {
            return [parseFloat(t[0]) * (p.test(t[0]) ? e / 100 : 1), parseFloat(t[1]) * (p.test(t[1]) ? n / 100 : 1)]
        }

        function i(e, n) {
            return parseInt(t.css(e, n), 10) || 0
        }

        function o(e) {
            var n = e[0];
            return 9 === n.nodeType ? {
                width: e.width(),
                height: e.height(),
                offset: {
                    top: 0,
                    left: 0
                }
            } : t.isWindow(n) ? {
                width: e.width(),
                height: e.height(),
                offset: {
                    top: e.scrollTop(),
                    left: e.scrollLeft()
                }
            } : n.preventDefault ? {
                width: 0,
                height: 0,
                offset: {
                    top: n.pageY,
                    left: n.pageX
                }
            } : {
                width: e.outerWidth(),
                height: e.outerHeight(),
                offset: e.offset()
            }
        }
        t.ui = t.ui || {};
        var r, s = Math.max,
            a = Math.abs,
            l = Math.round,
            c = /left|center|right/,
            u = /top|center|bottom/,
            d = /[\+\-]\d+(\.[\d]+)?%?/,
            h = /^\w+/,
            p = /%$/,
            f = t.fn.position;
        t.position = {
                scrollbarWidth: function() {
                    if (r !== e) return r;
                    var n, i, o = t("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
                        s = o.children()[0];
                    return t("body").append(o), n = s.offsetWidth, o.css("overflow", "scroll"), i = s.offsetWidth, n === i && (i = o[0].clientWidth), o.remove(), r = n - i
                },
                getScrollInfo: function(e) {
                    var n = e.isWindow ? "" : e.element.css("overflow-x"),
                        i = e.isWindow ? "" : e.element.css("overflow-y"),
                        o = "scroll" === n || "auto" === n && e.width < e.element[0].scrollWidth,
                        r = "scroll" === i || "auto" === i && e.height < e.element[0].scrollHeight;
                    return {
                        width: r ? t.position.scrollbarWidth() : 0,
                        height: o ? t.position.scrollbarWidth() : 0
                    }
                },
                getWithinInfo: function(e) {
                    var n = t(e || window),
                        i = t.isWindow(n[0]);
                    return {
                        element: n,
                        isWindow: i,
                        offset: n.offset() || {
                            left: 0,
                            top: 0
                        },
                        scrollLeft: n.scrollLeft(),
                        scrollTop: n.scrollTop(),
                        width: i ? n.width() : n.outerWidth(),
                        height: i ? n.height() : n.outerHeight()
                    }
                }
            }, t.fn.position = function(e) {
                if (!e || !e.of) return f.apply(this, arguments);
                e = t.extend({}, e);
                var r, p, m, g, v, b, y = t(e.of),
                    _ = t.position.getWithinInfo(e.within),
                    w = t.position.getScrollInfo(_),
                    x = (e.collision || "flip").split(" "),
                    C = {};
                return b = o(y), y[0].preventDefault && (e.at = "left top"), p = b.width, m = b.height, g = b.offset, v = t.extend({}, g), t.each(["my", "at"], function() {
                    var t, n, i = (e[this] || "").split(" ");
                    1 === i.length && (i = c.test(i[0]) ? i.concat(["center"]) : u.test(i[0]) ? ["center"].concat(i) : ["center", "center"]), i[0] = c.test(i[0]) ? i[0] : "center", i[1] = u.test(i[1]) ? i[1] : "center", t = d.exec(i[0]), n = d.exec(i[1]), C[this] = [t ? t[0] : 0, n ? n[0] : 0], e[this] = [h.exec(i[0])[0], h.exec(i[1])[0]]
                }), 1 === x.length && (x[1] = x[0]), "right" === e.at[0] ? v.left += p : "center" === e.at[0] && (v.left += p / 2), "bottom" === e.at[1] ? v.top += m : "center" === e.at[1] && (v.top += m / 2), r = n(C.at, p, m), v.left += r[0], v.top += r[1], this.each(function() {
                    var o, c, u = t(this),
                        d = u.outerWidth(),
                        h = u.outerHeight(),
                        f = i(this, "marginLeft"),
                        b = i(this, "marginTop"),
                        k = d + f + i(this, "marginRight") + w.width,
                        T = h + b + i(this, "marginBottom") + w.height,
                        $ = t.extend({}, v),
                        S = n(C.my, u.outerWidth(), u.outerHeight());
                    "right" === e.my[0] ? $.left -= d : "center" === e.my[0] && ($.left -= d / 2), "bottom" === e.my[1] ? $.top -= h : "center" === e.my[1] && ($.top -= h / 2), $.left += S[0], $.top += S[1], t.support.offsetFractions || ($.left = l($.left), $.top = l($.top)), o = {
                        marginLeft: f,
                        marginTop: b
                    }, t.each(["left", "top"], function(n, i) {
                        t.ui.position[x[n]] && t.ui.position[x[n]][i]($, {
                            targetWidth: p,
                            targetHeight: m,
                            elemWidth: d,
                            elemHeight: h,
                            collisionPosition: o,
                            collisionWidth: k,
                            collisionHeight: T,
                            offset: [r[0] + S[0], r[1] + S[1]],
                            my: e.my,
                            at: e.at,
                            within: _,
                            elem: u
                        })
                    }), e.using && (c = function(t) {
                        var n = g.left - $.left,
                            i = n + p - d,
                            o = g.top - $.top,
                            r = o + m - h,
                            l = {
                                target: {
                                    element: y,
                                    left: g.left,
                                    top: g.top,
                                    width: p,
                                    height: m
                                },
                                element: {
                                    element: u,
                                    left: $.left,
                                    top: $.top,
                                    width: d,
                                    height: h
                                },
                                horizontal: 0 > i ? "left" : n > 0 ? "right" : "center",
                                vertical: 0 > r ? "top" : o > 0 ? "bottom" : "middle"
                            };
                        d > p && a(n + i) < p && (l.horizontal = "center"), h > m && a(o + r) < m && (l.vertical = "middle"), l.important = s(a(n), a(i)) > s(a(o), a(r)) ? "horizontal" : "vertical", e.using.call(this, t, l)
                    }), u.offset(t.extend($, {
                        using: c
                    }))
                })
            }, t.ui.position = {
                fit: {
                    left: function(t, e) {
                        var n, i = e.within,
                            o = i.isWindow ? i.scrollLeft : i.offset.left,
                            r = i.width,
                            a = t.left - e.collisionPosition.marginLeft,
                            l = o - a,
                            c = a + e.collisionWidth - r - o;
                        e.collisionWidth > r ? l > 0 && 0 >= c ? (n = t.left + l + e.collisionWidth - r - o, t.left += l - n) : t.left = c > 0 && 0 >= l ? o : l > c ? o + r - e.collisionWidth : o : l > 0 ? t.left += l : c > 0 ? t.left -= c : t.left = s(t.left - a, t.left)
                    },
                    top: function(t, e) {
                        var n, i = e.within,
                            o = i.isWindow ? i.scrollTop : i.offset.top,
                            r = e.within.height,
                            a = t.top - e.collisionPosition.marginTop,
                            l = o - a,
                            c = a + e.collisionHeight - r - o;
                        e.collisionHeight > r ? l > 0 && 0 >= c ? (n = t.top + l + e.collisionHeight - r - o, t.top += l - n) : t.top = c > 0 && 0 >= l ? o : l > c ? o + r - e.collisionHeight : o : l > 0 ? t.top += l : c > 0 ? t.top -= c : t.top = s(t.top - a, t.top)
                    }
                },
                flip: {
                    left: function(t, e) {
                        var n, i, o = e.within,
                            r = o.offset.left + o.scrollLeft,
                            s = o.width,
                            l = o.isWindow ? o.scrollLeft : o.offset.left,
                            c = t.left - e.collisionPosition.marginLeft,
                            u = c - l,
                            d = c + e.collisionWidth - s - l,
                            h = "left" === e.my[0] ? -e.elemWidth : "right" === e.my[0] ? e.elemWidth : 0,
                            p = "left" === e.at[0] ? e.targetWidth : "right" === e.at[0] ? -e.targetWidth : 0,
                            f = -2 * e.offset[0];
                        0 > u ? (n = t.left + h + p + f + e.collisionWidth - s - r, (0 > n || n < a(u)) && (t.left += h + p + f)) : d > 0 && (i = t.left - e.collisionPosition.marginLeft + h + p + f - l, (i > 0 || a(i) < d) && (t.left += h + p + f))
                    },
                    top: function(t, e) {
                        var n, i, o = e.within,
                            r = o.offset.top + o.scrollTop,
                            s = o.height,
                            l = o.isWindow ? o.scrollTop : o.offset.top,
                            c = t.top - e.collisionPosition.marginTop,
                            u = c - l,
                            d = c + e.collisionHeight - s - l,
                            h = "top" === e.my[1],
                            p = h ? -e.elemHeight : "bottom" === e.my[1] ? e.elemHeight : 0,
                            f = "top" === e.at[1] ? e.targetHeight : "bottom" === e.at[1] ? -e.targetHeight : 0,
                            m = -2 * e.offset[1];
                        0 > u ? (i = t.top + p + f + m + e.collisionHeight - s - r, t.top + p + f + m > u && (0 > i || i < a(u)) && (t.top += p + f + m)) : d > 0 && (n = t.top - e.collisionPosition.marginTop + p + f + m - l, t.top + p + f + m > d && (n > 0 || a(n) < d) && (t.top += p + f + m))
                    }
                },
                flipfit: {
                    left: function() {
                        t.ui.position.flip.left.apply(this, arguments), t.ui.position.fit.left.apply(this, arguments)
                    },
                    top: function() {
                        t.ui.position.flip.top.apply(this, arguments), t.ui.position.fit.top.apply(this, arguments)
                    }
                }
            },
            function() {
                var e, n, i, o, r, s = document.getElementsByTagName("body")[0],
                    a = document.createElement("div");
                e = document.createElement(s ? "div" : "body"), i = {
                    visibility: "hidden",
                    width: 0,
                    height: 0,
                    border: 0,
                    margin: 0,
                    background: "none"
                }, s && t.extend(i, {
                    position: "absolute",
                    left: "-1000px",
                    top: "-1000px"
                });
                for (r in i) e.style[r] = i[r];
                e.appendChild(a), n = s || document.documentElement, n.insertBefore(e, n.firstChild), a.style.cssText = "position: absolute; left: 10.7432222px;", o = t(a).offset().left, t.support.offsetFractions = o > 10 && 11 > o, e.innerHTML = "", n.removeChild(e)
            }()
    }(jQuery),
    /*!
     * jQuery UI Draggable 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/draggable/
     *
     * Depends:
     *	jquery.ui.core.js
     *	jquery.ui.mouse.js
     *	jquery.ui.widget.js
     */
    function(t) {
        t.widget("ui.draggable", t.ui.mouse, {
            version: "1.10.3",
            widgetEventPrefix: "drag",
            options: {
                addClasses: !0,
                appendTo: "parent",
                axis: !1,
                connectToSortable: !1,
                containment: !1,
                cursor: "auto",
                cursorAt: !1,
                grid: !1,
                handle: !1,
                helper: "original",
                iframeFix: !1,
                opacity: !1,
                refreshPositions: !1,
                revert: !1,
                revertDuration: 500,
                scope: "default",
                scroll: !0,
                scrollSensitivity: 20,
                scrollSpeed: 20,
                snap: !1,
                snapMode: "both",
                snapTolerance: 20,
                stack: !1,
                zIndex: !1,
                drag: null,
                start: null,
                stop: null
            },
            _create: function() {
                "original" !== this.options.helper || /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative"), this.options.addClasses && this.element.addClass("ui-draggable"), this.options.disabled && this.element.addClass("ui-draggable-disabled"), this._mouseInit()
            },
            _destroy: function() {
                return (this.helper || this.element).is(".ui-draggable-dragging") ? void(this.destroyOnClear = !0) : (this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"), void this._mouseDestroy())
            },
            _mouseCapture: function(e) {
                var n = this.document[0],
                    i = this.options;
                try {
                    n.activeElement && "body" !== n.activeElement.nodeName.toLowerCase() && t(n.activeElement).blur()
                } catch (o) {}
                return this.helper || i.disabled || t(e.target).closest(".ui-resizable-handle").length > 0 ? !1 : (this.handle = this._getHandle(e), this.handle ? (t(i.iframeFix === !0 ? "iframe" : i.iframeFix).each(function() {
                    t("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>").css({
                        width: this.offsetWidth + "px",
                        height: this.offsetHeight + "px",
                        position: "absolute",
                        opacity: "0.001",
                        zIndex: 1e3
                    }).css(t(this).offset()).appendTo("body")
                }), !0) : !1)
            },
            _mouseStart: function(e) {
                var n = this.options;
                return this.helper = this._createHelper(e), this.helper.addClass("ui-draggable-dragging"), this._cacheHelperProportions(), t.ui.ddmanager && (t.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(), this.offsetParent = this.helper.offsetParent(), this.offsetParentCssPosition = this.offsetParent.css("position"), this.offset = this.positionAbs = this.element.offset(), this.offset = {
                    top: this.offset.top - this.margins.top,
                    left: this.offset.left - this.margins.left
                }, this.offset.scroll = !1, t.extend(this.offset, {
                    click: {
                        left: e.pageX - this.offset.left,
                        top: e.pageY - this.offset.top
                    },
                    parent: this._getParentOffset(),
                    relative: this._getRelativeOffset()
                }), this.originalPosition = this.position = this._generatePosition(e), this.originalPageX = e.pageX, this.originalPageY = e.pageY, n.cursorAt && this._adjustOffsetFromHelper(n.cursorAt), this._setContainment(), this._trigger("start", e) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), t.ui.ddmanager && !n.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this._mouseDrag(e, !0), t.ui.ddmanager && t.ui.ddmanager.dragStart(this, e), !0)
            },
            _mouseDrag: function(e, n) {
                if ("fixed" === this.offsetParentCssPosition && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), !n) {
                    var i = this._uiHash();
                    if (this._trigger("drag", e, i) === !1) return this._mouseUp({}), !1;
                    this.position = i.position
                }
                return this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), !1
            },
            _mouseStop: function(e) {
                var n = this,
                    i = !1;
                return t.ui.ddmanager && !this.options.dropBehaviour && (i = t.ui.ddmanager.drop(this, e)), this.dropped && (i = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !i || "valid" === this.options.revert && i || this.options.revert === !0 || t.isFunction(this.options.revert) && this.options.revert.call(this.element, i) ? t(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
                    n._trigger("stop", e) !== !1 && n._clear()
                }) : this._trigger("stop", e) !== !1 && this._clear(), !1
            },
            _mouseUp: function(e) {
                return t("div.ui-draggable-iframeFix").each(function() {
                    this.parentNode.removeChild(this)
                }), t.ui.ddmanager && t.ui.ddmanager.dragStop(this, e), this.element.focus(), t.ui.mouse.prototype._mouseUp.call(this, e)
            },
            cancel: function() {
                return this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear(), this
            },
            _getHandle: function(e) {
                return this.options.handle ? !!t(e.target).closest(this.element.find(this.options.handle)).length : !0
            },
            _createHelper: function(e) {
                var n = this.options,
                    i = t.isFunction(n.helper) ? t(n.helper.apply(this.element[0], [e])) : "clone" === n.helper ? this.element.clone().removeAttr("id") : this.element;
                return i.parents("body").length || i.appendTo("parent" === n.appendTo ? this.element[0].parentNode : n.appendTo), i[0] === this.element[0] || /(fixed|absolute)/.test(i.css("position")) || i.css("position", "absolute"), i
            },
            _adjustOffsetFromHelper: function(e) {
                "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
                    left: +e[0],
                    top: +e[1] || 0
                }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top)
            },
            _getParentOffset: function() {
                var e = this.offsetParent.offset(),
                    n = this.document[0];
                return "absolute" === this.cssPosition && this.scrollParent[0] !== n && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === n.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {
                    top: 0,
                    left: 0
                }), {
                    top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                    left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
                }
            },
            _getRelativeOffset: function() {
                if ("relative" === this.cssPosition) {
                    var t = this.element.position();
                    return {
                        top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                        left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                    }
                }
                return {
                    top: 0,
                    left: 0
                }
            },
            _cacheMargins: function() {
                this.margins = {
                    left: parseInt(this.element.css("marginLeft"), 10) || 0,
                    top: parseInt(this.element.css("marginTop"), 10) || 0,
                    right: parseInt(this.element.css("marginRight"), 10) || 0,
                    bottom: parseInt(this.element.css("marginBottom"), 10) || 0
                }
            },
            _cacheHelperProportions: function() {
                this.helperProportions = {
                    width: this.helper.outerWidth(),
                    height: this.helper.outerHeight()
                }
            },
            _setContainment: function() {
                var e, n, i, o = this.options,
                    r = this.document[0];
                return o.containment ? "window" === o.containment ? void(this.containment = [t(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, t(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, t(window).scrollLeft() + t(window).width() - this.helperProportions.width - this.margins.left, t(window).scrollTop() + (t(window).height() || r.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : "document" === o.containment ? void(this.containment = [0, 0, t(r).width() - this.helperProportions.width - this.margins.left, (t(r).height() || r.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : o.containment.constructor === Array ? void(this.containment = o.containment) : ("parent" === o.containment && (o.containment = this.helper[0].parentNode), n = t(o.containment), i = n[0], void(i && (e = "hidden" !== n.css("overflow"), this.containment = [(parseInt(n.css("borderLeftWidth"), 10) || 0) + (parseInt(n.css("paddingLeft"), 10) || 0), (parseInt(n.css("borderTopWidth"), 10) || 0) + (parseInt(n.css("paddingTop"), 10) || 0), (e ? Math.max(i.scrollWidth, i.offsetWidth) : i.offsetWidth) - (parseInt(n.css("borderRightWidth"), 10) || 0) - (parseInt(n.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (e ? Math.max(i.scrollHeight, i.offsetHeight) : i.offsetHeight) - (parseInt(n.css("borderBottomWidth"), 10) || 0) - (parseInt(n.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relative_container = n))) : void(this.containment = null)
            },
            _convertPositionTo: function(e, n) {
                n || (n = this.position);
                var i = "absolute" === e ? 1 : -1,
                    o = this.document[0],
                    r = "absolute" === this.cssPosition && (this.scrollParent[0] === o || !t.contains(this.scrollParent[0], this.offsetParent[0])),
                    s = r ? this.offsetParent : this.scrollParent,
                    a = r && /(html|body)/i.test(s[0].nodeName);
                return this.offset.scroll || (this.offset.scroll = {
                    top: s.scrollTop(),
                    left: s.scrollLeft()
                }), {
                    top: n.top + this.offset.relative.top * i + this.offset.parent.top * i - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : a ? 0 : this.offset.scroll.top) * i,
                    left: n.left + this.offset.relative.left * i + this.offset.parent.left * i - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : a ? 0 : this.offset.scroll.left) * i
                }
            },
            _generatePosition: function(e) {
                var n, i, o, r, s = this.options,
                    a = this.document[0],
                    l = "absolute" === this.cssPosition && (this.scrollParent[0] === a || !t.contains(this.scrollParent[0], this.offsetParent[0])),
                    c = l ? this.offsetParent : this.scrollParent,
                    u = l && /(html|body)/i.test(c[0].nodeName),
                    d = e.pageX,
                    h = e.pageY;
                return this.offset.scroll || (this.offset.scroll = {
                    top: c.scrollTop(),
                    left: c.scrollLeft()
                }), this.originalPosition && (this.containment && (this.relative_container ? (i = this.relative_container.offset(), n = [this.containment[0] + i.left, this.containment[1] + i.top, this.containment[2] + i.left, this.containment[3] + i.top]) : n = this.containment, e.pageX - this.offset.click.left < n[0] && (d = n[0] + this.offset.click.left), e.pageY - this.offset.click.top < n[1] && (h = n[1] + this.offset.click.top), e.pageX - this.offset.click.left > n[2] && (d = n[2] + this.offset.click.left), e.pageY - this.offset.click.top > n[3] && (h = n[3] + this.offset.click.top)), s.grid && (o = s.grid[1] ? this.originalPageY + Math.round((h - this.originalPageY) / s.grid[1]) * s.grid[1] : this.originalPageY, h = n ? o - this.offset.click.top >= n[1] || o - this.offset.click.top > n[3] ? o : o - this.offset.click.top >= n[1] ? o - s.grid[1] : o + s.grid[1] : o, r = s.grid[0] ? this.originalPageX + Math.round((d - this.originalPageX) / s.grid[0]) * s.grid[0] : this.originalPageX, d = n ? r - this.offset.click.left >= n[0] || r - this.offset.click.left > n[2] ? r : r - this.offset.click.left >= n[0] ? r - s.grid[0] : r + s.grid[0] : r)), {
                    top: h - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : u ? 0 : this.offset.scroll.top),
                    left: d - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : u ? 0 : this.offset.scroll.left)
                }
            },
            _clear: function() {
                this.helper.removeClass("ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy()
            },
            _trigger: function(e, n, i) {
                return i = i || this._uiHash(), t.ui.plugin.call(this, e, [n, i, this], !0), "drag" === e && (this.positionAbs = this._convertPositionTo("absolute")), t.Widget.prototype._trigger.call(this, e, n, i)
            },
            plugins: {},
            _uiHash: function() {
                return {
                    helper: this.helper,
                    position: this.position,
                    originalPosition: this.originalPosition,
                    offset: this.positionAbs
                }
            }
        }), t.ui.plugin.add("draggable", "connectToSortable", {
            start: function(e, n, i) {
                var o = i.options,
                    r = t.extend({}, n, {
                        item: i.element
                    });
                i.sortables = [], t(o.connectToSortable).each(function() {
                    var n = t(this).sortable("instance");
                    n && !n.options.disabled && (i.sortables.push({
                        instance: n,
                        shouldRevert: n.options.revert
                    }), n.refreshPositions(), n._trigger("activate", e, r))
                })
            },
            stop: function(e, n, i) {
                var o = t.extend({}, n, {
                    item: i.element
                });
                t.each(i.sortables, function() {
                    this.instance.isOver ? (this.instance.isOver = 0, i.cancelHelperRemoval = !0, this.instance.cancelHelperRemoval = !1, this.shouldRevert && (this.instance.options.revert = this.shouldRevert), this.instance._mouseStop(e), this.instance.options.helper = this.instance.options._helper, "original" === i.options.helper && this.instance.currentItem.css({
                        top: "auto",
                        left: "auto"
                    })) : (this.instance.cancelHelperRemoval = !1, this.instance._trigger("deactivate", e, o))
                })
            },
            drag: function(e, n, i) {
                var o = this;
                t.each(i.sortables, function() {
                    var r = !1,
                        s = this;
                    this.instance.positionAbs = i.positionAbs, this.instance.helperProportions = i.helperProportions, this.instance.offset.click = i.offset.click, this.instance._intersectsWith(this.instance.containerCache) && (r = !0, t.each(i.sortables, function() {
                        return this.instance.positionAbs = i.positionAbs, this.instance.helperProportions = i.helperProportions, this.instance.offset.click = i.offset.click, this !== s && this.instance._intersectsWith(this.instance.containerCache) && t.contains(s.instance.element[0], this.instance.element[0]) && (r = !1), r
                    })), r ? (this.instance.isOver || (this.instance.isOver = 1, this.instance.currentItem = t(o).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item", !0), this.instance.options._helper = this.instance.options.helper, this.instance.options.helper = function() {
                        return n.helper[0]
                    }, e.target = this.instance.currentItem[0], this.instance._mouseCapture(e, !0), this.instance._mouseStart(e, !0, !0), this.instance.offset.click.top = i.offset.click.top, this.instance.offset.click.left = i.offset.click.left, this.instance.offset.parent.left -= i.offset.parent.left - this.instance.offset.parent.left, this.instance.offset.parent.top -= i.offset.parent.top - this.instance.offset.parent.top, i._trigger("toSortable", e), i.dropped = this.instance.element, i.currentItem = i.element, this.instance.fromOutside = i), this.instance.currentItem && this.instance._mouseDrag(e)) : this.instance.isOver && (this.instance.isOver = 0, this.instance.cancelHelperRemoval = !0, this.instance.options.revert = !1, this.instance._trigger("out", e, this.instance._uiHash(this.instance)), this.instance._mouseStop(e, !0), this.instance.options.helper = this.instance.options._helper, this.instance.currentItem.remove(), this.instance.placeholder && this.instance.placeholder.remove(), i._trigger("fromSortable", e), i.dropped = !1)
                })
            }
        }), t.ui.plugin.add("draggable", "cursor", {
            start: function(e, n, i) {
                var o = t("body"),
                    r = i.options;
                o.css("cursor") && (r._cursor = o.css("cursor")), o.css("cursor", r.cursor)
            },
            stop: function(e, n, i) {
                var o = i.options;
                o._cursor && t("body").css("cursor", o._cursor)
            }
        }), t.ui.plugin.add("draggable", "opacity", {
            start: function(e, n, i) {
                var o = t(n.helper),
                    r = i.options;
                o.css("opacity") && (r._opacity = o.css("opacity")), o.css("opacity", r.opacity)
            },
            stop: function(e, n, i) {
                var o = i.options;
                o._opacity && t(n.helper).css("opacity", o._opacity)
            }
        }), t.ui.plugin.add("draggable", "scroll", {
            start: function(t, e, n) {
                n.scrollParent[0] !== n.document[0] && "HTML" !== n.scrollParent[0].tagName && (n.overflowOffset = n.scrollParent.offset())
            },
            drag: function(e, n, i) {
                var o = i.options,
                    r = !1,
                    s = i.document[0];
                i.scrollParent[0] !== s && "HTML" !== i.scrollParent[0].tagName ? (o.axis && "x" === o.axis || (i.overflowOffset.top + i.scrollParent[0].offsetHeight - e.pageY < o.scrollSensitivity ? i.scrollParent[0].scrollTop = r = i.scrollParent[0].scrollTop + o.scrollSpeed : e.pageY - i.overflowOffset.top < o.scrollSensitivity && (i.scrollParent[0].scrollTop = r = i.scrollParent[0].scrollTop - o.scrollSpeed)), o.axis && "y" === o.axis || (i.overflowOffset.left + i.scrollParent[0].offsetWidth - e.pageX < o.scrollSensitivity ? i.scrollParent[0].scrollLeft = r = i.scrollParent[0].scrollLeft + o.scrollSpeed : e.pageX - i.overflowOffset.left < o.scrollSensitivity && (i.scrollParent[0].scrollLeft = r = i.scrollParent[0].scrollLeft - o.scrollSpeed))) : (o.axis && "x" === o.axis || (e.pageY - t(s).scrollTop() < o.scrollSensitivity ? r = t(s).scrollTop(t(s).scrollTop() - o.scrollSpeed) : t(window).height() - (e.pageY - t(s).scrollTop()) < o.scrollSensitivity && (r = t(s).scrollTop(t(s).scrollTop() + o.scrollSpeed))), o.axis && "y" === o.axis || (e.pageX - t(s).scrollLeft() < o.scrollSensitivity ? r = t(s).scrollLeft(t(s).scrollLeft() - o.scrollSpeed) : t(window).width() - (e.pageX - t(s).scrollLeft()) < o.scrollSensitivity && (r = t(s).scrollLeft(t(s).scrollLeft() + o.scrollSpeed)))), r !== !1 && t.ui.ddmanager && !o.dropBehaviour && t.ui.ddmanager.prepareOffsets(i, e)
            }
        }), t.ui.plugin.add("draggable", "snap", {
            start: function(e, n, i) {
                var o = i.options;
                i.snapElements = [], t(o.snap.constructor !== String ? o.snap.items || ":data(ui-draggable)" : o.snap).each(function() {
                    var e = t(this),
                        n = e.offset();
                    this !== i.element[0] && i.snapElements.push({
                        item: this,
                        width: e.outerWidth(),
                        height: e.outerHeight(),
                        top: n.top,
                        left: n.left
                    })
                })
            },
            drag: function(e, n, i) {
                var o, r, s, a, l, c, u, d, h, p, f = i.options,
                    m = f.snapTolerance,
                    g = n.offset.left,
                    v = g + i.helperProportions.width,
                    b = n.offset.top,
                    y = b + i.helperProportions.height;
                for (h = i.snapElements.length - 1; h >= 0; h--) l = i.snapElements[h].left, c = l + i.snapElements[h].width, u = i.snapElements[h].top, d = u + i.snapElements[h].height, l - m > v || g > c + m || u - m > y || b > d + m || !t.contains(i.snapElements[h].item.ownerDocument, i.snapElements[h].item) ? (i.snapElements[h].snapping && i.options.snap.release && i.options.snap.release.call(i.element, e, t.extend(i._uiHash(), {
                    snapItem: i.snapElements[h].item
                })), i.snapElements[h].snapping = !1) : ("inner" !== f.snapMode && (o = Math.abs(u - y) <= m, r = Math.abs(d - b) <= m, s = Math.abs(l - v) <= m, a = Math.abs(c - g) <= m, o && (n.position.top = i._convertPositionTo("relative", {
                    top: u - i.helperProportions.height,
                    left: 0
                }).top - i.margins.top), r && (n.position.top = i._convertPositionTo("relative", {
                    top: d,
                    left: 0
                }).top - i.margins.top), s && (n.position.left = i._convertPositionTo("relative", {
                    top: 0,
                    left: l - i.helperProportions.width
                }).left - i.margins.left), a && (n.position.left = i._convertPositionTo("relative", {
                    top: 0,
                    left: c
                }).left - i.margins.left)), p = o || r || s || a, "outer" !== f.snapMode && (o = Math.abs(u - b) <= m, r = Math.abs(d - y) <= m, s = Math.abs(l - g) <= m, a = Math.abs(c - v) <= m, o && (n.position.top = i._convertPositionTo("relative", {
                    top: u,
                    left: 0
                }).top - i.margins.top), r && (n.position.top = i._convertPositionTo("relative", {
                    top: d - i.helperProportions.height,
                    left: 0
                }).top - i.margins.top), s && (n.position.left = i._convertPositionTo("relative", {
                    top: 0,
                    left: l
                }).left - i.margins.left), a && (n.position.left = i._convertPositionTo("relative", {
                    top: 0,
                    left: c - i.helperProportions.width
                }).left - i.margins.left)), !i.snapElements[h].snapping && (o || r || s || a || p) && i.options.snap.snap && i.options.snap.snap.call(i.element, e, t.extend(i._uiHash(), {
                    snapItem: i.snapElements[h].item
                })), i.snapElements[h].snapping = o || r || s || a || p)
            }
        }), t.ui.plugin.add("draggable", "stack", {
            start: function(e, n, i) {
                var o, r = i.options,
                    s = t.makeArray(t(r.stack)).sort(function(e, n) {
                        return (parseInt(t(e).css("zIndex"), 10) || 0) - (parseInt(t(n).css("zIndex"), 10) || 0)
                    });
                s.length && (o = parseInt(t(s[0]).css("zIndex"), 10) || 0, t(s).each(function(e) {
                    t(this).css("zIndex", o + e)
                }), this.css("zIndex", o + s.length))
            }
        }), t.ui.plugin.add("draggable", "zIndex", {
            start: function(e, n, i) {
                var o = t(n.helper),
                    r = i.options;
                o.css("zIndex") && (r._zIndex = o.css("zIndex")), o.css("zIndex", r.zIndex)
            },
            stop: function(e, n, i) {
                var o = i.options;
                o._zIndex && t(n.helper).css("zIndex", o._zIndex)
            }
        })
    }(jQuery),
    /*!
     * jQuery UI Droppable 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/droppable/
     *
     * Depends:
     *	jquery.ui.core.js
     *	jquery.ui.widget.js
     *	jquery.ui.mouse.js
     *	jquery.ui.draggable.js
     */
    function(t) {
        function e(t, e, n) {
            return t >= e && e + n > t
        }
        t.widget("ui.droppable", {
            version: "1.10.3",
            widgetEventPrefix: "drop",
            options: {
                accept: "*",
                activeClass: !1,
                addClasses: !0,
                greedy: !1,
                hoverClass: !1,
                scope: "default",
                tolerance: "intersect",
                activate: null,
                deactivate: null,
                drop: null,
                out: null,
                over: null
            },
            _create: function() {
                var e, n = this.options,
                    i = n.accept;
                this.isover = !1, this.isout = !0, this.accept = t.isFunction(i) ? i : function(t) {
                    return t.is(i)
                }, this.proportions = function() {
                    return arguments.length ? void(e = arguments[0]) : e ? e : e = {
                        width: this.element[0].offsetWidth,
                        height: this.element[0].offsetHeight
                    }
                }, t.ui.ddmanager.droppables[n.scope] = t.ui.ddmanager.droppables[n.scope] || [], t.ui.ddmanager.droppables[n.scope].push(this), n.addClasses && this.element.addClass("ui-droppable")
            },
            _destroy: function() {
                for (var e = 0, n = t.ui.ddmanager.droppables[this.options.scope]; e < n.length; e++) n[e] === this && n.splice(e, 1);
                this.element.removeClass("ui-droppable ui-droppable-disabled")
            },
            _setOption: function(e, n) {
                "accept" === e && (this.accept = t.isFunction(n) ? n : function(t) {
                    return t.is(n)
                }), this._super(e, n)
            },
            _activate: function(e) {
                var n = t.ui.ddmanager.current;
                this.options.activeClass && this.element.addClass(this.options.activeClass), n && this._trigger("activate", e, this.ui(n))
            },
            _deactivate: function(e) {
                var n = t.ui.ddmanager.current;
                this.options.activeClass && this.element.removeClass(this.options.activeClass), n && this._trigger("deactivate", e, this.ui(n))
            },
            _over: function(e) {
                var n = t.ui.ddmanager.current;
                n && (n.currentItem || n.element)[0] !== this.element[0] && this.accept.call(this.element[0], n.currentItem || n.element) && (this.options.hoverClass && this.element.addClass(this.options.hoverClass), this._trigger("over", e, this.ui(n)))
            },
            _out: function(e) {
                var n = t.ui.ddmanager.current;
                n && (n.currentItem || n.element)[0] !== this.element[0] && this.accept.call(this.element[0], n.currentItem || n.element) && (this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("out", e, this.ui(n)))
            },
            _drop: function(e, n) {
                var i = n || t.ui.ddmanager.current,
                    o = !1;
                return i && (i.currentItem || i.element)[0] !== this.element[0] ? (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function() {
                    var e = t(this).droppable("instance");
                    return e.options.greedy && !e.options.disabled && e.options.scope === i.options.scope && e.accept.call(e.element[0], i.currentItem || i.element) && t.ui.intersect(i, t.extend(e, {
                        offset: e.element.offset()
                    }), e.options.tolerance) ? (o = !0, !1) : void 0
                }), o ? !1 : this.accept.call(this.element[0], i.currentItem || i.element) ? (this.options.activeClass && this.element.removeClass(this.options.activeClass), this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("drop", e, this.ui(i)), this.element) : !1) : !1
            },
            ui: function(t) {
                return {
                    draggable: t.currentItem || t.element,
                    helper: t.helper,
                    position: t.position,
                    offset: t.positionAbs
                }
            }
        }), t.ui.intersect = function(t, n, i) {
            if (!n.offset) return !1;
            var o, r, s = (t.positionAbs || t.position.absolute).left,
                a = (t.positionAbs || t.position.absolute).top,
                l = s + t.helperProportions.width,
                c = a + t.helperProportions.height,
                u = n.offset.left,
                d = n.offset.top,
                h = u + n.proportions().width,
                p = d + n.proportions().height;
            switch (i) {
                case "fit":
                    return s >= u && h >= l && a >= d && p >= c;
                case "intersect":
                    return u < s + t.helperProportions.width / 2 && l - t.helperProportions.width / 2 < h && d < a + t.helperProportions.height / 2 && c - t.helperProportions.height / 2 < p;
                case "pointer":
                    return o = (t.positionAbs || t.position.absolute).left + (t.clickOffset || t.offset.click).left, r = (t.positionAbs || t.position.absolute).top + (t.clickOffset || t.offset.click).top, e(r, d, n.proportions().height) && e(o, u, n.proportions().width);
                case "touch":
                    return (a >= d && p >= a || c >= d && p >= c || d > a && c > p) && (s >= u && h >= s || l >= u && h >= l || u > s && l > h);
                default:
                    return !1
            }
        }, t.ui.ddmanager = {
            current: null,
            droppables: {
                "default": []
            },
            prepareOffsets: function(e, n) {
                var i, o, r = t.ui.ddmanager.droppables[e.options.scope] || [],
                    s = n ? n.type : null,
                    a = (e.currentItem || e.element).find(":data(ui-droppable)").addBack();
                t: for (i = 0; i < r.length; i++)
                    if (!(r[i].options.disabled || e && !r[i].accept.call(r[i].element[0], e.currentItem || e.element))) {
                        for (o = 0; o < a.length; o++)
                            if (a[o] === r[i].element[0]) {
                                r[i].proportions().height = 0;
                                continue t
                            }
                        r[i].visible = "none" !== r[i].element.css("display"), r[i].visible && ("mousedown" === s && r[i]._activate.call(r[i], n), r[i].offset = r[i].element.offset(), r[i].proportions({
                            width: r[i].element[0].offsetWidth,
                            height: r[i].element[0].offsetHeight
                        }))
                    }
            },
            drop: function(e, n) {
                var i = !1;
                return t.each((t.ui.ddmanager.droppables[e.options.scope] || []).slice(), function() {
                    this.options && (!this.options.disabled && this.visible && t.ui.intersect(e, this, this.options.tolerance) && (i = this._drop.call(this, n) || i), !this.options.disabled && this.visible && this.accept.call(this.element[0], e.currentItem || e.element) && (this.isout = !0, this.isover = !1, this._deactivate.call(this, n)))
                }), i
            },
            dragStart: function(e, n) {
                e.element.parentsUntil("body").bind("scroll.droppable", function() {
                    e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, n)
                })
            },
            drag: function(e, n) {
                e.options.refreshPositions && t.ui.ddmanager.prepareOffsets(e, n), t.each(t.ui.ddmanager.droppables[e.options.scope] || [], function() {
                    if (!this.options.disabled && !this.greedyChild && this.visible) {
                        var i, o, r, s = t.ui.intersect(e, this, this.options.tolerance),
                            a = !s && this.isover ? "isout" : s && !this.isover ? "isover" : null;
                        a && (this.options.greedy && (o = this.options.scope, r = this.element.parents(":data(ui-droppable)").filter(function() {
                            return t(this).droppable("instance").options.scope === o
                        }), r.length && (i = t(r[0]).droppable("instance"), i.greedyChild = "isover" === a)), i && "isover" === a && (i.isover = !1, i.isout = !0, i._out.call(i, n)), this[a] = !0, this["isout" === a ? "isover" : "isout"] = !1, this["isover" === a ? "_over" : "_out"].call(this, n), i && "isout" === a && (i.isout = !1, i.isover = !0, i._over.call(i, n)))
                    }
                })
            },
            dragStop: function(e, n) {
                e.element.parentsUntil("body").unbind("scroll.droppable"), e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, n)
            }
        }
    }(jQuery),
    /*!
     * jQuery UI Resizable 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/resizable/
     *
     * Depends:
     *	jquery.ui.core.js
     *	jquery.ui.mouse.js
     *	jquery.ui.widget.js
     */
    function(t) {
        function e(t) {
            return parseInt(t, 10) || 0
        }

        function n(t) {
            return !isNaN(parseInt(t, 10))
        }

        function i(e, n) {
            if ("hidden" === t(e).css("overflow")) return !1;
            var i = n && "left" === n ? "scrollLeft" : "scrollTop",
                o = !1;
            return e[i] > 0 ? !0 : (e[i] = 1, o = e[i] > 0, e[i] = 0, o)
        }
        t.widget("ui.resizable", t.ui.mouse, {
            version: "1.10.3",
            widgetEventPrefix: "resize",
            options: {
                alsoResize: !1,
                animate: !1,
                animateDuration: "slow",
                animateEasing: "swing",
                aspectRatio: !1,
                autoHide: !1,
                containment: !1,
                ghost: !1,
                grid: !1,
                handles: "e,s,se",
                helper: !1,
                maxHeight: null,
                maxWidth: null,
                minHeight: 10,
                minWidth: 10,
                zIndex: 90,
                resize: null,
                start: null,
                stop: null
            },
            _create: function() {
                var e, n, i, o, r, s = this,
                    a = this.options;
                if (this.element.addClass("ui-resizable"), t.extend(this, {
                        _aspectRatio: !!a.aspectRatio,
                        aspectRatio: a.aspectRatio,
                        originalElement: this.element,
                        _proportionallyResizeElements: [],
                        _helper: a.helper || a.ghost || a.animate ? a.helper || "ui-resizable-helper" : null
                    }), this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i) && (this.element.wrap(t("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
                        position: this.element.css("position"),
                        width: this.element.outerWidth(),
                        height: this.element.outerHeight(),
                        top: this.element.css("top"),
                        left: this.element.css("left")
                    })), this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")), this.elementIsWrapper = !0, this.element.css({
                        marginLeft: this.originalElement.css("marginLeft"),
                        marginTop: this.originalElement.css("marginTop"),
                        marginRight: this.originalElement.css("marginRight"),
                        marginBottom: this.originalElement.css("marginBottom")
                    }), this.originalElement.css({
                        marginLeft: 0,
                        marginTop: 0,
                        marginRight: 0,
                        marginBottom: 0
                    }), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
                        position: "static",
                        zoom: 1,
                        display: "block"
                    })), this.originalElement.css({
                        margin: this.originalElement.css("margin")
                    }), this._proportionallyResize()), this.handles = a.handles || (t(".ui-resizable-handle", this.element).length ? {
                        n: ".ui-resizable-n",
                        e: ".ui-resizable-e",
                        s: ".ui-resizable-s",
                        w: ".ui-resizable-w",
                        se: ".ui-resizable-se",
                        sw: ".ui-resizable-sw",
                        ne: ".ui-resizable-ne",
                        nw: ".ui-resizable-nw"
                    } : "e,s,se"), this.handles.constructor === String)
                    for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), e = this.handles.split(","), this.handles = {}, n = 0; n < e.length; n++) i = t.trim(e[n]), r = "ui-resizable-" + i, o = t("<div class='ui-resizable-handle " + r + "'></div>"), o.css({
                        zIndex: a.zIndex
                    }), "se" === i && o.addClass("ui-icon ui-icon-gripsmall-diagonal-se"), this.handles[i] = ".ui-resizable-" + i, this.element.append(o);
                this._renderAxis = function(e) {
                    var n, i, o, r;
                    e = e || this.element;
                    for (n in this.handles) this.handles[n].constructor === String && (this.handles[n] = t(this.handles[n], this.element).show()), this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i) && (i = t(this.handles[n], this.element), r = /sw|ne|nw|se|n|s/.test(n) ? i.outerHeight() : i.outerWidth(), o = ["padding", /ne|nw|n/.test(n) ? "Top" : /se|sw|s/.test(n) ? "Bottom" : /^e$/.test(n) ? "Right" : "Left"].join(""), e.css(o, r), this._proportionallyResize()), t(this.handles[n]).length
                }, this._renderAxis(this.element), this._handles = t(".ui-resizable-handle", this.element).disableSelection(), this._handles.mouseover(function() {
                    s.resizing || (this.className && (o = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), s.axis = o && o[1] ? o[1] : "se")
                }), a.autoHide && (this._handles.hide(), t(this.element).addClass("ui-resizable-autohide").mouseenter(function() {
                    a.disabled || (t(this).removeClass("ui-resizable-autohide"), s._handles.show())
                }).mouseleave(function() {
                    a.disabled || s.resizing || (t(this).addClass("ui-resizable-autohide"), s._handles.hide())
                })), this._mouseInit()
            },
            _destroy: function() {
                this._mouseDestroy();
                var e, n = function(e) {
                    t(e).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()
                };
                return this.elementIsWrapper && (n(this.element), e = this.element, this.originalElement.css({
                    position: e.css("position"),
                    width: e.outerWidth(),
                    height: e.outerHeight(),
                    top: e.css("top"),
                    left: e.css("left")
                }).insertAfter(e), e.remove()), this.originalElement.css("resize", this.originalResizeStyle), n(this.originalElement), this
            },
            _mouseCapture: function(e) {
                var n, i, o = !1;
                for (n in this.handles) i = t(this.handles[n])[0], (i === e.target || t.contains(i, e.target)) && (o = !0);
                return !this.options.disabled && o
            },
            _mouseStart: function(n) {
                var i, o, r, s = this.options,
                    a = this.element.position(),
                    l = this.element;
                return this.resizing = !0, /absolute/.test(l.css("position")) ? l.css({
                    position: "absolute",
                    top: l.css("top"),
                    left: l.css("left")
                }) : l.is(".ui-draggable") && l.css({
                    position: "absolute",
                    top: a.top,
                    left: a.left
                }), this._renderProxy(), i = e(this.helper.css("left")), o = e(this.helper.css("top")), s.containment && (i += t(s.containment).scrollLeft() || 0, o += t(s.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
                    left: i,
                    top: o
                }, this.size = this._helper ? {
                    width: l.outerWidth(),
                    height: l.outerHeight()
                } : {
                    width: l.width(),
                    height: l.height()
                }, this.originalSize = this._helper ? {
                    width: l.outerWidth(),
                    height: l.outerHeight()
                } : {
                    width: l.width(),
                    height: l.height()
                }, this.originalPosition = {
                    left: i,
                    top: o
                }, this.sizeDiff = {
                    width: l.outerWidth() - l.width(),
                    height: l.outerHeight() - l.height()
                }, this.originalMousePosition = {
                    left: n.pageX,
                    top: n.pageY
                }, this.aspectRatio = "number" == typeof s.aspectRatio ? s.aspectRatio : this.originalSize.width / this.originalSize.height || 1, r = t(".ui-resizable-" + this.axis).css("cursor"), t("body").css("cursor", "auto" === r ? this.axis + "-resize" : r), l.addClass("ui-resizable-resizing"), this._propagate("start", n), !0
            },
            _mouseDrag: function(e) {
                var n, i = this.helper,
                    o = {},
                    r = this.originalMousePosition,
                    s = this.axis,
                    a = this.position.top,
                    l = this.position.left,
                    c = this.size.width,
                    u = this.size.height,
                    d = e.pageX - r.left || 0,
                    h = e.pageY - r.top || 0,
                    p = this._change[s];
                return p ? (n = p.apply(this, [e, d, h]), this._updateVirtualBoundaries(e.shiftKey), (this._aspectRatio || e.shiftKey) && (n = this._updateRatio(n, e)), n = this._respectSize(n, e), this._updateCache(n), this._propagate("resize", e), this.position.top !== a && (o.top = this.position.top + "px"), this.position.left !== l && (o.left = this.position.left + "px"), this.size.width !== c && (o.width = this.size.width + "px"), this.size.height !== u && (o.height = this.size.height + "px"), i.css(o), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), t.isEmptyObject(o) || this._trigger("resize", e, this.ui()), !1) : !1
            },
            _mouseStop: function(e) {
                this.resizing = !1;
                var n, o, r, s, a, l, c, u = this.options,
                    d = this;
                return this._helper && (n = this._proportionallyResizeElements, o = n.length && /textarea/i.test(n[0].nodeName), r = o && i(n[0], "left") ? 0 : d.sizeDiff.height, s = o ? 0 : d.sizeDiff.width, a = {
                    width: d.helper.width() - s,
                    height: d.helper.height() - r
                }, l = parseInt(d.element.css("left"), 10) + (d.position.left - d.originalPosition.left) || null, c = parseInt(d.element.css("top"), 10) + (d.position.top - d.originalPosition.top) || null, u.animate || this.element.css(t.extend(a, {
                    top: c,
                    left: l
                })), d.helper.height(d.size.height), d.helper.width(d.size.width), this._helper && !u.animate && this._proportionallyResize()), t("body").css("cursor", "auto"), this.element.removeClass("ui-resizable-resizing"), this._propagate("stop", e), this._helper && this.helper.remove(), !1
            },
            _updateVirtualBoundaries: function(t) {
                var e, i, o, r, s, a = this.options;
                s = {
                    minWidth: n(a.minWidth) ? a.minWidth : 0,
                    maxWidth: n(a.maxWidth) ? a.maxWidth : 1 / 0,
                    minHeight: n(a.minHeight) ? a.minHeight : 0,
                    maxHeight: n(a.maxHeight) ? a.maxHeight : 1 / 0
                }, (this._aspectRatio || t) && (e = s.minHeight * this.aspectRatio, o = s.minWidth / this.aspectRatio, i = s.maxHeight * this.aspectRatio, r = s.maxWidth / this.aspectRatio, e > s.minWidth && (s.minWidth = e), o > s.minHeight && (s.minHeight = o), i < s.maxWidth && (s.maxWidth = i), r < s.maxHeight && (s.maxHeight = r)), this._vBoundaries = s
            },
            _updateCache: function(t) {
                this.offset = this.helper.offset(), n(t.left) && (this.position.left = t.left), n(t.top) && (this.position.top = t.top), n(t.height) && (this.size.height = t.height), n(t.width) && (this.size.width = t.width)
            },
            _updateRatio: function(t) {
                var e = this.position,
                    i = this.size,
                    o = this.axis;
                return n(t.height) ? t.width = t.height * this.aspectRatio : n(t.width) && (t.height = t.width / this.aspectRatio), "sw" === o && (t.left = e.left + (i.width - t.width), t.top = null), "nw" === o && (t.top = e.top + (i.height - t.height), t.left = e.left + (i.width - t.width)), t
            },
            _respectSize: function(t) {
                var e = this._vBoundaries,
                    i = this.axis,
                    o = n(t.width) && e.maxWidth && e.maxWidth < t.width,
                    r = n(t.height) && e.maxHeight && e.maxHeight < t.height,
                    s = n(t.width) && e.minWidth && e.minWidth > t.width,
                    a = n(t.height) && e.minHeight && e.minHeight > t.height,
                    l = this.originalPosition.left + this.originalSize.width,
                    c = this.position.top + this.size.height,
                    u = /sw|nw|w/.test(i),
                    d = /nw|ne|n/.test(i);
                return s && (t.width = e.minWidth), a && (t.height = e.minHeight), o && (t.width = e.maxWidth), r && (t.height = e.maxHeight), s && u && (t.left = l - e.minWidth), o && u && (t.left = l - e.maxWidth), a && d && (t.top = c - e.minHeight), r && d && (t.top = c - e.maxHeight), t.width || t.height || t.left || !t.top ? t.width || t.height || t.top || !t.left || (t.left = null) : t.top = null, t
            },
            _proportionallyResize: function() {
                if (this._proportionallyResizeElements.length) {
                    var t, e, n, i, o, r = this.helper || this.element;
                    for (t = 0; t < this._proportionallyResizeElements.length; t++) {
                        if (o = this._proportionallyResizeElements[t], !this.borderDif)
                            for (this.borderDif = [], n = [o.css("borderTopWidth"), o.css("borderRightWidth"), o.css("borderBottomWidth"), o.css("borderLeftWidth")], i = [o.css("paddingTop"), o.css("paddingRight"), o.css("paddingBottom"), o.css("paddingLeft")], e = 0; e < n.length; e++) this.borderDif[e] = (parseInt(n[e], 10) || 0) + (parseInt(i[e], 10) || 0);
                        o.css({
                            height: r.height() - this.borderDif[0] - this.borderDif[2] || 0,
                            width: r.width() - this.borderDif[1] - this.borderDif[3] || 0
                        })
                    }
                }
            },
            _renderProxy: function() {
                var e = this.element,
                    n = this.options;
                this.elementOffset = e.offset(), this._helper ? (this.helper = this.helper || t("<div style='overflow:hidden;'></div>"), this.helper.addClass(this._helper).css({
                    width: this.element.outerWidth() - 1,
                    height: this.element.outerHeight() - 1,
                    position: "absolute",
                    left: this.elementOffset.left + "px",
                    top: this.elementOffset.top + "px",
                    zIndex: ++n.zIndex
                }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element
            },
            _change: {
                e: function(t, e) {
                    return {
                        width: this.originalSize.width + e
                    }
                },
                w: function(t, e) {
                    var n = this.originalSize,
                        i = this.originalPosition;
                    return {
                        left: i.left + e,
                        width: n.width - e
                    }
                },
                n: function(t, e, n) {
                    var i = this.originalSize,
                        o = this.originalPosition;
                    return {
                        top: o.top + n,
                        height: i.height - n
                    }
                },
                s: function(t, e, n) {
                    return {
                        height: this.originalSize.height + n
                    }
                },
                se: function(e, n, i) {
                    return t.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [e, n, i]))
                },
                sw: function(e, n, i) {
                    return t.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [e, n, i]))
                },
                ne: function(e, n, i) {
                    return t.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [e, n, i]))
                },
                nw: function(e, n, i) {
                    return t.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [e, n, i]))
                }
            },
            _propagate: function(e, n) {
                t.ui.plugin.call(this, e, [n, this.ui()]), "resize" !== e && this._trigger(e, n, this.ui())
            },
            plugins: {},
            ui: function() {
                return {
                    originalElement: this.originalElement,
                    element: this.element,
                    helper: this.helper,
                    position: this.position,
                    size: this.size,
                    originalSize: this.originalSize,
                    originalPosition: this.originalPosition
                }
            }
        }), t.ui.plugin.add("resizable", "animate", {
            stop: function(e) {
                var n = t(this).resizable("instance"),
                    o = n.options,
                    r = n._proportionallyResizeElements,
                    s = r.length && /textarea/i.test(r[0].nodeName),
                    a = s && i(r[0], "left") ? 0 : n.sizeDiff.height,
                    l = s ? 0 : n.sizeDiff.width,
                    c = {
                        width: n.size.width - l,
                        height: n.size.height - a
                    },
                    u = parseInt(n.element.css("left"), 10) + (n.position.left - n.originalPosition.left) || null,
                    d = parseInt(n.element.css("top"), 10) + (n.position.top - n.originalPosition.top) || null;
                n.element.animate(t.extend(c, d && u ? {
                    top: d,
                    left: u
                } : {}), {
                    duration: o.animateDuration,
                    easing: o.animateEasing,
                    step: function() {
                        var i = {
                            width: parseInt(n.element.css("width"), 10),
                            height: parseInt(n.element.css("height"), 10),
                            top: parseInt(n.element.css("top"), 10),
                            left: parseInt(n.element.css("left"), 10)
                        };
                        r && r.length && t(r[0]).css({
                            width: i.width,
                            height: i.height
                        }), n._updateCache(i), n._propagate("resize", e)
                    }
                })
            }
        }), t.ui.plugin.add("resizable", "containment", {
            start: function() {
                var n, o, r, s, a, l, c, u = t(this).resizable("instance"),
                    d = u.options,
                    h = u.element,
                    p = d.containment,
                    f = p instanceof t ? p.get(0) : /parent/.test(p) ? h.parent().get(0) : p;
                f && (u.containerElement = t(f), /document/.test(p) || p === document ? (u.containerOffset = {
                    left: 0,
                    top: 0
                }, u.containerPosition = {
                    left: 0,
                    top: 0
                }, u.parentData = {
                    element: t(document),
                    left: 0,
                    top: 0,
                    width: t(document).width(),
                    height: t(document).height() || document.body.parentNode.scrollHeight
                }) : (n = t(f), o = [], t(["Top", "Right", "Left", "Bottom"]).each(function(t, i) {
                    o[t] = e(n.css("padding" + i))
                }), u.containerOffset = n.offset(), u.containerPosition = n.position(), u.containerSize = {
                    height: n.innerHeight() - o[3],
                    width: n.innerWidth() - o[1]
                }, r = u.containerOffset, s = u.containerSize.height, a = u.containerSize.width, l = i(f, "left") ? f.scrollWidth : a, c = i(f) ? f.scrollHeight : s, u.parentData = {
                    element: f,
                    left: r.left,
                    top: r.top,
                    width: l,
                    height: c
                }))
            },
            resize: function(e) {
                var n, i, o, r, s = t(this).resizable("instance"),
                    a = s.options,
                    l = s.containerOffset,
                    c = s.position,
                    u = s._aspectRatio || e.shiftKey,
                    d = {
                        top: 0,
                        left: 0
                    },
                    h = s.containerElement;
                h[0] !== document && /static/.test(h.css("position")) && (d = l), c.left < (s._helper ? l.left : 0) && (s.size.width = s.size.width + (s._helper ? s.position.left - l.left : s.position.left - d.left), u && (s.size.height = s.size.width / s.aspectRatio), s.position.left = a.helper ? l.left : 0), c.top < (s._helper ? l.top : 0) && (s.size.height = s.size.height + (s._helper ? s.position.top - l.top : s.position.top), u && (s.size.width = s.size.height * s.aspectRatio), s.position.top = s._helper ? l.top : 0), s.offset.left = s.parentData.left + s.position.left, s.offset.top = s.parentData.top + s.position.top, n = Math.abs((s._helper ? s.offset.left - d.left : s.offset.left - d.left) + s.sizeDiff.width), i = Math.abs((s._helper ? s.offset.top - d.top : s.offset.top - l.top) + s.sizeDiff.height), o = s.containerElement.get(0) === s.element.parent().get(0), r = /relative|absolute/.test(s.containerElement.css("position")), o && r && (n -= Math.abs(s.parentData.left)), n + s.size.width >= s.parentData.width && (s.size.width = s.parentData.width - n, u && (s.size.height = s.size.width / s.aspectRatio)), i + s.size.height >= s.parentData.height && (s.size.height = s.parentData.height - i, u && (s.size.width = s.size.height * s.aspectRatio))
            },
            stop: function() {
                var e = t(this).resizable("instance"),
                    n = e.options,
                    i = e.containerOffset,
                    o = e.containerPosition,
                    r = e.containerElement,
                    s = t(e.helper),
                    a = s.offset(),
                    l = s.outerWidth() - e.sizeDiff.width,
                    c = s.outerHeight() - e.sizeDiff.height;
                e._helper && !n.animate && /relative/.test(r.css("position")) && t(this).css({
                    left: a.left - o.left - i.left,
                    width: l,
                    height: c
                }), e._helper && !n.animate && /static/.test(r.css("position")) && t(this).css({
                    left: a.left - o.left - i.left,
                    width: l,
                    height: c
                })
            }
        }), t.ui.plugin.add("resizable", "alsoResize", {
            start: function() {
                var e = t(this).resizable("instance"),
                    n = e.options,
                    i = function(e) {
                        t(e).each(function() {
                            var e = t(this);
                            e.data("ui-resizable-alsoresize", {
                                width: parseInt(e.width(), 10),
                                height: parseInt(e.height(), 10),
                                left: parseInt(e.css("left"), 10),
                                top: parseInt(e.css("top"), 10)
                            })
                        })
                    };
                "object" != typeof n.alsoResize || n.alsoResize.parentNode ? i(n.alsoResize) : n.alsoResize.length ? (n.alsoResize = n.alsoResize[0], i(n.alsoResize)) : t.each(n.alsoResize, function(t) {
                    i(t)
                })
            },
            resize: function(e, n) {
                var i = t(this).resizable("instance"),
                    o = i.options,
                    r = i.originalSize,
                    s = i.originalPosition,
                    a = {
                        height: i.size.height - r.height || 0,
                        width: i.size.width - r.width || 0,
                        top: i.position.top - s.top || 0,
                        left: i.position.left - s.left || 0
                    },
                    l = function(e, i) {
                        t(e).each(function() {
                            var e = t(this),
                                o = t(this).data("ui-resizable-alsoresize"),
                                r = {},
                                s = i && i.length ? i : e.parents(n.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
                            t.each(s, function(t, e) {
                                var n = (o[e] || 0) + (a[e] || 0);
                                n && n >= 0 && (r[e] = n || null)
                            }), e.css(r)
                        })
                    };
                "object" != typeof o.alsoResize || o.alsoResize.nodeType ? l(o.alsoResize) : t.each(o.alsoResize, function(t, e) {
                    l(t, e)
                })
            },
            stop: function() {
                t(this).removeData("resizable-alsoresize")
            }
        }), t.ui.plugin.add("resizable", "ghost", {
            start: function() {
                var e = t(this).resizable("instance"),
                    n = e.options,
                    i = e.size;
                e.ghost = e.originalElement.clone(), e.ghost.css({
                    opacity: .25,
                    display: "block",
                    position: "relative",
                    height: i.height,
                    width: i.width,
                    margin: 0,
                    left: 0,
                    top: 0
                }).addClass("ui-resizable-ghost").addClass("string" == typeof n.ghost ? n.ghost : ""), e.ghost.appendTo(e.helper)
            },
            resize: function() {
                var e = t(this).resizable("instance");
                e.ghost && e.ghost.css({
                    position: "relative",
                    height: e.size.height,
                    width: e.size.width
                })
            },
            stop: function() {
                var e = t(this).resizable("instance");
                e.ghost && e.helper && e.helper.get(0).removeChild(e.ghost.get(0))
            }
        }), t.ui.plugin.add("resizable", "grid", {
            resize: function() {
                var e = t(this).resizable("instance"),
                    n = e.options,
                    i = e.size,
                    o = e.originalSize,
                    r = e.originalPosition,
                    s = e.axis,
                    a = "number" == typeof n.grid ? [n.grid, n.grid] : n.grid,
                    l = a[0] || 1,
                    c = a[1] || 1,
                    u = Math.round((i.width - o.width) / l) * l,
                    d = Math.round((i.height - o.height) / c) * c,
                    h = o.width + u,
                    p = o.height + d,
                    f = n.maxWidth && n.maxWidth < h,
                    m = n.maxHeight && n.maxHeight < p,
                    g = n.minWidth && n.minWidth > h,
                    v = n.minHeight && n.minHeight > p;
                n.grid = a, g && (h += l), v && (p += c), f && (h -= l), m && (p -= c), /^(se|s|e)$/.test(s) ? (e.size.width = h, e.size.height = p) : /^(ne)$/.test(s) ? (e.size.width = h, e.size.height = p, e.position.top = r.top - d) : /^(sw)$/.test(s) ? (e.size.width = h, e.size.height = p, e.position.left = r.left - u) : (e.size.width = h, e.size.height = p, e.position.top = r.top - d, e.position.left = r.left - u)
            }
        })
    }(jQuery),
    /*!
     * jQuery UI Selectable 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/selectable/
     *
     * Depends:
     *	jquery.ui.core.js
     *	jquery.ui.mouse.js
     *	jquery.ui.widget.js
     */
    function(t) {
        t.widget("ui.selectable", t.ui.mouse, {
            version: "1.10.3",
            options: {
                appendTo: "body",
                autoRefresh: !0,
                distance: 0,
                filter: "*",
                tolerance: "touch",
                selected: null,
                selecting: null,
                start: null,
                stop: null,
                unselected: null,
                unselecting: null
            },
            _create: function() {
                var e, n = this;
                this.element.addClass("ui-selectable"), this.dragged = !1, this.refresh = function() {
                    e = t(n.options.filter, n.element[0]), e.addClass("ui-selectee"), e.each(function() {
                        var e = t(this),
                            n = e.offset();
                        t.data(this, "selectable-item", {
                            element: this,
                            $element: e,
                            left: n.left,
                            top: n.top,
                            right: n.left + e.outerWidth(),
                            bottom: n.top + e.outerHeight(),
                            startselected: !1,
                            selected: e.hasClass("ui-selected"),
                            selecting: e.hasClass("ui-selecting"),
                            unselecting: e.hasClass("ui-unselecting")
                        })
                    })
                }, this.refresh(), this.selectees = e.addClass("ui-selectee"), this._mouseInit(), this.helper = t("<div class='ui-selectable-helper'></div>")
            },
            _destroy: function() {
                this.selectees.removeClass("ui-selectee").removeData("selectable-item"), this.element.removeClass("ui-selectable ui-selectable-disabled"), this._mouseDestroy()
            },
            _mouseStart: function(e) {
                var n = this,
                    i = this.options;
                this.opos = [e.pageX, e.pageY], this.options.disabled || (this.selectees = t(i.filter, this.element[0]), this._trigger("start", e), t(i.appendTo).append(this.helper), this.helper.css({
                    left: e.pageX,
                    top: e.pageY,
                    width: 0,
                    height: 0
                }), i.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function() {
                    var i = t.data(this, "selectable-item");
                    i.startselected = !0, e.metaKey || e.ctrlKey || (i.$element.removeClass("ui-selected"), i.selected = !1, i.$element.addClass("ui-unselecting"), i.unselecting = !0, n._trigger("unselecting", e, {
                        unselecting: i.element
                    }))
                }), t(e.target).parents().addBack().each(function() {
                    var i, o = t.data(this, "selectable-item");
                    return o ? (i = !e.metaKey && !e.ctrlKey || !o.$element.hasClass("ui-selected"), o.$element.removeClass(i ? "ui-unselecting" : "ui-selected").addClass(i ? "ui-selecting" : "ui-unselecting"), o.unselecting = !i, o.selecting = i, o.selected = i, i ? n._trigger("selecting", e, {
                        selecting: o.element
                    }) : n._trigger("unselecting", e, {
                        unselecting: o.element
                    }), !1) : void 0
                }))
            },
            _mouseDrag: function(e) {
                if (this.dragged = !0, !this.options.disabled) {
                    var n, i = this,
                        o = this.options,
                        r = this.opos[0],
                        s = this.opos[1],
                        a = e.pageX,
                        l = e.pageY;
                    return r > a && (n = a, a = r, r = n), s > l && (n = l, l = s, s = n), this.helper.css({
                        left: r,
                        top: s,
                        width: a - r,
                        height: l - s
                    }), this.selectees.each(function() {
                        var n = t.data(this, "selectable-item"),
                            c = !1;
                        n && n.element !== i.element[0] && ("touch" === o.tolerance ? c = !(n.left > a || n.right < r || n.top > l || n.bottom < s) : "fit" === o.tolerance && (c = n.left > r && n.right < a && n.top > s && n.bottom < l), c ? (n.selected && (n.$element.removeClass("ui-selected"), n.selected = !1), n.unselecting && (n.$element.removeClass("ui-unselecting"), n.unselecting = !1), n.selecting || (n.$element.addClass("ui-selecting"), n.selecting = !0, i._trigger("selecting", e, {
                            selecting: n.element
                        }))) : (n.selecting && ((e.metaKey || e.ctrlKey) && n.startselected ? (n.$element.removeClass("ui-selecting"), n.selecting = !1, n.$element.addClass("ui-selected"), n.selected = !0) : (n.$element.removeClass("ui-selecting"), n.selecting = !1, n.startselected && (n.$element.addClass("ui-unselecting"), n.unselecting = !0), i._trigger("unselecting", e, {
                            unselecting: n.element
                        }))), n.selected && (e.metaKey || e.ctrlKey || n.startselected || (n.$element.removeClass("ui-selected"), n.selected = !1, n.$element.addClass("ui-unselecting"), n.unselecting = !0, i._trigger("unselecting", e, {
                            unselecting: n.element
                        })))))
                    }), !1
                }
            },
            _mouseStop: function(e) {
                var n = this;
                return this.dragged = !1, t(".ui-unselecting", this.element[0]).each(function() {
                    var i = t.data(this, "selectable-item");
                    i.$element.removeClass("ui-unselecting"), i.unselecting = !1, i.startselected = !1, n._trigger("unselected", e, {
                        unselected: i.element
                    })
                }), t(".ui-selecting", this.element[0]).each(function() {
                    var i = t.data(this, "selectable-item");
                    i.$element.removeClass("ui-selecting").addClass("ui-selected"), i.selecting = !1, i.selected = !0, i.startselected = !0, n._trigger("selected", e, {
                        selected: i.element
                    })
                }), this._trigger("stop", e), this.helper.remove(), !1
            }
        })
    }(jQuery),
    /*!
     * jQuery UI Sortable 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/sortable/
     *
     * Depends:
     *	jquery.ui.core.js
     *	jquery.ui.mouse.js
     *	jquery.ui.widget.js
     */
    function(t) {
        function e(t, e, n) {
            return t > e && e + n > t
        }

        function n(t) {
            return /left|right/.test(t.css("float")) || /inline|table-cell/.test(t.css("display"))
        }
        t.widget("ui.sortable", t.ui.mouse, {
            version: "1.10.3",
            widgetEventPrefix: "sort",
            ready: !1,
            options: {
                appendTo: "parent",
                axis: !1,
                connectWith: !1,
                containment: !1,
                cursor: "auto",
                cursorAt: !1,
                dropOnEmpty: !0,
                forcePlaceholderSize: !1,
                forceHelperSize: !1,
                grid: !1,
                handle: !1,
                helper: "original",
                items: "> *",
                opacity: !1,
                placeholder: !1,
                revert: !1,
                scroll: !0,
                scrollSensitivity: 20,
                scrollSpeed: 20,
                scope: "default",
                tolerance: "intersect",
                zIndex: 1e3,
                activate: null,
                beforeStop: null,
                change: null,
                deactivate: null,
                out: null,
                over: null,
                receive: null,
                remove: null,
                sort: null,
                start: null,
                stop: null,
                update: null
            },
            _create: function() {
                var t = this.options;
                this.containerCache = {}, this.element.addClass("ui-sortable"), this.refresh(), this.floating = this.items.length ? "x" === t.axis || n(this.items[0].item) : !1, this.offset = this.element.offset(), this._mouseInit(), this.ready = !0
            },
            _destroy: function() {
                this.element.removeClass("ui-sortable ui-sortable-disabled"), this._mouseDestroy();
                for (var t = this.items.length - 1; t >= 0; t--) this.items[t].item.removeData(this.widgetName + "-item");
                return this
            },
            _setOption: function(e, n) {
                "disabled" === e ? (this.options[e] = n, this.widget().toggleClass("ui-sortable-disabled", !!n)) : t.Widget.prototype._setOption.apply(this, arguments)
            },
            _mouseCapture: function(e, n) {
                var i = null,
                    o = !1,
                    r = this;
                return this.reverting ? !1 : this.options.disabled || "static" === this.options.type ? !1 : (this._refreshItems(e), t(e.target).parents().each(function() {
                    return t.data(this, r.widgetName + "-item") === r ? (i = t(this), !1) : void 0
                }), t.data(e.target, r.widgetName + "-item") === r && (i = t(e.target)), i && (!this.options.handle || n || (t(this.options.handle, i).find("*").addBack().each(function() {
                    this === e.target && (o = !0)
                }), o)) ? (this.currentItem = i, this._removeCurrentsFromItems(), !0) : !1)
            },
            _mouseStart: function(e, n, i) {
                var o, r, s = this.options;
                if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(e), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), this.offset = this.currentItem.offset(), this.offset = {
                        top: this.offset.top - this.margins.top,
                        left: this.offset.left - this.margins.left
                    }, t.extend(this.offset, {
                        click: {
                            left: e.pageX - this.offset.left,
                            top: e.pageY - this.offset.top
                        },
                        parent: this._getParentOffset(),
                        relative: this._getRelativeOffset()
                    }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(e), this.originalPageX = e.pageX, this.originalPageY = e.pageY, s.cursorAt && this._adjustOffsetFromHelper(s.cursorAt), this.domPosition = {
                        prev: this.currentItem.prev()[0],
                        parent: this.currentItem.parent()[0]
                    }, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), s.containment && this._setContainment(), s.cursor && "auto" !== s.cursor && (r = this.document.find("body"), this.storedCursor = r.css("cursor"), r.css("cursor", s.cursor), this.storedStylesheet = t("<style>*{ cursor: " + s.cursor + " !important; }</style>").appendTo(r)), s.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), this.helper.css("opacity", s.opacity)), s.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", s.zIndex)), this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", e, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !i)
                    for (o = this.containers.length - 1; o >= 0; o--) this.containers[o]._trigger("activate", e, this._uiHash(this));
                return t.ui.ddmanager && (t.ui.ddmanager.current = this), t.ui.ddmanager && !s.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this.dragging = !0, this.helper.addClass("ui-sortable-helper"), this._mouseDrag(e), !0
            },
            _mouseDrag: function(e) {
                var n, i, o, r, s = this.options,
                    a = !1;
                for (this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - e.pageY < s.scrollSensitivity ? this.scrollParent[0].scrollTop = a = this.scrollParent[0].scrollTop + s.scrollSpeed : e.pageY - this.overflowOffset.top < s.scrollSensitivity && (this.scrollParent[0].scrollTop = a = this.scrollParent[0].scrollTop - s.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - e.pageX < s.scrollSensitivity ? this.scrollParent[0].scrollLeft = a = this.scrollParent[0].scrollLeft + s.scrollSpeed : e.pageX - this.overflowOffset.left < s.scrollSensitivity && (this.scrollParent[0].scrollLeft = a = this.scrollParent[0].scrollLeft - s.scrollSpeed)) : (e.pageY - t(document).scrollTop() < s.scrollSensitivity ? a = t(document).scrollTop(t(document).scrollTop() - s.scrollSpeed) : t(window).height() - (e.pageY - t(document).scrollTop()) < s.scrollSensitivity && (a = t(document).scrollTop(t(document).scrollTop() + s.scrollSpeed)), e.pageX - t(document).scrollLeft() < s.scrollSensitivity ? a = t(document).scrollLeft(t(document).scrollLeft() - s.scrollSpeed) : t(window).width() - (e.pageX - t(document).scrollLeft()) < s.scrollSensitivity && (a = t(document).scrollLeft(t(document).scrollLeft() + s.scrollSpeed))), a !== !1 && t.ui.ddmanager && !s.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e)), this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), n = this.items.length - 1; n >= 0; n--)
                    if (i = this.items[n], o = i.item[0], r = this._intersectsWithPointer(i), r && i.instance === this.currentContainer && o !== this.currentItem[0] && this.placeholder[1 === r ? "next" : "prev"]()[0] !== o && !t.contains(this.placeholder[0], o) && ("semi-dynamic" === this.options.type ? !t.contains(this.element[0], o) : !0)) {
                        if (this.direction = 1 === r ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(i)) break;
                        this._rearrange(e, i), this._trigger("change", e, this._uiHash());
                        break
                    }
                return this._contactContainers(e), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), this._trigger("sort", e, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1
            },
            _mouseStop: function(e, n) {
                if (e) {
                    if (t.ui.ddmanager && !this.options.dropBehaviour && t.ui.ddmanager.drop(this, e), this.options.revert) {
                        var i = this,
                            o = this.placeholder.offset(),
                            r = this.options.axis,
                            s = {};
                        r && "x" !== r || (s.left = o.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollLeft)), r && "y" !== r || (s.top = o.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollTop)), this.reverting = !0, t(this.helper).animate(s, parseInt(this.options.revert, 10) || 500, function() {
                            i._clear(e)
                        })
                    } else this._clear(e, n);
                    return !1
                }
            },
            cancel: function() {
                if (this.dragging) {
                    this._mouseUp({
                        target: null
                    }), "original" === this.options.helper ? this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper") : this.currentItem.show();
                    for (var e = this.containers.length - 1; e >= 0; e--) this.containers[e]._trigger("deactivate", null, this._uiHash(this)), this.containers[e].containerCache.over && (this.containers[e]._trigger("out", null, this._uiHash(this)), this.containers[e].containerCache.over = 0)
                }
                return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), t.extend(this, {
                    helper: null,
                    dragging: !1,
                    reverting: !1,
                    _noFinalSort: null
                }), this.domPosition.prev ? t(this.domPosition.prev).after(this.currentItem) : t(this.domPosition.parent).prepend(this.currentItem)), this
            },
            serialize: function(e) {
                var n = this._getItemsAsjQuery(e && e.connected),
                    i = [];
                return e = e || {}, t(n).each(function() {
                    var n = (t(e.item || this).attr(e.attribute || "id") || "").match(e.expression || /(.+)[\-=_](.+)/);
                    n && i.push((e.key || n[1] + "[]") + "=" + (e.key && e.expression ? n[1] : n[2]))
                }), !i.length && e.key && i.push(e.key + "="), i.join("&")
            },
            toArray: function(e) {
                var n = this._getItemsAsjQuery(e && e.connected),
                    i = [];
                return e = e || {}, n.each(function() {
                    i.push(t(e.item || this).attr(e.attribute || "id") || "")
                }), i
            },
            _intersectsWith: function(t) {
                var e = this.positionAbs.left,
                    n = e + this.helperProportions.width,
                    i = this.positionAbs.top,
                    o = i + this.helperProportions.height,
                    r = t.left,
                    s = r + t.width,
                    a = t.top,
                    l = a + t.height,
                    c = this.offset.click.top,
                    u = this.offset.click.left,
                    d = "x" === this.options.axis || i + c > a && l > i + c,
                    h = "y" === this.options.axis || e + u > r && s > e + u,
                    p = d && h;
                return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > t[this.floating ? "width" : "height"] ? p : r < e + this.helperProportions.width / 2 && n - this.helperProportions.width / 2 < s && a < i + this.helperProportions.height / 2 && o - this.helperProportions.height / 2 < l
            },
            _intersectsWithPointer: function(t) {
                var n = "x" === this.options.axis || e(this.positionAbs.top + this.offset.click.top, t.top, t.height),
                    i = "y" === this.options.axis || e(this.positionAbs.left + this.offset.click.left, t.left, t.width),
                    o = n && i,
                    r = this._getDragVerticalDirection(),
                    s = this._getDragHorizontalDirection();
                return o ? this.floating ? s && "right" === s || "down" === r ? 2 : 1 : r && ("down" === r ? 2 : 1) : !1
            },
            _intersectsWithSides: function(t) {
                var n = e(this.positionAbs.top + this.offset.click.top, t.top + t.height / 2, t.height),
                    i = e(this.positionAbs.left + this.offset.click.left, t.left + t.width / 2, t.width),
                    o = this._getDragVerticalDirection(),
                    r = this._getDragHorizontalDirection();
                return this.floating && r ? "right" === r && i || "left" === r && !i : o && ("down" === o && n || "up" === o && !n)
            },
            _getDragVerticalDirection: function() {
                var t = this.positionAbs.top - this.lastPositionAbs.top;
                return 0 !== t && (t > 0 ? "down" : "up")
            },
            _getDragHorizontalDirection: function() {
                var t = this.positionAbs.left - this.lastPositionAbs.left;
                return 0 !== t && (t > 0 ? "right" : "left")
            },
            refresh: function(t) {
                return this._refreshItems(t), this.refreshPositions(), this
            },
            _connectWith: function() {
                var t = this.options;
                return t.connectWith.constructor === String ? [t.connectWith] : t.connectWith
            },
            _getItemsAsjQuery: function(e) {
                var n, i, o, r, s = [],
                    a = [],
                    l = this._connectWith();
                if (l && e)
                    for (n = l.length - 1; n >= 0; n--)
                        for (o = t(l[n]), i = o.length - 1; i >= 0; i--) r = t.data(o[i], this.widgetFullName), r && r !== this && !r.options.disabled && a.push([t.isFunction(r.options.items) ? r.options.items.call(r.element) : t(r.options.items, r.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), r]);
                for (a.push([t.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
                        options: this.options,
                        item: this.currentItem
                    }) : t(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]), n = a.length - 1; n >= 0; n--) a[n][0].each(function() {
                    s.push(this)
                });
                return t(s)
            },
            _removeCurrentsFromItems: function() {
                var e = this.currentItem.find(":data(" + this.widgetName + "-item)");
                this.items = t.grep(this.items, function(t) {
                    for (var n = 0; n < e.length; n++)
                        if (e[n] === t.item[0]) return !1;
                    return !0
                })
            },
            _refreshItems: function(e) {
                this.items = [], this.containers = [this];
                var n, i, o, r, s, a, l, c, u = this.items,
                    d = [
                        [t.isFunction(this.options.items) ? this.options.items.call(this.element[0], e, {
                            item: this.currentItem
                        }) : t(this.options.items, this.element), this]
                    ],
                    h = this._connectWith();
                if (h && this.ready)
                    for (n = h.length - 1; n >= 0; n--)
                        for (o = t(h[n]), i = o.length - 1; i >= 0; i--) r = t.data(o[i], this.widgetFullName), r && r !== this && !r.options.disabled && (d.push([t.isFunction(r.options.items) ? r.options.items.call(r.element[0], e, {
                            item: this.currentItem
                        }) : t(r.options.items, r.element), r]), this.containers.push(r));
                for (n = d.length - 1; n >= 0; n--)
                    for (s = d[n][1], a = d[n][0], i = 0, c = a.length; c > i; i++) l = t(a[i]), l.data(this.widgetName + "-item", s), u.push({
                        item: l,
                        instance: s,
                        width: 0,
                        height: 0,
                        left: 0,
                        top: 0
                    })
            },
            refreshPositions: function(e) {
                this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
                var n, i, o, r;
                for (n = this.items.length - 1; n >= 0; n--) i = this.items[n], i.instance !== this.currentContainer && this.currentContainer && i.item[0] !== this.currentItem[0] || (o = this.options.toleranceElement ? t(this.options.toleranceElement, i.item) : i.item, e || (i.width = o.outerWidth(), i.height = o.outerHeight()), r = o.offset(), i.left = r.left, i.top = r.top);
                if (this.options.custom && this.options.custom.refreshContainers) this.options.custom.refreshContainers.call(this);
                else
                    for (n = this.containers.length - 1; n >= 0; n--) r = this.containers[n].element.offset(), this.containers[n].containerCache.left = r.left, this.containers[n].containerCache.top = r.top, this.containers[n].containerCache.width = this.containers[n].element.outerWidth(), this.containers[n].containerCache.height = this.containers[n].element.outerHeight();
                return this
            },
            _createPlaceholder: function(e) {
                e = e || this;
                var n, i = e.options;
                i.placeholder && i.placeholder.constructor !== String || (n = i.placeholder, i.placeholder = {
                    element: function() {
                        var i = e.currentItem[0].nodeName.toLowerCase(),
                            o = t("<" + i + ">", e.document[0]).addClass(n || e.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper");
                        return "tr" === i ? e.currentItem.children().each(function() {
                            t("<td>&#160;</td>", e.document[0]).attr("colspan", t(this).attr("colspan") || 1).appendTo(o)
                        }) : "img" === i && o.attr("src", e.currentItem.attr("src")), n || o.css("visibility", "hidden"), o
                    },
                    update: function(t, o) {
                        (!n || i.forcePlaceholderSize) && (o.height() || o.height(e.currentItem.innerHeight() - parseInt(e.currentItem.css("paddingTop") || 0, 10) - parseInt(e.currentItem.css("paddingBottom") || 0, 10)), o.width() || o.width(e.currentItem.innerWidth() - parseInt(e.currentItem.css("paddingLeft") || 0, 10) - parseInt(e.currentItem.css("paddingRight") || 0, 10)))
                    }
                }), e.placeholder = t(i.placeholder.element.call(e.element, e.currentItem)), e.currentItem.after(e.placeholder), i.placeholder.update(e, e.placeholder)
            },
            _contactContainers: function(i) {
                var o, r, s, a, l, c, u, d, h, p, f = null,
                    m = null;
                for (o = this.containers.length - 1; o >= 0; o--)
                    if (!t.contains(this.currentItem[0], this.containers[o].element[0]))
                        if (this._intersectsWith(this.containers[o].containerCache)) {
                            if (f && t.contains(this.containers[o].element[0], f.element[0])) continue;
                            f = this.containers[o], m = o
                        } else this.containers[o].containerCache.over && (this.containers[o]._trigger("out", i, this._uiHash(this)), this.containers[o].containerCache.over = 0);
                if (f)
                    if (1 === this.containers.length) this.containers[m].containerCache.over || (this.containers[m]._trigger("over", i, this._uiHash(this)), this.containers[m].containerCache.over = 1);
                    else {
                        for (s = 1e4, a = null, p = f.floating || n(this.currentItem), l = p ? "left" : "top", c = p ? "width" : "height", u = this.positionAbs[l] + this.offset.click[l], r = this.items.length - 1; r >= 0; r--) t.contains(this.containers[m].element[0], this.items[r].item[0]) && this.items[r].item[0] !== this.currentItem[0] && (!p || e(this.positionAbs.top + this.offset.click.top, this.items[r].top, this.items[r].height)) && (d = this.items[r].item.offset()[l], h = !1, Math.abs(d - u) > Math.abs(d + this.items[r][c] - u) && (h = !0, d += this.items[r][c]), Math.abs(d - u) < s && (s = Math.abs(d - u), a = this.items[r], this.direction = h ? "up" : "down"));
                        if (!a && !this.options.dropOnEmpty) return;
                        if (this.currentContainer === this.containers[m]) return;
                        a ? this._rearrange(i, a, null, !0) : this._rearrange(i, null, this.containers[m].element, !0), this._trigger("change", i, this._uiHash()), this.containers[m]._trigger("change", i, this._uiHash(this)), this.currentContainer = this.containers[m], this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[m]._trigger("over", i, this._uiHash(this)), this.containers[m].containerCache.over = 1
                    }
            },
            _createHelper: function(e) {
                var n = this.options,
                    i = t.isFunction(n.helper) ? t(n.helper.apply(this.element[0], [e, this.currentItem])) : "clone" === n.helper ? this.currentItem.clone() : this.currentItem;
                return i.parents("body").length || t("parent" !== n.appendTo ? n.appendTo : this.currentItem[0].parentNode)[0].appendChild(i[0]), i[0] === this.currentItem[0] && (this._storedCSS = {
                    width: this.currentItem[0].style.width,
                    height: this.currentItem[0].style.height,
                    position: this.currentItem.css("position"),
                    top: this.currentItem.css("top"),
                    left: this.currentItem.css("left")
                }), (!i[0].style.width || n.forceHelperSize) && i.width(this.currentItem.width()), (!i[0].style.height || n.forceHelperSize) && i.height(this.currentItem.height()), i
            },
            _adjustOffsetFromHelper: function(e) {
                "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
                    left: +e[0],
                    top: +e[1] || 0
                }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top)
            },
            _getParentOffset: function() {
                this.offsetParent = this.helper.offsetParent();
                var e = this.offsetParent.offset();
                return "absolute" === this.cssPosition && this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {
                    top: 0,
                    left: 0
                }), {
                    top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                    left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
                }
            },
            _getRelativeOffset: function() {
                if ("relative" === this.cssPosition) {
                    var t = this.currentItem.position();
                    return {
                        top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                        left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                    }
                }
                return {
                    top: 0,
                    left: 0
                }
            },
            _cacheMargins: function() {
                this.margins = {
                    left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
                    top: parseInt(this.currentItem.css("marginTop"), 10) || 0
                }
            },
            _cacheHelperProportions: function() {
                this.helperProportions = {
                    width: this.helper.outerWidth(),
                    height: this.helper.outerHeight()
                }
            },
            _setContainment: function() {
                var e, n, i, o = this.options;
                "parent" === o.containment && (o.containment = this.helper[0].parentNode), ("document" === o.containment || "window" === o.containment) && (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, t("document" === o.containment ? document : window).width() - this.helperProportions.width - this.margins.left, (t("document" === o.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), /^(document|window|parent)$/.test(o.containment) || (e = t(o.containment)[0], n = t(o.containment).offset(), i = "hidden" !== t(e).css("overflow"), this.containment = [n.left + (parseInt(t(e).css("borderLeftWidth"), 10) || 0) + (parseInt(t(e).css("paddingLeft"), 10) || 0) - this.margins.left, n.top + (parseInt(t(e).css("borderTopWidth"), 10) || 0) + (parseInt(t(e).css("paddingTop"), 10) || 0) - this.margins.top, n.left + (i ? Math.max(e.scrollWidth, e.offsetWidth) : e.offsetWidth) - (parseInt(t(e).css("borderLeftWidth"), 10) || 0) - (parseInt(t(e).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, n.top + (i ? Math.max(e.scrollHeight, e.offsetHeight) : e.offsetHeight) - (parseInt(t(e).css("borderTopWidth"), 10) || 0) - (parseInt(t(e).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top])
            },
            _convertPositionTo: function(e, n) {
                n || (n = this.position);
                var i = "absolute" === e ? 1 : -1,
                    o = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                    r = /(html|body)/i.test(o[0].tagName);
                return {
                    top: n.top + this.offset.relative.top * i + this.offset.parent.top * i - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : r ? 0 : o.scrollTop()) * i,
                    left: n.left + this.offset.relative.left * i + this.offset.parent.left * i - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : r ? 0 : o.scrollLeft()) * i
                }
            },
            _generatePosition: function(e) {
                var n, i, o = this.options,
                    r = e.pageX,
                    s = e.pageY,
                    a = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                    l = /(html|body)/i.test(a[0].tagName);
                return "relative" !== this.cssPosition || this.scrollParent[0] !== document && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), this.originalPosition && (this.containment && (e.pageX - this.offset.click.left < this.containment[0] && (r = this.containment[0] + this.offset.click.left), e.pageY - this.offset.click.top < this.containment[1] && (s = this.containment[1] + this.offset.click.top), e.pageX - this.offset.click.left > this.containment[2] && (r = this.containment[2] + this.offset.click.left), e.pageY - this.offset.click.top > this.containment[3] && (s = this.containment[3] + this.offset.click.top)), o.grid && (n = this.originalPageY + Math.round((s - this.originalPageY) / o.grid[1]) * o.grid[1], s = this.containment ? n - this.offset.click.top >= this.containment[1] && n - this.offset.click.top <= this.containment[3] ? n : n - this.offset.click.top >= this.containment[1] ? n - o.grid[1] : n + o.grid[1] : n, i = this.originalPageX + Math.round((r - this.originalPageX) / o.grid[0]) * o.grid[0], r = this.containment ? i - this.offset.click.left >= this.containment[0] && i - this.offset.click.left <= this.containment[2] ? i : i - this.offset.click.left >= this.containment[0] ? i - o.grid[0] : i + o.grid[0] : i)), {
                    top: s - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : l ? 0 : a.scrollTop()),
                    left: r - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : l ? 0 : a.scrollLeft())
                }
            },
            _rearrange: function(t, e, n, i) {
                n ? n[0].appendChild(this.placeholder[0]) : e.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? e.item[0] : e.item[0].nextSibling), this.counter = this.counter ? ++this.counter : 1;
                var o = this.counter;
                this._delay(function() {
                    o === this.counter && this.refreshPositions(!i)
                })
            },
            _clear: function(t, e) {
                this.reverting = !1;
                var n, i = [];
                if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
                    for (n in this._storedCSS)("auto" === this._storedCSS[n] || "static" === this._storedCSS[n]) && (this._storedCSS[n] = "");
                    this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
                } else this.currentItem.show();
                for (this.fromOutside && !e && i.push(function(t) {
                        this._trigger("receive", t, this._uiHash(this.fromOutside))
                    }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || e || i.push(function(t) {
                        this._trigger("update", t, this._uiHash())
                    }), this !== this.currentContainer && (e || (i.push(function(t) {
                        this._trigger("remove", t, this._uiHash())
                    }), i.push(function(t) {
                        return function(e) {
                            t._trigger("receive", e, this._uiHash(this))
                        }
                    }.call(this, this.currentContainer)), i.push(function(t) {
                        return function(e) {
                            t._trigger("update", e, this._uiHash(this))
                        }
                    }.call(this, this.currentContainer)))), n = this.containers.length - 1; n >= 0; n--) e || i.push(function(t) {
                    return function(e) {
                        t._trigger("deactivate", e, this._uiHash(this))
                    }
                }.call(this, this.containers[n])), this.containers[n].containerCache.over && (i.push(function(t) {
                    return function(e) {
                        t._trigger("out", e, this._uiHash(this))
                    }
                }.call(this, this.containers[n])), this.containers[n].containerCache.over = 0);
                if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, this.cancelHelperRemoval) {
                    if (!e) {
                        for (this._trigger("beforeStop", t, this._uiHash()), n = 0; n < i.length; n++) i[n].call(this, t);
                        this._trigger("stop", t, this._uiHash())
                    }
                    return this.fromOutside = !1, !1
                }
                if (e || this._trigger("beforeStop", t, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.helper[0] !== this.currentItem[0] && this.helper.remove(), this.helper = null, !e) {
                    for (n = 0; n < i.length; n++) i[n].call(this, t);
                    this._trigger("stop", t, this._uiHash())
                }
                return this.fromOutside = !1, !0
            },
            _trigger: function() {
                t.Widget.prototype._trigger.apply(this, arguments) === !1 && this.cancel()
            },
            _uiHash: function(e) {
                var n = e || this;
                return {
                    helper: n.helper,
                    placeholder: n.placeholder || t([]),
                    position: n.position,
                    originalPosition: n.originalPosition,
                    offset: n.positionAbs,
                    item: n.currentItem,
                    sender: e ? e.element : null
                }
            }
        })
    }(jQuery),
    /*!
     * jQuery UI Effects 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/category/effects-core/
     */
    function(t, e) {
        var n = "ui-effects-";
        t.effects = {
                effect: {}
            },
            /*!
             * jQuery Color Animations v2.1.2
             * https://github.com/jquery/jquery-color
             *
             * Copyright 2013 jQuery Foundation and other contributors
             * Released under the MIT license.
             * http://jquery.org/license
             *
             * Date: Wed Jan 16 08:47:09 2013 -0600
             */
            function(t, e) {
                function n(t, e, n) {
                    var i = d[e.type] || {};
                    return null == t ? n || !e.def ? null : e.def : (t = i.floor ? ~~t : parseFloat(t), isNaN(t) ? e.def : i.mod ? (t + i.mod) % i.mod : 0 > t ? 0 : i.max < t ? i.max : t)
                }

                function i(e) {
                    var n = c(),
                        i = n._rgba = [];
                    return e = e.toLowerCase(), f(l, function(t, o) {
                        var r, s = o.re.exec(e),
                            a = s && o.parse(s),
                            l = o.space || "rgba";
                        return a ? (r = n[l](a), n[u[l].cache] = r[u[l].cache], i = n._rgba = r._rgba, !1) : void 0
                    }), i.length ? ("0,0,0,0" === i.join() && t.extend(i, r.transparent), n) : r[e]
                }

                function o(t, e, n) {
                    return n = (n + 1) % 1, 1 > 6 * n ? t + (e - t) * n * 6 : 1 > 2 * n ? e : 2 > 3 * n ? t + (e - t) * (2 / 3 - n) * 6 : t
                }
                var r, s = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",
                    a = /^([\-+])=\s*(\d+\.?\d*)/,
                    l = [{
                        re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                        parse: function(t) {
                            return [t[1], t[2], t[3], t[4]]
                        }
                    }, {
                        re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                        parse: function(t) {
                            return [2.55 * t[1], 2.55 * t[2], 2.55 * t[3], t[4]]
                        }
                    }, {
                        re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
                        parse: function(t) {
                            return [parseInt(t[1], 16), parseInt(t[2], 16), parseInt(t[3], 16)]
                        }
                    }, {
                        re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
                        parse: function(t) {
                            return [parseInt(t[1] + t[1], 16), parseInt(t[2] + t[2], 16), parseInt(t[3] + t[3], 16)]
                        }
                    }, {
                        re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                        space: "hsla",
                        parse: function(t) {
                            return [t[1], t[2] / 100, t[3] / 100, t[4]]
                        }
                    }],
                    c = t.Color = function(e, n, i, o) {
                        return new t.Color.fn.parse(e, n, i, o)
                    },
                    u = {
                        rgba: {
                            props: {
                                red: {
                                    idx: 0,
                                    type: "byte"
                                },
                                green: {
                                    idx: 1,
                                    type: "byte"
                                },
                                blue: {
                                    idx: 2,
                                    type: "byte"
                                }
                            }
                        },
                        hsla: {
                            props: {
                                hue: {
                                    idx: 0,
                                    type: "degrees"
                                },
                                saturation: {
                                    idx: 1,
                                    type: "percent"
                                },
                                lightness: {
                                    idx: 2,
                                    type: "percent"
                                }
                            }
                        }
                    },
                    d = {
                        "byte": {
                            floor: !0,
                            max: 255
                        },
                        percent: {
                            max: 1
                        },
                        degrees: {
                            mod: 360,
                            floor: !0
                        }
                    },
                    h = c.support = {},
                    p = t("<p>")[0],
                    f = t.each;
                p.style.cssText = "background-color:rgba(1,1,1,.5)", h.rgba = p.style.backgroundColor.indexOf("rgba") > -1, f(u, function(t, e) {
                    e.cache = "_" + t, e.props.alpha = {
                        idx: 3,
                        type: "percent",
                        def: 1
                    }
                }), c.fn = t.extend(c.prototype, {
                    parse: function(o, s, a, l) {
                        if (o === e) return this._rgba = [null, null, null, null], this;
                        (o.jquery || o.nodeType) && (o = t(o).css(s), s = e);
                        var d = this,
                            h = t.type(o),
                            p = this._rgba = [];
                        return s !== e && (o = [o, s, a, l], h = "array"), "string" === h ? this.parse(i(o) || r._default) : "array" === h ? (f(u.rgba.props, function(t, e) {
                            p[e.idx] = n(o[e.idx], e)
                        }), this) : "object" === h ? (o instanceof c ? f(u, function(t, e) {
                            o[e.cache] && (d[e.cache] = o[e.cache].slice())
                        }) : f(u, function(e, i) {
                            var r = i.cache;
                            f(i.props, function(t, e) {
                                if (!d[r] && i.to) {
                                    if ("alpha" === t || null == o[t]) return;
                                    d[r] = i.to(d._rgba)
                                }
                                d[r][e.idx] = n(o[t], e, !0)
                            }), d[r] && t.inArray(null, d[r].slice(0, 3)) < 0 && (d[r][3] = 1, i.from && (d._rgba = i.from(d[r])))
                        }), this) : void 0
                    },
                    is: function(t) {
                        var e = c(t),
                            n = !0,
                            i = this;
                        return f(u, function(t, o) {
                            var r, s = e[o.cache];
                            return s && (r = i[o.cache] || o.to && o.to(i._rgba) || [], f(o.props, function(t, e) {
                                return null != s[e.idx] ? n = s[e.idx] === r[e.idx] : void 0
                            })), n
                        }), n
                    },
                    _space: function() {
                        var t = [],
                            e = this;
                        return f(u, function(n, i) {
                            e[i.cache] && t.push(n)
                        }), t.pop()
                    },
                    transition: function(t, e) {
                        var i = c(t),
                            o = i._space(),
                            r = u[o],
                            s = 0 === this.alpha() ? c("transparent") : this,
                            a = s[r.cache] || r.to(s._rgba),
                            l = a.slice();
                        return i = i[r.cache], f(r.props, function(t, o) {
                            var r = o.idx,
                                s = a[r],
                                c = i[r],
                                u = d[o.type] || {};
                            null !== c && (null === s ? l[r] = c : (u.mod && (c - s > u.mod / 2 ? s += u.mod : s - c > u.mod / 2 && (s -= u.mod)), l[r] = n((c - s) * e + s, o)))
                        }), this[o](l)
                    },
                    blend: function(e) {
                        if (1 === this._rgba[3]) return this;
                        var n = this._rgba.slice(),
                            i = n.pop(),
                            o = c(e)._rgba;
                        return c(t.map(n, function(t, e) {
                            return (1 - i) * o[e] + i * t
                        }))
                    },
                    toRgbaString: function() {
                        var e = "rgba(",
                            n = t.map(this._rgba, function(t, e) {
                                return null == t ? e > 2 ? 1 : 0 : t
                            });
                        return 1 === n[3] && (n.pop(), e = "rgb("), e + n.join() + ")"
                    },
                    toHslaString: function() {
                        var e = "hsla(",
                            n = t.map(this.hsla(), function(t, e) {
                                return null == t && (t = e > 2 ? 1 : 0), e && 3 > e && (t = Math.round(100 * t) + "%"), t
                            });
                        return 1 === n[3] && (n.pop(), e = "hsl("), e + n.join() + ")"
                    },
                    toHexString: function(e) {
                        var n = this._rgba.slice(),
                            i = n.pop();
                        return e && n.push(~~(255 * i)), "#" + t.map(n, function(t) {
                            return t = (t || 0).toString(16), 1 === t.length ? "0" + t : t
                        }).join("")
                    },
                    toString: function() {
                        return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
                    }
                }), c.fn.parse.prototype = c.fn, u.hsla.to = function(t) {
                    if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
                    var e, n, i = t[0] / 255,
                        o = t[1] / 255,
                        r = t[2] / 255,
                        s = t[3],
                        a = Math.max(i, o, r),
                        l = Math.min(i, o, r),
                        c = a - l,
                        u = a + l,
                        d = .5 * u;
                    return e = l === a ? 0 : i === a ? 60 * (o - r) / c + 360 : o === a ? 60 * (r - i) / c + 120 : 60 * (i - o) / c + 240, n = 0 === c ? 0 : .5 >= d ? c / u : c / (2 - u), [Math.round(e) % 360, n, d, null == s ? 1 : s]
                }, u.hsla.from = function(t) {
                    if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
                    var e = t[0] / 360,
                        n = t[1],
                        i = t[2],
                        r = t[3],
                        s = .5 >= i ? i * (1 + n) : i + n - i * n,
                        a = 2 * i - s;
                    return [Math.round(255 * o(a, s, e + 1 / 3)), Math.round(255 * o(a, s, e)), Math.round(255 * o(a, s, e - 1 / 3)), r]
                }, f(u, function(i, o) {
                    var r = o.props,
                        s = o.cache,
                        l = o.to,
                        u = o.from;
                    c.fn[i] = function(i) {
                        if (l && !this[s] && (this[s] = l(this._rgba)), i === e) return this[s].slice();
                        var o, a = t.type(i),
                            d = "array" === a || "object" === a ? i : arguments,
                            h = this[s].slice();
                        return f(r, function(t, e) {
                            var i = d["object" === a ? t : e.idx];
                            null == i && (i = h[e.idx]), h[e.idx] = n(i, e)
                        }), u ? (o = c(u(h)), o[s] = h, o) : c(h)
                    }, f(r, function(e, n) {
                        c.fn[e] || (c.fn[e] = function(o) {
                            var r, s = t.type(o),
                                l = "alpha" === e ? this._hsla ? "hsla" : "rgba" : i,
                                c = this[l](),
                                u = c[n.idx];
                            return "undefined" === s ? u : ("function" === s && (o = o.call(this, u), s = t.type(o)), null == o && n.empty ? this : ("string" === s && (r = a.exec(o), r && (o = u + parseFloat(r[2]) * ("+" === r[1] ? 1 : -1))), c[n.idx] = o, this[l](c)))
                        })
                    })
                }), c.hook = function(e) {
                    var n = e.split(" ");
                    f(n, function(e, n) {
                        t.cssHooks[n] = {
                            set: function(e, o) {
                                var r, s, a = "";
                                if ("transparent" !== o && ("string" !== t.type(o) || (r = i(o)))) {
                                    if (o = c(r || o), !h.rgba && 1 !== o._rgba[3]) {
                                        for (s = "backgroundColor" === n ? e.parentNode : e;
                                            ("" === a || "transparent" === a) && s && s.style;) try {
                                            a = t.css(s, "backgroundColor"), s = s.parentNode
                                        } catch (l) {}
                                        o = o.blend(a && "transparent" !== a ? a : "_default")
                                    }
                                    o = o.toRgbaString()
                                }
                                try {
                                    e.style[n] = o
                                } catch (l) {}
                            }
                        }, t.fx.step[n] = function(e) {
                            e.colorInit || (e.start = c(e.elem, n), e.end = c(e.end), e.colorInit = !0), t.cssHooks[n].set(e.elem, e.start.transition(e.end, e.pos))
                        }
                    })
                }, c.hook(s), t.cssHooks.borderColor = {
                    expand: function(t) {
                        var e = {};
                        return f(["Top", "Right", "Bottom", "Left"], function(n, i) {
                            e["border" + i + "Color"] = t
                        }), e
                    }
                }, r = t.Color.names = {
                    aqua: "#00ffff",
                    black: "#000000",
                    blue: "#0000ff",
                    fuchsia: "#ff00ff",
                    gray: "#808080",
                    green: "#008000",
                    lime: "#00ff00",
                    maroon: "#800000",
                    navy: "#000080",
                    olive: "#808000",
                    purple: "#800080",
                    red: "#ff0000",
                    silver: "#c0c0c0",
                    teal: "#008080",
                    white: "#ffffff",
                    yellow: "#ffff00",
                    transparent: [null, null, null, 0],
                    _default: "#ffffff"
                }
            }(jQuery),
            function() {
                function n(e) {
                    var n, i, o = e.ownerDocument.defaultView ? e.ownerDocument.defaultView.getComputedStyle(e, null) : e.currentStyle,
                        r = {};
                    if (o && o.length && o[0] && o[o[0]])
                        for (i = o.length; i--;) n = o[i], "string" == typeof o[n] && (r[t.camelCase(n)] = o[n]);
                    else
                        for (n in o) "string" == typeof o[n] && (r[n] = o[n]);
                    return r
                }

                function i(e, n) {
                    var i, o, s = {};
                    for (i in n) o = n[i], e[i] !== o && (r[i] || (t.fx.step[i] || !isNaN(parseFloat(o))) && (s[i] = o));
                    return s
                }
                var o = ["add", "remove", "toggle"],
                    r = {
                        border: 1,
                        borderBottom: 1,
                        borderColor: 1,
                        borderLeft: 1,
                        borderRight: 1,
                        borderTop: 1,
                        borderWidth: 1,
                        margin: 1,
                        padding: 1
                    };
                t.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function(e, n) {
                    t.fx.step[n] = function(t) {
                        ("none" !== t.end && !t.setAttr || 1 === t.pos && !t.setAttr) && (jQuery.style(t.elem, n, t.end), t.setAttr = !0)
                    }
                }), t.fn.addBack || (t.fn.addBack = function(t) {
                    return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
                }), t.effects.animateClass = function(e, r, s, a) {
                    var l = t.speed(r, s, a);
                    return this.queue(function() {
                        var r, s = t(this),
                            a = s.attr("class") || "",
                            c = l.children ? s.find("*").addBack() : s;
                        c = c.map(function() {
                            var e = t(this);
                            return {
                                el: e,
                                start: n(this)
                            }
                        }), r = function() {
                            t.each(o, function(t, n) {
                                e[n] && s[n + "Class"](e[n])
                            })
                        }, r(), c = c.map(function() {
                            return this.end = n(this.el[0]), this.diff = i(this.start, this.end), this
                        }), s.attr("class", a), c = c.map(function() {
                            var e = this,
                                n = t.Deferred(),
                                i = t.extend({}, l, {
                                    queue: !1,
                                    complete: function() {
                                        n.resolve(e)
                                    }
                                });
                            return this.el.animate(this.diff, i), n.promise()
                        }), t.when.apply(t, c.get()).done(function() {
                            r(), t.each(arguments, function() {
                                var e = this.el;
                                t.each(this.diff, function(t) {
                                    e.css(t, "")
                                })
                            }), l.complete.call(s[0])
                        })
                    })
                }, t.fn.extend({
                    addClass: function(e) {
                        return function(n, i, o, r) {
                            return i ? t.effects.animateClass.call(this, {
                                add: n
                            }, i, o, r) : e.apply(this, arguments)
                        }
                    }(t.fn.addClass),
                    removeClass: function(e) {
                        return function(n, i, o, r) {
                            return arguments.length > 1 ? t.effects.animateClass.call(this, {
                                remove: n
                            }, i, o, r) : e.apply(this, arguments)
                        }
                    }(t.fn.removeClass),
                    toggleClass: function(n) {
                        return function(i, o, r, s, a) {
                            return "boolean" == typeof o || o === e ? r ? t.effects.animateClass.call(this, o ? {
                                add: i
                            } : {
                                remove: i
                            }, r, s, a) : n.apply(this, arguments) : t.effects.animateClass.call(this, {
                                toggle: i
                            }, o, r, s)
                        }
                    }(t.fn.toggleClass),
                    switchClass: function(e, n, i, o, r) {
                        return t.effects.animateClass.call(this, {
                            add: n,
                            remove: e
                        }, i, o, r)
                    }
                })
            }(),
            function() {
                function i(e, n, i, o) {
                    return t.isPlainObject(e) && (n = e, e = e.effect), e = {
                        effect: e
                    }, null == n && (n = {}), t.isFunction(n) && (o = n, i = null, n = {}), ("number" == typeof n || t.fx.speeds[n]) && (o = i, i = n, n = {}), t.isFunction(i) && (o = i, i = null), n && t.extend(e, n), i = i || n.duration, e.duration = t.fx.off ? 0 : "number" == typeof i ? i : i in t.fx.speeds ? t.fx.speeds[i] : t.fx.speeds._default, e.complete = o || n.complete, e
                }

                function o(e) {
                    return !e || "number" == typeof e || t.fx.speeds[e] ? !0 : "string" != typeof e || t.effects.effect[e] ? t.isFunction(e) ? !0 : "object" != typeof e || e.effect ? !1 : !0 : !0
                }
                t.extend(t.effects, {
                    version: "1.10.3",
                    save: function(t, e) {
                        for (var i = 0; i < e.length; i++) null !== e[i] && t.data(n + e[i], t[0].style[e[i]])
                    },
                    restore: function(t, i) {
                        var o, r;
                        for (r = 0; r < i.length; r++) null !== i[r] && (o = t.data(n + i[r]), o === e && (o = ""), t.css(i[r], o))
                    },
                    setMode: function(t, e) {
                        return "toggle" === e && (e = t.is(":hidden") ? "show" : "hide"), e
                    },
                    getBaseline: function(t, e) {
                        var n, i;
                        switch (t[0]) {
                            case "top":
                                n = 0;
                                break;
                            case "middle":
                                n = .5;
                                break;
                            case "bottom":
                                n = 1;
                                break;
                            default:
                                n = t[0] / e.height
                        }
                        switch (t[1]) {
                            case "left":
                                i = 0;
                                break;
                            case "center":
                                i = .5;
                                break;
                            case "right":
                                i = 1;
                                break;
                            default:
                                i = t[1] / e.width
                        }
                        return {
                            x: i,
                            y: n
                        }
                    },
                    createWrapper: function(e) {
                        if (e.parent().is(".ui-effects-wrapper")) return e.parent();
                        var n = {
                                width: e.outerWidth(!0),
                                height: e.outerHeight(!0),
                                "float": e.css("float")
                            },
                            i = t("<div></div>").addClass("ui-effects-wrapper").css({
                                fontSize: "100%",
                                background: "transparent",
                                border: "none",
                                margin: 0,
                                padding: 0
                            }),
                            o = {
                                width: e.width(),
                                height: e.height()
                            },
                            r = document.activeElement;
                        try {
                            r.id
                        } catch (s) {
                            r = document.body
                        }
                        return e.wrap(i), (e[0] === r || t.contains(e[0], r)) && t(r).focus(), i = e.parent(), "static" === e.css("position") ? (i.css({
                            position: "relative"
                        }), e.css({
                            position: "relative"
                        })) : (t.extend(n, {
                            position: e.css("position"),
                            zIndex: e.css("z-index")
                        }), t.each(["top", "left", "bottom", "right"], function(t, i) {
                            n[i] = e.css(i), isNaN(parseInt(n[i], 10)) && (n[i] = "auto")
                        }), e.css({
                            position: "relative",
                            top: 0,
                            left: 0,
                            right: "auto",
                            bottom: "auto"
                        })), e.css(o), i.css(n).show()
                    },
                    removeWrapper: function(e) {
                        var n = document.activeElement;
                        return e.parent().is(".ui-effects-wrapper") && (e.parent().replaceWith(e), (e[0] === n || t.contains(e[0], n)) && t(n).focus()), e
                    },
                    setTransition: function(e, n, i, o) {
                        return o = o || {}, t.each(n, function(t, n) {
                            var r = e.cssUnit(n);
                            r[0] > 0 && (o[n] = r[0] * i + r[1])
                        }), o
                    }
                }), t.fn.extend({
                    effect: function() {
                        function e(e) {
                            function i() {
                                t.isFunction(r) && r.call(o[0]), t.isFunction(e) && e()
                            }
                            var o = t(this),
                                r = n.complete,
                                a = n.mode;
                            (o.is(":hidden") ? "hide" === a : "show" === a) ? (o[a](), i()) : s.call(o[0], n, i)
                        }
                        var n = i.apply(this, arguments),
                            o = n.mode,
                            r = n.queue,
                            s = t.effects.effect[n.effect];
                        return t.fx.off || !s ? o ? this[o](n.duration, n.complete) : this.each(function() {
                            n.complete && n.complete.call(this)
                        }) : r === !1 ? this.each(e) : this.queue(r || "fx", e)
                    },
                    show: function(t) {
                        return function(e) {
                            if (o(e)) return t.apply(this, arguments);
                            var n = i.apply(this, arguments);
                            return n.mode = "show", this.effect.call(this, n)
                        }
                    }(t.fn.show),
                    hide: function(t) {
                        return function(e) {
                            if (o(e)) return t.apply(this, arguments);
                            var n = i.apply(this, arguments);
                            return n.mode = "hide", this.effect.call(this, n)
                        }
                    }(t.fn.hide),
                    toggle: function(t) {
                        return function(e) {
                            if (o(e) || "boolean" == typeof e) return t.apply(this, arguments);
                            var n = i.apply(this, arguments);
                            return n.mode = "toggle", this.effect.call(this, n)
                        }
                    }(t.fn.toggle),
                    cssUnit: function(e) {
                        var n = this.css(e),
                            i = [];
                        return t.each(["em", "px", "%", "pt"], function(t, e) {
                            n.indexOf(e) > 0 && (i = [parseFloat(n), e])
                        }), i
                    }
                })
            }(),
            function() {
                var e = {};
                t.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(t, n) {
                    e[n] = function(e) {
                        return Math.pow(e, t + 2)
                    }
                }), t.extend(e, {
                    Sine: function(t) {
                        return 1 - Math.cos(t * Math.PI / 2)
                    },
                    Circ: function(t) {
                        return 1 - Math.sqrt(1 - t * t)
                    },
                    Elastic: function(t) {
                        return 0 === t || 1 === t ? t : -Math.pow(2, 8 * (t - 1)) * Math.sin((80 * (t - 1) - 7.5) * Math.PI / 15)
                    },
                    Back: function(t) {
                        return t * t * (3 * t - 2)
                    },
                    Bounce: function(t) {
                        for (var e, n = 4; t < ((e = Math.pow(2, --n)) - 1) / 11;);
                        return 1 / Math.pow(4, 3 - n) - 7.5625 * Math.pow((3 * e - 2) / 22 - t, 2)
                    }
                }), t.each(e, function(e, n) {
                    t.easing["easeIn" + e] = n, t.easing["easeOut" + e] = function(t) {
                        return 1 - n(1 - t)
                    }, t.easing["easeInOut" + e] = function(t) {
                        return .5 > t ? n(2 * t) / 2 : 1 - n(-2 * t + 2) / 2
                    }
                })
            }()
    }(jQuery),
    /*!
     * jQuery UI Effects Fade 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/fade-effect/
     *
     * Depends:
     *	jquery.ui.effect.js
     */
    function(t) {
        t.effects.effect.fade = function(e, n) {
            var i = t(this),
                o = t.effects.setMode(i, e.mode || "toggle");
            i.animate({
                opacity: o
            }, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: n
            })
        }
    }(jQuery),
    /*!
     * jQuery UI Effects Slide 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/slide-effect/
     *
     * Depends:
     *	jquery.ui.effect.js
     */
    function(t) {
        t.effects.effect.slide = function(e, n) {
            var i, o = t(this),
                r = ["position", "top", "bottom", "left", "right", "width", "height"],
                s = t.effects.setMode(o, e.mode || "show"),
                a = "show" === s,
                l = e.direction || "left",
                c = "up" === l || "down" === l ? "top" : "left",
                u = "up" === l || "left" === l,
                d = {};
            t.effects.save(o, r), o.show(), i = e.distance || o["top" === c ? "outerHeight" : "outerWidth"](!0), t.effects.createWrapper(o).css({
                overflow: "hidden"
            }), a && o.css(c, u ? isNaN(i) ? "-" + i : -i : i), d[c] = (a ? u ? "+=" : "-=" : u ? "-=" : "+=") + i, o.animate(d, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: function() {
                    "hide" === s && o.hide(), t.effects.restore(o, r), t.effects.removeWrapper(o), n()
                }
            })
        }
    }(jQuery),
    /*!
     * jQuery UI Effects Shake 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/shake-effect/
     *
     * Depends:
     *	jquery.ui.effect.js
     */
    function(t) {
        t.effects.effect.shake = function(e, n) {
            var i, o = t(this),
                r = ["position", "top", "bottom", "left", "right", "height", "width"],
                s = t.effects.setMode(o, e.mode || "effect"),
                a = e.direction || "left",
                l = e.distance || 20,
                c = e.times || 3,
                u = 2 * c + 1,
                d = Math.round(e.duration / u),
                h = "up" === a || "down" === a ? "top" : "left",
                p = "up" === a || "left" === a,
                f = {},
                m = {},
                g = {},
                v = o.queue(),
                b = v.length;
            for (t.effects.save(o, r), o.show(), t.effects.createWrapper(o), f[h] = (p ? "-=" : "+=") + l, m[h] = (p ? "+=" : "-=") + 2 * l, g[h] = (p ? "-=" : "+=") + 2 * l, o.animate(f, d, e.easing), i = 1; c > i; i++) o.animate(m, d, e.easing).animate(g, d, e.easing);
            o.animate(m, d, e.easing).animate(f, d / 2, e.easing).queue(function() {
                "hide" === s && o.hide(), t.effects.restore(o, r), t.effects.removeWrapper(o), n()
            }), b > 1 && v.splice.apply(v, [1, 0].concat(v.splice(b, u + 1))), o.dequeue()
        }
    }(jQuery),
    /*!
     * jQuery UI Effects Highlight 1.10.3
     * http://jqueryui.com
     *
     * Copyright 2013 jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/highlight-effect/
     *
     * Depends:
     *	jquery.ui.effect.js
     */
    function(t) {
        t.effects.effect.highlight = function(e, n) {
            var i = t(this),
                o = ["backgroundImage", "backgroundColor", "opacity"],
                r = t.effects.setMode(i, e.mode || "show"),
                s = {
                    backgroundColor: i.css("backgroundColor")
                };
            "hide" === r && (s.opacity = 0), t.effects.save(i, o), i.show().css({
                backgroundImage: "none",
                backgroundColor: e.color || "#ffff99"
            }).animate(s, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: function() {
                    "hide" === r && i.hide(), t.effects.restore(i, o), n()
                }
            })
        }
    }(jQuery),
    function(t) {
        window.NestedFormEvents = function() {
            this.addFields = t.proxy(this.addFields, this), this.removeFields = t.proxy(this.removeFields, this)
        }, NestedFormEvents.prototype = {
            addFields: function(e) {
                var n = e.currentTarget,
                    i = t(n).data("association"),
                    o = t("#" + t(n).data("blueprint-id")),
                    r = o.data("blueprint"),
                    s = (t(n).closest(".fields").closestChild("input, textarea, select").eq(0).attr("name") || "").replace(new RegExp("[[a-z_]+]$"), "");
                if (s)
                    for (var a = s.match(/[a-z_]+_attributes(?=\]\[(new_)?\d+\])/g) || [], l = s.match(/[0-9]+/g) || [], c = 0; c < a.length; c++) l[c] && (r = r.replace(new RegExp("(_" + a[c] + ")_.+?_", "g"), "$1_" + l[c] + "_"), r = r.replace(new RegExp("(\\[" + a[c] + "\\])\\[.+?\\]", "g"), "$1[" + l[c] + "]"));
                var u = new RegExp("new_" + i, "g"),
                    d = this.newId();
                r = t.trim(r.replace(u, d));
                var h = this.insertFields(r, i, n);
                return h.trigger({
                    type: "nested:fieldAdded",
                    field: h
                }).trigger({
                    type: "nested:fieldAdded:" + i,
                    field: h
                }), !1
            },
            newId: function() {
                return (new Date).getTime()
            },
            insertFields: function(e, n, i) {
                var o = t(i).data("target");
                return o ? t(e).appendTo(t(o)) : t(e).insertBefore(i)
            },
            removeFields: function(e) {
                var n = t(e.currentTarget),
                    i = n.data("association"),
                    o = n.prev("input[type=hidden]");
                o.val("1");
                var r = n.closest(".fields");
                return r.hide(), r.trigger({
                    type: "nested:fieldRemoved",
                    field: r
                }).trigger({
                    type: "nested:fieldRemoved:" + i,
                    field: r
                }), !1
            }
        }, window.nestedFormEvents = new NestedFormEvents, t(document).delegate("form a.add_nested_fields", "click", nestedFormEvents.addFields).delegate("form a.remove_nested_fields", "click", nestedFormEvents.removeFields)
    }(jQuery),
    /*
     * Copyright 2011, Tobias Lindig
     *
     * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
     * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
     *
     */
    function(t) {
        t.fn.closestChild = function(e) {
            if (e && "" != e) {
                var n = [];
                for (n.push(this); n.length > 0;)
                    for (var i = n.shift(), o = i.children(), r = 0; r < o.length; ++r) {
                        var s = t(o[r]);
                        if (s.is(e)) return s;
                        n.push(s)
                    }
            }
            return t()
        }
    }(jQuery),
    function(t) {
        "use strict";
        t.ajaxPrefilter(function(t) {
            return t.iframe ? "iframe" : void 0
        }), t.ajaxTransport("iframe", function(e, n, i) {
            function o() {
                u.prop("disabled", !1), s.remove(), a.bind("load", function() {
                    a.remove()
                }), a.attr("src", "javascript:false;")
            }
            var r, s = null,
                a = null,
                l = "iframe-" + t.now(),
                c = t(e.files).filter(":file:enabled"),
                u = null;
            return e.dataTypes.shift(), c.length ? (s = t("<form enctype='multipart/form-data' method='post'></form>").hide().attr({
                action: e.url,
                target: l
            }), "string" == typeof e.data && e.data.length > 0 && t.error("data must not be serialized"), t.each(e.data || {}, function(e, n) {
                t.isPlainObject(n) && (e = n.name, n = n.value), t("<input type='hidden' />").attr({
                    name: e,
                    value: n
                }).appendTo(s)
            }), t("<input type='hidden' value='IFrame' name='X-Requested-With' />").appendTo(s), r = e.dataTypes[0] && e.accepts[e.dataTypes[0]] ? e.accepts[e.dataTypes[0]] + ("*" !== e.dataTypes[0] ? ", */*; q=0.01" : "") : e.accepts["*"], t("<input type='hidden' name='X-Http-Accept'>").attr("value", r).appendTo(s), u = c.after(function() {
                return t(this).clone().prop("disabled", !0)
            }).next(), c.appendTo(s), {
                send: function(e, n) {
                    a = t("<iframe src='javascript:false;' name='" + l + "' id='" + l + "' style='display:none'></iframe>"), a.bind("load", function() {
                        a.unbind("load").bind("load", function() {
                            var t = this.contentWindow ? this.contentWindow.document : this.contentDocument ? this.contentDocument : this.document,
                                e = t.documentElement ? t.documentElement : t.body,
                                r = e.getElementsByTagName("textarea")[0],
                                s = r && r.getAttribute("data-type") || null,
                                a = r && r.getAttribute("data-status") || 200,
                                l = r && r.getAttribute("data-statusText") || "OK",
                                c = {
                                    html: e.innerHTML,
                                    text: s ? r.value : e ? e.textContent || e.innerText : null
                                };
                            o(), i.responseText || (i.responseText = c.text), n(a, l, c, s ? "Content-Type: " + s : null)
                        }), s[0].submit()
                    }), t("body").append(s, a)
                },
                abort: function() {
                    null !== a && (a.unbind("load").attr("src", "javascript:false;"), o())
                }
            }) : void 0
        })
    }(jQuery),
    function(t) {
        var e;
        t.remotipart = e = {
            setup: function(n) {
                var i = n.data("ujs:submit-button"),
                    o = t('meta[name="csrf-param"]').attr("content"),
                    r = t('meta[name="csrf-token"]').attr("content"),
                    s = n.find('input[name="' + o + '"]').length;
                n.one("ajax:beforeSend.remotipart", function(a, l, c) {
                    return delete c.beforeSend, c.iframe = !0, c.files = t(t.rails.fileInputSelector, n), c.data = n.serializeArray(), i && c.data.push(i), c.files.each(function(t, e) {
                        for (var n = c.data.length - 1; n >= 0; n--) c.data[n].name == e.name && c.data.splice(n, 1)
                    }), c.processData = !1, void 0 === c.dataType && (c.dataType = "script *"), c.data.push({
                        name: "remotipart_submitted",
                        value: !0
                    }), r && o && !s && c.data.push({
                        name: o,
                        value: r
                    }), t.rails.fire(n, "ajax:remotipartSubmit", [l, c]) && (t.rails.ajax(c), setTimeout(function() {
                        t.rails.disableFormElements(n)
                    }, 20)), e.teardown(n), !1
                }).data("remotipartSubmitted", !0)
            },
            teardown: function(t) {
                t.unbind("ajax:beforeSend.remotipart").removeData("remotipartSubmitted")
            }
        }, t(document).on("ajax:aborted:file", "form", function() {
            var n = t(this);
            return e.setup(n), t.rails.handleRemote(n), !1
        })
    }(jQuery),
    /*
     * HACK: this is hacked to remove row-related functions, since row hover
     * can be done through pure CSS
     *
     * jQuery tableHover plugin
     * Version: 0.1.4
     *
     * Copyright (c) 2007 Roman Weich
     * http://p.sohei.org
     */
    function(t) {
        var e = function(t) {
            for (var e = t.rows, n = e.length, i = [], o = 0; n > o; o++)
                for (var r = e[o].cells, s = r.length, a = 0; s > a; a++) {
                    var l = r[a],
                        c = l.rowSpan || 1,
                        u = l.colSpan || 1,
                        d = -1;
                    i[o] || (i[o] = []);
                    for (var h = i[o]; h[++d];);
                    l.realIndex = d;
                    for (var p = o; o + c > p; p++) {
                        i[p] || (i[p] = []);
                        for (var f = i[p], m = d; d + u > m; m++) f[m] = 1
                    }
                }
        };
        t.fn.tableHover = function(n) {
            var i = t.extend({
                allowHead: !0,
                allowBody: !0,
                allowFoot: !0,
                headRows: !1,
                bodyRows: !0,
                footRows: !1,
                spanRows: !0,
                headCols: !1,
                bodyCols: !0,
                footCols: !1,
                spanCols: !0,
                ignoreCols: [],
                headCells: !1,
                bodyCells: !0,
                footCells: !1,
                rowClass: "hover",
                colClass: "",
                cellClass: "",
                clickClass: ""
            }, n);
            return this.each(function() {
                var n, o = [],
                    r = [],
                    s = this,
                    a = 0;
                if (s.tBodies && s.tBodies.length) {
                    var l = function(e, n) {
                            var r, s, l, c, u, d;
                            for (l = 0; l < e.length; l++, a++)
                                for (s = e[l], c = 0; c < s.cells.length; c++) {
                                    if (r = s.cells[c], "TBODY" == n && i.bodyCols || "THEAD" == n && i.headCols || "TFOOT" == n && i.footCols)
                                        for (d = r.colSpan; --d >= 0 && (u = r.realIndex + d, !(t.inArray(u + 1, i.ignoreCols) > -1));) o[u] || (o[u] = []), o[u].push(r);
                                    ("TBODY" == n && i.allowBody || "THEAD" == n && i.allowHead || "TFOOT" == n && i.allowFoot) && (r.thover = !0)
                                }
                        },
                        c = function(t) {
                            for (var e = t.target; e != this && e.thover !== !0;) e = e.parentNode;
                            e.thover === !0 && d(e, !0)
                        },
                        u = function(t) {
                            for (var e = t.target; e != this && e.thover !== !0;) e = e.parentNode;
                            e.thover === !0 && d(e, !1)
                        },
                        d = function(e, n) {
                            t.fn.tableHoverHover = n ? t.fn.addClass : t.fn.removeClass;
                            var r = o[e.realIndex] || [],
                                s = 0;
                            if ("" != i.colClass) {
                                for (; i.spanCols && ++s < e.colSpan && o[e.realIndex + s];) r = r.concat(o[e.realIndex + s]);
                                t(r).tableHoverHover(i.colClass)
                            }
                        };
                    for (e(s), n = 0; n < s.rows.length; n++) r[n] = [];
                    for (s.tHead && l(s.tHead.rows, "THEAD"), n = 0; n < s.tBodies.length; n++) l(s.tBodies[n].rows, "TBODY");
                    s.tFoot && l(s.tFoot.rows, "TFOOT"), t(this).bind("mouseover", c).bind("mouseout", u)
                }
            })
        }
    }(jQuery),
    /*!
     * Bootstrap v3.0.0
     *
     * Copyright 2013 Twitter, Inc
     * Licensed under the Apache License v2.0
     * http://www.apache.org/licenses/LICENSE-2.0
     *
     * Designed and built with all the love in the world @twitter by @mdo and @fat.
     */
    + function(t) {
        "use strict";
        var e = "Microsoft Internet Explorer" == window.navigator.appName,
            n = function(e, n) {
                if (this.$element = t(e), this.$input = this.$element.find(":file"), 0 !== this.$input.length) {
                    this.name = this.$input.attr("name") || n.name, this.$hidden = this.$element.find('input[type=hidden][name="' + this.name + '"]'), 0 === this.$hidden.length && (this.$hidden = t('<input type="hidden" />'), this.$element.prepend(this.$hidden)), this.$preview = this.$element.find(".fileinput-preview");
                    var i = this.$preview.css("height");
                    "inline" != this.$preview.css("display") && "0px" != i && "none" != i && this.$preview.css("line-height", i), this.original = {
                        exists: this.$element.hasClass("fileinput-exists"),
                        preview: this.$preview.html(),
                        hiddenVal: this.$hidden.val()
                    }, this.listen()
                }
            };
        n.prototype.listen = function() {
            this.$input.on("change.bs.fileinput", t.proxy(this.change, this)), t(this.$input[0].form).on("reset.bs.fileinput", t.proxy(this.reset, this)), this.$element.find('[data-trigger="fileinput"]').on("click.bs.fileinput", t.proxy(this.trigger, this)), this.$element.find('[data-dismiss="fileinput"]').on("click.bs.fileinput", t.proxy(this.clear, this))
        }, n.prototype.change = function(e) {
            if (void 0 === e.target.files && (e.target.files = e.target && e.target.value ? [{
                    name: e.target.value.replace(/^.+\\/, "")
                }] : []), 0 !== e.target.files.length) {
                this.$hidden.val(""), this.$hidden.attr("name", ""), this.$input.attr("name", this.name);
                var n = e.target.files[0];
                if (this.$preview.length > 0 && ("undefined" != typeof n.type ? n.type.match("image.*") : n.name.match(/\.(gif|png|jpe?g)$/i)) && "undefined" != typeof FileReader) {
                    var i = new FileReader,
                        o = this.$preview,
                        r = this.$element;
                    i.onload = function(i) {
                        var s = t("<img>").attr("src", i.target.result);
                        e.target.files[0].result = i.target.result, r.find(".fileinput-filename").text(n.name), "none" != o.css("max-height") && s.css("max-height", parseInt(o.css("max-height"), 10) - parseInt(o.css("padding-top"), 10) - parseInt(o.css("padding-bottom"), 10) - parseInt(o.css("border-top"), 10) - parseInt(o.css("border-bottom"), 10)), o.html(s), r.addClass("fileinput-exists").removeClass("fileinput-new"), r.trigger("change.bs.fileinput", e.target.files)
                    }, i.readAsDataURL(n)
                } else this.$element.find(".fileinput-filename").text(n.name), this.$preview.text(n.name), this.$element.addClass("fileinput-exists").removeClass("fileinput-new"), this.$element.trigger("change.bs.fileinput")
            }
        }, n.prototype.clear = function(t) {
            if (t && t.preventDefault(), this.$hidden.val(""), this.$hidden.attr("name", this.name), this.$input.attr("name", ""), e) {
                var n = this.$input.clone(!0);
                this.$input.after(n), this.$input.remove(), this.$input = n
            } else this.$input.val("");
            this.$preview.html(""), this.$element.find(".fileinput-filename").text(""), this.$element.addClass("fileinput-new").removeClass("fileinput-exists"), t !== !1 && (this.$input.trigger("change"), this.$element.trigger("clear.bs.fileinput"))
        }, n.prototype.reset = function() {
            this.clear(!1), this.$hidden.val(this.original.hiddenVal), this.$preview.html(this.original.preview), this.$element.find(".fileinput-filename").text(""), this.original.exists ? this.$element.addClass("fileinput-exists").removeClass("fileinput-new") : this.$element.addClass("fileinput-new").removeClass("fileinput-exists"), this.$element.trigger("reset.bs.fileinput")
        }, n.prototype.trigger = function(t) {
            this.$input.trigger("click"), t.preventDefault()
        }, t.fn.fileinput = function(e) {
            return this.each(function() {
                var i = t(this),
                    o = i.data("fileinput");
                o || i.data("fileinput", o = new n(this, e)), "string" == typeof e && o[e]()
            })
        }, t.fn.fileinput.Constructor = n, t(document).on("click.fileinput.data-api", '[data-provides="fileinput"]', function(e) {
            var n = t(this);
            if (!n.data("fileinput")) {
                n.fileinput(n.data());
                var i = t(e.target).closest('[data-dismiss="fileinput"],[data-trigger="fileinput"]');
                i.length > 0 && (e.preventDefault(), i.trigger("click.bs.fileinput"))
            }
        })
    }(window.jQuery),
    function() {
        var t;
        t = function() {
            function t() {}
            return $(document).keypress(function(t) {
                var e;
                return e = $(document.activeElement).is("input:focus,textarea:focus,[contenteditable]:focus"), !(8 === t.keyCode && !e)
            }), $(document).keypress(function(t) {
                var e;
                return e = t.target ? t.target : t.srcElement ? t.srcElement : null, !(13 === t.keyCode && "text" === e.type)
            }), t
        }(), window.ShiftKey = function() {
            function t() {}
            return t.down = !1, $(document).keydown(function(e) {
                return 16 === e.keyCode || 16 === e.charCode ? t.down = !0 : void 0
            }), $(document).keyup(function(e) {
                return 16 === e.keyCode || 16 === e.charCode ? t.down = !1 : void 0
            }), t
        }()
    }.call(this), jQuery.browser = {}, jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase()), jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !jQuery.browser.webkit, jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase()), jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase()), jQuery.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase()), jQuery.browser.windows = /win/.test(navigator.appVersion.toLowerCase()), jQuery.browser.mac = /mac/.test(navigator.appVersion.toLowerCase()), jQuery.browser.linux = /linux|x11/.test(navigator.appVersion.toLowerCase()), jQuery.browser.ios = /ipad|iphone|ipod/.test(navigator.userAgent.toLowerCase()), jQuery.browser.android = /android/.test(navigator.userAgent.toLowerCase()), navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style");
    msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}")), document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
} /*! jQuery Mobile v1.3.2 | Copyright 2010, 2013 jQuery Foundation, Inc. | jquery.org/license */ ! function(t, e, n) {
    "function" == typeof define && define.amd ? define(["jquery"], function(i) {
        return n(i, t, e), i.mobile
    }) : n(t.jQuery, t, e)
}(this, document, function(t, e, n) {
    ! function(t, e, n, i) {
        function o(t) {
            for (; t && "undefined" != typeof t.originalEvent;) t = t.originalEvent;
            return t
        }

        function r(e, n) {
            var r, s, a, l, c, u, d, h, p, f = e.type;
            if (e = t.Event(e), e.type = n, r = e.originalEvent, s = t.event.props, f.search(/^(mouse|click)/) > -1 && (s = D), r)
                for (d = s.length, l; d;) l = s[--d], e[l] = r[l];
            if (f.search(/mouse(down|up)|click/) > -1 && !e.which && (e.which = 1), -1 !== f.search(/^touch/) && (a = o(r), f = a.touches, c = a.changedTouches, u = f && f.length ? f[0] : c && c.length ? c[0] : i, u))
                for (h = 0, p = S.length; p > h; h++) l = S[h], e[l] = u[l];
            return e
        }

        function s(e) {
            for (var n, i, o = {}; e;) {
                n = t.data(e, k);
                for (i in n) n[i] && (o[i] = o.hasVirtualBinding = !0);
                e = e.parentNode
            }
            return o
        }

        function a(e, n) {
            for (var i; e;) {
                if (i = t.data(e, k), i && (!n || i[n])) return e;
                e = e.parentNode
            }
            return null
        }

        function l() {
            z = !1
        }

        function c() {
            z = !0
        }

        function u() {
            W = 0, H.length = 0, I = !1, c()
        }

        function d() {
            l()
        }

        function h() {
            p(), P = setTimeout(function() {
                P = 0, u()
            }, t.vmouse.resetTimerDuration)
        }

        function p() {
            P && (clearTimeout(P), P = 0)
        }

        function f(e, n, i) {
            var o;
            return (i && i[e] || !i && a(n.target, e)) && (o = r(n, e), t(n.target).trigger(o)), o
        }

        function m(e) {
            var n = t.data(e.target, T);
            if (!(I || W && W === n)) {
                var i = f("v" + e.type, e);
                i && (i.isDefaultPrevented() && e.preventDefault(), i.isPropagationStopped() && e.stopPropagation(), i.isImmediatePropagationStopped() && e.stopImmediatePropagation())
            }
        }

        function g(e) {
            var n, i, r = o(e).touches;
            if (r && 1 === r.length && (n = e.target, i = s(n), i.hasVirtualBinding)) {
                W = F++, t.data(n, T, W), p(), d(), M = !1;
                var a = o(e).touches[0];
                A = a.pageX, N = a.pageY, f("vmouseover", e, i), f("vmousedown", e, i)
            }
        }

        function v(t) {
            z || (M || f("vmousecancel", t, s(t.target)), M = !0, h())
        }

        function b(e) {
            if (!z) {
                var n = o(e).touches[0],
                    i = M,
                    r = t.vmouse.moveDistanceThreshold,
                    a = s(e.target);
                M = M || Math.abs(n.pageX - A) > r || Math.abs(n.pageY - N) > r, M && !i && f("vmousecancel", e, a), f("vmousemove", e, a), h()
            }
        }

        function y(t) {
            if (!z) {
                c();
                var e, n = s(t.target);
                if (f("vmouseup", t, n), !M) {
                    var i = f("vclick", t, n);
                    i && i.isDefaultPrevented() && (e = o(t).changedTouches[0], H.push({
                        touchID: W,
                        x: e.clientX,
                        y: e.clientY
                    }), I = !0)
                }
                f("vmouseout", t, n), M = !1, h()
            }
        }

        function _(e) {
            var n, i = t.data(e, k);
            if (i)
                for (n in i)
                    if (i[n]) return !0;
            return !1
        }

        function w() {}

        function x(e) {
            var n = e.substr(1);
            return {
                setup: function() {
                    _(this) || t.data(this, k, {});
                    var i = t.data(this, k);
                    i[e] = !0, E[e] = (E[e] || 0) + 1, 1 === E[e] && L.bind(n, m), t(this).bind(n, w), O && (E.touchstart = (E.touchstart || 0) + 1, 1 === E.touchstart && L.bind("touchstart", g).bind("touchend", y).bind("touchmove", b).bind("scroll", v))
                },
                teardown: function() {
                    --E[e], E[e] || L.unbind(n, m), O && (--E.touchstart, E.touchstart || L.unbind("touchstart", g).unbind("touchmove", b).unbind("touchend", y).unbind("scroll", v));
                    var i = t(this),
                        o = t.data(this, k);
                    o && (o[e] = !1), i.unbind(n, w), _(this) || i.removeData(k)
                }
            }
        }
        var C, k = "virtualMouseBindings",
            T = "virtualTouchID",
            $ = "vmouseover vmousedown vmousemove vmouseup vclick vmouseout vmousecancel".split(" "),
            S = "clientX clientY pageX pageY screenX screenY".split(" "),
            j = t.event.mouseHooks ? t.event.mouseHooks.props : [],
            D = t.event.props.concat(j),
            E = {},
            P = 0,
            A = 0,
            N = 0,
            M = !1,
            H = [],
            I = !1,
            z = !1,
            O = "addEventListener" in n,
            L = t(n),
            F = 1,
            W = 0;
        t.vmouse = {
            moveDistanceThreshold: 10,
            clickDistanceThreshold: 10,
            resetTimerDuration: 1500
        };
        for (var q = 0; q < $.length; q++) t.event.special[$[q]] = x($[q]);
        O && n.addEventListener("click", function(e) {
            var n, i, o, r, s, a, l = H.length,
                c = e.target;
            if (l)
                for (n = e.clientX, i = e.clientY, C = t.vmouse.clickDistanceThreshold, o = c; o;) {
                    for (r = 0; l > r; r++)
                        if (s = H[r], a = 0, o === c && Math.abs(s.x - n) < C && Math.abs(s.y - i) < C || t.data(o, T) === s.touchID) return e.preventDefault(), void e.stopPropagation();
                    o = o.parentNode
                }
        }, !0)
    }(t, e, n),
    function(t) {
        t.mobile = {}
    }(t),
    function(t) {
        var e = {
            touch: "ontouchend" in n
        };
        t.mobile.support = t.mobile.support || {}, t.extend(t.support, e), t.extend(t.mobile.support, e)
    }(t),
    function(t, e, i) {
        function o(e, n, i) {
            var o = i.type;
            i.type = n, t.event.dispatch.call(e, i), i.type = o
        }
        var r = t(n);
        t.each("touchstart touchmove touchend tap taphold swipe swipeleft swiperight scrollstart scrollstop".split(" "), function(e, n) {
            t.fn[n] = function(t) {
                return t ? this.bind(n, t) : this.trigger(n)
            }, t.attrFn && (t.attrFn[n] = !0)
        });
        var s = t.mobile.support.touch,
            a = "touchmove scroll",
            l = s ? "touchstart" : "mousedown",
            c = s ? "touchend" : "mouseup",
            u = s ? "touchmove" : "mousemove";
        t.event.special.scrollstart = {
            enabled: !0,
            setup: function() {
                function e(t, e) {
                    n = e, o(r, n ? "scrollstart" : "scrollstop", t)
                }
                var n, i, r = this,
                    s = t(r);
                s.bind(a, function(o) {
                    t.event.special.scrollstart.enabled && (n || e(o, !0), clearTimeout(i), i = setTimeout(function() {
                        e(o, !1)
                    }, 50))
                })
            }
        }, t.event.special.tap = {
            tapholdThreshold: 750,
            setup: function() {
                var e = this,
                    n = t(e);
                n.bind("vmousedown", function(i) {
                    function s() {
                        clearTimeout(c)
                    }

                    function a() {
                        s(), n.unbind("vclick", l).unbind("vmouseup", s), r.unbind("vmousecancel", a)
                    }

                    function l(t) {
                        a(), u === t.target && o(e, "tap", t)
                    }
                    if (i.which && 1 !== i.which) return !1; {
                        var c, u = i.target;
                        i.originalEvent
                    }
                    n.bind("vmouseup", s).bind("vclick", l), r.bind("vmousecancel", a), c = setTimeout(function() {
                        o(e, "taphold", t.Event("taphold", {
                            target: u
                        }))
                    }, t.event.special.tap.tapholdThreshold)
                })
            }
        }, t.event.special.swipe = {
            scrollSupressionThreshold: 30,
            durationThreshold: 1e3,
            horizontalDistanceThreshold: 30,
            verticalDistanceThreshold: 75,
            start: function(e) {
                var n = e.originalEvent.touches ? e.originalEvent.touches[0] : e;
                return {
                    time: (new Date).getTime(),
                    coords: [n.pageX, n.pageY],
                    origin: t(e.target)
                }
            },
            stop: function(t) {
                var e = t.originalEvent.touches ? t.originalEvent.touches[0] : t;
                return {
                    time: (new Date).getTime(),
                    coords: [e.pageX, e.pageY]
                }
            },
            handleSwipe: function(e, n) {
                n.time - e.time < t.event.special.swipe.durationThreshold && Math.abs(e.coords[0] - n.coords[0]) > t.event.special.swipe.horizontalDistanceThreshold && Math.abs(e.coords[1] - n.coords[1]) < t.event.special.swipe.verticalDistanceThreshold && e.origin.trigger("swipe").trigger(e.coords[0] > n.coords[0] ? "swipeleft" : "swiperight")
            },
            setup: function() {
                var e = this,
                    n = t(e);
                n.bind(l, function(e) {
                    function o(e) {
                        s && (r = t.event.special.swipe.stop(e), Math.abs(s.coords[0] - r.coords[0]) > t.event.special.swipe.scrollSupressionThreshold && e.preventDefault())
                    }
                    var r, s = t.event.special.swipe.start(e);
                    n.bind(u, o).one(c, function() {
                        n.unbind(u, o), s && r && t.event.special.swipe.handleSwipe(s, r), s = r = i
                    })
                })
            }
        }, t.each({
            scrollstop: "scrollstart",
            taphold: "tap",
            swipeleft: "swipe",
            swiperight: "swipe"
        }, function(e, n) {
            t.event.special[e] = {
                setup: function() {
                    t(this).bind(n, t.noop)
                }
            }
        })
    }(t, this)
}),
/*
 * jQuery UI Touch Punch 0.2.2
 *
 * Copyright 2011, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
function(t) {
    function e(t, e) {
        if (!(t.originalEvent.touches.length > 1)) {
            t.preventDefault();
            var n = t.originalEvent.changedTouches[0],
                i = document.createEvent("MouseEvents");
            i.initMouseEvent(e, !0, !0, window, 1, n.screenX, n.screenY, n.clientX, n.clientY, !1, !1, !1, !1, 0, null), t.target.dispatchEvent(i)
        }
    }
    if (t.support.touch = "ontouchend" in document, t.support.touch) {
        var n, i = t.ui.mouse.prototype,
            o = i._mouseInit;
        i._touchStart = function(t) {
            var i = this;
            !n && i._mouseCapture(t.originalEvent.changedTouches[0]) && (n = !0, i._touchMoved = !1, e(t, "mouseover"), e(t, "mousemove"), e(t, "mousedown"))
        }, i._touchMove = function(t) {
            n && (this._touchMoved = !0, e(t, "mousemove"))
        }, i._touchEnd = function(t) {
            n && (e(t, "mouseup"), e(t, "mouseout"), this._touchMoved || e(t, "click"), n = !1)
        }, i._mouseInit = function() {
            var e = this;
            e.element.bind("touchstart", t.proxy(e, "_touchStart")).bind("touchmove", t.proxy(e, "_touchMove")).bind("touchend", t.proxy(e, "_touchEnd")), o.call(e)
        }
    }
}(jQuery),
/*!
 * Bootstrap v3.0.3
 *
 * Copyright 2013 Twitter, Inc
 * Licensed under the Apache License v2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Designed and built with all the love in the world @twitter by @mdo and @fat.
 */
+ function(t) {
    "use strict";
    var e = '[data-dismiss="alert"]',
        n = function(n) {
            t(n).on("click", e, this.close)
        };
    n.prototype.close = function(e) {
        function n() {
            r.trigger("closed.bs.alert").remove()
        }
        var i = t(this),
            o = i.attr("data-target");
        o || (o = i.attr("href"), o = o && o.replace(/.*(?=#[^\s]*$)/, ""));
        var r = t(o);
        e && e.preventDefault(), r.length || (r = i.hasClass("alert") ? i : i.parent()), r.trigger(e = t.Event("close.bs.alert")), e.isDefaultPrevented() || (r.removeClass("in"), t.support.transition && r.hasClass("fade") ? r.one(t.support.transition.end, n).emulateTransitionEnd(150) : n())
    };
    var i = t.fn.alert;
    t.fn.alert = function(e) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.alert");
            o || i.data("bs.alert", o = new n(this)), "string" == typeof e && o[e].call(i)
        })
    }, t.fn.alert.Constructor = n, t.fn.alert.noConflict = function() {
        return t.fn.alert = i, this
    }, t(document).on("click.bs.alert.data-api", e, n.prototype.close)
}(jQuery), + function(t) {
    function e() {
        t(i).remove(), t(o).each(function(e) {
            var i = n(t(this));
            i.hasClass("open") && (i.trigger(e = t.Event("hide.bs.dropdown")), e.isDefaultPrevented() || i.removeClass("open").trigger("hidden.bs.dropdown"))
        })
    }

    function n(e) {
        var n = e.attr("data-target");
        n || (n = e.attr("href"), n = n && /#/.test(n) && n.replace(/.*(?=#[^\s]*$)/, ""));
        var i = n && t(n);
        return i && i.length ? i : e.parent()
    }
    var i = ".dropdown-backdrop",
        o = "[data-toggle=dropdown]",
        r = function(e) {
            t(e).on("click.bs.dropdown", this.toggle)
        };
    r.prototype.toggle = function(i) {
        var o = t(this);
        if (!o.is(".disabled, :disabled")) {
            var r = n(o),
                s = r.hasClass("open");
            if (e(), !s) {
                if ("ontouchstart" in document.documentElement && !r.closest(".navbar-nav").length && t('<div class="dropdown-backdrop"/>').insertAfter(t(this)).on("click", e), r.trigger(i = t.Event("show.bs.dropdown")), i.isDefaultPrevented()) return;
                r.toggleClass("open").trigger("shown.bs.dropdown"), o.focus()
            }
            return !1
        }
    }, r.prototype.keydown = function(e) {
        if (/(38|40|27)/.test(e.keyCode)) {
            var i = t(this);
            if (e.preventDefault(), e.stopPropagation(), !i.is(".disabled, :disabled")) {
                var r = n(i),
                    s = r.hasClass("open");
                if (!s || s && 27 == e.keyCode) return 27 == e.which && r.find(o).focus(), i.click();
                var a = t("[role=menu] li:not(.divider):visible a", r);
                if (a.length) {
                    var l = a.index(a.filter(":focus"));
                    38 == e.keyCode && l > 0 && l--, 40 == e.keyCode && l < a.length - 1 && l++, ~l || (l = 0), a.eq(l).focus()
                }
            }
        }
    };
    var s = t.fn.dropdown;
    t.fn.dropdown = function(e) {
        return this.each(function() {
            var n = t(this),
                i = n.data("bs.dropdown");
            i || n.data("bs.dropdown", i = new r(this)), "string" == typeof e && i[e].call(n)
        })
    }, t.fn.dropdown.Constructor = r, t.fn.dropdown.noConflict = function() {
        return t.fn.dropdown = s, this
    }, t(document).on("click.bs.dropdown.data-api", e).on("click.bs.dropdown.data-api", ".dropdown form", function(t) {
        t.stopPropagation()
    }).on("click.bs.dropdown.data-api", o, r.prototype.toggle).on("keydown.bs.dropdown.data-api", o + ", [role=menu]", r.prototype.keydown)
}(jQuery), + function(t) {
    "use strict";
    var e = function(e, n) {
        this.options = n, this.$element = t(e), this.$backdrop = this.isShown = null, this.options.remote && this.$element.load(this.options.remote)
    };
    e.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, e.prototype.toggle = function(t) {
        return this[this.isShown ? "hide" : "show"](t)
    }, e.prototype.show = function(e) {
        var n = this,
            i = t.Event("show.bs.modal", {
                relatedTarget: e
            });
        this.$element.trigger(i), this.isShown || i.isDefaultPrevented() || (this.isShown = !0, this.escape(), this.$element.on("click.dismiss.modal", '[data-dismiss="modal"]', t.proxy(this.hide, this)), this.backdrop(function() {
            var i = t.support.transition && n.$element.hasClass("fade");
            n.$element.parent().length || n.$element.appendTo(document.body), n.$element.show(), i && n.$element[0].offsetWidth, n.$element.addClass("in").attr("aria-hidden", !1), n.enforceFocus();
            var o = t.Event("shown.bs.modal", {
                relatedTarget: e
            });
            i ? n.$element.find(".modal-dialog").one(t.support.transition.end, function() {
                n.$element.focus().trigger(o)
            }).emulateTransitionEnd(300) : n.$element.focus().trigger(o)
        }))
    }, e.prototype.hide = function(e) {
        e && e.preventDefault(), e = t.Event("hide.bs.modal"), this.$element.trigger(e), this.isShown && !e.isDefaultPrevented() && (this.isShown = !1, this.escape(), t(document).off("focusin.bs.modal"), this.$element.removeClass("in").attr("aria-hidden", !0).off("click.dismiss.modal"), t.support.transition && this.$element.hasClass("fade") ? this.$element.one(t.support.transition.end, t.proxy(this.hideModal, this)).emulateTransitionEnd(300) : this.hideModal())
    }, e.prototype.enforceFocus = function() {
        t(document).off("focusin.bs.modal").on("focusin.bs.modal", t.proxy(function(t) {
            this.$element[0] !== t.target && !this.$element.has(t.target).length && this.$element.focus()
        }, this))
    }, e.prototype.escape = function() {
        this.isShown && this.options.keyboard ? this.$element.on("keyup.dismiss.bs.modal", t.proxy(function(t) {
            27 == t.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keyup.dismiss.bs.modal")
    }, e.prototype.hideModal = function() {
        var t = this;
        this.$element.hide(), this.backdrop(function() {
            t.removeBackdrop(), t.$element.trigger("hidden.bs.modal")
        })
    }, e.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, e.prototype.backdrop = function(e) {
        var n = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var i = t.support.transition && n;
            if (this.$backdrop = t('<div class="modal-backdrop ' + n + '" />').appendTo(document.body), this.$element.on("click.dismiss.modal", t.proxy(function(t) {
                    t.target === t.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus.call(this.$element[0]) : this.hide.call(this))
                }, this)), i && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !e) return;
            i ? this.$backdrop.one(t.support.transition.end, e).emulateTransitionEnd(150) : e()
        } else !this.isShown && this.$backdrop ? (this.$backdrop.removeClass("in"), t.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one(t.support.transition.end, e).emulateTransitionEnd(150) : e()) : e && e()
    };
    var n = t.fn.modal;
    t.fn.modal = function(n, i) {
        return this.each(function() {
            var o = t(this),
                r = o.data("bs.modal"),
                s = t.extend({}, e.DEFAULTS, o.data(), "object" == typeof n && n);
            r || o.data("bs.modal", r = new e(this, s)), "string" == typeof n ? r[n](i) : s.show && r.show(i)
        })
    }, t.fn.modal.Constructor = e, t.fn.modal.noConflict = function() {
        return t.fn.modal = n, this
    }, t(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(e) {
        var n = t(this),
            i = n.attr("href"),
            o = t(n.attr("data-target") || i && i.replace(/.*(?=#[^\s]+$)/, "")),
            r = o.data("modal") ? "toggle" : t.extend({
                remote: !/#/.test(i) && i
            }, o.data(), n.data());
        e.preventDefault(), o.modal(r, this).one("hide", function() {
            n.is(":visible") && n.focus()
        })
    }), t(document).on("show.bs.modal", ".modal", function() {
        t(document.body).addClass("modal-open")
    }).on("hidden.bs.modal", ".modal", function() {
        t(document.body).removeClass("modal-open")
    })
}(jQuery), + function(t) {
    "use strict";
    var e = function(t, e) {
        this.type = this.options = this.enabled = this.timeout = this.hoverState = this.$element = null, this.init("tooltip", t, e)
    };
    e.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1
    }, e.prototype.init = function(e, n, i) {
        this.enabled = !0, this.type = e, this.$element = t(n), this.options = this.getOptions(i);
        for (var o = this.options.trigger.split(" "), r = o.length; r--;) {
            var s = o[r];
            if ("click" == s) this.$element.on("click." + this.type, this.options.selector, t.proxy(this.toggle, this));
            else if ("manual" != s) {
                var a = "hover" == s ? "mouseenter" : "focus",
                    l = "hover" == s ? "mouseleave" : "blur";
                this.$element.on(a + "." + this.type, this.options.selector, t.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, t.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = t.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, e.prototype.getDefaults = function() {
        return e.DEFAULTS
    }, e.prototype.getOptions = function(e) {
        return e = t.extend({}, this.getDefaults(), this.$element.data(), e), e.delay && "number" == typeof e.delay && (e.delay = {
            show: e.delay,
            hide: e.delay
        }), e
    }, e.prototype.getDelegateOptions = function() {
        var e = {},
            n = this.getDefaults();
        return this._options && t.each(this._options, function(t, i) {
            n[t] != i && (e[t] = i)
        }), e
    }, e.prototype.enter = function(e) {
        var n = e instanceof this.constructor ? e : t(e.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
        return clearTimeout(n.timeout), n.hoverState = "in", n.options.delay && n.options.delay.show ? void(n.timeout = setTimeout(function() {
            "in" == n.hoverState && n.show()
        }, n.options.delay.show)) : n.show()
    }, e.prototype.leave = function(e) {
        var n = e instanceof this.constructor ? e : t(e.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
        return clearTimeout(n.timeout), n.hoverState = "out", n.options.delay && n.options.delay.hide ? void(n.timeout = setTimeout(function() {
            "out" == n.hoverState && n.hide()
        }, n.options.delay.hide)) : n.hide()
    }, e.prototype.show = function() {
        var e = t.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            if (this.$element.trigger(e), e.isDefaultPrevented()) return;
            var n = this.tip();
            this.setContent(), this.options.animation && n.addClass("fade");
            var i = "function" == typeof this.options.placement ? this.options.placement.call(this, n[0], this.$element[0]) : this.options.placement,
                o = /\s?auto?\s?/i,
                r = o.test(i);
            r && (i = i.replace(o, "") || "top"), n.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(i), this.options.container ? n.appendTo(this.options.container) : n.insertAfter(this.$element);
            var s = this.getPosition(),
                a = n[0].offsetWidth,
                l = n[0].offsetHeight;
            if (r) {
                var c = this.$element.parent(),
                    u = i,
                    d = document.documentElement.scrollTop || document.body.scrollTop,
                    h = "body" == this.options.container ? window.innerWidth : c.outerWidth(),
                    p = "body" == this.options.container ? window.innerHeight : c.outerHeight(),
                    f = "body" == this.options.container ? 0 : c.offset().left;
                i = "bottom" == i && s.top + s.height + l - d > p ? "top" : "top" == i && s.top - d - l < 0 ? "bottom" : "right" == i && s.right + a > h ? "left" : "left" == i && s.left - a < f ? "right" : i, n.removeClass(u).addClass(i)
            }
            var m = this.getCalculatedOffset(i, s, a, l);
            this.applyPlacement(m, i), this.$element.trigger("shown.bs." + this.type)
        }
    }, e.prototype.applyPlacement = function(t, e) {
        var n, i = this.tip(),
            o = i[0].offsetWidth,
            r = i[0].offsetHeight,
            s = parseInt(i.css("margin-top"), 10),
            a = parseInt(i.css("margin-left"), 10);
        isNaN(s) && (s = 0), isNaN(a) && (a = 0), t.top = t.top + s, t.left = t.left + a, i.offset(t).addClass("in");
        var l = i[0].offsetWidth,
            c = i[0].offsetHeight;
        if ("top" == e && c != r && (n = !0, t.top = t.top + r - c), /bottom|top/.test(e)) {
            var u = 0;
            t.left < 0 && (u = -2 * t.left, t.left = 0, i.offset(t), l = i[0].offsetWidth, c = i[0].offsetHeight), this.replaceArrow(u - o + l, l, "left")
        } else this.replaceArrow(c - r, c, "top");
        n && i.offset(t)
    }, e.prototype.replaceArrow = function(t, e, n) {
        this.arrow().css(n, t ? 50 * (1 - t / e) + "%" : "")
    }, e.prototype.setContent = function() {
        var t = this.tip(),
            e = this.getTitle();
        t.find(".tooltip-inner")[this.options.html ? "html" : "text"](e), t.removeClass("fade in top bottom left right")
    }, e.prototype.hide = function() {
        function e() {
            "in" != n.hoverState && i.detach()
        }
        var n = this,
            i = this.tip(),
            o = t.Event("hide.bs." + this.type);
        return this.$element.trigger(o), o.isDefaultPrevented() ? void 0 : (i.removeClass("in"), t.support.transition && this.$tip.hasClass("fade") ? i.one(t.support.transition.end, e).emulateTransitionEnd(150) : e(), this.$element.trigger("hidden.bs." + this.type), this)
    }, e.prototype.fixTitle = function() {
        var t = this.$element;
        (t.attr("title") || "string" != typeof t.attr("data-original-title")) && t.attr("data-original-title", t.attr("title") || "").attr("title", "")
    }, e.prototype.hasContent = function() {
        return this.getTitle()
    }, e.prototype.getPosition = function() {
        var e = this.$element[0];
        return t.extend({}, "function" == typeof e.getBoundingClientRect ? e.getBoundingClientRect() : {
            width: e.offsetWidth,
            height: e.offsetHeight
        }, this.$element.offset())
    }, e.prototype.getCalculatedOffset = function(t, e, n, i) {
        return "bottom" == t ? {
            top: e.top + e.height,
            left: e.left + e.width / 2 - n / 2
        } : "top" == t ? {
            top: e.top - i,
            left: e.left + e.width / 2 - n / 2
        } : "left" == t ? {
            top: e.top + e.height / 2 - i / 2,
            left: e.left - n
        } : {
            top: e.top + e.height / 2 - i / 2,
            left: e.left + e.width
        }
    }, e.prototype.getTitle = function() {
        var t, e = this.$element,
            n = this.options;
        return t = e.attr("data-original-title") || ("function" == typeof n.title ? n.title.call(e[0]) : n.title)
    }, e.prototype.tip = function() {
        return this.$tip = this.$tip || t(this.options.template)
    }, e.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, e.prototype.validate = function() {
        this.$element[0].parentNode || (this.hide(), this.$element = null, this.options = null)
    }, e.prototype.enable = function() {
        this.enabled = !0
    }, e.prototype.disable = function() {
        this.enabled = !1
    }, e.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled
    }, e.prototype.toggle = function(e) {
        var n = e ? t(e.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type) : this;
        n.tip().hasClass("in") ? n.leave(n) : n.enter(n)
    }, e.prototype.destroy = function() {
        this.hide().$element.off("." + this.type).removeData("bs." + this.type)
    };
    var n = t.fn.tooltip;
    t.fn.tooltip = function(n) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.tooltip"),
                r = "object" == typeof n && n;
            o || i.data("bs.tooltip", o = new e(this, r)), "string" == typeof n && o[n]()
        })
    }, t.fn.tooltip.Constructor = e, t.fn.tooltip.noConflict = function() {
        return t.fn.tooltip = n, this
    }
}(jQuery), + function(t) {
    "use strict";
    var e = function(t, e) {
        this.init("popover", t, e)
    };
    if (!t.fn.tooltip) throw new Error("Popover requires tooltip.js");
    e.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), e.prototype = t.extend({}, t.fn.tooltip.Constructor.prototype), e.prototype.constructor = e, e.prototype.getDefaults = function() {
        return e.DEFAULTS
    }, e.prototype.setContent = function() {
        var t = this.tip(),
            e = this.getTitle(),
            n = this.getContent();
        t.find(".popover-title")[this.options.html ? "html" : "text"](e), t.find(".popover-content")[this.options.html ? "html" : "text"](n), t.removeClass("fade top bottom left right in"), t.find(".popover-title").html() || t.find(".popover-title").hide()
    }, e.prototype.hasContent = function() {
        return this.getTitle() || this.getContent()
    }, e.prototype.getContent = function() {
        var t = this.$element,
            e = this.options;
        return t.attr("data-content") || ("function" == typeof e.content ? e.content.call(t[0]) : e.content)
    }, e.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    }, e.prototype.tip = function() {
        return this.$tip || (this.$tip = t(this.options.template)), this.$tip
    };
    var n = t.fn.popover;
    t.fn.popover = function(n) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.popover"),
                r = "object" == typeof n && n;
            o || i.data("bs.popover", o = new e(this, r)), "string" == typeof n && o[n]()
        })
    }, t.fn.popover.Constructor = e, t.fn.popover.noConflict = function() {
        return t.fn.popover = n, this
    }
}(jQuery), + function(t) {
    function e() {
        var t = document.createElement("bootstrap"),
            e = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                transition: "transitionend"
            };
        for (var n in e)
            if (void 0 !== t.style[n]) return {
                end: e[n]
            }
    }
    t.fn.emulateTransitionEnd = function(e) {
        var n = !1,
            i = this;
        t(this).one(t.support.transition.end, function() {
            n = !0
        });
        var o = function() {
            n || t(i).trigger(t.support.transition.end)
        };
        return setTimeout(o, e), this
    }, t(function() {
        t.support.transition = e()
    })
}(jQuery),
/* ========================================================================
 * Bootstrap: button.js v3.1.0
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2013 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */
+ function(t) {
    "use strict";
    var e = function(n, i) {
        this.$element = t(n), this.options = t.extend({}, e.DEFAULTS, i)
    };
    e.DEFAULTS = {
        loadingText: "loading..."
    }, e.prototype.setState = function(t) {
        var e = "disabled",
            n = this.$element,
            i = n.is("input") ? "val" : "html",
            o = n.data();
        t += "Text", o.resetText || n.data("resetText", n[i]()), n[i](o[t] || this.options[t]), setTimeout(function() {
            "loadingText" == t ? n.addClass(e).attr(e, e) : n.removeClass(e).removeAttr(e)
        }, 0)
    }, e.prototype.toggle = function() {
        var t = this.$element.closest('[data-toggle="buttons"]'),
            e = !0;
        if (t.length) {
            var n = this.$element.find("input");
            "radio" === n.prop("type") && (n.prop("checked") && this.$element.hasClass("active") ? e = !1 : t.find(".active").removeClass("active")), e && n.prop("checked", !this.$element.hasClass("active")).trigger("change")
        }
        e && this.$element.toggleClass("active")
    };
    var n = t.fn.button;
    t.fn.button = function(n) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.button"),
                r = "object" == typeof n && n;
            o || i.data("bs.button", o = new e(this, r)), "toggle" == n ? o.toggle() : n && o.setState(n)
        })
    }, t.fn.button.Constructor = e, t.fn.button.noConflict = function() {
        return t.fn.button = n, this
    }, t(document).on("click.bs.button.data-api", "[data-toggle^=button]", function(e) {
        var n = t(e.target);
		//n.hasClass("btn") || (n = n.closest(".btn")), n.button("toggle"), e.preventDefault() //fastnail commented out 2015/08/07
    })
}(jQuery),
/* ===========================================================
 * bootstrap-modal.js v2.0
 * ===========================================================
 * Copyright 2012 Jordan Schroter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */
! function(t) {
    "use strict";
    var e = function(t, e) {
        this.init(t, e)
    };
    e.prototype = {
        constructor: e,
        init: function(e, n) {
            this.options = n, this.$element = t(e).delegate('[data-dismiss="modal"]', "click.dismiss.modal", t.proxy(this.hide, this)), this.options.remote && this.$element.find(".modal-body").load(this.options.remote);
            var i = "function" == typeof this.options.manager ? this.options.manager.call(this) : this.options.manager;
            i = i.appendModal ? i : t(i).modalmanager().data("modalmanager"), i.appendModal(this)
        },
        toggle: function() {
            return this[this.isShown ? "hide" : "show"]()
        },
        show: function() {
            var e = this,
                n = t.Event("show");
            if (!this.isShown && (this.$element.triggerHandler(n), !n.isDefaultPrevented())) {
                if (this.options.width) {
                    this.$element.css("width", this.options.width);
                    var e = this;
                    this.$element.css("margin-left", function() {
                        return /%/gi.test(e.options.width) ? -(parseInt(e.options.width) / 2) + "%" : -(t(this).width() / 2) + "px"
                    })
                }
                var i = this.options.height ? "height" : "max-height",
                    o = this.options.height || this.options.maxHeight;
                o && this.$element.find(".modal-body").css("overflow", "auto").css(i, o), this.escape(), this.tab(), this.options.loading && this.loading()
            }
        },
        hide: function(e) {
            return e && e.preventDefault(), e = t.Event("hide"), this.$element.triggerHandler(e), !this.isShown || e.isDefaultPrevented() ? this.isShown = !1 : (this.isShown = !1, this.escape(), this.tab(), this.isLoading && this.loading(), t(document).off("focusin.modal"), this.$element.removeClass("in").removeClass("animated").removeClass(this.options.attentionAnimation).removeClass("modal-overflow").attr("aria-hidden", !0), void(t.support.transition && this.$element.hasClass("fade") ? this.hideWithTransition() : this.hideModal()))
        },
        tab: function() {
            var e = this;
            this.isShown && this.options.consumeTab ? this.$element.on("keydown.tabindex.modal", "[data-tabindex]", function(n) {
                if (n.keyCode && 9 == n.keyCode) {
                    var i = t(this),
                        o = t(this);
                    e.$element.find("[data-tabindex]:enabled:not([readonly])").each(function(e) {
                        i = e.shiftKey ? i.data("tabindex") > t(this).data("tabindex") ? i = t(this) : o = t(this) : i.data("tabindex") < t(this).data("tabindex") ? i = t(this) : o = t(this)
                    }), i[0] !== t(this)[0] ? i.focus() : o.focus(), n.preventDefault()
                }
            }) : this.isShown || this.$element.off("keydown.tabindex.modal")
        },
        escape: function() {
            var t = this;
            this.isShown && this.options.keyboard ? (this.$element.attr("tabindex") || this.$element.attr("tabindex", -1), this.$element.on("keyup.dismiss.modal", function(e) {
                27 == e.which && t.hide()
            })) : this.isShown || this.$element.off("keyup.dismiss.modal")
        },
        hideWithTransition: function() {
            var e = this,
                n = setTimeout(function() {
                    e.$element.off(t.support.transition.end), e.hideModal()
                }, 500);
            this.$element.one(t.support.transition.end, function() {
                clearTimeout(n), e.hideModal()
            })
        },
        hideModal: function() {
            this.$element.hide().triggerHandler("hidden");
            var t = this.options.height ? "height" : "max-height",
                e = this.options.height || this.options.maxHeight;
            e && this.$element.find(".modal-body").css("overflow", "").css(t, "")
        },
        removeLoading: function() {
            this.$loading.remove(), this.$loading = null, this.isLoading = !1
        },
        loading: function(e) {
            e = e || function() {};
            var n = this.$element.hasClass("fade") ? "fade" : "";
            if (this.isLoading)
                if (this.isLoading && this.$loading) {
                    this.$loading.removeClass("in");
                    var i = this;
                    t.support.transition && this.$element.hasClass("fade") ? this.$loading.one(t.support.transition.end, function() {
                        i.removeLoading()
                    }) : i.removeLoading()
                } else e && e(this.isLoading);
            else {
                var o = t.support.transition && n;
                this.$loading = t('<div class="loading-mask ' + n + '">').append(this.options.spinner).appendTo(this.$element), o && this.$loading[0].offsetWidth, this.$loading.addClass("in"), this.isLoading = !0, o ? this.$loading.one(t.support.transition.end, e) : e()
            }
        },
        focus: function() {
            var t = this.$element.find(this.options.focusOn);
            t = t.length ? t : this.$element, t.focus()
        },
        attention: function() {
            if (this.options.attentionAnimation) {
                this.$element.removeClass("animated").removeClass(this.options.attentionAnimation);
                var t = this;
                setTimeout(function() {
                    t.$element.addClass("animated").addClass(t.options.attentionAnimation)
                }, 0)
            }
            this.focus()
        },
        destroy: function() {
            var e = t.Event("destroy");
            this.$element.triggerHandler(e), e.isDefaultPrevented() || this.teardown()
        },
        teardown: function() {
            return this.$parent.length ? (this.$parent !== this.$element.parent() && this.$element.appendTo(this.$parent), this.$element.off(".modal"), this.$element.removeData("modal"), void this.$element.removeClass("in").attr("aria-hidden", !0)) : (this.$element.remove(), void(this.$element = null))
        }
    }, t.fn.modal = function(n) {
        return this.each(function() {
            var i = t(this),
                o = i.data("modal"),
                r = t.extend({}, t.fn.modal.defaults, i.data(), "object" == typeof n && n);
            o || i.data("modal", o = new e(this, r)), "string" == typeof n ? o[n]() : r.show && o.show()
        })
    }, t.fn.modal.defaults = {
        keyboard: !0,
        backdrop: !0,
        loading: !1,
        show: !0,
        width: null,
        height: null,
        maxHeight: null,
        modalOverflow: !1,
        consumeTab: !0,
        focusOn: null,
        attentionAnimation: "shake",
        manager: "body",
        spinner: '<div class="loading-spinner" style="width: 200px; margin-left: -100px;"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div>'
    }, t.fn.modal.Constructor = e, t(function() {
        t(document).off(".modal").on("click.modal.data-api", '[data-toggle="modal"]', function(e) {
            var n = t(this),
                i = n.attr("href"),
                o = t(n.attr("data-target") || i && i.replace(/.*(?=#[^\s]+$)/, "")),
                r = o.data("modal") ? "toggle" : t.extend({
                    remote: !/#/.test(i) && i
                }, o.data(), n.data());
            e.preventDefault(), o.modal(r).one("hide", function() {
                n.focus()
            })
        })
    })
}(window.jQuery),
/* ===========================================================
 * bootstrap-modalmanager.js v2.0
 * ===========================================================
 * Copyright 2012 Jordan Schroter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */
! function(t) {
    "use strict";

    function e(t) {
        return function(e) {
            return this === e.target ? t.apply(this, arguments) : void 0
        }
    }
    var n = function(t, e) {
        this.init(t, e)
    };
    n.prototype = {
        constructor: n,
        init: function(e, n) {
            this.$element = t(e), this.options = t.extend({}, t.fn.modalmanager.defaults, this.$element.data(), "object" == typeof n && n), this.stack = [], this.backdropCount = 0
        },
        createModal: function(e, n) {
            t(e).modal(t.extend({
                manager: this
            }, n))
        },
        appendModal: function(n) {
            this.stack.push(n);
            var i = this;
            n.$element.on("show.modalmanager", e(function() {
                n.isShown = !0;
                var e = t.support.transition && n.$element.hasClass("fade");
                i.$element.toggleClass("modal-open", i.hasOpenModal()).toggleClass("page-overflow", t(window).height() < i.$element.height()), n.$parent = n.$element.parent(), n.$container = i.createContainer(n), n.$element.appendTo(n.$container);
                var o = t(window).height() < n.$element.height() || n.options.modalOverflow;
                i.backdrop(n, function() {
                    n.$element.show(), e && (n.$element[0].style.display = "run-in", n.$element[0].offsetWidth, n.$element.one(t.support.transition.end, function() {
                        n.$element[0].style.display = "block"
                    })), n.$element.toggleClass("modal-overflow", o).css("margin-top", o ? 0 : 0 - n.$element.height() / 2).addClass("in").attr("aria-hidden", !1);
                    var r = function() {
                        i.setFocus(), n.$element.triggerHandler("shown")
                    };
                    e ? n.$element.one(t.support.transition.end, r) : r()
                })
            })), n.$element.on("hidden.modalmanager", e(function() {
                i.backdrop(n), n.$backdrop && t.support.transition && n.$element.hasClass("fade") ? n.$backdrop.one(t.support.transition.end, function() {
                    i.destroyModal(n)
                }) : i.destroyModal(n)
            })), n.$element.on("destroy.modalmanager", e(function() {
                i.removeModal(n)
            }))
        },
        destroyModal: function(t) {
            t.destroy();
            var e = this.hasOpenModal();
            this.$element.toggleClass("modal-open", e), e || this.$element.removeClass("page-overflow"), this.removeContainer(t), this.setFocus()
        },
        hasOpenModal: function() {
            for (var t = 0; t < this.stack.length; t++)
                if (this.stack[t].isShown) return !0;
            return !1
        },
        setFocus: function() {
            for (var t, e = 0; e < this.stack.length; e++) this.stack[e].isShown && (t = this.stack[e]);
            t && t.focus()
        },
        removeModal: function(t) {
            t.$element.off(".modalmanager"), t.$backdrop && this.removeBackdrop.call(t), this.stack.splice(this.getIndexOfModal(t), 1)
        },
        getModalAt: function(t) {
            return this.stack[t]
        },
        getIndexOfModal: function(t) {
            for (var e = 0; e < this.stack.length; e++)
                if (t === this.stack[e]) return e
        },
        removeBackdrop: function(t) {
            t.$backdrop.remove(), t.$backdrop = null
        },
        createBackdrop: function(e) {
            var n;
            return this.isLoading ? (n = this.$loading, n.off(".modalmanager"), this.$spinner.remove(), this.isLoading = !1, this.$loading = this.$spinner = null) : n = t('<div class="modal-backdrop ' + e + '" />').appendTo(this.$element), n
        },
        removeContainer: function(t) {
            t.$container.remove(), t.$container = null
        },
        createContainer: function(n) {
            var o;
            return o = t('<div class="modal-scrollable">').css("z-index", i("modal", n ? this.getIndexOfModal(n) : this.stack.length)).appendTo(this.$element), n && "static" != n.options.backdrop ? o.on("click.modal", e(function() {
                n.hide()
            })) : n && o.on("click.modal", e(function() {
                n.attention()
            })), o
        },
        backdrop: function(e, n) {
            var o = e.$element.hasClass("fade") ? "fade" : "",
                r = e.options.backdrop && this.backdropCount < this.options.backdropLimit;
            if (e.isShown && r) {
                var s = t.support.transition && o && !this.isLoading;
                e.$backdrop = this.createBackdrop(o), e.$backdrop.css("z-index", i("backdrop", this.getIndexOfModal(e))), s && e.$backdrop[0].offsetWidth, e.$backdrop.addClass("in"), this.backdropCount += 1, s ? e.$backdrop.one(t.support.transition.end, n) : n()
            } else if (!e.isShown && e.$backdrop) {
                e.$backdrop.removeClass("in"), this.backdropCount -= 1;
                var a = this;
                t.support.transition && e.$element.hasClass("fade") ? e.$backdrop.one(t.support.transition.end, function() {
                    a.removeBackdrop(e)
                }) : a.removeBackdrop(e)
            } else n && n()
        },
        removeLoading: function() {
            this.$loading && this.$loading.remove(), this.$loading = null, this.isLoading = !1
        },
        loading: function(e) {
            if (e = e || function() {}, this.$element.toggleClass("modal-open", !this.isLoading || this.hasOpenModal()).toggleClass("page-overflow", t(window).height() < this.$element.height()), this.isLoading)
                if (this.isLoading && this.$loading) {
                    this.$loading.removeClass("in"), this.$spinner && this.$spinner.remove();
                    var n = this;
                    t.support.transition ? this.$loading.one(t.support.transition.end, function() {
                        n.removeLoading()
                    }) : n.removeLoading()
                } else e && e(this.isLoading);
            else {
                this.$loading = this.createBackdrop("fade"), this.$loading[0].offsetWidth, this.$loading.css("z-index", i("backdrop", this.stack.length)).addClass("in");
                var o = t(this.options.spinner).css("z-index", i("modal", this.stack.length)).appendTo(this.$element).addClass("in");
                this.$spinner = t(this.createContainer()).append(o).on("click.modalmanager", t.proxy(this.loading, this)), this.isLoading = !0, t.support.transition ? this.$loading.one(t.support.transition.end, e) : e()
            }
        }
    };
    var i = function() {
        var e, n = {};
        return function(i, o) {
            if ("undefined" == typeof e) {
                var r = t('<div class="modal hide" />').appendTo("body"),
                    s = t('<div class="modal-backdrop hide" />').appendTo("body");
                n.modal = +r.css("z-index"), n.backdrop = +s.css("z-index"), e = n.modal - n.backdrop, r.remove(), s.remove(), s = r = null
            }
            return n[i] + e * o
        }
    }();
    t.fn.modalmanager = function(e) {
        return this.each(function() {
            var i = t(this),
                o = i.data("modalmanager");
            o || i.data("modalmanager", o = new n(this, e)), "string" == typeof e && o[e]()
        })
    }, t.fn.modalmanager.defaults = {
        backdropLimit: 999,
        spinner: '<div class="loading-spinner fade" style="width: 200px; margin-left: -100px;"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div>'
    }, t.fn.modalmanager.Constructor = n
}(jQuery),
/* ===================================================
 * tagmanager.js v3.0.1
 * http://welldonethings.com/tags/manager
 * ===================================================
 * Copyright 2012 Max Favilli
 *
 * Licensed under the Mozilla Public License, Version 2.0 You may not use this work except in compliance with the License.
 *
 * http://www.mozilla.org/MPL/2.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */
function(t) {
    "use strict";
    var e = {
            prefilled: null,
            CapitalizeFirstLetter: !1,
            preventSubmitOnEnter: !0,
            isClearInputOnEsc: !0,
            AjaxPush: null,
            AjaxPushAllTags: null,
            AjaxPushParameters: null,
            delimiters: [9, 13, 44],
            backspace: [8],
            maxTags: 0,
            hiddenTagListName: null,
            hiddenTagListId: null,
            replace: !0,
            output: null,
            deleteTagsOnBackspace: !0,
            tagsContainer: null,
            tagCloseIcon: "x",
            tagClass: "",
            validator: null,
            onlyTagList: !1,
            tagList: null
        },
        n = {
            pushTag: function(e, n) {
                var o, r, s, a, l, c, u, d, h, p, f, m, g = t(this),
                    v = g.data("opts"),
                    b = g.data("tlis"),
                    y = g.data("tlid");
                if (e = i.trimTag(e, v.delimiterChars), e && !(e.length <= 0)) {
                    if (v.onlyTagList && void 0 !== v.tagList && v.tagList) {
                        var _ = v.tagList;
                        t.each(_, function(t, e) {
                            _[t] = e.toLowerCase()
                        });
                        var w = t.inArray(e.toLowerCase(), _);
                        if (-1 === w) return
                    }
                    v.CapitalizeFirstLetter && e.length > 1 && (e = e.charAt(0).toUpperCase() + e.slice(1).toLowerCase()), (!v.validator || v.validator(e)) && (v.maxTags > 0 && b.length >= v.maxTags || (o = !1, r = jQuery.map(b, function(t) {
                        return t.toLowerCase()
                    }), l = t.inArray(e.toLowerCase(), r), -1 !== l && (o = !0), o ? (g.trigger("tm:duplicated", e), t("#" + g.data("tm_rndid") + "_" + y[l]).stop().animate({
                        backgroundColor: v.blinkBGColor_1
                    }, 100).animate({
                        backgroundColor: v.blinkBGColor_2
                    }, 100).animate({
                        backgroundColor: v.blinkBGColor_1
                    }, 100).animate({
                        backgroundColor: v.blinkBGColor_2
                    }, 100).animate({
                        backgroundColor: v.blinkBGColor_1
                    }, 100).animate({
                        backgroundColor: v.blinkBGColor_2
                    }, 100)) : (n || g.trigger("tm:pushing", e), s = Math.max.apply(null, y), s = s === -1 / 0 ? 0 : s, a = ++s, b.push(e), y.push(a), n || null !== v.AjaxPush && null == v.AjaxPushAllTags && -1 === t.inArray(e, v.prefilled) && t.post(v.AjaxPush, t.extend({
                        tag: e
                    }, v.AjaxPushParameters)), c = g.data("tm_rndid") + "_" + a, u = g.data("tm_rndid") + "_Remover_" + a, d = t("<span/>").text(e).html(), h = '<span class="' + i.tagClasses.call(g) + '" id="' + c + '">', h += "<span>" + d + "</span>", h += '<a href="#" class="tm-tag-remove" id="' + u + '" TagIdToRemove="' + a + '">', h += v.tagCloseIcon + "</a></span> ", p = t(h), null !== v.tagsContainer ? t(v.tagsContainer).append(p) : a > 1 ? (f = a - 1, m = t("#" + g.data("tm_rndid") + "_" + f), m.after(p)) : g.before(p), p.find("#" + u).on("click", g, function(e) {
                        e.preventDefault();
                        var n = parseInt(t(this).attr("TagIdToRemove"));
                        i.spliceTag.call(g, n, e.data)
                    }), i.refreshHiddenTagList.call(g), n || g.trigger("tm:pushed", e), i.showOrHide.call(g)), g.val("")))
                }
            },
            popTag: function() {
                var e, n, o = t(this),
                    r = o.data("tlis"),
                    s = o.data("tlid");
                s.length > 0 && (e = s.pop(), n = r[r.length - 1], o.trigger("tm:popping", n), r.pop(), t("#" + o.data("tm_rndid") + "_" + e).remove(), i.refreshHiddenTagList.call(o), o.trigger("tm:popped", n))
            },
            empty: function() {
                for (var e, n = t(this), o = n.data("tlis"), r = n.data("tlid"); r.length > 0;) e = r.pop(), o.pop(), t("#" + n.data("tm_rndid") + "_" + e).remove(), i.refreshHiddenTagList.call(n);
                n.trigger("tm:emptied", null), i.showOrHide.call(n)
            },
            tags: function() {
                var t = this,
                    e = t.data("tlis");
                return e
            }
        },
        i = {
            showOrHide: function() {
                var t = this,
                    e = t.data("opts"),
                    n = t.data("tlis");
                e.maxTags > 0 && n.length < e.maxTags && (t.show(), t.trigger("tm:show")), e.maxTags > 0 && n.length >= e.maxTags && (t.hide(), t.trigger("tm:hide"))
            },
            tagClasses: function() {
                var e, n = t(this),
                    i = n.data("opts"),
                    o = i.tagBaseClass,
                    r = i.inputBaseClass;
                return e = o, n.attr("class") && t.each(n.attr("class").split(" "), function(t, n) {
                    -1 !== n.indexOf(r + "-") && (e += " " + o + n.substring(r.length))
                }), e += i.tagClass ? " " + i.tagClass : ""
            },
            trimTag: function(e, n) {
                var i;
                for (e = t.trim(e), i = 0; i < e.length && -1 === t.inArray(e.charCodeAt(i), n); i++);
                return e.substring(0, i)
            },
            refreshHiddenTagList: function() {
                var e = t(this),
                    n = e.data("tlis"),
                    i = e.data("lhiddenTagList");
                i && t(i).val(n.join(e.data("opts").baseDelimiter)).change(), e.trigger("tm:refresh", n.join(e.data("opts").baseDelimiter))
            },
            killEvent: function(t) {
                t.cancelBubble = !0, t.returnValue = !1, t.stopPropagation(), t.preventDefault()
            },
            keyInArray: function(e, n) {
                return -1 !== t.inArray(e.which, n)
            },
            applyDelimiter: function(e) {
                var i = t(this);
                n.pushTag.call(i, t(this).val()), e.preventDefault()
            },
            prefill: function(e) {
                var i = t(this);
                t.each(e, function(t, e) {
                    n.pushTag.call(i, e, !0)
                })
            },
            pushAllTags: function(e, n) {
                var i = t(this),
                    o = i.data("opts"),
                    r = i.data("tlis");
                o.AjaxPushAllTags && ("tm:pushed" !== e.type || -1 === t.inArray(n, o.prefilled)) && t.post(o.AjaxPush, t.extend({
                    tags: r.join(o.baseDelimiter)
                }, o.AjaxPushParameters))
            },
            spliceTag: function(e) {
                var n, o = this,
                    r = o.data("tlis"),
                    s = o.data("tlid"),
                    a = t.inArray(e, s); - 1 !== a && (n = r[a], o.trigger("tm:splicing", n), t("#" + o.data("tm_rndid") + "_" + e).remove(), r.splice(a, 1), s.splice(a, 1), i.refreshHiddenTagList.call(o), o.trigger("tm:spliced", n)), i.showOrHide.call(o)
            },
            init: function(o) {
                var r, s, a = t.extend({}, e, o);
                return a.hiddenTagListName = null === a.hiddenTagListName ? "hidden-" + this.attr("name") : a.hiddenTagListName, r = a.delimeters || a.delimiters, s = [9, 13, 17, 18, 19, 37, 38, 39, 40], a.delimiterChars = [], a.delimiterKeys = [], t.each(r, function(e, n) {
                    -1 !== t.inArray(n, s) ? a.delimiterKeys.push(n) : a.delimiterChars.push(n)
                }), a.baseDelimiter = String.fromCharCode(a.delimiterChars[0] || 44), a.tagBaseClass = "tm-tag", a.inputBaseClass = "tm-input", t.isFunction(a.validator) || (a.validator = null), this.each(function() {
                    var e = t(this),
                        o = "",
                        r = "",
                        s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                    if (e.data("tagManager")) return !1;
                    e.data("tagManager", !0);
                    for (var l = 0; 5 > l; l++) r += s.charAt(Math.floor(Math.random() * s.length));
                    if (e.data("tm_rndid", r), e.data("opts", a).data("tlis", []).data("tlid", []), null === a.output ? (o = t("<input/>", {
                            type: "hidden",
                            name: a.hiddenTagListName
                        }), e.after(o), e.data("lhiddenTagList", o)) : e.data("lhiddenTagList", t(a.output)), a.AjaxPushAllTags && (e.on("tm:spliced", i.pushAllTags), e.on("tm:popped", i.pushAllTags), e.on("tm:pushed", i.pushAllTags)), e.on("focus keypress", function() {
                            t(this).popover && t(this).popover("hide")
                        }), a.isClearInputOnEsc && e.on("keyup", function(e) {
                            27 === e.which && (t(this).val(""), i.killEvent(e))
                        }), e.on("keypress", function(t) {
                            i.keyInArray(t, a.delimiterChars) && i.applyDelimiter.call(e, t)
                        }), e.on("keydown", function(t) {
                            13 === t.which && a.preventSubmitOnEnter && i.killEvent(t), i.keyInArray(t, a.delimiterKeys) && i.applyDelimiter.call(e, t)
                        }), a.deleteTagsOnBackspace && e.on("keydown", function(o) {
                            i.keyInArray(o, a.backspace) && t(this).val().length <= 0 && (n.popTag.call(e), i.killEvent(o))
                        }), e.change(function(t) {
                            /webkit/.test(navigator.userAgent.toLowerCase()) || e.focus(), i.killEvent(t)
                        }), null !== a.prefilled) "object" == typeof a.prefilled ? i.prefill.call(e, a.prefilled) : "string" == typeof a.prefilled ? i.prefill.call(e, a.prefilled.split(a.baseDelimiter)) : "function" == typeof a.prefilled && i.prefill.call(e, a.prefilled());
                    else if (null !== a.output) {
                        if (t(a.output) && t(a.output).val()) {
                            t(a.output)
                        }
                        i.prefill.call(e, t(a.output).val().split(a.baseDelimiter))
                    }
                }), this
            }
        };
    t.fn.tagsManager = function(e) {
        var o = t(this);
        return 0 in this ? n[e] ? n[e].apply(o, Array.prototype.slice.call(arguments, 1)) : "object" != typeof e && e ? (t.error("Method " + e + " does not exist."), !1) : i.init.apply(this, arguments) : this
    }
}(jQuery),
function() {
    var t = this,
        e = t._,
        n = {},
        i = Array.prototype,
        o = Object.prototype,
        r = Function.prototype,
        s = i.push,
        a = i.slice,
        l = i.concat,
        c = o.toString,
        u = o.hasOwnProperty,
        d = i.forEach,
        h = i.map,
        p = i.reduce,
        f = i.reduceRight,
        m = i.filter,
        g = i.every,
        v = i.some,
        b = i.indexOf,
        y = i.lastIndexOf,
        _ = Array.isArray,
        w = Object.keys,
        x = r.bind,
        C = function(t) {
            return t instanceof C ? t : this instanceof C ? void(this._wrapped = t) : new C(t)
        };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = C), exports._ = C) : t._ = C, C.VERSION = "1.5.2";
    var k = C.each = C.forEach = function(t, e, i) {
        if (null != t)
            if (d && t.forEach === d) t.forEach(e, i);
            else if (t.length === +t.length) {
            for (var o = 0, r = t.length; r > o; o++)
                if (e.call(i, t[o], o, t) === n) return
        } else
            for (var s = C.keys(t), o = 0, r = s.length; r > o; o++)
                if (e.call(i, t[s[o]], s[o], t) === n) return
    };
    C.map = C.collect = function(t, e, n) {
        var i = [];
        return null == t ? i : h && t.map === h ? t.map(e, n) : (k(t, function(t, o, r) {
            i.push(e.call(n, t, o, r))
        }), i)
    };
    var T = "Reduce of empty array with no initial value";
    C.reduce = C.foldl = C.inject = function(t, e, n, i) {
        var o = arguments.length > 2;
        if (null == t && (t = []), p && t.reduce === p) return i && (e = C.bind(e, i)), o ? t.reduce(e, n) : t.reduce(e);
        if (k(t, function(t, r, s) {
                o ? n = e.call(i, n, t, r, s) : (n = t, o = !0)
            }), !o) throw new TypeError(T);
        return n
    }, C.reduceRight = C.foldr = function(t, e, n, i) {
        var o = arguments.length > 2;
        if (null == t && (t = []), f && t.reduceRight === f) return i && (e = C.bind(e, i)), o ? t.reduceRight(e, n) : t.reduceRight(e);
        var r = t.length;
        if (r !== +r) {
            var s = C.keys(t);
            r = s.length
        }
        if (k(t, function(a, l, c) {
                l = s ? s[--r] : --r, o ? n = e.call(i, n, t[l], l, c) : (n = t[l], o = !0)
            }), !o) throw new TypeError(T);
        return n
    }, C.find = C.detect = function(t, e, n) {
        var i;
        return $(t, function(t, o, r) {
            return e.call(n, t, o, r) ? (i = t, !0) : void 0
        }), i
    }, C.filter = C.select = function(t, e, n) {
        var i = [];
        return null == t ? i : m && t.filter === m ? t.filter(e, n) : (k(t, function(t, o, r) {
            e.call(n, t, o, r) && i.push(t)
        }), i)
    }, C.reject = function(t, e, n) {
        return C.filter(t, function(t, i, o) {
            return !e.call(n, t, i, o)
        }, n)
    }, C.every = C.all = function(t, e, i) {
        e || (e = C.identity);
        var o = !0;
        return null == t ? o : g && t.every === g ? t.every(e, i) : (k(t, function(t, r, s) {
            return (o = o && e.call(i, t, r, s)) ? void 0 : n
        }), !!o)
    };
    var $ = C.some = C.any = function(t, e, i) {
        e || (e = C.identity);
        var o = !1;
        return null == t ? o : v && t.some === v ? t.some(e, i) : (k(t, function(t, r, s) {
            return o || (o = e.call(i, t, r, s)) ? n : void 0
        }), !!o)
    };
    C.contains = C.include = function(t, e) {
        return null == t ? !1 : b && t.indexOf === b ? -1 != t.indexOf(e) : $(t, function(t) {
            return t === e
        })
    }, C.invoke = function(t, e) {
        var n = a.call(arguments, 2),
            i = C.isFunction(e);
        return C.map(t, function(t) {
            return (i ? e : t[e]).apply(t, n)
        })
    }, C.pluck = function(t, e) {
        return C.map(t, function(t) {
            return t[e]
        })
    }, C.where = function(t, e, n) {
        return C.isEmpty(e) ? n ? void 0 : [] : C[n ? "find" : "filter"](t, function(t) {
            for (var n in e)
                if (e[n] !== t[n]) return !1;
            return !0
        })
    }, C.findWhere = function(t, e) {
        return C.where(t, e, !0)
    }, C.max = function(t, e, n) {
        if (!e && C.isArray(t) && t[0] === +t[0] && t.length < 65535) return Math.max.apply(Math, t);
        if (!e && C.isEmpty(t)) return -1 / 0;
        var i = {
            computed: -1 / 0,
            value: -1 / 0
        };
        return k(t, function(t, o, r) {
            var s = e ? e.call(n, t, o, r) : t;
            s > i.computed && (i = {
                value: t,
                computed: s
            })
        }), i.value
    }, C.min = function(t, e, n) {
        if (!e && C.isArray(t) && t[0] === +t[0] && t.length < 65535) return Math.min.apply(Math, t);
        if (!e && C.isEmpty(t)) return 1 / 0;
        var i = {
            computed: 1 / 0,
            value: 1 / 0
        };
        return k(t, function(t, o, r) {
            var s = e ? e.call(n, t, o, r) : t;
            s < i.computed && (i = {
                value: t,
                computed: s
            })
        }), i.value
    }, C.shuffle = function(t) {
        var e, n = 0,
            i = [];
        return k(t, function(t) {
            e = C.random(n++), i[n - 1] = i[e], i[e] = t
        }), i
    }, C.sample = function(t, e, n) {
        return arguments.length < 2 || n ? t[C.random(t.length - 1)] : C.shuffle(t).slice(0, Math.max(0, e))
    };
    var S = function(t) {
        return C.isFunction(t) ? t : function(e) {
            return e[t]
        }
    };
    C.sortBy = function(t, e, n) {
        var i = S(e);
        return C.pluck(C.map(t, function(t, e, o) {
            return {
                value: t,
                index: e,
                criteria: i.call(n, t, e, o)
            }
        }).sort(function(t, e) {
            var n = t.criteria,
                i = e.criteria;
            if (n !== i) {
                if (n > i || void 0 === n) return 1;
                if (i > n || void 0 === i) return -1
            }
            return t.index - e.index
        }), "value")
    };
    var j = function(t) {
        return function(e, n, i) {
            var o = {},
                r = null == n ? C.identity : S(n);
            return k(e, function(n, s) {
                var a = r.call(i, n, s, e);
                t(o, a, n)
            }), o
        }
    };
    C.groupBy = j(function(t, e, n) {
        (C.has(t, e) ? t[e] : t[e] = []).push(n)
    }), C.indexBy = j(function(t, e, n) {
        t[e] = n
    }), C.countBy = j(function(t, e) {
        C.has(t, e) ? t[e] ++ : t[e] = 1
    }), C.sortedIndex = function(t, e, n, i) {
        n = null == n ? C.identity : S(n);
        for (var o = n.call(i, e), r = 0, s = t.length; s > r;) {
            var a = r + s >>> 1;
            n.call(i, t[a]) < o ? r = a + 1 : s = a
        }
        return r
    }, C.toArray = function(t) {
        return t ? C.isArray(t) ? a.call(t) : t.length === +t.length ? C.map(t, C.identity) : C.values(t) : []
    }, C.size = function(t) {
        return null == t ? 0 : t.length === +t.length ? t.length : C.keys(t).length
    }, C.first = C.head = C.take = function(t, e, n) {
        return null == t ? void 0 : null == e || n ? t[0] : a.call(t, 0, e)
    }, C.initial = function(t, e, n) {
        return a.call(t, 0, t.length - (null == e || n ? 1 : e))
    }, C.last = function(t, e, n) {
        return null == t ? void 0 : null == e || n ? t[t.length - 1] : a.call(t, Math.max(t.length - e, 0))
    }, C.rest = C.tail = C.drop = function(t, e, n) {
        return a.call(t, null == e || n ? 1 : e)
    }, C.compact = function(t) {
        return C.filter(t, C.identity)
    };
    var D = function(t, e, n) {
        return e && C.every(t, C.isArray) ? l.apply(n, t) : (k(t, function(t) {
            C.isArray(t) || C.isArguments(t) ? e ? s.apply(n, t) : D(t, e, n) : n.push(t)
        }), n)
    };
    C.flatten = function(t, e) {
        return D(t, e, [])
    }, C.without = function(t) {
        return C.difference(t, a.call(arguments, 1))
    }, C.uniq = C.unique = function(t, e, n, i) {
        C.isFunction(e) && (i = n, n = e, e = !1);
        var o = n ? C.map(t, n, i) : t,
            r = [],
            s = [];
        return k(o, function(n, i) {
            (e ? i && s[s.length - 1] === n : C.contains(s, n)) || (s.push(n), r.push(t[i]))
        }), r
    }, C.union = function() {
        return C.uniq(C.flatten(arguments, !0))
    }, C.intersection = function(t) {
        var e = a.call(arguments, 1);
        return C.filter(C.uniq(t), function(t) {
            return C.every(e, function(e) {
                return C.indexOf(e, t) >= 0
            })
        })
    }, C.difference = function(t) {
        var e = l.apply(i, a.call(arguments, 1));
        return C.filter(t, function(t) {
            return !C.contains(e, t)
        })
    }, C.zip = function() {
        for (var t = C.max(C.pluck(arguments, "length").concat(0)), e = new Array(t), n = 0; t > n; n++) e[n] = C.pluck(arguments, "" + n);
        return e
    }, C.object = function(t, e) {
        if (null == t) return {};
        for (var n = {}, i = 0, o = t.length; o > i; i++) e ? n[t[i]] = e[i] : n[t[i][0]] = t[i][1];
        return n
    }, C.indexOf = function(t, e, n) {
        if (null == t) return -1;
        var i = 0,
            o = t.length;
        if (n) {
            if ("number" != typeof n) return i = C.sortedIndex(t, e), t[i] === e ? i : -1;
            i = 0 > n ? Math.max(0, o + n) : n
        }
        if (b && t.indexOf === b) return t.indexOf(e, n);
        for (; o > i; i++)
            if (t[i] === e) return i;
        return -1
    }, C.lastIndexOf = function(t, e, n) {
        if (null == t) return -1;
        var i = null != n;
        if (y && t.lastIndexOf === y) return i ? t.lastIndexOf(e, n) : t.lastIndexOf(e);
        for (var o = i ? n : t.length; o--;)
            if (t[o] === e) return o;
        return -1
    }, C.range = function(t, e, n) {
        arguments.length <= 1 && (e = t || 0, t = 0), n = arguments[2] || 1;
        for (var i = Math.max(Math.ceil((e - t) / n), 0), o = 0, r = new Array(i); i > o;) r[o++] = t, t += n;
        return r
    };
    var E = function() {};
    C.bind = function(t, e) {
        var n, i;
        if (x && t.bind === x) return x.apply(t, a.call(arguments, 1));
        if (!C.isFunction(t)) throw new TypeError;
        return n = a.call(arguments, 2), i = function() {
            if (!(this instanceof i)) return t.apply(e, n.concat(a.call(arguments)));
            E.prototype = t.prototype;
            var o = new E;
            E.prototype = null;
            var r = t.apply(o, n.concat(a.call(arguments)));
            return Object(r) === r ? r : o
        }
    }, C.partial = function(t) {
        var e = a.call(arguments, 1);
        return function() {
            return t.apply(this, e.concat(a.call(arguments)))
        }
    }, C.bindAll = function(t) {
        var e = a.call(arguments, 1);
        if (0 === e.length) throw new Error("bindAll must be passed function names");
        return k(e, function(e) {
            t[e] = C.bind(t[e], t)
        }), t
    }, C.memoize = function(t, e) {
        var n = {};
        return e || (e = C.identity),
            function() {
                var i = e.apply(this, arguments);
                return C.has(n, i) ? n[i] : n[i] = t.apply(this, arguments)
            }
    }, C.delay = function(t, e) {
        var n = a.call(arguments, 2);
        return setTimeout(function() {
            return t.apply(null, n)
        }, e)
    }, C.defer = function(t) {
        return C.delay.apply(C, [t, 1].concat(a.call(arguments, 1)))
    }, C.throttle = function(t, e, n) {
        var i, o, r, s = null,
            a = 0;
        n || (n = {});
        var l = function() {
            a = n.leading === !1 ? 0 : new Date, s = null, r = t.apply(i, o)
        };
        return function() {
            var c = new Date;
            a || n.leading !== !1 || (a = c);
            var u = e - (c - a);
            return i = this, o = arguments, 0 >= u ? (clearTimeout(s), s = null, a = c, r = t.apply(i, o)) : s || n.trailing === !1 || (s = setTimeout(l, u)), r
        }
    }, C.debounce = function(t, e, n) {
        var i, o, r, s, a;
        return function() {
            r = this, o = arguments, s = new Date;
            var l = function() {
                    var c = new Date - s;
                    e > c ? i = setTimeout(l, e - c) : (i = null, n || (a = t.apply(r, o)))
                },
                c = n && !i;
            return i || (i = setTimeout(l, e)), c && (a = t.apply(r, o)), a
        }
    }, C.once = function(t) {
        var e, n = !1;
        return function() {
            return n ? e : (n = !0, e = t.apply(this, arguments), t = null, e)
        }
    }, C.wrap = function(t, e) {
        return function() {
            var n = [t];
            return s.apply(n, arguments), e.apply(this, n)
        }
    }, C.compose = function() {
        var t = arguments;
        return function() {
            for (var e = arguments, n = t.length - 1; n >= 0; n--) e = [t[n].apply(this, e)];
            return e[0]
        }
    }, C.after = function(t, e) {
        return function() {
            return --t < 1 ? e.apply(this, arguments) : void 0
        }
    }, C.keys = w || function(t) {
        if (t !== Object(t)) throw new TypeError("Invalid object");
        var e = [];
        for (var n in t) C.has(t, n) && e.push(n);
        return e
    }, C.values = function(t) {
        for (var e = C.keys(t), n = e.length, i = new Array(n), o = 0; n > o; o++) i[o] = t[e[o]];
        return i
    }, C.pairs = function(t) {
        for (var e = C.keys(t), n = e.length, i = new Array(n), o = 0; n > o; o++) i[o] = [e[o], t[e[o]]];
        return i
    }, C.invert = function(t) {
        for (var e = {}, n = C.keys(t), i = 0, o = n.length; o > i; i++) e[t[n[i]]] = n[i];
        return e
    }, C.functions = C.methods = function(t) {
        var e = [];
        for (var n in t) C.isFunction(t[n]) && e.push(n);
        return e.sort()
    }, C.extend = function(t) {
        return k(a.call(arguments, 1), function(e) {
            if (e)
                for (var n in e) t[n] = e[n]
        }), t
    }, C.pick = function(t) {
        var e = {},
            n = l.apply(i, a.call(arguments, 1));
        return k(n, function(n) {
            n in t && (e[n] = t[n])
        }), e
    }, C.omit = function(t) {
        var e = {},
            n = l.apply(i, a.call(arguments, 1));
        for (var o in t) C.contains(n, o) || (e[o] = t[o]);
        return e
    }, C.defaults = function(t) {
        return k(a.call(arguments, 1), function(e) {
            if (e)
                for (var n in e) void 0 === t[n] && (t[n] = e[n])
        }), t
    }, C.clone = function(t) {
        return C.isObject(t) ? C.isArray(t) ? t.slice() : C.extend({}, t) : t
    }, C.tap = function(t, e) {
        return e(t), t
    };
    var P = function(t, e, n, i) {
        if (t === e) return 0 !== t || 1 / t == 1 / e;
        if (null == t || null == e) return t === e;
        t instanceof C && (t = t._wrapped), e instanceof C && (e = e._wrapped);
        var o = c.call(t);
        if (o != c.call(e)) return !1;
        switch (o) {
            case "[object String]":
                return t == String(e);
            case "[object Number]":
                return t != +t ? e != +e : 0 == t ? 1 / t == 1 / e : t == +e;
            case "[object Date]":
            case "[object Boolean]":
                return +t == +e;
            case "[object RegExp]":
                return t.source == e.source && t.global == e.global && t.multiline == e.multiline && t.ignoreCase == e.ignoreCase
        }
        if ("object" != typeof t || "object" != typeof e) return !1;
        for (var r = n.length; r--;)
            if (n[r] == t) return i[r] == e;
        var s = t.constructor,
            a = e.constructor;
        if (s !== a && !(C.isFunction(s) && s instanceof s && C.isFunction(a) && a instanceof a)) return !1;
        n.push(t), i.push(e);
        var l = 0,
            u = !0;
        if ("[object Array]" == o) {
            if (l = t.length, u = l == e.length)
                for (; l-- && (u = P(t[l], e[l], n, i)););
        } else {
            for (var d in t)
                if (C.has(t, d) && (l++, !(u = C.has(e, d) && P(t[d], e[d], n, i)))) break;
            if (u) {
                for (d in e)
                    if (C.has(e, d) && !l--) break;
                u = !l
            }
        }
        return n.pop(), i.pop(), u
    };
    C.isEqual = function(t, e) {
        return P(t, e, [], [])
    }, C.isEmpty = function(t) {
        if (null == t) return !0;
        if (C.isArray(t) || C.isString(t)) return 0 === t.length;
        for (var e in t)
            if (C.has(t, e)) return !1;
        return !0
    }, C.isElement = function(t) {
        return !(!t || 1 !== t.nodeType)
    }, C.isArray = _ || function(t) {
        return "[object Array]" == c.call(t)
    }, C.isObject = function(t) {
        return t === Object(t)
    }, k(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function(t) {
        C["is" + t] = function(e) {
            return c.call(e) == "[object " + t + "]"
        }
    }), C.isArguments(arguments) || (C.isArguments = function(t) {
        return !(!t || !C.has(t, "callee"))
    }), "function" != typeof /./ && (C.isFunction = function(t) {
        return "function" == typeof t
    }), C.isFinite = function(t) {
        return isFinite(t) && !isNaN(parseFloat(t))
    }, C.isNaN = function(t) {
        return C.isNumber(t) && t != +t
    }, C.isBoolean = function(t) {
        return t === !0 || t === !1 || "[object Boolean]" == c.call(t)
    }, C.isNull = function(t) {
        return null === t
    }, C.isUndefined = function(t) {
        return void 0 === t
    }, C.has = function(t, e) {
        return u.call(t, e)
    }, C.noConflict = function() {
        return t._ = e, this
    }, C.identity = function(t) {
        return t
    }, C.times = function(t, e, n) {
        for (var i = Array(Math.max(0, t)), o = 0; t > o; o++) i[o] = e.call(n, o);
        return i
    }, C.random = function(t, e) {
        return null == e && (e = t, t = 0), t + Math.floor(Math.random() * (e - t + 1))
    };
    var A = {
        escape: {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;"
        }
    };
    A.unescape = C.invert(A.escape);
    var N = {
        escape: new RegExp("[" + C.keys(A.escape).join("") + "]", "g"),
        unescape: new RegExp("(" + C.keys(A.unescape).join("|") + ")", "g")
    };
    C.each(["escape", "unescape"], function(t) {
        C[t] = function(e) {
            return null == e ? "" : ("" + e).replace(N[t], function(e) {
                return A[t][e]
            })
        }
    }), C.result = function(t, e) {
        if (null == t) return void 0;
        var n = t[e];
        return C.isFunction(n) ? n.call(t) : n
    }, C.mixin = function(t) {
        k(C.functions(t), function(e) {
            var n = C[e] = t[e];
            C.prototype[e] = function() {
                var t = [this._wrapped];
                return s.apply(t, arguments), O.call(this, n.apply(C, t))
            }
        })
    };
    var M = 0;
    C.uniqueId = function(t) {
        var e = ++M + "";
        return t ? t + e : e
    }, C.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var H = /(.)^/,
        I = {
            "'": "'",
            "\\": "\\",
            "\r": "r",
            "\n": "n",
            "	": "t",
            "\u2028": "u2028",
            "\u2029": "u2029"
        },
        z = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    C.template = function(t, e, n) {
        var i;
        n = C.defaults({}, n, C.templateSettings);
        var o = new RegExp([(n.escape || H).source, (n.interpolate || H).source, (n.evaluate || H).source].join("|") + "|$", "g"),
            r = 0,
            s = "__p+='";
        t.replace(o, function(e, n, i, o, a) {
            return s += t.slice(r, a).replace(z, function(t) {
                return "\\" + I[t]
            }), n && (s += "'+\n((__t=(" + n + "))==null?'':_.escape(__t))+\n'"), i && (s += "'+\n((__t=(" + i + "))==null?'':__t)+\n'"), o && (s += "';\n" + o + "\n__p+='"), r = a + e.length, e
        }), s += "';\n", n.variable || (s = "with(obj||{}){\n" + s + "}\n"), s = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + s + "return __p;\n";
        try {
            i = new Function(n.variable || "obj", "_", s)
        } catch (a) {
            throw a.source = s, a
        }
        if (e) return i(e, C);
        var l = function(t) {
            return i.call(this, t, C)
        };
        return l.source = "function(" + (n.variable || "obj") + "){\n" + s + "}", l
    }, C.chain = function(t) {
        return C(t).chain()
    };
    var O = function(t) {
        return this._chain ? C(t).chain() : t
    };
    C.mixin(C), k(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(t) {
        var e = i[t];
        C.prototype[t] = function() {
            var n = this._wrapped;
            return e.apply(n, arguments), "shift" != t && "splice" != t || 0 !== n.length || delete n[0], O.call(this, n)
        }
    }), k(["concat", "join", "slice"], function(t) {
        var e = i[t];
        C.prototype[t] = function() {
            return O.call(this, e.apply(this._wrapped, arguments))
        }
    }), C.extend(C.prototype, {
        chain: function() {
            return this._chain = !0, this
        },
        value: function() {
            return this._wrapped
        }
    })
}.call(this),
    function(t) {
        function e(e, n) {
            function s(e) {
                return t.isArray(le.readonly) ? (e = t(".dwwl", H).index(e), le.readonly[e]) : le.readonly
            }

            function c(e) {
                var n = '<div class="dw-bf">',
                    e = pe[e],
                    e = e.values ? e : r(e),
                    i = 1,
                    o = e.values,
                    s = e.keys || o;
                return t.each(o, function(t, e) {
                    0 == i % 20 && (n += '</div><div class="dw-bf">'), n += '<div class="dw-li dw-v" data-val="' + s[t] + '" style="height:' + N + "px;line-height:" + N + 'px;"><div class="dw-i">' + e + "</div></div>", i++
                }), n += "</div>"
            }

            function u(e) {
                K = t(".dw-li", e).index(t(".dw-v", e).eq(0)), Z = t(".dw-li", e).index(t(".dw-v", e).eq(-1)), ee = t(".dw-ul", H).index(e)
            }

            function p(t) {
                var e = le.headerText;
                return e ? "function" == typeof e ? e.call(se, t) : e.replace(/\{value\}/i, t) : ""
            }

            function x() {
                oe.temp = fe && null !== oe.val && oe.val != ae.val() || null === oe.values ? le.parseValue(ae.val() || "", oe) : oe.values.slice(0), P()
            }

            function C(e) {
                var n, i = window.getComputedStyle ? getComputedStyle(e[0]) : e[0].style;
                return f ? (t.each(["t", "webkitT", "MozT", "OT", "msT"], function(t, e) {
                    return void 0 !== i[e + "ransform"] ? (n = i[e + "ransform"], !1) : void 0
                }), n = n.split(")")[0].split(", "), e = n[13] || n[5]) : e = i.top.replace("px", ""), Math.round(A - e / N)
            }

            function k(t, e) {
                clearTimeout(ue[e]), delete ue[e], t.closest(".dwwl").removeClass("dwa")
            }

            function T(t, e, n, i, o) {
                var r = (A - n) * N,
                    s = t[0].style;
                r == he[e] && ue[e] || (i && r != he[e] && S("onAnimStart", [H, e, i]), he[e] = r, s[g + "Transition"] = "all " + (i ? i.toFixed(3) : 0) + "s ease-out", f ? s[g + "Transform"] = "translate3d(0," + r + "px,0)" : s.top = r + "px", ue[e] && k(t, e), i && void 0 !== o && (t.closest(".dwwl").addClass("dwa"), ue[e] = setTimeout(function() {
                    k(t, e)
                }, 1e3 * i)), de[e] = n)
            }

            function $(e, n, i, o, r) {
                !1 !== S("validate", [H, n, e]) && (t(".dw-ul", H).each(function(i) {
                    var s = t(this),
                        a = t('.dw-li[data-val="' + oe.temp[i] + '"]', s),
                        l = t(".dw-li", s),
                        c = l.index(a),
                        u = l.length,
                        d = i == n || void 0 === n;
                    if (!a.hasClass("dw-v")) {
                        for (var h = a, p = 0, f = 0; c - p >= 0 && !h.hasClass("dw-v");) p++, h = l.eq(c - p);
                        for (; u > c + f && !a.hasClass("dw-v");) f++, a = l.eq(c + f);
                        (p > f && f && 2 !== o || !p || 0 > c - p || 1 == o) && a.hasClass("dw-v") ? c += f : (a = h, c -= p)
                    }(!a.hasClass("dw-sel") || d) && (oe.temp[i] = a.attr("data-val"), t(".dw-sel", s).removeClass("dw-sel"), a.addClass("dw-sel"), T(s, i, c, d ? e : .1, d ? r : !1))
                }), M = le.formatResult(oe.temp), "inline" == le.display ? P(i, 0, !0) : t(".dwv", H).html(p(M)), i && S("onChange", [M]))
            }

            function S(e, i) {
                var o;
                return i.push(oe), t.each([R.defaults, ce, n], function(t, n) {
                    n[e] && (o = n[e].apply(se, i))
                }), o
            }

            function j(e, n, i, o, r) {
                var n = Math.max(K, Math.min(n, Z)),
                    s = t(".dw-li", e).eq(n),
                    a = void 0 === r ? n : r,
                    l = ee,
                    c = o ? n == a ? .1 : Math.abs((n - a) * le.timeUnit) : 0;
                oe.temp[l] = s.attr("data-val"), T(e, l, n, c, r), setTimeout(function() {
                    $(c, l, !0, i, r)
                }, 10)
            }

            function D(t) {
                var e = de[ee] + 1;
                j(t, e > Z ? K : e, 1, !0)
            }

            function E(t) {
                var e = de[ee] - 1;
                j(t, K > e ? Z : e, 2, !0)
            }

            function P(t, e, n, i) {
                me && !n && $(e), M = le.formatResult(oe.temp), i || (oe.values = oe.temp.slice(0), oe.val = M), t && fe && ae.val(M).trigger("change")
            }
            var A, N, M, H, I, z, O, L, F, W, q, R, V, B, Y, Q, U, X, G, J, K, Z, te, ee, ne, ie, oe = this,
                re = t.mobiscroll,
                se = e,
                ae = t(se),
                le = v({}, w),
                ce = {},
                ue = {},
                de = {},
                he = {},
                pe = [],
                fe = ae.is("input"),
                me = !1,
                ge = function(e) {
                    i(e) && !a && !s(this) && !B && (e.preventDefault(), a = !0, Y = "clickpick" != le.mode, te = t(".dw-ul", this), u(te), J = (Q = void 0 !== ue[ee]) ? C(te) : de[ee], U = o(e, "Y"), X = new Date, G = U, T(te, ee, J, .001), Y && te.closest(".dwwl").addClass("dwa"), t(document).bind(y, ve).bind(_, be))
                },
                ve = function(t) {
                    Y && (t.preventDefault(), t.stopPropagation(), G = o(t, "Y"), T(te, ee, Math.max(K - 1, Math.min(J + (U - G) / N, Z + 1)))), Q = !0
                },
                be = function() {
                    var e, n = new Date - X,
                        i = Math.max(K - 1, Math.min(J + (U - G) / N, Z + 1)),
                        o = te.offset().top;
                    if (300 > n ? (n = (G - U) / n, e = n * n / le.speedUnit, 0 > G - U && (e = -e)) : e = G - U, n = Math.round(J - e / N), !e && !Q) {
                        var o = Math.floor((G - o) / N),
                            r = t(".dw-li", te).eq(o);
                        e = Y, !1 !== S("onValueTap", [r]) ? n = o : e = !0, e && (r.addClass("dw-hl"), setTimeout(function() {
                            r.removeClass("dw-hl")
                        }, 200))
                    }
                    Y && j(te, n, 0, !0, Math.round(i)), a = !1, te = null, t(document).unbind(y, ve).unbind(_, be)
                },
                ye = function(e) {
                    if (t(document).bind(_, _e), t(this).hasClass("dwb-d") || t(this).addClass("dwb-a"), t(this).hasClass("dwwb")) {
                        var n = t(this).closest(".dwwl");
                        if (i(e) && !s(n) && !n.hasClass("dwa")) {
                            e.stopPropagation(), e.preventDefault(), B = !0;
                            var o = n.find(".dw-ul"),
                                r = t(this).hasClass("dwwbp") ? D : E;
                            u(o), clearInterval(ne), ne = setInterval(function() {
                                r(o)
                            }, le.delay), r(o)
                        }
                    }
                },
                _e = function() {
                    B && (clearInterval(ne), B = !1), t(document).unbind(_, _e), t(".dwb-a", H).removeClass("dwb-a")
                },
                we = function(e) {
                    if (!s(this)) {
                        e.preventDefault();
                        var e = e.originalEvent,
                            e = e.wheelDelta ? e.wheelDelta / 120 : e.detail ? -e.detail / 3 : 0,
                            n = t(".dw-ul", this);
                        u(n), j(n, Math.round(de[ee] - e), 0 > e ? 1 : 2)
                    }
                };
            oe.position = function(e) {
                if (!("inline" == le.display || I === t(window).width() && O === t(window).height() && e || !1 === S("onPosition", [H]))) {
                    var n, i, o, r, s, a, l, c, u, d = 0,
                        h = 0,
                        e = t(window).scrollTop();
                    r = t(".dwwr", H);
                    var p = t(".dw", H),
                        f = {};
                    s = void 0 === le.anchor ? ae : le.anchor, I = t(window).width(), O = t(window).height(), z = (z = window.innerHeight) || O, /modal|bubble/.test(le.display) && (t(".dwc", H).each(function() {
                        n = t(this).outerWidth(!0), d += n, h = n > h ? n : h
                    }), n = d > I ? h : d, r.width(n).css("white-space", d > I ? "" : "nowrap")), L = p.outerWidth(), F = p.outerHeight(!0), "modal" == le.display ? (i = (I - L) / 2, o = e + (z - F) / 2) : "bubble" == le.display ? (u = !0, c = t(".dw-arrw-i", H), i = s.offset(), a = i.top, l = i.left, r = s.outerWidth(), s = s.outerHeight(), i = l - (p.outerWidth(!0) - r) / 2, i = i > I - L ? I - (L + 20) : i, i = i >= 0 ? i : 20, o = a - F, e > o || a > e + z ? (p.removeClass("dw-bubble-top").addClass("dw-bubble-bottom"), o = a + s) : p.removeClass("dw-bubble-bottom").addClass("dw-bubble-top"), c = c.outerWidth(), r = l + r / 2 - (i + (L - c) / 2), t(".dw-arr", H).css({
                        left: Math.max(0, Math.min(r, c))
                    })) : (f.width = "100%", "top" == le.display ? o = e : "bottom" == le.display && (o = e + z - F)), f.top = 0 > o ? 0 : o, f.left = i, p.css(f), t(".dw-persp", H).height(0).height(o + F > t(document).height() ? o + F : t(document).height()), u && (o + F > e + z || a > e + z) && t(window).scrollTop(o + F - z)
                }
            }, oe.enable = function() {
                le.disabled = !1, fe && ae.prop("disabled", !1)
            }, oe.disable = function() {
                le.disabled = !0, fe && ae.prop("disabled", !0)
            }, oe.setValue = function(e, n, i, o) {
                oe.temp = t.isArray(e) ? e.slice(0) : le.parseValue.call(se, e + "", oe), P(n, i, !1, o)
            }, oe.getValue = function() {
                return oe.values
            }, oe.getValues = function() {
                var t, e = [];
                for (t in oe._selectedValues) e.push(oe._selectedValues[t]);
                return e
            }, oe.changeWheel = function(e, n) {
                if (H) {
                    var i = 0,
                        o = e.length;
                    t.each(le.wheels, function(r, s) {
                        return t.each(s, function(r, s) {
                            return -1 < t.inArray(i, e) && (pe[i] = s, t(".dw-ul", H).eq(i).html(c(i)), o--, !o) ? (oe.position(), $(n, void 0, !0), !1) : void i++
                        }), o ? void 0 : !1
                    })
                }
            }, oe.isVisible = function() {
                return me
            }, oe.tap = function(t, e) {
                var n, i;
                le.tap && t.bind("touchstart", function(t) {
                    t.preventDefault(), n = o(t, "X"), i = o(t, "Y")
                }).bind("touchend", function(t) {
                    20 > Math.abs(o(t, "X") - n) && 20 > Math.abs(o(t, "Y") - i) && e.call(this, t), l = !0, setTimeout(function() {
                        l = !1
                    }, 300)
                }), t.bind("click", function(t) {
                    l || e.call(this, t)
                })
            }, oe.show = function(e) {
                if (le.disabled || me) return !1;
                "top" == le.display && (W = "slidedown"), "bottom" == le.display && (W = "slideup"), x(), S("onBeforeShow", []);
                var n = 0,
                    i = "";
                W && !e && (i = "dw-" + W + " dw-in");
                var o = '<div class="' + le.theme + " dw-" + le.display + (m ? " dw" + m : "") + '">' + ("inline" == le.display ? '<div class="dw dwbg dwi"><div class="dwwr">' : '<div class="dw-persp"><div class="dwo"></div><div class="dw dwbg ' + i + '"><div class="dw-arrw"><div class="dw-arrw-i"><div class="dw-arr"></div></div></div><div class="dwwr">' + (le.headerText ? '<div class="dwv"></div>' : "")) + '<div class="dwcc">';
                t.each(le.wheels, function(e, i) {
                    o += '<div class="dwc' + ("scroller" != le.mode ? " dwpm" : " dwsc") + (le.showLabel ? "" : " dwhl") + '"><div class="dwwc dwrc"><table cellpadding="0" cellspacing="0"><tr>', t.each(i, function(t, e) {
                        pe[n] = e, o += '<td><div class="dwwl dwrc dwwl' + n + '">' + ("scroller" != le.mode ? '<div class="dwb-e dwwb dwwbp" style="height:' + N + "px;line-height:" + N + 'px;"><span>+</span></div><div class="dwb-e dwwb dwwbm" style="height:' + N + "px;line-height:" + N + 'px;"><span>&ndash;</span></div>' : "") + '<div class="dwl">' + (e.label || t) + '</div><div class="dwww"><div class="dww" style="height:' + le.rows * N + "px;min-width:" + le.width + 'px;"><div class="dw-ul">', o += c(n), o += '</div><div class="dwwol"></div></div><div class="dwwo"></div></div><div class="dwwol"></div></div></td>', n++
                    }), o += "</tr></table></div></div>"
                }), o += "</div>" + ("inline" != le.display ? '<div class="dwbc' + (le.button3 ? " dwbc-p" : "") + '"><span class="dwbw dwb-s"><span class="dwb dwb-e">' + le.setText + "</span></span>" + (le.button3 ? '<span class="dwbw dwb-n"><span class="dwb dwb-e">' + le.button3Text + "</span></span>" : "") + '<span class="dwbw dwb-c"><span class="dwb dwb-e">' + le.cancelText + "</span></span></div></div>" : "") + "</div></div></div>", H = t(o), $(), S("onMarkupReady", [H]), "inline" != le.display ? (H.appendTo("body"), W && !e && (H.addClass("dw-trans"), setTimeout(function() {
                    H.removeClass("dw-trans").find(".dw").removeClass(i)
                }, 350))) : ae.is("div") ? ae.html(H) : H.insertAfter(ae), S("onMarkupInserted", [H]), me = !0, R.init(H, oe), "inline" != le.display && (oe.tap(t(".dwb-s span", H), function() {
                    oe.hide(!1, "set") !== !1 && (P(!0, 0, !0), S("onSelect", [oe.val]))
                }), oe.tap(t(".dwb-c span", H), function() {
                    oe.cancel()
                }), le.button3 && oe.tap(t(".dwb-n span", H), le.button3), le.scrollLock && H.bind("touchmove", function(t) {
                    z >= F && I >= L && t.preventDefault()
                }), t("input,select,button").each(function() {
                    this.disabled || t(this).addClass("dwtd").prop("disabled", !0).data("autocomplete", t(this).attr("autocomplete")).attr("autocomplete", "off")
                }), oe.position(), t(window).bind("orientationchange.dw resize.dw", function() {
                    clearTimeout(q), q = setTimeout(function() {
                        oe.position(!0)
                    }, 100)
                })), H.delegate(".dwwl", "DOMMouseScroll mousewheel", we).delegate(".dwb-e", b, ye).delegate(".dwwl", b, ge), S("onShow", [H, M])
            }, oe.hide = function(e, n) {
                return me && !1 !== S("onClose", [M, n]) ? (t(".dwtd").each(function() {
                    t(this).prop("disabled", !1).removeClass("dwtd"), t(this).data("autocomplete") ? t(this).attr("autocomplete", t(this).data("autocomplete")) : t(this).removeAttr("autocomplete")
                }), ae.blur(), void(H && ("inline" != le.display && W && !e ? (H.addClass("dw-trans").find(".dw").addClass("dw-" + W + " dw-out"), setTimeout(function() {
                    H.remove(), H = null
                }, 350)) : (H.remove(), H = null), me = !1, he = {}, t(window).unbind(".dw")))) : !1
            }, oe.cancel = function() {
                !1 !== oe.hide(!1, "cancel") && S("onCancel", [oe.val])
            }, oe.init = function(t) {
                R = v({
                    defaults: {},
                    init: h
                }, re.themes[t.theme || le.theme]), V = re.i18n[t.lang || le.lang], v(n, t), v(le, R.defaults, V, n), oe.settings = le, ae.unbind(".dw"), (t = re.presets[le.preset]) && (ce = t.call(se, oe), v(le, ce, n)), A = Math.floor(le.rows / 2), N = le.height, W = le.animate, me && oe.hide(), "inline" == le.display ? oe.show() : (x(), fe && le.showOnFocus && (void 0 === ie && (ie = se.readOnly), se.readOnly = !0, ae.bind("focus.dw", function() {
                    oe.show()
                })))
            }, oe.trigger = function(t, e) {
                return S(t, e)
            }, oe.option = function(t, e) {
                var n = {};
                "object" == typeof t ? n = t : n[t] = e, oe.init(n)
            }, oe.destroy = function() {
                oe.hide(), ae.unbind(".dw"), delete d[se.id], fe && (se.readOnly = ie)
            }, oe.getInst = function() {
                return oe
            }, oe.values = null, oe.val = null, oe.temp = null, oe._selectedValues = {}, oe.init(n)
        }

        function n(t) {
            for (var e in t)
                if (void 0 !== p[t[e]]) return !0;
            return !1
        }

        function i(t) {
            if ("touchstart" === t.type) c = !0;
            else if (c) return c = !1;
            return !0
        }

        function o(t, e) {
            var n = t.originalEvent,
                i = t.changedTouches;
            return i || n && n.changedTouches ? n ? n.changedTouches[0]["page" + e] : i[0]["page" + e] : t["page" + e]
        }

        function r(e) {
            var n = {
                values: [],
                keys: []
            };
            return t.each(e, function(t, e) {
                n.keys.push(t), n.values.push(e)
            }), n
        }

        function s(t, n, i) {
            var o = t;
            return "object" == typeof n ? t.each(function() {
                this.id || (u += 1, this.id = "mobiscroll" + u), d[this.id] = new e(this, n)
            }) : ("string" == typeof n && t.each(function() {
                var t;
                return (t = d[this.id]) && t[n] && (t = t[n].apply(this, Array.prototype.slice.call(i, 1)), void 0 !== t) ? (o = t, !1) : void 0
            }), o)
        }
        var a, l, c, u = (new Date).getTime(),
            d = {},
            h = function() {},
            p = document.createElement("modernizr").style,
            f = n(["perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"]),
            m = function() {
                var t, e = ["Webkit", "Moz", "O", "ms"];
                for (t in e)
                    if (n([e[t] + "Transform"])) return "-" + e[t].toLowerCase();
                return ""
            }(),
            g = m.replace(/^\-/, "").replace("moz", "Moz"),
            v = t.extend,
            b = "touchstart mousedown",
            y = "touchmove mousemove",
            _ = "touchend mouseup",
            w = {
                width: 70,
                height: 40,
                rows: 3,
                delay: 300,
                disabled: !1,
                readonly: !1,
                showOnFocus: !0,
                showLabel: !0,
                wheels: [],
                theme: "",
                headerText: "{value}",
                display: "modal",
                mode: "scroller",
                preset: "",
                lang: "en-US",
                setText: "Set",
                cancelText: "Cancel",
                scrollLock: !0,
                tap: !0,
                speedUnit: .0012,
                timeUnit: .1,
                formatResult: function(t) {
                    return t.join(" ")
                },
                parseValue: function(e, n) {
                    var i, o = e.split(" "),
                        s = [],
                        a = 0;
                    return t.each(n.settings.wheels, function(e, n) {
                        t.each(n, function(e, n) {
                            n = n.values ? n : r(n), i = n.keys || n.values, s.push(-1 !== t.inArray(o[a], i) ? o[a] : i[0]), a++
                        })
                    }), s
                }
            };
        t(document).bind("mouseover mouseup mousedown click", function(t) {
            return l ? (t.stopPropagation(), t.preventDefault(), !1) : void 0
        }), t.fn.mobiscroll = function(e) {
            return v(this, t.mobiscroll.shorts), s(this, e, arguments)
        }, t.mobiscroll = t.mobiscroll || {
            setDefaults: function(t) {
                v(w, t)
            },
            presetShort: function(t) {
                this.shorts[t] = function(e) {
                    return s(this, v(e, {
                        preset: t
                    }), arguments)
                }
            },
            shorts: {},
            presets: {},
            themes: {},
            i18n: {}
        }, t.scroller = t.scroller || t.mobiscroll, t.fn.scroller = t.fn.scroller || t.fn.mobiscroll
    }(jQuery),
    function(t) {
        var e = {
            controls: ["calendar"],
            firstDay: 0,
            firstSelectDay: 0,
            swipe: !0,
            events: !1,
            navigation: "yearMonth",
            dateText: "Date",
            timeText: "Time",
            calendarText: "Calendar",
            prevText: "Prev",
            nextText: "Next"
        };
        t.mobiscroll.presetShort("calendar"), t.mobiscroll.presets.calendar = function(n) {
            function i(t, e) {
                var n, i, o;
                if (e) {
                    if (e.dates)
                        for (n = 0; n < e.dates.length; n++)
                            if (o = e.dates[n], i = o.d || o, i.getTime() === t.getTime()) return o;
                    if (e.daysOfMonth)
                        for (n = 0; n < e.daysOfMonth.length; n++)
                            if (o = e.daysOfMonth[n], i = o.d || o, i = (i + "").split("/"), i[1]) {
                                if (i[0] - 1 == t.getMonth() && i[1] == t.getDate()) return o
                            } else if (i[0] == t.getDate()) return o || !0;
                    if (e.daysOfWeek)
                        for (n = 0; n < e.daysOfWeek.length; n++)
                            if (o = e.daysOfWeek[n], i = o.d || o, i === t.getDay()) return o || !0
                }
                return !1
            }

            function o(e, o) {
                var r, s, a, l, c, u, d, h, p, f, m, g, v, b = 1,
                    y = 0;
                s = new Date(e, o, 1);
                var _ = s.getFullYear(),
                    w = s.getMonth(),
                    x = n.getDate(!0),
                    C = new Date(_, w + 1, 0).getDate(),
                    k = new Date(_, w, 1).getDay(),
                    T = new Date(_, w, 0).getDate() - k + 1,
                    j = '<div class="dw-cal-table">';
                for (1 < W.firstDay - k + 1 && (y = 7), v = 0; 42 > v; v++) g = v + W.firstDay - y, r = new Date(_, w, g - k + 1), s = r.getFullYear(), a = r.getMonth(), l = r.getDate(), c = r < new Date($.getFullYear(), $.getMonth(), $.getDate()) || r > S ? !1 : !1 === i(r, W.invalid), h = (u = i(r, W.marked)) ? u.color : null, h ? (d = t('<div style="background-color:' + h + ';"></div>').appendTo("body"), p = d.css("background-color").replace(/rgb|rgba|\(|\)|\s/g, "").split(","), p = .299 * p[0] + .587 * p[1] + .114 * p[2], d.remove(), p = p > 130 ? "#000" : "#fff") : p = void 0, f = u.text, d = Q ? void 0 !== n._selectedValues[r] : x.getFullYear() === s && x.getMonth() === a && x.getDate() === l, m = k > g || g >= C + k, 0 == v % 7 && (j += (v ? "</div>" : "") + '<div class="dw-cal-row' + (!Q && x - r >= 0 && 6048e5 > x - r ? " dw-cal-week-hl" : "") + '">', q && ("month" == q && a !== w && v ? b = 1 == l ? 1 : 2 : "year" == q && (b = r, b = new Date(b), b.setHours(0, 0, 0), b.setDate(b.getDate() + 4 - (b.getDay() || 7)), r = new Date(b.getFullYear(), 0, 1), b = Math.ceil(((b - r) / 864e5 + 1) / 7)), j += '<div class="dw-week-nr">' + b + "</div>", b++)), f = '<div class="dw-cal-day-txt' + (V ? " ui-shadow" + (d ? " ui-btn-up-c" : " ui-btn-up-b") : "") + '" title="' + f + '"' + (h ? ' style="background:' + h + ";color:" + p + ';text-shadow:none;"' : "") + ">" + f + "</div>", j += '<div class="dw-cal-day' + (V ? " ui-body-c" : "") + (d ? " dw-sel" : "") + (c && m ? " dw-cal-day-diff" : "") + (c ? " dw-cal-day-v dwb-e" : "") + '" data-date="' + (g - k + 1) + '" data-full="' + s + "-" + a + "-" + l + '"><div class="dw-i' + (d && V ? " ui-btn-active" : "") + (c && V ? ' ui-btn-up-c ui-state-default ui-btn" data-theme="c"' : '"') + (u ? '><div class="dw-cal-day-fg">' : ">") + (k > g ? T + g : g >= C + k ? g - C - k + 1 : g - k + 1) + (u ? "</div>" : "") + (U ? u.text ? '<div class="dw-cal-day-txt-c">' + f + "</div>" : "" : u ? '<div class="dw-cal-day-m"' + (h ? ' style="background-color:' + h + ";border-color:" + h + " " + h + ' transparent transparent"' : "") + "></div>" : "") + "</div></div>";
                return j + "</div></div>"
            }

            function r(e, n) {
                t(".dw-cal-month", g).text(x[n]), t(".dw-cal-year", g).text(e), new Date(e, n - 1, 1) < k ? t(".dw-cal-prev-m", g).addClass(J) : t(".dw-cal-prev-m", g).removeClass(J), new Date(e, n + 1, 1) > T ? t(".dw-cal-next-m", g).addClass(J) : t(".dw-cal-next-m", g).removeClass(J), new Date(e - 1, n, 1) < k ? t(".dw-cal-prev-y", g).addClass(J) : t(".dw-cal-prev-y", g).removeClass(J), new Date(e + 1, n, 1) > T ? t(".dw-cal-next-y", g).addClass(J) : t(".dw-cal-next-y", g).removeClass(J)
            }

            function s() {
                var e = n.getDate(!0);
                e.getFullYear() === D && e.getMonth() === E ? (j = e, e = n.getDate(!0), e.getFullYear() === D && e.getMonth() === E && !Q && (t(".dw-cal .dw-sel", g).removeClass("dw-sel"), e = t('.dw-cal .dw-cal-day[data-full="' + D + "-" + E + "-" + e.getDate() + '"]', g).addClass("dw-sel").parent(), t(".dw-cal-week-hl", g).removeClass("dw-cal-week-hl"), e && e.addClass("dw-cal-week-hl"), V && (t(".dw-cal .ui-btn-active", g).removeClass("ui-btn-active"), t(".dw-cal .dw-cal-day-txt", g).removeClass("ui-btn-up-c").addClass("ui-btn-up-b"), t(".dw-cal .dw-sel .dw-i", g).addClass("ui-btn-active"), t(".dw-cal .dw-sel .dw-cal-day-txt", g).removeClass("ui-btn-up-b").addClass("ui-btn-up-c")))) : e > j ? (D = e.getFullYear(), E = e.getMonth(), l.call(this, D, E, "next", !0)) : j > e && (D = e.getFullYear(), E = e.getMonth(), l.call(this, D, E, "prev", !0))
            }

            function a(t, e, n) {
                y.html(o(t, e - 1)), _.html(n), w.html(o(t, e + 1))
            }

            function l(t, e, i, s) {
                if (z) O.push({
                    y: t,
                    m: e,
                    dir: i,
                    load: s
                });
                else {
                    var c = new Date(t, e, 1),
                        t = c.getFullYear(),
                        e = c.getMonth();
                    n.trigger("onMonthChange", [t, e]), z = !0, "next" == i ? (s && w.html(o(t, e)), C = w.html(), y.hide(), w.show(), v.removeClass("dw-cal-anim-prev")) : (s && y.html(o(t, e)), C = y.html(), y.show(), w.hide(), v.addClass("dw-cal-anim-prev")), r(t, e), setTimeout(function() {
                        "next" == i ? v.addClass("dw-cal-anim-a dw-cal-anim-prev") : v.addClass("dw-cal-anim-a").removeClass("dw-cal-anim-prev"), setTimeout(function() {
                            if (front.html(C), v.removeClass("dw-cal-anim-a"), a(t, e, C), z = !1, O.length) {
                                var n = O.shift();
                                l(n.y, n.m, n.dir, n.load)
                            } else D = t, E = e, j = new Date(t, e, 1)
                        }, 200)
                    }, 10)
                }
            }

            function c() {
                var e = new Date(D, E, t(this).attr("data-date"));
                if (Q)
                    if ("week" == W.selectType) {
                        var i, o = t(this).hasClass("dw-sel"),
                            r = e.getDay() - W.firstSelectDay,
                            s = 0,
                            r = 0 > r ? 7 + r : r;
                        for (W.multiSelect || (n._selectedValues = {}), s; 7 > s; s++) i = new Date(e.getFullYear(), e.getMonth(), e.getDate() - r + s), o ? delete n._selectedValues[i] : n._selectedValues[i] = i;
                        u()
                    } else i = t('.dw-cal .dw-cal-day[data-full="' + t(this).attr("data-full") + '"]', g).toggleClass("dw-sel"), V && (t(".dw-i", i).toggleClass("ui-btn-active"), t(".dw-cal-day-txt", i).toggleClass("ui-btn-up-b ui-btn-up-c")), t(this).hasClass("dw-sel") ? n._selectedValues[e] = e : delete n._selectedValues[e];
                i = "inline" === W.display, o = n.getDate(!0), n.trigger("onDayChange", [{
                    date: e,
                    marked: 0 < t(".dw-cal-day-m", this).length,
                    cell: this
                }]), n.setDate(new Date(D, E, t(this).attr("data-date"), o.getHours(), o.getMinutes(), o.getSeconds()), i, .2, !i)
            }

            function u() {
                n.isVisible() && (C = o(D, E), front.html(C), a(D, E, C))
            }

            function d(t, e) {
                var n = t.originalEvent,
                    i = t.changedTouches;
                    //alert( n.changedTouches[0]["page" + e]);
                return i || n && n.changedTouches ? n ? n.changedTouches[0]["page" + e] : i[0]["page" + e] : t["page" + e]
            }

            function h() {
                var e, n = 0,
                    i = 0;
                (X || G) && (t(".dwc", g).removeClass("dwc-h"), t.each(F, function(t, o) {
                    e = o.outerWidth(!0), n = Math.max(n, e), i += e
                })), X || i > g.parent().width() ? (t(".dw", g).addClass("dw-cal-tabbed"), t(".dwc", g).addClass("dwc-h"), F[t(".dw-cal-tabs .dw-sel", g).attr("data-control")].removeClass("dwc-h"), "liquid" !== R && t(".dwcc", g).width(n)) : (t(".dwcc", g).width("auto"), t(".dw", g).removeClass("dw-cal-tabbed"))
            }

            function p() {
                if (B) {
                    var e = 0;
                    t(".dw-cal-btnc", g).addClass("dw-cal-btnc-test"), t.each(W.monthNames, function(n, i) {
                        e = Math.max(t(".dw-cal-month", g).text(i).outerWidth(!0), e)
                    }), t(".dw-cal-btnc", g).addClass("dw-cal-btnc-test"), x = e > t(".dw-cal-btnw", g).width() ? W.monthNamesShort : W.monthNames, t(".dw-cal-month", g).text(x[E])
                }
            }
            var f, m, g, v, b, y, _, w, x, C, k, T, $, S, j, D, E, P, A, N, M, H, I, z = !1,
                O = [],
                L = {},
                F = {};
            f = t.extend({}, n.settings);
            var W = t.extend(n.settings, e, f),
                q = W.weekCounter,
                R = W.layout || (/top|bottom/.test(W.display) ? "liquid" : ""),
                V = "jqm" == W.theme,
                B = "yearMonth" == W.navigation,
                Y = W.markedDisplay;
            f = W.controls.join(",");
            var Q = W.multiSelect || "week" == W.selectType,
                U = W.events,
                X = (!0 === W.tabs || !1 !== W.tabs && "liquid" == R) && 1 < W.controls.length,
                G = !X && void 0 === W.tabs && "liquid" !== R && 1 < W.controls.length,
                J = "dwb-d" + (V ? " ui-disabled" : "");
            if (f.match(/date/) && (F.date = 1), f.match(/time/) && (F.time = 1), n.settings.preset = "date" + (F.time ? "time" : ""), n.settings.selectedValues)
                for (m = 0; m < n.settings.selectedValues.length; m++) f = n.settings.selectedValues[m], n._selectedValues[f] = f;
            return n._setValue || (n._setValue = n.setValue), n.setValue = function() {
                n._setValue.apply(this, arguments), n.isVisible() ? s() : j = n.getDate(!0)
            }, n.addValue = function(t) {
                n._selectedValues[t] = t, u()
            }, n.removeValue = function(t) {
                delete n._selectedValues[t], u()
            }, n.setValues = function(t) {
                var e = 0;
                for (n._selectedValues = {}, e; e < t.length; e++) n._selectedValues[t[e]] = t[e];
                u()
            }, n.refresh = u, f = t.mobiscroll.presets.datetime.call(this, n), I = f.validate, t.extend(f, {
                onMarkupReady: function(e) {
                    g = e;
                    var i, r, s = "",
                        u = W.dateOrder.search(/m/i),
                        f = W.dateOrder.search(/y/i);
                    for (j = n.getDate(!0), D = j.getFullYear(), E = j.getMonth(), x = W.monthNames, W.minDate ? (k = new Date(W.minDate.getFullYear(), W.minDate.getMonth(), 1), $ = W.minDate) : $ = k = new Date(W.startYear, 0, 1), W.maxDate ? (T = new Date(W.maxDate.getFullYear(), W.maxDate.getMonth(), 1), S = W.maxDate) : S = T = new Date(W.endYear, 11, 31, 23, 59, 59), e.addClass("dw-calendar"), F.date ? F.date = t(".dwc", g).eq(0) : t(".dwc", g).eq(0).addClass("dwc-h").hide(), F.time && (F.time = t(".dwc", g).eq(1)), r = '<div class="dw-cal-btnw"><div class="dw-cal-prev dw-cal-prev-m dw-cal-btn dwb dwb-e"><div class="dw-cal-btn-txt"' + (V ? 'data-role="button" data-icon="arrow-l" data-iconpos="notext"' : "") + ">" + W.prevText + "</div></div>" + (!B && u > f ? '<span class="dw-cal-year">' + D + "</span>&nbsp;" : "") + '<span class="dw-cal-month">' + x[E] + "</span>" + (!B && f > u ? '&nbsp;<span class="dw-cal-year">' + D + "</span>" : "") + '<div class="dw-cal-next dw-cal-next-m dw-cal-btn dwb dwb-e"><div class="dw-cal-btn-txt"' + (V ? 'data-role="button" data-icon="arrow-r" data-iconpos="notext"' : "") + ">" + W.nextText + "</div></div></div>", B && (s = '<div class="dw-cal-btnw"><div class="dw-cal-prev dw-cal-prev-y dw-cal-btn dwb dwb-e"><div class="dw-cal-btn-txt"' + (V ? 'data-role="button" data-icon="arrow-l" data-iconpos="notext"' : "") + ">" + W.prevText + '</div></div><span class="dw-cal-year">' + D + '</span><div class="dw-cal-next dw-cal-next-y dw-cal-btn dwb dwb-e"><div class="dw-cal-btn-txt"' + (V ? 'data-role="button" data-icon="arrow-r" data-iconpos="notext"' : "") + ">" + W.nextText + "</div></div></div>"), b = '<div class="dwc dw-cal-c"><div class="dw-cal' + (U ? " dw-cal-ev" : Y ? " dw-cal-m-" + Y : "") + (q ? " dw-weeks" : "") + (V ? " ui-body-" + W.jqmBody : "") + '"><div class="dw-cal-header"><div class="dw-cal-btnc ' + (B ? "dw-cal-btnc-ym" : "dw-cal-btnc-m") + '">' + (u > f ? s + r : r + s) + '</div><div class="dw-cal-days"><table cellpadding="0" cellspacing="0"><tr>', m = 0; 7 > m; m++) b += "<th>" + W.dayNamesShort[(m + W.firstDay) % 7] + "</th>";
                    if (b += '</tr></table></div></div><div class="dw-cal-anim-c"><div class="dw-cal-front"></div><div class="dw-cal-anim"><div class="dw-cal-slide dw-cal-slide1" style="display:none;"></div><div class="dw-cal-slide dw-cal-slide2"></div><div class="dw-cal-slide dw-cal-slide3"></div></div></div><div class="dw-cal-f"></div></div></div>', F.calendar = t(b), t.each(W.controls, function(e, n) {
                            t(".dwcc", g).append(F[n])
                        }), i = '<div class="dw-cal-tabs"' + (V ? ' data-role="navbar"' : "") + "><ul>", t.each(W.controls, function(t, e) {
                            F[e] && (i += '<li class="dw-cal-tab' + (t ? "" : " dw-sel") + '" data-control="' + e + '"><' + (V ? 'a href="#"' : 'span class="dw-i"') + (!t && V ? ' class="ui-btn-active"' : "") + ">" + W[e + "Text"] + "</" + (V ? "a" : "span") + "></li>")
                        }), i += "</ul></div>", t(".dwcc", e).before(i), v = t(".dw-cal-anim-c", g), front = t(".dw-cal-front", g), y = t(".dw-cal-slide1", g), _ = t(".dw-cal-slide2", g), w = t(".dw-cal-slide3", g), C = o(D, E), front.html(C), a(D, E, C), v.delegate(".dw-cal-day-v", "touchstart mousedown", function(t) {
                            if ("touchstart" === t.type) J = !0;
                            else if (J) return !1;
                            P = d(t, "X"), A = d(t, "Y")
                        }).delegate(".dw-cal-day-v", "touchend touchcancel", function(e) {
                            !z && Math.abs(d(e, "X") - P) < 20 && Math.abs(d(e, "Y") - A) < 20 && c.call(this), t(".dwb-a", g).removeClass("dwb-a")
                        }).delegate(".dw-cal-day-v", "click", function() {
                            !z && !J && c.call(this), J = !1
                        }), n.tap(t(".dw-cal-prev-m", g), function() {
                            new Date(D, E - 1, 1) >= k && O.length < 5 && l(D, --E, "prev")
                        }), n.tap(t(".dw-cal-next-m", g), function() {
                            new Date(D, E + 1, 1) <= T && O.length < 5 && l(D, ++E, "next")
                        }), n.tap(t(".dw-cal-prev-y", g), function() {
                            new Date(D - 1, E, 1) >= k && O.length < 5 && (D--, l(D, E, "prev", !0))
                        }), n.tap(t(".dw-cal-next-y", g), function() {
                            new Date(D + 1, E, 1) <= T && O.length < 5 && (D++, l(D, E, "next", !0))
                        }), n.tap(t(".dw-cal-tab", g), function() {
                            t(".dwc", g).addClass("dwc-h"), t(".dw-cal-tab", g).removeClass("dw-sel"), t(this).addClass("dw-sel"), F[t(this).attr("data-control")].removeClass("dwc-h")
                        }), "liquid" == R && "bubble" !== W.display ? e.addClass("dw-cal-liq") : W.calendarWidth && t(".dw-cal", g).width(W.calendarWidth), W.calendarHeight && t(".dw-cal-anim-c", g).height(W.calendarHeight), t(window).bind(void 0 !== window.onorientationchange ? "orientationchange.dw-cal" : "resize.dw-cal", function() {
                            clearTimeout(H), H = setTimeout(function() {
                                G && h(), p(), g[0].style.display = "run-in", setTimeout(function() {
                                    g[0].style.display = ""
                                }, 0)
                            }, 100)
                        }), t(".dwwl", e).bind("mousedown touchstart", function() {
                            clearTimeout(L[t(".dwwl", e).index(this)])
                        }), W.swipe) {
                        var I, Q, X = !0,
                            J = void 0 !== window.ontouchstart;
                        v.bind(J ? "touchstart" : "mousedown", function(e) {
                            X && (I = new Date, P = d(e, "X"), A = d(e, "Y"), X = !1, t(document).bind(J ? "touchmove.dw-sw" : "mousemove.dw-sw", function(e) {
                                N = d(e, "X"), M = d(e, "Y"), Q = new Date, diff = N - P, !z && (Math.abs(N - P) > 20 || Math.abs(M - A) > 20) && t(".dwb-a", g).removeClass("dwb-a"), 300 > Q - I && Math.abs(diff) > 20 && e.preventDefault()
                            }).bind(J ? "touchend.dw-sw touchcancel.dw-sw" : "mouseup.dw-sw", function(e) {
                                X = !0, Q = new Date, N = d(e, "X"), diff = N - P, 300 > Q - I && O.length < 5 && (diff > 50 && new Date(D, E - 1, 1) >= k ? l(D, --E, "prev") : diff < -50 && new Date(D, E + 1, 1) <= T && l(D, ++E, "next")), t(document).unbind(".dw-sw")
                            }))
                        })
                    }
                },
                onMarkupInserted: function() {
                    p(), h()
                },
                onPosition: function() {
                    if ("liquid" == R && "modal" == W.display) {
                        v.height("auto");
                        var e = window.innerHeight || t(window).height(),
                            n = t(".dw", g).outerHeight(),
                            i = n - v.height();
                        e > n && v.height(e - i)
                    }
                },
                onClose: function() {
                    t(window).unbind(".dw-cal")
                },
                validate: function(t, e, n) {
                    void 0 !== e && (clearTimeout(L[e]), L[e] = setTimeout(function() {
                        s()
                    }, 1e3 * n)), I.call(this, t, e)
                }
            }), f
        }
    }(jQuery),
    function(t) {
        var e = t.mobiscroll,
            n = new Date,
            i = {
                dateFormat: "mm/dd/yy",
                dateOrder: "mmddy",
                timeWheels: "hhiiA",
                timeFormat: "hh:ii A",
                startYear: n.getFullYear() - 100,
                endYear: n.getFullYear() + 1,
                monthNames: "January,February,March,April,May,June,July,August,September,October,November,December".split(","),
                monthNamesShort: "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),
                dayNames: "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),
                dayNamesShort: "Sun,Mon,Tue,Wed,Thu,Fri,Sat".split(","),
                shortYearCutoff: "+10",
                monthText: "Month",
                dayText: "Day",
                yearText: "Year",
                hourText: "Hours",
                minuteText: "Minutes",
                secText: "Seconds",
                ampmText: "&nbsp;",
                nowText: "Now",
                showNow: !1,
                stepHour: 1,
                stepMinute: 1,
                stepSecond: 1,
                separator: " "
            },
            o = function(n) {
                function o(t, e, n) {
                    return void 0 !== _[e] ? +t[_[e]] : void 0 !== n ? n : D[w[e]] ? D[w[e]]() : w[e](D)
                }

                function r(t, e, n, i) {
                    t.push({
                        values: n,
                        keys: e,
                        label: i
                    })
                }

                function s(t, e) {
                    return Math.floor(t / e) * e
                }

                function a(t) {
                    var e = o(t, "h", 0);
                    return new Date(o(t, "y"), o(t, "m"), o(t, "d", 1), o(t, "a") ? e + 12 : e, o(t, "i", 0), o(t, "s", 0))
                }
                var l, c = t(this),
                    u = {};
                if (c.is("input")) {
                    switch (c.attr("type")) {
                        case "date":
                            l = "yy-mm-dd";
                            break;
                        case "datetime":
                            l = "yy-mm-ddTHH:ii:ssZ";
                            break;
                        case "datetime-local":
                            l = "yy-mm-ddTHH:ii:ss";
                            break;
                        case "month":
                            l = "yy-mm", u.dateOrder = "mmyy";
                            break;
                        case "time":
                            l = "HH:ii:ss"
                    }
                    var d = c.attr("min"),
                        c = c.attr("max");
                    d && (u.minDate = e.parseDate(l, d)), c && (u.maxDate = e.parseDate(l, c))
                }
                var h, p, f, m, g, d = t.extend({}, n.settings),
                    v = t.extend(n.settings, i, u, d),
                    b = 0,
                    c = [],
                    y = [],
                    _ = {},
                    w = {
                        y: "getFullYear",
                        m: "getMonth",
                        d: "getDate",
                        h: function(t) {
                            return t = t.getHours(), t = S && t >= 12 ? t - 12 : t, s(t, E)
                        },
                        i: function(t) {
                            return s(t.getMinutes(), P)
                        },
                        s: function(t) {
                            return s(t.getSeconds(), A)
                        },
                        a: function(t) {
                            return $ && 11 < t.getHours() ? 1 : 0
                        }
                    },
                    x = v.preset,
                    C = v.dateOrder,
                    k = v.timeWheels,
                    T = C.match(/D/),
                    $ = k.match(/a/i),
                    S = k.match(/h/),
                    j = "datetime" == x ? v.dateFormat + v.separator + v.timeFormat : "time" == x ? v.timeFormat : v.dateFormat,
                    D = new Date,
                    E = v.stepHour,
                    P = v.stepMinute,
                    A = v.stepSecond,
                    N = v.minDate || new Date(v.startYear, 0, 1),
                    M = v.maxDate || new Date(v.endYear, 11, 31, 23, 59, 59);
                if (l = l || j, x.match(/date/i)) {
                    for (t.each(["y", "m", "d"], function(t, e) {
                            h = C.search(RegExp(e, "i")), h > -1 && y.push({
                                o: h,
                                v: e
                            })
                        }), y.sort(function(t, e) {
                            return t.o > e.o ? 1 : -1
                        }), t.each(y, function(t, e) {
                            _[e.v] = t
                        }), d = [], u = 0; 3 > u; u++)
                        if (u == _.y) {
                            for (b++, f = [], p = [], m = N.getFullYear(), g = M.getFullYear(), h = m; g >= h; h++) p.push(h), f.push(C.match(/yy/i) ? h : (h + "").substr(2, 2));
                            r(d, p, f, v.yearText)
                        } else if (u == _.m) {
                        for (b++, f = [], p = [], h = 0; 12 > h; h++) m = C.replace(/[dy]/gi, "").replace(/mm/, 9 > h ? "0" + (h + 1) : h + 1).replace(/m/, h + 1), p.push(h), f.push(m.match(/MM/) ? m.replace(/MM/, '<span class="dw-mon">' + v.monthNames[h] + "</span>") : m.replace(/M/, '<span class="dw-mon">' + v.monthNamesShort[h] + "</span>"));
                        r(d, p, f, v.monthText)
                    } else if (u == _.d) {
                        for (b++, f = [], p = [], h = 1; 32 > h; h++) p.push(h), f.push(C.match(/dd/i) && 10 > h ? "0" + h : h);
                        r(d, p, f, v.dayText)
                    }
                    c.push(d)
                }
                if (x.match(/time/i)) {
                    for (y = [], t.each(["h", "i", "s", "a"], function(t, e) {
                            t = k.search(RegExp(e, "i")), t > -1 && y.push({
                                o: t,
                                v: e
                            })
                        }), y.sort(function(t, e) {
                            return t.o > e.o ? 1 : -1
                        }), t.each(y, function(t, e) {
                            _[e.v] = b + t
                        }), d = [], u = b; b + 4 > u; u++)
                        if (u == _.h) {
                            for (b++, f = [], p = [], h = 0;
                                (S ? 12 : 24) > h; h += E) p.push(h), f.push(S && 0 == h ? 12 : k.match(/hh/i) && 10 > h ? "0" + h : h);
                            r(d, p, f, v.hourText)
                        } else if (u == _.i) {
                        for (b++, f = [], p = [], h = 0; 60 > h; h += P) p.push(h), f.push(k.match(/ii/) && 10 > h ? "0" + h : h);
                        r(d, p, f, v.minuteText)
                    } else if (u == _.s) {
                        for (b++, f = [], p = [], h = 0; 60 > h; h += A) p.push(h), f.push(k.match(/ss/) && 10 > h ? "0" + h : h);
                        r(d, p, f, v.secText)
                    } else u == _.a && (b++, p = k.match(/A/), r(d, [0, 1], p ? ["AM", "PM"] : ["am", "pm"], v.ampmText));
                    c.push(d)
                }
                return n.setDate = function(t, e, i, o) {
                    for (var r in _) n.temp[_[r]] = t[w[r]] ? t[w[r]]() : w[r](t);
                    n.setValue(n.temp, e, i, o)
                }, n.getDate = function(t) {
                    return a(t ? n.temp : n.values)
                }, {
                    button3Text: v.showNow ? v.nowText : void 0,
                    button3: v.showNow ? function() {
                        n.setDate(new Date, !1, .3, !0)
                    } : void 0,
                    wheels: c,
                    headerText: function() {
                        return e.formatDate(j, a(n.temp), v)
                    },
                    formatResult: function(t) {
                        return e.formatDate(l, a(t), v)
                    },
                    parseValue: function(t) {
                        var n, i = new Date,
                            o = [];
                        try {
                            i = e.parseDate(l, t, v)
                        } catch (r) {}
                        for (n in _) o[_[n]] = i[w[n]] ? i[w[n]]() : w[n](i);
                        return o
                    },
                    validate: function(e) {
                        var i = n.temp,
                            r = {
                                y: N.getFullYear(),
                                m: 0,
                                d: 1,
                                h: 0,
                                i: 0,
                                s: 0,
                                a: 0
                            },
                            a = {
                                y: M.getFullYear(),
                                m: 11,
                                d: 31,
                                h: s(S ? 11 : 23, E),
                                i: s(59, P),
                                s: s(59, A),
                                a: 1
                            },
                            l = !0,
                            c = !0;
                        t.each("y,m,d,a,h,i,s".split(","), function(n, s) {
                            if (void 0 !== _[s]) {
                                var u, d, h = r[s],
                                    p = a[s],
                                    f = 31,
                                    m = o(i, s),
                                    g = t(".dw-ul", e).eq(_[s]);
                                if ("d" == s && (u = o(i, "y"), d = o(i, "m"), p = f = 32 - new Date(u, d, 32).getDate(), T && t(".dw-li", g).each(function() {
                                        var e = t(this),
                                            n = e.data("val"),
                                            i = new Date(u, d, n).getDay(),
                                            n = C.replace(/[my]/gi, "").replace(/dd/, 10 > n ? "0" + n : n).replace(/d/, n);
                                        t(".dw-i", e).html(n.match(/DD/) ? n.replace(/DD/, '<span class="dw-day">' + v.dayNames[i] + "</span>") : n.replace(/D/, '<span class="dw-day">' + v.dayNamesShort[i] + "</span>"))
                                    })), l && N && (h = N[w[s]] ? N[w[s]]() : w[s](N)), c && M && (p = M[w[s]] ? M[w[s]]() : w[s](M)), "y" != s) {
                                    var b = t(".dw-li", g).index(t('.dw-li[data-val="' + h + '"]', g)),
                                        y = t(".dw-li", g).index(t('.dw-li[data-val="' + p + '"]', g));
                                    t(".dw-li", g).removeClass("dw-v").slice(b, y + 1).addClass("dw-v"), "d" == s && t(".dw-li", g).removeClass("dw-h").slice(f).addClass("dw-h")
                                }
                                if (h > m && (m = h), m > p && (m = p), l && (l = m == h), c && (c = m == p), v.invalid && "d" == s) {
                                    var x = [];
                                    if (v.invalid.dates && t.each(v.invalid.dates, function(t, e) {
                                            e.getFullYear() == u && e.getMonth() == d && x.push(e.getDate() - 1)
                                        }), v.invalid.daysOfWeek) {
                                        var k, $ = new Date(u, d, 1).getDay();
                                        t.each(v.invalid.daysOfWeek, function(t, e) {
                                            for (k = e - $; f > k; k += 7) k >= 0 && x.push(k)
                                        })
                                    }
                                    v.invalid.daysOfMonth && t.each(v.invalid.daysOfMonth, function(t, e) {
                                        e = (e + "").split("/"), e[1] ? e[0] - 1 == d && x.push(e[1] - 1) : x.push(e[0] - 1)
                                    }), t.each(x, function(e, n) {
                                        t(".dw-li", g).eq(n).removeClass("dw-v")
                                    })
                                }
                                i[_[s]] = m
                            }
                        })
                    }
                }
            };
        t.each(["date", "time", "datetime"], function(t, n) {
            e.presets[n] = o, e.presetShort(n)
        }), e.formatDate = function(e, n, o) {
            if (!n) return null;
            var r, o = t.extend({}, i, o),
                s = function(t) {
                    for (var n = 0; r + 1 < e.length && e.charAt(r + 1) == t;) n++, r++;
                    return n
                },
                a = function(t, e, n) {
                    if (e = "" + e, s(t))
                        for (; e.length < n;) e = "0" + e;
                    return e
                },
                l = function(t, e, n, i) {
                    return s(t) ? i[e] : n[e]
                },
                c = "",
                u = !1;
            for (r = 0; r < e.length; r++)
                if (u) "'" != e.charAt(r) || s("'") ? c += e.charAt(r) : u = !1;
                else switch (e.charAt(r)) {
                    case "d":
                        c += a("d", n.getDate(), 2);
                        break;
                    case "D":
                        c += l("D", n.getDay(), o.dayNamesShort, o.dayNames);
                        break;
                    case "o":
                        c += a("o", (n.getTime() - new Date(n.getFullYear(), 0, 0).getTime()) / 864e5, 3);
                        break;
                    case "m":
                        c += a("m", n.getMonth() + 1, 2);
                        break;
                    case "M":
                        c += l("M", n.getMonth(), o.monthNamesShort, o.monthNames);
                        break;
                    case "y":
                        c += s("y") ? n.getFullYear() : (10 > n.getYear() % 100 ? "0" : "") + n.getYear() % 100;
                        break;
                    case "h":
                        var d = n.getHours(),
                            c = c + a("h", d > 12 ? d - 12 : 0 == d ? 12 : d, 2);
                        break;
                    case "H":
                        c += a("H", n.getHours(), 2);
                        break;
                    case "i":
                        c += a("i", n.getMinutes(), 2);
                        break;
                    case "s":
                        c += a("s", n.getSeconds(), 2);
                        break;
                    case "a":
                        c += 11 < n.getHours() ? "pm" : "am";
                        break;
                    case "A":
                        c += 11 < n.getHours() ? "PM" : "AM";
                        break;
                    case "'":
                        s("'") ? c += "'" : u = !0;
                        break;
                    default:
                        c += e.charAt(r)
                }
                return c
        }, e.parseDate = function(e, n, o) {
            var r = new Date;
            if (!e || !n) return r;
            var s, n = "object" == typeof n ? n.toString() : n + "",
                a = t.extend({}, i, o),
                l = a.shortYearCutoff,
                o = r.getFullYear(),
                c = r.getMonth() + 1,
                u = r.getDate(),
                d = -1,
                h = r.getHours(),
                r = r.getMinutes(),
                p = 0,
                f = -1,
                m = !1,
                g = function(t) {
                    return (t = s + 1 < e.length && e.charAt(s + 1) == t) && s++, t
                },
                v = function(t) {
                    return g(t), (t = n.substr(y).match(RegExp("^\\d{1," + ("@" == t ? 14 : "!" == t ? 20 : "y" == t ? 4 : "o" == t ? 3 : 2) + "}"))) ? (y += t[0].length, parseInt(t[0], 10)) : 0
                },
                b = function(t, e, i) {
                    for (t = g(t) ? i : e, e = 0; e < t.length; e++)
                        if (n.substr(y, t[e].length).toLowerCase() == t[e].toLowerCase()) return y += t[e].length, e + 1;
                    return 0
                },
                y = 0;
            for (s = 0; s < e.length; s++)
                if (m) "'" != e.charAt(s) || g("'") ? y++ : m = !1;
                else switch (e.charAt(s)) {
                    case "d":
                        u = v("d");
                        break;
                    case "D":
                        b("D", a.dayNamesShort, a.dayNames);
                        break;
                    case "o":
                        d = v("o");
                        break;
                    case "m":
                        c = v("m");
                        break;
                    case "M":
                        c = b("M", a.monthNamesShort, a.monthNames);
                        break;
                    case "y":
                        o = v("y");
                        break;
                    case "H":
                        h = v("H");
                        break;
                    case "h":
                        h = v("h");
                        break;
                    case "i":
                        r = v("i");
                        break;
                    case "s":
                        p = v("s");
                        break;
                    case "a":
                        f = b("a", ["am", "pm"], ["am", "pm"]) - 1;
                        break;
                    case "A":
                        f = b("A", ["am", "pm"], ["am", "pm"]) - 1;
                        break;
                    case "'":
                        g("'") ? y++ : m = !0;
                        break;
                    default:
                        y++
                }
                if (100 > o && (o += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (o <= ("string" != typeof l ? l : (new Date).getFullYear() % 100 + parseInt(l, 10)) ? 0 : -100)), d > -1)
                    for (c = 1, u = d;;) {
                        if (a = 32 - new Date(o, c - 1, 32).getDate(), a >= u) break;
                        c++, u -= a
                    }
                if (h = new Date(o, c - 1, u, -1 == f ? h : f && 12 > h ? h + 12 : f || 12 != h ? h : 0, r, p), h.getFullYear() != o || h.getMonth() + 1 != c || h.getDate() != u) throw "Invalid date";
            return h
        }
    }(jQuery),
    function(t) {
        var e = t.mobiscroll,
            n = {
                invalid: [],
                showInput: !0,
                inputClass: ""
            },
            i = function(e) {
                function i(e, n, i, r) {
                    for (var s = 0; n > s;) {
                        var a = t(".dwwl" + s, e),
                            l = o(r, s, i);
                        t.each(l, function(e, n) {
                            t('.dw-li[data-val="' + n + '"]', a).removeClass("dw-v")
                        }), s++
                    }
                }

                function o(t, e, n) {
                    for (var i, o = 0, r = []; e > o;) {
                        var s = t[o];
                        for (i in n)
                            if (n[i].key == s) {
                                n = n[i].children;
                                break
                            }
                        o++
                    }
                    for (o = 0; o < n.length;) n[o].invalid && r.push(n[o].key), o++;
                    return r
                }

                function r(t, e) {
                    for (var n = []; t;) n[--t] = !0;
                    return n[e] = !1, n
                }

                function s(t, e, n) {
                    var i, o, r = 0,
                        s = [],
                        l = y;
                    if (e)
                        for (i = 0; e > i; i++) s[i] = [{}];
                    for (; r < t.length;) {
                        i = s;
                        for (var e = r, c = l, u = {
                                keys: [],
                                values: [],
                                label: _[r]
                            }, d = 0; d < c.length;) u.values.push(c[d].value), u.keys.push(c[d].key), d++;
                        for (i[e] = [u], i = 0, e = void 0; i < l.length && void 0 === e;) l[i].key == t[r] && (void 0 !== n && n >= r || void 0 === n) && (e = i), i++;
                        if (void 0 !== e && l[e].children) r++, l = l[e].children;
                        else {
                            if (!(o = a(l)) || !o.children) break;
                            r++, l = o.children
                        }
                    }
                    return s
                }

                function a(t, e) {
                    if (!t) return !1;
                    for (var n, i = 0; i < t.length;)
                        if (!(n = t[i++]).invalid) return e ? i - 1 : n;
                    return !1
                }

                function l(e, n) {
                    t(".dwc", e).css("display", "").slice(n).hide()
                }

                function c(t, e) {
                    var n, i, o = [],
                        r = y,
                        s = 0,
                        l = !1;
                    if (void 0 !== t[s] && e >= s)
                        for (l = 0, n = t[s], i = void 0; l < r.length && void 0 === i;) r[l].key == t[s] && !r[l].invalid && (i = l), l++;
                    else i = a(r, !0), n = r[i].key;
                    for (l = void 0 !== i ? r[i].children : !1, o[s] = n; l;) {
                        if (r = r[i].children, s++, void 0 !== t[s] && e >= s)
                            for (l = 0, n = t[s], i = void 0; l < r.length && void 0 === i;) r[l].key == t[s] && !r[l].invalid && (i = l), l++;
                        else i = a(r, !0), i = !1 === i ? void 0 : i, n = r[i].key;
                        l = void 0 !== i && a(r[i].children) ? r[i].children : !1, o[s] = n
                    }
                    return {
                        lvl: s + 1,
                        nVector: o
                    }
                }

                function u(e) {
                    var n = [];
                    return g = g > v++ ? g : v, e.children("li").each(function(e) {
                        var i = t(this),
                            o = i.clone();
                        o.children("ul,ol").remove();
                        var o = o.html().replace(/^\s\s*/, "").replace(/\s\s*$/, ""),
                            r = i.data("invalid") ? !0 : !1,
                            e = {
                                key: i.data("val") || e,
                                value: o,
                                invalid: r,
                                children: null
                            },
                            i = i.children("ul,ol");
                        i.length && (e.children = u(i)), n.push(e)
                    }), v--, n
                }
                var d, h, p = t.extend({}, e.settings),
                    f = t.extend(e.settings, n, p),
                    p = t(this),
                    m = this.id + "_dummy",
                    g = 0,
                    v = 0,
                    b = {},
                    y = f.wheelArray || u(p),
                    _ = function(t) {
                        var e, n = [];
                        for (e = 0; t > e; e++) n[e] = f.labels && f.labels[e] ? f.labels[e] : e;
                        return n
                    }(g),
                    w = [],
                    x = function(t) {
                        for (var e, n = [], i = !0, o = 0; i;) e = a(t), n[o++] = e.key, (i = e.children) && (t = e.children);
                        return n
                    }(y),
                    x = s(x, g);
                return t("#" + m).remove(), f.showInput && (d = t('<input type="text" id="' + m + '" value="" class="' + f.inputClass + '" readonly />').insertBefore(p), e.settings.anchor = d, f.showOnFocus && d.focus(function() {
                    e.show()
                })), f.wheelArray || p.hide().closest(".ui-field-contain").trigger("create"), {
                    width: 50,
                    wheels: x,
                    headerText: !1,
                    onBeforeShow: function() {
                        var t = e.temp;
                        w = t.slice(0), e.settings.wheels = s(t, g, g), h = !0
                    },
                    onSelect: function(t) {
                        d && d.val(t)
                    },
                    onChange: function(t) {
                        d && "inline" == f.display && d.val(t)
                    },
                    onClose: function() {
                        d && d.blur()
                    },
                    onShow: function(e) {
                        t(".dwwl", e).bind("mousedown touchstart", function() {
                            clearTimeout(b[t(".dwwl", e).index(this)])
                        })
                    },
                    validate: function(t, n, o) {
                        var a = e.temp;
                        if (void 0 !== n && w[n] != a[n] || void 0 === n && !h) {
                            e.settings.wheels = s(a, null, n);
                            var u = [],
                                d = (n || 0) + 1,
                                p = c(a, n);
                            for (void 0 !== n && (e.temp = p.nVector.slice(0)); d < p.lvl;) u.push(d++);
                            if (l(t, p.lvl), w = e.temp.slice(0), u.length) return h = !0, e.settings.readonly = r(g, n), clearTimeout(b[n]), b[n] = setTimeout(function() {
                                e.changeWheel(u), e.settings.readonly = !1
                            }, 1e3 * o), !1;
                            i(t, p.lvl, y, e.temp)
                        } else p = c(a, a.length), i(t, p.lvl, y, a), l(t, p.lvl);
                        h = !1
                    }
                }
            };
        t.each(["list", "image", "treelist"], function(t, n) {
            e.presets[n] = i, e.presetShort(n)
        })
    }(jQuery),
    function(t) {
        var e;
        t.mobiscroll.themes.wp = {
            defaults: {
                width: 70,
                height: 76,
                accent: "none",
                dateOrder: "mmMMddDDyy",
                showLabel: !1,
                onAnimStart: function(n, i, o) {
                    t(".dwwl" + i, n).addClass("wpam"), clearTimeout(e[i]), e[i] = setTimeout(function() {
                        t(".dwwl" + i, n).removeClass("wpam")
                    }, 1e3 * o + 100)
                }
            },
            init: function(n, i) {
                var o, r;
                e = {}, t(".dw", n).addClass("wp-" + i.settings.accent), t(".dwwl", n).delegate(".dw-sel", "touchstart mousedown DOMMouseScroll mousewheel", function() {
                    o = !0, r = t(this).closest(".dwwl").hasClass("wpa"), t(".dwwl", n).removeClass("wpa"), t(this).closest(".dwwl").addClass("wpa")
                }).bind("touchmove mousemove", function() {
                    o = !1
                }).bind("touchend mouseup", function() {
                    o && r && t(this).closest(".dwwl").removeClass("wpa")
                })
            }
        }, t.mobiscroll.themes["wp light"] = t.mobiscroll.themes.wp
    }(jQuery),
    function(t) {
        t.mobiscroll.themes.tsmobi = {
            defaults: {},
            init: function(e, n) {
                t(".dwo", e).click(function() {
                    n.hide()
                })
            }
        }, t.mobiscroll.themes["tsmobi no-btns"] = t.mobiscroll.themes.tsmobi
    }(jQuery),
    function(t) {
        t.mobiscroll.themes.tstree = {
            defaults: {
                width: 70,
                height: 76
            },
            init: function(e, n) {
                t(".dwo", e).click(function() {
                    n.hide()
                })
            }
        }, t.mobiscroll.themes["tstree wp"] = t.mobiscroll.themes.tstree
    }(jQuery),
    function() {
        var t, e, n, i, o = [].indexOf || function(t) {
            for (var e = 0, n = this.length; n > e; e++)
                if (e in this && this[e] === t) return e;
            return -1
        };
        t = jQuery, t.fn.disableClientSideValidations = function() {
            return ClientSideValidations.disable(this), this
        }, t.fn.enableClientSideValidations = function() {
            return this.filter(ClientSideValidations.selectors.forms).each(function() {
                return ClientSideValidations.enablers.form(this)
            }), this.filter(ClientSideValidations.selectors.inputs).each(function() {
                return ClientSideValidations.enablers.input(this)
            }), this
        }, t.fn.resetClientSideValidations = function() {
            return this.filter(ClientSideValidations.selectors.forms).each(function() {
                return ClientSideValidations.reset(this)
            }), this
        }, t.fn.validate = function() {
            return this.filter(ClientSideValidations.selectors.forms).each(function() {
                return t(this).enableClientSideValidations()
            }), this
        }, t.fn.isValid = function(o) {
            var r;
            return r = t(this[0]), r.is("form") ? n(r, o) : "false" !== r.data("validate") ? e(r, i(this[0].name, o)) : void 0
        }, i = function(t, e) {
            var n, i, o;
            if (n = t.match(/\[(\w+_attributes)\].*\[(\w+)\]$/))
                for (o in e) i = e[o], o.match("\\[" + n[1] + "\\].*\\[\\]\\[" + n[2] + "\\]$") && (t = t.replace(/\[[\da-z_]+\]\[(\w+)\]$/g, "[][$1]"));
            return e[t] || {}
        }, n = function(e, n) {
            var i;
            return e.trigger("form:validate:before.ClientSideValidations"), i = !0, e.find(ClientSideValidations.selectors.validate_inputs).each(function() {
                return t(this).isValid(n) || (i = !1), !0
            }), e.trigger(i ? "form:validate:pass.ClientSideValidations" : "form:validate:fail.ClientSideValidations"), e.trigger("form:validate:after.ClientSideValidations"), i
        }, e = function(e, n) {
            var i, o, r, s, a, l, c;
            return e.trigger("element:validate:before.ClientSideValidations"), l = function() {
                return e.trigger("element:validate:pass.ClientSideValidations").data("valid", null)
            }, s = function(t) {
                return e.trigger("element:validate:fail.ClientSideValidations", t).data("valid", !1), !1
            }, i = function() {
                return e.trigger("element:validate:after.ClientSideValidations").data("valid") !== !1
            }, r = function(t) {
                var i, o, r, a, l, c, u, d;
                a = !0;
                for (o in t)
                    if (i = t[o], n[o]) {
                        for (d = n[o], c = 0, u = d.length; u > c; c++)
                            if (l = d[c], r = i.call(t, e, l)) {
                                a = s(r);
                                break
                            }
                        if (!a) break
                    }
                return a
            }, o = e.attr("name").replace(/\[([^\]]*?)\]$/, "[_destroy]"), "1" === t("input[name='" + o + "']").val() ? (l(), i()) : e.data("changed") === !1 ? i() : (e.data("changed", !1), a = ClientSideValidations.validators.local, c = ClientSideValidations.validators.remote, r(a) && r(c) && l(), i())
        }, void 0 === window.ClientSideValidations && (window.ClientSideValidations = {}), void 0 === window.ClientSideValidations.forms && (window.ClientSideValidations.forms = {}), window.ClientSideValidations.selectors = {
            inputs: ':input:not(button):not([type="submit"])[name]:visible:enabled',
            validate_inputs: ":input:enabled:visible[data-validate]",
            forms: "form[data-validate]"
        }, window.ClientSideValidations.reset = function(e) {
            var n, i;
            n = t(e), ClientSideValidations.disable(e);
            for (i in e.ClientSideValidations.settings.validators) e.ClientSideValidations.removeError(n.find("[name='" + i + "']"));
            return ClientSideValidations.enablers.form(e)
        }, window.ClientSideValidations.disable = function(e) {
            var n;
            return n = t(e), n.off(".ClientSideValidations"), n.is("form") ? ClientSideValidations.disable(n.find(":input")) : (n.removeData("valid"), n.removeData("changed"), n.filter(":input").each(function() {
                return t(this).removeAttr("data-validate")
            }))
        }, window.ClientSideValidations.enablers = {
            form: function(e) {
                var n, i, o, r;
                n = t(e), e.ClientSideValidations = {
                    settings: window.ClientSideValidations.forms[n.attr("id")],
                    addError: function(t, n) {
                        return ClientSideValidations.formBuilders[e.ClientSideValidations.settings.type].add(t, e.ClientSideValidations.settings, n)
                    },
                    removeError: function(t) {
                        return ClientSideValidations.formBuilders[e.ClientSideValidations.settings.type].remove(t, e.ClientSideValidations.settings)
                    }
                }, r = {
                    "submit.ClientSideValidations": function(t) {
                        return n.isValid(e.ClientSideValidations.settings.validators) ? void 0 : (t.preventDefault(), t.stopImmediatePropagation())
                    },
                    "ajax:beforeSend.ClientSideValidations": function(t) {
                        return t.target === this ? n.isValid(e.ClientSideValidations.settings.validators) : void 0
                    },
                    "form:validate:after.ClientSideValidations": function(t) {
                        return ClientSideValidations.callbacks.form.after(n, t)
                    },
                    "form:validate:before.ClientSideValidations": function(t) {
                        return ClientSideValidations.callbacks.form.before(n, t)
                    },
                    "form:validate:fail.ClientSideValidations": function(t) {
                        return ClientSideValidations.callbacks.form.fail(n, t)
                    },
                    "form:validate:pass.ClientSideValidations": function(t) {
                        return ClientSideValidations.callbacks.form.pass(n, t)
                    }
                };
                for (o in r) i = r[o], n.on(o, i);
                return n.find(ClientSideValidations.selectors.inputs).each(function() {
                    return ClientSideValidations.enablers.input(this)
                })
            },
            input: function(e) {
                var n, i, o, r, s, a;
                i = t(e), s = e.form, n = t(s), a = {
                    "focusout.ClientSideValidations": function() {
                        //return t(this).isValid(s.ClientSideValidations.settings.validators)
                    },
                    "change.ClientSideValidations": function() {
                        return t(this).data("changed", !0)
                    },
                    "element:validate:after.ClientSideValidations": function(e) {
                        return ClientSideValidations.callbacks.element.after(t(this), e)
                    },
                    "element:validate:before.ClientSideValidations": function(e) {
                        return ClientSideValidations.callbacks.element.before(t(this), e)
                    },
                    "element:validate:fail.ClientSideValidations": function(e, n) {
                        var i;
                        return i = t(this), ClientSideValidations.callbacks.element.fail(i, n, function() {
                            return s.ClientSideValidations.addError(i, n)
                        }, e)
                    },
                    "element:validate:pass.ClientSideValidations": function(e) {
                        var n;
                        return n = t(this), ClientSideValidations.callbacks.element.pass(n, function() {
                            return s.ClientSideValidations.removeError(n)
                        }, e)
                    }
                };
                for (r in a) o = a[r], i.filter(':not(:radio):not([id$=_confirmation]):not([data-validate="false"])').each(function() {
                    return t(this).attr("data-validate", !0)
                }).on(r, o);
                return i.filter(":checkbox").on("click.ClientSideValidations", function() {
                    return t(this).isValid(s.ClientSideValidations.settings.validators), !0
                }), i.filter("[id$=_confirmation]").each(function() {
                    var e, i, a, l;
                    if (e = t(this), i = n.find("#" + this.id.match(/(.+)_confirmation/)[1] + ":input"), i[0]) {
                        a = {
                            "focusout.ClientSideValidations": function() {
                                return i.data("changed", !0).isValid(s.ClientSideValidations.settings.validators)
                            },
                            "keyup.ClientSideValidations": function() {
                                return i.data("changed", !0).isValid(s.ClientSideValidations.settings.validators)
                            }
                        }, l = [];
                        for (r in a) o = a[r], l.push(t("#" + e.attr("id")).on(r, o));
                        return l
                    }
                })
            }
        }, window.ClientSideValidations.validators = {
            all: function() {
                return jQuery.extend({}, ClientSideValidations.validators.local, ClientSideValidations.validators.remote)
            },
            local: {
                presence: function(t, e) {
                    return /^\s*$/.test(t.val() || "") ? e.message : void 0
                },
                acceptance: function(t, e) {
                    var n;
                    switch (t.attr("type")) {
                        case "checkbox":
                            if (!t.prop("checked")) return e.message;
                            break;
                        case "text":
                            if (t.val() !== ((null != (n = e.accept) ? n.toString() : void 0) || "1")) return e.message
                    }
                },
                format: function(t, e) {
                    var n;
                    if (n = this.presence(t, e)) {
                        if (e.allow_blank === !0) return;
                        return n
                    }
                    return e["with"] && !e["with"].test(t.val()) ? e.message : e.without && e.without.test(t.val()) ? e.message : void 0
                },
                numericality: function(e, n) {
                    var i, o, r, s, a, l, c;
                    if (c = jQuery.trim(e.val()), !ClientSideValidations.patterns.numericality.test(c)) {
                        if (n.allow_blank === !0 && this.presence(e, {
                                message: n.messages.numericality
                            })) return;
                        return n.messages.numericality
                    }
                    if (c = c.replace(new RegExp("\\" + ClientSideValidations.number_format.delimiter, "g"), "").replace(new RegExp("\\" + ClientSideValidations.number_format.separator, "g"), "."), n.only_integer && !/^[+-]?\d+$/.test(c)) return n.messages.only_integer;
                    i = {
                        greater_than: ">",
                        greater_than_or_equal_to: ">=",
                        equal_to: "==",
                        less_than: "<",
                        less_than_or_equal_to: "<="
                    }, a = t(e[0].form);
                    for (o in i)
                        if (l = i[o], null != n[o]) {
                            if (!isNaN(parseFloat(n[o])) && isFinite(n[o])) r = n[o];
                            else {
                                if (1 !== a.find("[name*=" + n[o] + "]").size()) return;
                                r = a.find("[name*=" + n[o] + "]").val()
                            }
                            if (s = new Function("return " + c + " " + l + " " + r), !s()) return n.messages[o]
                        }
                    return !n.odd || parseInt(c, 10) % 2 ? n.even && parseInt(c, 10) % 2 ? n.messages.even : void 0 : n.messages.odd
                },
                length: function(t, e) {
                    var n, i, o, r, s, a, l, c;
                    if (c = e.js_tokenizer || "split('')", l = new Function("element", "return (element.val()." + c + " || '').length")(t), n = {
                            is: "==",
                            minimum: ">=",
                            maximum: "<="
                        }, i = {}, i.message = e.is ? e.messages.is : e.minimum ? e.messages.minimum : void 0, s = this.presence(t, i)) {
                        if (e.allow_blank === !0) return;
                        return s
                    }
                    for (o in n)
                        if (a = n[o], e[o] && (r = new Function("return " + l + " " + a + " " + e[o]), !r())) return e.messages[o]
                },
                exclusion: function(t, e) {
                    var n, i, r, s, a;
                    if (i = this.presence(t, e)) {
                        if (e.allow_blank === !0) return;
                        return i
                    }
                    return e["in"] && (a = t.val(), o.call(function() {
                        var t, n, i, o;
                        for (i = e["in"], o = [], t = 0, n = i.length; n > t; t++) r = i[t], o.push(r.toString());
                        return o
                    }(), a) >= 0) ? e.message : e.range && (n = e.range[0], s = e.range[1], t.val() >= n && t.val() <= s) ? e.message : void 0
                },
                inclusion: function(t, e) {
                    var n, i, r, s, a;
                    if (i = this.presence(t, e)) {
                        if (e.allow_blank === !0) return;
                        return i
                    }
                    if (e["in"]) {
                        if (a = t.val(), o.call(function() {
                                var t, n, i, o;
                                for (i = e["in"], o = [], t = 0, n = i.length; n > t; t++) r = i[t], o.push(r.toString());
                                return o
                            }(), a) >= 0) return;
                        return e.message
                    }
                    if (e.range) {
                        if (n = e.range[0], s = e.range[1], t.val() >= n && t.val() <= s) return;
                        return e.message
                    }
                },
                confirmation: function(t, e) {
                    return t.val() !== jQuery("#" + t.attr("id") + "_confirmation").val() ? e.message : void 0
                },
                uniqueness: function(e, n) {
                    var i, o, r, s, a, l, c;
                    return r = e.attr("name"), /_attributes\]\[\d/.test(r) && (o = r.match(/^(.+_attributes\])\[\d+\](.+)$/), s = o[1], a = o[2], c = e.val(), s && a && (i = e.closest("form"), l = !0, i.find(':input[name^="' + s + '"][name$="' + a + '"]').each(function() {
                        if (t(this).attr("name") !== r) {
                            if (t(this).val() === c) return l = !1, t(this).data("notLocallyUnique", !0);
                            if (t(this).data("notLocallyUnique")) return t(this).removeData("notLocallyUnique").data("changed", !0)
                        }
                    }), !l)) ? n.message : void 0
                }
            },
            remote: {
                uniqueness: function(t, e) {
                    var n, i, o, r, s, a, l, c;
                    if (o = ClientSideValidations.validators.local.presence(t, e)) {
                        if (e.allow_blank === !0) return;
                        return o
                    }
                    if (n = {}, n.case_sensitive = !!e.case_sensitive, e.id && (n.id = e.id), e.scope) {
                        n.scope = {}, c = e.scope;
                        for (i in c) s = c[i], l = t.attr("name").replace(/\[\w+\]$/, "[" + i + "]"), a = jQuery("[name='" + l + "']"), jQuery("[name='" + l + "']:checkbox").each(function() {
                            return this.checked ? a = this : void 0
                        }), a[0] && a.val() !== s ? (n.scope[i] = a.val(), a.unbind("change." + t.id).bind("change." + t.id, function() {
                            return t.trigger("change.ClientSideValidations"), t.trigger("focusout.ClientSideValidations")
                        })) : n.scope[i] = s
                    }
                    return /_attributes\]/.test(t.attr("name")) ? (r = t.attr("name").match(/\[\w+_attributes\]/g).pop().match(/\[(\w+)_attributes\]/).pop(), r += /(\[\w+\])$/.exec(t.attr("name"))[1]) : r = t.attr("name"), e["class"] && (r = e["class"] + "[" + r.split("[")[1]), n[r] = t.val(), 200 === jQuery.ajax({
                        url: ClientSideValidations.remote_validators_url_for("uniqueness"),
                        data: n,
                        async: !1,
                        cache: !1
                    }).status ? e.message : void 0
                }
            }
        }, window.ClientSideValidations.remote_validators_url_for = function(t) {
            return null != ClientSideValidations.remote_validators_prefix ? "//" + window.location.host + "/" + ClientSideValidations.remote_validators_prefix + "/validators/" + t : "//" + window.location.host + "/validators/" + t
        }, window.ClientSideValidations.disableValidators = function() {
            var t, e, n, i;
            if (void 0 !== window.ClientSideValidations.disabled_validators) {
                n = window.ClientSideValidations.validators.remote, i = [];
                for (e in n) t = n[e], i.push(o.call(window.ClientSideValidations.disabled_validators, e) >= 0 ? delete window.ClientSideValidations.validators.remote[e] : void 0);
                return i
            }
        }, window.ClientSideValidations.formBuilders = {
            "ActionView::Helpers::FormBuilder": {
                add: function(e, n, i) {
                    var o, r, s, a;
                    return o = t(e[0].form), e.data("valid") !== !1 && null == o.find("label.message[for='" + e.attr("id") + "']")[0] && (r = jQuery(n.input_tag), a = jQuery(n.label_tag), s = o.find("label[for='" + e.attr("id") + "']:not(.message)"), e.attr("autofocus") && e.attr("autofocus", !1), e.before(r), r.find("span#input_tag").replaceWith(e), r.find("label.message").attr("for", e.attr("id")), a.find("label.message").attr("for", e.attr("id")), a.insertAfter(s), a.find("label#label_tag").replaceWith(s)), o.find("label.message[for='" + e.attr("id") + "']").text(i)
                },
                remove: function(e, n) {
                    var i, o, r, s, a;
                    return o = t(e[0].form), i = jQuery(n.input_tag).attr("class"), r = e.closest("." + i.replace(" ", ".")), s = o.find("label[for='" + e.attr("id") + "']:not(.message)"), a = s.closest("." + i), r[0] ? (r.find("#" + e.attr("id")).detach(), r.replaceWith(e), s.detach(), a.replaceWith(s)) : void 0
                }
            }
        }, window.ClientSideValidations.patterns = {
            numericality: /^(-|\+)?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d*)?$/
        }, window.ClientSideValidations.callbacks = {
            element: {
                after: function() {},
                before: function() {},
                fail: function(t, e, n) {
                    return n()
                },
                pass: function(t, e) {
                    return e()
                }
            },
            form: {
                after: function() {},
                before: function() {},
                fail: function() {},
                pass: function() {}
            }
        }, t(function() {
            return ClientSideValidations.disableValidators(), t(ClientSideValidations.selectors.forms).validate()
        })
    }.call(this),
    /*
     Based on https://github.com/dockyard/client_side_validations-simple_form
     Copyright (c) 2012 DockYard, LLC, MIT license
    */
    function() {
        ClientSideValidations.formBuilders["SimpleForm::FormBuilder"] = {
            add: function(t, e, n) {
                this.remove(t, e);
                var i, o, r;
                return i = t.parent().find("" + e.error_tag + "." + e.error_class), null == i[0] && (r = t.closest("" + e.wrapper_tag + "." + e.wrapper_class), i = $("<" + e.error_tag + "/>", {
                    "class": e.error_class,
                    text: n
                }), r.find(".controls").append(i)), o = t.closest("." + e.wrapper_class), o.addClass(e.wrapper_error_class), i.text(n)
            },
            remove: function(t, e) {
                var n, i, o;
                return i = t.closest("." + e.wrapper_class + "." + e.wrapper_error_class), o = t.closest("" + e.wrapper_tag + "." + e.wrapper_class), i.removeClass(e.wrapper_error_class), n = o.find("" + e.error_tag + "." + e.error_class), n.remove()
            }
        }
    }.call(this), ClientSideValidations.formBuilders["NestedForm::SimpleBuilder"] = ClientSideValidations.formBuilders["SimpleForm::FormBuilder"], $(function() {
        $(document).on("nested:fieldAdded", "form", function(t) {
            $(t.target).find(":input").enableClientSideValidations()
        })
    }),
    function(t) {
        "use strict";
        var e = Array.prototype.slice,
            n = function(t) {
                return ("0" + t.toString()).substr(-2)
            },
            i = {
                day_names: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                abbr_day_names: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                month_names: [null, "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                abbr_month_names: [null, "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            },
            o = {
                precision: 3,
                separator: ".",
                delimiter: ",",
                strip_insignificant_zeros: !1
            },
            r = {
                unit: "$",
                precision: 2,
                format: "%u%n",
                delimiter: ",",
                separator: "."
            },
            s = {
                precision: 3,
                separator: ".",
                delimiter: ""
            },
            a = [null, "kb", "mb", "gb", "tb"],
            l = ["AM", "PM"];
        t.reset = function() {
            this.defaultLocale = "en", this.locale = "en", this.defaultSeparator = ".", this.placeholder = /(?:\{\{|%\{)(.*?)(?:\}\}?)/gm, this.fallbacks = !1, this.translations = {}
        }, t.locales = {}, t.locales.get = function(e) {
            var n = this[e] || this[t.locale] || this["default"];
            return "function" == typeof n && (n = n(e)), n instanceof Array == !1 && (n = [n]), n
        }, t.locales["default"] = function(e) {
            var n, i = [],
                o = [];
            return e && i.push(e), !e && t.locale && i.push(t.locale), t.fallbacks && t.defaultLocale && i.push(t.defaultLocale), i.forEach(function(e) {
                n = e.split("-")[0], ~o.indexOf(e) || o.push(e), t.fallbacks && n && n !== e && !~o.indexOf(n) && o.push(n)
            }), i.length || i.push("en"), o
        }, t.pluralization = {}, t.pluralization.get = function(e) {
            return this[e] || this[t.locale] || this["default"]
        }, t.pluralization["default"] = function(t) {
            switch (t) {
                case 0:
                    return ["zero", "other"];
                case 1:
                    return ["one"];
                default:
                    return ["other"]
            }
        }, t.reset(), t.currentLocale = function() {
            return this.locale || this.defaultLocale
        }, t.isSet = function(t) {
            return void 0 !== t && null !== t
        }, t.lookup = function(t, e) {
            e = this.prepareOptions(e);
            var n, i, o, r = this.locales.get(e.locale);
            for (r[0]; r.length;)
                if (n = r.shift(), i = t.split(this.defaultSeparator), o = this.translations[n]) {
                    for (; i.length && (o = o[i.shift()], void 0 !== o && null !== o););
                    if (void 0 !== o && null !== o) return o
                }
            return this.isSet(e.defaultValue) ? e.defaultValue : void 0
        }, t.prepareOptions = function() {
            for (var t, n = e.call(arguments), i = {}; n.length;)
                if (t = n.shift(), "object" == typeof t)
                    for (var o in t) t.hasOwnProperty(o) && (this.isSet(i[o]) || (i[o] = t[o]));
            return i
        }, t.translate = function(t, e) {
            e = this.prepareOptions(e);
            var n = this.lookup(t, e);
            return void 0 === n || null === n ? this.missingTranslation(t) : ("string" == typeof n ? n = this.interpolate(n, e) : n instanceof Object && this.isSet(e.count) && (n = this.pluralize(e.count, n, e)), n)
        }, t.interpolate = function(t, e) {
            e = this.prepareOptions(e);
            var n, i, o, r, s = t.match(this.placeholder);
            if (!s) return t;
            for (; s.length;) n = s.shift(), o = n.replace(this.placeholder, "$1"), i = e[o], this.isSet(e[o]) || (i = "[missing " + n + " value]"), r = new RegExp(n.replace(/\{/gm, "\\{").replace(/\}/gm, "\\}")), t = t.replace(r, i);
            return t
        }, t.pluralize = function(t, e, n) {
            n = this.prepareOptions(n);
            var i, o, r, s, a;
            if (i = e instanceof Object ? e : this.lookup(e, n), !i) return this.missingTranslation(e);
            for (o = this.pluralization.get(n.locale), r = o(Math.abs(t)); r.length;)
                if (s = r.shift(), this.isSet(i[s])) {
                    a = i[s];
                    break
                }
            return n.count = String(t), this.interpolate(a, n)
        }, t.missingTranslation = function() {
            var t = '[missing "';
            return t += this.currentLocale() + ".", t += e.call(arguments).join("."), t += '" translation]'
        }, t.toNumber = function(t, e) {
            e = this.prepareOptions(e, this.lookup("number.format"), o);
            var n, i, r = 0 > t,
                s = Math.abs(t).toFixed(e.precision).toString(),
                a = s.split("."),
                l = [];
            for (t = a[0], n = a[1]; t.length > 0;) l.unshift(t.substr(Math.max(0, t.length - 3), 3)), t = t.substr(0, t.length - 3);
            return i = l.join(e.delimiter), e.strip_insignificant_zeros && n && (n = n.replace(/0+$/, "")), e.precision > 0 && n && (i += e.separator + n), r && (i = "-" + i), i
        }, t.toCurrency = function(t, e) {
            return e = this.prepareOptions(e, this.lookup("number.currency.format"), this.lookup("number.format"), r), t = this.toNumber(t, e), t = e.format.replace("%u", e.unit).replace("%n", t)
        }, t.localize = function(t, e) {
            switch (t) {
                case "currency":
                    return this.toCurrency(e);
                case "number":
                    return t = this.lookup("number.format"), this.toNumber(e, t);
                case "percentage":
                    return this.toPercentage(e);
                default:
                    return t.match(/^(date|time)/) ? this.toTime(t, e) : e.toString()
            }
        }, t.parseDate = function(t) {
            var e, n;
            if ("object" == typeof t) return t;
            if (e = t.toString().match(/(\d{4})-(\d{2})-(\d{2})(?:[ T](\d{2}):(\d{2}):(\d{2}))?(Z|\+0000)?/)) {
                for (var i = 1; 6 >= i; i++) e[i] = parseInt(e[i], 10) || 0;
                e[2] -= 1, n = e[7] ? new Date(Date.UTC(e[1], e[2], e[3], e[4], e[5], e[6])) : new Date(e[1], e[2], e[3], e[4], e[5], e[6])
            } else "number" == typeof t ? (n = new Date, n.setTime(t)) : t.match(/\d+ \d+:\d+:\d+ [+-]\d+ \d+/) ? (n = new Date, n.setTime(Date.parse(t))) : (n = new Date, n.setTime(Date.parse(t)));
            return n
        }, t.strftime = function(t, e) {
            var o = this.lookup("date");
            o || (o = i), o.meridian || (o.meridian = l);
            var r = t.getDay(),
                s = t.getDate(),
                a = t.getFullYear(),
                c = t.getMonth() + 1,
                u = t.getHours(),
                d = u,
                h = u > 11 ? 1 : 0,
                p = t.getSeconds(),
                f = t.getMinutes(),
                m = t.getTimezoneOffset(),
                g = Math.floor(Math.abs(m / 60)),
                v = Math.abs(m) - 60 * g,
                b = (m > 0 ? "-" : "+") + (g.toString().length < 2 ? "0" + g : g) + (v.toString().length < 2 ? "0" + v : v);
            return d > 12 ? d -= 12 : 0 === d && (d = 12), e = e.replace("%a", o.abbr_day_names[r]), e = e.replace("%A", o.day_names[r]), e = e.replace("%b", o.month_names[c]), e = e.replace("%B", o.month_names[c]), e = e.replace("%d", n(s)), e = e.replace("%e", s), e = e.replace("%-d", s), e = e.replace("%H", n(u)), e = e.replace("%-H", u), e = e.replace("%I", n(d)), e = e.replace("%-I", d), e = e.replace("%m", n(c)), e = e.replace("%-m", c), e = e.replace("%M", n(f)), e = e.replace("%-M", f), e = e.replace("%p", o.meridian[h]), e = e.replace("%S", n(p)), e = e.replace("%-S", p), e = e.replace("%w", r), e = e.replace("%y", n(a)), e = e.replace("%-y", n(a).replace(/^0+/, "")), e = e.replace("%Y", a), e = e.replace("%z", b)
        }, t.toTime = function(t, e) {
            var n = this.parseDate(e),
                i = this.lookup(t);
            return n.toString().match(/invalid/i) ? n.toString() : i ? this.strftime(n, i) : n.toString()
        }, t.toPercentage = function(t, e) {
            return e = this.prepareOptions(e, this.lookup("number.percentage.format"), this.lookup("number.format"), s), t = this.toNumber(t, e), t + "%"
        }, t.toHumanSize = function(t, e) {
            for (var n, i, o = 1024, r = t, s = 0; r >= o && 4 > s;) r /= o, s += 1;
            return 0 === s ? (n = this.t("number.human.storage_units.units.byte", {
                count: r
            }), i = 0) : (n = this.t("number.human.storage_units.units." + a[s]), i = r - Math.floor(r) === 0 ? 0 : 1), e = this.prepareOptions(e, {
                precision: i,
                format: "%n%u",
                delimiter: ""
            }), t = this.toNumber(r, e), t = e.format.replace("%u", n).replace("%n", t)
        }, t.t = t.translate, t.l = t.localize, t.p = t.pluralize
    }("undefined" == typeof exports ? this.I18n = {} : exports), I18n.asian_locale = function() {
        return $.inArray(I18n.locale, ["ja", "ko", "zh-CN", "zh-TW"]) >= 0
    }, I18n.translations || (I18n.translations = {}), I18n.translations.en = {
        date: {
            abbr_day_names: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            month_names: [null, "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            abbr_month_names: [null, "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        },
        datetime: {
            prompts: {
                year: "Year",
                month: "Month",
                day: "Day",
                hour: "Hour",
                minute: "Minute"
            }
        },
        errors: {
            app_error: "Application Error"
        },
        keywords: {
            actions: "Actions",
            sentence_delimiter: ". "
        },
        actions: {
            set: "Set",
            clear: "Clear",
            remove: "Remove",
            show_all: "Show All",
            hide_all: "Hide All",
            no_models_matched: "No %{models} matched",
            no_models_exist: "No %{models} have been created.",
            upgrade_to_ts_full: "Please upgrade to TS Full Version",
            confirmation: "Are you sure?",
            cannot_undo: "You cannot undo this action.",
            may_incur_fee: "This action may incur a charge.",
            additional_results: {
                one: "%{count} additional result found",
                other: "%{count} additional results found"
            },
            hide: "Hide",
            show: "Show",
            buy: "Buy",
            invalid_date_range: "Invalid date range"
        },
        beauty_solution: {
            staff: {
                staff_not_available: "Staff not available"
            }
        },
        mongoid: {
            models: {
                customer: {
                    one: "Customer",
                    other: "Customers"
                },
                menu_item: {
                    one: "Menu Item",
                    other: "Menu Items"
                },
                table: {
                    one: "Table",
                    other: "Tables"
                }
            },
            errors: {
                models: {
                    table_combo: {
                        max_recursive_tables_exceeded: "Cannot apply 'Combine All' to more than 8 Tables at once."
                    }
                }
            }
        },
        active_admin: {
            filter: "Filter"
        },
        table_solution: {
            hq_view: {
                all_shops: "All Shops"
            }
        },
        customers: {
            query: {
                filter_active: "Filtering\u2026"
            }
        },
        reservations: {
            table_capacity_exceeded: "Reservation guests (%{people}) exceeds table capacity (%{capacity})"
        },
        sections: {
            uncheck_to_combine: "Uncheck above to manually specify Table Combos"
        }
    }, I18n.translations.ja = {
        date: {
            abbr_day_names: ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"],
            month_names: [null, "1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
            abbr_month_names: [null, "1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"]
        },
        datetime: {
            prompts: {
                year: "\u5e74",
                month: "\u6708",
                day: "\u65e5",
                hour: "\u6642",
                minute: "\u5206"
            }
        },
        errors: {
            app_error: "\u30b7\u30b9\u30c6\u30e0\u30a8\u30e9\u30fc"
        },
        keywords: {
            actions: "\u64cd\u4f5c",
            sentence_delimiter: "\u3002"
        },
        actions: {
            set: "\u30bb\u30c3\u30c8",
            clear: "\u30af\u30ea\u30a2",
            remove: "\u524a\u9664\u3059\u308b",
            show_all: "\u5168\u3066\u8868\u793a",
            hide_all: "\u5168\u3066\u96a0\u3059",
            no_models_matched: "\u8a72\u5f53\u3059\u308b%{models}\u304c\u3042\u308a\u307e\u305b\u3093",
            no_models_exist: "\u307e\u3060%{models}\u304c\u4f5c\u6210\u3055\u308c\u3066\u3044\u307e\u305b\u3093\u3002",
            upgrade_to_ts_full: "TS\u30d5\u30eb\u6a5f\u80fd\u7248\u306b\u30a2\u30c3\u30d7\u30b0\u30ec\u30fc\u30c9\u3057\u3066\u4e0b\u3055\u3044",
            confirmation: "\u672c\u5f53\u306b\u3088\u308d\u3057\u3044\u3067\u3059\u304b\uff1f",
            cannot_undo: "\u3053\u306e\u64cd\u4f5c\u306f\u53d6\u6d88\u3067\u304d\u307e\u305b\u3093\u3002",
            may_incur_fee: "\u8cbb\u7528\u304c\u304b\u304b\u308a\u307e\u3059\u3002",
            additional_results: "\u305d\u306e\u4ed6\u306e\u691c\u7d22\u7d50\u679c\u304c%{count}\u4ef6\u3042\u308a\u307e\u3059",
            hide: "\u96a0\u3059",
            show: "\u8a73\u7d30",
            buy: "\u8cb7\u3046",
            invalid_date_range: "\u6307\u5b9a\u3057\u305f\u65e5\u4ed8\u7bc4\u56f2\u304c\u6709\u52b9\u3067\u306f\u3042\u308a\u307e\u305b\u3093"
        },
        beauty_solution: {
            staff: {
                staff_not_available: "\u30b9\u30bf\u30c3\u30d5\u4f11\u307f"
            }
        },
        mongoid: {
            models: {
                customer: "\u9867\u5ba2",
                menu_item: "\u500b\u5225\u30e1\u30cb\u30e5\u30fc",
                table: "\u30c6\u30fc\u30d6\u30eb"
            },
            errors: {
                models: {
                    table_combo: {
                        max_recursive_tables_exceeded: "\u300c\u5168\u3066\u3092\u63a5\u7d9a\u300d\u306f\u30018\u30c6\u30fc\u30d6\u30eb\u4ee5\u4e0a\u3092\u7d44\u307f\u5408\u308f\u305b\u308b\u3053\u3068\u306f\u3067\u304d\u307e\u305b\u3093\u3002"
                    }
                }
            }
        },
        active_admin: {
            filter: "\u7d5e\u308a\u8fbc\u3080"
        },
        table_solution: {
            hq_view: {
                all_shops: "\u5168\u5e97\u8217"
            }
        },
        customers: {
            query: {
                filter_active: "\u7d5e\u308a\u8fbc\u307f\u4e2d\u2026"
            }
        },
        reservations: {
            table_capacity_exceeded: "\u4e88\u7d04\u4eba\u6570 (%{people}) \u304c\u30c6\u30fc\u30d6\u30eb\u306e\u4e0a\u9650\u4eba\u6570 (%{capacity}) \u3092\u8d85\u3048\u3066\u3044\u307e\u3059"
        },
        sections: {
            uncheck_to_combine: "\u624b\u52d5\u3067\u63a5\u7d9a\u3092\u6307\u5b9a\u3059\u308b\u5834\u5408\u306f\u3001\u4e0a\u8a18\u306e\u30c1\u30a7\u30c3\u30af\u3092\u5916\u3057\u3066\u304f\u3060\u3055\u3044\u3002"
        }
    }, I18n.translations.ko = {
        date: {
            abbr_day_names: ["\uc77c", "\uc6d4", "\ud654", "\uc218", "\ubaa9", "\uae08", "\ud1a0"],
            month_names: [null, "1\uc6d4", "2\uc6d4", "3\uc6d4", "4\uc6d4", "5\uc6d4", "6\uc6d4", "7\uc6d4", "8\uc6d4", "9\uc6d4", "10\uc6d4", "11\uc6d4", "12\uc6d4"],
            abbr_month_names: [null, "1\uc6d4", "2\uc6d4", "3\uc6d4", "4\uc6d4", "5\uc6d4", "6\uc6d4", "7\uc6d4", "8\uc6d4", "9\uc6d4", "10\uc6d4", "11\uc6d4", "12\uc6d4"]
        },
        datetime: {
            prompts: {
                year: "\ub144",
                month: "\uc6d4",
                day: "\uc77c",
                hour: "\uc2dc",
                minute: "\ubd84"
            }
        },
        errors: {
            app_error: "\uc2dc\uc2a4\ud15c \uc624\ub958"
        },
        keywords: {
            actions: "\uc870\uc791",
            sentence_delimiter: "\u3002"
        },
        actions: {
            set: "\uc124\uc815",
            clear: "\uc9c0\uc6b0\uae30",
            remove: "\uc0ad\uc81c",
            show_all: "\ubaa8\ub450 \ud45c\uc2dc",
            hide_all: "\uc228\uae30\uae30",
            no_models_matched: "\ud574\ub2f9 %{models}\uc774 \uc5c6\uc2b5\ub2c8\ub2e4",
            no_models_exist: "\uc544\uc9c1 %{models}\uac00 \uc791\uc131\ub418\uc9c0 \uc54a\uc2b5\ub2c8\ub2e4.",
            upgrade_to_ts_full: "TS \ud480 \uae30\ub2a5 \ubc84\uc804\uc73c\ub85c \uc5c5\uadf8\ub808\uc774\ub4dc\ud558\uc2ed\uc2dc\uc624.",
            confirmation: "\uc0ad\uc81c \ud558\uc2dc\uaca0\uc2b5\ub2c8\uae4c?",
            cannot_undo: "\uc774 \uc791\uc5c5\uc740 \ucde8\uc18c \ud560 \uc218 \uc5c6\uc2b5\ub2c8\ub2e4.",
            may_incur_fee: "\uc694\uae08\uc774 \ubd80\uacfc \ub420 \uc218 \uc788\uc2b5\ub2c8\ub2e4.",
            additional_results: "\ub2e4\ub978 \uac80\uc0c9 \uacb0\uacfc\uac00 %{count} \uac74 \uc788\uc2b5\ub2c8\ub2e4",
            hide: "\uc228\uae30\uae30",
            show: "\uc790\uc138\ud55c",
            buy: "\uc0ac\uae30",
            invalid_date_range: "\uc9c0\uc815\ub41c \ub0a0\uc9dc \ubc94\uc704\uac00 \uc720\ud6a8\ud558\uc9c0 \uc54a\uc2b5\ub2c8\ub2e4"
        },
        beauty_solution: {
            staff: {
                staff_not_available: "\ud734\uac00 \uc9c1\uc6d0"
            }
        },
        mongoid: {
            models: {
                customer: "\uace0\uac1d",
                menu_item: "\uac1c\ubcc4 \uba54\ub274",
                table: "\ud14c\uc774\ube14"
            },
            errors: {
                models: {
                    table_combo: null
                }
            }
        },
        active_admin: null,
        table_solution: {
            hq_view: {
                all_shops: "\ubaa8\ub4e0 \uc0c1\uc810"
            }
        },
        customers: {
            query: {
                filter_active: "\uac80\uc0c9 \uc911 ..."
            }
        },
        reservations: {
            table_capacity_exceeded: "\uc608\uc57d \uc778\uc6d0 (%{people})\uc774 \ud14c\uc774\ube14\uc758 \ucd5c\ub300 \uc778\uc6d0\uc218 (%{capacity})\ub97c \ucd08\uacfc\ud569\ub2c8\ub2e4"
        },
        sections: {
            uncheck_to_combine: "\uc218\ub3d9\uc73c\ub85c \ud14c\uc774\ube14 \uc5f0\uacb0\uc744 \uc9c0\uc815\ud558\ub824\uba74 \uc704\uc758 \ud655\uc778\ub780\uc744 \uc120\ud0dd \ucde8\uc18c."
        }
    }, I18n.translations["zh-CN"] = {
        date: {
            abbr_day_names: ["\u65e5", "\u4e00", "\u4e8c", "\u4e09", "\u56db", "\u4e94", "\u516d"],
            month_names: [null, "\u4e00\u6708", "\u4e8c\u6708", "\u4e09\u6708", "\u56db\u6708", "\u4e94\u6708", "\u516d\u6708", "\u4e03\u6708", "\u516b\u6708", "\u4e5d\u6708", "\u5341\u6708", "\u5341\u4e00\u6708", "\u5341\u4e8c\u6708"],
            abbr_month_names: [null, "1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"]
        },
        datetime: {
            prompts: {
                year: "\u5e74",
                month: "\u6708",
                day: "\u65e5",
                hour: "\u65f6",
                minute: "\u5206"
            }
        },
        errors: {
            app_error: "\u7cfb\u7edf\u9519\u8bef"
        },
        keywords: {
            actions: "\u64cd\u4f5c",
            sentence_delimiter: "\u3002"
        },
        actions: null,
        beauty_solution: {
            staff: {
                staff_not_available: "\u5458\u5de5\u4f11\u5047"
            }
        },
        mongoid: {
            models: {
                customer: "\u987e\u5ba2",
                menu_item: "\u83dc\u5355\u9879\u76ee",
                table: "\u684c\u5b50"
            },
            errors: {
                models: {
                    table_combo: {
                        max_recursive_tables_exceeded: "\u300c\u6240\u6709\u684c\u5b50\u8fde\u63a5\u300d\u4e0d\u80fd\u7ec4\u54088\u4e2a\u4ee5\u4e0a\u7684\u684c\u5b50\u3002"
                    }
                }
            }
        },
        active_admin: {
            filter: "\u8fc7\u6ee4"
        },
        table_solution: {
            hq_view: {
                all_shops: "\u6240\u6709\u5546\u5e97"
            }
        },
        customers: {
            query: {
                filter_active: "\u641c\u7d22\u4e2d\u2026"
            }
        },
        reservations: {
            table_capacity_exceeded: "\u4e88\u7d04\u4eba\u6570 (%{people})\u8d85\u8fc7\u4e86\u684c\u5b50\u7684\u4e0a\u9650\u4eba\u6570 (%{capacity})"
        },
        sections: {
            uncheck_to_combine: "\u5982\u679c\u5e0c\u671b\u624b\u52a8\u6307\u5b9a\u7684\u8bdd\uff0c\u8bf7\u53bb\u6389\u4e0a\u9762\u7684\u52fe\u3002"
        }
    }, I18n.translations["zh-TW"] = {
        date: {
            abbr_day_names: ["\u65e5", "\u4e00", "\u4e8c", "\u4e09", "\u56db", "\u4e94", "\u516d"],
            month_names: [null, "\u4e00\u6708", "\u4e8c\u6708", "\u4e09\u6708", "\u56db\u6708", "\u4e94\u6708", "\u516d\u6708", "\u4e03\u6708", "\u516b\u6708", "\u4e5d\u6708", "\u5341\u6708", "\u5341\u4e00\u6708", "\u5341\u4e8c\u6708"],
            abbr_month_names: [null, "1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"]
        },
        datetime: {
            prompts: {
                year: "\u5e74",
                month: "\u6708",
                day: "\u65e5",
                hour: "\u6642",
                minute: "\u5206"
            }
        },
        errors: {
            app_error: "\u7cfb\u7d71\u932f\u8aa4"
        },
        keywords: {
            actions: "\u64cd\u4f5c",
            sentence_delimiter: "\u3002"
        },
        actions: null,
        beauty_solution: {
            staff: {
                staff_not_available: "\u54e1\u5de5\u4f11\u5047"
            }
        },
        mongoid: {
            models: {
                customer: "\u9867\u5ba2",
                menu_item: "\u83dc\u55ae\u9805\u76ee",
                table: "\u684c\u5b50"
            },
            errors: {
                models: {
                    table_combo: {
                        max_recursive_tables_exceeded: "\u300c\u6240\u6709\u684c\u5b50\u9023\u63a5\u300d\u4e0d\u80fd\u7d44\u54088\u500b\u4ee5\u4e0a\u7684\u684c\u5b50\u3002"
                    }
                }
            }
        },
        active_admin: {
            filter: "\u7be9\u9078"
        },
        table_solution: {
            hq_view: {
                all_shops: "\u6240\u6709\u5546\u5e97"
            }
        },
        customers: {
            query: {
                filter_active: "\u641c\u7d22\u4e2d\u2026"
            }
        },
        reservations: {
            table_capacity_exceeded: "\u4e88\u7d04\u4eba\u6578 (%{people})\u8d85\u904e\u4e86\u684c\u5b50\u7684\u4e0a\u9650\u4eba\u6578 (%{capacity})"
        },
        sections: {
            uncheck_to_combine: "\u5982\u679c\u5e0c\u671b\u624b\u52d5\u6307\u5b9a\u7684\u8a71\uff0c\u8acb\u53bb\u6389\u4e0a\u9762\u7684\u52fe\u3002"
        }
    }, I18n.translations.de = {
        date: {
            abbr_day_names: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
            month_names: [null, "Januar", "Februar", "M\xe4rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
            abbr_month_names: [null, "Jan", "Feb", "M\xe4r", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"]
        },
        datetime: {
            prompts: {
                year: "Jahr",
                month: "Monat",
                day: "Tag",
                hour: "Stunden",
                minute: "Minuten"
            }
        },
        errors: {
            app_error: "Anwendungsfehler"
        },
        keywords: {
            actions: "Aktionen",
            sentence_delimiter: ". "
        },
        actions: {
            set: "Setze",
            clear: "L\xf6sche",
            remove: "Entferne",
            show_all: "Zeige Alle",
            hide_all: "Verstecke Alle",
            no_models_matched: "Kein %{models} passen",
            no_models_exist: "Kein %{models} wurde geschafft.",
            upgrade_to_ts_full: "Bitte bis der Voll TS Version aktualisieren",
            confirmation: "Sind sie sicher?",
            cannot_undo: "Diese Aktion kann nicht r\xfcckg\xe4ngig gemacht werden.",
            may_incur_fee: "Diese Aktion kann mit Geb\xfchren verbunden.",
            additional_results: {
                one: "%{count} weiteres Ergebnis gefunden",
                other: "%{count} weitere Ergebnisse gefunden"
            },
            hide: "Verstecke",
            show: "Zeige",
            buy: "Kaufe",
            invalid_date_range: "Ung\xfcltige Datumsbereich"
        },
        beauty_solution: {
            staff: {
                staff_not_available: "Personal nicht verf\xfcgbar"
            }
        },
        mongoid: {
            models: {
                customer: {
                    one: "Kunde",
                    other: "Kundschaft"
                },
                menu_item: {
                    one: "Men\xfcpunkt",
                    other: "Men\xfcpunkte"
                },
                table: {
                    one: "Tisch",
                    other: "Tischen"
                }
            },
            errors: {
                models: {
                    table_combo: {
                        max_recursive_tables_exceeded: "Kann nicht gleichzeitig 'Verbinde Alle'zu mehr als 8 Tischen anwenden."
                    }
                }
            }
        },
        active_admin: {
            filter: "Filtern"
        },
        table_solution: {
            hq_view: {
                all_shops: "Alle Shops"
            }
        },
        customers: {
            query: {
                filter_active: "Filtering\u2026"
            }
        },
        reservations: {
            table_capacity_exceeded: "Reservierung G\xe4ste (%{people}) \xfcbersteigt Tischkapazit\xe4t (%{capacity})"
        },
        sections: {
            uncheck_to_combine: "Deaktivieren Sie oben, um manuelle Tisch Combos einzugeben"
        }
    }, I18n.translations.es = {
        date: {
            abbr_day_names: ["dom", "lun", "mar", "mi\xe9", "jue", "vie", "s\xe1b"],
            month_names: [null, "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
            abbr_month_names: [null, "ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"]
        },
        datetime: {
            prompts: {
                year: "A\xf1o",
                month: "Mes",
                day: "D\xeda",
                hour: "Hora",
                minute: "Minutos"
            }
        },
        errors: {
            app_error: "Error de aplicaci\xf3n"
        },
        keywords: {
            actions: "Acciones",
            sentence_delimiter: ". "
        },
        actions: {
            set: "Establecer",
            clear: "Borrar",
            remove: "Quitar",
            show_all: "Muestra todos",
            hide_all: "Oculta todos",
            no_models_matched: "No hay %{models} emparejados",
            no_models_exist: "No %{models} fue creado.",
            upgrade_to_ts_full: "Por favor, actualice a la versi\xf3n completa TS",
            confirmation: "Est\xe1s seguro?",
            cannot_undo: "No se puede deshacer esta acci\xf3n.",
            may_incur_fee: "Esta acci\xf3n puede incurrir en un cargo.",
            additional_results: {
                one: "%{count} resultado adicional encontrado",
                other: "%{count} resultados adicionales encontrados"
            },
            hide: "Ocultar",
            show: "Muestrar",
            buy: "Comprar",
            invalid_date_range: "Rango de fechas v\xe1lido"
        },
        beauty_solution: {
            staff: {
                staff_not_available: "El personal no disponible"
            }
        },
        mongoid: {
            models: {
                customer: {
                    one: "Cliente",
                    other: "Clientes"
                },
                menu_item: {
                    one: "Elemento de men\xfa",
                    other: "Elementos de men\xfa"
                },
                table: {
                    one: "Mesa",
                    other: "Mesas"
                }
            },
            errors: {
                models: {
                    table_combo: {
                        max_recursive_tables_exceeded: "No se puede aplicar 'Combine todos' a m\xe1s de 8 mesas a la vez."
                    }
                }
            }
        },
        active_admin: {
            filter: "Filtrar"
        },
        table_solution: {
            hq_view: {
                all_shops: "Todas Tiendas"
            }
        },
        customers: {
            query: {
                filter_active: "Filtraci\xf3n\u2026"
            }
        },
        reservations: {
            table_capacity_exceeded: "Hu\xe9spedes reserva (%{people}) excede la capacidad de mesas (%{capacity})"
        },
        sections: {
            uncheck_to_combine: "Desmarque arriba para especificar manualmente Mesa Combos"
        }
    }, I18n.translations.fr = {
        date: {
            abbr_day_names: ["dim", "lun", "mar", "mer", "jeu", "ven", "sam"],
            month_names: [null, "janvier", "f\xe9vrier", "mars", "avril", "mai", "juin", "juillet", "ao\xfbt", "septembre", "octobre", "novembre", "d\xe9cembre"],
            abbr_month_names: [null, "jan.", "f\xe9v.", "mar.", "avr.", "mai", "juin", "juil.", "ao\xfbt", "sept.", "oct.", "nov.", "d\xe9c."]
        },
        datetime: {
            prompts: {
                year: "Ann\xe9e",
                month: "Mois",
                day: "Jour",
                hour: "Heure",
                minute: "Minute"
            }
        },
        errors: {
            app_error: "Erreur d'application"
        },
        keywords: {
            actions: "Actions",
            sentence_delimiter: ". "
        },
        actions: {
            set: "R\xe9gler",
            clear: "Effacer",
            remove: "Supprimer",
            show_all: "Afficher tout",
            hide_all: "Masquer tout",
            no_models_matched: "Aucun %{models} ne correspond.",
            no_models_exist: "Aucun %{models} \xe0 \xe9t\xe9 cr\xe9\xe9.",
            upgrade_to_ts_full: "Veuillez mettre \xe0 niveau vers la version compl\xe8te",
            confirmation: "\xcates-vous s\xfbr?",
            cannot_undo: "Vous ne pouvez pas annuler cette action.",
            may_incur_fee: "Cette action peut encourir une charge.",
            additional_results: {
                one: "%{count} autre r\xe9sultat trouv\xe9",
                other: "%{count} autres r\xe9sultats trouv\xe9s"
            },
            hide: "Masquer",
            show: "Afficher",
            buy: "Acheter",
            invalid_date_range: "Dates invalides"
        },
        beauty_solution: {
            staff: {
                staff_not_available: "Personnel non disponible"
            }
        },
        mongoid: {
            models: {
                customer: {
                    one: "Client",
                    other: "Clients"
                },
                menu_item: {
                    one: "\xc9lement de menu",
                    other: "\xc9lement de menu"
                },
                table: {
                    one: "Table",
                    other: "Tables"
                }
            },
            errors: {
                models: {
                    table_combo: null
                }
            }
        },
        active_admin: null,
        table_solution: {
            hq_view: {
                all_shops: "Tous magazins"
            }
        },
        customers: {
            query: {
                filter_active: "Filtrant\u2026"
            }
        },
        reservations: {
            table_capacity_exceeded: "Le nombre de personnes de la r\xe9servation (%{people}) d\xe9passe la capacit\xe9 de la table (%{capacity})"
        },
        sections: {
            uncheck_to_combine: "D\xe9cochez ci-dessus pour indiquer manuellement les Liens des Tables"
        }
    }, I18n.translations.it = {
        date: {
            abbr_day_names: ["dom", "lun", "mar", "mer", "gio", "ven", "sab"],
            month_names: [null, "gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"],
            abbr_month_names: [null, "gen", "feb", "mar", "apr", "mag", "giu", "lug", "ago", "set", "ott", "nov", "dic"]
        },
        datetime: {
            prompts: {
                year: "Anno",
                month: "Mese",
                day: "Giorno",
                hour: "Ora",
                minute: "Minuto"
            }
        },
        errors: {
            app_error: "Errore dell'Applicazione"
        },
        keywords: {
            actions: "Azioni",
            sentence_delimiter: ". "
        },
        actions: {
            set: "Imposta",
            clear: "Cancella",
            remove: "Rimuovi",
            show_all: "Mostra Tutto",
            hide_all: "Nascondi Tutto",
            no_models_matched: "Nessun %{models} coincide",
            no_models_exist: "Nessuno dei %{models} \xe8 stato creato.",
            upgrade_to_ts_full: "Per favore aggiorna alla Versione TS Completa",
            confirmation: "Sei sicuro?",
            cannot_undo: "Non puoi annullare questa azione.",
            may_incur_fee: "Questa azione potrebbe incorrere in dei costi.",
            additional_results: {
                one: "%{count} risultato addizionale ritrovato",
                other: "%{count} risultati addizionali ritrovati"
            },
            hide: "Nascondi",
            show: "Mostra",
            buy: "Compra",
            invalid_date_range: "gamma di date invalide"
        },
        beauty_solution: {
            staff: {
                staff_not_available: "Personale non disponibile"
            }
        },
        mongoid: {
            models: {
                customer: {
                    one: "Cliente",
                    other: "Clienti"
                },
                menu_item: {
                    one: "Menu Strumento",
                    other: "Menu Strumenti"
                },
                table: {
                    one: "Tavolo",
                    other: "Tavoli"
                }
            },
            errors: {
                models: {
                    table_combo: {
                        max_recursive_tables_exceeded: "'Combina Tutti' non applicabile a pi\xf9 di 8 Tavoli alla volta."
                    }
                }
            }
        },
        active_admin: {
            filter: "Filtra"
        },
        table_solution: {
            hq_view: {
                all_shops: "Tutti Negozi"
            }
        },
        customers: {
            query: {
                filter_active: "Filtrando\u2026"
            }
        },
        reservations: {
            table_capacity_exceeded: "Clienti (%{people}) prenotati eccedono la capacit\xe1 (%{capacity}) di tavoli"
        },
        sections: {
            uncheck_to_combine: "Deseleziona sopra per specificare manualmente Combo di Tavoli"
        }
    },
    function() {
        var extractPath, maximizeHeight, popoverClickAway, show_away_popover;
        jQuery.extend(document, {
            isReady: function() {
                return "interactive" === this.readyState || "complete" === this.readyState
            }
        }), Array.prototype.uniq || (Array.prototype.uniq = function() {
            var t;
            return t = [], $.each(this, function(e, n) {
                return -1 === $.inArray(n, t) ? t.push(n) : void 0
            }), t
        }), Array.prototype.equals || (Array.prototype.equals = function(t) {
            return 0 === $(this).not(t).length && 0 === $(t).not(this).length
        }), Array.prototype.diff || (Array.prototype.diff = function(t) {
            return $.grep(this, function(e) {
                return -1 === $.inArray(e, t)
            })
        }), Array.prototype.remove || (Array.prototype.remove = function(t) {
            return this.splice(this.indexOf(t), 1)
        }), Array.prototype.compact || (Array.prototype.compact = function() {
            return this.filter(function(t) {
                return null != t && !!t
            })
        }), Array.prototype.flatten || (Array.prototype.flatten = function() {
            return [].concat.apply([], this)
        }), Array.prototype.indexOf || (Array.prototype.indexOf = function(t, e) {
            var n, i;
            for (null == e ? e = 0 : 0 > e && (e = Math.max(0, this.length + e)), n = e, i = this.length; i > n;) {
                if (this[n] === t) return n;
                n++
            }
            return -1
        }), Array.prototype.include || (Array.prototype.include = function(t) {
            return -1 !== this.indexOf(t)
        }), window.prevHref = "", extractPath = function(t) {
            return /^[a-z]+:\/\/\/?[^\/]+(\/[^?]*)/i.exec(t)[1]
        }, window.pushHistory = function(t, e) {
            var n;
            null == e && (e = !0), e && (window.prevHref = window.location.href);
            try {
                return history.pushState({
                    path: t
                }, "", t)
            } catch (i) {
                n = i
            }
        }, window.pushHistoryBack = function() {
            var t, e;
            try {
                return e = extractPath(window.prevHref), history.pushState({
                    path: e
                }, "", e)
            } catch (n) {
                t = n
            }
        }, $(document).on("click", '[data-dismiss="modal"]', window.pushHistoryBack), $(document).on("click", "[data-remote='true'][data-method!='delete']", function() {
            var t;
            return t = !$(this).hasParent("#ajax-modal"), $(this).is("form") ? void 0 : window.pushHistory(this.href, t)
        }), window.show_modal = function(t) {
            return null == t && (t = "large"), "large" !== t && $("#ajax-modal").removeClass("modal-large"), "medium" !== t && $("#ajax-modal").removeClass("modal-medium"), "small" !== t && $("#ajax-modal").removeClass("modal-small"), "large" === t && $("#ajax-modal").addClass("modal-large"), "medium" === t && $("#ajax-modal").addClass("modal-medium"), "small" === t && $("#ajax-modal").addClass("modal-small"), $("#ajax-modal").modal("show")
        }, window.hide_modal = function() {
            return $("#ajax-modal").modal("hide")
        }, jQuery.extend(jQuery.fn, {
            renew: function(t) {
                return $(this).ready(t), $(this).ajaxSuccess(function() {
                    return setTimeout(t, 10)
                })
            }
        }), jQuery.extend(jQuery.fn, {
            hasParent: function(t) {
                return $(t).find(this).length
            }
        }), jQuery.extend(jQuery.fn, {
            fadeReplace: function(t) {
                return $(this).fadeOut("slow", function() {
                    return $(this).replaceWith(t), $(this).fadeIn("fast")
                })
            }
        }), window.fadeTimer = null, jQuery.extend(jQuery.fn, {
            alertFade: function(t) {
                return clearTimeout(window.fadeTimer), $(this).replaceWith(t), window.fadeTimer = setTimeout(function() {
                    return $("#messages .alert").fadeOut("slow")
                }, 1e4)
            }
        }), window.flash_alert = function(t) {
            return t.length > 500 && (t = I18n.t("errors.app_error")), $("#messages").html('<div class="alert alert-error float"><a class="close" data-dismiss="alert">\xd7</a>' + t + "</div>"), window.fadeTimer = setTimeout(function() {
                return $("#messages .alert").fadeOut("slow")
            }, 1e4)
        }, $(document).on("click", ".alert.float", function() {
            return $(this).remove()
        }), $(document).renew(function() {
            return $("[data-popover], a[rel='popover']").popover(), $("[data-tooltip], a[rel='tooltip']").tooltip()
        }), popoverClickAway = !1, $(document).click(function() {
            return popoverClickAway ? ($("[data-popover][data-trigger='away'],a[rel=popover][data-trigger='away']").popover("hide"), popoverClickAway = !1) : popoverClickAway = !0, !0
        }), $(document).on("mouseup", "[data-popover][data-trigger='away'],a[rel=popover][data-trigger='away']", function(t) {
            var e;
            return e = $($(t.target)[0]), show_away_popover(e) ? t.preventDefault() : show_away_popover(e.parent()) ? t.preventDefault() : void 0
        }), show_away_popover = function(t) {
            return !t.data("popover") || t.hasClass("ui-draggable-dragging") || t.hasClass("ui-resizable-resizing") ? !1 : ($(".popover").hide(), t.popover("show"), popoverClickAway = !1, !0)
        }, $(document).on("change", ".event-tag-select", function() {
            return $(this).next().find(".dwwl0").toggle("single" === $(this).val())
        }), $(document).ajaxSuccess(function() {
            return $('[data-toggle="dropdown"]').each(function() {
                return $(this).parent().removeClass("open")
            })
        }), $(document).on("ajax:error", function(event, xhr, status) {
            var err;
            try {
                return eval(xhr.responseText)
            } catch (_error) {
                err = _error
            }
        }), $(document).renew(function() {
            return maximizeHeight()
        }), $(window).resize(function() {
            return maximizeHeight()
        }), maximizeHeight = function() {
            var t;
            return t = $(window).height(), $(".maximize-height").each(function(e, n) {
                var i, o, r, s;
                return n = $(n), i = n.data("max-height-buffer") ? parseInt(n.data("max-height-buffer"), 10) : 10, o = n.offset().top, s = parseInt(n.css("padding-top").replace("px", ""), 10), r = parseInt(n.css("padding-bottom").replace("px", ""), 10), n.height(t - o - s - r - i)
            })
        }, window.refocus = function() {
            return setTimeout(function() {
                return $('[autofocus="autofocus"]').focus()
            }, 500)
        }, $(document).on("click", '[data-toggle="dropdown"]', function() {
            return $(".alert.float").remove()
        }), window.clear_selection = function() {
            var t;
            if (t = window.getSelection ? window.getSelection() : document.selection) {
                if (t.removeAllRanges) return t.removeAllRanges();
                if (t.empty) return t.empty()
            }
        }
    }.call(this),
    function() {
        window.TagManagerHelper = function() {
                function t() {}
                return t.init = function() {
                    return $(".tm-input").each(function() {
                        return $(this).tagsManager({
                            deleteTagsOnBackspace: !1,
                            output: "#" + $(this).data("output"),
                            tagClass: $(this).data("tag-class")
                        })
                    })
                }, t
            }(), window.Device = function() {
                function t() {}
                return t.device = function() {
                    return $("body").data("device")
                }, t.is_mobile = function() {
                    return "mobile" === this.device()
                }, t.is_tablet = function() {
                    return "tablet" === this.device()
                }, t.is_desktop = function() {
                    return "desktop" === this.device()
                }, t
            }(), window.TS = function() {
                
                function t() {}
                var e;
                var host_site = baseUrl ;
                return e = "", window.location.href.match("/beautysolution/") && (e = "/beautysolution"), t.url_base = function() {
                    return e + host_site
                }, t.app_mode = function() {
                    return $("body").data("app-mode")
                }, t.app_lite = function() {
                    return "lite" === this.app_mode()
                }, t.app_full = function() {
                    return "full" === this.app_mode()
                }, $(document).renew(function() {
                    return $(".ts-lite-disabled").tooltip({
                        title: I18n.t("actions.upgrade_to_ts_full"),
                        placement: "bottom"
                    })
                }), t
            }(), window.BsHack = function() {
                function t() {}
                return t.fix_btn_toggle = function() {
					return $('[data-toggle="buttons"]').each(function(t, e) {
                        return $(e).children("label.btn").each(function(t, e) {
                            return e = $(e), "checked" === e.children("input").first().attr("checked") ? e.addClass("active") : void 0
                        })
                    })
                }, t
            }(), $.each([1, 2, 3, 4, 5], function(t) {
                return $(document).on("click", ".show-field-group-" + t, function() {
                    var e;
                    e = $(this).data("target"), $.each([1, 2, 3, 4, 5], function(n) {
                        return t === n ? ($(".show-field-group-" + n).parent().addClass("active"), $("#" + e + "-field-group-" + n).toggle(!0)) : ($(".show-field-group-" + n).parent().removeClass("active"), $("#" + e + "-field-group-" + n).toggle(!1))
                    })
                })
            }), $(function() {
                window.setTimeout(function() {
                    window.addEventListener("popstate", function() {
                        $.getScript(location.href)
                    })
                }, 1e3)
            }), $(function() {
                $.fn.modal.defaults.modalOverflow = !0, $.fn.modal.defaults.backdrop = "static", $.fn.modal.defaults.attentionAnimation = !1, $.fn.modalmanager.defaults.spinner = '<div class="loading-spinner x48-loader"/>', $(document).on("click", '[data-remote="true"][data-modal="true"]', function() {
                    $("#ajax-modal").modal("hide"), $("body").modalmanager("loading")
                })
            }),
            function(t) {
                t.elementFromPoint = function(e, n) {
                    return document.elementFromPoint(Math.round(e - t(document).scrollLeft()), Math.round(n - t(document).scrollTop()))
                }
            }(jQuery)
    }.call(this),
    function() {
        window.DateUtil = function() {
            function t() {}
            return t.formatDate = function(t) {
                var e, n, i;
                return i = t.getFullYear(), n = (t.getMonth() + 1).toString(), e = t.getDate().toString(), 1 === n.length && (n = "0" + n), 1 === e.length && (e = "0" + e), i + "-" + n + "-" + e
            }, t.formatMonth = function(t) {
                return t.substr(5, 2)
            }, t.stringToDate = function(t) {
                var e;
                return t ? (e = new Date(t), new Date(e.getTime() + 6e4 * e.getTimezoneOffset())) : void 0
            }, t.beginningOfDay = function(t) {
                return new Date(t.setHours(0, 0, 0, 0))
            }, t.validateDateRange = function(t, e) {
                return !t || !e || e >= t
            }, t.eql = function(t, e) {
                return null == t && null == e || null != t && null != e && t.getTime() === e.getTime()
            }, t.nowEpoch = function() {
                return new Date / 1e3
            }, t.current_15_min = function() {
                return 900 * Math.floor(this.nowEpoch() / 900)
            }, t.next_15_min = function() {
                return 900500 - new Date % 9e5
            }, t
        }()
    }.call(this),
    function() {
        window.TablesSortable = function() {
            function t() {}
            return t.init = function() {
                return this.make_sections_movable(), this.make_tables_draggable()
            }, t.make_sections_movable = function() {
                return $("#tables-list-table tr.section-row").each(function(t) {
                    return function(e, n) {
                        var i;
                        return i = $(n), i.find(".move-up-btn").click(function() {
                            return t.ajax_move_section(i.data("id"), "higher")
                        }), i.find(".move-down-btn").click(function() {
                            return t.ajax_move_section(i.data("id"), "lower")
                        })
                    }
                }(this))
            }, t.make_tables_draggable = function() {
                return $("#tables-list-table tbody").sortable({
                    helper: this.fixHelper,
                    handle: ".tbl-sortable-handle",
                    stop: function(t) {
                        return function(e, n) {
                            var i, o, r;
                            return r = n.item, o = r.prevAll(".section-row"), o.length || (r.detach().insertAfter("tr.section-row:first"), o = r.prevAll(".section-row")), i = r.index() - o.index() - 1, t.ajax_update_table_position(r.data("id"), o.data("id"), i)
                        }
                    }(this)
                }).disableSelection()
            }, t.fixHelper = function(t, e) {
                return e.children().each(function() {
                    return $(this).width($(this).width())
                }), e
            }, t.ajax_move_section = function(t, e) {
                return $.ajax({
                    type: "PUT",
                    url: TS.url_base() + ("sections/" + _.escape(t)),
                    data: {
                        section: {
                            move_to: e
                        }
                    },
                    dataType: "script"
                })
            }, t.ajax_update_table_position = function(t, e, n) {
                return $.ajax({
                    type: "PUT",
                    url: TS.url_base() + ("tables_json/" + _.escape(t)),
                    data: JSON.stringify({
                        table: {
                            section_id: e,
                            move_to: n
                        }
                    }),
                    dataType: "json",
                    contentType: "application/json",
                    success: function() {
                        return function() {}
                    }(this),
                    error: function() {
                        return function(t) {
                            return window.flash_alert(t.responseText)
                        }
                    }(this)
                })
            }, t
        }()
    }.call(this),
    function() {
        window.ReservationStatus = function() {
            function t() {}
            return t.init_list_view = function(e) {
                return null == e && (e = null), t.make_status_badge_clickable(e), t.init_status_badge_mobi(e)
            }, t.make_status_badge_clickable = function(t) {
                return null == t && (t = null), this.badge_for_id(t).click(function(t) {
                    return t.stopPropagation(), $(this).mobiscroll("show")
                })
            }, t.init_status_badge_mobi = function(e) {
                return null == e && (e = null), t.init_status_mobiscroll(this.badge_for_id(e), function(t) {
                    return t.replaceWith("<span class='res-sts-mobi x16-loader' data-id='" + _.escape(t.data("id")) + "'/>")
                })
            }, t.badge_for_id = function(t) {
                var e;
                return null == t && (t = null), e = ".res-sts-mobi", t && (e += "[data-id='" + _.escape(t) + "']"), $(e)
            }, t.init_status_mobiscroll = function(e, n) {
                return t.has_status_variables() ? (e.mobiscroll().treelist({
                    rows: 5,
                    wheelArray: t.status_wheels,
                    theme: "tstree wp",
                    mode: "scroller",
                    display: "modal",
                    inputClass: "hide",
                    setText: !1,
                    cancelText: !1,
                    onValueTap: function(e, i) {
                        var o;
                        return e.parent().parent().parent().parent().parent().hasClass("dwwl1") ? (o = $(this).data("id"), $(".tstree.wp").remove(), $("button").removeAttr("disabled"), n($(this)), setTimeout(function() {
                            var e;
                            return e = i.temp[1], t.update_status(o, e)
                        }, 50)) : void 0
                    }
                }), e.each(function() {
                    return $(this).mobiscroll("setValue", t.get_status_with_group($(this).data("sts")))
                })) : void 0
            }, t.status_wheels = void 0, t.status_groups = void 0, t.get_status_with_group = function(t) {
                return [this.status_groups[t] + "_group", t]
            }, t.has_status_variables = function() {
                return null != this.status_wheels && null != this.status_groups
            }, t.update_status = function(t, e) {
                return $.ajax({
                    type: "PUT",
                    data: {
                        status: e
                    },
                    url: TS.url_base() + ("reservations/" + _.escape(t) + "/update_status"),
                    dataType: "script"
                })
            }, t
        }()
    }.call(this),
    function() {
        var t, e, n, i = {}.hasOwnProperty,
            o = function(t, e) {
                function n() {
                    this.constructor = t
                }
                for (var o in e) i.call(e, o) && (t[o] = e[o]);
                return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
            };
        t = function() {
            function t(t) {
                this.obj = t, this.bars = [], this.set_effective_times()
            }
            return t.prototype.effective_start = null, t.prototype.effective_end = null, t.prototype.effective_times_exist = function() {
                return null != this.effective_start && null != this.effective_end
            }, t.prototype.set_effective_times = function() {
                var t, e, n;
                return n = null, t = null, e = Timetable.selected_sheet(), $.each(Timetable.sheets(), function(e) {
                    return function(i, o) {
                        return e.obj.start_at_epoch >= o.start_at_epoch && e.obj.start_at_epoch < o.end_at_epoch ? e.effective_start = e.obj.start_at_epoch : e.obj.start_at_epoch < o.start_at_epoch && e.end_at_epoch() > o.start_at_epoch && (n || (n = o)), e.end_at_epoch() > o.start_at_epoch && e.end_at_epoch() <= o.end_at_epoch ? e.effective_end = e.end_at_epoch() : e.end_at_epoch() > o.end_at_epoch ? t = o : void 0
                    }
                }(this)), this.effective_start || null == n || (this.effective_start = n.start_at_epoch), this.effective_end || null == t || (this.effective_end = t.end_at_epoch), e && this.effective_start && this.effective_start <= e.start_at_epoch && (this.effective_start = e.start_at_epoch), e && this.effective_end && this.effective_end >= e.end_at_epoch ? this.effective_end = e.end_at_epoch : void 0
            }, t.prototype.set_res = function(t) {
                return this.obj = t, this.set_effective_times()
            }, t.prototype.set_bars = function(t) {
                this.bars = t
            }, t.prototype.add_bar = function(t) {
                return this.bars.push(t)
            }, t.prototype.res_id = function() {
                return this.obj.id
            }, t.prototype.each_bar = function(t) {
                return $.each(this.bars, function() {
                    return function(e, n) {
                        return t(n)
                    }
                }(this))
            }, t.prototype.end_at_epoch = function() {
                return this.obj.start_at_epoch + this.obj.duration
            }, t.prototype.setup_events = function() {
                return this.make_draggable(), this.make_resizable(), this.make_hoverable(), this.make_clickable()
            }, t.prototype.group_stylist_seat_keys = function() {
                var t, e, n, i, o;
                for (o = $.map(this.obj.stylist_seat_keys, function(t) {
                        var e;
                        return e = Timetable.stylists_list.indexOf(t), -1 !== e ? [
                            [e, t]
                        ] : null
                    }), o = $.grep(o, function(t) {
                        return null != t
                    }), o.sort(function(t, e) {
                        return t[0] - e[0]
                    }), e = [], n = 0; n < o.length;) {
                    for (t = [], t.push(o[n][1]), i = 1; null != o[n + i] && o[n + i][0] === o[n][0] + i;) t.push(o[n + i][1]), i++;
                    e.push(t), n += i
                }
                return e
            }, t.prototype.other_bars = function(t) {
                return this.bars.filter(function(e) {
                    return $(e)[0] !== $(t)[0]
                })
            }, t.prototype.layout = function() {}, t.prototype.get_style_string = function(t) {
                var e, n, i, o, r, s, a, l, c, u;
                if (Timetable.timetable_elem().length) return a = Timetable.timetable_elem().offset().top, s = Timetable.timetable_elem().offset().left, r = {}, (null != t ? t.length : void 0) ? (u = Timetable.get_table_cell(t[0], this.effective_start), c = Timetable.get_table_cell(t[0], this.effective_end - 900), null != u && u.length && null != c && c.length ? (i = Timetable.get_table_row(t[0]).offset().top - a, n = c.offset().left + c.width() - s, e = u.offset().left - s, r = this.get_bar_table_css(i, n, e, t.length)) : r = this.get_bar_other_css(!1)) : r = this.get_bar_other_css(),
                    function() {
                        var t;
                        t = [];
                        for (o in r) l = r[o], t.push("" + o + ": " + l);
                        return t
                    }().join(";")
            }, t.prototype.get_bar_table_css = function(t, e, n, i) {
                return {
                    width: "" + (e - n - 4) + "px",
                    height: "" + (Timetable.avg_height * i - 1) + "px",
                    position: "absolute",
                    left: "" + _.escape(n) + "px",
                    top: "" + _.escape(t) + "px",
                    "white-space": i > 1 ? "nowrap" : "nowrap"
                }
            }, t.prototype.get_bar_other_css = function(t) {
                return null == t && (t = !0), {
                    width: "" + Math.min(Timetable.avg_width * this.obj.duration / 900, 200) + "px",
                    height: "" + (Timetable.avg_height - 1) + "px",
                    visibility: t ? "visible" : "hidden"
                }
            }, t.prototype.clear = function() {
                return $("[data-id='" + _.escape(this.res_id()) + "']").remove(), Timetable.timebars.remove(this)
            }, t.prototype.make_hoverable = function() {
                return this.each_bar(function(t) {
                    return function(e) {
                        return e.hover(function() {
                            return Timetable.no_hilite ? void 0 : t.each_bar(function(t) {								
								
                                //add edit link to open booking dialog
//                                var padding_top = 0;
//                                var padding_right = 25;
//                                var top = (parseFloat(t.css('top')) + padding_top) + 'px';
//                                var left = (parseFloat(t.css('left')) + parseFloat(t.css('width')) - padding_right) + 'px';
//                                var order_id = t.attr('data-id');
//                                var strHtml = "<div class=\"edit-link\" style=\"width: 28px; height: 18px;position: absolute;left: "+left+";top: "+top+" ;z-index: 500;\">\n\
//                                                 <a href=\"javascript:void()\" onclick=\"editBooking("+order_id+")\" style=\"color: red;text-decoration: none;\">編集</a>\n\
//                                              </div>";
//                                if(t.prev('.edit-link').length == 0){
//                                    t.before(strHtml);
//                                }
                                //end edit
                                return t.addClass("active");
                            })
                        }, function() {
                            return $(e).hasClass("ui-resizable-resizing") ? void 0 : t.each_bar(function(t) {
                                //t.prev('.edit-link').remove();
                                return t.removeClass("active")
                            })
                        })
                    }
                }(this))
            }, t.prototype.make_draggable = function() {
                var t;
                return t = Timetable.timetable_elem().offset().left,  this.each_bar(function() {
                    return function(e) {
                        return e.draggable({
                            snap: "td.slot",
                            snapTolerance: Timetable.avg_width / 2,
                            revert: "invalid",
                            revertDuration: 250,
                            disabled: Timetable.is_clickable_mode() && !Device.is_desktop() && e.hasClass("timebar-res"),
                            start: function() {
                                return $(".popover").hide(),  $(this).data({
                                    
                                    originalPosition: {
                                        top: $(this).css("top"),
                                        left: $(this).css("left")
                                    }
                                }),$(this).prev('.edit-link').remove(), Timetable.get_table_col($(this).data("t")).addClass("hilite"),Timetable.bg_timebar_blk_soft(!0, e), Timetable.make_cover_div(!1)
                            },
                            stop: function() {
                                return Timetable.get_table_col($(this).data("t")).removeClass("hilite"), Timetable.bg_timebar_blk_soft(!1, e), Timetable.make_cover_div(!0)
                            },
                            drag: function() {
                                return $(this).data("timebar").each_bar(function(n) {
                                    return e !== n ? n.css({
                                        left: e.offset().left - t
                                    }) : void 0
                                })
                            }
                        })
                    }
                }(this))
            }, t.prototype.make_resizable = function() {
                return Device.is_desktop() && this.obj.stylist_seat_keys.length > 0 && !(this instanceof n && this.is_limbo()) ? this.each_bar(function(t) {
                    return function(e) {
                        return e.resizable({
                            autoHide: !0,
                            handles: "n, s, e, w",
                            resize: function(e, n) {
                                return t.each_bar(function(t) {
                                    return t.css({
                                        width: n.size.width,
                                        left: n.position.left
                                    })
                                })
                            },
                            start: function() {
                                return t.each_bar(function(t) {
                                    return t.addClass("ui-resizable-resizing"), t.prev('.edit-link').remove()
                                }), Timetable.no_hilite = !0, Timetable.bg_timebar_blk_soft(!0, e), Timetable.make_cover_div(!1)
                            },
                            stop: function(n, i) {
                                switch (t.each_bar(function(t) {
                                    return t.removeClass("ui-resizable-resizing")
                                }), e.data("uiResizable").axis) {
                                    case "n":
                                        t.stretch_tables(i, !0);
                                        break;
                                    case "s":
                                        t.stretch_tables(i, !1);
                                        break;
                                    case "e":
                                        t.update_duration(i, !0);
                                        break;
                                    case "w":
                                        t.update_duration(i, !1)
                                }
                                return Timetable.no_hilite = !1, Timetable.bg_timebar_blk_soft(!1, e), Timetable.make_cover_div(!0)
                            }
                        })
                    }
                }(this)) : void 0
            }, t.prototype.stretch_tables = function(t, e) {
                var n, i, o, r, s, a, l, c;
                return null == e && (e = !0), t.element.data("stys") ? (a = t.element.data("stys").split(","), s = Math.max(Math.round(t.element.height() / Timetable.avg_height), 1), o = $.map(this.other_bars(t.element), function(t) {
                    return t.data("stys").split(",")
                }).flatten(), e ? (n = a[a.length - 1], i = Timetable.stylists_list.indexOf(n) + 1, l = Math.max(i - s, 0)) : (n = a[0], l = Timetable.stylists_list.indexOf(n), i = l + s), r = Timetable.stylists_list.slice(l, i), c = r.concat(o), a.equals(r) ? this.reset_position_and_size(t) : t.element.data("timebar").ajax_update({
                    stylist_ids: c
                })) : void 0
            }, t.prototype.update_duration = function(t, e) {
                var i;
                if (null == e && (e = !0), i = {}, e) i.duration = Math.max(900, 900 * Math.round(t.size.width / Timetable.avg_width));
                else if (i.start_at_epoch = this.obj.start_at_epoch + 900 * Math.round((t.position.left - t.originalPosition.left) / Timetable.avg_width), i.duration = this.obj.start_at_epoch + this.obj.duration - i.start_at_epoch, this instanceof n && i.start_at_epoch !== this.obj.start_at_epoch && !confirm(window.i18n_confirm_time_change)) return this.reset_position_and_size(t), void this.layout();
                return this.ajax_update(i)
            }, t.prototype.reset_position_and_size = function(t) {
                return t.element.css(t.originalSize), t.element.css({
                    top: t.originalPosition.top + .5,
                    left: t.originalPosition.left + .5
                })
            }, t.prototype.show_loader = function() {
                return this.each_bar(function(t) {
                    return t.addClass("loading")
                })
            }, t.prototype.hide_loader = function() {
                return this.each_bar(function(t) {
                    return t.removeClass("loading")
                })
            }, t.prototype.error_blink = function() {
                return this.each_bar(function(t) {
                    return t.effect("highlight", {
                        color: "#ff0000"
                    }, 5e3)
                })
            }, t
        }(), n = function(t) {
            function e(t) {
                this.obj = t, e.__super__.constructor.call(this, this.obj)
            }
            return o(e, t), e.prototype.div_html = function(t) {
                var e, n, i, o, s, a, l,services;
				var r = '';
				services ='';
				
				if((_.escape(this.obj.is_hp) == 1)){
					r = "border-color: red;  border-width: 3px;";
				}else{ 
					if(_.escape(this.obj.has_nail) == 0){
						r = "border: 2px solid #F99;";
					}
					if(_.escape(this.obj.service_name) !== ''){
						var service_str = _.escape(this.obj.service_name);
						var service_1 = false;
						var service_2 = false;
						if(service_str.search("Hお直し") != -1 || service_str.search("ハンドお直し") != -1){
							service_1 = true;
						}					
						if(service_str.search("Fお直し") != -1 || service_str.search("フットお直し") != -1){
							service_2 = true;
						}
						if(service_1 == true || service_2 == true){
							r = "border: 2px solid #5C5;";
						}
					}
				}
				
				if(_.escape(this.obj.is_blocked) == 1) {
					r += "opacity:0.7;";
				}
				//services += (_.escape(this.obj.has_nail) == 0) ? "[未]" : '';
				services += (_.escape(this.obj.visit_section) == 1) ? "[N]" + _.escape(this.obj.service_name) : _.escape(this.obj.service_name);
				return e = (null != t ? t.length : void 0) ? "data-stys='" + _.escape(t.join(",")) + "'" : "", n = this.obj.has_memo ? '<i class="fa fa-thumb-tack"></i>' : "", i = this.obj.is_online_or_provider ? '<i class="fa fa-globe"></i>' : "", o = (null != (a = this.obj) ? a.points : void 0) && (null != (l = this.obj) ? l.points : void 0) > 0 ? '<span class="points">P</span>' : "", s = this.obj.has_requested_stylist ? '<i class="fa fa-hand-o-right"></i>' : "", r += this.get_style_string(t), "<div class='timebar timebar-res'\n  style='" + r + "'\n  data-id='" + this.obj.id + "'\n  data-t='" + _.escape(this.obj.start_at_epoch) + "'\n  data-d='" + _.escape(this.obj.duration) + "'\n  data-sn='" + _.escape(this.obj.serial_num) + "'\n  data-sts='" + _.escape(this.obj.status) + "'\n  " + e + ">\n  <div>\n    <span class='name'>" + _.escape(this.text_customer_name()) + "</span>\n  <span class='time'>" + _.escape(this.text_time()) + "</span>\n   </div>\n <div>\n  <span class='service'>" + services + "</span>\n  </div>\n <div>\n    <div class='icons'>\n      " + i + "\n      " + n + "\n      " + o + "\n      " + s + "\n    </div>\n    <span class='ords'>" + this.text_orders() + "</span>\n  </div>\n</div>"
            }, e.prototype.create_div_html = function() {
                var t;
                return this.is_assigned() ? function() {
                    var e, n, i, o;
                    for (i = this.group_stylist_seat_keys(), o = [], e = 0, n = i.length; n > e; e++) t = i[e], o.push(this.div_html(t));
                    return o
                }.call(this).join("\n") : this.div_html()
            }, e.prototype.is_assigned = function() {
                return !(this.is_noshow() || this.is_cancelled() || this.is_rejected() || this.is_limbo())
            }, e.prototype.make_clickable = function() {
                return this.each_bar(function(t) {
                    return function(e) {
                        return e.on("dblclick tap", function(e) {
                            return Timetable.is_clickable_mode() ? void 0 : (e.stopPropagation(), t.ajax_edit())
                        }), ReservationStatus.init_status_mobiscroll(e, function() {
                            return t.show_loader()
                        }), e.on("click", function(n) {
                            if (Timetable.is_clickable_mode()) switch (n.stopPropagation(), Timetable.mode()) {
                                case "status":
                                    return e.mobiscroll("show");
                                case "customer":
                                    return t.ajax_customer_show()
                            }
                        })
                    }
                }(this))
            }, e.prototype.text_customer_name = function() {                
                if (this.obj.customer_name != null) {
                    return this.obj.customer_name;
                } else if (this.obj.customer_kana != null) {
                    return this.obj.customer_kana; 
                } else {
                    return this.obj.serial_num;
                }              
            }, e.prototype.text_orders = function() {
                return this.obj.orders.length > 0 ? _.escape(this.obj.orders.join(", ")) : "&nbsp;"
            }, e.prototype.text_time = function() {
                return this.obj.start_at.substr(11, 5)
            }, e.prototype.is_limbo = function() {
                return !(this.is_cancelled_group() || this.obj.stylist_seat_keys.length && this.effective_times_exist())
            }, e.prototype.is_cancelled_group = function() {
                return this.is_cancelled() || this.is_noshow() || this.is_rejected()
            }, e.prototype.is_cancelled = function() {
                return "cancelled" === this.obj.status
            }, e.prototype.is_noshow = function() {
                return "noshow" === this.obj.status
            }, e.prototype.is_rejected = function() {
                return "rejected" === this.obj.status
            }, e.prototype.ajax_update = function(t) {
				var action_query = '';
				if(controller == 'orders' && action == 'seat'){
					action_query = "&is_seat=1"
				}
                return this.show_loader(), $.ajax({
					
                    type: "PUT",
                    url: TS.url_base() + ("ajax/updatetimebar?order_id=" + _.escape(this.obj.id)) + action_query,
                    data: t,
                    dataType: "script"
                })
            }, e.prototype.ajax_limbo = function() {
                return this.show_loader(), $.ajax({
                    type: "PUT",
                    url: TS.url_base() + ("reservations/" + _.escape(this.obj.id) + "/limbo"),
                    dataType: "script"
                })
            }, e.prototype.ajax_noshow = function() {
                return this.show_loader(), $.ajax({
                    type: "PUT",
                    url: TS.url_base() + ("reservations/" + _.escape(this.obj.id) + "/noshow"),
                    dataType: "script"
                })
            }, e.prototype.ajax_cancel = function() {
                return this.show_loader(), $.ajax({
                    type: "PUT",
                    url: TS.url_base() + ("reservations/" + _.escape(this.obj.id) + "/cancelled"),
                    dataType: "script"
                })
            }, e.prototype.ajax_reject = function() {
                return this.show_loader(), $.ajax({
                    type: "PUT",
                    url: TS.url_base() + ("reservations/" + _.escape(this.obj.id) + "/rejected"),
                    dataType: "script"
                })
            }, e.prototype.ajax_edit = function() {
				// thai
            	optionDialog(_.escape(this.obj.serial_num));
				return false;
		
                var t;				 
                /*return false, t = TS.url_base() + ("ajax/bookingdialog?order_id=" + _.escape(this.obj.serial_num)), false, $.ajax({
                    type: "GET",
                    url: t,
                    dataType: "script"
                })*/
                var url = TS.url_base() + ("ajax/bookingdialog?order_id=" + _.escape(this.obj.serial_num));
                var data = {};
                var method = 'GET';   
                showBookingDialog(url, data, method);
                //showMedicalChartDialog(_.escape(this.obj.serial_num))
				
            }, e.ajax_new = function(t, e, n) {
				
				var disable_seat_cookie = getCookie('DisableSeatCookie');
				var enable_seat_cookie = getCookie('EnableSeatCookie');
				var i;       
				var is_seat='';
				var url='';
				var method = 'POST';    
				var data = {
							reservation: {
								start_at_epoch: t,
								stylist_ids: n,
								duration: e
							}
						};
				// Disable chart seat
				if(disable_seat_cookie == 'on' && controller == 'orders' && action =='seat'){
					url = TS.url_base() + 'ajax/disablechartseat';	
					$.ajax({
						type: method,
						url: url,
						data: data,
						success: function (response) {
							res = JSON.parse(response);
							var dt = res.date;
							$.ajax({
								type: method,
								url: TS.url_base() + 'ajax/loadcalendar?is_seat=1',
								data: {
									date : dt,
									controller : controller,
									action: action,
									is_edit_seat: 1
								},
								success: function (response) {
									
									$("#workspace").replaceWith(response); 
									$("ul#btn-tab li").removeClass('active');
									if(action == "seat"){
										$("ul#btn-tab li").eq(0).addClass('active');
									}else {
										$("ul#btn-tab li").eq(1).addClass('active');
									}	
								}
							}); 
						}	
					});
				}else if(enable_seat_cookie == 'on' && controller == 'orders' && action =='seat'){
					url = TS.url_base() + 'ajax/enablechartseat';	
					$.ajax({
						type: method,
						url: url,
						data: data,
						success: function (response) {
							res = JSON.parse(response);
							var dt = res.date;
							$.ajax({
								type: method,
								url: TS.url_base() + 'ajax/loadcalendar?is_seat=1',
								data: {
									date : dt,
									controller : controller,
									action: action,
									is_edit_seat: 1
								},
								success: function (response) {
									
									$("#workspace").replaceWith(response); 
									$("ul#btn-tab li").removeClass('active');
									if(action == "seat"){
										$("ul#btn-tab li").eq(0).addClass('active');
									}else {
										$("ul#btn-tab li").eq(1).addClass('active');
									}	
								}
							}); 
						}	
					});
				}else{
					if(controller == 'orders' && action =='seat'){
						is_seat = "?is_seat=1";
					}
					url = TS.url_base() + 'ajax/bookingdialog'+is_seat;
					showBookingDialog(url, data, method);
				}
				
                /*
                return null == t && (t = null), null == e && (e = null), null == n && (n = []), $("body").modalmanager("loading"), i = TS.url_base() + "bookings", window.pushHistory(i), $.ajax({
                    type: "GET",
                    url: i,
                    data: {
                        reservation: {
                            start_at_epoch: t,
                            stylist_ids: n,
                            duration: e
                        }
                    },
                    dataType: "script"
                })*/
            }, e.prototype.ajax_customer_show = function() {
                var t;
                return null != this.obj.customer_slug ? ($("body").modalmanager("loading"), t = TS.url_base() + ("customers/" + _.escape(this.obj.customer_slug)), window.pushHistory(t), $.ajax({
                    type: "GET",
                    url: t,
                    dataType: "script"
                })) : void 0
            }, e
        }(t), e = function(t) {
            function e(t) {
                this.obj = t, e.__super__.constructor.call(this, this.obj)
            }
            return o(e, t), e.prototype.div_html = function(t) {
                var e, n, i;
                return e = (null != t ? t.length : void 0) ? "data-stys='" + _.escape(t.join(",")) + "'" : "", n = this.obj.memo || "", i = this.get_style_string(t), "<div class='timebar timebar-blk'\n  style='" + i + "'\n  data-id='" + _.escape(this.obj.id) + "'\n  data-type='" + _.escape(this.obj.blockage_type) + "'\n  data-t='" + _.escape(this.obj.start_at_epoch) + "'\n  data-d='" + _.escape(this.obj.duration) + "'\n  data-sn='" + _.escape(this.obj.serial_num) + "'\n  data-sts='" + _.escape(this.obj.status) + "'\n  " + e + ">\n  " + _.escape(n) + "\n</div>"
            }, e.prototype.create_div_html = function() {
                var t;
                return null != this.obj.stylist_seat_keys ? function() {
                    var e, n, i, o;
                    for (i = this.group_stylist_seat_keys(), o = [], e = 0, n = i.length; n > e; e++) t = i[e], o.push(this.div_html(t));
                    return o
                }.call(this).join("\n") : void 0
            }, e.prototype.is_assigned = function() {
                return !0
            }, e.prototype.make_clickable = function() {
                return this.each_bar(function(t) {
                    return function(e) {
                        return e.on("dblclick tap", function(e) {
                            return e.stopPropagation(), t.ajax_edit()
                        })
                    }
                }(this))
            }, e.prototype.ajax_edit = function() {
                var t;
                return $("body").modalmanager("loading"), t = "" + TS.url_base() + "blockages/" + this.obj.id + "/edit", window.pushHistory(t), $.ajax({
                    type: "GET",
                    url: t,
                    dataType: "script"
                })
            }, e.prototype.ajax_update = function(t) {
                return this.show_loader(), $.ajax({
                    type: "PUT",
                    url: "" + TS.url_base() + "blockages/" + this.obj.id,
                    data: JSON.stringify({
                        blockage: t
                    }),
                    dataType: "json",
                    contentType: "application/json",
                    success: function() {
                        return function(t) {
                            return Timetable.create_timebar(t.blockage)
                        }
                    }(this),
                    error: function(t) {
                        return function(e) {
                            return t.hide_loader(), t.error_blink(), window.flash_alert(e.responseText), Timetable.relayout()
                        }
                    }(this)
                })
            }, e.prototype.ajax_destroy = function() {
                return this.show_loader(), $.ajax({
                    type: "DELETE",
                    url: "" + TS.url_base() + "blockages/" + this.obj.id,
                    dataType: "json",
                    contentType: "application/json",
                    success: function(t) {
                        return function() {
                            return t.clear()
                        }
                    }(this),
                    error: function() {
                        return function() {
                            return Timetable.relayout()
                        }
                    }(this)
                })
            }, e.prototype.ajax_limbo = function() {
                return this.ajax_destroy()
            }, e.prototype.ajax_noshow = function() {
                return this.ajax_destroy()
            }, e.prototype.ajax_cancel = function() {
                return this.ajax_destroy()
            }, e.prototype.ajax_reject = function() {
                return this.ajax_destroy()
            }, e.ajax_create = function(t, e, n, i) {
                return null == t && (t = null), null == e && (e = null), null == n && (n = []), null == i && (i = null), $.ajax({
                    type: "POST",
                    url: TS.url_base() + "blockages",
                    data: JSON.stringify({
                        blockage: {
                            start_at_epoch: t,
                            duration: e,
                            stylist_ids: n
                        }
                    }),
                    dataType: "json",
                    contentType: "application/json",
                    success: function(t) {
                        return Timetable.clear_selection(), Timetable.create_timebar(t.blockage)
                    },
                    error: function() {
                        return function(t) {
                            return Timetable.clear_selection(), window.flash_alert(t.responseText)
                        }
                    }(this)
                })
            }, e
        }(t), window.Timetable = function() {
            function t() {}
            return t.timebars = [], t.timebar_id_map = [], t.no_hilite = !1, $(document).renew(function() {
                return t.set_table_dimensions()
            }), t.stylists_list = [], t.set_stylists_list = function() {
                return t.stylists_list = [], $("#res-timetable-table tr[data-sty]").each(function(e, n) {
                    return t.stylists_list.push($(n).data("sty"))
                })
            }, t.timebar_by_id = function(t) {
                return this.timebar_id_map[t]
            }, t.draw = function() {
                return document.isReady() ? (this.hack_limbo_and_meals_layout(), this.ctrl_clear_timetable(), this.set_stylists_list(), this.set_table_dimensions(), this.set_timer(), this.make_timetable_droppable(), this.make_timetable_selectable(), this.make_timetable_clickable(), this.make_cols_hilite(), this.setup_action_btns(), this.make_cover_div(), this.make_meals_clickable(), this.create_timebars(), setTimeout(function(t) {
                    return function() {
                        return t.set_timebars_events()
                    }
                }(this), 500), this.timetable_elem().removeClass("timebars-loading"), this.draw_stylist_work_offs()) : void 0
            }, t.stylists_working_off = function() {
                
                return $.map(window.stylists, function(t) {
                    return t.is_working ? null : t
                })
                
            }, t.find_rows_for_stylist = function(t) {
                var e;
                return e = [], $.each($("#res-timetable-table tr.timetable-row"), function(n, i) {
                    var o;
                    return o = $(i).data("sty").split("_")[0], o === t.id ? e.push(i) : void 0
                }), e
            }, t.slots_count = function() {
                var t, e;
                return t = $("tr.timetable-row:not(.stylist-work-off):first"), e = $(t).find("td:not(.timetable-column-stylist)"), $(e).length
            }, t.set_row_status_work_off = function(e, n) {
                var i;
                return $(n).addClass("stylist-work-off"), $(n).find("td:not(.timetable-column-stylist)").remove(), "0" === $(n).data("sty").split("_")[1] ? (i = $("<td>"), i.addClass("slot-off text-center"), i.attr("colspan", t.slots_count()), i.attr("rowspan", e.num_seats), i.html(I18n.t("beauty_solution.staff.staff_not_available")), i.appendTo($(n))) : void 0
            }, t.draw_stylist_work_offs = function() {
                
                return $.each(this.stylists_working_off(), function(e, n) {
                    var i;
                    return i = t.find_rows_for_stylist(n), $.each(i, function(e, i) {
                        return t.set_row_status_work_off(n, i)
                    })
                })
            
            }, t.hack_limbo_and_meals_layout = function() {
                return $("#limbo-wrapper").css("overflow", "visible"), $("#limbo-wrapper").css("margin-right", $("#meals-nav").width() + 10)
            }, t.make_meals_clickable = function() {
                return $("#meals-nav li").click(function() {
                    return $("#meals-nav li").removeClass("active"), $(this).addClass("active"), t.ajax_reload()
                })
            }, t.selected_meal = function() {
                return $("#meals-nav li.active a").length ? $("#meals-nav li.active a").data("meal") : null
            }, t.sheet_set = function() {
                return window.sheet_set
            }, t.sheets = function() {
                return [this.sheet_set().all_day, this.sheet_set().breakfast, this.sheet_set().lunch, this.sheet_set().tea, this.sheet_set().dinner, this.sheet_set().night].compact()
            }, t.selected_sheet = function() {
                var t;
                return t = this.selected_meal(), null != t ? this.sheet_set()[t] : null
            }, t.draw_timebar = function(e) {
                return t.timetable_elem().length ? ($("body").css("overflow", "visible"), this.ctrl_clear_timebar(e.id), this.create_timebar(e), this.set_table_dimensions(), $("body").removeAttr("style")) : void 0
            }, t.place_bars = function(t) {
                var e, n, i, o, r, s;
                for ($("[data-id='" + _.escape(t.res_id()) + "']").remove(), $("body").css("overflow", "visible"), e = t.create_div_html(), t.is_assigned() ? $(e).appendTo("#timebar-container") : (t.is_noshow() && $(e).appendTo("#res-noshow-body"), t.is_cancelled() && $(e).appendTo("#res-cancelled-body"), t.is_rejected() && $(e).appendTo("#res-rejected-body"), t.is_limbo() && $(e).appendTo("#res-limbo-body")), $("body").removeAttr("style"), r = $("[data-id='" + _.escape(t.res_id()) + "']"), s = [], i = 0, o = r.length; o > i; i++) n = r[i], s.push($(n));
                return s
            }, t.create_timebar = function(i) {
                var o, r, s, a, l, c;
                if (t.timetable_elem().length) {
                    for (a = i.blockage_type ? e : n, this.timebar_id_map[i.id] ? (o = this.timebar_id_map[i.id], o.set_res(i)) : (o = new a(i), this.timebar_id_map[i.id] = o, this.timebars.push(o)), s = this.place_bars(o), o.set_bars(s), l = 0, c = s.length; c > l; l++) r = s[l], r.data("timebar", o);
                    return o.setup_events()
                }
            }, t.cover_div = null, t.make_cover_div = function(e) {
                var n, i;
                return null == e && (e = !0), Device.is_desktop() ? void 0 : ($("#res-cover").length || (this.cover_div = $("<div id='res-cover' class='cover'/>").appendTo("#timebar-container")), e ? (n = "draw" === t.mode() || "block" === t.mode(), i = $("#res-timetable-table"), this.cover_div.css({
                    width: n ? $("td.slot:first").offset().left - i.offset().left : i.width() + 2,
                    height: i.height() + 2,
                    display: ""
                })) : this.cover_div.css({
                    display: "none"
                }))
            }, t.timer = null, t.set_timer = function() {
                return this.timer && window.clearTimeout(this.timer), $("td.slot").removeClass("now"), $('td.slot[data-t="' + DateUtil.current_15_min() + '"]').addClass("now"), this.timer = window.setTimeout(t.set_timer, DateUtil.next_15_min())
            }, t.mode = function(t) {
                var e;
                return e = $("#res-timetable"), null != t ? e.attr("data-mode", t) : e.attr("data-mode")
            }, t.is_clickable_mode = function() {
                return "status" === t.mode() || "customer" === t.mode()
            }, t.action_btns = function() {
                return $("#res-action-btns .btn")
            }, t.make_timetable_clickable = function() {
                return Device.is_desktop() ? $("#res-timetable-table td.slot").on("dblclick", function(e) {
                    var i, o;
                    return "block" !== t.mode() ? (e.stopPropagation(), o = $(this).data("t"), i = [$(this).parent().data("sty")], n.ajax_new(o, null, i)) : void 0
                }) : void 0
            }, t.enable_res_drag = function() {
                var e;
                return e = t.is_clickable_mode() && !Device.is_desktop(), $(".timebar-res").draggable("option", "disabled", e)
            }, t.enable_draw = function() {
                var e;
                return e = "block" === t.mode() || (Device.is_desktop() || "draw" === t.mode()) && !TS.app_lite(), t.timetable_elem().selectable("option", "disabled", !e)
            }, t.setup_action_btns = function() {
                return this.setup_action_state($("#res-status-btn"), "status"), this.setup_action_state($("#res-block-btn"), "block"), this.setup_action_state($("#res-customer-btn"), "customer"), this.setup_action_state($("#res-draw-btn"), "draw")
            }, t.setup_action_state = function(e, n) {
                return e.length && !e.data("init") ? (e.click(function() {
                    var i;
                    return i = t.action_btns(), t.mode() === n ? (i.removeClass("active"), t.mode("default")) : (i.removeClass("active"), e.addClass("active"), t.mode(n)), t.enable_draw(), t.enable_res_drag(), t.make_cover_div()
                }), e.data("init", !0)) : void 0
            }, t.tt = !1, t.timetable_elem = function() {
                return this.tt || (this.tt = $("#res-timetable-table"))
            }, t.avg_height = 33, t.avg_width = 16, t.set_table_dimensions = function() {
                var t, e, n;
                return this.tt = $("#res-timetable-table"), $("td.slot").length ? (n = $("tr[data-sty]:not(.stylist-work-off):first"), t = n.find("td.slot:first"), e = n.find("td.slot").length, this.avg_width = (n.position().left + n.width() - t.position().left) / e, this.avg_height = n.height()) : void 0
            }, t.ctrl_clear_timetable = function() {
                return this.timebars = [], $("#timebar-container").html(""), $("#res-limbo-body").html(""), $("#res-noshow-body").html(""), $("#res-cancelled-body").html(""), $("#res-rejected-body").html("")
            }, t.ctrl_clear_timebar = function(t) {
                return $(".timebar[data-id=" + t + "]").remove()
            }, t.create_timebars = function() {
                var t, i, o, r, s, a, l;
                if (null != window.reservations)
                    for (a = window.reservations, i = 0, r = a.length; r > i; i++) t = a[i], this.timebars.push(new n(t));
                if (null != window.blockages)
                    for (l = window.blockages, o = 0, s = l.length; s > o; o++) t = l[o], this.timebars.push(new e(t));
                return this.draw_all_bars()
            }, t.clear_bars = function() {
                var t, e, n, i, o;
                for ($("timebars").remove(), i = this.timebars, o = [], e = 0, n = i.length; n > e; e++) t = i[e], o.push(t.set_bars([]));
                return o
            }, t.draw_all_bars = function() {
                var t, e, n, i, o, r, s, a, l, c, u, d, h;
                for ($(".timebar").remove(), o = {
                        noshow: [],
                        cancelled: [],
                        rejected: [],
                        limbo: [],
                        assigned: []
                    }, u = this.timebars, s = 0, l = u.length; l > s; s++) e = u[s], this.timebar_id_map[e.res_id()] = e, n = e.create_div_html(), e.is_assigned() ? o.assigned.push(n) : (e.is_noshow() && o.noshow.push(n), e.is_cancelled() && o.cancelled.push(n), e.is_rejected() && o.rejected.push(n), e.is_limbo() && o.limbo.push(n));
                for ($("body").css("overflow", "visible"), $("#res-noshow-body").append(o.noshow.join("\n")), $("#res-cancelled-body").append(o.cancelled.join("\n")), $("#res-rejected-body").append(o.rejected.join("\n")), $("#res-limbo-body").append(o.limbo.join("\n")), $("#timebar-container").append(o.assigned.join("\n")), $("body").removeAttr("style"), d = $(".timebar"), h = [], a = 0, c = d.length; c > a; a++) t = d[a], i = $(t), r = i.data("id"), e = this.timebar_id_map[r], e.add_bar(i), h.push(i.data("timebar", e));
                return h
            }, t.layout_timebars = function() {
                return $("#res-timetable").length || (t.timebars = []), $.each(t.timebars, function(t, e) {
                    return e.layout()
                })
            }, t.set_timebars_events = function() {
                return $.each(t.timebars, function(t, e) {
                    return e.setup_events()
                })
            }, t.make_timetable_droppable = function() {
                return this.make_res_timetable_droppable(), this.make_res_limbo_droppable(), this.make_res_noshow_droppable(), this.make_res_cancelled_droppable(), this.make_res_rejected_droppable()
            }, t.make_res_limbo_droppable = function() {
                return $("#res-limbo").droppable({
                    accept: ".timebar",
                    hoverClass: "ui-state-hover",
                    tolerance: "pointer",
                    drop: function(t, e) {
                        return e.draggable.data("timebar").ajax_limbo()
                    }
                })
            }, t.make_res_noshow_droppable = function() {
                return $("#res-noshow").droppable({
                    accept: ".timebar",
                    hoverClass: "ui-state-hover",
                    tolerance: "pointer",
                    drop: function(t, e) {
                        return e.draggable.data("timebar").ajax_noshow()
                    }
                })
            }, t.make_res_cancelled_droppable = function() {
                return $("#res-cancelled").droppable({
                    accept: ".timebar",
                    hoverClass: "ui-state-hover",
                    tolerance: "pointer",
                    drop: function(t, e) {
                        return e.draggable.data("timebar").ajax_cancel()
                    }
                })
            }, t.make_res_rejected_droppable = function() {
                return $("#res-rejected").droppable({
                    accept: ".timebar",
                    hoverClass: "ui-state-hover",
                    tolerance: "pointer",
                    drop: function(t, e) {
                        return e.draggable.data("timebar").ajax_reject()
                    }
                })
            }, t.handle_drop = function(e, n) {
                var i, o, r, s, a, l, c, u;
                if (n.draggable.css({
                        visibility: "hidden"
                    }), i = $($.elementFromPoint(n.offset.left + 4, n.offset.top + t.avg_height / 2)), n.draggable.css({
                        visibility: ""
                    }), i.hasClass("slot")) {
                    if (c = {}, n.draggable.draggable("option", "revert", "invalid"), a = i.data("t"), a !== n.draggable.data("t")) {
                        if (n.draggable.hasClass("timebar-res") && !confirm(window.i18n_confirm_time_change)) return void t.revert_draggable(n);
                        c.start_at_epoch = a
                    }
                    r = i.parent().data("sty"), s = [], r && (l = n.draggable.data("stys"), l ? (l = l.split(","), l[0] !== r && (u = t.stylists_list.indexOf(r), s = t.stylists_list.slice(u, u + l.length), o = $.map(n.draggable.data("timebar").other_bars(n.draggable), function(t) {
                        return t.data("stys").split(",")
                    }).flatten(), c.stylist_ids = s.concat(o))) : c.stylist_ids = [r]), c.start_at_epoch || c.stylist_ids ? n.draggable.data("timebar").ajax_update(c) : t.revert_draggable(n)
                } else t.revert_draggable(n)
            }, t.make_res_timetable_droppable = function() {
                return $("#res-timetable-table").droppable({
                    accept: ".timebar",
                    drop: function(e, n) {
                        return jQuery.browser.chrome && jQuery.browser.windows ? setTimeout(function() {
                            return t.handle_drop(e, n)
                        }, 0) : t.handle_drop(e, n)
                    }
                })
            }, t.revert_draggable = function(t) {
                return t.draggable.draggable("option", "revert", !0), $.each(t.draggable.data("timebar").bars, function() {
                    var e;
                    return e = $(this), e.data("stys") !== t.draggable.data("stys") ? e.animate({
                        left: t.draggable.data("originalPosition").left
                    }, 250) : void 0
                }), jQuery.browser.chrome && jQuery.browser.windows ? t.draggable.animate({
                    top: t.draggable.data("originalPosition").top,
                    left: t.draggable.data("originalPosition").left
                }, 250) : void 0
            }, t.get_table_row = function(t) {
                return $("tr[data-sty=" + t + "]")
            }, t.get_table_col = function(t) {
                return $("td.slot[data-t=" + t + "]")
            }, t.get_table_cell = function(t, e) {
                return $("tr[data-sty=" + t + "] td[data-t=" + e + "]")
            }, t.make_cols_hilite = function() {
                return Device.is_desktop() ? t.timetable_elem().tableHover({
                    colClass: "col-hover",
                    headCols: !0
                }) : void 0
            }, t.make_timetable_selectable = function() {
                return t.timetable_elem().selectable({
                    filter: "td.slot",
                    cancel: "td:not(.slot)",
                    disabled: TS.app_lite() || !Device.is_desktop(),
                    distance: 3,
                    start: function() {
                        return $(".ui-selectable-helper").remove(), t.no_hilite = !0, t.bg_timebar_blk_soft(!0)
                    },
                    stop: function() {
                        return t.get_data_from_selection(), t.no_hilite = !1, t.bg_timebar_blk_soft(!1)
                    }
                })
            }, t.get_data_from_selection = function() {
                var i, o, r, s;
                return o = $("td.slot.ui-selected"), o.length <= 0 ? t.clear_selection() : (s = $.map(o, function(t) {
                    return $(t).parent().data("sty")
                }).uniq(), r = o.first().data("t"), i = o.last().data("t") - r + 900, "block" === t.mode() ? e.ajax_create(r, i, s) : n.ajax_new(r, i, s))
            }, t.bg_timebar_blk_soft = function(t, e) {
                return null == e && (e = null), e && "soft" === e.attr("data-type") ? $('#timebar-container .timebar:not(.timebar-blk[data-type="soft"])').css({
                    "z-index": t ? -10 : ""
                }) : $('#timebar-container .timebar-blk[data-type="soft"]').css({
                    "z-index": t ? -10 : ""
                })
            }, t.clear_selection = function() {
                return $("td.slot.ui-selected").removeClass("ui-selected")
            }, t.relayout = function() {
                return t.clear_bars(), t.draw_all_bars(), setTimeout(function() {
                    return t.set_timebars_events()
                }, 500)
            }, t.debouncer = function(t) {
                var e;
                return e = null,
                    function(n) {
                        return n.target === window ? (clearTimeout(e), e = setTimeout(t, 100)) : void 0
                    }
            }, t.ajax_reload = function() {
                return $.ajax({
                    type: "GET",
                    url: TS.url_base() + "reservations/timetable",
                    data: {
                        meal: this.selected_meal()
                    },
                    dataType: "script"
                })
            }, $(window).on("resize.timetable", t.debouncer(function() {
                return t.set_table_dimensions(), t.relayout()
            })), $(document).ready(function() {
                return $("#ajax-modal").on("hide", t.clear_selection), $("#ajax-modal").on("shown", t.layout_timebars), $("#ajax-modal").on("hidden", function() {
                    return window.setTimeout(t.layout_timebars, 500)
                })
            }), t
        }()
    }.call(this),
    function() {
		function myDateFormatter (dateObject) {
			var d = new Date(dateObject);
			var day = d.getDate();
			var month = d.getMonth() + 1;
			var year = d.getFullYear();
			if (day < 10) {
				day = "0" + day;
			}
			if (month < 10) {
				month = "0" + month;
			}
			var date = year + "-" + month + "-" + day;

			return date;
		};
        window.Reservations = function() {
            function t() {}             
            return t.set_date = function(t) {	
				/******************* set date when user choosen it in calendar  *****************************/				
				var d = new Date();
				var strDate = myDateFormatter(d);											
				var date1 = new Date(strDate).getTime();
				var date2 = new Date(t).getTime();						
				var url = baseUrl;						
                                /*
				var pathName = window.location.pathname.split("/");	
				var controllerName = pathName[pathName.length - 2];
				var actionName	 = pathName[pathName.length-1];*/
                var controllerName = controller;
                var actionName = action;
                
				if (controllerName == 'orders' && actionName == 'lists') {
					url += "orders/lists?date="+t;
					window.location.href = url;
				} else if (controllerName == 'orders' && actionName == 'calendar') {
					url += "orders/calendar?date="+t;
					window.location.href = url;																			
				} else if (controllerName == 'ordertimelylimits' && actionName == 'index') {
					url += "ordertimelylimits?date="+t;
					window.location.href = url;												
				} else{	
					var action_query = '';
					if (controllerName == 'pages' && actionName == 'index') {
						url += "orders/staff?date="+t;	
					}					
					if(controllerName == 'orders' && actionName == 'seat'){
						url += "orders/seat?date="+t;	
						action_query = "?is_seat=1"
					}
					var url_ajax = baseUrl+'ajax/loadcalendar'+action_query;
					return $.ajax({
						type: "POST",
						url: url_ajax,
						data: {
							date: t,
							controller: controllerName,
							action: actionName
						},						
						success: function (response) {	
							history.pushState("","", url);
							$("#workspace").replaceWith(response);		
                            $("ul#btn-tab li").removeClass('active');
                            if(actionName == "seat"){
                                $("ul#btn-tab li").eq(0).addClass('active');
                            }else {
                                $("ul#btn-tab li").eq(1).addClass('active');
							}										
							if(date1 > date2){	
                               $('.label-info').replaceWith("<b class='label label-important'>過去</b><b id='todayId'><a href='"+url+"' data-remote='true'>本日</a></b>");									
								$( "b" ).remove( ".invisible" );								
							}
							else if(date1 < date2){	
                               $('.label-info').replaceWith("<b id='todayId'><a href='"+url+"' data-remote='true'>本日</a></b>");														
							}else{							
								$('#todayId').replaceWith("<b class='label label-info'>本日</b>");													
							}												
						}
					});	
				}		
				/******************* set date when user choosen it in calendar  *****************************/				
				//return false;	
				/*	
                return $.ajax({
                    type: "GET",
                    data: {
                        date: t
                    },
                    url: TS.url_base() + "ajax/loadcalendar",
                    dataType: "script"
                })
				*/
            }, t
        }()
    }.call(this),
     
     
    function() {
        window.Geocode = function() {
            function t() {}
            return t.initShopForm = function() {
                return this.bindGeocodeLookup(), this.bindGeocodeMapLink(), this.setGeocodeMapLink()
            }, t.bindGeocodeLookup = function() {
                return $("#geocode-lookup-link").click(function(t) {
                    return function() {
                        return t.ajaxGeocode()
                    }
                }(this))
            }, t.bindGeocodeMapLink = function() {
                return $("#shop_latitude, #shop_longitude").on("change keyup focus blur", function(t) {
                    return function() {
                        return t.setMapGeocode()
                    }
                }(this))
            }, t.setGeocodeMapLink = function() {
                var t, e;
                return t = $("#shop_latitude").val(), e = $("#shop_longitude").val(), $("#geocode-map-link").prop("href", "https://www.google.com/maps/place/" + t + "+" + e), $("#geocode-map-link").toggle(!(!t || !e))
            }, t.setGeocodeValues = function(t, e) {
                return $("#shop_latitude").val(t), $("#shop_longitude").val(e), this.setGeocodeMapLink()
            }, t.ajaxGeocode = function() {
                var t;
                if ($("#shop_latitude").length) return t = {
                    region: $("#shop_address_region").val(),
                    city: $("#shop_address_city").val(),
                    street: $("#shop_address_street").val()
                }, $.ajax({
                    type: "POST",
                    url: "" + TS.url_base() + "geocode",
                    data: JSON.stringify(t),
                    dataType: "json",
                    contentType: "application/json",
                    success: function(t) {
                        return function(e) {
                            var n, i;
                            return n = e.latitude, i = e.longitude, n && i ? t.setGeocodeValues(n, i) : void 0
                        }
                    }(this)
                })
            }, t
        }()
    }.call(this),
            
    function() {
        window.PostalCode = function() {
            function t() {}
            return t.init = function() {
                return this.bindPostalLookup()
            }, t.bindPostalLookup = function() {
                var e;
                return e = $(".postal-search-btn"), e.unbind("click"), e.click(function() {
                    return t.ajaxPostal(this)
                })
            }, t.setAddress = function(t, e, n, i) {
                return e && t.find(".address-input-region").val(e), n && t.find(".address-input-city").val(n), i ? t.find(".address-input-street").val(i) : void 0
            }, t.ajaxPostal = function(t) {
                var e, n;
                return n = $($(t).closest(".address-input-form")), e = {
                    postal_code: n.find(".address-input-postal_code").val()
                }, $.ajax({
                    type: "POST",
                    url: "" + TS.url_base() + "postal_codes",
                    data: JSON.stringify(e),
                    dataType: "json",
                    contentType: "application/json",
                    success: function(t) {
                        return function(e) {
                            var i, o, r;
                            if (e) return e = e.postal_code, r = e.region, o = e.city, i = e.street, t.setAddress(n, r, o, i)
                        }
                    }(this)
                })
            }, t
        }()
    }.call(this),
           
        
    
    function() {
        window.ShopForm = function() {
            function t() {}
            return t.init = function() {
                return this.toggle_stylist_request_from_shop_type()
            }, t.toggle_stylist_request_from_shop_type = function() {
                return $("#shop_shop_types_hair").change(function() {
                    return $("#shop_tc_show_stylist_request").prop("checked", $("#shop_shop_types_hair").prop("checked"))
                })
            }, t
        }()
    }.call(this),
    function() {
        window.MenuItemForm = function() {
            function t() {}
            return t.init = function() {
                return this.makeBeautycheckLinkToggleable(), this.toggleBeautycheckLink()
            }, t.makeBeautycheckLinkToggleable = function() {
                return $("#menu_item_providers_beautycheck").change(function(t) {
                    return function() {
                        return t.toggleBeautycheckLink()
                    }
                }(this))
            }, t.toggleBeautycheckLink = function() {
                return $(".menu_item_online_link").toggle($("#menu_item_providers_beautycheck").prop("checked"))
            }, t
        }()
    }.call(this),
    function() {
        window.Sheets = function() {
            function t() {}
            return t.init_calendar = function() {
                return $("#sheets-calendar td.day").click(function() {
                    return $("#sheets-calendar td.day").removeClass("selected"), $(this).addClass("selected"), t.show_loader(), t.ajax_sheet_override($(this).data("date"))
                })
            }, t.show_loader = function() {
                var t;
                return t = '<div class="sheet-override-left sheet-override-placeholder col-xs-9"><span class="x16-loader"/></div>', $(".sheet-override-left").replaceWith($(t))
            }, t.init_sheet_override = function() {
                return $("#sheet-override-block").click(function() {}), $("#sheet-override-destroy").click(function() {
                    return t.ajax_sheet_override($(this).data("date"), "DELETE")
                })
            }, t.ajax_sheet_override = function(t, e) {
                return null == e && (e = "GET"), $.ajax({
                    type: e,
                    url: "" + TS.url_base() + "sheets/overrides/" + t,
                    dataType: "script"
                })
            }, t.init_form = function() {
                return t.set_form_state($('.sheet_sheet_type input[checked="checked"]').val()), $(".sheet_sheet_type label.btn").click(function() {
                    return t.set_form_state($(this).find("input").val())
                })
            }, t.set_form_state = function(t) {
                return $("form .sheet_days").toggle("irregular" !== t), $("form .sheet_date_range").toggle("irregular" !== t), $("form .sheet_meal").toggle("closed" !== t), $("form .sheet_times").toggle("closed" !== t), $("form .sheet_is_show_online").toggle("closed" !== t), $("form .sheet_floor_plan_id").toggle("closed" !== t), $("form .sheet_menu_id").toggle("closed" !== t)
            }, t
        }()
    }.call(this),
    function() {
        window.ShopSelector = function() {
            function t() {}
            return t.init = function() {
                return t.initShops()
            }, t.initShops = function() {
                return $("#shops-container select").on("change", function() {
                    var e, n;
                    return n = t.shopsControls(), n.filter(function() {
                        return "" === $(this).val()
                    }).length || (e = n.last().closest(".fieldset").clone(!0, !0), e.find("select").val(""), e.appendTo(t.shopsContainer()).hide().slideDown()), t.toggleShopRemoveBtns()
                }), t.toggleShopRemoveBtns(), $(".rmvShop").on("click tap", function() {
                    return $(this).closest(".fieldset").remove(), t.toggleShopRemoveBtns()
                })
            }, t.toggleShopRemoveBtns = function() {
                return $(".rmvShop").toggleClass("invisible", $("#shops-container .fieldset").length <= 1)
            }, t.shopsContainer = function() {
                return $("#shops-container")
            }, t.shopsControls = function() {
                return t.shopsContainer().find("select")
            }, t
        }()
    }.call(this),
    function() {
        window.StylistsCalendar = function() {
            function t() {}
            return t.container = function() {
                return $("#stylists-matrix table")
            }, t.modeContainer = function() {
                return $("#stylists-matrix")
            }, t.init = function() {
                return this.bindOverrideBtnEvents(), this.bindSelectableEvent(), this.bindSingleCellClickEvents(), this.setDefaultActionMode()
            }, t.bindOverrideBtnEvents = function() {
                return $("#stylists-calendar").on("click", ".override-on", function() {
                    return t.setActionMode(this, "on")
                }), $("#stylists-calendar").on("click", ".override-off", function() {
                    return t.setActionMode(this, "off")
                }), $("#stylists-calendar").on("click", ".override-clear", function() {
                    return t.setActionMode(this, "clear")
                })
            }, t.bindSelectableEvent = function() {
                return t.container().selectable({
                    filter: "td.workday.workday-future",
                    cancel: "td:not(.workday)",
                    distance: 3,
                    stop: function() {
                        return void 0 === t.actionMode() ? t.selectedCells().removeClass("ui-selected") : "clear" === t.actionMode() ? t.ajaxRevertOverride() : t.ajaxOverride()
                    }
                })
            }, t.bindSingleCellClickEvents = function() {
                return this.container().on("click", ".workday.workday-future", function() {
                    var e;
                    return e = this, void 0 !== t.actionMode() && $(e).addClass("ui-selected"), t.container().selectable("option", "stop")()
                })
            }, t.setDefaultActionMode = function() {
                return $("#stylists-calendar .override-off").trigger("click")
            }, t.setActionMode = function(t, e) {
                switch (this.modeContainer().attr("data-override", e), $(t).addClass("active").siblings().removeClass("active btn-dkgray btn-danger btn-success").end(), e) {
                    case "on":
                        return $(t).addClass("btn-success");
                    case "off":
                        return $(t).addClass("btn-dkgray");
                    case "clear":
                        return $(t).addClass("btn-danger")
                }
            }, t.selectedCells = function() {
                return $("td.workday.ui-selected")
            }, t.actionMode = function() {
                return this.modeContainer().attr("data-override")
            }, t.ajaxOverrideData = function() {
                var e, n, i;
                return i = $.map(t.selectedCells(), function(t) {
                    return $(t).data("stylist")
                }), n = $.map(t.selectedCells(), function(t) {
                    return $(t).data("date")
                }), e = {}, e.is_on = "on" === t.actionMode(), e.stylist_ids = i.uniq(), e.dates = n.uniq(), e
            }, t.removeWorkdayClasses = function(t) {
                return $(t).removeClass("workday-override").removeClass("workday-regular").removeClass("workday-on").removeClass("workday-off")
            }, t.overrideOnIcon = function() {
                return $("<i>").addClass("fa fa-circle-o fa-lg")
            }, t.overrideOffIcon = function() {
                return $("<i>").addClass("fa fa-times fa-lg")
            }, t.showCellOverridden = function(e, n) {
                return t.removeWorkdayClasses(e), n ? $(e).addClass("workday-override workday-on").html(t.overrideOnIcon()) : $(e).addClass("workday-override workday-off").html(t.overrideOffIcon())
            }, t.showCellRegular = function(e) {
                return t.removeWorkdayClasses(e), 1 === $(e).data("regular") ? $(e).addClass("workday-regular workday-on").html(t.overrideOnIcon()) : $(e).addClass("workday-regular workday-off").html(t.overrideOffIcon())
            }, t.ajaxOverride = function() {
                return $.ajax({
                    url: TS.url_base() + "stylists/override",
                    type: "PUT",
                    data: t.ajaxOverrideData(),
                    dataType: "json",
                    success: function(e) {
                        var n;
                        return n = e.stylists[0].is_working, $.each(t.selectedCells(), function(e, i) {
                            return t.showCellOverridden(i, n)
                        })
                    },
                    error: function(t) {
                        return alert(t.responseJSON.error)
                    },
                    complete: function() {
                        return t.selectedCells().removeClass("ui-selected")
                    }
                })
            }, t.ajaxRevertOverride = function() {
                return $.ajax({
                    url: TS.url_base() + "stylists/override_revert",
                    type: "PUT",
                    data: t.ajaxOverrideData(),
                    dataType: "json",
                    success: function() {
                        return $.each(t.selectedCells(), function(e, n) {
                            return t.showCellRegular(n)
                        })
                    },
                    error: function(t) {
                        return alert(t.responseJSON.error)
                    },
                    complete: function() {
                        return t.selectedCells().removeClass("ui-selected")
                    }
                })
            }, t
        }()
    }.call(this),
    function() {
        window.UserForm = function() {
            function t() {}
            return t.init = function() {
                return this.make_shops_toggleable(), this.toggle_shops(), this.make_role_toggleable()
            }, t.make_shops_toggleable = function() {
                return $("#merchant_user_can_access_all_shops").change(function(t) {
                    return function() {
                        return t.toggle_shops()
                    }
                }(this))
            }, t.toggle_shops = function() {
                return $(".merchant_user_accessible_shop_ids").toggle(!$("#merchant_user_can_access_all_shops").prop("checked"))
            }, t.make_role_toggleable = function() {
                return $("#merchant_user_role").change(function() {
                    return $("#merchant_user_can_access_all_shops").prop("checked", $(this).val().match(/^(admin|franchise)/)), t.toggle_shops()
                })
            }, t
        }()
    }.call(this),
    function() {
        window.Providers = function() {
            function t() {}
            return t.make_provider_selectable = function() {
                return $("select.provider-select").on("change", function() {
                    return t.get_provider($("select.provider-select").val())
                })
            }, t.toggleEmailProvider = function() {
                return $("#shop_scraper_email").prop("disabled", $("#shop_scraper_use_shop_email").is(":checked"))
            }, $(document).renew(function() {
                return $("#provider-new-res-btn").click(function() {
                    var t, e;
                    return t = $("#provider-new-res-btn"), e = $("#provider-res-form"), t.hasClass("active") ? (t.removeClass("active"), e.hide("slide", 250)) : (t.addClass("active"), e.show("slide", 250))
                }), $("#hide-provider-login-btn").click(function() {
                    return $("#provider-login-hidden").show(), $("#provider-login-shown").hide()
                }), $("#show-provider-login-btn").click(function() {
                    return $("#provider-login-hidden").hide(), $("#provider-login-shown").show()
                }), t.toggleEmailProvider(), $("#shop_scraper_use_shop_email").change(function() {
                    return t.toggleEmailProvider()
                })
            }), t.set_date = function(t) {
                return $.ajax({
                    type: "GET",
                    data: {
                        date: t
                    },
                    url: TS.url_base() + "online/web",
                    dataType: "script"
                })
            }, t.get_provider = function(t) {
                return $.ajax({
                    type: "GET",
                    url: TS.url_base() + ("online/web/" + _.escape(t)),
                    dataType: "script"
                })
            }, t
        }()
    }.call(this),
    function() {
        window.ProviderActions = function() {
            function t() {}
            return t.base_path = function() {
                return "" + TS.url_base() + "online/inbox/"
            }, t.sendAjax = function(e, n, i) {
                return $.ajax({
                    type: n,
                    url: "" + t.base_path() + e,
                    dataType: "script",
                    data: i
                })
            }, t.sendAjaxRequestFor = function(e, n, i) {
                var o;
                return o = $(".selected").attr("data-id"), $.ajax({
                    type: n,
                    data: i,
                    url: "" + t.base_path() + o + "/" + e,
                    dataType: "script"
                })
            }, t.checkForSelected = function() {
                var t;
                return t = $(".selected").attr("data-id"), t ? this.sendAjax(t, "GET") : void 0
            }, t.updateEmail = function(t, e) {
                return this.sendAjax($(".selected").attr("data-id"), "PUT", {
                    status: t,
                    email_action: e
                })
            }, t.setInfoAndSendAjax = function(e) {
                return $(".selected").removeClass("selected"), $(e).addClass("selected"), t.sendAjax($(e).attr("data-id"), "GET")
            }, t.showValues = function(t) {
                return $(".extracted-values tbody").each(function(e, n) {
                    var i;
                    return i = $(t), $(n).hasClass("hide") ? ($(n).removeClass("hide"), i.text(I18n.t("actions.hide_all"))) : ($(n).addClass("hide"), i.text(I18n.t("actions.show_all")))
                })
            }, t.bindActionEvents = function() {
                return $(".skip").click(function() {
                    return t.updateEmail("skipped")
                }), $(".ok").click(function() {
                    return t.updateEmail("processed")
                }), $(".accept").click(function() {
                    return t.updateEmail("processed", "accept")
                }), $(".reject").click(function() {
                    return t.updateEmail("processed", "rejected")
                }), $(".ignore").click(function() {
                    return t.updateEmail("ignored")
                }), $(".import").click(function() {
                    if ($(this).hasClass("active")) return $(this).removeClass("active"), $(".import-res-form").hide().html(""), $(".mail-container").show();
                    switch ($(".selected").attr("data-action")) {
                        case "amend":
                            return $(this).addClass("active"), $(".import-res-form").show(), t.sendAjaxRequestFor("edit_reservation", "GET");
                        case "cancel":
                            return t.sendAjaxRequestFor("cancel", "PUT");
                        default:
                            return $(this).addClass("active"), $(".import-res-form").show(), t.sendAjaxRequestFor("import", "GET")
                    }
                })
            }, t.bindBrowseEvents = function() {
                return this.bindBrowseTableEvent(".unprocessed-emails tbody tr"), this.bindBrowseTableEvent(".processed-emails tbody tr")
            }, t.bindAttrsEvents = function() {
                return $(".show-all").click(function() {
                    return t.showValues(this)
                })
            }, t.bindBrowseTableEvent = function(e) {
                return $(e).click(function() {
                    return t.setInfoAndSendAjax(this)
                })
            }, t.setScrollPosition = function() {
                return $(".unprocessed-emails .selected").length ? $("#providers-unprocessed .scrollable").scrollTop($(".unprocessed-emails .selected")[0].offsetTop) : void 0
            }, t
        }()
    }.call(this),
    function() {
        window.MobiscrollHelper = function() {
            function t() {}
            return t.init = function() {
                return $(".mobidate").each(function() {
                    return $(this).mobiscroll().calendar({
                        cancelText: I18n.t("actions.clear"),
                        display: "bubble",
                        theme: "tsmobi",
                        navigation: "month",
                        controls: ["calendar"],
                        dayNamesShort: I18n.t("date.abbr_day_names"),
                        monthNames: I18n.t("date.month_names").slice(1),
                        yearText: I18n.t("datetime.prompts.year"),
                        monthText: I18n.t("datetime.prompts.month"),
                        dayText: I18n.t("datetime.prompts.day"),
                        dateFormat: "yy-mm-dd",
                        onDayChange: function(t) {
                            return $(this).val(DateUtil.formatDate(t.date)), $(this).mobiscroll("hide")
                        },
                        onCancel: function() {
                            return $(this).val("")
                        }
                    })
                })
            }, t
        }()
    }.call(this),
    function() {
        var t;
        t = function() {
            function t() {}
            return $(document).renew(function() {
                return $(".cell-toggle-show-all").click(function() {
                    return $(this).toggle(!1), $(this).next(".cell-toggle-hide-all").toggle(!0), $(".cell-toggle-show").toggle(!1), $(".cell-toggle-hide").toggle(!0)
                }), $(".cell-toggle-hide-all").click(function() {
                    return $(this).toggle(!1), $(this).prev(".cell-toggle-show-all").toggle(!0), $(".cell-toggle-show").toggle(!0), $(".cell-toggle-hide").toggle(!1)
                }), $(".cell-toggle-show").click(function() {
                    return $(this).toggle(!1), $(this).next(".cell-toggle-hide").toggle(!0)
                }), $(".cell-toggle-show").click(function() {
                    return $(this).toggle(!1), $(this).next(".cell-toggle-hide").toggle(!0)
                }), $(".cell-toggle-hide").click(function() {
                    return $(this).toggle(!1), $(this).prev(".cell-toggle-show").toggle(!0)
                })
            }), t
        }()
    }.call(this),
    function() {
        window.Keypad = function() {
            function t() {}
            return t.init = function(t) {
                return $(".btn-keypad").on("click", function() {
                    var e;
                    return e = $(t), e.val().length < e.attr("maxlength") ? e.val(e.val() + $(this).data("number")) : void 0
                }), $(".btn-keypad-clear").on("click", function() {
                    return $(t).val("")
                })
            }, t
        }()
    }.call(this),
    function() {
        window.Customers = function() {
            function t() {}
            return t.initialize = function() {
                return t.make_q_queryable(), t.make_dates_queryable(), t.make_filter_queryable(), t.make_filter_clearable()
            }, t.make_q_queryable = function() {
                return $("#cust-query #cust-q").on("keypress", function(e) {
                    return 13 === e.which ? t.set_q() : void 0
                }), $("#cust-query #cust-q").on("blur", function() {
                    return t.set_q()
                })
            }, t.make_dates_queryable = function() {
                return $("#cust-query .mobidate").each(function() {
                    return $(this).mobiscroll().calendar({
                        cancelText: I18n.t("actions.clear"),
                        display: "bubble",
                        theme: "tsmobi",
                        navigation: "month",
                        controls: ["calendar"],
                        dayNamesShort: I18n.t("date.abbr_day_names"),
                        monthNames: I18n.t("date.month_names").slice(1),
                        yearText: I18n.t("datetime.prompts.year"),
                        monthText: I18n.t("datetime.prompts.month"),
                        dayText: I18n.t("datetime.prompts.day"),
                        dateFormat: "yy-mm-dd",
                        onShow: function() {
                            return setTimeout(window.clear_selection, 10)
                        },
                        onDayChange: function(e) {
                            var n, i, o;
                            return o = t.get_date_range(this.id, e.date), n = o[0], i = o[1], DateUtil.validateDateRange(n, i) ? ($(this).val(DateUtil.formatDate(e.date)), $(this).mobiscroll("hide"), t.query_from_fields(), $(this).addClass("active")) : alert(I18n.t("actions.invalid_date_range"))
                        },
                        onCancel: function() {
                            return $("#cust-query .mobidate").val(""), t.query_from_fields(), $("#cust-query .mobidate").removeClass("active")
                        }
                    })
                }), $("#cust-query #cust-cal-btn").on("click", function() {
                    return $("#cust-query .mobidate:first").mobiscroll("show")
                })
            }, t.make_filter_queryable = function() {
                var e;
                return e = t.filter(), $.each(["shop"], function(n, i) {
                    return $("#cust-query .filter-options a.cust-filter-" + i).click(function() {
                        return e.data(i, $(this).data(i)), t.set_filter_label_active($(this).html()), $("#cust-query .filter-options a.cust-filter-" + i).removeClass("active"), $(this).addClass("active"), t.query_from_fields()
                    })
                })
            }, t.make_filter_clearable = function() {
                return t.filter_clear().on("click", function() {
                    return $.each(["shop"], function(e, n) {
                        return t.filter().data(n, "")
                    }), t.clear_filter_label(), t.clear_filter_data(), t.query_from_fields()
                })
            }, t.filter = function() {
                return $("#cust-filter")
            }, t.filter_clear = function() {
                return $("#cust-filter-clear")
            }, t.set_filter_label = function(e) {
                return $("#cust-filter-label").html(e), t.filter().addClass("btn-warning")
            }, t.set_filter_label_active = function(e) {
                return t.set_filter_label(t.get_shop() ? I18n.t("customers.query.filter_active") : e)
            }, t.clear_filter_label = function() {
                return t.set_filter_label(I18n.t("active_admin.filter")), t.filter().removeClass("btn-warning"), $("#cust-query .filter-options a").removeClass("active")
            }, t.clear_filter_data = function() {
                return $("#cust-query input").val(null)
            }, t.get_q = function() {
                return $("#cust-q").val()
            }, t.set_q = function() {
                return t.query_from_fields(), $("#cust-query #cust-q").toggleClass("active", t.get_q())
            }, t.get_shop = function() {
                return t.filter().data("shop")
            }, t.get_date = function(t, e, n) {
                return e && e === "cust-" + t + "-date" ? DateUtil.beginningOfDay(n) : DateUtil.stringToDate($("#cust-" + t + "-date").val())
            }, t.get_date_range = function(e, n) {
                return [t.get_date("from", e, n), t.get_date("to", e, n)]
            }, t.query_from_fields = function() {
                var e, n, i, o;
                return n = t.get_q(), i = t.get_shop(), e = t.get_date("from"), o = t.get_date("to"), t.query(n, e, o, i)
            }, t.query = function(t, e, n, i) {
                var o;
                return $("#cust-loading").removeClass("invisible"), o = {}, t && (o.q = t), e && (o.from_date = e), n && (o.to_date = n), i && (o.shop = i), $.ajax({
                    type: "GET",
                    data: o,
                    url: TS.url_base() + "customers",
                    dataType: "script"
                })
            }, t
        }()
    }.call(this),
    function() {
        window.ResCustomersQuery = function() {
            function t(t, e, n) {
                this.scope = t, this.model = null != e ? e : "reservation", this.simple = null != n ? n : !1, this.ajax_locked = !1, this.requery = !1, this.make_queryable(), this.make_clear_button(), this.create_dummy_field()
            }
            return t.selector_attrs = ["customer_user_name", "customer_phones_attributes_0_number", "customer_emails_attributes_0_email"], t.prototype.selectors = function() {
                return $.map(t.selector_attrs, function(t) {
                    return function(e) {
                        return "#" + t.model + "_" + e
                    }
                }(this))
            }, t.prototype.elem_scope = function() {
                return $(this.scope)
            }, t.prototype.elem_cus_added = function() {
                return this.elem_scope().find("#res-form-cus-added")
            }, t.prototype.elem_cus_added_items = function() {
                return this.elem_cus_added().children(".res-form-cus")
            }, t.prototype.elem_cus_clear = function() {
                return this.elem_scope().find("#res-form-cus-clear")
            }, t.prototype.elem_cus_results = function() {
                return this.elem_scope().find("#res-form-cus-results")
            }, t.prototype.elem_cus_sex_nil = function() {
                return this.elem_scope().find("#" + this.model + "_customer_sex_0")
            }, t.prototype.ajax_locked = !1, t.prototype.requery = !1, t.prototype.model = null, t.prototype.scope = null, t.prototype.simple = null, t.prototype.populate_existing_customers = function(t) {
                return $.each(t, function(t) {
                    return function(e, n) {
                        return t.add_customer(n, !1)
                    }
                }(this))
            }, t.prototype.create_dummy_field = function() {
                return this.elem_cus_added().append("<input name='" + this.model + "[customer_ids][]' type='hidden' value=''>")
            }, t.prototype.make_queryable = function() {
                return $.each(this.selectors(), function(t) {
                    return function(e, n) {
                        return $(n).on("blur, keyup", function() {
                            return t.ajax_query()
                        })
                    }
                }(this))
            }, t.prototype.get_params = function() {
                return $.map(this.selectors(), function() {
                    return function(t) {
                        return $(t).attr('data-query')+ "=" + $(t).val()
                    }
                }(this)).compact().join("-*-")
            }, t.prototype.make_clear_button = function() {
                return this.elem_cus_clear().click(function(t) {
                    return function() {
                        return t.clear()
                    }
                }(this))
            }, t.prototype.clear = function() {
                return this.elem_cus_results().html(""), $.each(this.selectors(), function() {
                    return function(t, e) {
                        return $(e).val("")
                    }
                }(this)), this.elem_cus_sex_nil().prop("checked", !0)
            }, t.prototype.ajax_query = function() {
                return this.get_params() ? this.ajax_locked ? this.requery = !0 : (this.ajax_locked = !0, $.ajax({
                    type: "GET",                    
                    url: TS.url_base() + "ajax/customers_json",
                    data: {
                        q: this.get_params()
                    },
                    dataType: "json",
                    contentType: "application/json",                    
                    success: function(t) {
                        return function(e) {                          
                            return t.update_results(e), t.ajax_locked = !1, t.do_requery()
                        }
                    }(this),
                    error: function(t) {
                        return function(e) {
                            return window.flash_alert(_.escape(e.responseText)), t.ajax_locked = !1, t.do_requery()
                        }
                    }(this)
                })) : void this.elem_cus_results().html("")
            }, t.prototype.do_requery = function() {
                return this.requery ? (this.requery = !1, this.ajax_query()) : void 0
            }, t.prototype.added_ids = function() {
                return $.map(this.elem_cus_added_items(), function() {
                    return function(t) {
                        return $(t).data("cus").id
                    }
                }(this))
            }, t.prototype.update_results = function(t) {
                return this.elem_cus_results().html(""), $.each(t.customers, function(t) {
                    return function(e, n) {
                        return t.added_ids().include(n.id) ? void 0 : t.append_result(n)
                    }
                }(this)), this.append_count(t.count, t.customers.length), resizeDialog(0)
            }, t.prototype.build_customer = function(t, e) {
                var n, i;
				/*ar strHtml = " <div class=\"res-form-cus\">\
									<div>\
										<div class=\"name\">"+_.escape(t.name)+"<\/div>\
										<div class=\"phone\" style=\"display: inline-block; padding-left: 50px;\">"+_.escape(t.phone)+"<\/div>\
										<div class=\"email\" style=\"display: inline-block; padding-left: 50px;\">"+_.escape(t.email)+"<\/div>\
									<\/div>\
							    <div\/>";*/
								
                return null == e && (e = !1), n = "<div class='res-form-cus'>", n += "<div>", n += "<div class='name' style='width:30%;'>" + _.escape(t.name) + "</div>", n += "<div class='phone' style='display: inline-block; width:30%;'>" + _.escape(t.phone) + "</div>", n += "<div class='email' style='display: inline-block; width:30%;'>" + _.escape(t.email) + "</div> ", n += "</div>", n += "</div>", i = $(n), i.data("cus", t), i.click(function(n) {
                    return function() {
						return e ? void 0 : n.add_customer(t)
                    }
                }(this)), e && i.addClass("alert alert-success"), i
            }, t.prototype.append_result = function(t) {
                return this.elem_cus_results().append(this.build_customer(t))
            }, t.prototype.append_count = function(t, e) {
                var n;
                return null != t && null != e && t > e && (n = "<div class='res-form-cus-total'>" + _.escape(I18n.t("actions.additional_results", {
                    count: t - e
                })) + "</div>"), this.elem_cus_results().append($(n))
            }, t.prototype.add_customer = function(t, e) {
				$('#reservation_customer_user_name').val(_.escape(t.name));
				$('#reservation_customer_phones_attributes_0_number').val(_.escape(t.phone));
				$('#reservation_customer_emails_attributes_0_email').val(_.escape(t.email));
				$('#res-form-cus-results').html('');
				resizeDialog(0);
                /*return null == e && (e = !0) ,this.elem_cus_added().append(this.build_customer(t, !0)), e ? this.clear() : void 0*/
            }, t
        }()
    }.call(this),
    function() {
        window.DailyMemo = function() {
            function t() {}
            return t.elem = null, t.prev_memo = null, t.prev_date = null, t.init = function() {
                return this.elem = $("#res-daily-memo"), this.prev_memo = this.elem_memo(), this.prev_date = this.elem_date(), this.listen_to_changes()
            }, t.elem_memo = function() {
                return this.elem.val()
            }, t.elem_date = function() {
                return this.elem.data("date")
            }, t.elem_hilite = function() {
                return this.elem_memo().length > 0 ? this.elem.addClass("hilite") : this.elem.removeClass("hilite")
            }, t.listen_to_changes = function() {
                return this.elem.on("change blur", t.upsert_memo), this.elem.on("keydown", function(e) {
                    return 13 === e.which ? (t.upsert_memo(), e.stopPropagation(), e.preventDefault()) : void 0
                })
            }, t.upsert_memo = function() {
                var e, n;
                return e = t.elem_date(), n = t.elem_memo(), e !== t.prev_date || n !== t.prev_memo ? (t.show_loader(), $.ajax({
                    type: "PUT",
                    url: TS.url_base() + ("daily_memos/" + _.escape(e)),
                    data: JSON.stringify({
                        daily_memo: {
                            memo: n
                        }
                    }),
                    contentType: "application/json",
                    success: t.hide_loader
                }), t.prev_date = e, t.prev_memo = n, t.elem_hilite()) : void 0
            }, t.show_loader = function() {
                return $("#res-daily-memo-icon").html('<span class="res-sts-mobi x16-loader"/>')
            }, t.hide_loader = function() {
                return $("#res-daily-memo-icon").html('<i class="fa fa-thumb-tack fa-lg"></i>')
            }, t
        }()
    }.call(this),
    function() {
        window.WidgetForm = function() {
            function t() {}
            return t.init = function() {
                return this.make_link_type_toggleable(), this.toggle_link_type()
            }, t.make_link_type_toggleable = function() {
                return $('input[name="widget[link_type]"]').change(function(t) {
                    return function() {
                        return t.toggle_link_type()
                    }
                }(this))
            }, t.toggle_link_type = function() {
                return $(".widget_chain").toggle($("#widget_link_type_chain").prop("checked")), $(".widget_shop").toggle($("#widget_link_type_shop").prop("checked"))
            }, t
        }()
    }.call(this),
    function() {
        window.WidgetGenerator = function() {
            function t() {}
            return t.init = function(t, e) {
                return this.env = null != t ? t : "production", this.domain = null != e ? e : null, this.bindEvents(), $("#widget-preview-h").length ? this.setOptions() : void 0
            }, t.bindEvents = function() {
                return $("#widget-generator a#generate").bind("click", function(t) {
                    return function() {
                        return t.setOptions()
                    }
                }(this))
            }, t.setOptions = function() {
                return this.options = {}, this.options.layout = $('input[name="widget[layout]"]:checked').val(), this.options.theme = $('input[name="widget[theme]"]:checked').val(), this.options.lang = $('input[name="widget[lang]"]:checked').val(), this.options.linktype = $('input[name="widget[link_type]"]:checked').val(), this.options.interval = $('input[name="widget[time_interval]"]:checked').val() || "15", this.options.autolang = $("#widget_autolang").is(":checked"), this.options.shop = $("#widget_shop").val(), this.options.chain = $("#widget_chain").val(), this.options.franchise = $("#widget-form").data("franchise-id"), this.generateCode()
            }, t.generateCode = function() {
                return $("#widget_code_section").val(this.jsString("production") + "\n\n" + this.codeString("production")), this.generateWidgetPreview(this.jsString(), this.codeString())
            }, t.jsString = function(t) {
                return $("<script type='text/javascript'></script>").attr("src", this.widgetUrl(t)).get(0).outerHTML
            }, t.codeString = function(t) {
                var e;
                switch (t || (t = this.env), e = $("<div></div>"), e.attr("data-autolang", this.options.autolang), e.attr("data-lang", this.options.lang), e.attr("data-layout", this.options.layout), e.attr("data-theme", this.options.theme), "15" !== this.options.interval && e.attr("data-time-interval", this.options.interval), "production" !== t && this.domain && e.attr("data-domain", this.domain), this.options.linktype) {
                    case "shop":
                        e.attr("data-shop-id", this.options.shop);
                        break;
                    case "chain":
                        e.attr("data-chain-id", this.options.chain);
                        break;
                    default:
                        e.attr("data-franchise-id", this.options.franchise)
                }
                return e.attr("id", "bc-widget"), e.get(0).outerHTML
            }, t.widgetUrl = function(t) {
                var e;
                switch (t || (t = this.env), e = "/bc_widget/bc_widget.js", t) {
                    case "production":
                        return "https://vesper-widget.s3.amazonaws.com" + e;
                    case "staging":
                        return "https://vesper-staging-widget.s3.amazonaws.com" + e;
                    default:
                        return "" + window.location.protocol + "//" + window.location.host + "/assets/widget/bc_widget.js"
                }
            }, t.generateWidgetPreview = function(t, e) {
                var n;
                return $("#widget-preview-h, #widget-preview-v").html(""), n = $("horizontal" === this.options.layout ? "#widget-preview-h" : "#widget-preview-v"), n.html($(e).attr("data-env", this.env)), $("body").append(t)
            }, t
        }()
    }.call(this),
    function() {
        window.ReservationForm = function() {
            function t() {}
            return t.init = function() {
                return this.buildSeatNumForAll(), this.bindStylistChangeEvent(), this.bindNestedFieldEvent()
            }, t.buildSeatNumForAll = function() {
                return $("select.stylist").each(function(e, n) {
                    return t.buildSeatNumForSelect($(n))
                })
            }, t.buildSeatNumForSelect = function(t) {
                var e, n, i, o, r;
                for (e = t.siblings("select.seat_num"), e.find("option").remove(':not([value=""])'), i = t.find("option:selected").data("num_seats"), e.find("option").remove(), n = o = 0, r = i - 1; r >= 0 ? r >= o : o >= r; n = r >= 0 ? ++o : --o) e.append($("<option>", {
                    val: n,
                    html: n + 1
                }));
                return e.val(e.data("selected-seat") || 0)
            }, t.bindStylistChangeEvent = function() {
                return $("#stylist-fields").on("change", "select.stylist", function() {
                    return t.buildSeatNumForSelect($(this))
                })
            }, t.bindNestedFieldEvent = function() {
                return $(document).on("nested:fieldAdded", function() {
                    return t.hideUnavailableStylists()
                })
            }, t.stylistIdsAvailable = function() {
                return $.map(window.stylistsAvailable, function(t) {
                    return t.id
                })
            }, t.showAllStylists = function() {
                return $('#stylist-fields select.stylist option[value!=""]').removeClass("hide")
            }, t.hideUnavailableStylists = function() {
                var e;
                return e = $('#stylist-fields select.stylist option[value!=""]'), $.each(e, function(e, n) {
                    var i;
                    return i = $(n).attr("value"), -1 === $.inArray(i, t.stylistIdsAvailable()) ? $(n).addClass("hide") : void 0
                })
            }, t.unselectHiddenStylists = function() {
                var t;
                return t = $("#stylist-fields select.stylist option:selected.hide"), $.each(t, function(t, e) {
                    return $(e).prop("selected", !1), $(e).parent("select").siblings().filter("select.seat_num").find("option").remove()
                })
            }, t.ajaxRefreshStylists = function(e) {
                return $.ajax({
                    url: TS.url_base() + "ajax/stylists_by_date",
                    type: "GET",
                    data: {
                        date: e
                    },
                    dataType: "json",
                    success: function(e) {
                        //return t.refreshStylistsList(e.stylists)
                    },
                    error: function(t) {
                        return alert(t.responseJSON.error)
                    }
                })
            }, t.refreshStylistsList = function(e) {
                return window.stylistsAvailable = e, t.showAllStylists(), t.hideUnavailableStylists(), t.unselectHiddenStylists()
            }, t
        }()
    }.call(this),
    

    function() {
        window.MenuItemsSortable = function() {
            function t() {}
            return t.init = function() {
                return this.make_menu_categories_movable(), this.make_menu_items_draggable()
            }, t.make_menu_categories_movable = function() {
                return $("#menu-items-list-table tr.menu-category-row").each(function(t) {
                    return function(e, n) {
                        var i;
                        return i = $(n), i.find(".move-up-btn").click(function() {
                            return t.ajax_move_menu_category(i.data("id"), "higher")
                        }), i.find(".move-down-btn").click(function() {
                            return t.ajax_move_menu_category(i.data("id"), "lower")
                        })
                    }
                }(this))
            }, t.make_menu_items_draggable = function() {
                return $("#menu-items-list-table tbody").sortable({
                    helper: this.fixHelper,
                    handle: ".tbl-sortable-handle",
                    stop: function(t) {
                        return function(e, n) {
                            var i, o, r;
                            return r = n.item, o = r.prevAll(".menu-category-row"), "position_online" === t.sort_order() ? i = r.index() : (o.length || (r.detach().insertAfter("tr.menu-category-row:first"), o = r.prevAll(".menu-category-row")), i = r.index() - o.index() - 1), t.ajax_update_menu_item_position(r.data("id"), o.data("id"), i)
                        }
                    }(this)
                }).disableSelection()
            }, t.fixHelper = function(t, e) {
                return e.children().each(function() {
                    return $(this).width($(this).width())
                }), e
            }, t.sort_order = function() {
                return $(".sort-order.active").data("sort-order")
            }, t.get_data_for_ajax = function(t, e) {
                var n, i;
                return i = "move_" + this.sort_order() + "_to", n = {
                    menu_item: {}
                }, n.menu_item[i] = t, e && (n.menu_item.menu_category_id = e), n
            }, t.ajax_move_menu_category = function(t, e) {
                return $.ajax({
                    type: "PUT",
                    url: TS.url_base() + ("menu_categories/" + _.escape(t)),
                    data: {
                        menu_category: {
                            move_position_to: e
                        }
                    },
                    dataType: "script"
                })
            }, t.ajax_update_menu_item_position = function(e, n, i) {
                return $.ajax({
                    type: "PUT",
                    url: TS.url_base() + ("menu_items/" + _.escape(e)),
                    data: JSON.stringify(t.get_data_for_ajax(i, n)),
                    dataType: "json",
                    contentType: "application/json",
                    error: function() {
                        return function(t) {
                            return window.flash_alert(t.responseText)
                        }
                    }(this)
                })
            }, t
        }()
    }.call(this),
    function() {
        window.SimpleSortable = function() {
            function t() {}
            return t.init = function() {
                return this.make_shops_draggable()
            }, t.init_shops = function() {
                return this.make_rows_draggable("#shops-list-table", "shop")
            }, t.init_reservation_flags = function() {
                return this.make_rows_draggable("#res-flags-list-table", "reservation_flag")
            }, t.make_rows_draggable = function(t, e) {
                return $("" + t + " tbody").sortable({
                    helper: this.fixHelper,
                    handle: ".tbl-sortable-handle",
                    stop: function(t) {
                        return function(n, i) {
                            return t.ajax_update_position(e, i.item.data("id"), i.item.index())
                        }
                    }(this)
                }).disableSelection()
            }, t.fixHelper = function(t, e) {
                return e.children().each(function() {
                    return $(this).width($(this).width())
                }), e
            }, t.ajax_update_position = function(t, e, n) {
                var i;
                return i = {}, i[t] = {
                    move_to: n
                }, $.ajax({
                    type: "PUT",
                    url: "" + TS.url_base() + this.pluralize(t) + "/" + _.escape(e),
                    data: JSON.stringify(i),
                    dataType: "json",
                    contentType: "application/json",
                    error: function() {
                        return function(t) {
                            return window.flash_alert(t.responseText)
                        }
                    }(this)
                })
            }, t.pluralize = function(t) {
                return "" + t + "s"
            }, t
        }()
    }.call(this),
    function() {
        window.FormHelper = function() {
            function t() {}
            return t.removeFieldSet = function(t) {
                return $(t).parentsUntil(".controls").remove()
            }, t
        }()
    }.call(this);

//////// booking form /////////
    
$(document).ready(function () {
    // add 2 div when refresh
    $('body').append("<div class=\"modal-backdrop fade in\" style=\"z-index: 1040; display: none\"></div>");
    $('body').append("<div class=\"modal-scrollable\" style=\"z-index: 1050; display: none\"><div class=\"loading-spinner x48-loader in\" style=\"z-index: 1050;\"></div></div>")
    $('body').append("<div id=\"dialog\" ></div>");
    $('body').append("<div id=\"dialog_seat\" ></div>");

    invoiceAcceptance = function (){
       /* if($("#nailist_id").val() * 1 == 0){
            alert('ネイリストを選択してください。'); 
            return false;
        }*/
        if (confirm("本当によろしいですか？")) {
            data ={
                order_id    :  $('#dialog-order-id').val(),
                controller  : 'orders',
                nailist_id  : $("#nailist_id").val(),
                start_date  : 1
            }
             $.ajax({
                type: "POST",
                url: baseUrl + 'ajax/stopwatchdialog' + "?" + $.param(data),
                data: data,
                success: function (response) {
                    $('#btnInvoiceAcceptance').attr('disabled', 'disabled');
                }
            });  
            return false;
        }
    }
    invoicePayment = function() {
        if($("#nailist_id").val() * 1 == 0){
            alert('ネイリストを選択してください。'); 
            return false;
        }
        if (confirm("本当によろしいですか？")) {
            data ={
                order_id    :  $('#dialog-order-id').val(),
                is_paid     : 1,
                controller  : 'orders',
                nailist_id  : $("#nailist_id").val()

            }
            $.ajax({
                type: "POST",
                url: baseUrl + 'ajax/stopwatchdialog'+ "?" + $.param(data),
                data: data,
                success: function (response) {
                    $('#btnInvoicePayment').attr('disabled', 'disabled');
                    $('#btnInvoiceAutoCancel').attr('disabled', 'disabled');
                    $('#btnInvoiceAcceptance').attr('disabled', 'disabled');
                }
            });  
            return false;
        }
    }
    invoiceAutoCancel= function() {
        if (confirm("本当によろしいですか？")) {
             data ={
                id      : $('#dialog-order-id').val(),
                value   : 1,
                controller   : 'orders'
            }
             $.ajax({
                type: "POST",
                url: baseUrl + 'ajax/autocancel',
                data: data,
                success: function (response) {
                   $('#btnInvoiceAutoCancel').attr('disabled', 'disabled');
                   $('#btnInvoiceAcceptance').attr('disabled', 'disabled');
                   $('#btnInvoicePayment').attr('disabled', 'disabled');
                }
            });  
            return false;
        }
    }
    invoiceCancel= function() {
        if (confirm("本当によろしいですか？")) {
             data ={
                id          : $('#dialog-order-id').val(),
                value       : 1,
                controller  : 'orders'
            }
             $.ajax({
                type: "POST",
                url: baseUrl + 'ajax/cancel',
                data: data,
                success: function (response) {              
                  if (!response) {
                        //alert('エラー発生されいますので、もう一度お願いします。');
                        $('#btnInvoiceCancel').attr('disabled', 'disabled');
                       $('#btnInvoiceAcceptance').attr('disabled', 'disabled');
                       $('#btnInvoicePayment').attr('disabled', 'disabled');
                    }
                    return false;
                }
            });  
            return false;
        } 
    }
    showBookingDialog = function(varUrl, varData, varMehthod){
        $(".modal-backdrop").css('display','');
        $(".modal-scrollable").css('display', '');
        $.ajax({
            type: varMehthod,
            url: varUrl,
            data: varData,
            success: function (response) {
                $(".modal-backdrop").css('display', '');
                $("#dialog").html(response);
                $("#dialog").dialog({
                    minWidth: 1200,
                    maxWidth: 1200,
                    minHeight: 550,
                    modal: true,
                    resizable: false,
                    close: function(event,ui){
                         $( this ).dialog( "close" );
                    }
                });
                $(".modal-scrollable").css('display', 'none');
                $(".ui-dialog-titlebar").hide();
                $(".ui-dialog").css("z-index","1050");
                $('.ui-dialog').css("top", "60px");
				$('.ui-dialog').css("height", "694px");
                resizeDialog(0);
				disableOptionService();
            }
        });
    }
	
	disableOptionService = function(){
		var services = $(".purpose_items").find(":hidden");
		if(services.length == 0) return false;
		for (var i=0; i< services.length; i ++){
			value = services[i].value;
			$("#slt_reservation_purpose option[value='"+value+"']").css('display','none');
		}
	}

    closeBookingDialog = function (dt, is_seat){
        $("#dialog").dialog('close');
        $(".modal-backdrop").css('display','none');  
        $('body').append("<div id=\"dialog\" ></div>");  
        $("#dialog").remove();
        var action_query = '';
		if(is_seat == 1){
			action_query = "?is_seat=1"
		}
		
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/loadcalendar'+action_query,
            data: {
				date : dt,
				controller : controller,
				action : action,
			},
            success: function (response) {
               $("#workspace").replaceWith(response);   
               $("ul#btn-tab li").removeClass('active');
              if(action == "seat"){
                    $("ul#btn-tab li").eq(0).addClass('active');
              }else {
                    $("ul#btn-tab li").eq(1).addClass('active');
              }
            }
        }); 
        // Call function check new order
         checkNewOrder(1,1,1)
    };

    resizeDialog = function(action){
        var defaultHeightDialog = 694;
		var heightDialogNoControl = 684;
        var resizePX = 35;
        var resizeWarningPX = 70;
		var resizeServicePX = 44;
		var resizeSearchPX = 27;
		var num = 0;
        var num1;
		var num2;
        if(action == 1){//delete nailist
            num1 = $('#stylist-fields .fields').not("[style='display: none;']").length - 1;
			num2 = $('#device-fields .fields').not("[style='display: none;']").length;
        }else if(action == 2){ //add nailist
            num1 = $('#stylist-fields .fields').not("[style='display: none;']").length + 1;
			num2 = $('#device-fields .fields').not("[style='display: none;']").length;
        }else if(action == 3){ //delete device
			num1 = $('#stylist-fields .fields').not("[style='display: none;']").length;
            num2 = $('#device-fields .fields').not("[style='display: none;']").length - 1;
        }else if(action == 4){ //add device
			num1 = $('#stylist-fields .fields').not("[style='display: none;']").length;
			num2 = $('#device-fields .fields').not("[style='display: none;']").length + 1;
        }else{//update
            num1 = $('#stylist-fields .fields').not("[style='display: none;']").length;
			num2 = $('#device-fields .fields').not("[style='display: none;']").length;
        }

		num = (num1 > num2) ? num1 : num2;
		
        if(num > 0) { 
           addHeigthDialog = (num-1)*resizePX;
           newHeighDialog = defaultHeightDialog + addHeigthDialog + 'px';
           parent.$('.ui-dialog').css("height", newHeighDialog);
        }else{
		   parent.$('.ui-dialog').css("height", heightDialogNoControl);
		}
		//in case of service happend
		var num_service = $(".purpose_items").children().length;
		if(num_service > 0){
		   addHeigthDialog = (num_service)*resizeServicePX;
		   curDialogHeight = parent.$('.ui-dialog').height();
           newHeighDialog = curDialogHeight + addHeigthDialog + 'px';
           parent.$('.ui-dialog').css("height", newHeighDialog);
		}
		
		// in case of search user information
		var num_search = $(".res-form-cus").length;
		if(num_search > 0){
		   addHeigthDialog = (num_search)*resizeSearchPX;
		   curDialogHeight = parent.$('.ui-dialog').height();
           newHeighDialog = curDialogHeight + addHeigthDialog + 'px';
           parent.$('.ui-dialog').css("height", newHeighDialog);
		}

        //in case of warning message happend
        w_future = $(".reservation_start_at_warning_future").not("[style='display: none;']").length;
        w_past = $(".reservation_start_at_warning_past").not("[style='display: none;']").length;
        if((w_future == 1 && w_past == 0) || (w_future == 0 && w_past == 1)){
            curDialogHeight = parent.$('.ui-dialog').height();
            parent.$('.ui-dialog').css("height", curDialogHeight + resizeWarningPX + 'px');
        }
		
    }
    
    editBooking = function(order_id){		
        var url = baseUrl + "ajax/bookingdialog?order_id=" + order_id;
        var data = {};
        var method = 'GET';   
        showBookingDialog(url, data, method);
    };
    
	optionDialog = function(order_id) {     
		$(".modal-backdrop").css('display','');
        $(".modal-scrollable").css('display', '');       
		var action_query = '';
		if(action == "seat"){
			action_query = "?is_seat=1"
		}
        // Load seatbooking dialog
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/seatbookingdialog',
            data: { order_id: order_id},
            success: function (response) {
                if($("#option-dialog").length > 0)
                     $("#option-dialog").remove();
                $('body').append(response);
                $('#dialog-order-id').val(order_id);  
                $("#dialog-timedial").val($("#timedial").val());
                 // Call dialog and show
                $("#option-dialog").dialog({
                    width: 965,  
                    modal: true,
                    resizable: false,
                    show: { effect: "fold ", duration: 200 },
                    dialogClass: "no-close",
                    buttons: [
                        {
                            text: "閉じる",
                            class: 'btn',
                            click: function() {
                                $( this ).dialog( "close" );
								$(".modal-backdrop").css('display','none');  
                                // Load calendar again
                                var data = $("#dialog-timedial").val() !== "" ? $("#dialog-timedial").val() : null;//getCookie("cookie_user");
                                $.ajax({
                                    type: "POST",
                                    url: baseUrl + 'ajax/loadcalendar'+action_query,
                                    data: {
										date : data,
										controller: controller,
										action: action
									},
                                    success: function (response) {
                                       $("#workspace").replaceWith(response);   
                                       $("ul#btn-tab li").removeClass('active');
                                      if(action == "seat"){
                                            $("ul#btn-tab li").eq(0).addClass('active');
                                      }else {
                                            $("ul#btn-tab li").eq(1).addClass('active');
                                      }
                                    }
                                }); 
                               // call function check_new_order()
                                checkNewOrder(1,1,1)                                
                            }           
                        }
                    ],
                   create: function( event, ui ){
                        // code here
                    },
                    open: function( event, ui ) {
                        $(this).css({'height':'auto !important'});
                        $( this ).closest('div.ui-dialog').css({'z-index':305,"top":"60px"}).addClass('dialogBox');
                        $( this ).closest('div.ui-dialog').find('div.ui-dialog-buttonset').removeClass('ui-dialog-buttonset').addClass('text-center btnCloseDialog');
                    }
                });     
				$(".modal-scrollable").css('display', 'none');
				$(".ui-dialog").css("z-index","1050");
                $(".ui-dialog-titlebar").hide();    
            }
        }); 
	}    
	
	 optionEdit = function(){    
		$("#option-dialog").dialog('close');
        var order_id = $('#dialog-order-id').val();
		var action_query = '';
		if(action == "seat"){
			action_query = "&is_seat=1"
		}
        var url = baseUrl + ("ajax/bookingdialog?order_id=" + order_id + action_query);		
        var data = {};
        var method = 'GET';   
        showBookingDialog(url, data, method);
        return false;
    }
    optionMediacal = function (){    
    	$("#option-dialog").dialog('close');
        var order_id = $('#dialog-order-id').val();
        
        medicalIdList = new cookieList("medicalIdList");
        
        var medicalids = medicalIdList.items();

        if (medicalids.length > 0 && medicalids.indexOf(order_id) != -1) {
            alert('カルテは開いています');
        } else {
            //$.cookie("medicalid", order_id, { path: "/" });
            //medicalIdList.add(order_id);
            var url = baseUrl + 'medicalchart/'+order_id;
            var win = window.open(url, '_blank');
            win.focus();
            
        }
        $(".modal-backdrop").css('display','none'); 
        return false;        
    }
	
	optionCopy = function(){    
		$("#option-dialog").dialog('close');
        var order_id = $('#dialog-order-id').val();
		var action_query = '';
		if(action == "seat"){
			action_query = "&is_seat=1"
		}
        var url = baseUrl + ("ajax/bookingdialog?order_id=" + order_id + "&is_copy=1" + action_query);		
        var data = {};
        var method = 'GET';   
        $.ajax({
            type: method,
            url: url,
            data: data,
            success: function (response) {
                //user = JSON.parse(response);
				setCookie("cookie_user",response,1);
				alert("顧客の情報がコピーされました。");
				$(".modal-backdrop").css('display','none'); 
            }
        });
        return false;
    }
	
	durationConfirm = function(dt, url) {
		if(confirm('サービスの目安時間合計よりも短い滞在時間に変更しようとしています。よろしいでしょうか？')) {
            $.ajax({
                type: "PUT",
                url: url,
                data: dt,
				dataType: "script"                
            });
        }
	}
});
