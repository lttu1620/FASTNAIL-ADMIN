   
$(document).ready(function () {
    //init data for stylist and orders select box
    var busy_services = [];
    var tmpDataStylist = $('#stylist-fields').find('.tmpfields').html();
    var tmpDataDevice = $('#device-fields').find('.tmpfields').html();
    var tmpDataOrders = $('#orders-fields').find('.tmpfields').html();
    $('#stylist_seats_fields_blueprint').attr('data-blueprint', '<div class="fields">' + tmpDataStylist + '</div>');
    $('#device_seats_fields_blueprint').attr('data-blueprint', '<div class="fields device-new_device_seats">' + tmpDataDevice + '</div>');
    $('#orders_fields_blueprint').attr('data-blueprint', '<div class="fields">' + tmpDataOrders + '</div>');
    var data = getCookie("cookie_user");
    if(data){
        $("#res-form-cus-paste").css("display",'');
    }

    $('#commit').click(function () {
        dt = $("#new_reservation").serialize();
        var cur_btn_name = $(this).val();
        $(this).val('お待ちください・・・');
        $(this).addClass("disabled");
        $.ajax({
            type: 'POST',    
            url :   baseUrl +"ajax/bookingaddupdate", 
            data: dt,
            dataType: 'json',
            success: function(data){
            	$(this).val(cur_btn_name);
                $(this).removeClass("disabled");
                if(data.message == 'OK'){
                    if($("[name='reservation[is_seat]']").length > 0){
                    	closeBookingDialog(data.request_date, 1);
                    }else{
                    	closeBookingDialog(data.request_date, 0);
                    }
                    
                }else if(data.message == 'Duplicated'){
                    $('#messages').alertFade("<div id=\'messages\'>\n<div class=\'alert alert-error float\'>\n<a class=\'close\' data-dismiss=\'alert\'>×<\/a>\n<div id=\"flash_error\">既にある予約と、時間がかぶっています<\/div>\n<\/div>\n<\/div>\n");
                }else if(data.message == 'OutOfTimeRange'){
                    $('#messages').alertFade("<div id=\'messages\'>\n<div class=\'alert alert-error float\'>\n<a class=\'close\' data-dismiss=\'alert\'>×<\/a>\n<div id=\"flash_error\">営業時間外の予約です<\/div>\n<\/div>\n<\/div>\n");
                }else if(data.message == 'Validate'){
                    errStr = '';
                    for (errMsg in data.request_date) {
                        errStr += data.request_date[errMsg] + '<br />';
                    }
                    $('#messages').alertFade("<div id=\'messages\'>\n\n\
                                                <div class=\'alert alert-error float\'>\n\n\
                                                    <a class=\'close\' data-dismiss=\'alert\'>×<\/a>\n\n\
                                                        <div id=\"flash_error\">"+errStr+"<\/div>\n\n\
                                                <\/div>\n\n\
                                            <\/div>\n");
                }else{
                    alert(data.message);
                }
                $('#commit').val(cur_btn_name);
                $('#commit').removeClass("disabled");
            }
        });
    });
    $('#delete').click(function () {
        var order_id = $(this).attr('data-order-id');
        var start_timestamp = $(this).attr('data-start-timestamp');
        if (confirm('本当によろしいですか？') == false) {
            return false;
        }
        $(this).addClass('disabled');
        $.ajax({
            type : 'POST',
            url: baseUrl + "ajax/bookingdelete",
            data: {
                    order_id : order_id, 
                    request_date : start_timestamp
                  },
            dataType: 'json',
            success: function(data){
            	$(this).removeClass('disabled');
                if(data.message == 'OK'){
                    if($("[name='reservation[is_seat]']").length > 0){
                    	closeBookingDialog(data.request_date, 1);
                    }else{
                    	closeBookingDialog(data.request_date, 0);
                    }
                }else{
                    alert(data.message);
                }
                $('#delete').removeClass('disabled'); 
            }
        });
    });
    
    $('#reservation_customer_sex_1').click(function(){
        $(this).parent().addClass('active');
        $(this).attr( "checked","checked" );
        $('#reservation_customer_sex_2').parent().removeClass('active');
        $('#reservation_customer_sex_2').removeAttr("checked");
    });
    
    $('#reservation_customer_sex_2').click(function(){
        $(this).parent().addClass('active');
        $(this).attr( "checked","checked" );
        $('#reservation_customer_sex_1').parent().removeClass('active');
        $('#reservation_customer_sex_1').removeAttr("checked");
    });
    
    $("#btn_close_dialog").click(function(){
        date = $(this).attr('data-request-date');
        if($("[name='reservation[is_seat]']").length > 0){
        	closeBookingDialog(date,1);
        }else{
        	closeBookingDialog(date,0);
        }
        return false;
    });
    
    $("#res-form-cus-paste").click(function(){
		var data = getCookie("cookie_user");
		var user = JSON.parse(data);
		$("[name='reservation[customer][user_name]']").val(user.name);
		$("[name='reservation[customer][phone]']").val(user.phone);
		$("[name='reservation[customer][email]']").val(user.email);
		if(user.sex == 1){
			$("#reservation_customer_sex_1").parent().addClass("active");
			$( "#reservation_customer_sex_1" ).attr( "checked","checked" );
			$("#reservation_customer_sex_2").parent().removeClass("active");
		}else{
			$("#reservation_customer_sex_2").parent().addClass("active");
			$( "#reservation_customer_sex_2" ).attr( "checked","checked" );
			$("#reservation_customer_sex_1").parent().removeClass("active");
		}
		return false;
    });
    
    //customize dialog
    $('#btn_add_nested_fields').click(function(){
        resizeDialog(2);
    });

	// add device
    $('#btn_add_nested_device_fields').click(function(){
    	resizeDialog(4);
    	cur_num = $("#device-fields").children('.fields').length;
        add_device = $('#device_seats_fields_blueprint').attr('data-blueprint');
    	add_device = add_device.replace(/new_device_seats/g, cur_num);
    	$("#device-fields").append(add_device);
    	
    });

	// remove device
    removeDevice = function(num){
    	resizeDialog(3);
        //$(".device-"+num).css("display", "none");
        //$("#reservation_device_seats_attributes_"+num+"__destroy").val(1);
        $(".device-"+num).remove();
    }

    //Phone
    /*$("#reservation_customer_phones_attributes_0_number").inputmask();*/

    // Handler input for JP Phone
    // Handler evet for input Phone type
    $('.phoneInput').keyup(function(e) {
            var errorId = $(this).data('errorid');
            if (typeof errorId == 'undefined' || $('#'+errorId).length == 0) {
                return false;
            }  
            var phone = $(this).val();          
            if (checkJPPhone2(phone) == false && phone.length > 0) {
                //$('#'+errorId).show();
                $('#messages').alertFade("<div id=\'messages\'>\n<div class=\'alert alert-error float\'>\n<a class=\'close\' data-dismiss=\'alert\'>×<\/a>\n<div id=\"flash_error\">電話番号は正しくはありません<\/div>\n<\/div>\n<\/div>\n");
            } else {
            	$('#messages').alertFade("<div id=\'messages\'><\/div>");
            }
        });  
        $(".phoneInput").keydown(function(e) {      
            //console.log(e.keyCode);    

            // Allow: backspace, delete, tab, escape, enter, subtract and
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110,173]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey)) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey)) ||
                //Japanese characters
                (e.keyCode == 229) || 
                //- characters
                (e.keyCode == 189) || 
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $('.phoneInput').bind("paste", function(e) {
            e.preventDefault();
        });
        
        $('#slt_reservation_purpose').change(function(){
			var selected_item = $(this).find(":selected");
			var value = selected_item.val();
			if(value == 0) return false;

			// prepare data for add new service item
			var name = selected_item.text();
			var time_stamp = $('option:selected', this).attr('data-service-time');
			
			var dvice_type =  $('option:selected', this).attr('data-device-type');
			if(time_stamp > 0){
				time = time_stamp/60;
			}else{
				time = '00';
			}
		    var newPurpose = "<div class='row service-"+value+"'> \
								<div class='col-sm-7'> \
									<div class='control-group string optional reservation_purpose'> \
										<div class='controls'> \
											<div> \
												<label style='width: 216px;'>"+name+"<\/label> \
												<span style='padding-right: 10px;'>"+time+"分<\/span> \
												<input type='hidden' name='reservation[services][ids][]'  value='"+value+"' \/> \
												<input type='hidden' name='reservation[services][times][]'  value='"+time_stamp+"' \/> \
												<a href='javascript:void(0)' class='btn btn-small btn-danger remove_purpose' onclick='removeService("+value+","+time_stamp+")'> \
													<span height='16' width='16'><\/span> 削除する \
												<\/a> \
											<\/div> \
										<\/div> \
									<\/div> \
								<\/div> \
							<\/div> ";

		    // add service item
			$("#slt_reservation_purpose option[value='0']").prop('selected', true);								
			$(".purpose_items").append(newPurpose);
			selected_item.css('display', 'none');
			$(".purpose_items").css('display','');
			resizeDialog(0);

			// prepare data for updating new time duration
			var time_duration = $("#reservation_duration").val();
			var time_estimate = $("#reservation_estimate_duration").attr("data-estimate-duration");
			var new_time_duration = parseInt(time_stamp) + parseInt(time_duration);
			var new_time_estimate = parseInt(time_stamp) + parseInt(time_estimate);

			//update new time duration
			$("#reservation_duration option[value='"+new_time_duration+"']").prop('selected', true);
			$("[name='reservation[duration_disable]']").val(new_time_duration);

			$("#reservation_estimate_duration").val(reservation_duration[new_time_estimate]);
			$("#reservation_estimate_duration").attr("data-estimate-duration",new_time_estimate);
			$("[name='reservation[estimate_duration]']").val(new_time_estimate);
            
            
			//add service's device if having in case of (type=1 or 2)
            if(dvice_type == 0){
                return false;
            }
            
			var order_end_date_new = order_start_date_new + new_time_duration;
			$.ajax({
	            type : 'POST',
	            url: baseUrl + "ajax/checkservicedevice",
	            data: {
		            	services: {
		            		check_avalable: 1,
		            		device_type: dvice_type,
		            		order_start_date: order_start_date_new ,
		            		order_end_date: order_end_date_new,
		            		shop_id: shopId ,
		            		not_in_order_id:  not_in_order_id		
		                }
	                  },
	            dataType: 'json',
	            success: function(response){
		            if(response.message == 'OK'){
	            		cur_num = $("#device-fields").children('.fields').length;
						//check if exist device then do not add more
						if($("#device-fields").find("option[value='d_"+response.device.id+"']").is(":selected") === true){
//                            var element = $("#device-fields").find("option[value='d_"+response.device.id+"']").filter(":selected").first();
//                            if(element.length > 0){
//                                var e_parent = element.parent().parent(); //<div class='fields'>
//                                e_parent.css('display', '');
//                                e_parent.find("input[type='hidden']").val('false');
//                            }
                            return false;
						}
			            device_id = '"d_'+response.device.id+'"';
		            	add_device = $('#device_seats_fields_blueprint').attr('data-blueprint');
		            	add_device = add_device.replace(/new_device_seats/g, cur_num);
		            	add_device = add_device.replace(device_id, device_id + " selected");
		            	$("#device-fields").append(add_device);
	           		 	resizeDialog(0);
		            }else if(response.message == 'Busy'){
			            //if service is busy
                        $('#messages').alertFade("<div id=\'messages\'>\n<div class=\'alert alert-error float\'>\n<a class=\'close\' data-dismiss=\'alert\'>×<\/a>\n<div id=\"flash_error\">デバイスが埋まっているため、予約が作成できません。<\/div>\n<\/div>\n<\/div>\n");
                        $('#commit').addClass("disabled");
                        busy_services.push(value);
			        }
	         	 }
			});		
		
        });
		
		 removeService = function(value, time_stamp){
            //hide error message 
            $('#messages').alertFade("<div id=\'messages\'><\/div>");
			//remove service item
			$('.service-'+value).remove();
			$("#slt_reservation_purpose option[value='"+value+"']").css('display','');
			$("#slt_reservation_purpose option[value='0']").prop('selected', true);	
			resizeDialog(0);	
            
			// prepare data for updating new time duration
			var time_duration = $("#reservation_duration").val();
			var time_estimate = $("#reservation_estimate_duration").attr("data-estimate-duration");
			var new_time_duration =  parseInt(time_duration) - parseInt(time_stamp);
			new_time_duration = new_time_duration > 0 ? new_time_duration : 0;
			var new_time_estimate =  parseInt(time_estimate) - parseInt(time_stamp);
			new_time_estimate = new_time_estimate > 0 ? new_time_estimate : 0;
			
			//update new time duration
			$("#reservation_duration option[value='"+new_time_duration+"']").prop('selected', true);
			$("[name='reservation[duration_disable]']").val(new_time_duration);
			
			$("#reservation_estimate_duration").val(reservation_duration[new_time_estimate]);
			$("#reservation_estimate_duration").attr("data-estimate-duration",new_time_estimate);
			$("[name='reservation[estimate_duration]']").val(new_time_estimate);
            
            //update status for commit button
            if(busy_services.length > 0){
                //console.log('value='+value);
                var idx = busy_services.indexOf(value.toString());
                //console.log('idex='+idx);
                if(idx != -1){
                    busy_services.splice(idx, 1);
                }
                if(busy_services.length == 0){
                  $('#commit').removeClass("disabled");
                }
            }
            //console.debug(busy_services);
		}
});