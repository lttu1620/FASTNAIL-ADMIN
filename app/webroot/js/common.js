function getDateTimeZone(offset) {
    // create Date object for current location
    d = new Date();

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);

    // create new Date object for different city
    // using supplied offset
    nd = new Date(utc + (3600000*offset));

    // return date
    return nd;
}
 
function setCookie(cname,cvalue,exdays) {
        //var d = new Date();
		var d= getDateTimeZone("+9");
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname+"="+cvalue+"; "+expires;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

function paging(url, div) {
    $.get(url, {}, function (result) {
        if (result) {
            $('#' + div).html(result);
        }
    });
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}
function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}
function isValidDate(dateStr, format) {
    if (format == null) {
        format = "MDY";
    }
    format = format.toUpperCase();
    if (format.length != 3) {
        format = "MDY";
    }
    if ((format.indexOf("M") == -1) || (format.indexOf("D") == -1) || (format.indexOf("Y") == -1)) {
        format = "MDY";
    }
    if (format.substring(0, 1) == "Y") {
        var reg1 = /^\d{2}(\-|\/|\.)\d{1,2}\1\d{1,2}$/
        var reg2 = /^\d{4}(\-|\/|\.)\d{1,2}\1\d{1,2}$/
    } else if (format.substring(1, 2) == "Y") {
        var reg1 = /^\d{1,2}(\-|\/|\.)\d{2}\1\d{1,2}$/
        var reg2 = /^\d{1,2}(\-|\/|\.)\d{4}\1\d{1,2}$/
    } else {
        var reg1 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{2}$/
        var reg2 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{4}$/
    }
    if ((reg1.test(dateStr) == false) && (reg2.test(dateStr) == false)) {
        return false;
    }
    var parts = dateStr.split(RegExp.$1);
    if (format.substring(0, 1) == "M") {
        var mm = parts[0];
    } else if (format.substring(1, 2) == "M") {
        var mm = parts[1];
    } else {
        var mm = parts[2];
    }
    if (format.substring(0, 1) == "D") {
        var dd = parts[0];
    } else if (format.substring(1, 2) == "D") {
        var dd = parts[1];
    } else {
        var dd = parts[2];
    }
    if (format.substring(0, 1) == "Y") {
        var yy = parts[0];
    } else if (format.substring(1, 2) == "Y") {
        var yy = parts[1];
    } else {
        var yy = parts[2];
    }
    if (parseFloat(yy) <= 50) {
        yy = (parseFloat(yy) + 2000).toString();
    }
    if (parseFloat(yy) <= 99) {
        yy = (parseFloat(yy) + 1900).toString();
    }
    var dt = new Date(parseFloat(yy), parseFloat(mm) - 1, parseFloat(dd), 0, 0, 0, 0);
    if (parseFloat(dd) != dt.getDate()) {
        return false;
    }
    if (parseFloat(mm) - 1 != dt.getMonth()) {
        return false;
    }
    return true;
}
function isPhoneNumber(str) {
    var alphaExp = /^((\(\+?84\)[\-\.\s]?)|(\+?84[\-\.\s]?)|(0))((\d{3}[\-\.\s]?\d{6})|(\d{2}[\-\.\s]?\d{8}))$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isAlphabet(str) {
    var alphaExp = /^[a-zA-Z]+$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isAlphabetAndNumber(str) {
    var alphaExp = /^[a-zA-Z0-9_]+$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isUserName(str) {
    var alphaExp = /^[a-zA-Z0-9_.\-]+$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isEmail(email) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(email);
}
;
function isNumber(str) {
    var alphaExp = /^[0-9]+$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function strip_tags(str) {
    str = str.replace(/&nbsp;/g, '');
    str = jQuery.trim(str);
    allowed_tags = '';
    var key = '', allowed = false;
    var matches = [];
    var allowed_array = [];
    var allowed_tag = '';
    var i = 0;
    var k = '';
    var html = '';
    var replacer = function (search, replace, str) {
        return str.split(search).join(replace);
    };
    // Build allowes tags associative array
    if (allowed_tags) {
        allowed_array = allowed_tags.match(/([a-zA-Z]+)/gi);
    }
    str += '';
    // Match tags
    matches = str.match(/(<\/?[\S][^>]*>)/gi);
    // Go through all HTML tags
    for (key in matches) {
        if (isNaN(key)) {
// IE7 Hack
            continue;
        }
// Save HTML tag
        html = matches[key].toString();
        // Is tag not in allowed list? Remove from str!
        allowed = false;
        // Go through all allowed tags
        for (k in allowed_array) {
// Init
            allowed_tag = allowed_array[k];
            i = -1;
            if (i != 0) {
                i = html.toLowerCase().indexOf('<' + allowed_tag + '>');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('<' + allowed_tag + ' ');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('</' + allowed_tag);
            }

// Determine
            if (i == 0) {
                allowed = true;
                break;
            }
        }
        if (!allowed) {
            str = replacer(html, "", str); // Custom replace. No regexing
        }
    }
    return str;
}
function checkAll(strItemName, value) {
    var x = document.getElementsByName(strItemName);
    for (var i = 0; i < x.length; i++) {
        if (value == 1 && !x[i].disabled) {
            if (!x[i].checked)
                x[i].checked = 'checked';
        } else {
            if (x[i].checked)
                x[i].checked = '';
        }
    }
}
function getItemsChecked(strItemName, sep) {
    var x = document.getElementsByName(strItemName);
    var p = "";
    for (var i = 0; i < x.length; i++) {
        if (x[i].checked) {
            p += x[i].value + sep;
        }
    }
    var result = (p != '' ? p.substr(0, p.length - 1) : '');
    return result;
}
deleteItem = function (id) {
    if (!id || $("#actionId").length <= 0) {
        return false;
    }
    if (confirm('Are you sure') == false) {
        return false;
    }
    $("#action").val('delete');
    $("#actionId").val(id);
    $("#dataForm").submit();
}
disableItem = function (id, disable) {
    if (typeof disable == "undefined") {
        return false;
    }
    if (!id || $("#actionId").length <= 0) {
        return false;
    }
    if (confirm('Are you sure') == false) {
        return false;
    }
    $("#action").val(disable == 0 ? 'disable' : 'enable');
    $("#actionId").val(id);
    $("#dataForm").submit();
}
enableItem = function (id) {
    if (confirm('Are you sure') == false && $("#item_" + id).length > 0) {
        return false;
    }
    $("#action").val('enable');
    $("#actionId").val(id);
    $("#dataForm").submit();
}
back = function () {
    if (referer.indexOf(url) === -1) {
        location.href = referer;
    } else {
        location.href = '/' + controller;
    }
    return false;
}

selectChangeUniversity = function (_element, _campus, _deparment, _mode)
{
    // Remove all old elementnot first element;
    $('#' + _campus + ' :not(:eq(0)), #' + _deparment + " :not(:eq(0))").remove();
    id = _element.value;
    // ajax

    loading_ajax('/Ajax/university/' + id,
            "",
            function () {// function on before send
            },
            function (msg) {// action when successfull
                if (_campus !== undefined && _campus !== "")
                {
                    var _lstCampus = msg.Campus;
                    $.each(_lstCampus, function (idx, obj) {
                        $('#' + _campus).append($("<option>").val(obj.id).append(obj.value));
                    });
                }
                // 
                if (_deparment !== undefined && _deparment !== "")
                {
                    var _lstDepartment = msg.Department;
                    $.each(_lstDepartment, function (idx, obj) {
                        $('#' + _deparment).append($("<option>").val(obj.id).append(obj.value));
                    });
                }
            },
            function () { // function on error
            }, true
            );
    return false;
}

likeComment = function (commentId, userId, feedId, is_like, page, limit)
{
    $.ajax({
        type: "POST",
        url: '/Ajax/likecomment/' + commentId + "/" + userId,
        data: {page: page, limit: limit, status: is_like, feedId: feedId},
        success: function (msg) {
            $('#chat-box').html(msg);
        }
    });
    return false;
}
onblurUniversity = function (_element, _refElementID)
{
    if (_element.value.trim() == "")
        $('#' + _refElementID).val('');
    console.log($('#' + _refElementID).val());
    return false;
}
function ChangeDataType(obj) {
    $('.DivEffect').switchClass('Show', 'Hidden', 500, 'easeInOutQuad'); //.removeClass('Show').addClass('Hidden');
    if (obj.value != "")
        $(".div" + obj.value).switchClass('Hidden', 'Show', 500, 'easeInOutQuad'); //.addClass('Show');
    else
        $(".divnumber").switchClass('Hidden', 'Show', 500, 'easeInOutQuad');
    return false;
}
var base_ajax_time_out = 120000;
var base_ajax_time_out_message = "システムタイムアウトを発生しました。";
var base_url_system_error = "/Error/400";
// Common Ajax function
function loading_ajax(url, data_post, before_send, on_success, on_error, is_async) {
    if (is_async == undefined || is_async == null) {
        is_async = true;
    }
    $.ajax({
        url: url,
        data: data_post,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        async: is_async,
        timeout: base_ajax_time_out,
        beforeSend: function (xhr) {
            before_send();
        },
        success: function (msg) {
            if (msg != null && msg != "") {
                on_success(msg)
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            if (xhr.status == 0) {
                return false;
            }
            if (on_error == null || on_error == undefined) {
                return false;
            }
            if (textStatus === "timeout") {
                alert(base_ajax_time_out_message);
                return false;
            }
            //window.location.href = base_url_system_error;
        }
    });
}
back = function () {
    location.href = baseUrl + controller;
    return false;
}
$(function () {
    $(".btn-disable").click(function () {
        var items = getItemsChecked('items[]', ',');
        if (items == '') {
            alert('Please select a item');
            return false;
        }
        $("#action").val('disable');
        return true;
    });
    $(".btn-enable").click(function () {
        var items = getItemsChecked('items[]', ',');
        if (items == '') {
            alert('Please select a item');
            return false;
        }
        $("#action").val('enable');
        return true;
    });
    $(".btn-select-kill").click(function () {
        var items = getItemsChecked('items[]', ',');
        if (items == '') {
            alert('Please select a item');
            return false;
        }
        if (confirm('Are you sure?') == false) {
            return false;
        }
        location.href = baseUrl + controller + '/ps/id=' + items;
        return false;
    });
    $(".btn-search").click(function () {
        var action = $("#searchForm").attr('action');
    });
    $(".btn-addnew").click(function () {
        location.href = baseUrl + controller + '/update';
        return false;
    });
    $("#fblogin").click(function () {
        FB.login(function (response) {
            if (response.authResponse) {
                FB.api('/me', function (response) {
                    var params = response;
                    var url = '/ajax/fblogin';
                    $.ajax({
                        cache: false,
                        async: true,
                        data: params,
                        url: url,
                        success: function (redirectUrl) {
                            if (redirectUrl) {
                                location.href = redirectUrl;
                            }
                        }
                    });
                });
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {scope: 'email,user_likes'});
    });
    if ($('.form-body .js-thumb').length > 0) {
        $('.form-body .js-thumb').lightBox();
    }
    var collapse = getCookie('collapse');
    if (collapse == '') {
        collapse = '1';
    }
    if (collapse == '0') {
        $('.search-body').show();
        if ($(".search-collapse").length > 0) {
            //$("[data-widget='collapse']").click();
            $(".search-collapse").click();
        }
    }
    $(".search-collapse").click(function () {
        if (collapse == '1') {
            collapse = '0';
        } else {
            collapse = '1';
        }
        setCookie('collapse', collapse, 1);
        return true;
    });
    $(".dialog").click(function () {
        $('body').append("<div id=\"dialog\"></div>");
        var dialogTitle = $(this).attr('alt');
        var url = $(this).attr('href');
        var data = {};
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (response) {
                $("#dialog").html(response);
                $("#dialog").dialog({
                    title: dialogTitle,
                    minWidth: 600,
                    minHeight: 200,
                    modal: true,
                    draggable: true,
                    /*
                     buttons: [{
                     text: "Close",
                     click: function () {
                     $(this).dialog("close");
                     }
                     }] */
                });
            }
        });
        return false;
    });
    $(".btn-statistic").click(function () {
        var items = getItemsChecked('items[]', ',');
        if (items == '') {
            alert('Please select a item');
            return false;
        }
        return true;
    });
   
    $('.toggle').show();
    $(".toggle-event").change(function () {
        var data_controller = controller;
        
        //get controller in case there are multi-controllers on a screen
        var classList = $(this).attr('class').split(/\s+/);
        if (classList.length == 2) {
            data_controller = classList[1];
        }
        
        var id = $(this).val();
        var disable = $(this).prop('checked') ? 0 : 1;
        var data = {
            controller: data_controller,
            action: action,
            id: id,
            disable: disable
        };
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/disable',
            data: data,
            success: function (response) {
                if (response) {
                    alert(response);
                }
            }
        });
        return false;
    });

    //admin | approved toggle click
    $(".toggle input.admin,input.approved,input.plus,input.cancel").change(function () {
        var _class = $(this).attr('class');
        var id = $(this).val();
        var _value = $(this).prop('checked') ? 1 : 0;

        var data = {
            controller: controller,
            id: id,
            value: _value,
        };
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/' + _class,
            data: data,
            success: function (response) {
                if (response) {
                    alert(response);
                }
            }
        });
        return false;
    });

    // Handler error load image 
    $.each($("div.form-group ").find('img'), function (_index, _object) {
        // check load image error
        $("<img/>")
                .error(function () {
                    console.log($(_object).attr("src") + ": error loading image");
                    $(_object).closest('div').css({'max-width': ''}).html($('<span>').css({'color': 'red'}).append($(_object).attr("src") + ': image url not exists.'));
                })
                .attr("src", $(_object).attr("src"))
                ;
    });

});
changeAdminApprovedItem = function (id, value, option) {
    if (typeof value == "undefined") {
        return false;
    }
    if (!id || $("#actionId").length <= 0) {
        return false;
    }
    if (confirm('Are you sure') == false) {
        return false;
    }
    if (option == 0) {
        $("#action2").val(value == 0 ? 'is_admin' : 'user');
    } else if (option == 1) {
        $("#action2").val(value == 0 ? 'is_approved' : 'user');
    } else {
        return false;
    }
    $("#actionId2").val(id);
    $("#optionId2").val(option);
    $("#dataForm").submit();
}
DrawChart = function (_element, _data, _xkey, _ykeys, _labels, _lineColors, _type, _mode) {
    if (_type === undefined || _type == null)
    {
        _type = 'line-chart';
    }
// Reset html of element before Draw chart
    $('#' + _element).html();
    switch (_type)
    {
        case "bar_chart":
            var bar = new Morris.Bar({
                element: _element,
                resize: false,
                data: _data,
                barColors: _lineColors,
                xkey: _xkey,
                ykeys: _ykeys,
                labels: _labels,
                hideHover: 'auto',
                xLabelFormat: function (d) {
                    return  d.src.labels;
                },
            });
            break;
        case "sales_chart":
            // Sales chart
            var area = new Morris.Area({
                element: _element,
                resize: false,
                data: _data,
                xkey: _xkey,
                ykeys: _ykeys,
                labels: _labels,
                lineColors: _lineColors,
                hideHover: 'auto',
                xLabelFormat: function (d) {
                    var xlable = MonthChart(_mode, d);
                    return xlable
                },
            });
            break;
        case "line-chart":
        default:
            //Line Chart
            var line = new Morris.Line({
                element: _element,
                resize: false,
                data: _data,
                xkey: _xkey,
                ykeys: _ykeys,
                labels: _labels,
                lineColors: _lineColors,
                hideHover: 'auto',
                xLabelFormat: function (d) {
                    console.log(d);
                    var xlable = MonthChart(_mode, d);
                    return xlable
                },
            });
            break;
    }
}
function MonthChart(_mode, _date)
{

    switch (_mode) {
        case "week":
        case "day":
            return  $.datepicker.formatDate('dd/mm/yy', _date);
//            var to = _date.setTime(_date.getTime() - (_date.getDay() ? _date.getDay() : 7) * 24 * 60 * 60 * 1000);
//            var from = to + 6 * 24 * 60 * 60 * 1000;
//            return  $.datepicker.formatDate('dd/mm',new Date(to))    + "->" +
//                    $.datepicker.formatDate('dd/mm/yy',new Date(from));
            break;
        case "month":
            var _lastdatmonth = new Date(_date.getFullYear(), _date.getMonth() + 1, _date.getDate() - 1);
            return   $.datepicker.formatDate('dd/mm', (_date < new Date(document.getElementById('date_from').value) ? new Date(document.getElementById('date_from').value) : _date))
                    + "->" +
                    $.datepicker.formatDate('dd/mm/yy',
                            _lastdatmonth > new Date(document.getElementById('date_to').value) ? new Date(document.getElementById('date_to').value) : _lastdatmonth);
            break;
    }
    return;
}

DrawChartNew = function (_element, _data, _type, _mode, _categories) {
    // Reset html of element before Draw chart
    $('#' + _element).html();
    switch (_type) {
        case "sales_chart":
            $('#' + _element).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'area'
                },
                title: {
                    text: (_mode == "day" ? 'Daily' : _mode == 'week' ? 'Weekly' : 'Mothly') + ' Average Newsfeed view'
                },
                subtitle: {
                    //text: 'Source: http://www.oceanize.jp'
                },
                xAxis: {
                    categories: JSON.parse(_categories)
                },
                yAxis: {
                    title: {
                        //text: 'Temperature (°C)'
                    }
                },
                plotOptions: {
                    area: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            style: {
                                textShadow: '0 0 3px black, 0 0 3px black'
                            }
                        }
                    }
                },
                series: _data
            });

            break;
        case "bar_chart":
            $('#' + _element).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: (_mode == "day" ? 'Daily' : _mode == 'week' ? 'Weekly' : 'Mothly') + ' Average Newsfeed view'
                },
                subtitle: {
                    //text: 'Source: http://www.oceanize.jp'
                },
                xAxis: {
                    categories: JSON.parse(_categories)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total fruit consumption'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            style: {
                                textShadow: '0 0 3px black, 0 0 3px black'
                            }
                        }
                    }
                },
                series: _data
            });
            break;
        case "line_chart":
            $('#' + _element).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'line'
                },
                title: {
                    text: (_mode == "day" ? 'Daily' : _mode == 'week' ? 'Weekly' : 'Mothly') + ' Average Newsfeed view'
                },
                subtitle: {
                    //text: 'Source: http://www.oceanize.jp'
                },
                xAxis: {
                    categories: JSON.parse(_categories)
                },
                yAxis: {
                    title: {
                        text: 'Temperature (°C)'
                    },
                    plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + ' ngày ' + this.x + '</b>: ' + Highcharts.numberFormat(this.y, 0, ',', '.');
                    },
                    backgroundColor: '#FCFFC5',
                    borderColor: 'black',
                    borderRadius: 10,
                    borderWidth: 2,
                    valueDecimals: 0
                },
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    borderWidth: 0
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: _data
            });
            break;
    }


}
callbackCampuse = function (item) {
    $('#university_id').val(item.id);
}

callbackUserProfile = function (item) {
    $('#university_id').val(item.id);
    selectChangeUniversity(document.getElementById('university_id'), 'campus_id', 'department_id', 1);
}

autocomplete = function (id, url, func) {
    $("#" + id).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: url,
                dataType: "jsonp",
                data: {
                    q: request.term
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            func(ui.item);
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
}
updateStatistic = function (id) {
    if (!id || $("#actionId").length <= 0) {
        return false;
    }
    if (confirm('Are you sure') == false) {
        return false;
    }
    $("#actionId2").val(id);
    $("#dataForm").submit();
}

function tagDelete(tagId, feedId)
{
    $.ajax({
        type: "POST",
        url: baseUrl + '/ajax/deletefeedtag',
        data: {tag_id: tagId, news_feed_id: feedId},
        success: function (msg) {
            if (msg == 'OK') {
                $('.tag-' + feedId + '-' + tagId).remove();
            }
        }
    });
    return false;
}

function showListTag(feedId) {
    $.ajax({
        type: "POST",
        url: baseUrl + 'ajax/showlisttag',
        data: {feedId: feedId},
        success: function (response) {
            if (response != '') {
                $('.tag-select-' + feedId).html(response);
                $('.tag-select-' + feedId).show();
            }
        }
    });
    return false;
}

function addfeedtag(feedId, tagId) {
    val_arr = tagId.split('-');
    tag_id = val_arr[0];
    tag_color = val_arr[1];
    style = "";
    if (tag_color != '') {
        style = "style='background-color:" + tag_color + "'";
    }
    $.ajax({
        type: "POST",
        url: baseUrl + '/ajax/addfeedtag',
        data: {tag_id: tag_id, news_feed_id: feedId},
        success: function (msg) {
            if (msg == 'OK') {
                if ($('.tag-' + feedId + '-' + tag_id).length == 0) {
                    tagname = $('#select-tag-id-' + feedId + ' option:selected').text();
                    build_html = "<span class='post-wrap tag-" + feedId + "-" + tag_id + "' " + style + ">";
                    build_html += tagname + "&nbsp&nbsp&nbsp&nbsp&nbsp";
                    build_html += "<div class='delete-button' " + style + ">";
                    build_html += "<a onclick='tagDelete(" + tag_id + "," + feedId + ");' class='button-delete' " + style + "><i class='fa fa-fw fa-times'></i></a></div></span>";
                    $(build_html).insertBefore(".tag-select-" + feedId);
                }
            }
            $('.tag-select-' + feedId).hide();
        }
    });
    return false;

}

function closeDiaglog(_imgsrc, _element) {
    $.each($("#dialog"), function (index, value) {
        $(this).remove();
    });
    var _imageEffect = $('#' + _element).closest('div.form-group').find('img');
    d = new Date();
    _imageEffect.attr('src', _imgsrc + "?" + d.getTime());
}

resendRegister = function (url, email) {
    var data = {
        email: email
    };
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (response) {
            alert(response);
        }
    });
    return false;
}

approve_beauty = function (user_id, beauty_id) {
    if (confirm('Are you sure?') == false) {
        return false;
    }
    $.ajax({
        type: "POST",
        url: baseUrl + 'ajax/approvebeauty',
        data: {
            user_id: user_id,
            beauty_id: beauty_id
        },
        success: function () {
            location.href = baseUrl + controller;
        }
    });
}

approve_bought_item = function (id, is_completed) {
    if (is_completed != '0') {
        return false;
    }
    if (confirm('Are you sure?') == false) {
        return false;
    }
    $.ajax({
        type: "POST",
        url: baseUrl + 'ajax/completed',
        data: {
            id: id
        },
        success: function () {
            //location.href = baseUrl + controller;
            location.reload();
        }
    });
}

cancel_bought_item = function (id, is_cancel) {
    if (is_cancel != '0') {
        return false;
    }
    if (confirm('Are you sure?') == false) {
        return false;
    }
    $.ajax({
        type: "POST",
        url: baseUrl + 'ajax/cancelboughtitem',
        data: {
            id: id
        },
        success: function () {
            //location.href = baseUrl + controller;
            location.reload();
        }
    });
}

runbatch = function () {
    if (confirm('Are you sure?') == false) {
        return false;
    }
    alert('Batch is running...');
    var data = {};
    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: baseUrl + '/ajax/runbatch',
        data: data,
        success: function (response) {
            alert(response);
        }
    });
    return false;
}

killProcess = function (id) {
    if (confirm('Are you sure?') == false) {
        return false;
    }
    location.href = baseUrl + controller + '/ps?id=' + id;
    return false;
}

//Check format for JP Phone 
function checkJPPhone(phone) {        
    var regEx=/^[0-9]{2,5}-[0-9]{3,4}-[0-9]{3,4}$/;    
    var ok = regEx.test(phone);    
    return ok;   
}
function checkJPPhone2(phone) {        
    var regEx=/^[0-9]{2,5}-[0-9]{3,4}-[0-9]{3,4}$/;
    var regEx2=/^[0-9]{10,11}$/;
    var ok = regEx.test(phone);
    if (ok === false) {
        var ok = regEx2.test(phone);
    }
    return ok;   
}
function checkNewOrder(count_without_nailist, count_add_nail, check_neworder) {
    if ($('#medicalchart').length > 0) {
        return false;
    }  
    var data = {};
    if ($('#orders_seat').length > 0 || $('#pages_index').length > 0 || $('#orders_lists').length > 0) {
        data = {
            count_without_nailist: count_without_nailist,
            count_add_nail: count_add_nail,
            check_neworder: check_neworder
        };
    } else {
        data = {
            count_without_nailist: count_without_nailist,               
            check_neworder: check_neworder
        }; 
    }
    
    setTimeout(function () { 
        var objOrderIds = {};
        var id = 0;
        if($(".timebar").length > 0) {
            $(".timebar").each(function() {
                var order_id = $(this).data('id');
                objOrderIds[id] = order_id;
                id++;
            });
        }
        data['drawedOrderIds'] = objOrderIds;
        
        $.ajax({
        type: "POST",
        url: baseUrl + 'ajax/checkneworder',
        async: false,
        cache: false,
        data: data,
        success: function (json) {
            result = JSON.parse(json); 
            var totalNewNoNailist = result['totalNewNoNailist'];
            var notifyHtml = result['notifyHtml'];
            var totalAddNail = result['totalAddNail'];
            var notifyHtmlAddNail = result['notifyHtmlAddNail'];
            
            if (notifyHtmlAddNail && $('#orders_lists').length > 0) {
                if ($('#info_message').length > 0) {
                    $('#info_message').remove();
                }
                showNotification({
                    message: notifyHtmlAddNail,
                    autoClose: false,
                    duration: 180,
                    type: 'error'
                });
            }else if (notifyHtml) {
                if ($('#info_message').length > 0) {
                    $('#info_message').remove();
                }
                showNotification({
                    message: notifyHtml,
                    autoClose: false,
                    duration: 180
                });
            }
            
            if (parseInt(totalNewNoNailist) > 0) {
                $("#alert-no-nailist").html(totalNewNoNailist);
            } else {
                $("#alert-no-nailist").text('');
            }
            var it = 0;
            if (totalAddNail.length > 0) {
                $("#nailAdd").removeClass('hidden').html('');
                for (var i = 0; i < totalAddNail.length ; i++) {
                    $("#nailAdd").append(" \
                        <a href='javascript:optionDialog(" + totalAddNail[i]['id'] + ");' > \
                        <div class='col-lg-3 col-xs-6'> \
                        <div class='small-box bg-red custom-small-box'> \
                        <div class='inner custom_inner'> \
                        <h3> " + totalAddNail[i]['user_name']+ "</h3> \
                        <p>ネイルの選択が終わりました</p> \
                        </div> \
                        <div class='icon custom_icon'> \
                        <i class='fa fa-bell'></i> \
                        </div> \
                        </div> \
                        </div> \
                        </a> \
                        ");
                };
                    //$("#p_totalAddNail").text(totalAddNail.length);
                    //$("#orderName").text(totalAddNail[0]['user_name']);

                    $('#nailAdd').fadeOut(1000, function() {
                        $(this).fadeIn(1000);
                    });
                    it = setInterval(function() {
                        $('#nailAdd').fadeOut(1000, function() {
                            $(this).fadeIn(1000);
                        });
                    }, 1000);
                } else if (count_add_nail == 1) {
                    $("#nailAdd").addClass('hidden');
                    clearTimeout(it);
                }
            }
        });
    }, 2000);
}

$(function() {   
    /**
    * Crop Image
    **/
    $(".crop-img").click(function (e) {
        e.preventDefault();
        var url = baseUrl + 'images/get_image?img=' + urldecode($('a.js-thumb').attr('href'));
        $('.resize-image').attr('src', url);
        resizeableImage($('.resize-image'));
        $("#modal-resize").modal('show');
    });
     
    $(".date_picker").datepicker({
        format: 'yyyy-mm-dd',                           
        clearBtn: true,
        todayHighlight: true
    });

    $("#shop_id").change(function () {
        var type = 'checkbox';
        if ($('#nailist_id').attr('class').match(/selectbox/)) {
            type = 'selectbox';
        }
        var id = $('#id').val();
        var shop_id = $(this).val();
        var data = {
            order_id: id,
            shop_id: shop_id,
            type: type
        };
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/shownailistsofshop',
            data: data,
            success: function (response) {
                if (response) {
                    $('#nailist_id').html(response);
                } else {
                    $('#nailist_id').html('');
                }
            }
        });
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/showdevicesofshop',
            data: data,
            success: function (response) {
                if (response) {
                    $('#device_id').html(response);
                } else {
                    $('#device_id').html('');
                }
            }
        });
        return false;
    });
    
    //define class numberOnly for fields which accept only number
    $(document).on('keydown', '.numberOnly', function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||         
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    $('.numberOnly').bind("paste",function(e) {
        e.preventDefault();
    });
    
    //event submit form edit customer
    $(document).on('click', '#btnCusSubmit', function(e) {
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/customerdialog',
            data: $('#UserCustomerdialogForm').serialize(),
            success: function (response) {
                console.debug(response);
            }
        });
    });
    
    //show dialog function
    showMedicalChartDialog = function (order_id) {
        var W = $( window ).width();
        var H = $( window ).height();
        window.open(baseUrl + 'ajax/medicalchartdialog?id=' + order_id, "popupWindow", "width=" + W + ",height=" + H + ",scrollbars=yes");
    };

    $('.datetime_picker').click(function() {
        $(this).next().click();
    });
     // Check New_order  
    checkNewOrder(1,1,0); 
    setInterval(function () { checkNewOrder(1,1,1); },timeoutNewOrder);    

    // Handler input for JP Phone
    // Handler evet for input Phone type
    $('.phoneInput').keyup(function(e) {
        var errorId = $(this).data('errorid');
        var format = $(this).data('format');
        if (typeof errorId == 'undefined' || $('#'+errorId).length == 0) {
            return false;
        }  
        if (typeof format == 'undefined') {
            format = '1';
        }
        var phone = $(this).val();    
        if (format == '1') {
            if (checkJPPhone(phone) == false && phone.length > 0) {
                $('#'+errorId).show();
            } else {
                $('#'+errorId).hide();
            }
        } else {
            if (checkJPPhone2(phone) == false && phone.length > 0) {
                $('#'+errorId).show();
            } else {
                $('#'+errorId).hide();
            }
        }
    });
    $(".phoneInput").keydown(function(e) { 
        // Allow: backspace, delete, tab, escape, enter, subtract and
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110,173]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey)) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey)) ||
            //Japanese characters
            (e.keyCode == 229) || 
            //- characters
            (e.keyCode == 189) || 
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('.phoneInput').bind("paste", function(e) {
        e.preventDefault();
    });

    $(".btn-cancelOrder").click(function () { 
       var that = $("input.btn-cancelOrder"); 
       var id = that.data('id');
       var status = that.data('status');
       $("#action").val(status);
       $("#actionId").val(id);
       $("#dataForm").submit();
    });
});

function saveTimelyLimit() {
    var data = [];
    $( ".timely_limit" ).each(function() {
        if ($(this).val() != '' && $('#hp_limit_' + $(this).attr('id')).val() != '') {
            var row = {};
            var time = $(this).attr('name');
            var limit = $(this).val();
            var hp_limit = $('#hp_limit_' + $(this).attr('id')).val();
            
            row['time'] = $('#date').val() + ' ' + time;
            row['limit'] = limit;
            row['hp_limit'] = hp_limit;
            data.push(row);
        } else if ($(this).val() != '') {
            $('#hp_limit_' + $(this).attr('id')).focus();
            alert('Input both limit and hp limit');
            return false;
        } else if ($('#hp_limit_' + $(this).attr('id')).val() != '') {
            $(this).focus();
            alert('Input both limit and hp limit');
            return false;
        }
    });
    
    $('#flashMessage').addClass('Hidden');
    $.ajax({
        method: 'POST',
        type: "text",
        url: baseUrl + 'ordertimelylimits',
        data: {data:data},
        cache: false,
        success: function (json) {
            if (json) {
                var result = jQuery.parseJSON(json);
                if (typeof result['msg'] != undefined) {
                    $('#flashMessage').html(result['msg']);
                    $('#flashMessage').removeClass('Hidden');
                    $("html, body").animate({ scrollTop: 0 }, "fast");
                }
            }
        }
    });
}

function openMediacal(order_id) {    
    var medicalIdList = new cookieList("medicalIdList");
    var medicalids = medicalIdList.items();

    if (medicalids.length > 0 && medicalids.indexOf("" +order_id) != -1) {
        alert('Mediacal window is opening');
    } else {
        //$.cookie("medicalid", order_id, { path: "/" });
        medicalIdList.add(order_id);
        var url = baseUrl + 'medicalchart/'+order_id;
        var win = window.open(url);
        win.focus();
    }
    return false;
}

$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        $('.modal-backdrop').attr('style', 'display:none');
    }
});
