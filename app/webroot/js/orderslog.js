    $(document).ready(function () {

        $("#popupWrapperLog, .bgBlack").addClass("active");

        $(".close").click(function () {
            $("#popupWrapperLog, .bgBlack").removeClass("active");
        });

        $("#read-more").click(function () {
            cur_limit = $(this).attr('data-cur-limit');
            order_id = $(this).attr('data-order-id');
            label = $(this).text();
            if(label == 'トップへ戻る'){//back to top
               curHtml = $('#popupWrapperLog').html();      
            	$("#popupWrapperLog").hide().html(curHtml).fadeIn('fast');        
            } else{// read more
            	$.ajax({
                    type: "POST",
                    url: baseUrl + 'ajax/orderslogreadmore/' + order_id,
                    data: {cur_limit: cur_limit},
                    success: function (response) {
                    	data = response.split("---");
                        logList = data[0];
                        limit = parseInt(data[1]);
                        total = parseInt(data[2]);
                        if(total <= limit){
                        	$("#read-more").text('トップへ戻る');
                        	//$("#read-more").attr('href', '#backtotop');
                        }
                        $('.log-list').html(logList);
                        $("input[name='userlogs[cur_limit]']").val(limit);
                        $("#read-more").attr('data-cur-limit', limit);
                    }
                });    
            }
        });
		// add log
		var isClick = false;
        $('#addLog').click(function () {
            dt = $('#frm_user_log').serialize();
            //just one click
            if(isClick == false){
            		$(this).addClass('disable');
            		isClick = true;
            }else{
                return false;
            }
            //call ajax
            $.ajax({
                type: "POST",
                url: baseUrl + 'ajax/orderslogupdate',
                data: dt,
                success: function (response) {
                    if (response != 'Failed') {
                    	data = response.split("---");
                        logList = data[0];
                        limit = parseInt(data[1]);
                        total = parseInt(data[2]);
                        $('.log-list').html(logList);
                        $("input[name='userlogs[cur_limit]']").val(limit);
                        $("#read-more").attr('data-cur-limit', limit);
                        $("[name='userlogs[memo]']").val('');
                        if(total > limit){
                        	$("#read-more").text('もっと見る');
                        	$(".bgGray").show();
                         }
                      }
                    isClick = false; 
                    $('#addLog').removeClass('disable');
                }
            });
            return false;
        });
        
        
         /*Dropdown nailist*/
        $('.drop-nailist').on('click',function(){
            $('#logs_nailists').toggle();
        });

        $("#logs_nailists").on("click", ".user .nailists", function() {
            $('.b_nailists').html($(this).html());
            $('.b_nailists').attr('data-id',$(this).data('id'));
            $('.b_nailists').attr('data-name',$(this).data('name'));
            $('.b_nailists').parent().addClass('user');
            $('.b_nailists').addClass('user-log');
            $('#userlogs-nailist').val($(this).data('id'));
        });
        
    });
