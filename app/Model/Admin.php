<?php
/**
 * Admin's model.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Admin extends AppModel
{
	public $name = 'Admin';
	public $table = 'admins';
	public $primaryKey = 'id';

        /**
	 * Verify data before the processing to insert or update.
	 *
	 * @author Truongnn
	 * @param array $data Input data.
	 * @return bool Returns the boolean.
	 */
	public function validateInsertUpdate($data)
	{
		$this->set($data[$this->name]);
		$this->validate = array(
			'login_id'         => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('login_id can not empty'),
				),
				'isUnique' => array(
					'rule'    => array('isUnique', 2, 13),
					'message' => __('login_id must have unique value')
				),
			),
			'password'         => array(
				'maxLength'   => array(
					'allowEmpty' => false,
					'rule'       => array('between', 6, 40),
					'message'    => __('Your password must be not null or must be between 6 and 40 characters'),
				), 'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('password can not empty'),
				),
			),
			'password_confirm' => array(
				'maxLength' => array(
					'allowEmpty' => false,
					'rule'       => array('between', 6, 40),
					'message'    => __('Your password confirm must be not null or must be between 6 and 40 characters'),
				),
				'compare'   => array(
					'rule'    => array('validate_passwords'),
					'message' => __('The password confirm you entered do not match'),
				)
			),
			'name'             => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('Name can not empty'),
				),
				'between'  => array(
					'rule'    => array('between', 2, 40),
					'message' => __('Between 2 to 40 characters')
				),
			),
		);
		if ($this->validates()) {
			return true;
		}
		return false;
	}
        /**
	 * Validate password.
	 *
	 * @author Truongnn
	 * @return bool Returns the boolean.
	 */
	public function validate_passwords()
	{
            return $this->data[$this->name]['password'] === $this->data[$this->name]['password_confirm'];
	}

	public function validate_passwords_admin($data)
	{
		$this->set($data[$this->name]);
		$this->validate = array(
			'password'         => array(
				'maxLength' => array(
					'allowEmpty' => false,
					'rule'       => array('between', 6, 40),
					'message'    => __('Your password must be not null or must be between 6 and 40 characters'),
				),
				'notEmpty'  => array(
					'rule'    => 'notEmpty',
					'message' => __('password can not empty'),
				),
			),
			'password_confirm' => array(
				'maxLength' => array(
					'allowEmpty' => false,
					'rule'       => array('between', 6, 40),
					'message'    => __('Your password confirm must be not null or must be between 6 and 40 characters'),
				),
				'compare'   => array(
					'rule'    => array('validate_passwords'),
					'message' => __('The password confirm you entered do not match'),
				)
			)
		);
		if ($this->validates()) {
			return $data[$this->name]['password'] === $data[$this->name]['password_confirm'];
		} else {
			return false;
		}
	}
         /**
	 * Validate profile.
	 *
	 * @author Truongnn
	 * @param array $data Input data.
	 * @return bool Returns the boolean.
	 */
	public function validate_profile($data)
	{
		$this->set($data[$this->name]);
		$this->validate = array(
			'name' => array(
				'maxLength' => array(
					'allowEmpty' => false,
					'rule'       => array('between', 2, 40),
					'message'    => __('Your name must be not null or must be between 2 and 40 characters'),
				),
				'notEmpty'  => array(
					'rule'    => 'notEmpty',
					'message' => __('Name can not empty'),
				),
			),
		);
		if ($this->validates()) {
			return true;
		}
		return false;
	}
}
