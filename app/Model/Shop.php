<?php

/**
 * Shop of model.
 *
 * @package Model
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Shop extends AppModel {

    public $name = 'Shop';
    public $table = 'shops';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author VuLTH
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'prefecture_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Prefecture can not empty'),
                )
            ),
            'admin_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Admin can not empty'),
                )
            ),
            'shop_group_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Shop group can not empty'),
                )
            ),
            'name' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 45),
                    'message' => __('Name must be no larger then 45 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                )
            ),
            'is_designate' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Designate can not empty'),
                )
            ),
            'phone' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 45),
                    'message' => __('Phone must be no larger then 45 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Phone can not empty'),
                )
            ),
            'address' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 128),
                    'message' => __('Address must be no larger then 128 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Address can not empty'),
                )
            ),
            'open_time' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Open time can not empty'),
                )
            ),
            'close_time' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Close time can not empty'),
                ),
                'compare' => array(
                    'rule' => array('validate_dates'),
                    'message' => __('Close time must be greater or equal open time'),
                )
            ),
            'is_plus' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Plus can not empty'),
                )
            ),
            'max_seat' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Max seat can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxlength', 3),
                    'message' => __('Max seat must be not larger than 3 character long'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Max seat must be numeric'),
                )
            ),
            'hp_max_seat' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('HP max seat can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxlength', 3),
                    'message' => __('HP max seat must be not larger than 3 character long'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('HP max seat must be numeric'),
                )
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Validate date.
     *
     * @author truongnn
     * @return bool True if close_time >= open_time or otherwise.
     */
    public function validate_dates() {
        return $this->data[$this->name]['close_time'] >= $this->data[$this->name]['open_time'];
    }

}
