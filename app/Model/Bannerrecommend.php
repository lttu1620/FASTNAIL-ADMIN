<?php

/**
 * Bannerrecommend of model.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Bannerrecommend extends AppModel {

    public $name = 'Bannerrecommend';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author diennvt
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateBannerrecommendInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 128),
                    'message' => __('Name must be no larger then 128 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
