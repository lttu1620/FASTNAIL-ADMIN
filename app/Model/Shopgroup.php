<?php

/**
 * ShopGroup of model.
 *
 * @package Model
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Shopgroup extends AppModel {

    public $name = 'Shopgroup';
    public $table = 'shop_groups';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author VuLTH
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 64),
                    'message' => __('Between 1 to 64 characters')
                ),
            ),
            'address' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Address can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 255),
                    'message' => __('Between 1 to 255 characters')
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
