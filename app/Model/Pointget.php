<?php

/**
 * Point Get's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Pointget extends AppModel
{
    public $name = 'Pointget';
    public $table = 'point_gets';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns the boolean
     */
    public function validateInsertUpdate($data)
    {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'between'  => array(
                    'rule'    => array('between', 1, 255),
                    'message' => __('Between 1 to 255 characters')
                ),
            ),
            'point' => array(
                'between'  => array(
                    'rule'    => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                ),
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
