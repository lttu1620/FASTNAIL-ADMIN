<?php

/**
 * Tag of model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Tag extends AppModel {

    public $name = 'Tag';
    public $table = 'tags';
    public $primaryKey = 'id';

  /**
     * Verify data before the processing to insert or update.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'color' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 10),
                    'message' => __('Color must be between 1 to 10 characters')
                ),
            ),
            'feed_count' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Feed count can not empty')
                ),
                'between' => array(
                    'rule' =>array('between',1,11),
                    'messages' => __('Feed count must be choose')
                )
            ),
        );
        if ($this->validates())
            return true;
        return false;
    }

}
