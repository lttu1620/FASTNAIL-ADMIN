<?php

/**
 * Mailsendlog's model.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Mailsendlog extends AppModel
{
	public $name = 'Mailsendlog';	
	public $primaryKey = 'id';     
}
