<?php

/**
 * Device of model.
 *
 * @package Model
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Device extends AppModel {

    public $name = 'Device';
    public $table = 'devices';
    public $primaryKey = 'id';

  /**
     * Verify data before the processing to insert or update.
     *
     * @author Hoang Gia Thong
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 45),
                    'message' => __('Color must be between 1 to 45 characters')
                ),
            ),
            'shop_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Shop can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 11),
                    'message' => __('Color must be between 1 to 45 characters')
                ),
            ),
        );
        if ($this->validates())
            return true;
        return false;
    }

}
