<?php

/**
 * User bought item's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Userboughtitem extends AppModel
{
    public $name = 'Userboughtitem';
    public $table = 'user_bought_items';
    public $primaryKey = 'id';
}
