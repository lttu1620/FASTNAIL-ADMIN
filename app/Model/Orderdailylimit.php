<?php

/**
 * Ordernail of model.
 *
 * @package Model
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Orderdailylimit extends AppModel {

    public $name = 'Orderdailylimit';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Tuan cao
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateOrdernailInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'date' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Date can not empty'),
                ),
            ),
            'shop_id' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Shop id must be no larger then 11 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Shop id section can not empty'),
                ),
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
