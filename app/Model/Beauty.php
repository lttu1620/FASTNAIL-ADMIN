<?php
/**
 * Beauty of model.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Beauty extends AppModel {

    public $name = 'Beauty';
    public $table = 'beauties';
    public $primaryKey = 'id';

  /**
     * Verify data before the processing to insert or update.
     *
     * @author diennvt
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'title' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Title can not empty'),
                ),
                'between' => array(
                    'rule' => 'checkTitleJapanese',
                    'message' => 'タイトルは全角55文字、または英数字110文字以内で入力してください。'
                ),
            ),
            'mission_type' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'ミッションタイプは必須項目です。',
                ),               
            ),
            'segment_prefecture' => array(
                'rule' => '/^\d{1,2}(?:,\d{1,2})*$/',
                'allowEmpty' => true,
                'message' => '例：1,2,3,4,10,20... または 01,02,03,04,10,20,...（空白なし）'
            ),
            'segment_user_generation' => array(
                'rule' => '/^\d{1,2}(?:,\d{1,2})*$/',
                'allowEmpty' => true,
                'message' => '例：1,2,3,4,10,20... または 01,02,03,04,10,20,...（空白なし）'
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
    
    /**
     * Detect japanese
     *
     * @author thailh
     * @param string $string String for truncating     
     * @return boolean True/False
     */
    public static function is_japanese($string) {
        return preg_match('/[\x{4E00}-\x{9FBF}\x{3040}-\x{309F}\x{30A0}-\x{30FF}]/u', $string);
    } 
    
    public function checkTitleJapanese($title) {
        $value = array_values($title);
        $is_jp = self::is_japanese($value[0]);
        $cnt = iconv_strlen($value[0], 'UTF-8');
        if($is_jp) {
            return $cnt <= 55;
        }
        return $cnt <= 110;
    }

}
