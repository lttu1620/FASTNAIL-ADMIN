<?php

/**
 * Claim's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Claim extends AppModel
{
    public $name = 'Claim';
    public $table = 'Claims';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns the boolean
     */
    public function validateInsertUpdate($data)
    {
        $this->set($data[$this->name]);
        $this->validate = array(
            'nailist_id' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Nailist id can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                ),
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
