<?php

/**
 * Order update timebar log's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Orderupdatetimebarlog extends AppModel
{
    public $name = 'Orderupdatetimebarlog';
    public $table = 'order_update_timebar_logs';
    public $primaryKey = 'id';
}
