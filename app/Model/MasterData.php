<?php

App::uses('AppModel', 'Model');

/**
 * MasterData of model
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class MasterData extends AppModel
{
    /**
     * Get list price.
     *
     * @author Le Tuan Tu
     * @return array List all price.
     */
    public static function prices_all()
    {
        $result = AppCache::read(Configure::read('prices_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_nails_price'), array());
            AppCache::write(Configure::read('prices_all')->key, $result, Configure::read('prices_all')->seconds);
        }
        if (isset($result['0000'])) {
            unset($result['0000']);
        }
        return $result;
    }

    /**
     * Get all colors.
     *
     * @author Le Tuan Tu
     * @return array List all colors.
     */
    public static function colors_all()
    {
        $result = AppCache::read(Configure::read('colors_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_colors_all'), array());
            AppCache::write(Configure::read('colors_all')->key, $result, Configure::read('colors_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all bullions.
     *
     * @author Le Tuan Tu
     * @return array List all bullions.
     */
    public static function bullions_all()
    {
        $result = AppCache::read(Configure::read('bullions_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_bullions_all'), array());
            AppCache::write(Configure::read('bullions_all')->key, $result, Configure::read('bullions_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all designs.
     *
     * @author Le Tuan Tu
     * @return array List all designs.
     */
    public static function designs_all()
    {
        $result = AppCache::read(Configure::read('designs_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_designs_all'), array());
            AppCache::write(Configure::read('designs_all')->key, $result, Configure::read('designs_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all flowers.
     *
     * @author Le Tuan Tu
     * @return array List all flowers.
     */
    public static function flowers_all()
    {
        $result = AppCache::read(Configure::read('flowers_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_flowers_all'), array());
            AppCache::write(Configure::read('flowers_all')->key, $result, Configure::read('flowers_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all genres.
     *
     * @author Le Tuan Tu
     * @return array List all genres.
     */
    public static function genres_all()
    {
        $result = AppCache::read(Configure::read('genres_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_genres_all'), array());
            AppCache::write(Configure::read('genres_all')->key, $result, Configure::read('genres_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all holograms.
     *
     * @author Le Tuan Tu
     * @return array List all holograms.
     */
    public static function holograms_all()
    {
        $result = AppCache::read(Configure::read('holograms_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_holograms_all'), array());
            AppCache::write(Configure::read('holograms_all')->key, $result, Configure::read('holograms_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all materials.
     *
     * @author Le Tuan Tu
     * @return array List all materials.
     */
    public static function materials_all()
    {
        $result = AppCache::read(Configure::read('materials_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_materials_all'), array());
            AppCache::write(Configure::read('materials_all')->key, $result, Configure::read('materials_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all paints.
     *
     * @author Le Tuan Tu
     * @return array List all paints.
     */
    public static function paints_all()
    {
        $result = AppCache::read(Configure::read('paints_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_paints_all'), array());
            AppCache::write(Configure::read('paints_all')->key, $result, Configure::read('paints_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all powders.
     *
     * @author Le Tuan Tu
     * @return array List all powders.
     */
    public static function powders_all()
    {
        $result = AppCache::read(Configure::read('powders_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_powders_all'), array());
            AppCache::write(Configure::read('powders_all')->key, $result, Configure::read('powders_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all ramejells.
     *
     * @author Le Tuan Tu
     * @return array List all ramejells.
     */
    public static function ramejells_all()
    {
        $result = AppCache::read(Configure::read('ramejells_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_ramejells_all'), array());
            AppCache::write(Configure::read('ramejells_all')->key, $result, Configure::read('ramejells_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all scenes.
     *
     * @author Le Tuan Tu
     * @return array List all scenes.
     */
    public static function scenes_all()
    {
        $result = AppCache::read(Configure::read('scenes_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_scenes_all'), array());
            AppCache::write(Configure::read('scenes_all')->key, $result, Configure::read('scenes_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all shells.
     *
     * @author Le Tuan Tu
     * @return array List all shells.
     */
    public static function shells_all()
    {
        $result = AppCache::read(Configure::read('shells_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_shells_all'), array());
            AppCache::write(Configure::read('shells_all')->key, $result, Configure::read('shells_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all stones.
     *
     * @author Le Tuan Tu
     * @return array List all stones.
     */
    public static function stones_all()
    {
        $result = AppCache::read(Configure::read('stones_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_stones_all'), array());
            AppCache::write(Configure::read('stones_all')->key, $result, Configure::read('stones_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all tags.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function tags_all()
    {
        $result = AppCache::read(Configure::read('tags_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_tags_all'), array());
            AppCache::write(Configure::read('tags_all')->key, $result, Configure::read('tags_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all shops.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function shops_all($shop_group_id = 0)
    {                 
        $result = AppCache::read(Configure::read('shops_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_shops_all'), array());
            AppCache::write(Configure::read('shops_all')->key, $result, Configure::read('shops_all')->seconds);
        }
        if (!empty(CakeSession::read('Auth')['User']) && CakeSession::read('Auth')['User']->admin_type == 1) { // system admin
            return $result;
        } 
        if (!empty($shop_group_id)) {            
            $result = static::getCommonComponent()->arrayFilter($result, 'shop_group_id', $shop_group_id);
        }
        return $result;
    }

    /**
     * Get all nailists.
     *
     * @author Tran Xuan Khoa
     * @return array List all nailists.
     */
    public static function nailists_all($shop_id = 0)
    {
        $result = AppCache::read(Configure::read('nailists_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_nailists_all'), array());
            AppCache::write(Configure::read('nailists_all')->key, $result, Configure::read('nailists_all')->seconds);
        }
        if (!empty($shop_id)) {            
            $result = static::getCommonComponent()->arrayFilter($result, 'shop_id', $shop_id);
        }
        return $result;
    }

    /**
     * Get all nails.
     *
     * @author Tran Xuan Khoa
     * @return array List all nails.
     */
    public static function nails_all()
    {
        $result = AppCache::read(Configure::read('nails_all')->key);

        if (!$result) {
            $result = Api::call(Configure::read('API.url_nails_all'), array());
            AppCache::write(Configure::read('nails_all')->key, $result, Configure::read('nails_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all items.
     *
     * @author Tran Xuan Khoa
     * @return array List all nails.
     */
    public static function items_all()
    {
        $result = AppCache::read(Configure::read('items_all')->key);
        if (!$result) {
            $result = Api::call(Configure::read('API.url_items_all'), array());
            AppCache::write(Configure::read('items_all')->key, $result, Configure::read('items_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all shopgroups_all.
     *
     * @author VuLTH
     * @return array List all shopgroups_all.
     */
    public static function shopgroups_all()
    {
        $result = AppCache::read(Configure::read('shopgroups_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_shopgroups_all'), array());
            AppCache::write(Configure::read('shopgroups_all')->key, $result, Configure::read('shopgroups_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all prefectures_all.
     *
     * @author VuLTH
     * @return array List all prefectures_all.
     */
    public static function prefectures_all()
    {
        $result = AppCache::read(Configure::read('prefectures_all')->key);
        //AppCache::delete(Configure::read('prefectures_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_prefectures_all'), array());
            AppCache::write(Configure::read('prefectures_all')->key, $result, Configure::read('prefectures_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all admins_all.
     *
     * @author VuLTH
     * @param int $disable
     * @param int $admin_type
     * @return array List all admins_all.
     */
    public static function admins_all($disable = 0, $admin_type = 0)
    {
        $result = AppCache::read(Configure::read('admins_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_admins_all'), array(
                'disable'    => $disable,
                'admin_type' => $admin_type,
            ));
            AppCache::write(Configure::read('admins_all')->key, $result, Configure::read('admins_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all admins_all.
     *
     * @author VuLTH
     * @return array List all admins_all.
     */
    public static function areas_all()
    {
        $result = AppCache::read(Configure::read('areas_all')->key);
        //AppCache::delete(Configure::read('areas_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_areas_all'), array());
            AppCache::write(Configure::read('areas_all')->key, $result, Configure::read('areas_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all admins_all.
     *
     * @author VuLTH
     * @return array List all admins_all.
     */
    public static function surveys_all()
    {
        $result = AppCache::read(Configure::read('surveys_all')->key);
        //AppCache::delete(Configure::read('areas_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_surveys_all'), array());
            AppCache::write(Configure::read('surveys_all')->key, $result, Configure::read('surveys_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all user.
     *
     * @author Le Tuan Tu
     * @return array List all user.
     */
    public static function users_all()
    {
        $result = AppCache::read(Configure::read('users_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_users_all'), array());
            AppCache::write(Configure::read('users_all')->key, $result, Configure::read('users_all')->seconds);
        }
        return $result;
    }

    /**
     * Get all user.
     *
     * @author Le Tuan Tu
     * @return array List all user.
     */
    public static function devices_all($shop_id = 0)
    {
        $result = AppCache::read(Configure::read('devices_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_devices_all'), array());
            AppCache::write(Configure::read('devices_all')->key, $result, Configure::read('devices_all')->seconds);
        }
        if (!empty($shop_id)) {            
            $result = static::getCommonComponent()->arrayFilter($result, 'shop_id', $shop_id);
        }
        return $result;
    }
    
    /**
     * Get all services.
     *
     * @author diennvt
     * @return array List all services.
     */
    public static function services_all()
    {
    	$result = AppCache::read(Configure::read('services_all')->key);
    	if ($result === false) {
    		$result = Api::call(Configure::read('API.url_services_all'), array());
    		AppCache::write(Configure::read('services_all')->key, $result, Configure::read('services_all')->seconds);
    	}
    	return $result;
    }
}
