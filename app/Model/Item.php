<?php

/**
 * Item's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Item extends AppModel
{
    public $name = 'Item';
    public $table = 'items';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns the boolean
     */
    public function validateInsertUpdate($data)
    {
        $this->set($data[$this->name]);
        $this->validate = array(
            'item_cd' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Item cd can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 45),
                    'message' => __('Between 1 to 45 characters')
                ),
            ),
            'item_section' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Item section can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 45),
                    'message' => __('Between 1 to 45 characters')
                ),
            ),
            'name' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 45),
                    'message' => __('Between 1 to 45 characters')
                ),
            ),
            'price' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Price can not empty'),
                ),
            ),
            'priority' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Priority can not empty'),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
