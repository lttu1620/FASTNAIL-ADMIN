<?php

/**
 * Item's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Pointitem extends AppModel {

    public $name = 'Pointitem';
    public $table = 'point_items';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns the boolean
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'between' => array(
                    'rule' => array('between', 1, 255),
                    'message' => __('Between 1 to 255 characters')
                ),
            ),
            'description' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Description can not empty'),
                ),
            ),
            'point' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Point can not empty'),
                ),
            ),
            'start_date' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Start date can not empty'),
                ),
            ),
            'end_date' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('End date can not empty'),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

}
