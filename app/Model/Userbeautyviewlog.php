<?php

/**
 * User beauty view log's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Userbeautyviewlog extends AppModel
{
    public $name = 'Userbeautyviewlog';
    public $table = 'user_beauty_view_logs';
    public $primaryKey = 'id';
}
