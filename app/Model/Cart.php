<?php

/**
 * Cart of model.
 *
 * @package Model
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Cart extends AppModel {

    public $name = 'Cart';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Tran Xuan Khoa
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateCartInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            /*
            'code' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 64),
                    'message' => __('Code must be no larger then 64 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Code can not empty'),
                ),
            ),
            'name' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 64),
                    'message' => __('Name must be no larger then 11 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
            ),
            'shop_id' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Shop id must be no larger then 11 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Shop id section can not empty'),
                ),
            ),*/
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
