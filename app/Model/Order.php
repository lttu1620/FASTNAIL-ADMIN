<?php

/**
 * Order of model.
 *
 * @package Model
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Order extends AppModel {

    public $name = 'Order';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Tran Xuan Khoa
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateOrderInsertUpdate($data) {
        $this->set($data);
        $this->validate = array(
            'shop_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Shop ID can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Shop ID must be not larger than %d character long'),
                )
            ),
            'cart_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Cart ID can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Cart ID must be not larger than %d character long'),
                )
            ),
            'user_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('User ID can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('User ID must be not larger than %d character long'),
                ),
                'format' => array(
                    'rule' => '/^\d+$/',
                    'message' => __('User ID is invalid'),
                ),
            ),
            'total_price' => array(
                'maxLength' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Total price must be not larger than %d character long'),
                )
            ),
            'total_tax_price' => array(
                'maxLength' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Total tax price must be not larger than %d character long'),
                )
            ),            
            'user_name' => array(
                /*
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('User name can not empty'),
                ),
                */
                'maxLength' => array(
                    'rule' => array('maxlength', 64),
                    'message' => __('User name must be not larger than %d character long'),
                )
            ),
            'reservation_date' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Reservation date can not empty'),
                )
            ),
            'kana' => array(
                'maxLength' => array(
                    'rule' => array('maxlength', 64),
                    'message' => __('Name kana must be not larger than %d character long'),
                ),
                'kata' => array(
                    'allowEmpty' => true,
                    'rule' => 'katakana',
                    'message' => __('Name kana must be katakana'),
                ),
            ),
            'email' => array(
                /*
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                * 
                */
                'maxLength' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxlength', 255),
                    'message' => __('Email must be not larger than %d character long'),
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => __('Email is invalid'),
                ),
            ),
            'phone' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Phone can not empty'),
                ),
                'format' => array(                 
                    'rule' => 'phone_format2',
                    'message' => __('Phone is invalid'),
                ),                
                /*
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Phone can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxlength', 15),
                    'message' => __('Phone must be no larger then 13 character long'),
                ),                
                'format' => array(
                    'allowEmpty' => true,
                    'rule' => array('phone', '/^\d{2,5}\-\d{3,4}\-\d{3,4}+$/'),
                    'message' => __('Phone is invalid'),
                ),
                'between' => array(
                    'rule'      => array('lengthBetween', 12,13),
                    'message'   => __("The phone number must contain 10 - 11 digits"),
                )*/
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Tran Xuan Khoa
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateOrderInsertUpdateCalendar($data) {
        $this->set($data);
        $this->validate = array(
            'user_name' => array(
                'maxLength' => array(
                    'rule' => array('maxlength', 64),
                    'message' => __('User name must be not larger than %d character long'),
                ),
            ),
            'email' => array(
                'maxLength' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxlength', 255),
                    'message' => __('Email must be not larger than %d character long'),
                ),
                'format' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address'),
                ),
            ),
            'phone' => array(    
                'format' => array(      
                    'allowEmpty' => true,
                    'rule' => 'phone_format2',
                    'message' => __('Phone is invalid'),
                ),
                /*
                'maxLength' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxlength', 15),
                    'message' => __('Phone must be no larger then 13 character long'),
                ),
                'format' => array(
                    'rule' => array('phone', '/^\d{2,5}\-\d{3,4}\-\d{3,4}+$/'),
                    'message' => __('Phone is invalid'),
                ),               
                'between' => array(
                    'rule' => array('lengthBetween', 12, 13),
                    'message' => __("The phone number must contain 10 - 11 digits"),
                )*/
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
