<?php

/**
 * ShopGroup of model.
 *
 * @package Model
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Prefecture extends AppModel {

    public $name = 'Prefecture';
    public $table = 'prefectures';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author VuLTH
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'area_id' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Area can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                ),
            ),
            'name' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 45),
                    'message' => __('Between 1 to 45 characters')
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    

}
