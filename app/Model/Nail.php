<?php

/**
 * Nail of model.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Nail extends AppModel {

    public $name = 'Nail';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author diennvt
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateNailInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'photo_cd' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Photo code must be no larger then 11 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Photo code can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Photo code must be numeric'),
                )
            ),

            'show_section' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Show section must be no larger then 11 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Show section can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Show section must be numeric'),
                )
            ),

            'hf_section' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Section must be no larger then 11 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Section can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Section must be numeric'),
                )
            ),
            'menu_section' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Menu section must be no larger then 11 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Menu section can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Menu section must be numeric'),
                )
            ),
            'price' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Price must be no larger then 11 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Price can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Price must be numeric'),
                )
            ),            
            'time' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 4),
                    'message' => __('Time must be no larger then 4 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Time can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Time must be numeric'),
                )
            ),
            'print' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 4),
                    'message' => __('Print must be no larger then 4 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Print can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Print must be numeric'),
                )
            ),
            'limited' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 4),
                    'message' => __('Limited must be no larger then 4 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Limited can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Limited must be numeric'),
                )
            ),
            'rank' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 11),
                    'message' => __('Rank must be no larger then 11 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Rank can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Rank must be numeric'),
                )
            ),
            'coupon_type' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 1),
                    'message' => __('Coupon type must be no larger then 1 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Coupon type can not empty'),
                ),
            ),
            /*
            'hp_coupon' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 4),
                    'message' => __('Coupon must be no larger then 4 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Coupon can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Coupon must be numeric'),
                )
            ),
            'coupon_memo' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 255),
                    'message' => __('Coupon memo must be no larger then 255 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Coupon memo can not empty'),
                ),
                'numeric' => array(
                    'rule' => 'numeric',
                    'message' => __('Coupon memo must be numeric'),
                )
            ),
             * 
             */
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
