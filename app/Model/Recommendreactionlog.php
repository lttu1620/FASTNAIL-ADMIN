<?php

/**
 * Recommend Reaction Log's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Recommendreactionlog extends AppModel
{
    public $name = 'Recommendreactionlog';
    public $primaryKey = 'id';
}
