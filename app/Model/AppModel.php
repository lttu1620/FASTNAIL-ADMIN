<?php

/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Model', 'Model');
App::import('Component', 'Common');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model
{
	public $useTable = false;

	/*
	  public $useTable = false;
	  public $Common = null;
	  public $Image = null;

	  public function updateField($updates = array(), $conds = array()) {
	  if (empty($updates) || empty($conds)) return false;
	  $sql = "UPDATE {$this->table} SET ";
	  foreach ($updates as $field => $value) {
	  $sql .= "`{$field}`='{$value}'";
	  }
	  $sql .= " WHERE ";
	  if (is_string($conds)) {
	  $sql .= "{$conds}";
	  } else {
	  foreach ($conds as $cond) {
	  $sql .= $cond;
	  }
	  }
	  if ($this->query($sql) === false) {
	  return false;
	  }
	  return true;
	  }

	  public function batchInsert($data, $updates = array(), $ignore = '') {
	  if (!empty($ignore))
	  $ignore = 'IGNORE';
	  $inserts = $field = array();
	  foreach ($data as $i => $row) {
	  $insert = array();
	  foreach ($row as $key => $val) {
	  if ($i == 0)
	  $field[] = "`{$key}`";
	  if ($val != 'NOW()') {
	  $val = "'{$val}'";
	  }
	  $insert[] = $val;
	  }
	  $inserts[] = "(" . implode(',', $insert) . ")";
	  }
	  if (!empty($inserts)) {
	  $sql = " INSERT {$ignore} INTO {$this->table}(" . implode(",", $field) . ")";
	  $sql .= " VALUES " . implode(",", $inserts);
	  if (!empty($updates)) {
	  $sql .= " ON DUPLICATE KEY UPDATE " . implode(",", $updates);
	  }
	  if ($this->query($sql) === false) {
	  return false;
	  }
	  return true;
	  }
	  return false;
	  }

	  public function delete($id = NULL, $cascade = true) {
	  if (parent::delete($id, $cascade) === false) {
	  return false;
	  }
	  return true;
	  }

	  public function active($id, $value = 1) {
	  if (empty($id)) return false;
	  if (is_array($id)) $id = implode(',', $id);
	  return $this->updateField(array('is_active' => $value), "{$this->primaryKey} IN ({$id})");
	  }

	  public function inactive($id) {
	  return $this->active($id, 0);
	  }
	 */
         /**
	 * Get CommonComponent object.
	 *
         * @author thailh
	 * @return CommonComponent Returns the CommonComponent object.
	 */
	public static function getCommonComponent()
	{
		return new CommonComponent(new ComponentCollection());
	}
     /**
     * Truncate string.
     *
     * @author thailvn
     * @param string $text Input string.
     * @param int $length Length.
     * @param object $options See more String::truncate.
     * @return string Returns the string.
     */
    public static function truncate($text, $length = 100, $options = array()) {
        return static::getCommonComponent()->truncate($text, $length, $options);
    }
     /**
     * Set validate cause error.
     *
     * @author thailvn
     * @param array $error Input array.
     * @return array Returns the array.
     */
    public function setValidationErrors($errors = array())
	{
        if (empty($errors)) {
            return true;
        }
        $result = array();
        if (!empty($errors)) {
            foreach ($errors as $field => $error) {
                $result[$field] = array(
                    key($error) => __($error[key($error)])
                );
            }
        }
        $this->validationErrors = $result;
	}

     /**
     * katakana method. Function to check katakana string
     * @created         : 2015-04-13
     * @author          : khoatx
     * @param           : string $check
     * @exception       : non
     * @return          : boolean
     */
    public function katakana($check) {
        $value = array_values($check);
        $value = $value[0];
        $spaces = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/;
        $pattern = "/^([{$spaces}ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂ"
            . "ッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロ"
            . "ヮワヰヱヲンヴヵヶヷヸヹヺ・ーヽヾ]+)$/u";
        return preg_match($pattern, $value);
    }

    public function phone_format1($check) {
        $value = array_values($check);    
        if (empty($value[0])) return false;
        $value = $value[0];
        $pattern = '/^\d{2,5}\-\d{3,4}\-\d{3,4}+$/'; 
        return preg_match($pattern, $value);
    }
    
    public function phone_format2($check) {
        $value = array_values($check);     
        if (empty($value[0])) return false;
        $value = $value[0];
        $ok = $this->phone_format1($check);
        if (!$ok) {
            $pattern = '/^\d{10,11}+$/'; 
            $ok = preg_match($pattern, $value);
        }
        return $ok;
    }    
    
}
