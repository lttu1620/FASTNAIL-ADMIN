<?php

/**
 * ShopGroup of model.
 *
 * @package Model
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Surveylog extends AppModel {

    public $name = 'Surveylog';
    public $table = 'survey_logs';
    public $primaryKey = 'id';
}
