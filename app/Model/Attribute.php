<?php

/**
 * Attribute of model.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Attribute extends AppModel {

    public $name = 'Attribute';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author diennvt
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateAttributeInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 128),
                    'message' => __('Name must be no larger then 128 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                )
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to upload image_url.
     *
     * @author diennvt
     * @return bool Returns the boolean.
     */
    public function checkUploadImage_url() {
        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile("Attribute.image_url");
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }
}
