<?php

/**
 * Survey's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Survey extends AppModel
{
    public $name = 'Survey';
    public $table = 'Surveys';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns the boolean
     */
    public function validateInsertUpdate($data)
    {
        $this->set($data[$this->name]);
        $this->validate = array(
            'order_id' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Order id can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                ),
            ),
            'title' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Title can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 45),
                    'message' => __('Between 1 to 45 characters')
                ),
            ),
            'url' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Url can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 255),
                    'message' => __('Between 1 to 255 characters')
                ),
            ),
            'started' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Started can not empty'),
                )
            ),
            'finished' => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Started can not empty'),
                )
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
