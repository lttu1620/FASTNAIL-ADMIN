<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

//App::import('Component', 'Cookie');
//$cookie = new CookieComponent(new ComponentCollection());
Router::connect('/login', array('controller' => 'pages', 'action' => 'login'));
Router::connect('/session/*', array('controller' => 'pages', 'action' => 'session'));
Router::connect('/', array('controller' => 'orders', 'action' => 'seat', 'seat'));
Router::connect('/orders/staff', array('controller' => 'pages', 'action' => 'index', 'index'));
Router::connect('/fblogin', array('controller' => 'pages', 'action' => 'fblogin'));
Router::connect('/logout', array('controller' => 'pages', 'action' => 'logout'));
Router::connect('/register', array('controller' => 'pages', 'action' => 'register'));
Router::connect('/register/profile', array('controller' => 'pages', 'action' => 'registerprofile'));
Router::connect('/register/company', array('controller' => 'pages', 'action' => 'registercompany'));
Router::connect('/register/active', array('controller' => 'pages', 'action' => 'registeractive'));
Router::connect('/register/approve', array('controller' => 'pages', 'action' => 'registerapprove'));
Router::connect('/pending', array('controller' => 'pages', 'action' => 'pending'));
Router::connect('/forgetpassword', array('controller' => 'pages', 'action' => 'forgetpassword'));
Router::connect('/sendmailnotification', array('controller' => 'pages', 'action' => 'sendmailnotification'));
Router::connect("/lp", array('controller' => 'pages', 'action' => 'lp'));
Router::connect("/users/companyinformation/*", array('controller' => 'companies', 'action' => 'update'));
Router::connect("/contact", array('controller' => 'pages', 'action' => 'contact'));
Router::connect("/privacypolicy", array('controller' => 'pages', 'action' => 'privacypolicy'));
Router::connect("/paints", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/paints/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/flowers", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/flowers/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/genres", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/genres/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/powders", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/powders/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/scenes", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/scenes/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/stones", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/stones/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/keywords", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/keywords/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/bullions", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/bullions/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/holograms", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/holograms/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/colors", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/colors/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/colorjells", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/colorjells/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/colorjells", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/colorjells/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/shells", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/shells/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/designs", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/designs/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/ramejells", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/ramejells/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/tags", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/tags/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/areas", array('controller' => 'attributes', 'action' => 'index'));
Router::connect("/areas/update/*", array('controller' => 'attributes', 'action' => 'update'));
Router::connect("/medicalchart/*", array('controller' => 'ajax', 'action' => 'medicalchartdialog'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
//Router::connect('/register', array('controller' => 'pages', 'action' => 'register'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
