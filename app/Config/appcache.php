<?php

class cacheObj {

    public $key = null;
    public $seconds = 86400; // 24*60*60

    public function __construct($key = null, $seconds = null) {
        $this->key = $key;
        $this->seconds = $seconds;
    }
}

Configure::write('prices_all', new cacheObj('prices_all', 60 * 60));
Configure::write('bullions_all', new cacheObj('bullions_all', 60 * 60));
Configure::write('colors_all', new cacheObj('colors_all', 60 * 60));
Configure::write('designs_all', new cacheObj('designs_all', 60 * 60));
Configure::write('flowers_all', new cacheObj('flowers_all', 60 * 60));
Configure::write('genres_all', new cacheObj('genres_all', 60 * 60));
Configure::write('holograms_all', new cacheObj('holograms_all', 60 * 60));
Configure::write('materials_all', new cacheObj('materials_all', 60 * 60));
Configure::write('paints_all', new cacheObj('paints_all', 60 * 60));
Configure::write('powders_all', new cacheObj('powders_all', 60 * 60));
Configure::write('ramejells_all', new cacheObj('ramejells_all', 60 * 60));
Configure::write('scenes_all', new cacheObj('scenes_all', 60 * 60));
Configure::write('shells_all', new cacheObj('shells_all', 60 * 60));
Configure::write('stones_all', new cacheObj('stones_all', 60 * 60));
Configure::write('tags_all', new cacheObj('tags_all', 60 * 60));
Configure::write('shops_all', new cacheObj('shops_all', 60 * 60));
Configure::write('nailists_all', new cacheObj('nailists_all', 60 * 60));
Configure::write('nails_all', new cacheObj('nails_all', 60 * 60));
Configure::write('items_all', new cacheObj('items_all', 60 * 60));
Configure::write('shopgroups_all', new cacheObj('shopgroups_all', 60 * 60));
Configure::write('prefectures_all', new cacheObj('prefectures_all', 60 * 60));
Configure::write('admins_all', new cacheObj('admins_all', 60 * 60));
Configure::write('users_all', new cacheObj('users_all', 60 * 60));
Configure::write('areas_all', new cacheObj('areas_all', 60 * 60));
Configure::write('surveys_all', new cacheObj('surveys_all', 60 * 60));
Configure::write('devices_all', new cacheObj('devices_all', 60 * 60));
Configure::write('services_all', new cacheObj('services_all', 60 * 60));

