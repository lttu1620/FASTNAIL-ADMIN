<?php

Configure::write('seo', array(
    'default' => array(
        'title' => __('FASTNAIL admin'),
        'description' => '',
        'keywords' => '',
    ),
    'admins' => array(
        'index' => array(
            'title' => __('Admin list'),
        ),
        'password' => array(
            'title' => __('Change password'),
        ),
        'profile' => array(
            'title' => __('Change profile'),
        ),
        'update' => array(
            'title' => __('Add/update Admin'),
        )
    ),
    'adminsettings' => array(
        'index' => array(
            'title' => __('Admin settings'),
        )
    ),
    'attributes' => array(
        'index' => array(
            'title' => __('Attributes list'),
        ),
        'update' => array(
            'title' => __('Add/update attributes'),
        )
    ),
    'carts' => array(
        'index' => array(
            'title' => __('Cart list'),
        ),
        'update' => array(
            'title' => __('Add/update cart'),
        )
    ),
    'categories' => array(
        'follower' => array(
            'title' => __('Category follower list'),
        ),
        'index' => array(
            'title' => __('Category list'),
        ),
        'update' => array(
            'title' => __('Add/update Category'),
        )
    ),
    'claims' => array(
        'index' => array(
            'title' => __('Claim list'),
        ),
        'update' => array(
            'title' => __('Add/update Claim'),
        )
    ),
    'contacts' => array(
        'index' => array(
            'title' => __('Contact list'),
        ),
        'update' => array(
            'title' => __('Add/update contact'),
        )
    ),
    'devices' => array(
        'index' => array(
            'title' => __('Device list'),
        ),
        'update' => array(
            'title' => __('Add/update device'),
        )
    ),
    'items' => array(
        'index' => array(
            'title' => __('Item list'),
        ),
        'update' => array(
            'title' => __('Add/update item'),
        )
    ),
    'loginlogs' => array(
        'index' => array(
            'title' => __('Login log list'),
        )
    ),
    'mailsendlogs' => array(
        'index' => array(
            'title' => __('Mail send log list'),
        )
    ),
    'materials' => array(
        'index' => array(
            'title' => __('Material list'),
        ),
        'update' => array(
            'title' => __('Add/update material'),
        )
    ),
    'nailists' => array(
        'index' => array(
            'title' => __('Nailist list'),
        ),
        'update' => array(
            'title' => __('Add/update nailist'),
        )
    ),
    'nails' => array(
        'index' => array(
            'title' => __('Nail list'),
        ),
        'update' => array(
            'title' => __('Add/update nail'),
        ),
        'detail' => array(
            'title' => __('Nail detail'),
        )
    ),
    'orderdailylimits' => array(
        'calendar' => array(
            'title' => __('Order daily limit calendar'),
        )
    ),
    'orders' => array(
        'index' => array(
            'title' => __('Order list'),
        ),
        'lists' => array(
            'title' => '台帳',
        ),
        'update' => array(
            'title' => __('Add/update order'),
        ),
        'calendar' => array(
            'title' => __('Calendar'),
        ),
        'seat' => array(
            'title' => '空き枠チャート',
        )
    ),
    'prefectures' => array(
        'index' => array(
            'title' => __('Prefecture list'),
        ),
        'update' => array(
            'title' => __('Add/update prefecture'),
        )
    ),
    'pages' => array(
        'index' => array(
            'title' => '指名チャート',
        ),
        'display' => array(
            'title' => __('Display news feed'),
        ),
        'login' => array(
            'title' => __('Sign In'),
        ),
        'fblogin' => array(
            'title' => __('Login facebook'),
        ),
        'register' => array(
            'title' => __('Register New Membership'),
        ),
        'logout' => array(
            'title' => __('Logout'),
        ),
        'pending' => array(
            'title' => __('Notice'),
        ),
        'forgetpassword' => array(
            'title' => __('Forget password'),
        ),
        'newpassword' => array(
            'title' => __('New password'),
        ),
        'lp' => array(
            'title' => 'キャプチャー｜大学生と企業の“イマ”を掴むニュースアプリ',
        ),
        'contact' => array(
            'title' => __('Contact form'),
        ),
        'privacypolicy' => array(
            'title' => 'プライバシーポリシー - キャプチャー｜大学生と企業の“イマ”を掴むニュースアプリ',
        )
    ),
    'settings' => array(
        'index' => array(
            'title' => __('Settings list'),
        ),
        'update' => array(
            'title' => __('Add/update settings'),
        )
    ),
    'shopgroups' => array(
        'index' => array(
            'title' => __('Shop group list'),
        ),
        'update' => array(
            'title' => __('Add/update shop group'),
        )
    ),
    'shops' => array(
        'index' => array(
            'title' => __('Shop list'),
        ),
        'update' => array(
            'title' => __('Add/update shop'),
        ),
        'customer' => array(
            'title' => '顧客管理',
        )
    ),
    'startlogs' => array(
        'index' => array(
            'title' => __('Start log list'),
        )
    ),
    'statistics' => array(
        'index' => array(
            'title' => __('Statistic'),
        ),
        'users' => array(
            'title' => __('Statistic user'),
        )
    ),
    'surveylogs' => array(
        'index' => array(
            'title' => __('Survey log list'),
        )
    ),
    'surveys' => array(
        'index' => array(
            'title' => __('Survey list'),
        ),
        'update' => array(
            'title' => __('Add/update survey'),
        )
    ),
    'tags' => array(
        'index' => array(
            'title' => __('Tag list'),
        ),
        'update' => array(
            'title' => __('Add/update tag'),
        )
    ),
    'users' => array(
        'index' => array(
            'title' => __('User list'),
        ),
        'update' => array(
            'title' => __('Register user'),
        ),
        'profile' => array(
            'title' => __('Update user profile'),
        ),
        'profileinformation' => array(
            'title' => __('Update user information'),
        ),
        'recruiterinformation' => array(
            'title' => __('Update recruiter profile'),
        ),
        'facebookinformation' => array(
            'title' => __('Update user facebook information'),
        ),
        'password' => array(
            'title' => __('Change password'),
        ),
        'newsfeedfavorites' => array(
            'title' => __('News feed favorites list'),
        )
    ),
    'usersettings' => array(
        'index' => array(
            'title' => __('User settings'),
        )
    ),
    'system' => array(
        'ps' => array(
            'title' => __('Process list')
        )
    )
));

