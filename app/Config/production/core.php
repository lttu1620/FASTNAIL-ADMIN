<?php
date_default_timezone_set('Asia/Tokyo');
Configure::write('Facebook.appId', '674403482679977');
Configure::write('Facebook.appSecret', 'ef05554516bac43dd0aecdd0621b0ac8');
Configure::write('API.Host', 'http://api.fastnail.town/');
Configure::write('ADMIN.Host', 'https://admin.fastnail.town/');
Configure::write('FE.Host', 'https://fastnail.town/');
Configure::write('Config.img_url', 'https://img.fastnail.town/');
Configure::write('Config.HTTPS', true);
Configure::write('debug', 0);
if (file_exists(__DIR__ . DS . 'cache.php')) {
    include __DIR__ . DS . 'cache.php';
}
if (isset($_SERVER['SERVER_NAME']) && file_exists(__DIR__ . DS . $_SERVER['SERVER_NAME'] . '.php')) {
    include __DIR__ . DS . $_SERVER['SERVER_NAME'] . '.php';
}
Configure::write('Session', array(
    'defaults' => 'cache',
    'cookie' => 'BEFastNail',
    'timeout' => 24*60,  // 24*60 minutes = 1 day
    'handler' => array(
        'cache' => 'Redis',       
    )
));
