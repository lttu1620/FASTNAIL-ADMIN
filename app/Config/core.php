<?php

include_once ROOT . "/app/Config/errorcode.php";
include_once ROOT . "/app/Config/appacl.php";
include_once ROOT . "/app/Config/apiurl.php";

/**
 * This is core configuration file.
 *
 * Use it to configure core behavior of Cake.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * CakePHP Debug Level:
 *
 * Production Mode:
 * 	0: No error messages, errors, or warnings shown. Flash messages redirect.
 *
 * Development Mode:
 * 	1: Errors and warnings shown, model caches refreshed, flash messages halted.
 * 	2: As in 1, but also with full debug messages and SQL output.
 *
 * In production mode, flash messages redirect after a time interval.
 * In development mode, you need to click the flash message to continue.
 */
Configure::write('debug', 2);

/**
 * Configure the Error handler used to handle errors for your application. By default
 * ErrorHandler::handleError() is used. It will display errors using Debugger, when debug > 0
 * and log errors with CakeLog when debug = 0.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle errors. You can set this to any callable type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `level` - integer - The level of errors you are interested in capturing.
 * - `trace` - boolean - Include stack traces for errors in log files.
 *
 * @see ErrorHandler for more information on error handling and configuration.
 */
Configure::write('Error', array(
    'handler' => 'ErrorHandler::handleError',
    'level' => E_ALL & ~E_DEPRECATED,
    'trace' => true
));

/**
 * Configure the Exception handler used for uncaught exceptions. By default,
 * ErrorHandler::handleException() is used. It will display a HTML page for the exception, and
 * while debug > 0, framework errors like Missing Controller will be displayed. When debug = 0,
 * framework errors will be coerced into generic HTTP errors.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle exceptions. You can set this to any callback type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `renderer` - string - The class responsible for rendering uncaught exceptions. If you choose a custom class you
 *   should place the file for that class in app/Lib/Error. This class needs to implement a render method.
 * - `log` - boolean - Should Exceptions be logged?
 * - `skipLog` - array - list of exceptions to skip for logging. Exceptions that
 *   extend one of the listed exceptions will also be skipped for logging.
 *   Example: `'skipLog' => array('NotFoundException', 'UnauthorizedException')`
 *
 * @see ErrorHandler for more information on exception handling and configuration.
 */
Configure::write('Exception', array(
    'handler' => 'ErrorHandler::handleException',
    'renderer' => 'AppExceptionRenderer', // ExceptionRenderer
    'log' => true
));

/**
 * Application wide charset encoding
 */
Configure::write('App.encoding', 'UTF-8');

/**
 * To configure CakePHP *not* to use mod_rewrite and to
 * use CakePHP pretty URLs, remove these .htaccess
 * files:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * And uncomment the App.baseUrl below. But keep in mind
 * that plugin assets such as images, CSS and JavaScript files
 * will not work without URL rewriting!
 * To work around this issue you should either symlink or copy
 * the plugin assets into you app's webroot directory. This is
 * recommended even when you are using mod_rewrite. Handling static
 * assets through the Dispatcher is incredibly inefficient and
 * included primarily as a development convenience - and
 * thus not recommended for production applications.
 */
//Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * To configure CakePHP to use a particular domain URL
 * for any URL generation inside the application, set the following
 * configuration variable to the http(s) address to your domain. This
 * will override the automatic detection of full base URL and can be
 * useful when generating links from the CLI (e.g. sending emails)
 */
//Configure::write('App.fullBaseUrl', 'http://example.com');
//
if (in_array(env('HTTP_HOST'), array('localhost', '127.0.0.1'))) {
    Configure::write('App.fullBaseUrl', 'http://localhost');
}
/**
 * Web path to the public images directory under webroot.
 * If not set defaults to 'img/'
 */
//Configure::write('App.imageBaseUrl', 'img/');

/**
 * Web path to the CSS files directory under webroot.
 * If not set defaults to 'css/'
 */
//Configure::write('App.cssBaseUrl', 'css/');

/**
 * Web path to the js files directory under webroot.
 * If not set defaults to 'js/'
 */
//Configure::write('App.jsBaseUrl', 'js/');

/**
 * Uncomment the define below to use CakePHP prefix routes.
 *
 * The value of the define determines the names of the routes
 * and their associated controller actions:
 *
 * Set to an array of prefixes you want to use in your application. Use for
 * admin or other prefixed routes.
 *
 * 	Routing.prefixes = array('admin', 'manager');
 *
 * Enables:
 * 	`admin_index()` and `/admin/controller/index`
 * 	`manager_index()` and `/manager/controller/index`
 *
 */
//Configure::write('Routing.prefixes', array('admin'));

/**
 * Turn off all caching application-wide.
 *
 */
//Configure::write('Cache.disable', true);

/**
 * Enable cache checking.
 *
 * If set to true, for view caching you must still use the controller
 * public $cacheAction inside your controllers to define caching settings.
 * You can either set it controller-wide by setting public $cacheAction = true,
 * or in each action using $this->cacheAction = true.
 *
 */
//Configure::write('Cache.check', true);

/**
 * Enable cache view prefixes.
 *
 * If set it will be prepended to the cache name for view file caching. This is
 * helpful if you deploy the same application via multiple subdomains and languages,
 * for instance. Each version can then have its own view cache namespace.
 * Note: The final cache file name will then be `prefix_cachefilename`.
 */
//Configure::write('Cache.viewPrefix', 'prefix');

/**
 * Session configuration.
 *
 * Contains an array of settings to use for session configuration. The defaults key is
 * used to define a default preset to use for sessions, any settings declared here will override
 * the settings of the default config.
 *
 * ## Options
 *
 * - `Session.cookie` - The name of the cookie to use. Defaults to 'CAKEPHP'
 * - `Session.timeout` - The number of minutes you want sessions to live for. This timeout is handled by CakePHP
 * - `Session.cookieTimeout` - The number of minutes you want session cookies to live for.
 * - `Session.checkAgent` - Do you want the user agent to be checked when starting sessions? You might want to set the
 *    value to false, when dealing with older versions of IE, Chrome Frame or certain web-browsing devices and AJAX
 * - `Session.defaults` - The default configuration set to use as a basis for your session.
 *    There are four builtins: php, cake, cache, database.
 * - `Session.handler` - Can be used to enable a custom session handler. Expects an array of callables,
 *    that can be used with `session_save_handler`. Using this option will automatically add `session.save_handler`
 *    to the ini array.
 * - `Session.autoRegenerate` - Enabling this setting, turns on automatic renewal of sessions, and
 *    sessionids that change frequently. See CakeSession::$requestCountdown.
 * - `Session.ini` - An associative array of additional ini values to set.
 *
 * The built in defaults are:
 *
 * - 'php' - Uses settings defined in your php.ini.
 * - 'cake' - Saves session files in CakePHP's /tmp directory.
 * - 'database' - Uses CakePHP's database sessions.
 * - 'cache' - Use the Cache class to save sessions.
 *
 * To define a custom session handler, save it at /app/Model/Datasource/Session/<name>.php.
 * Make sure the class implements `CakeSessionHandlerInterface` and set Session.handler to <name>
 *
 * To use database sessions, run the app/Config/Schema/sessions.php schema using
 * the cake shell command: cake schema create Sessions
 *
 */
Configure::write('Session', array(
    'defaults' => 'php',
    'cookie' => 'BEFastNail',
    'timeout' => 600,
));

/**
 * A random string used in security hashing methods.
 */
Configure::write('Security.salt', 'DYhG93b0qyJfIxfs2guVoUubWwvniR2G0FgaC9mi');
Configure::write('Security.salt', '123456');

/**
 * A random numeric string (digits only) used to encrypt/decrypt strings.
 */
Configure::write('Security.cipherSeed', '76859309657453542496749683645');
Configure::write('Security.cipherSeed', '123456');

/**
 * Apply timestamps with the last modified time to static assets (js, css, images).
 * Will append a query string parameter containing the time the file was modified. This is
 * useful for invalidating browser caches.
 *
 * Set to `true` to apply timestamps when debug > 0. Set to 'force' to always enable
 * timestamping regardless of debug value.
 */
//Configure::write('Asset.timestamp', true);

/**
 * Compress CSS output by removing comments, whitespace, repeating tags, etc.
 * This requires a/var/cache directory to be writable by the web server for caching.
 * and /vendors/csspp/csspp.php
 *
 * To use, prefix the CSS link URL with '/ccss/' instead of '/css/' or use HtmlHelper::css().
 */
//Configure::write('Asset.filter.css', 'css.php');

/**
 * Plug in your own custom JavaScript compressor by dropping a script in your webroot to handle the
 * output, and setting the config below to the name of the script.
 *
 * To use, prefix your JavaScript link URLs with '/cjs/' instead of '/js/' or use JsHelper::link().
 */
//Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * The class name and database used in CakePHP's
 * access control lists.
 */
Configure::write('Acl.classname', 'DbAcl');
Configure::write('Acl.database', 'default');

/**
 * Uncomment this line and correct your server timezone to fix
 * any date & time related errors.
 */
//date_default_timezone_set('UTC');

/**
 * `Config.timezone` is available in which you can set users' timezone string.
 * If a method of CakeTime class is called with $timezone parameter as null and `Config.timezone` is set,
 * then the value of `Config.timezone` will be used. This feature allows you to set users' timezone just
 * once instead of passing it each time in function calls.
 */
//Configure::write('Config.timezone', 'Europe/Paris');

/**
 * Cache Engine Configuration
 * Default settings provided below
 *
 * File storage engine.
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'File', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
 * 		'prefix' => 'cake_', //[optional]  prefix every cache file with this string
 * 		'lock' => false, //[optional]  use file locking
 * 		'serialize' => true, //[optional]
 * 		'mask' => 0664, //[optional]
 * 	));
 *
 * APC (http://pecl.php.net/package/APC)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Apc', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 	));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Xcache', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional] prefix every cache file with this string
 * 		'user' => 'user', //user from xcache.admin.user settings
 * 		'password' => 'password', //plaintext password (xcache.admin.pass)
 * 	));
 *
 * Memcached (http://www.danga.com/memcached/)
 *
 * Uses the memcached extension. See http://php.net/memcached
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Memcached', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 		'servers' => array(
 * 			'127.0.0.1:11211' // localhost, default port 11211
 * 		), //[optional]
 * 		'persistent' => 'my_connection', // [optional] The name of the persistent connection.
 * 		'compress' => false, // [optional] compress data in Memcached (slower, but uses less memory)
 * 	));
 *
 *  Wincache (http://php.net/wincache)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Wincache', //[required]
 * 		'duration' => 3600, //[optional]
 * 		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 	));
 */

Configure::write('API.rewriteUrl', array(
    '/companies\/follower\/(\d+)\/disable/' => 'followcompanies/disable',
    '/companies\/follower\/disable/' => 'followcompanies/disable',
    '/categories\/follower\/(\d+)\/disable/' => 'followcategories/disable',
    '/subcategories\/follower\/(\d+)\/disable/' => 'followsubcategories/disable',
    '/newsfeedfavorites\/index\/(\d+)\/disable/' => 'newsfeedfavorites/disable',
));

Configure::write('Config.language', 'jpn');
Configure::write('Config.pageSize', 10);
Configure::write('Config.StrAll', __('All'));
Configure::write('Config.StrChooseOne', __('-- Choose one --'));
Configure::write('Config.searchPageSize', array(
    10 => 10,
    20 => 20,
    30 => 30,
    40 => 40,
    50 => 50,
    60 => 60,
    70 => 70,
    80 => 80,
    90 => 90,
    100 => 100,
));
Configure::write('Config.searchStatus', array(
    0 => __('Enabled'),
    1 => __('Disabled')
));
Configure::write('Config.statusSendLog', array(
    1 => __('Success'),
    0 => __('Fail')
));
Configure::write('Config.searchGender', array(
    '0' => 'なし',
    '1' => __('Male'),
    '2' => __('Female'),
));
Configure::write('Config.searchMember', array(
    '1' => __('Member'),
    '0' => __('Guest'),
));
Configure::write('Config.searchChartType', array(
    'line' => __('Line chart'),
    'column' => __('Column chart'),
    'area' => __('Area chart'),
));
Configure::write('Config.searchChartMode', array(
    'day' => __('Day'),
    'week' => __('Week'),
    'month' => __('Month'),
));
Configure::write('Config.SettingType', array(
    'global' => __('global'),
    'admin' => __('admin'),
    'user' => __('user'),  
));
Configure::write('Config.SettingDataType', array(
    'number' => __('number'),
    'textarea' => __('textarea'),
    'string' => __('string'),
    'boolean' => __('boolean'),
    'image' => __('image')
));
Configure::write('Config.AdminType', array(
    0 => __('Shop admin'),
    1 => __('System admin')
));
Configure::write('Config.searchAdminType', array(
    0 => __('Shop'),
    1 => __('Admin')
));
Configure::write('Config.searchColor', array(
    '#EEEEEE' => 'Default',
    '#000000' => 'Black',
    '#FFFFFF' => 'White',
    '#FF0000' => 'Red',
    '#FFFF00' => 'Yellow',
    '#008000' => 'Green',
    '#0000FF' => 'Blue',
    '#00FFFF' => 'Aqua'
));

Configure::write('Facebook.appId', '674403482679977');
Configure::write('Facebook.appSecret', 'ef05554516bac43dd0aecdd0621b0ac8');

Configure::write('App.jsBaseUrl', 'js/');
Configure::write('App.cssBaseUrl', 'css/');
Configure::write('App.imageBaseUrl', 'img/');
Configure::write('App.registExpire', 2 * 24 * 60 * 60); // 2 days
// Config type of user_activation
Configure::write('Config.UserActivationRegistType', array(
    'registerApprove' => 'register_approve',
    'forgetPassword' => 'forget_password',
    'registerCompany' => 'register_company',
    'registerUser' => 'register_user',
    'registerRecruiter' => 'register_recruiter'
));

Configure::write('Config.searchDevice', array(
    1 => __('IOS'),
    2 => __('Android'),
    3 => __('Web')
));
Configure::write('Config.showSection', array(
    '1' => 1,
    '2' => 2,
    '3' => 3,
    '4' => 4,
    '5' => 5,
    '6' => 6,
    '7' => 7,
    '8' => 8,
    '9' => 9,
    '10' => 10,
));
Configure::write('Config.ReservationInterval', array(
    '15' => 15,
    '30' => 30,
));
Configure::write('Config.hfSection', array(
    '0' => 'なし',
    '1' => __('Hand'),
    '2' => __('Foot'),
    '3' => __('Hand&Foot'),
    
));
Configure::write('Config.BooleanValue', array( // with Yes default
    1 => __('Yes'),
    0 => __('No')
));
Configure::write('Config.booleanValue', array( // with No default
    0 => __('No'),
    1 => __('Yes')
));
Configure::write('Config.searchBooleanValue', array(
    '' => __('Select'),
    1 => __('Yes'),
    0 => __('No')
));
Configure::write('Config.BoughtItemType', array( // with Yes default
    1 => __('Bought items'),
    2 => __('Canceled items'),
    3 => __('Completed items'),
    4 => __('Scanned QR items'),
    5 => __('Not scan QR items'),
));
Configure::write('Config.ItemTax', 0.08);
Configure::write('Config.BookingDuration', array(
	'0' => '0分',
    '900'   => '15分',
    '1800'  => '30分',
    '2700'  => '45分',
    '3600'  => '1時間',
    '4500'  => '1時間 15分',
    '5400'  => '1時間 30分',
    '6300'  => '1時間 45分',
    '7200'  => '2時間',
    '8100'  => '2時間 15分',
    '9000'  => '2時間 30分',
    '9900'  => '2時間 45分',
    '10800' => '3時間',
	'11700' => '3時間 15分'	,
    '12600' => '3時間 30分',
	'13500' => '3時間 45分',	
    '14400' => '4時間',
	'15300' => '4時間 15分'	,
    '16200' => '4時間 30分',
	'17100' => '4時間 45分',	
    '18000' => '5時間',
	'18900' => '5時間 15分'	,
	'19800' => '5時間 30分',
	'20700' => '5時間 45分',	
    '21600' => '6時間',
	'22500' => '6時間 15分'	,
	'23400' => '6時間 30分',
	'24300' => '6時間 45分',
    '25200' => '7時間',
	'26100' => '7時間 15分'	,
	'27000' => '7時間 30分',
	'27900' => '7時間 45分',
	'28800' => '8時間',
	'29700' => '8時間 15分'	,
	'30600' => '8時間 30分',
	'31500' => '8時間 45分',
));
Configure::write('Config.BookingMedia', array(
    '予約サイト' => array(
        'beautycheck' => 'BeautyCheck',
        'cpon'        => 'クーポンランド',
        'hotpepper'   => 'ホットペッパー',
        'luxa'        => 'Luxa',
    ),
    'きっかけ'  => array(
        'customer'  => 'お客様紹介',
        'staff'     => 'スタッフ紹介',
        'concierge' => 'コンシェルジュ',
        'travel'    => '旅行会社',
        'hotel'     => 'ホテル',
        'dm'        => 'DM',
        'mail_mag'  => 'メルマガ',
        'media'     => 'メディア掲載',
        'marketing' => '広告宣伝',
        'other'     => 'その他',
    )
));
Configure::write('Config.BookingSource', array(
    'phone'     => '電話予約',
    'web'       => 'ウェブ',
    'email'     => 'Eメール',
    'in_person' => '来店予約',
    'walk_in'   => 'ウォークイン',
    'app'       => 'アプリ',
    'other'     => 'その他',
));

Configure::write('Config.VisitElement', array(
    0 => 'なし',
    1 => 'PCホームページ',
    2 => '携帯ホームページ',
    3 => 'スマホホームページ',
    4 => 'ご友人紹介',
    5 => '店舗看板',
    6 => 'テレビCM',
    7 => '新聞折込',
    8 => '商品券',
    9 => 'ホットペッパー',
    10 => 'クーポンランド',
    11 => '電車・バス広告',
    12 => 'シティリビング',
    13 => '手配りチラシ',
    14 => 'その他',
    15 => '既存会員'
));

Configure::write('Config.ReservationType', array(
    0 => __('-- Choose one --'),
    1 => '電話',
    2 => 'メール',
    3 => '前回来店時予約',
    4 => 'その他直接来店',
    5 => 'ネット予約',
    6 => '他店舗からのご案内',
    7 => 'Rakuten BEAUTY',
    8 => 'アプリ予約'
));

Configure::write('Config.MailOption', array(
    0 => '不要',
    1 => '希望'
));

Configure::write('Config.missionType', array(
    0 => __('News'),
    1 => __('access_link'),
    2 => __('answer_survey'),
    3 => __('capture_photo'),
    4 => __('register_from_other_site'),
));

Configure::write('Config.segmentDevice', array(
    0 => __('All'),
    1 => __('IOS'),
    2 => __('Android'),
    3 => __('Web')
));

Configure::write('Config.OnOff', array(
    0 => 'なし',
    1 => 'あり'
));

Configure::write('Config.NailType', array(
    /*0 => 'なし',*/
    1 => '今のまま整える程度',
    2 => 'ラウンド',
    3 => 'オーバル',
    4 => 'スクエアオフ'
));

Configure::write('Config.CouponType', array(
    0 => 'なし',
    1 => '新規限定クーポン',
    2 => '全員クーポン'
));

Configure::write('Config.NumberFakeNail', array(
    0 => 'なし',
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5,
    6 => 6,
    7 => 7,
    8 => 8,
    9 => 9,
    10 => 10
));

Configure::write('Config.Finger', array(
    1 => '小',
    2 => '薬',
    3 => '中',
    4 => '人',
    5 => '親'
));

Configure::write('Config.NailOff', array(
    1 => 'なし',
    2 => 'ソフトジェル',
    3 => 'ハードジェル',
    4 => 'マニキュア',
    5 => 'アクリル'
));

Configure::write('Config.NailOffPrice', array(
    1 => '',
    2 => '1本 ¥299 (¥322)〜',
    3 => '1本 ¥900 (¥972)',
    4 => '1本 ¥100 (¥108)',
    5 => '1本 ¥350 (¥378)～'
));

Configure::write('Config.FingerItemLength', array(
    1 => 'なし',
    2 => 'あり'
));

Configure::write('Config.FingerItemPrice', '1本¥1,500（¥1,620）');

Configure::write('Config.NailLength', array(
    /*0 => 'なし',*/
    1 => '今のまま整える程度',
    2 => 'のびた分だけ',
    3 => 'ぎりぎり短く',
    4 => '1mm程度短く',
    5 => '2mm程度短く',
    6 => '3mm程度短く',
));

Configure::write('Config.PayType', array(
    0 => 'なし',
    1 => '1',
    2 => '2'
));

Configure::write('Config.actionType', array(
    1 => __('Favorite'),
    2 => __('Booking')
));

Configure::write('Config.VisitSection', array(
    0 => 'なし',
    1 => '1',
    2 => '2',
    3 => '3',
    4 => '4',
    5 => '5',
    6 => '6',
    7 => '7',
    8 => '8',
    9 => '9',
    10 => '10',
));

Configure::write('Config.OrderNailistLogType', array(
    1 => '受付',
    2 => 'オフタイム「 開始」',
    3 => 'オフタイム「 終了」',
    4 => 'サービスタイム「 開始」',
    5 => 'サービスタイム「 終了」',
    6 => '決算',
));

Configure::write('Config.NewRepeat', array(
    1 => '新規',
    2 => 'リピーター'
));

Configure::write('Config.OrderService', array(
    1 => 'ハンドジェル',
    2 =>'フットジェル',
    3 =>'ハンド&フットジェル',
    4 =>'ハンドオフのみ',
    5 =>'フットオフのみ',
    6 =>'ハンドお直し',
    7 =>'フットお直し',
));

Configure::write('Config.OrderPurpose', array(
		0 => __('-- Choose one --'),
		1 => 'Hオフオン',
		2 => 'Hオーダーオン',
		3 => 'Fオフオン',
		4 => 'Fオーダーオン',
		5 => 'Hオフオン&Fオン',
		6 => 'Hオフオン&Fオフオン',
		7 => 'Hオン&Fオン',
		8 => 'Hオン&Fオフオン',
		9 => 'Hオフ',
		10 => 'Fオフ',
		11 => 'Hお直し',
		12 => 'Fお直し'
));

Configure::write('Config.deviceType', array(
    1 => __('Hand'),
    2 => __('Foot')
));

Configure::write('Config.LogLimit', 4);

Configure::write('Config.DefaultShopOpenTime', 10*60*60);
Configure::write('Config.DefaultShopCloseTime', 23*60*60);
Configure::write('Config.DefaulNormalShopTime', 30*60);
Configure::write('Config.DefaulPlusShopTime', 45*60);
Configure::write('Config.DefaulMinTime', 15*60);

Configure::write('FE.Host', 'http://localhost/oceanize/fastnail/frontend/');
Configure::write('Config.secretKey', 'F@stn@il');
Configure::write('Config.AdminOrder.Timeout', 10 * 60 + 30);
Configure::write('Config.Static', '20151001');

Configure::write('API.Timeout', 30);
Configure::write('API.TimeoutNewOrder', 2*60*1000);
Configure::write('API.secretKey', 'fastnail');

/*
* Config value and class for Status Order
*/

Configure::write('Config.StatusOrder', array(
    1 =>   '来店中' ,//đang ở tiệm
    2 =>  '来店前',//sắp tới tiệm
    3 =>  '会計終了' ,//đã thanh toán
    4 =>  '無断キャンセル',//auto cancel
    5 =>  'キャンセル' //cancel
));

Configure::write('Config.StatusOrderExport', array(
    1 => '会計済み', // paid
    2 => 'キャンセル', // cancel
    3 => '無断キャンセル', // autocancel
    4 => '受付済', // accepted order
    5 => '未受付' // not accepted
));



/*
* Config User_point_logs type
*/

Configure::write('Config.UserPointLogType', array(
    1 => 'サイトアクセス',
    2 => 'アンケートの回答',
    3 => '写真撮り',
    4 => 'サイト登録',
    5 => 'アイテム交換',   
    7 => '予約で使用',
    8 => '予約をキャンセル',
    99 => 'ビューティー登録'
));

Configure::write('Config.pointItem', array(
    '1' => __('Nails'),
    '2' => __('Item'),
    '3' => __('Service')
));

/*
* Config ShopRegion 
*/

Configure::write('Config.ShopRegion', array(
    1   => '関東',
    2   => '関西'
));

Configure::write('Config.ItemSection', array(
    1 => 'オフ',
    2 => 'リペア',
    3 => '物販',
    4 => '特典',
    5 => 'その他',   
));

Configure::write('Config.contacts.subject', array(
    'ポイントについて' => 'ポイントについて',
    'アプリについて' => 'アプリについて',
    'その他' => 'その他',  
));

$env = getenv('FUEL_ENV');
if (!$env) {
    $env = 'development';
}
include_once ('appcache.php');
include_once ROOT . "/app/Config/seo.php";
if ($env == 'test') {
    include_once ('test/core.php');
} elseif ($env == 'production') {
    include_once ('production/core.php');
} elseif ($env == 'development') {
    include_once ('development/core.php');
}

/**
 * Configure the cache handlers that CakePHP will use for internal
 * metadata like class maps, and model schema.
 *
 * By default File is used, but for improved performance you should use APC.
 *
 * Note: 'default' and other application caches should be configured in app/Config/bootstrap.php.
 *       Please check the comments in bootstrap.php for more info on the cache engines available
 *       and their settings.
 */
//$engine = 'File';

// In development mode, caches should expire quickly.
//$duration = '+999 days';
//if (Configure::read('debug') > 0) {
//    $duration = '+10 seconds';
//}

// Prefix each application on the same server with a different string, to avoid Memcache and APC conflicts.
//$prefix = 'myapp_';
//
//if (!defined('CACHE_PATH')) {
//	define('CACHE_PATH', CACHE);
//}

//Cache::config('default', array(
//    'engine' => $engine, //[required]
//    'duration' => $duration, //[optional]   
//    'path' => CACHE_PATH, //[optional] use system tmp directory - remember to use absolute path
//    'prefix' => 'cake_', //[optional]  prefix every cache file with this string
//    'lock' => false, //[optional]  use file locking   
//    'mask' => 0664, //[optional]
//));

/**
 * Configure the cache used for general framework caching. Path information,
 * object listings, and translation cache files are stored with this configuration.
 */
//Cache::config('_cake_core_', array(
//    'engine' => $engine,
//    'prefix' => $prefix . 'cake_core_',
//    'path' => CACHE_PATH . 'persistent' . DS,
//    'serialize' => ($engine === 'File'),
//    'duration' => $duration
//));

/**
 * Configure the cache for model and datasource caches. This cache configuration
 * is used to store schema descriptions, and table listings in connections.
 */
//Cache::config('_cake_model_', array(
//    'engine' => $engine,
//    'prefix' => $prefix . 'cake_model_',
//    'path' => CACHE_PATH . 'models' . DS,
//    'serialize' => ($engine === 'File'),
//    'duration' => $duration
//));