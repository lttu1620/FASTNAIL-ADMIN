<?php
Configure::write('API.url_admins_list', 'admins/list');
Configure::write('API.url_admins_detail', 'admins/detail');
Configure::write('API.url_admins_disable', 'admins/disable');
Configure::write('API.url_admins_addupdate', 'admins/addupdate');
Configure::write('API.url_admins_login', 'admins/login');
Configure::write('API.url_admins_password', 'admins/updatepassword');

Configure::write('API.url_users_all', 'users/all');
Configure::write('API.url_users_list', 'users/list');
Configure::write('API.url_users_detail', 'users/detail');
Configure::write('API.url_users_disable', 'users/disable');
Configure::write('API.url_users_addupdate', 'users/addupdate');
Configure::write('API.url_users_forgetpassword', 'users/forgetpassword');
Configure::write('API.url_users_registeremail', 'users/registeremail');
Configure::write('API.url_users_registeractive', 'users/registeractive');
Configure::write('API.url_users_login', 'users/login');
Configure::write('API.url_users_login_facebook', 'users/fblogin');
Configure::write('API.url_users_registercompany', 'users/registercompany');
Configure::write('API.url_users_token', 'users/token');
Configure::write('API.url_users_resendregisteremail', 'users/resendregisteremail');
Configure::write('API.url_users_resendregistercompany', 'users/resendregistercompany');
Configure::write('API.url_users_resendforgetpassword', 'users/resendforgetpassword');
Configure::write('API.url_users_searchinfo', 'users/searchinfo');

Configure::write('API.url_useractivations_detail', 'useractivations/detail');
Configure::write('API.url_useractivations_addupdate', 'useractivations/addupdate');
Configure::write('API.url_useractivations_disable', 'useractivations/disable');
Configure::write('API.url_useractivations_check', 'useractivations/check');

Configure::write('API.url_upload_image', 'upload/image.json');
Configure::write('API.url_upload_video', 'upload/video.json');

Configure::write('API.url_settings_all', 'settings/all');
Configure::write('API.url_settings_list', 'settings/list');
Configure::write('API.url_settings_detail', 'settings/detail');
Configure::write('API.url_settings_addupdate', 'settings/addupdate');
Configure::write('API.url_settings_multiupdate', 'settings/multiupdate');

Configure::write('API.url_usersettings_all', 'usersettings/all');
Configure::write('API.url_usersettings_disable', 'usersettings/disable');
Configure::write('API.url_usersettings_addupdate', 'usersettings/addupdate');
Configure::write('API.url_usersettings_multiupdate', 'usersettings/multiupdate');

Configure::write('API.url_adminsettings_all', 'adminsettings/all');
Configure::write('API.url_adminsettings_disable', 'adminsettings/disable');
Configure::write('API.url_adminsettings_addupdate', 'adminsettings/addupdate');
Configure::write('API.url_adminsettings_multiupdate', 'adminsettings/multiupdate');

Configure::write('API.url_shells_list', 'shells/list');
Configure::write('API.url_shells_detail', 'shells/detail');
Configure::write('API.url_shells_addupdate', 'shells/addupdate');
Configure::write('API.url_shells_disable', 'shells/disable');

Configure::write('API.url_designs_list', 'designs/list');
Configure::write('API.url_designs_detail', 'designs/detail');
Configure::write('API.url_designs_addupdate', 'designs/addupdate');
Configure::write('API.url_designs_disable', 'designs/disable');

Configure::write('API.url_paints_list', 'paints/list');
Configure::write('API.url_paints_detail', 'paints/detail');
Configure::write('API.url_paints_addupdate', 'paints/addupdate');
Configure::write('API.url_paints_disable', 'paints/disable');

Configure::write('API.url_bullions_list', 'bullions/list');
Configure::write('API.url_bullions_detail', 'bullions/detail');
Configure::write('API.url_bullions_addupdate', 'bullions/addupdate');
Configure::write('API.url_bullions_disable', 'bullions/disable');

Configure::write('API.url_colors_list', 'colors/list');
Configure::write('API.url_colors_detail', 'colors/detail');
Configure::write('API.url_colors_addupdate', 'colors/addupdate');
Configure::write('API.url_colors_disable', 'colors/disable');

Configure::write('API.url_colorjells_list', 'colorjells/list');
Configure::write('API.url_colorjells_detail', 'colorjells/detail');
Configure::write('API.url_colorjells_addupdate', 'colorjells/addupdate');
Configure::write('API.url_colorjells_disable', 'colorjells/disable');

Configure::write('API.url_flowers_list', 'flowers/list');
Configure::write('API.url_flowers_detail', 'flowers/detail');
Configure::write('API.url_flowers_addupdate', 'flowers/addupdate');
Configure::write('API.url_flowers_disable', 'flowers/disable');

Configure::write('API.url_genres_list', 'genres/list');
Configure::write('API.url_genres_detail', 'genres/detail');
Configure::write('API.url_genres_addupdate', 'genres/addupdate');
Configure::write('API.url_genres_disable', 'genres/disable');

Configure::write('API.url_holograms_list', 'holograms/list');
Configure::write('API.url_holograms_detail', 'holograms/detail');
Configure::write('API.url_holograms_addupdate', 'holograms/addupdate');
Configure::write('API.url_holograms_disable', 'holograms/disable');

Configure::write('API.url_powders_list', 'powders/list');
Configure::write('API.url_powders_detail', 'powders/detail');
Configure::write('API.url_powders_addupdate', 'powders/addupdate');
Configure::write('API.url_powders_disable', 'powders/disable');

Configure::write('API.url_scenes_list', 'scenes/list');
Configure::write('API.url_scenes_detail', 'scenes/detail');
Configure::write('API.url_scenes_addupdate', 'scenes/addupdate');
Configure::write('API.url_scenes_disable', 'scenes/disable');

Configure::write('API.url_stones_list', 'stones/list');
Configure::write('API.url_stones_detail', 'stones/detail');
Configure::write('API.url_stones_addupdate', 'stones/addupdate');
Configure::write('API.url_stones_disable', 'stones/disable');

Configure::write('API.url_keywords_list', 'keywords/list');
Configure::write('API.url_keywords_detail', 'keywords/detail');
Configure::write('API.url_keywords_addupdate', 'keywords/addupdate');
Configure::write('API.url_keywords_disable', 'keywords/disable');

Configure::write('API.url_materials_list', 'materials/list');
Configure::write('API.url_materials_detail', 'materials/detail');
Configure::write('API.url_materials_addupdate', 'materials/addupdate');
Configure::write('API.url_materials_disable', 'materials/disable');

Configure::write('API.url_ramejells_list', 'ramejells/list');
Configure::write('API.url_ramejells_detail', 'ramejells/detail');
Configure::write('API.url_ramejells_addupdate', 'ramejells/addupdate');
Configure::write('API.url_ramejells_disable', 'ramejells/disable');

Configure::write('API.url_tags_list', 'tags/list');
Configure::write('API.url_tags_detail', 'tags/detail');
Configure::write('API.url_tags_addupdate', 'tags/addupdate');
Configure::write('API.url_tags_disable', 'tags/disable');

Configure::write('API.url_nails_list', 'nails/list');
Configure::write('API.url_nails_detail', 'nails/detail');
Configure::write('API.url_nails_addupdate', 'nails/addupdate');
Configure::write('API.url_nails_disable', 'nails/disable');
Configure::write('API.url_nails_attribute', 'nails/attribute');
Configure::write('API.url_nails_price', 'nails/price');

Configure::write('API.url_bullions_all', 'bullions/all');
Configure::write('API.url_colors_all', 'colors/all');
Configure::write('API.url_designs_all', 'designs/all');
Configure::write('API.url_flowers_all', 'flowers/all');
Configure::write('API.url_genres_all', 'genres/all');
Configure::write('API.url_holograms_all', 'holograms/all');
Configure::write('API.url_materials_all', 'materials/all');
Configure::write('API.url_paints_all', 'paints/all');
Configure::write('API.url_powders_all', 'powders/all');
Configure::write('API.url_ramejells_all', 'ramejells/all');
Configure::write('API.url_scenes_all', 'scenes/all');
Configure::write('API.url_shells_all', 'shells/all');
Configure::write('API.url_stones_all', 'stones/all');
Configure::write('API.url_tags_all', 'tags/all');

Configure::write('API.url_shops_list', 'shops/list');
Configure::write('API.url_shops_detail', 'shops/detail');
Configure::write('API.url_shops_addupdate', 'shops/addupdate');
Configure::write('API.url_shops_disable', 'shops/disable');
Configure::write('API.url_shops_all', 'shops/all');
Configure::write('API.url_shops_customer', 'shops/customer');

Configure::write('API.url_nailists_list', 'nailists/list');
Configure::write('API.url_nailists_detail', 'nailists/detail');
Configure::write('API.url_nailists_addupdate', 'nailists/addupdate');
Configure::write('API.url_nailists_disable', 'nailists/disable');
Configure::write('API.url_nailists_all', 'nailists/all');

Configure::write('API.url_orders_list', 'orders/list');
Configure::write('API.url_orders_export', 'orders/export');
Configure::write('API.url_orders_update', 'orders/addupdate');
Configure::write('API.url_orders_update_calendar', 'orders/addupdatecalendar');
Configure::write('API.url_orders_adminaddupdate', 'orders/adminaddupdate');
Configure::write('API.url_orders_month_calendar', 'orders/monthcalendar');
Configure::write('API.url_orders_disable', 'orders/disable');
Configure::write('API.url_orders_detail', 'orders/detail');
Configure::write('API.url_orders_allbydate', 'orders/allbydate');
Configure::write('API.url_ordernails_all', 'ordernails/all');
Configure::write('API.url_orderitems_all', 'orderitems/all');
Configure::write('API.url_orders_detailforcalendar', 'orders/detailforcalendar');
Configure::write('API.url_orders_stopwatch', 'orders/stopwatch');
Configure::write('API.url_orders_autocancel', 'orders/autocancel');
Configure::write('API.url_orders_getmaxseatrealtime', 'orders/getmaxseatrealtime');
Configure::write('API.url_orders_undostopwatch', 'orders/undostopwatch');

Configure::write('API.url_ordernails_add','ordernails/add');

Configure::write('API.url_orderlogs_all', 'orderlogs/all');
Configure::write('API.url_orderlogs_addupdatebyorderid', 'orderlogs/addupdatebyorderid');

Configure::write('API.url_nails_all', 'nails/all');

Configure::write('API.url_items_all', 'items/all');
Configure::write('API.url_items_list', 'items/list');
Configure::write('API.url_items_addupdate', 'items/addupdate');
Configure::write('API.url_items_disable', 'items/disable');
Configure::write('API.url_items_detail', 'items/detail');

Configure::write('API.url_point_items_list', 'pointitems/list');
Configure::write('API.url_point_items_add_update', 'pointitems/addupdate');
Configure::write('API.url_point_items_disable', 'pointitems/disable');
Configure::write('API.url_point_items_detail', 'pointitems/detail');

Configure::write('API.url_claims_list', 'claims/list');
Configure::write('API.url_claims_addupdate', 'claims/addupdate');
Configure::write('API.url_claims_disable', 'claims/disable');
Configure::write('API.url_claims_detail', 'claims/detail');

Configure::write('API.url_surveys_list', 'surveys/list');
Configure::write('API.url_surveys_addupdate', 'surveys/addupdate');
Configure::write('API.url_surveys_disable', 'surveys/disable');
Configure::write('API.url_surveys_detail', 'surveys/detail');

Configure::write('API.url_orders_date_calendar', 'orders/datecalendar');
Configure::write('API.url_orders_date_calendar_data', 'orders/datecalendardata');
Configure::write('API.url_devices_list', 'devices/list');
Configure::write('API.url_devices_all', 'devices/all');
Configure::write('API.url_devices_addupdate', 'devices/addupdate');
Configure::write('API.url_devices_detail', 'devices/detail');
Configure::write('API.url_devices_disable', 'devices/disable');
Configure::write('API.url_shopgroups_list', 'shopgroups/list');
Configure::write('API.url_shopgroups_detail', 'shopgroups/detail');
Configure::write('API.url_shopgroups_addupdate', 'shopgroups/addupdate');
Configure::write('API.url_shopgroups_disable', 'shopgroups/disable');
Configure::write('API.url_shopgroups_all', 'shopgroups/all');
Configure::write('API.url_prefectures_all', 'prefectures/all');
Configure::write('API.url_mailsendlogs_list', 'mailsendlogs/list');

Configure::write('API.url_admins_all', 'admins/all');

Configure::write('API.url_prefectures_list', 'prefectures/list');
Configure::write('API.url_prefectures_detail', 'prefectures/detail');
Configure::write('API.url_prefectures_addupdate', 'prefectures/addupdate');
Configure::write('API.url_prefectures_disable', 'prefectures/disable');
Configure::write('API.url_areas_all','areas/all');
Configure::write('API.url_surveylogs_list','surveylogs/list');
Configure::write('API.url_surveys_all', 'surveys/all');

Configure::write('API.url_carts_list', 'carts/list');
Configure::write('API.url_carts_disable', 'carts/disable');
Configure::write('API.url_carts_addupdate', 'carts/addupdate');
Configure::write('API.url_carts_detail', 'carts/detail');

Configure::write('API.url_cartnailists_all', 'cartnailists/all');

Configure::write('API.url_update_timebar', 'orders/updatetimebar');
Configure::write('API.url_check_neworder', 'orders/checkneworder');

Configure::write('API.url_users_changepassword', 'users/changepassword');

Configure::write('API.url_userlogs_addupdate', 'userlogs/addupdate');
Configure::write('API.url_userlogs_list', 'userlogs/list');

Configure::write('API.url_order_devices_all', 'orderdevices/all');

Configure::write('API.url_order_daily_limit_list', 'orderdailylimits/list');
Configure::write('API.url_order_daily_limit_add_update', 'orderdailylimits/addupdate');
Configure::write('API.url_orderlogs_allforupdate', 'orderlogs/allforupdate');
Configure::write('API.url_orderlogs_nailist', 'orderlogs/nailist');

Configure::write('API.url_order_timely_limit_list', 'ordertimelylimits/list');
Configure::write('API.url_order_timely_limit_multi_update', 'ordertimelylimits/multiupdate');
Configure::write('API.url_order_timely_limit_multiupdate', 'ordertimelylimits/multiupdate');
Configure::write('API.url_ordertimelylimits_decreaselimit', 'ordertimelylimits/decreaselimit');
Configure::write('API.url_ordertimelylimits_increaselimit', 'ordertimelylimits/increaselimit');

Configure::write('API.url_orderlogs_nailist_logs_all', 'ordernailistlogs/all');
Configure::write('API.url_orders_detail_for_view', 'orders/detailforview');
Configure::write('API.url_orders_cancel', 'orders/cancel');
Configure::write('API.url_orders_vacancyminseat', 'orders/vacancyminseat');

Configure::write('API.url_system_deletecache', 'system/deletecache');
Configure::write('API.url_system_runbatch', 'system/runbatch');
Configure::write('API.url_system_ps', 'system/ps');

Configure::write('API.url_services_list', 'services/list');
Configure::write('API.url_services_disable', 'services/disable');
Configure::write('API.url_services_addupdate', 'services/addupdate');
Configure::write('API.url_services_detail', 'services/detail');
Configure::write('API.url_services_all', 'services/all');

Configure::write('API.url_point_get_list', 'pointgets/list');
Configure::write('API.url_point_get_disable', 'pointgets/disable');
Configure::write('API.url_point_get_addupdate', 'pointgets/addupdate');
Configure::write('API.url_point_get_detail', 'pointgets/detail');

Configure::write('API.url_user_beauty_view_log_list', 'userbeautyviewlogs/list');
Configure::write('API.url_user_beauty_view_log_get_cvr', 'userbeautyviewlogs/cvr');
Configure::write('API.url_user_beauty_view_log_disable', 'userbeautyviewlogs/disable');

Configure::write('API.url_userselectednaillogs_list', 'userselectednaillogs/list');

Configure::write('API.url_recommend_reaction_log_list', 'recommendreactionlogs/list');

Configure::write('API.url_beauties_list', 'beauties/list');
Configure::write('API.url_beauties_all', 'beauties/all');
Configure::write('API.url_beauties_addupdate', 'beauties/addupdate');
Configure::write('API.url_beauties_detail', 'beauties/detail');
Configure::write('API.url_beauties_disable', 'beauties/disable');

Configure::write('API.url_nailranklogs_list', 'nailranklogs/list');
Configure::write('API.url_userpointlogs_list', 'userpointlogs/list');

Configure::write('API.url_order_list_attribute', 'orders/listattribute');
Configure::write('API.url_order_update_attribute', 'orders/updateattribute');

Configure::write('API.url_bannerrecommends_list', 'bannerrecommends/list');
Configure::write('API.url_bannerrecommends_detail', 'bannerrecommends/detail');
Configure::write('API.url_bannerrecommends_addupdate', 'bannerrecommends/addupdate');
Configure::write('API.url_bannerrecommends_disable', 'bannerrecommends/disable');

Configure::write('API.url_campaigns_list', 'campaigns/list');
Configure::write('API.url_campaigns_detail', 'campaigns/detail');
Configure::write('API.url_campaigns_addupdate', 'campaigns/addupdate');
Configure::write('API.url_campaigns_disable', 'campaigns/disable');

Configure::write('API.url_approve_beauty', 'userbeautyviewlogs/disable');

Configure::write('API.url_contacts_list', 'contacts/list');
Configure::write('API.url_contacts_detail', 'contacts/detail');
Configure::write('API.url_contacts_addupdate', 'contacts/addupdate');
Configure::write('API.url_contacts_disable', 'contacts/disable');

Configure::write('API.url_user_bought_item_list', 'userboughtitems/list');
Configure::write('API.url_user_bought_item_completed', 'userboughtitems/completed');
Configure::write('API.url_user_bought_item_cancel', 'userboughtitems/cancel');

Configure::write('API.url_order_update_timebar_log_list', 'orderupdatetimebarlogs/all');

Configure::write('API.url_holiday_list', 'holidays/list');
Configure::write('API.url_holiday_add_update', 'holidays/addupdate');
Configure::write('API.url_holiday_disable', 'holidays/disable');


