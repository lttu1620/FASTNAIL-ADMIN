<?php
date_default_timezone_set('Asia/Tokyo');
Configure::write('Facebook.appId', '674403482679977');
Configure::write('Facebook.appSecret', 'ef05554516bac43dd0aecdd0621b0ac8');
Configure::write('API.Host', 'http://sv4.evolable-asia.z-hosts.com:8090/');
Configure::write('ADMIN.Host', 'http://sv4.evolable-asia.z-hosts.com:8091/');
Configure::write('FE.Host', 'http://sv4.evolable-asia.z-hosts.com:8092/');
Configure::write('Config.img_url', 'http://sv4.evolable-asia.z-hosts.com:84/');
Configure::write('Session', array(
    'defaults' => 'cache',
    'cookie' => 'BEFastNail',
    'timeout' => 24*60,  // 24*60 minutes = 1 day
    'handler' => array(
        'cache' => 'Redis',       
    )
));
if (file_exists(__DIR__ . DS . 'cache.php')) {
    include __DIR__ . DS . 'cache.php';
}
if (isset($_SERVER['SERVER_NAME']) && file_exists(__DIR__ . DS . $_SERVER['SERVER_NAME'] . '.php')) {
    include __DIR__ . DS . $_SERVER['SERVER_NAME'] . '.php';
}