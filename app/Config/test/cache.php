<?php

/**
 * Configure the cache handlers that CakePHP will use for internal
 * metadata like class maps, and model schema.
 *
 * By default File is used, but for improved performance you should use APC.
 *
 * Note: 'default' and other application caches should be configured in app/Config/bootstrap.php.
 *       Please check the comments in bootstrap.php for more info on the cache engines available
 *       and their settings.
 */
$prefix = 'fn_admin_';
$duration = '+999 days';
$engine = 'Redis'; // Redis, File

if ($engine == 'Redis') {   
    Cache::config('default', array(
        'engine' => $engine, //[required]
        'duration' => $duration, //[optional]   
        'server' => 'localhost',
        'port' => 6379,
        'persistent' => false,
        'prefix' => $prefix, //[optional]  prefix every cache file with this string   
    ));
} else {   
    define('CACHE_PATH', CACHE);
    
    Cache::config('default', array(
        'engine' => $engine, //[required]
        'duration' => $duration, //[optional]   
        'path' => CACHE_PATH, //[optional] use system tmp directory - remember to use absolute path
        'prefix' => $prefix, //[optional]  prefix every cache file with this string
        'lock' => false, //[optional]  use file locking 
    ));

    /**
     * Configure the cache used for general framework caching. Path information,
     * object listings, and translation cache files are stored with this configuration.
     */
    Cache::config('_cake_core_', array(
        'engine' => $engine,
        'prefix' => $prefix . 'cake_core_',
        'path' => CACHE_PATH . 'persistent' . DS,
        'serialize' => true,
        'duration' => $duration
    ));

    /**
     * Configure the cache for model and datasource caches. This cache configuration
     * is used to store schema descriptions, and table listings in connections.
     */
    Cache::config('_cake_model_', array(
        'engine' => $engine,
        'prefix' => $prefix . 'cake_model_',
        'path' => CACHE_PATH . 'models' . DS,
        'serialize' => true,
        'duration' => $duration
    ));
}