<?php

/**
 * class Util - support functions for Util
 *
 * @package Lib
 * @created 2015-06-17
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Util {

    /**
     * Get time
     *  
     * @author thailh
     * @param string $d string of time
     * @return int A timestamp on success, false otherwise
     */
    public static function gmtime($d) {
        if (empty($d)) {
            $d = date('Y/m/d H:i:s');
        }
        return strtotime(gmdate("M d Y H:i:s", strtotime($d)));
    }

}
