<div id='workspace'> 
    <div id='reservations-index'>
        <?php echo $this->element('headercalendar', array('dt'=> $dt)); ?>
        <div class='container-fluid' id='reservations-content'>                    
            <div data-date='<?php echo $dt?>' data-mode='default' id='res-timetable'>          
                <?php            
                if (!empty($max_real_seat)) {
                    echo $this->element('calendar',array(
                            'data'              => $data,
                            'dt'                => $dt,
                            'deviceRecord'      => $deviceRecord,
                            'max_seat'          => $max_seat,
                            'max_real_seat'     => $max_real_seat,
                            'shop_open_time'    => $shop_open_time,
                            'shop_close_time'   => $shop_close_time,
                            'AppUI'             => $AppUI
                        )
                    );
                } else {	
                     echo $this->element('calendar', array(
                            'data'              => $data,
                            'dt'                => $dt,
                            'staffRecord'       => $staffRecord,
                            'deviceRecord'      => $deviceRecord,
                            'shop_open_time'    => $shop_open_time,
                            'shop_close_time'   => $shop_close_time,
                            'AppUI'             => $AppUI
                        )
                    );
                }
                ?>
            </div>
        </div>   
    </div>
</div>