<?php echo $this->Html->script('jquery.cookie.js');?>
<?php $emptyStr = "なし"; ?>
<div id="option-dialog" class =''>
    <div class='bt_top'>
        <input type="hidden" id="dialog-order-id" value="0"/>
        <input type="hidden" id="dialog-timedial" value="0"/>
        <button onclick="optionCopy();" type="button" class="btn btn-primary" id="btnCopy" >コピー</button>
        <button onclick="optionEdit();" type="button" class="btn btn-primary" id="btnModalEdit" >編集</button>
        <button onclick="optionMediacal();" type="button" class="btn btn-primary" id="btnModalMedical" >カルテ表示</button>
    </div>
    <table width='100%' cellspacing='0' cellpadding='0' border='0'>
        <tbody>
            <tr>
                <th  width='15%'> 会員番号</th>
                <td  width='25%'>：
                    <?php echo !empty($o_detail['user_code']) ? $o_detail['user_code'] : $emptyStr ?>
                 </td>
                <th width='25%'> ステータス</th>
                <td width='30%'>：
                  <?php
                    $status ='';
                    // check status of Order
                    if(!empty($o_detail['is_cancel'])){
                        $status= "キャンセル";
                    }elseif(!empty($o_detail['is_autocancel']) ){
                        $status= "無断キャンセル";
                    }elseif (!empty($o_detail['is_paid'])) {
                        $status= "会計済み";//会計済み: Đã thanh toán
                    }elseif(!empty($o_detail['sr_start_date']) ||!empty($o_detail['sr_end_date']) ){
                        if (!empty($o_detail['sr_end_date'])) {
                            $status ="ネイル済み";//ネイル済み: đã làm service nail xong
                        } elseif (!empty($o_detail['sr_start_date']) ) {
                            $status = "ネイル中"; // ネイル中: đang làm service nail
                        }

                    }elseif(!empty($o_detail['off_start_date']) || !empty($o_detail['off_end_date']) ){
                        if (!empty($o_detail['off_end_date'])) {
                            $status = "オフ済み"; //オフ済み: đã off xong
                        }elseif (!empty($o_detail['off_start_date'])) {
                            $status ="オフ中";//オフ中: đang off
                        }
                    }elseif (!empty($o_detail['consult_start_date']) || !empty($o_detail['consult_end_date'])) {
                          if (!empty($o_detail['consult_end_date'])) {
                            $status ="コンサル済み";//コンサル済み: đã giới thiệu xong
                        } elseif (!empty($o_detail['consult_start_date']) ) {
                            $status = "コンサル中"; // コンサル中: đang giới thiệu
                        }
                    }elseif (!empty($o_detail['start_date'])) {
                         $status = "受付済み";//受付済み: Đã tiếp nhận hóa đơn
                    }elseif( (!empty($o_detail['seat_id']) ? $o_detail['seat_id'] :0) > $max_seat){
                            $status ="HP予約失敗（空き枠無し）";
                    }else{
                        $status ="未受付";// Chưa tiếp nhận hóa đơn.
                    }
                    echo  $status;
                  ?>
                </td>
            </tr>

            <tr>

                <th>お名前</th>
                <td>：
                    <?php echo $o_detail['user_name'] ?>
                </td>
                <th>メニュー</th>
                <td><div class="title-purpose">：</div>
                <div class="content-purpose">
                <?php
                $arrService = array();
                if(!empty($o_detail['services'])){
                    foreach($o_detail['services'] as $p) {
                        $arrService[] =  $p['name'];
                    }
                }
//                if(!empty($o_detail['service'])){
//                    $arrService[] = Configure::read('Config.OrderService')[$o_detail['service']];
//                }
//                if(!empty($o_detail['purpose'])){
//                    $arrService[] = Configure::read('Config.OrderPurpose')[$o_detail['purpose']];
//                }
                $text = implode('、',$arrService);
                echo !empty($text) ? $text : $emptyStr;
                ?>
                </div>
                </td>
            </tr>

            <tr>
                <th>お名前（カナ）</th>
                <td>：
                    <?php echo $o_detail['kana'] ?>
                </td>
                <th>ご要望</th>
                <td>：
                    <?php echo !empty($o_detail['request']) ? $o_detail['request'] : $emptyStr ?>
                </td>
            </tr>

            <tr>
                 <th> 電話番号</th>
                <td>：
                    <?php echo !empty($o_detail['phone']) ? $o_detail['phone'] : $emptyStr ;?>
                 </td>
                <th> ホットペッパー予約番号</th>
                <td>：
                 <?php echo !empty($o_detail['hp_order_code']) ? $o_detail['hp_order_code'] : $emptyStr ?>
                </td>
            </tr>

            <tr>
                <th>予約方法</th>
                <td>：
                     <?php echo !empty($o_detail['reservation_type']) ? Configure::read('Config.ReservationType')[$o_detail['reservation_type']] : $emptyStr ?>
                </td>
                <th>整理番号</th>
                <td>：
                    <?php echo !empty($o_detail['shop_counter_code']) ? $o_detail['shop_counter_code'] : $emptyStr ;?>
                </td>
            </tr>
            
            <tr>
                <th>来店経路</th>
                <td>：
                     <?php echo !empty($o_detail['visit_element']) ? Configure::read('Config.VisitElement')[$o_detail['visit_element']] : $emptyStr ?>
                </td>
            </tr>
        </tbody>
    </table>
    <div class='sectionSelect'>
        <div class='titleSelect'>ネイリスト</div>
        <select name='nailist_id' id='nailist_id' class='dialogSelect wP100 arrow_02 select_style form-control'>
            <option value="0">
                <?php echo Configure::read('Config.StrChooseOne') ?>
            </option>
        <?php
            // Load NailList by shop_id
            foreach ($listNailist as $row) {
                echo "
                <option value='{$row['id']}'>
                    {$row['name']}
                </option>";
            }
            $is_cancel      = !empty($o_detail['is_cancel']) || ( !empty($o_detail['sr_start_date']) ||!empty($o_detail['sr_end_date'])
                    || !empty($o_detail['off_start_date']) || !empty($o_detail['off_end_date']) );
            $is_autocancel  = !empty($o_detail['is_autocancel'])||  (!empty($o_detail['order_end_date']) && $o_detail['order_end_date'] > time());
            $isPayment      = !empty($o_detail['is_paid']);
        ?>
        </select>
    </div>
    <div class='btnGroup'>
        <button id ='btnInvoiceAcceptance' class='btn btn-salmon margin btnFB' onclick='return invoiceAcceptance();'
            <?php if (!empty($o_detail['start_date']) ) echo "disabled ='disabled'";?>>受付
        </button>
        <button id ='btnInvoicePayment' class='btn btn-salmon margin btnFB' onclick= 'return invoicePayment();'
            <?php if ($isPayment ) echo "disabled ='disabled'";?>>お会計
        </button>
        <button id ='btnInvoiceAutoCancel' class='btn btn-salmon margin btnFB' onclick= 'return invoiceAutoCancel();'
            <?php if ($is_autocancel || $isPayment) echo "disabled ='disabled'";?>>無断キャンセル
        </button>
        <button id ='btnInvoiceCancel' class='btn btn-salmon margin btnFB' onclick= 'return invoiceCancel();'
            <?php if ($is_cancel||$isPayment) echo "disabled ='disabled'";?>>キャンセル
        </button>
    </div>
</div>