<?php

function displayTime($start_date, $end_date) {
    $time = "00:00";
    $hour_time = "";
    $min_time = "";
    $second_time = "";
    
	$balance_seconds = $end_date - $start_date;
	$hour_time = intval($balance_seconds / 3600);
	$min_time = intval(($balance_seconds - ($hour_time * 3600)) / 60);
	$second_time = $balance_seconds - ($hour_time * 3600) - ($min_time * 60);
	if ($hour_time > 0) {
		if ($hour_time < 10) {
			$hour_time = '0' . $hour_time . ":";
		} else {
			$hour_time = $hour_time . ":";
		}
	} else {
		$hour_time = "";
	}
	if ($min_time < 10) {
		$min_time = '0' . $min_time;
	} else if ($min_time > 59) {
		$min_time = '00';
	}
	if ($second_time < 10)
		$second_time = '0' . $second_time;
	$time = $hour_time . $min_time . ':' . $second_time;
    
	return $time;
}
?>
<?php
if (isset($setadminorderid)) {
    echo $setadminorderid;
} else {
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title><?php echo!empty($title) ? $title . '様' : $meta['title']; ?>｜FASTNAIL</title>
            <meta name="description" content="<?php echo $meta['description']; ?>" />
            <meta name="keywords" content="<?php echo $meta['keywords']; ?>" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
            <?php
            echo $this->Html->meta('icon', 'img/favicon.ico');
            echo $this->fetch('meta');
            echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
            echo $this->Html->css('/bootstrap/css/bootstrap.min.css');
            echo $this->Html->css('/dist/css/AdminLTE.css');
            echo $this->Html->css('jquery-ui.css');
            echo $this->Html->css('keyboard.css');
            echo $this->Html->css('base_tablets.css');
            echo $this->Html->css('style_tablets.css');
            echo $this->Html->css('custom.css');
            echo $this->fetch('css');
            echo $this->Html->script('/plugins/jQuery/jQuery-2.1.3.min.js');
            echo $this->Html->script('jquery-1.8.3');
            echo $this->Html->script('jquery.min');
            echo $this->Html->script('jquery-ui.min');
            echo $this->Html->script('jquery.popupoverlay');
            echo $this->Html->script('qrcode.js');
            echo $this->Html->script('jquery.keyboard');
            echo $this->Html->script('jquery.cookie');
            echo $this->Html->script('function.js');
            echo $this->Html->script('medicalchart.js');
            echo $this->Html->script('/bootstrap/js/bootstrap.min.js');
            echo $this->fetch('script');
            ?>
            <script>
                var adminOrderTimeout = <?php echo $adminOrderTimeout; ?>;
            </script>

        </head>
        <body id="medicalchart">
            <!---- Header ---->
            <div id="headerWrapper">
                <div class="header">
                    <div class="logo"><a href="#">Fast Nail</a></div>
                    <?php if (!empty($data['Order']['order_history'])): ?>
                        <?php
                        $cnt = count($data['Order']['order_history']);
                        $first = $data['Order']['order_history'][0];
                        $last = $data['Order']['order_history'][$cnt - 1];
                        $prev = '';
                        $next = '';
                        $index = array_search($data['Order']['id'], $data['Order']['order_history']);
                        $curOrderId = !empty($curMedId) ? $curMedId : $data['Order']['id'];
                        $posPrev = '';
                        $posNext = '';
                        if ($index !== false) {
                            if ($index > 0) {
                                $posPrev = $index - 1;
                                $prev = $data['Order']['order_history'][$posPrev];
                            }
                            if ($index < ($cnt - 1)) {
                                $posNext = $index + 1;
                                $next = $data['Order']['order_history'][$posNext];
                            }
                        }
                        ?>
                        <div class="direction">
                            <ul>
                                <?php if ($prev == ''): ?>
                                    <li><a class="disable" href="javascript:void(0)"><i class="fa fa-fw fa-backward"></i></a></li>
                                <?php else: ?>
                                    <li><a href="<?php echo Router::url('/medicalchart/' . $prev . "?curMedId={$curOrderId}", true); ?>" ><i class="fa fa-fw fa-backward"></i></a></li>
                                <?php endif; ?>

                                <?php if ($next == ''): ?>
                                    <li><a class="disable" href="javascript:void(0)" ><i class="fa fa-fw fa-forward"></i></a></li>
                                <?php else: ?>
                                    <li><a href="<?php echo Router::url('/medicalchart/' . $next . "?curMedId={$curOrderId}", true); ?>" ><i class="fa fa-fw fa-forward"></i></a></li>
                                <?php endif; ?>

                                <?php if ($data['Order']['id'] == $curMedId): ?>
                                    <li><a class="disable" href="javascript:void(0)"><i class="fa fa-fw fa-fast-forward"></i></a></li>
        <?php else: ?>
                                    <li><a href="<?php echo Router::url('/medicalchart/' . $curOrderId . "?curMedId={$curOrderId}", true); ?>"><i class="fa fa-fw fa-fast-forward"></i></a></li>
        <?php endif; ?>

                            </ul>
                        </div>
                        <div class="med-position">
                            <div class="inner-position"><?php echo ($index + 1) ?>/<?php echo $cnt ?></div>
                        </div>
    <?php endif; ?>
                    <div class="titlePage">お客様オーダーシート</div>
                    <div class="navTop">
                        <div class="list"><a href="javascript:void(0);" id="orderLogsButton" data-order-id="<?php echo $data['Order']['id'] ?>">List</a></div>
                        <div class="time" style="position: relative">
                            <a href="javascript:void(0);" >Time</a>
                            <div  style="text-align: center; position:absolute; width:90px;left:-20px">
                                <div class="running-time">00:00</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---- Content ---->
            <div id="contentWrapper">
                <div class="content">
                    <!--<form action="" method="get" >-->
    <?php echo $this->Form->create(['type' => 'post', 'id' => 'MedicalChartForm', 'role' => 'form', 'accept-charset' => 'utf-8']) ?>
                    <div class="section heading mT15">
                        <div class="headingItem" style="display:none"><span class="titleItem">受付</span><span class="nameShow" id="txt_start_nailist"><?php echo isset($orderNailists[1]) ? $orderNailists[1] : ''; ?></span></div>
                        <div class="headingItem"><span class="titleItem">コンサル</span><span class="nameShow" id="txt_consult_nailist"><?php echo isset($orderNailists[7]) ? $orderNailists[7] : ''; ?></span></div>
                        <div class="headingItem"><span class="titleItem">オフ</span><span class="nameShow" id="txt_nail_nailist"><?php echo isset($orderNailists[2]) ? $orderNailists[2] : ''; ?></span></div>
                        <div class="headingItem"><span class="titleItem">サービス</span><span class="nameShow" id="txt_service_nailist"><?php echo isset($orderNailists[4]) ? $orderNailists[4] : ''; ?></span></div>
                        <div class="headingItem" style="display:none"><span class="titleItem">会計</span><span class="nameShow" id="txt_paid_nailist"><?php echo isset($orderNailists[6]) ? $orderNailists[6] : ''; ?></span></div>
                    </div>
                    <div class="section bgSalmon white">
                        <div class="fLeft wP50">
                            <table width="100%" cellpadding="0" cellspacing="0" border="1">
                                <tr colspan="2">
                                    <th width="30%">予約日</th>
                                    <td width="70%">
                                        <span class="readonly-textbox W200"><?php echo!empty($data['Order']['reservation_date']) ? date("Y-m-d H:i", $data['Order']['reservation_date']) : ''; ?></span>
                                    </td>
                                </tr>
                                <tr colspan="2">
                                    <th width="30%">会員番号</th>
                                    <td width="70%">
                                        <!--<input class="W200" type="text" maxlength="11" name="data[Order][user_code]" value="<?php //echo !empty($data['Order']['user_code']) ? $data['Order']['user_code'] : ''; ?>" readonly/>-->
                                        <?php if (empty($data['Order']['user_id'])): ?>
                                            <span class="readonly-textbox W100" id="user_code"><?php echo!empty($data['Order']['user_code']) ? $data['Order']['user_code'] : ''; ?></span>
                                            <span class="registerButton W50 aLeft"><a href="<?php echo $this->Html->url('/users/update?order_id=' . $data['Order']['id']); ?>" target="_blank">会員登録</a></span>
    <?php else: ?>
                                            <span class="readonly-textbox wP80" id="user_code"><?php echo!empty($data['Order']['user_code']) ? $data['Order']['user_code'] : ''; ?></span>
    <?php endif; ?>
    <?php echo $this->Item->showErrors($errors, 'user_code'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>来店区分</th>
                                    <td>
    <!--                                    <select class="W100 select_style" name="data[Order][visit_section]" disabled>-->
                                        <!--                                        --><?php //foreach ($listShowSection as $value => $name):  ?>
                                        <!--                                        <option value="--><?php //echo $value;  ?><!--" --><?php //echo !empty($data['Order']['visit_section']) && $data['Order']['visit_section'] == $value ? 'selected="selected"' : '';  ?><!--><?php //echo $name;  ?><!--</option>-->
                                        <!--                                        --><?php //endforeach;  ?>
                                        <!--                                    </select>-->
                                        <span class="readonly-textbox W200"><?php echo $displayVisitSection; ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>メール配信</th>
                                    <td>
                                        <!--<select class="W100 select_style" name="data[Order][is_mail]" disabled>
    <?php //foreach ($listMailOption as $value => $name):  ?>
                                            <option value="<?php //echo $value;  ?>" <?php //echo !empty($data['Order']['is_mail']) && $data['Order']['is_mail'] == $value ? 'selected="selected"' : '';  ?>><?php //echo $name;  ?></option>
    <?php //endforeach;  ?>
                                        </select>-->
                                        <span class="readonly-textbox W100"><?php echo isset($listMailOption[$data['Order']['is_mail']]) ? $listMailOption[$data['Order']['is_mail']] : ''; ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>メニュー</th>
                                    <td>
                                        <span class="readonly-textbox wP80 info f14px"><?php echo!empty($data['Order']['services']) ? implode('; ', array_column($data['Order']['services'], 'name')) : ''; ?></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="fRight wP50">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <th width="30%">店舗名</th>
                                    <td width="70%">
                                        <span class="readonly-textbox wP80"><?php echo!empty($data['Order']['shop']['name']) ? $data['Order']['shop']['name'] : ''; ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="30%">お名前</th>
                                    <td width="70%">
                                        <!--<input class="wP100" type="text" maxlength="64" name="data[Order][user_name]" value="<?php //echo !empty($data['Order']['user_name']) ? $data['Order']['user_name'] : '';  ?>" readonly/>-->
                                        <span class="readonly-textbox wP80" id="user_name"><?php echo!empty($data['Order']['display_user_name']) ? $data['Order']['display_user_name'] : ''; ?></span>
    <?php echo $this->Item->showErrors($errors, 'user_name'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>来店経路</th>
                                    <td>
                                        <!--<select class="wP100 select_style" name="data[Order][visit_element]" disabled>
    <?php //foreach ($listVisitElement as $value => $name):  ?>
                                            <option value="<?php //echo $value;  ?>" <?php //echo !empty($data['Order']['visit_element']) && $data['Order']['visit_element'] == $value ? 'selected="selected"' : '';  ?>><?php //echo $name;  ?></option>
    <?php //endforeach;  ?>
                                        </select>-->
                                        <span class="readonly-textbox wP80"><?php echo isset($listVisitElement[$data['Order']['visit_element']]) ? $listVisitElement[$data['Order']['visit_element']] : ''; ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>予約方法</th>
                                    <td>
                                        <!--<select class="W200 select_style" name="data[Order][reservation_type]" disabled>
    <?php //foreach ($listReservationType as $value => $name):  ?>
                                            <option value="<?php //echo $value;  ?>" <?php //echo !empty($data['Order']['reservation_type']) && $data['Order']['reservation_type'] == $value ? 'selected="selected"' : '';  ?>><?php //echo $name;  ?></option>
    <?php //endforeach;  ?>
                                        </select>-->
                                        <span class="readonly-textbox wP80"><?php echo!empty($data['Order']['reservation_type']) ? $listReservationType[$data['Order']['reservation_type']] : ''; ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="30%">オーダー区分</th>
                                    <td width="70%">
                                        <!--<select class="wP80 select_style" name="data[Order][hf_section]">
    <?php foreach ($listHfSection as $value => $name): ?>
                                                <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['hf_section']) && $data['Order']['hf_section'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
    <?php endforeach; ?>
                                        </select>-->
                                        <span class="readonly-textbox wP80"><?php echo!empty($data['Order']['hf_section']) ? $listHfSection[$data['Order']['hf_section']] : ''; ?></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

    <?php if (!empty($data['Order']['has_suborder'])): ?>
                        <div class="suborder-tab-section">
                            <a href="javascript:void(0);" id='tab-hand-btn' onclick="showSubOrderTab('hand')" class="suborder-tab-btn btn-active">ハンド</a>
                            <a href="javascript:void(0);" id='tab-foot-btn' onclick="showSubOrderTab('foot')" class="suborder-tab-btn">フット</a>
                        </div>
                        <!--Tab hand-->
                        <div class="section bgSalmonLight tab-detail-hand">
                            <div class="fLeft wP50">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">

                                    <tr>
                                        <th width="30%">オフ</th>
                                        <td width="70%"><select class="wP80 select_style" name="data[Order][off_nails]" id="sel_nail_off">
        <?php foreach ($listNailOff as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['suborders'][0]['off_nails']) && $data['Order']['suborders'][0]['off_nails'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name . ' ' . $listNailOffPrice[$value]; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>爪先の問題</th>
                                        <td><select class="W100 select_style" id="sel_problem">
        <?php foreach ($listOnOff as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo ($value == 1 && !empty($data['Order']['suborders'][0]['problem'])) ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>内容</th>
                                        <td>
                                            <textarea class="wP80" id="txt_problem" name="data[Order][problem]"><?php echo!empty($data['Order']['suborders'][0]['problem']) ? $data['Order']['suborders'][0]['problem'] : ''; ?></textarea>
        <?php echo $this->Item->showErrors($errors, 'problem'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="fRight wP50">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <th width="30%">爪 長さ</th>
                                        <td width="70%">
                                            <select class="wP80 select_style" name="data[Order][nail_length]">
        <?php foreach ($listNailLength as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['suborders'][0]['nail_length']) && $data['Order']['suborders'][0]['nail_length'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>爪 かたち</th>
                                        <td><select class="wP80 select_style f14px" name="data[Order][nail_type]">
        <?php foreach ($listNailType as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['suborders'][0]['nail_type']) && $data['Order']['suborders'][0]['nail_type'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>長さだし</th>
                                        <td><select class="W100 select_style" name="data[Order][nail_add_length]" id="sel_nail_add_length">
        <?php foreach ($listFingerItemLength as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['suborders'][0]['nail_add_length']) && $data['Order']['suborders'][0]['nail_add_length'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                            <span class="mL10"><?php echo $fingerItemPrice; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>ご要望</th>
                                        <td>
                                            <textarea class="wP80" id="txt_problem" name="data[Order][request]"><?php echo!empty($data['Order']['suborders'][0]['request']) ? $data['Order']['suborders'][0]['request'] : ''; ?></textarea>
        <?php echo $this->Item->showErrors($errors, 'request'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--Tab foot-->
                        <div class="section bgSalmonLight tab-detail-foot" style="display: none">
                            <div class="fLeft wP50">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">

                                    <tr>
                                        <th width="30%">オフ</th>
                                        <td width="70%"><select class="wP80 select_style" name="data[Order][f_off_nails]" id="f_sel_nail_off">
        <?php foreach ($listNailOff as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['suborders'][1]['off_nails']) && $data['Order']['suborders'][1]['off_nails'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name . ' ' . $listNailOffPrice[$value]; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>爪先の問題</th>
                                        <td><select class="W100 select_style" id="f_sel_problem">
        <?php foreach ($listOnOff as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo ($value == 1 && !empty($data['Order']['suborders'][1]['problem'])) ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>内容</th>
                                        <td>
                                            <textarea class="wP80" id="f_txt_problem" name="data[Order][f_problem]"><?php echo!empty($data['Order']['suborders'][1]['problem']) ? $data['Order']['suborders'][1]['problem'] : ''; ?></textarea>
        <?php echo $this->Item->showErrors($errors, 'problem'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="fRight wP50">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <th width="30%">爪 長さ</th>
                                        <td width="70%">
                                            <select class="wP80 select_style" name="data[Order][f_nail_length]">
        <?php foreach ($listNailLength as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['suborders'][1]['nail_length']) && $data['Order']['suborders'][1]['nail_length'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>爪 かたち</th>
                                        <td><select class="wP80 select_style f14px" name="data[Order][f_nail_type]">
        <?php foreach ($listNailType as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['suborders'][1]['nail_type']) && $data['Order']['suborders'][1]['nail_type'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>長さだし</th>
                                        <td><select class="W100 select_style" name="data[Order][f_nail_add_length]" id="f_sel_nail_add_length">
        <?php foreach ($listFingerItemLength as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['suborders'][1]['nail_add_length']) && $data['Order']['suborders'][1]['nail_add_length'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                            <span class="mL10"><?php echo $fingerItemPrice; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>ご要望</th>
                                        <td>
                                            <textarea class="wP80" id="f_txt_problem" name="data[Order][f_request]"><?php echo!empty($data['Order']['suborders'][1]['request']) ? $data['Order']['request'] : ''; ?></textarea>
        <?php echo $this->Item->showErrors($errors, 'request'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
    <?php else : ?>
                        <div class="section bgSalmonLight">
                            <div class="fLeft wP50">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">

                                    <tr>
                                        <th width="30%">オフ</th>
                                        <td width="70%"><select class="wP80 select_style" name="data[Order][off_nails]" id="sel_nail_off">
        <?php foreach ($listNailOff as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['off_nails']) && $data['Order']['off_nails'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name . ' ' . $listNailOffPrice[$value]; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>爪先の問題</th>
                                        <td><select class="W100 select_style" id="sel_problem">
        <?php foreach ($listOnOff as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo ($value == 1 && !empty($data['Order']['problem'])) ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>内容</th>
                                        <td>
                                            <textarea class="wP80" id="txt_problem" name="data[Order][problem]"><?php echo!empty($data['Order']['problem']) ? $data['Order']['problem'] : ''; ?></textarea>
        <?php echo $this->Item->showErrors($errors, 'problem'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="fRight wP50">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <th width="30%">爪 長さ</th>
                                        <td width="70%">
                                            <select class="wP80 select_style" name="data[Order][nail_length]">
        <?php foreach ($listNailLength as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['nail_length']) && $data['Order']['nail_length'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>爪 かたち</th>
                                        <td><select class="wP80 select_style f14px" name="data[Order][nail_type]">
        <?php foreach ($listNailType as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['nail_type']) && $data['Order']['nail_type'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>長さだし</th>
                                        <td><select class="W100 select_style" name="data[Order][nail_add_length]" id="sel_nail_add_length">
        <?php foreach ($listFingerItemLength as $value => $name): ?>
                                                    <option value="<?php echo $value; ?>" <?php echo!empty($data['Order']['nail_add_length']) && $data['Order']['nail_add_length'] == $value ? 'selected="selected"' : ''; ?>><?php echo $name; ?></option>
        <?php endforeach; ?>
                                            </select>
                                            <span class="mL10"><?php echo $fingerItemPrice; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>ご要望</th>
                                        <td>
                                            <textarea class="wP80" id="txt_problem" name="data[Order][request]"><?php echo!empty($data['Order']['request']) ? $data['Order']['request'] : ''; ?></textarea>
        <?php echo $this->Item->showErrors($errors, 'request'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
    <?php endif; ?>

                    <div class="hidden" id="listOrderedItems" ></div>
                    </form>
                    <input type="hidden" id="formInitVal" />

                    <?php if (!empty($data['Order']['has_suborder'])): ?>
                        <?php
                        $nail_hand = array();
                        $nail_foot = array();
                        if (!empty($data['Order']['nails'])) {
                            if (count($data['Order']['nails']) == 2) {
                                $nail_hand = $data['Order']['nails'][0];
                                $nail_foot = $data['Order']['nails'][1];
                            } else {
                                if ($data['Order']['nails'][0]['hf_section'] == 1) {
                                    $nail_hand = $data['Order']['nails'][0];
                                }
                                if ($data['Order']['nails'][0]['hf_section'] == 2) {
                                    $nail_foot = $data['Order']['nails'][0];
                                }
                            }
                        }
                        ?>

        <?php if (!empty($nail_hand)): ?>
                            <div class="picDetail tab-detail-hand">
                                <div class="picNum">写真番号 <?php echo $nail_hand['nail_photo_cd']; ?></div>
                                <div class="thumbnail"><img id="order_nail_img" src="<?php echo $nail_hand['nail_image_url']; ?>" alt="" /></div>
                                <div class="btn  bgSalmon W150 aCenter" id="choose_nail_hand"><a href="javascript:void(0);"><?php echo __('Edit'); ?></a></div>
                                <div class="btn  bgSalmon W150 aCenter" id="choose_nail_hand_by_qrcode"><a class="fade_open" href="#fade" >QR Code</a></div>
                            </div>
                            <input type="hidden" id="nail_id" value="<?php echo $nail_hand['nail_id']; ?>" />
        <?php else: ?>
                            <div class="picDetail tab-detail-hand">
                                <div class="btn noPic bgSalmon wP50 mT40 mB40" id="choose_nail_hand"><a href="javascript:void(0);">※ハンドネイルが選択されていません<br/>こちらから選択してください</a></div>
                                <div class="btn  bgSalmon W150 aCenter" id="choose_nail_hand_by_qrcode"><a class="fade_open" href="#fade" >QR Code</a></div>
                            </div>
                            <input type="hidden" id="nail_id" value="" />
        <?php endif; ?>

        <?php if (!empty($nail_foot)): ?>
                            <div class="picDetail tab-detail-foot" style="display: none">
                                <div class="picNum">写真番号 <?php echo $nail_foot['nail_photo_cd']; ?></div>
                                <div class="thumbnail"><img id="order_nail_img" src="<?php echo $nail_foot['nail_image_url']; ?>" alt="" /></div>
                                <div class="btn  bgSalmon W150 aCenter" id="choose_nail_foot"><a href="javascript:void(0);"><?php echo __('Edit'); ?></a></div>
                                <div class="btn  bgSalmon W150 aCenter" id="choose_nail_foot_by_qrcode"><a class="fade_open" href="#fade" >QR Code</a></div>
                            </div>
                            <input type="hidden" id="f_nail_id" value="<?php echo $nail_foot['nail_id']; ?>" />
        <?php else: ?>
                            <div class="picDetail tab-detail-foot" style="display: none">
                                <div class="btn noPic bgSalmon wP50 mT40 mB40" id="choose_nail_foot"><a href="javascript:void(0);">※フットネイルが選択されていません<br/>こちらから選択してください</a></div>
                                <div class="btn  bgSalmon W150 aCenter" id="choose_nail_foot_by_qrcode"><a class="fade_open" href="#fade" >QR Code</a></div>
                            </div>
                            <input type="hidden" id="f_nail_id" value="" />
                        <?php endif; ?>

    <?php else: ?>

        <?php if (!empty($data['Order']['nails'][0])): ?>
                            <div class="picDetail">
                                <div class="picNum">写真番号 <?php echo $data['Order']['nails'][0]['nail_photo_cd']; ?></div>
                                <div class="thumbnail"><img id="order_nail_img" src="<?php echo $data['Order']['nails'][0]['nail_image_url']; ?>" alt="" /></div>
                                <div class="btn  bgSalmon W150 aCenter" id="choose_nail"><a href="javascript:void(0);"><?php echo __('Edit'); ?></a></div>
                                <div class="btn  bgSalmon W150 aCenter" id="choose_nail_by_qrcode"><a class="fade_open" href="#fade" >QR Code</a></div>
                            </div>
                            <input type="hidden" id="nail_id" value="<?php echo $data['Order']['nails'][0]['nail_id']; ?>" />
        <?php else: ?>
                            <div class="picDetail">
                                <div class="btn noPic bgSalmon wP50 mT40 mB40" id="choose_nail"><a href="javascript:void(0);">※ネイルが選択されていません<br/>こちらから選択してください</a></div>
                                <div class="btn  bgSalmon W150 aCenter" id="choose_nail_by_qrcode"><a class="fade_open" href="#fade" >QR Code</a></div>
                            </div>
                            <input type="hidden" id="nail_id" value="" />
        <?php endif; ?>

    <?php endif; ?>

                    <div id="fade" class="qrcode"></div>
                    <input type="hidden" id="secret_key" value="<?php echo md5(Configure::read('Config.secretKey') . $data['Order']['id'] . time()); ?>" />
                    <input type="hidden" id="secret_time" value="<?php echo time(); ?>" />
                    <input type="hidden" id="order_id"  value="<?php echo $data['Order']['id']; ?>" />
                    <div class="nailInfo" class="section">

                        <?php if (!empty($data['Order']['has_suborder'])): ?>
                            <?php
                            $nail_hand_att = array();
                            $nail_foot_att = array();
                            if (!empty($data['Nail'])) {
                                if (count($data['Nail']) == 2) {
                                    $nail_hand_att = $data['Nail'][0];
                                    $nail_foot_att = $data['Nail'][1];
                                } else {
                                    if ($data['Nail'][0]['hf_section'] == 1) {
                                        $nail_hand_att = $data['Nail'][0];
                                    }
                                    if ($data['Nail'][0]['hf_section'] == 2) {
                                        $nail_foot_att = $data['Nail'][0];
                                    }
                                }
                            }
                            ?>
                            <!--Hand's attributes-->
                            <?php
                            $attributes_h = array(
                                'color_jell_id' => array(
                                    'title' => 'カラージェル',
                                    'key' => 'color_jells',
                                ),
                                'rame_jell_id' => array(
                                    'title' => 'ラメジェル',
                                    'key' => 'rame_jells',
                                ),
                                'stone_id' => array(
                                    'title' => 'ストーン',
                                    'key' => 'stones',
                                ),
                                'bullion_id' => array(
                                    'title' => 'ブリオン',
                                    'key' => 'bullions',
                                ),
                                'hologram_id' => array(
                                    'title' => 'ホログラム',
                                    'key' => 'holograms',
                                ),
                                'shell_id' => array(
                                    'title' => 'シエル',
                                    'key' => 'shells',
                                ),
                                'flower_id' => array(
                                    'title' => '押花',
                                    'key' => 'flowers',
                                ),
                                'powder_id' => array(
                                    'title' => 'パウダー',
                                    'key' => 'powders',
                                ),
                                'paint_id' => array(
                                    'title' => 'ペイント',
                                    'key' => 'paints',
                                ),
                            );
                            foreach ($attributes_h as &$attribute) {
                                $attribute['is_change'] = 0;
                                if (!empty($nail_hand_att['attributes'][$attribute['key']])) {
                                    $attribute['value'] = implode(', ', array_column($nail_hand_att['attributes'][$attribute['key']], 'name'));
                                    foreach ($nail_hand_att['attributes'][$attribute['key']] as $itemAttr) {
                                        if (isset($itemAttr['order_id'])) {
                                            $attribute['is_change'] = 1;
                                        }
                                    }
                                } else {
                                    $attribute['value'] = '';
                                }
                            }
                            unset($attribute);
                            ?>
                            <div class="style wP48 fLeft tab-detail-hand">
                                <ul>
        <?php foreach ($attributes_h as $key => $attribute) : ?>
                                        <li>
                                            <div class="title fLeft">
                                                <?php echo $attribute['title']; ?> 
                                                <i class="fa fa-fw fa-edit " data-nailtype="hand" data-object="<?php echo $key; ?>"></i>
                                            </div>
                                            <div class="info fRight<?php if ($attribute['is_change']) echo ' is_change'; ?>" id="<?php echo $key; ?>">
            <?php echo $attribute['value']; ?>
                                            </div>
                                        </li>
                            <?php endforeach; ?>
                                </ul>
                            </div>

                            <!--Foot's attributes-->
                            <?php
                            $attributes_f = array(
                                'color_jell_id' => array(
                                    'title' => 'カラージェル',
                                    'key' => 'color_jells',
                                ),
                                'rame_jell_id' => array(
                                    'title' => 'ラメジェル',
                                    'key' => 'rame_jells',
                                ),
                                'stone_id' => array(
                                    'title' => 'ストーン',
                                    'key' => 'stones',
                                ),
                                'bullion_id' => array(
                                    'title' => 'ブリオン',
                                    'key' => 'bullions',
                                ),
                                'hologram_id' => array(
                                    'title' => 'ホログラム',
                                    'key' => 'holograms',
                                ),
                                'shell_id' => array(
                                    'title' => 'シエル',
                                    'key' => 'shells',
                                ),
                                'flower_id' => array(
                                    'title' => '押花',
                                    'key' => 'flowers',
                                ),
                                'powder_id' => array(
                                    'title' => 'パウダー',
                                    'key' => 'powders',
                                ),
                                'paint_id' => array(
                                    'title' => 'ペイント',
                                    'key' => 'paints',
                                ),
                            );
                            foreach ($attributes_f as &$attribute) {
                                $attribute['is_change'] = 0;
                                if (!empty($nail_foot_att['attributes'][$attribute['key']])) {
                                    $attribute['value'] = implode(', ', array_column($nail_foot_att['attributes'][$attribute['key']], 'name'));
                                    foreach ($nail_foot_att['attributes'][$attribute['key']] as $itemAttr) {
                                        if (isset($itemAttr['order_id'])) {
                                            $attribute['is_change'] = 1;
                                        }
                                    }
                                } else {
                                    $attribute['value'] = '';
                                }
                            }
                            unset($attribute);
                            ?>
                            <div class="style wP48 fLeft tab-detail-foot" style="display: none">
                                <ul>
        <?php foreach ($attributes_f as $key => $attribute) : ?>
                                        <li>
                                            <div class="title fLeft">
                                                <?php echo $attribute['title']; ?> 
                                                <i class="fa fa-fw fa-edit " data-nailtype="foot" data-object="<?php echo $key; ?>"></i>
                                            </div>
                                            <div class="info fRight<?php if ($attribute['is_change']) echo ' is_change'; ?>" id="<?php echo "f_" . $key; ?>">
            <?php echo $attribute['value']; ?>
                                            </div>
                                        </li>
                            <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php else: ?>
                            <?php
                            $attributes = array(
                                'color_jell_id' => array(
                                    'title' => 'カラージェル',
                                    'key' => 'color_jells',
                                ),
                                'rame_jell_id' => array(
                                    'title' => 'ラメジェル',
                                    'key' => 'rame_jells',
                                ),
                                'stone_id' => array(
                                    'title' => 'ストーン',
                                    'key' => 'stones',
                                ),
                                'bullion_id' => array(
                                    'title' => 'ブリオン',
                                    'key' => 'bullions',
                                ),
                                'hologram_id' => array(
                                    'title' => 'ホログラム',
                                    'key' => 'holograms',
                                ),
                                'shell_id' => array(
                                    'title' => 'シエル',
                                    'key' => 'shells',
                                ),
                                'flower_id' => array(
                                    'title' => '押花',
                                    'key' => 'flowers',
                                ),
                                'powder_id' => array(
                                    'title' => 'パウダー',
                                    'key' => 'powders',
                                ),
                                'paint_id' => array(
                                    'title' => 'ペイント',
                                    'key' => 'paints',
                                ),
                            );
                            foreach ($attributes as &$attribute) {
                                $attribute['is_change'] = 0;
                                if (!empty($data['Nail'][0]['attributes'][$attribute['key']])) {
                                    $attribute['value'] = implode(', ', array_column($data['Nail'][0]['attributes'][$attribute['key']], 'name'));
                                    foreach ($data['Nail'][0]['attributes'][$attribute['key']] as $itemAttr) {
                                        if (isset($itemAttr['order_id'])) {
                                            $attribute['is_change'] = 1;
                                        }
                                    }
                                } else {
                                    $attribute['value'] = '';
                                }
                            }
                            unset($attribute);
                            ?>
                            <div class="style wP48 fLeft tab-detail-hand">
                                <ul>
        <?php foreach ($attributes as $key => $attribute) : ?>
                                        <li>
                                            <div class="title fLeft">
                                                <?php echo $attribute['title']; ?> 
                                                <i class="fa fa-fw fa-edit " data-nailtype="hand" data-object="<?php echo $key; ?>"></i>
                                            </div>
                                            <div class="info fRight<?php if ($attribute['is_change']) echo ' is_change'; ?>" id="<?php echo $key; ?>">
            <?php echo $attribute['value']; ?>
                                            </div>
                                        </li>
        <?php endforeach; ?>
                                </ul>
                            </div>
    <?php endif; ?>    

                        <div class="price wP48 fRight">
                            <ul class="nailPrice">
                                <?php if (!empty($data['Order']['has_suborder'])): ?>
                                    <?php
                                        $nail_price = 0;
                                        $f_nail_price = 0;
                                        if (!empty($data['Order']['nails'])) {
                                            if (count($data['Order']['nails']) == 2) {
                                                $nail_price = $data['Order']['nails'][0]['nail_price'];
                                                $f_nail_price = $data['Order']['nails'][1]['nail_price'];
                                            } else {
                                                if ($data['Order']['nails'][0]['hf_section'] == 1) {
                                                    $nail_price = $data['Order']['nails'][0]['nail_price'];
                                                }
                                                if ($data['Order']['nails'][0]['hf_section'] == 2) {
                                                    $f_nail_price = $data['Order']['nails'][0]['nail_price'];
                                                }
                                            }
                                        }
                                    ?>
                                    <li>
                                        <div class="title fLeft">ハンドネイル</div>
                                        <div class="info fRight"><div class="mT10"><span id="nail-price"><?php echo number_format($nail_price); ?></span>円<span class="f11px">(税抜) </span></div></div>
                                    </li>
                                    <li>
                                        <div class="title fLeft">フットネイル</div>
                                        <div class="info fRight"><div class="mT10"><span id="f-nail-price"><?php echo number_format($f_nail_price); ?></span>円<span class="f11px">(税抜) </span></div></div>
                                    </li>
                                <?php else: ?>
                                    <li>
                                        <div class="title fLeft">ネイル</div>
                                        <?php
                                        $nail_price = 0;
                                        if (isset($data['Order']['nails'][0]['nail_price'])) {
                                            $nail_price += $data['Order']['nails'][0]['nail_price'];
                                        }
                                        ?>
                                        <div class="info fRight"><div class="mT10"><span id="nail-price"><?php echo number_format($nail_price); ?></span>円<span class="f11px">(税抜) </span></div></div>
                                    </li>
                                <?php endif; ?>
                                
                                
                            </ul>
                            <div class="plus mT20">その他追加事項</div>
                            <ul id="item_list" class="item_list">

                                <?php
                                $subTotal = $nail_price;
                                ?>
    <?php if (!empty($data['Order']['items'])) { ?>
        <?php
        foreach ($data['Order']['items'] as $item) {
            $price = ($item['item_price']) * $item['item_quantity'];
            $subTotal += $price;
            ?>
                                        <li>
                                            <div class="title fLeft"><span class="delButton"><a href="javascript:void(0);" onclick="javascript:removeItem(<?php echo $item['item_id']; ?>)" class="mR5">X</a></span><?php echo $item['item_name']; ?></div>
                                            <div class="info fRight orderItem">
                                                <span class="priceAll"><span class="item_price" id="<?php echo $item['item_id']; ?>"><?php echo number_format($item['item_price']); ?></span>円</span><span class="taxLabel">(税抜) ×</span>
                                                <span class="mL5">
                                                    <input type="text" name="quantity" class="W50 ordered_item tbQty qty-input onlyNumber" id="item_<?php echo $item['item_id']; ?>"  value="<?php echo $item['item_quantity']; ?>" /><span>本</span>
                                                    <script>
                                                        jQuery(function ($) {
                                                            $('#item_<?php echo $item['item_id']; ?>').keyboard({
                                                                layout: 'custom',
                                                                customLayout: {
                                                                    'default': [
                                                                        '{clear} {bksp}',
                                                                        '7 8 9',
                                                                        '4 5 6',
                                                                        '1 2 3',
                                                                        '0 {a} {c}'
                                                                    ]
                                                                },
                                                                restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                                                                preventPaste: true, // prevent ctrl-v and right click
                                                                autoAccept: true
                                                            });
                                                        });
                                                    </script>
                                                    <span class="arrowBlock">
                                                        <a href="javascript:void(0)" onClick="javascript:upQtyClick(<?php echo $item['item_id']; ?>);" class="upQty"></a>
                                                        <a href="javascript:void(0)" onClick="javascript:downQtyClick(<?php echo $item['item_id']; ?>);" class="downQty"></a>
                                                    </span>
                                                </span>
                                            </div>
                                        </li>
                                            <?php } ?>
                                        <?php } ?>
                            </ul>
                            <div class="item-list mT30">
                                <div>
                                    <ul id="nailItem">
    <?php $itemSection = Configure::read('Config.ItemSection');
    unset($itemSection[5]);
    foreach ($itemSection as $key => $value) :
        ?>
                                            <li>
                                                <div class="itemContent">
                                        <?php echo $value; ?>
                                                    <i class="fa fa-fw fa-folder-open-o" data-object="<?php echo $key; ?>"></i>
                                                </div>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                                <select class="wP100  select_style f14px mB10" id="select_items">
                                    <option value="0">オプションを追加する</option>
                                    <?php foreach ($listItems as $itemSection): ?>                            
                                        <optgroup label="<?php echo $itemSection['nane'] ?>">
        <?php foreach ($itemSection['item'] as $item): ?>
            <?php if (!in_array($item['id'], $arrItemIds)): ?>
                                                    <option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
            <?php endif; ?>
        <?php endforeach; ?>
                                        </optgroup>
    <?php endforeach; ?>                            
                                </select>
                            </div>
                            <ul class="point_convert">
                                <li class="sub_total">
                                    <div class="title fLeft">小計</div>
                                    <div class="info fRight">
                                        <div class="mT12">
                                            <!--
                                            <span id="total_price"><?php echo number_format($subTotal); ?>円 <?php echo '(' . number_format($subTotal + ($subTotal * $itemTax)) . '円)'; ?></span>
                                            -->
                                            <span id="total_price"><?php echo number_format($subTotal); ?>(税抜)</span>
                                        </div>
                                    </div>
                                </li>
                                <li class="r_point">
                                    <div class="title fLeft">ポイント利用</div>
                                    <div class="info fRight">
                                        <input type="hidden" name="r_point" value="<?php echo $data['Order']['r_point_convert']; ?>">
                                            <div class="mT12">
                                                Rポイント <span class="price_point"><?php echo!empty($data['Order']['r_point_convert']) ? number_format($data['Order']['r_point_convert']) : '0'; ?>円</span>
                                            </div>
                                    </div>
                                </li>
                                <li class="fn_point">
                                    <div class="info fRight">
                                        <input type="hidden" name="fn_point" value="<?php echo $data['Order']['fn_point_convert']; ?>">
                                            <div class="mT12">
                                                FNポイント <span class="price_point"><?php echo!empty($data['Order']['fn_point_convert']) ? number_format($data['Order']['fn_point_convert']) : '0'; ?>円</span>
                                            </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="total">合計<span id="total_price_point"><?php
                                $total = ($subTotal + ($subTotal * $itemTax)) - $data['Order']['r_point_convert'] - $data['Order']['fn_point_convert'];
                                echo number_format($total);
                                ?>円(税込)</span></div>
                            <div class="btn save bgSalmon wP80 mT20 mB20 Hidden" id="btnSubmitMedicalChart"><a href="javascript:void(0);"><?php echo __('Save'); ?></a></div>.
    <?php foreach ($listItems as $itemSection): ?>
        <?php foreach ($itemSection['item'] as $item): ?>                    
                                    <input type="hidden" id="hidden_item_<?php echo $item['id']; ?>" value="<?php echo $item['name']; ?>" />
        <?php endforeach; ?>  
    <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bgBlack <?php echo!empty($is_undo) ? 'active' : '' ?>" ></div>
            <div id="popupWrapper" <?php echo!empty($is_undo) ? "class='active'" : "" ?>>
                <div id="undo-medical-box">
                    <div class="small-box bkgd" >
                        <div class='undo-close'><a href="javascript:void(0)">x</a></div>
                        <div class="inner inner-text" >
                            <label class="undo-time">15</label>
                        </div>
                        <a href="javascript:void(0)" data-id="<?php echo $data['Order']['id'] ?>" class="undo-med-btn small-box-footer"><i class="fa fa-undo"></i> 取り消す</a>
                    </div>
                </div>
                <div class="popupContent log">
                    <div class="nailTime">
                        <div class="close"><a href="">close</a></div>
                        <div class="ico_time"></div>
                        <div class="dropList">
                            <label>今回の担当者</label>
                            <ul class="drop-nailist" style="">
                                <li class="user">
                                        <?php
                                        $nailist_log_id = !empty($data['nailist_log'][0]['nailist_id']) ? $data['nailist_log'][0]['nailist_id'] : 0;
                                        $nailist_log_name = !empty($data['nailist_log'][0]['nailist_name']) ? $data['nailist_log'][0]['nailist_name'] : '';
                                        $nailist_log_image = !empty($data['nailist_log'][0]['nailist_image_url']) ? $data['nailist_log'][0]['nailist_image_url'] : '';
                                        if (!empty($nailist_log_id) && !empty($nailist_log_name) && !empty($nailist_log_image)):
                                            ?>
                                        <a href="javascript:void(0);" class="a_nailists" data-id="<?php echo $nailist_log_id ?>" data-name="<?php echo $nailist_log_name ?>">
                                            <div class="avatar">
                                                <img src="<?php echo $nailist_log_image ?>" alt="<?php echo $nailist_log_name ?>">
                                            </div>
        <?php echo $nailist_log_name ?>
                                        </a>
        <?php
    else:
        ?>
                                        <a href="javascript:void(0);" class="a_nailists" data-id="">ネイリストの指名</a>                
    <?php
    endif;
    ?>
                                    <span class="navDown_02">ネイリスト</span>
                                    <ul id="orders_nailists">
                                        <li class="user">
                                            <a href="javascript:void(0);" class="nailists" data-id="">なし</a>
                                        </li>
                                        <?php foreach ($data['nailists'] as $value): ?>
                                            <li class="user">
                                                <a href="javascript:void(0);" class="nailists" data-id="<?php echo $value['id'] ?>" data-name="<?php echo $value['name'] ?>">
                                                    <div class="avatar">
                                                        <img src="<?php echo $value['image_url'] ?>" alt="<?php echo $value['name'] ?>">
                                                    </div>
                            <?php echo $value['name'] ?>
                                                </a>
                                            </li>
                        <?php endforeach; ?>    
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        
                        <?php if (!empty($data['Order']['has_suborder'])): ?>
                        <!--Stop watch for hand-->
                        <div class="timeConsult three_cell bgGrownLight">
                            <div class="f28px mB40">コンサルタイム</div>
                            <?php
                            $consult_time = "00:00";
                            if (!empty($data['Order']['consult_start_date']) && !empty($data['Order']['consult_end_date'])) {
                                $consult_time = displayTime($data['Order']['consult_start_date'], $data['Order']['consult_end_date']);
                            }
                            ?>
                            <div class="hours_large mB10"><?php echo $consult_time ?></div>
                            <div class="hours_small">
                                <span class="fLeft consult_left"><?php echo (!empty($data['Order']['consult_start_date']) && ($data['Order']['consult_start_date'] != $data['Order']['consult_end_date'])) ? date('H:i', $data['Order']['consult_start_date']) : "00:00"; ?></span>
                                <span class="fRight consult_right"><?php echo (!empty($data['Order']['consult_end_date']) && ($data['Order']['consult_end_date'] != $data['Order']['consult_start_date'])) ? date('H:i', $data['Order']['consult_end_date']) : "00:00"; ?></span>
                            </div>
                            <?php if (empty($data['Order']['consult_start_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn consult_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1">開始</a></div>
                            <?php } elseif (!empty($data['Order']['consult_start_date']) && empty($data['Order']['consult_end_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn consult_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="0">終了</a></div>
                            <?php } else { ?>
                                <div class="btn consult_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1" class="disable">終了</a></div>
                            <?php } ?>
                        </div>

                        <div class="timeFrom three_cell bgGrayLight">
                            <div class="f28px mB40">ハンドオフ</div>
                            <?php
                            $nail_time = "00:00";
                            if (!empty($data['Order']['suborders'][0]['off_start_date']) && !empty($data['Order']['suborders'][0]['off_end_date'])) {
                                $nail_time = displayTime($data['Order']['suborders'][0]['off_start_date'], $data['Order']['suborders'][0]['off_end_date']);
                            }
                            ?>
                            <div class="hours_large mB10"><?php echo $nail_time ?></div>
                            <div class="hours_small">
                                <span class="fLeft order_left"><?php echo (!empty($data['Order']['suborders'][0]['off_start_date']) && ($data['Order']['suborders'][0]['off_start_date'] != $data['Order']['suborders'][0]['off_end_date'])) ? date('H:i', $data['Order']['suborders'][0]['off_start_date']) : "00:00"; ?></span>
                                <span class="fRight order_right"><?php echo (!empty($data['Order']['suborders'][0]['off_end_date']) && ($data['Order']['suborders'][0]['off_end_date'] != $data['Order']['suborders'][0]['off_start_date'])) ? date('H:i', $data['Order']['suborders'][0]['off_end_date']) : "00:00"; ?></span>
                            </div>
                            <?php if (empty($data['Order']['suborders'][0]['off_start_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn nail_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1">開始</a></div>
                            <?php } elseif (!empty($data['Order']['suborders'][0]['off_start_date']) && empty($data['Order']['suborders'][0]['off_end_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn nail_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="0">終了</a></div>
                            <?php } else { ?>
                                <div class="btn nail_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1" class="disable">終了</a></div>
                            <?php } ?>
                        </div>

                        <div class="timeTo three_cell bgSalmonLight">
                            <div class="f26px mB40">ハンドアテンド</div>
                            <?php
                            $service_time = "00:00";
                            if (!empty($data['Order']['suborders'][0]['sr_start_date']) && !empty($data['Order']['suborders'][0]['sr_end_date'])) {
                                $service_time = displayTime($data['Order']['suborders'][0]['sr_start_date'], $data['Order']['suborders'][0]['sr_end_date']);
                            }
                            ?>
                            <div class="hours_large mB10"><?php echo $service_time ?></div>
                            <div class="hours_small">
                                <span class="fLeft service_left"><?php echo (!empty($data['Order']['suborders'][0]['sr_start_date']) && ($data['Order']['suborders'][0]['sr_start_date'] != $data['Order']['suborders'][0]['sr_end_date'])) ? date('H:i', $data['Order']['suborders'][0]['sr_start_date']) : "00:00"; ?></span>
                                <span class="fRight service_right"><?php echo (!empty($data['Order']['suborders'][0]['sr_end_date']) && ($data['Order']['suborders'][0]['sr_end_date'] != $data['Order']['suborders'][0]['sr_start_date'])) ? date('H:i', $data['Order']['suborders'][0]['sr_end_date']) : "00:00"; ?></span>
                            </div>
                            <?php if (empty($data['Order']['suborders'][0]['sr_start_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn service_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1">開始</a></div>
                            <?php } elseif (!empty($data['Order']['suborders'][0]['sr_start_date']) && empty($data['Order']['suborders'][0]['sr_end_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn service_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="0">終了</a></div>
                            <?php } else { ?>
                                <div class="btn service_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1" class="disable">終了</a></div>
                            <?php } ?>
                        </div>
                        
                        <!--Stop watch for foot-->
                        <div class="f_timeConsult three_cell">
                        </div>

                        <div class="f_timeFrom three_cell bgGrayLight">
                            <div class="f28px mB40">フットオフ</div>
                            <?php
                            $nail_time = "00:00";
                            if (!empty($data['Order']['suborders'][1]['off_start_date']) && !empty($data['Order']['suborders'][1]['off_end_date'])) {
                                $nail_time = displayTime($data['Order']['suborders'][1]['off_start_date'], $data['Order']['suborders'][1]['off_end_date']);
                            }
                            ?>
                            <div class="hours_large mB10"><?php echo $nail_time ?></div>
                            <div class="hours_small">
                                <span class="fLeft f_order_left"><?php echo (!empty($data['Order']['suborders'][1]['off_start_date']) && ($data['Order']['suborders'][1]['off_start_date'] != $data['Order']['suborders'][1]['off_end_date'])) ? date('H:i', $data['Order']['suborders'][1]['off_start_date']) : "00:00"; ?></span>
                                <span class="fRight f_order_right"><?php echo (!empty($data['Order']['suborders'][1]['off_end_date']) && ($data['Order']['suborders'][1]['off_end_date'] != $data['Order']['suborders'][1]['off_start_date'])) ? date('H:i', $data['Order']['suborders'][1]['off_end_date']) : "00:00"; ?></span>
                            </div>
                            <?php if (empty($data['Order']['suborders'][1]['off_start_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn f_nail_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1">開始</a></div>
                            <?php } elseif (!empty($data['Order']['suborders'][1]['off_start_date']) && empty($data['Order']['suborders'][1]['off_end_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn f_nail_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="0">終了</a></div>
                            <?php } else { ?>
                                <div class="btn f_nail_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1" class="disable">終了</a></div>
                            <?php } ?>
                        </div>

                        <div class="f_timeTo three_cell bgSalmonLight">
                            <div class="f26px mB40">フットアテンド</div>
                            <?php
                            $service_time = "00:00";
                            if (!empty($data['Order']['suborders'][1]['sr_start_date']) && !empty($data['Order']['suborders'][1]['sr_end_date'])) {
                                $service_time = displayTime($data['Order']['suborders'][1]['sr_start_date'], $data['Order']['suborders'][1]['sr_end_date']);
                            }
                            ?>
                            <div class="hours_large mB10"><?php echo $service_time ?></div>
                            <div class="hours_small">
                                <span class="fLeft f_service_left"><?php echo (!empty($data['Order']['suborders'][1]['sr_start_date']) && ($data['Order']['suborders'][1]['sr_start_date'] != $data['Order']['suborders'][1]['sr_end_date'])) ? date('H:i', $data['Order']['suborders'][1]['sr_start_date']) : "00:00"; ?></span>
                                <span class="fRight f_service_right"><?php echo (!empty($data['Order']['suborders'][1]['sr_end_date']) && ($data['Order']['suborders'][1]['sr_end_date'] != $data['Order']['suborders'][1]['sr_start_date'])) ? date('H:i', $data['Order']['suborders'][1]['sr_end_date']) : "00:00"; ?></span>
                            </div>
                            <?php if (empty($data['Order']['suborders'][1]['sr_start_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn f_service_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1">開始</a></div>
                            <?php } elseif (!empty($data['Order']['suborders'][1]['sr_start_date']) && empty($data['Order']['suborders'][1]['sr_end_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn f_service_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="0">終了</a></div>
                            <?php } else { ?>
                                <div class="btn f_service_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1" class="disable">終了</a></div>
                            <?php } ?>
                        </div>
                        
                        <?php else: ?>
                        
                        <div class="timeConsult three_cell bgGrownLight">
                            <div class="f28px mB40">コンサルタイム</div>
                            <?php
                            $consult_time = "00:00";
                            if (!empty($data['Order']['consult_start_date']) && !empty($data['Order']['consult_end_date'])) {
                                $consult_time = displayTime($data['Order']['consult_start_date'], $data['Order']['consult_end_date']);
                            }
                            ?>
                            <div class="hours_large mB10"><?php echo $consult_time ?></div>
                            <div class="hours_small">
                                <span class="fLeft consult_left"><?php echo (!empty($data['Order']['consult_start_date']) && ($data['Order']['consult_start_date'] != $data['Order']['consult_end_date'])) ? date('H:i', $data['Order']['consult_start_date']) : "00:00"; ?></span>
                                <span class="fRight consult_right"><?php echo (!empty($data['Order']['consult_end_date']) && ($data['Order']['consult_end_date'] != $data['Order']['consult_start_date'])) ? date('H:i', $data['Order']['consult_end_date']) : "00:00"; ?></span>
                            </div>
                            <?php if (empty($data['Order']['consult_start_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn consult_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1">開始</a></div>
                            <?php } elseif (!empty($data['Order']['consult_start_date']) && empty($data['Order']['consult_end_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn consult_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="0">終了</a></div>
                            <?php } else { ?>
                                <div class="btn consult_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1" class="disable">終了</a></div>
                            <?php } ?>
                        </div>
                        <div class="timeFrom three_cell bgGrayLight">
                            <div class="f28px mB40">オフタイム</div>
                            <?php
                            $nail_time = "00:00";
                            if (!empty($data['Order']['off_start_date']) && !empty($data['Order']['off_end_date'])) {
                                $nail_time = displayTime($data['Order']['off_start_date'], $data['Order']['off_end_date']);
                            }
                            ?>
                            <div class="hours_large mB10"><?php echo $nail_time ?></div>
                            <div class="hours_small">
                                <span class="fLeft order_left"><?php echo (!empty($data['Order']['off_start_date']) && ($data['Order']['off_start_date'] != $data['Order']['off_end_date'])) ? date('H:i', $data['Order']['off_start_date']) : "00:00"; ?></span>
                                <span class="fRight order_right"><?php echo (!empty($data['Order']['off_end_date']) && ($data['Order']['off_end_date'] != $data['Order']['off_start_date'])) ? date('H:i', $data['Order']['off_end_date']) : "00:00"; ?></span>
                            </div>
                            <?php if (empty($data['Order']['off_start_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn nail_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1">開始</a></div>
                            <?php } elseif (!empty($data['Order']['off_start_date']) && empty($data['Order']['off_end_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn nail_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="0">終了</a></div>
                            <?php } else { ?>
                                <div class="btn nail_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1" class="disable">終了</a></div>
                            <?php } ?>
                        </div>
                        <div class="timeTo three_cell bgSalmonLight">
                            <div class="f26px mB40">アテンドタイム</div>
                            <?php
                            $service_time = "00:00";
                            if (!empty($data['Order']['sr_start_date']) && !empty($data['Order']['sr_end_date'])) {
                                $service_time = displayTime($data['Order']['sr_start_date'], $data['Order']['sr_end_date']);
                            }
                            ?>
                            <div class="hours_large mB10"><?php echo $service_time ?></div>
                            <div class="hours_small">
                                <span class="fLeft service_left"><?php echo (!empty($data['Order']['sr_start_date']) && ($data['Order']['sr_start_date'] != $data['Order']['sr_end_date'])) ? date('H:i', $data['Order']['sr_start_date']) : "00:00"; ?></span>
                                <span class="fRight service_right"><?php echo (!empty($data['Order']['sr_end_date']) && ($data['Order']['sr_end_date'] != $data['Order']['sr_start_date'])) ? date('H:i', $data['Order']['sr_end_date']) : "00:00"; ?></span>
                            </div>
                            <?php if (empty($data['Order']['sr_start_date']) && empty($data['Order']['is_paid'])) { ?>
                                <div class="btn service_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1">開始</a></div>
                            <?php } elseif (!empty($data['Order']['sr_start_date']) && empty($data['Order']['sr_end_date']) && empty($data['Order']['is_paid'])) { ?>
                                                        <div class="btn service_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="0">終了</a></div>
                            <?php } else { ?>
                                                        <div class="btn service_off"><a href="#" data-id="<?php echo $data['Order']['id'] ?>" data-on="1" class="disable">終了</a></div>
                            <?php } ?>
                        </div>
                        
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div id="popupWrapperLog"></div>
            <div id="succeedPopup" class="Hidden">
                <div class="close">
                    <a href="#"><?php echo $this->Html->image('/img/close.png', array('width' => 71, 'height' => 71, 'alt' => 'close', 'data-event' => 'close', 'data-target' => '#succeedPopup')); ?></a>
                </div>
                <div id="succeedContent" class="content"><?php echo!empty($succedMsg) ? $succedMsg : ''; ?></div>
            </div>
            <div id="bgPopup" class="Hidden"></div>
            </div>


            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <!--
                        <div class="modal-header">               
                            <h4 class="modal-title" id="myModalLabel"><?php echo __('Nail attribute') ?></h4>
                        </div>
                        -->
                        <div class="modal-body">
                        </div>
                        <!--
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary">更新する</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
                        </div>
                        -->
                    </div>
                </div>
            </div>

        </body>
        <div class="loadingWrapper">
            <div class="loading"><span class="icon"></span><span><?php echo __('Saving'); ?></span></div>
        </div>
    </html>
    <script  type="text/javascript" charset="utf-8" >
        var feBaseUrl = '<?php echo Configure::read('FE.Host'); ?>';
        var baseUrl = '<?php echo $this->html->url('/'); ?>';
        var order = "time_order_<?php echo $data['Order']['id'] ?>";
        var service = "time_service_<?php echo $data['Order']['id'] ?>";
        var consult = "time_consult_<?php echo $data['Order']['id'] ?>";
        var f_order = "f_time_order_<?php echo $data['Order']['id'] ?>";
        var f_service = "f_time_service_<?php echo $data['Order']['id'] ?>";
        var paid = "paid_<?php echo $data['Order']['id'] ?>";
        var start = "start_<?php echo $data['Order']['id'] ?>";
        var has_suborder = '<?php echo!empty($data['Order']['has_suborder']) ? 1 : 0 ?>';
        var nailTab = '<?php echo !empty($nailTab) ? $nailTab : '' ?>';
        var nail_id = '';
        var f_nail_id = '';
        if (has_suborder == 1) {
            nail_id = <?php echo (isset($nail_hand['id']) ? $nail_hand['id'] : 0); ?>;
            f_nail_id = <?php echo (isset($nail_foot['id']) ? $nail_foot['id'] : 0); ?>;
        } else {
            nail_id = <?php echo (isset($data['Order']['nails'][0]['id']) ? $data['Order']['nails'][0]['id'] : 0); ?>;
        }
        var valOrder = '';
        var valService = '';
        var valConsult = '';
        var f_valOrder = '';
        var f_valService = '';
        var itemTax = '<?php echo $itemTax ?>';
        if (nail_id == 0) {
            $('.nailInfo .tab-detail-hand .fa-edit').hide();
        }
        if (f_nail_id == 0) {
            $('.nailInfo .tab-detail-foot .fa-edit').hide();
        }
    </script>
<?php } ?>