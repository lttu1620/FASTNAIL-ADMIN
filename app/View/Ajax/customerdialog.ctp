<?php
echo $this->Html->charset();
echo $this->html->css('dialog.css');

?>

<div id="popupBox_04">
    <div class="signUp">
        <div class="title">お客様情報</div>
        <?php if (!empty($msg)): ?>
            <div class="taC"><?php echo $msg; ?></div>
            <input type="hidden" id="msg" value="1" />
        <?php endif; ?>
        <?php if (!empty($errors)): ?>
            <input type="hidden" id="errors" value="1" />
        <?php endif; ?>
        <!--<form action="register" method="post" >-->
        <?php echo $this->Form->create(['type' => 'post', 'role' => 'form', 'accept-charset' => 'utf-8']) ?>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <th width="22%">お名前</th>
                <td width="78%">
                    <input class="W400" type="text" name="data[User][name]" value="<?php echo isset($data['User']['name']) ? $data['User']['name'] : ''; ?>" />
                    <?php echo $this->Item->showErrors($errors, 'name'); ?>
                </td>
            </tr>
            <tr>
                <th>お名前（カナ）</th>
                <td>
                    <input class="W400" type="text" name="data[User][kana]" value="<?php echo isset($data['User']['kana']) ? $data['User']['kana'] : ''; ?>" />
                    <?php echo $this->Item->showErrors($errors, 'kana'); ?>
                </td>
            </tr>
            <tr>
                <th>性別</th>
                <td>
                    <input type="radio" name="data[User][sex]" id="r1" value="2" <?php echo empty($data['User']['sex']) || $data['User']['sex'] == 2 ? 'checked="checked"' : ''; ?> /><label for="r1">女性</label>
                    <input type="radio" name="data[User][sex]" id="r2" value="1" <?php echo !empty($data['User']['sex']) && $data['User']['sex'] == 1 ? 'checked="checked"' : ''; ?> /><label class="mL20" for="r2">男性</label>
                </td>
            </tr>
            <tr>
                <th>生年月日</th>
                <td>
                    <select class="W140 select_style" name="birthday_year">
                        <?php foreach ($listYears as $year): ?>
                            <option <?php echo!empty($data['birthday_year']) && $data['birthday_year'] == $year ? 'selected="selected"' : ''; ?>><?php echo $year; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span>年</span>

                    <select class="W140 select_style"  name="birthday_month">
                        <?php foreach ($listMonths as $month): ?>
                            <option <?php echo!empty($data['birthday_month']) && $data['birthday_month'] == $month ? 'selected="selected"' : ''; ?>><?php echo $month; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span>月</span>

                    <select class="W140 select_style"  name="birthday_day">
                        <?php foreach ($listDays as $day): ?>
                            <option <?php echo!empty($data['birthday_day']) && $data['birthday_day'] == $day ? 'selected="selected"' : ''; ?>><?php echo $day; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span>日</span>
                    <?php echo $this->Item->showErrors($errors, 'birthday'); ?>
                </td>
            </tr>
            <tr>
                <th>住所</th>
                <td>
                    <div class="mR20 W140" style="vertical-align:top; display: inline-block">
                        <select class="W140 select_style" name="data[User][prefecture_id]">
                            <?php foreach ($listPrefecture as $prefectureId => $prefectureName): ?>
                                <option <?php echo!empty($data['User']['prefecture_id']) && $data['User']['prefecture_id'] == $prefectureId ? 'selected="selected"' : ''; ?> value="<?php echo $prefectureId; ?>"><?php echo $prefectureName; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="W400 address" style="display: inline-block">
                        <div class="mB20" style="margin-bottom: 20px">
                            <span>住所1</span>
                            <span><input class="W350" type="text" name="data[User][address1]" value="<?php echo isset($data['User']['address1']) ? $data['User']['address1'] : ''; ?>" /></span>
                            <?php echo $this->Item->showErrors($errors, 'address1'); ?>
                        </div>
                        <div>
                            <span>住所2</span>
                            <span><input class="W350" type="text" name="data[User][address2]" value="<?php echo isset($data['User']['address2']) ? $data['User']['address2'] : ''; ?>" /></span>
                            <?php echo $this->Item->showErrors($errors, 'address2'); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>電話番号</th>
                <td>
                    <span><input maxlength="3" class="W120 numberOnly" type="text" name="data[User][phone_1]" value="<?php echo isset($data['User']['phone_1']) ? $data['User']['phone_1'] : ''; ?>" /></span>
                    <span><input maxlength="4" class="W120 numberOnly" type="text" name="data[User][phone_2]" value="<?php echo isset($data['User']['phone_2']) ? $data['User']['phone_2'] : ''; ?>" /></span>
                    <span><input maxlength="4" class="W120 numberOnly" type="text" name="data[User][phone_3]" value="<?php echo isset($data['User']['phone_3']) ? $data['User']['phone_3'] : ''; ?>" /></span>
                    <?php echo $this->Item->showErrors($errors, 'phone_1'); ?>
                    <?php echo $this->Item->showErrors($errors, 'phone_2'); ?>
                    <?php echo $this->Item->showErrors($errors, 'phone_3'); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __('Email'); ?></th>
                <td>
                    <input class=" W400" type="text" name="data[User][email]" <?php echo !empty($id) ? 'readonly' : ''?> value="<?php echo isset($data['User']['email']) ? $data['User']['email'] : ''; ?>" />
                    <?php echo $this->Item->showErrors($errors, 'email'); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __('Password'); ?></th>
                <td>
                    <input class=" W400" type="password" name="data[User][password]" value="" />
                    <?php echo $this->Item->showErrors($errors, 'password'); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __('Confirm password'); ?></th>
                <td>
                    <input class=" W400" type="password" name="data[User][password_confirm]" value="" />
                    <?php echo $this->Item->showErrors($errors, 'password_confirm'); ?>
                </td>
            </tr>
            <tr>
                <th class="radioGroup">メールマガジン<br/>配信サービス(無料）</th>
                <td>
                    <input type="radio" name="data[User][is_magazine]" id="r11" value="1" <?php echo empty($data['User']['is_magazine']) || $data['User']['is_magazine'] == 1 ? 'checked="checked"' : ''; ?> /><label for="r11">購読する</label>
                    <input type="radio" name="data[User][is_magazine]" id="r12" value="0" <?php echo isset($data['User']['is_magazine']) && $data['User']['is_magazine'] == 0 ? 'checked="checked"' : ''; ?> /><label class="mL20" for="r12">購読しない</label>
                </td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <td>
                    <input type="hidden" name="data[User][id]" value="<?php echo !empty($id) ? $id : ''?>" />
                    <input value="会員登録する" class="btn bgSalmon" id="btnCusSubmit" style="width:140px;" type="button">
                </td>
            </tr>
        </table>
        </form>
    </div>
</div>