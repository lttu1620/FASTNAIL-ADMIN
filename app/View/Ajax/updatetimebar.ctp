<?php
if (!isset($data['reservation_all'])) {
    echo "$('#messages').alertFade(\"<div id=\'messages\'><\/div>\");\n";
    echo "Timetable.draw_timebar(" . $data . "['reservation']);";
} else {
    $order_id = $data['order_id'];
    if ($data['type_err'] == 3) {
        ?>
        var action_query = '<?php echo !empty($data['is_seat']) ? '&is_seat=1' : ''; ?>';
        var url = '<?php echo Router::url('/ajax/updatetimebar?order_id='.$order_id); ?>' + action_query
        var dt = {confirm_yes : 1};
        <?php 
            if(!empty($data['start_at_epoch'])){
                echo "dt.start_at_epoch = {$data['start_at_epoch']};";
            }
            if(!empty($data['duration'])){
                echo "dt.duration = {$data['duration']};";
            }
        ?>
        durationConfirm(dt, url); //call this funtion from calendar.js
        <?php
    } elseif ($data['type_err'] == 2) {
        $errWarning = '滞在時間は900以上の値にしてください。';
    } elseif ($data['type_err'] == 1) {
        $errWarning = '営業時間外の予約です';
    } else {// 0
        $errWarning = '既にある予約と、時間がかぶっています';
    }

    echo "window.reservations = " . json_encode($data['reservation_all']) . ";\n";
    echo "window.blockages = [];\n";
    echo "Timetable.draw();\n";
    if ($data['type_err'] == 3) {
        echo "$('#messages').alertFade(\"<div id=\'messages\'><\/div>\");\n";
    } else {
        echo "$('#messages').alertFade('<div id=\"messages\"><div class=\"alert alert-error float\"><a class=\"close\" data-dismiss=\"alert\">×<\/a><div id=\"flash_error\">{$errWarning}<\/div><\/div><\/div>');\n";
        echo "$('#timebar-container .timebar[data-id={$order_id}]').effect(\"highlight\", {color: '#ff0000'}, 5000);\n";
        echo "$('#res-noshow .timebar[data-id={$order_id}]').effect(\"highlight\", {color: '#ff0000'}, 5000);\n";
        echo "$('#res-cancelled .timebar[data-id={$order_id}]').effect(\"highlight\", {color: '#ff0000'}, 5000);\n";
        echo "$('#res-limbo .timebar[data-id={$order_id}]').effect(\"highlight\", {color: '#000000'}, 5000);";
    }
}

        
