<style>
.col-md-120 .alert {   
    padding-bottom: 5px;    
}
.col-md-120 .group-checkbox .form-control {   
    width: 50%;
    position: relative;
}
.col-md-120 .btn {
    padding: 6px 12px !important;
}
.col-md-120 .box.box-primary {
    border-top: 1px solid #eee;   
}
</style>
<div class="col-md-120">    
    <div class="box box-primary mB0">   
        <div class="box-body">    
            <div id="popup_message"></div>
            <?php
                echo $this->SimpleForm->render($updateForm);
            ?>
        </div>
    </div>
</div>
