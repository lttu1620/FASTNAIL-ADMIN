<?php
    echo $this->Html->css('calendar/bootstrap-2-bundle.css'); 
    echo $this->Html->css('calendar/calendar.css'); 
    echo $this->Html->script('calendar/calendar.js');
   
?>

<script type="text/javascript">
//<![CDATA[
    I18n.defaultLocale = 'ja';
    I18n.locale = 'ja';
//]]>
</script>
<style>
    body{
          overflow-x: hidden;
          overflow-y: hidden;
    }
</style>

<?php if(!empty($info)):?>
<div class='modal modal-large hide fade' data-keyboard='false' id='ajax-modal' tabindex='-1'></div>
<div id='messages'>
</div>
<div id='main' role='main'>
    <div id='workspace'>
        <div id='reservation-form'>
            <div class='navbar navbar-static-top subnav'>
                <div class='navbar-inner'>
                    <h1>予約を作成する</h1>
<!--                    <ul class='nav nav-pills'>
                        <li class='active'>
                            <a class='show-field-group-1' data-target='reservations'>基本項目</a>
                        </li>
                        <li>
                            <a class='show-field-group-3' data-target='reservations'>注文、メモ</a>
                        </li>
                    </ul>-->

                </div>
            </div>

            <form accept-charset="UTF-8" class="simple_form form-horizontal" data-validate="true" id="new_reservation" method="post" novalidate="novalidate">
                <div style="margin:0;padding:0;display:inline">
                    <input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="qA4phUKiuqsQpXpqRj6qAsSNmjgt7a/MvklALPSNRWs=" />
                </div>
                <div id='reservation-form-body'>
                    <div id='reservations-field-group-1'>
                        <div class="control-group string required reservation_start_at fat-bottom"><label class="string required control-label" for="reservation_start_at"><abbr title="必須">*</abbr> 到着時間</label><div class="controls"><input autocomplete="off" class="string required form-control" id="reservation_start_at" name="reservation[start_at]" size="50" style="display:none" type="text" value="<?php echo $info['reservation_date'] ?>" /></div></div>
                        <script type="text/javascript">
                            //<![CDATA[
                            $('#reservation-form-body #reservation_start_at').mobiscroll().datetime({
                                mode: 'scroller',
                                height: 30,
                                display: 'inline',
                                theme: 'tsmobi',
                                rows: 3,
                                dayNamesShort: I18n.t('date.abbr_day_names'),
                                monthNamesShort: I18n.t('date.abbr_month_names').slice(1),
                                yearText: I18n.t('datetime.prompts.year'),
                                monthText: I18n.t('datetime.prompts.month'),
                                dayText: I18n.t('datetime.prompts.day'),
                                dateFormat: 'yy-mm-dd',
                                dateOrder: I18n.asian_locale() ? 'yyMd D' : 'yyMD d',
                                hourText: I18n.t('datetime.prompts.hour'),
                                minuteText: I18n.t('datetime.prompts.minute'),
                                timeFormat: 'HH:ii',
                                timeWheels: 'Hii',
                                stepMinute: 15,
                                endYear: new Date().getFullYear() + 3,
                                startYear: new Date().getFullYear() - 5,
                                onChange: function (date, inst) {
                                    if (date) {
                                        var parts = date.split(' ')[0].split('-');
                                        toggleStartAtWarning(new Date(parts[0], parts[1] - 1, parts[2]));
                                        newDate = parts.join('-');
                                        ReservationForm.ajaxRefreshStylists(newDate);
                                    }
                                }
                            });
                            //]]>
                        </script>
                        <script>
                            function toggleStartAtWarning(date) {
                                var yest = (new Date()).setDate((new Date()).getDate() - 1);
                                var future = (new Date()).setDate((new Date()).getDate() + 90);
                                $('.reservation_start_at_warning_future').toggle(date > future);
                                $('.reservation_start_at_warning_past').toggle(date < yest);
                                resizeDialog(0);
                            }
                        </script>
                        <script>
                            $(document).ready(function () {
                                toggleStartAtWarning(Date.parse('<?php echo date('Y/m/d', $info['startAtTimeStamp']) ?>'));
                            });
                        </script>
                        <div class="control-group string optional reservation_start_at_warning_past hide"><div class="controls"><div class='alert alert-danger' style='margin-bottom: 0'>
                                    <b>ご注意ください:</b>
                                    過去の予約です
                                </div>
                            </div></div><div class="control-group string optional reservation_start_at_warning_future hide"><div class="controls"><div class='alert alert-warning' style='margin-bottom: 0'>
                                    <b>ご注意ください:</b>
                                    3ヶ月以上先の予約です
                                </div>
                            </div></div><div class='row'>
                            <div class='col-sm-6'>
                                <div class="control-group string optional reservation_duration"><label class="string optional control-label" for="reservation_duration">滞在時間</label><div class="controls"><select class="select required form-control" id="reservation_duration" name="reservation[duration]" style="width: 150px">

                                            <option value=""></option>
                                            <?php if (!empty($info['reservation_duration'])): ?>
                                                <?php foreach ($info['reservation_duration'] as $k => $v): ?>
                                                    <option value="<?php echo $k ?>" <?php echo ($k == $info['duration']) ? 'selected' : '' ?> ><?php echo $v ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div></div></div>
                        </div>
               
                        <div class='row'>
                            <div class="control-group string optional reservation_source_media"><label class="string optional control-label" for="reservation_duration">来店経路</label><div class="controls"><div class='col-sm-5'>
                                        <select class="select optional form-control" id="reservation_source" name="reservation[visit_element]" prompt="-- 来店経路 --" style="width: 100%;"><option value="">-- 来店経路 --</option>
                                            <?php if (!empty($info['reservation_element'])): ?>
                                                <?php foreach ($info['reservation_element'] as $k => $v): ?>
                                                    <option value="<?php echo $k ?>" <?php echo ($k == $info['visit_element']) ? 'selected' : '' ?> ><?php echo $v ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>  
                                        </select>
                                    </div>
                                    <div class='col-sm-5'>
                                        <select class="grouped_select optional form-control" group_label_method="first" group_method="last" id="reservation_media" name="reservation[visit_section]" prompt="-- きっかけ --" style="width: 100%;"><option value="">-- きっかけ --</option>
                                            <?php if (!empty($info['reservation_section'])): ?>
                                                <?php foreach ($info['reservation_section'] as $key => $value): ?>
<!--                                                    <optgroup label="<?php //echo $key ?>">-->
                                                        //<?php //foreach ($value as $k => $v): ?>
                                                            <option value="<?php echo $key ?>" <?php echo ($key == $info['visit_section']) ? 'selected' : '' ?>><?php echo $value ?></option>
                                                        //<?php //endforeach; ?>
<!--                                                    </optgroup>-->
                                                <?php endforeach; ?>
                                            <?php endif; ?>  
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-12'>
                                <div class='control-group control-group-multirow'>
                                    <div class='control-label control-label-btn'>
                                        <a href="javascript:void(0)" id="btn_add_nested_fields" class="btn btn-small btn-success left-margin add_nested_fields" data-association="stylist_seats" data-blueprint-id="stylist_seats_fields_blueprint" data-target="#stylist-fields"><span class="x16-sprite x16-staff_add" height="16" width="16"></span> スタッフを追加</a>
                                    </div>
                                    <div class='controls' id='stylist-fields'>
                                        <div class="tmpfields" style="display: none">
                                            <select class="stylist" id="reservation_stylist_seats_attributes_new_stylist_seats_stylist_id" name="reservation[stylist_seats_attributes][new_stylist_seats][stylist_id]"><option value="">-- スタッフ --</option>
                                            <?php if (!empty($info['available_list'])): ?>
                                                <?php foreach ($info['available_list'] as $nl): ?>
                                                    <option value="<?php echo $nl['id']?>" data-num_seats="1"><?php echo $nl['name']?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>  
                                            </select>
                                            <select class="select required form-control seat_num" data-selected-seat="0" id="reservation_stylist_seats_attributes_new_stylist_seats_seat_num" name="reservation[stylist_seats_attributes][new_stylist_seats][seat_num]" style="width:100px; display:none">
                                                <option value=""></option>
                                            </select>
                                            <input id="reservation_stylist_seats_attributes_new_stylist_seats__destroy" name="reservation[stylist_seats_attributes][new_stylist_seats][_destroy]" type="hidden" value="false" /><a href="javascript:void(0)" class="btn btn-small btn-danger remove_nested_fields" data-association="stylist_seats" onclick="resizeDialog(1)">
                                                <span class="x16-sprite x16-staff_delete" height="16" width="16"></span> 削除する</a>
                                        </div>
                                        
                                         <?php if (!empty($info['selected_ids'])) : ?>
                                            <?php 
                                            $num = 0;
                                            foreach ($info['selected_ids'] as $id) 
                                                : ?>
                                                <div class="fields"><!-- Using select instead of #input_field as it allows to set data-attributes here. -->
                                            <select class="stylist" id="reservation_stylist_seats_attributes_<?php echo $num?>_stylist_id" name="reservation[stylist_seats_attributes][<?php echo $num?>][stylist_id]"><option value="">-- スタッフ --</option>
                                            <?php if (!empty($info['available_list'])): ?>
                                                <?php foreach ($info['available_list'] as $nl): ?>
                                                    <option value="<?php echo $nl['id']?>" <?php echo ($nl['id'] == $id) ? 'selected': ''?> data-num_seats="1"><?php echo $nl['name']?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>  
                                            </select>
                                            <select class="select required form-control seat_num" data-selected-seat="0" id="reservation_stylist_seats_attributes_<?php echo $num?>_seat_num" name="reservation[stylist_seats_attributes][<?php echo $num?>][seat_num]" style="width:100px; display:none">
                                                <option value=""></option>
                                            </select>
                                            <input id="reservation_stylist_seats_attributes_<?php echo $num?>__destroy" name="reservation[stylist_seats_attributes][<?php echo $num?>][_destroy]" type="hidden" value="false" /><a href="javascript:void(0)" class="btn btn-small btn-danger remove_nested_fields" data-association="stylist_seats" onclick="resizeDialog(1)">
                                                <span class="x16-sprite x16-staff_delete" height="16" width="16"></span> 削除する</a>
                                        </div>
                                            <?php $num++;
                                            endforeach; ?>
                                         <?php endif; ?>  
                                    </div>
                                </div>
                                
                                <script type="text/javascript">

                                    window.stylistsAvailable = <?php echo json_encode($info['available_list'])?>;
                                    $(function () {
                                        ReservationForm.hideUnavailableStylists();
                                    });
                                </script>

                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-6'>
                            </div>
                        </div>

                        <div class="control-group string optional reservation_customers"><label class="string optional control-label" for="reservation_customers">顧客</label><div class="controls"><div class='width90' id='res-form-cus-added'></div>
                                <div class='well well-small width90' style='margin-bottom: 0px'>
                                    <div class='pull-right'>
<!--                                        <a class='btn btn-danger btn-small' id='res-form-cus-clear'><span class="x16-sprite x16-customer_delete" height="16" width="16"></span> クリア</a>-->
                                    </div>
                                    <div class='' style='margin-bottom: 5px'>
                                        <input autocomplete="off" value="<?php echo !empty($info['user_name'])? $info['user_name'] : ''?>" class="string optional form-control" id="reservation_customer_last_name" name="reservation[customer][user_name]" placeholder="名前" size="50" style="width: 150px" type="text" />
                                        <input autocomplete="off" maxlength="11" value="<?php echo !empty($info['phone'])? $info['phone'] : ''?>" class="string tel required form-control form-control" id="reservation_customer_phones_attributes_0_number" name="reservation[customer][phone]" placeholder="電話番号" size="50" style="width: 150px" type="tel" />
<!--                                        <input autocomplete="off" value="<?php //echo !empty($info['kana'])? $info['kana'] : ''?>" class="string optional form-control" id="reservation_customer_first_name" name="reservation[customer][kana]" placeholder="名前（カタカナ）" size="50" style="width: 150px" type="text" />-->
<!--                                        <input autocomplete="off" class="string optional form-control" id="reservation_customer_company_name" name="reservation[customer][company_name]" placeholder="会社名" size="50" style="width: 150px" type="text" />-->
                                    </div>
                                    <div class='fields-inline'>
<!--                                        <div class="fields">
                                            <input autocomplete="off" value="<?php echo !empty($info['phone'])? $info['phone'] : ''?>" class="string tel required form-control form-control" id="reservation_customer_phones_attributes_0_number" name="reservation[customer][phone]" placeholder="電話番号" size="50" style="width: 150px" type="tel" />
                                        </div>-->
                                        <div class="fields">
                                            <input autocomplete="off" value="<?php echo !empty($info['email'])? $info['email'] : ''?>" class="string email required form-control form-control" id="reservation_customer_emails_attributes_0_email" name="reservation[customer][email]" placeholder="Eメール" size="50" style="width: 190px" type="email" />
                                        </div>
                                        <div class='btn-group' data-toggle='buttons'>
                                            <label class="radio btn btn-toggle" style="display: none">
                                                <input  checked="checked" class="radio_buttons required" id="reservation_customer_sex_0" item_wrapper_class="btn btn-toggle" name="reservation[customer][sex]" type="radio" value="0" /><span alt="?" class="x16-sprite x16-question" height="16" width="16"></span>
                                            </label>
                                            <label class="radio btn btn-toggle <?php echo !empty($info['sex'] && $info['sex'] == 1)? "active" : ''?>">
                                                    <input <?php echo !empty($info['sex'] && $info['sex'] == 1)? "checked=\"checked\"" : ''?> class="radio_buttons required" id="reservation_customer_sex_1" item_wrapper_class="btn btn-toggle" name="reservation[customer][sex]" type="radio" value="1" /><span alt="男" class="x16-sprite x16-male" height="16" width="16"></span>
                                            </label>
                                            <label class="radio btn btn-toggle <?php echo !empty($info['sex'] && $info['sex'] == 2)? "active" : ''?>">
                                                        <input <?php echo !empty($info['sex'] && $info['sex'] == 2)? "checked=\"checked\"" : ''?>class="radio_buttons required" id="reservation_customer_sex_2" item_wrapper_class="btn btn-toggle" name="reservation[customer][sex]" type="radio" value="2" /><span alt="女" class="x16-sprite x16-female" height="16" width="16"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class='width90' id='res-form-cus-results'></div>
                            </div></div><script>
                                res_cus_query = new window.ResCustomersQuery('#reservation-form-body');
                                res_cus_query.populate_existing_customers([]);
                        </script>

                    </div>
                    <div class='hide' id='reservations-field-group-2'>

                    </div>
                    <div class='hide' id='reservations-field-group-3'>
                        <div class="control-group text optional reservation_memo"><label class="text optional control-label" for="reservation_memo">メモ</label><div class="controls"><textarea class="text optional form-control width90" cols="40" id="reservation_memo" name="reservation[memo]" rows="3"></textarea></div></div>
                        <div class="control-group string optional reservation_points"><label class="string optional control-label" for="reservation_points">ポイント</label><div class="controls"><input class="string optional form-control col-sm-2" id="reservation_points" name="reservation[points]" size="50" style="margin-right: 30px" type="text" />
                                <input name="reservation[is_first_visit]" type="hidden" value="0" /><label class="checkbox"><input class="boolean optional" id="reservation_is_first_visit" inline_label="true" name="reservation[is_first_visit]" type="checkbox" value="1" />初めての来店</label>
                            </div></div><div class='control-group control-group-multirow'>
                            <div class='control-label control-label-btn'>
                                <a href="javascript:void(0)" class="btn btn-small btn-success left-margin add_nested_fields" data-association="orders" data-blueprint-id="orders_fields_blueprint" data-target="#orders-fields"><span class="x16-sprite x16-order_add" height="16" width="16"></span> 注文を追加</a>
                            </div>
                            <div class='controls' id='orders-fields'>
                                
                                <div class="tmpfields" style="display: none"><div class='fat-bottom'>
                                        <span class="grouped_select required reservation_orders_menu_item"><select class="grouped_select required form-control span4" id="reservation_orders_attributes_new_orders_menu_item_id" name="reservation[orders_attributes][new_orders][menu_item_id]"><option value="">-- 注文を選択してください --</option>
                                                <optgroup label="初期設定カテゴリ"><option value="53105fe74fb368637600010b">C・CＬ</option>
                                                    <option value="537b207d080366df8a000550">C・CＬああだ</option>
                                                    <option value="531566550803660c490000f1">C・P</option></optgroup></select></span>
                                        <div class='input-prepend'>
                                            <div class='add-on fat'>x</div>
                                            <select class="select required form-control" default="1" id="reservation_orders_attributes_new_orders_qty" name="reservation[orders_attributes][new_orders][qty]" style="width:80px;">
                                                    <?php for($i=1; $i<=999; $i++  ): ?>
                                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                    <?php endfor; ?>
                                            </select>
                                        </div>
                                        <input id="reservation_orders_attributes_0__destroy" name="reservation[orders_attributes][new_orders][_destroy]" type="hidden" value="false" /><a href="javascript:void(0)" class="btn btn-small btn-danger remove_nested_fields" data-association="orders"><span class="x16-sprite x16-order_delete" height="16" width="16"></span> 削除する</a>
                                    </div>
                                </div>
                                
                                
                                <div class="fields"><div class='fat-bottom'>
                                        <span class="grouped_select required reservation_orders_menu_item"><select class="grouped_select required form-control span4" id="reservation_orders_attributes_0_menu_item_id" name="reservation[orders_attributes][0][menu_item_id]"><option value="">-- 注文を選択してください --</option>
                                                <optgroup label="初期設定カテゴリ"><option value="53105fe74fb368637600010b">C・CＬ</option>
                                                    <option value="537b207d080366df8a000550">C・CＬああだ</option>
                                                    <option value="531566550803660c490000f1">C・P</option></optgroup></select></span>
                                        <div class='input-prepend'>
                                            <div class='add-on fat'>x</div>
                                            <select class="select required form-control" default="1" id="reservation_orders_attributes_0_qty" name="reservation[orders_attributes][0][qty]" style="width:80px;">
                                                    <?php for($i=1; $i<=999; $i++  ): ?>
                                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                    <?php endfor; ?>
                                            </select>
                                        </div>
                                        <input id="reservation_orders_attributes_0__destroy" name="reservation[orders_attributes][0][_destroy]" type="hidden" value="false" /><a href="javascript:void(0)" class="btn btn-small btn-danger remove_nested_fields" data-association="orders"><span class="x16-sprite x16-order_delete" height="16" width="16"></span> 削除する</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <script>
                    window.BsHack.fix_btn_toggle();
                    $(function () {
                        window.ReservationForm.init();
                    });
                </script>
                
                    <div class="modal-footer" style="margin-top: 63px; -webkit-border-radius: 0 0 0 0">
                        <?php if(!empty($info['order_id'])):?>
                        <div class="pull-left">
                            <input class="btn btn-danger"  name="delete" id="delete" data-start-timestamp="<?php echo date('Y-m-d',$info['startAtTimeStamp'])?>" data-order-id="<?php echo $info['order_id']?>" type="button" value="予約を削除する">
                        </div>
                        <input type="hidden" value="<?php echo $info['order_id']?>" name="reservation[order_id]">
                        <?php endif;?>
                        <input class="btn btn-primary" data-disable-with="お待ちください・・・" name="commit" id="commit" type="button" value="更新する">
                        <button class="btn" onclick="parent.closeBookingDialog('<?php echo date('Y-m-d',$info['startAtTimeStamp'])?>',1)">閉じる</button>
                    </div>
                <input type="hidden" value="<?php echo $info['shop_id']?>" name="reservation[shop_id]">
                <input type="hidden" value="<?php echo $info['cart_id']?>" name="reservation[cart_id]">
                <input type="hidden" value="<?php echo $info['user_id']?>" name="reservation[user_id]">
                
                <div data-blueprint="" id="stylist_seats_fields_blueprint" style="display: none"></div> <div data-blueprint="" id="orders_fields_blueprint" style="display: none"></div></form>
                     
            <script>//<![CDATA[
                if (window.ClientSideValidations === undefined)
                    window.ClientSideValidations = {};
                window.ClientSideValidations.disabled_validators = [];
                window.ClientSideValidations.number_format = {"separator": ".", "delimiter": ","};
                if (window.ClientSideValidations.patterns === undefined)
                    window.ClientSideValidations.patterns = {};
                window.ClientSideValidations.patterns.numericality = /^(-|\+)?(?:\d+|\d{1,3}(?:\,\d{3})+)(?:\.\d*)?$/;
                if (window.ClientSideValidations.forms === undefined)
                    window.ClientSideValidations.forms = {};
                window.ClientSideValidations.forms['new_reservation'] = {"type": "NestedForm::SimpleBuilder", "error_class": "help-inline", "error_tag": "span", "wrapper_error_class": "error", "wrapper_tag": "div", "wrapper_class": "control-group", "wrapper": "bootstrap", "validators": {"reservation[start_at]": {"presence": [{"message": "\u3092\u5165\u529b\u3057\u3066\u304f\u3060\u3055\u3044\u3002"}], "timeliness": [{"message": "translation missing: ja.mongoid.errors.models.reservation.attributes.start_at.timeliness"}]}, "reservation[duration]": {"presence": [{"message": "\u3092\u5165\u529b\u3057\u3066\u304f\u3060\u3055\u3044\u3002"}], "fifteen_minutes": [{"message": "translation missing: ja.mongoid.errors.models.reservation.attributes.duration.fifteen_minutes", "allow_blank": true}], "numericality": [{"messages": {"numericality": "\u306f\u6570\u5024\u3067\u5165\u529b\u3057\u3066\u304f\u3060\u3055\u3044\u3002", "only_integer": "\u306f\u6574\u6570\u3067\u5165\u529b\u3057\u3066\u304f\u3060\u3055\u3044\u3002", "greater_than_or_equal_to": "\u306f900\u4ee5\u4e0a\u306e\u5024\u306b\u3057\u3066\u304f\u3060\u3055\u3044\u3002"}, "only_integer": true, "allow_blank": true, "greater_than_or_equal_to": 900}]}, "reservation[source]": {"inclusion": [{"message": "\u306f\u4e00\u89a7\u306b\u3042\u308a\u307e\u305b\u3093\u3002", "in": ["phone", "web", "email", "in_person", "walk_in", "app", "other"], "allow_blank": true}]}, "reservation[media]": {"inclusion": [{"message": "\u306f\u4e00\u89a7\u306b\u3042\u308a\u307e\u305b\u3093\u3002", "in": ["beautycheck", "cpon", "hotpepper", "luxa", "customer", "staff", "concierge", "travel", "hotel", "dm", "mail_mag", "media", "marketing", "other"], "allow_blank": true}]}, "reservation[points]": {"numericality": [{"messages": {"numericality": "\u306f\u6570\u5024\u3067\u5165\u529b\u3057\u3066\u304f\u3060\u3055\u3044\u3002", "only_integer": "\u306f\u6574\u6570\u3067\u5165\u529b\u3057\u3066\u304f\u3060\u3055\u3044\u3002", "greater_than_or_equal_to": "\u306f0\u4ee5\u4e0a\u306e\u5024\u306b\u3057\u3066\u304f\u3060\u3055\u3044\u3002", "less_than_or_equal_to": "\u306f1000000\u4ee5\u4e0b\u306e\u5024\u306b\u3057\u3066\u304f\u3060\u3055\u3044\u3002"}, "only_integer": true, "allow_blank": true, "greater_than_or_equal_to": 0, "less_than_or_equal_to": 1000000}]}}};
                //]]></script>
        </div>
    </div>

</div>
<?php else:?>
        <div id="main" role="main">
        <div id="workspace">
            <p style="padding: 216px 20px 180px 391px; font-size: 72; color: red;"><?php echo __('Empty data')?></p>
            <div class="modal-footer" style="margin-top: 63px; -webkit-border-radius: 0 0 0 0">
                <button class="btn" onclick="parent.closeBookingDialog('<?php echo date('Y-m-d',time())?>',1)">閉じる</button>
            </div>
        </div>
    </div>
<?php endif;?>


<div style='display: none'>
    <img alt="" height="16" src="<?php echo $this->Html->image('loading_gray.gif'); ?>" width="16" />
    <img alt="" height="16" src="<?php echo $this->Html->image('loading.gif'); ?>" width="16" />
    <img alt="" height="48" src="<?php echo $this->Html->image('loading-1.gif'); ?>" width="48" />
</div>
