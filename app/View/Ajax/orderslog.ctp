<?php
    echo $this->Html->script('function.js');
    echo $this->Html->script('orderslog.js');
?>
<?php if (!empty($user_id)): ?>
    <div class="popupContent log" id="backtotop">
        <div class="nailList">
            <div class="close"><a href="">close</a></div>
            <div class="ico_list"></div>
            <form id="frm_user_log">
                <div class="dropdownList" style="display: none">
                    <label><?php echo __('Nailist')?></label>
                    <select class="wP100_full select_style f14px" name="userlogs[nailist_id]" id="userlogs-nailist">
                        <?php foreach($listNailist as $val):?>
                            <option value="<?php echo $val['id'] ?>" ><?php echo $val['name']?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="dropList">
                    <label>今回の担当者</label>
                    <ul class="drop-nailist">
                        <li class="user">
                        <a href="javascript:void(0);" class="b_nailists" data-id="">ネイリストの指名</a>                
                            <span class="navDown_02">ネイリスト</span>
                            <ul id="logs_nailists">
                                <?php foreach ($listNailist as $value): ?>
                                <li class="user">
                                    <a href="javascript:void(0);" class="nailists user-log" data-id="<?php echo $value['id']?>" data-name="<?php echo $value['name']?>">
                                        <div class="avatar">
                                            <img src="<?php echo $value['image_url']?>" alt="<?php echo $value['name']?>">
                                        </div>
                                        <?php echo $value['name']?>
                                    </a>
                                </li>
                                <?php endforeach; ?>    
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="sectionSub radius5 bgSalmonLight mB20">
                        <input type="hidden" name="userlogs[user_id]" value="<?php echo $user_id; ?>" />
                        <input type="hidden" name="userlogs[order_id]" value="<?php echo $order_id; ?>" />
                        <input type="hidden" name="userlogs[cur_limit]" value="<?php echo $cur_limit; ?>" />
                        <textarea name="userlogs[memo]" class="textComment" role="textbox" placeholder="<?php echo __('Comment here')?>"></textarea>
                        <button id="addLog" class="btn post bgSalmonDark white W120 fRight f18px"><strong>投稿する</strong></button>
                </div>
            </form>    
            <div class='log-list'>
            <?php foreach ($listLog as $log): ?>
                <div class="sectionSub radius5 border">
                    <div class="avatar"><img src="<?php echo $log['nailist_image']; ?>" ></div>
                    <div class="textNote"><?php echo nl2br($log['memo']); ?></div>
                    <div class="name">
                        <div><?php echo $log['shop_name']; ?></div>
                        <div><?php echo $log['nailist_name']; ?></div>
                    </div>
                    <div class="date"><?php echo date("Y.m.d", $log['created']); ?></div>
                </div>
            <?php endforeach ?>
            </div>
        </div>
        <?php if(!empty($listLog) && ($total > $cur_limit)){?>
        		<div class="btn bgGray wP100 mT40 mB20"><a href="javascript:void(0)" id="read-more" data-order-id="<?php echo $order_id; ?>" data-cur-limit="<?php echo $cur_limit; ?>" >もっと見る</a></div>
        <?php }else{?>
        		<div class="btn bgGray wP100 mT40 mB20" style="display: none"><a href="javascript:void(0)" id="read-more" data-order-id="<?php echo $order_id; ?>" data-cur-limit="<?php echo $cur_limit; ?>" >もっと見る</a></div>
        <?php }?>
    </div>
<?php else: ?>
    <div class="popupContent">
        <div class="nailList">
            <div class="close"><a href="">close</a></div>
            <div class="ico_list"></div>
            <p style="padding: 130px 20px 0 500px;font-size: 51px;"><?php echo '会員登録をしてください' ?><p>
        </div>
    </div>
    </div>
    </div>
<?php endif; ?>












