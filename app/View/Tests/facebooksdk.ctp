<script src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
$(function() {
    FB.init({
        appId: '<?php echo Configure::read('Facebook.appId')?>', 
        cookie: true, 
        status: true, 
        oauth: true,
        xfbml: true		
    });    
});
</script>
<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>