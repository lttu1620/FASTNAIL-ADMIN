
<br/><br/>
<form method="post">
	<img src="<?php echo base64_decode($imageUrl)?>" id="image">
	<br/><br/>
  <input type="hidden" name="image_url" value="<?php echo $imageUrl?>" /> <br/>
  <input type="text" name="x1" value="" placeholder="x1" /> <br/>
  <input type="text" name="y1" value="" placeholder="y1" /> <br/>
  <input type="text" name="x2" value="" placeholder="x2"/> <br/>
  <input type="text" name="y2" value="" placeholder="y2"/> <br/>
  <input type="text" name="width" value="" placeholder="width" /> <br/>
  <input type="text" name="height" value="" placeholder="height" /> <br/>
  <input type="submit" name="submit" value="Submit" />
</form>

<a class="dialog" href="<?php echo($this->Html->url('/ajax/dialog'))?>" alt="Dialog">Dialog</a>
<script>
    $(document).ready(function () {
    $('#image').imgAreaSelect({
        handles: true,
        onSelectEnd: function(img, selection){
            if (!selection.width || !selection.height){
                return;
            }
			$('input[name="x1"]').val(selection.x1);
            $('input[name="y1"]').val(selection.y1);
            $('input[name="x2"]').val(selection.x2);
            $('input[name="y2"]').val(selection.y2);   
            $('input[name="width"]').val(selection.width);   
            $('input[name="height"]').val(selection.height);   			
        },
		aspectRatio: '1:1'		
    });	
});
</script>