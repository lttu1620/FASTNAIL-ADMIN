<tr>
    <td><button class="btn btn-primary" id="orderLogsButton">Primary</button></td>
</tr>

<div class="bgBlack"></div>
<div id="popupWrapperLog"></div>

<script>
    $(document).ready(function () {
        $('#orderLogsButton').click(function () {
            $.ajax({
                type: "POST",
                url: baseUrl + 'ajax/orderslog/3',
                data: {},
                success: function (response) {
                    $('#popupWrapperLog').html(response);
                }
            });
        });
    });
    
</script>
