<h1>Register</h1>
<?php
echo $this->Form->create('test', array(
    'action' => '/validation',
    'class' => 'form-horizontal', 
    'role' => 'form',
    'inputDefaults' => array( 
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'control-group'),
            'label' => array('class' => 'control-label'),
            'between' => '<div class="controls">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        )
    )
);
$errorUsername = '';
if(isset($errors) && !empty($errors['username']))
{ 
    $errorUsername = $errors['username'][0];
}
echo $this->Form->input('username', array(
        'after' => '<span class="help-block">' . $errorUsername . '</span></div>',
        'error' => array('attributes' => array('class' => 'controls help-block'))
    )
);

/*
if(isset($errors) && !empty($errors['username']))
{ 
    echo $errors['username'][0];
}
 * 
 */
echo $this->Form->input('email', array());
echo $this->Form->input('website');
echo $this->Form->submit('Register', array(
    'div' => 'form-actions',
    'class' => 'btn btn-primary'
));
echo $this->Form->end();

if ($this->Form->isFieldError('username')) {
    echo $this->Form->error('username');
}
?>