<tr>
    <td><button class="btn btn-primary" id="customButton">Primary</button></td>
</tr>


<script>
	
	$(document).ready(function(){
		$('#customButton').click(function(){

			$('body').append("<div class=\"modal-backdrop fade in\" style=\"z-index: 1040\"></div>"); 
			$('body').append("<div id=\"dialog\" ></div>");
			$.ajax({
			    type: "GET",
			    url: baseUrl + 'ajax/customerdialog',
			    data: {},
			    success: function (response) {
			        
			        $("#dialog").html(response);
			        $("#dialog").dialog({
			            minWidth: 1000,
			            maxWidth: 1000,
			            minHeight: 760,
			            maxHeight: 760,
			            modal: true,
			            resizable: false,
                        draggable: false,
                        
			        });
			        //$(".ui-dialog-titlebar").hide();
			    }
			});
		});
	});
</script>