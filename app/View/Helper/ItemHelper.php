<?php

/**
 *
 * ItemHelper Helper - render a item (news feed, comment, ...)
 * @package View.Helper
 * @created 2015-03-12
 * @version 1.0
 * @author thailvn
 * @copyright Oceanize INC
 */
class ItemHelper extends AppHelper {

    /** @var array $helpers Use helpers */
    public $helpers = array('Html');

    /**
     * Render a question item
     *
     * @author thailvn
     * @param array $item Item information
     * @return string Html
     */
    function smallNailItem($item = array()) {
        return $this->fetch('smallNailItem', array('item' => $item));
    }

    function mediumNailItem($item = array()) {
        return $this->fetch('mediumNailItem', array('item' => $item));
    }
    /**
     * Render a question item
     *
     * @author VuLTH
     * @param array $item Item information
     * @return string Html
     */
    function search($item = array()) {
        return $this->fetch('search', array('item' => $item));
    }

    function showErrors($errors, $key) {
        if (!empty($errors[$key])) {
            if (!is_array($errors[$key])) {
                $errors[$key][] = $errors[$key];
            }
            return $this->fetch('errorMessage', array('errors' => $errors[$key]));
        }
    }
}
