<div class="row">
    <div class="col-md-12">    
        <div class="box box-primary">   
            <div class="box-body">                
            <?php
                echo $this->SimpleForm->render($updateForm);                            
            ?>
            </div>
        </div>
     </div>
</div>
<input type="hidden" id='object-info' value="nailist_id">
<?php
	echo $this->Html->css('/plugins/imageresize/css/normalize.css');
    echo $this->Html->css('/plugins/imageresize/css/fastnail.cropimage.css');
    echo $this->Html->css('/plugins/imageresize/css/component.css');
?>
<div id="modal-resize" class="modal fade" role="dialog">
    <div class="modal-dialog modal-resize" style="z-index: 1060; width: 600px;">
        <div class="modal-content">
            <div class="modal-body">
                <div class="resize-image-content">
                    <div class="component">
                        <div class="overlay-background"></div>
                        <div class="overlay">
                            <div class="overlay-inner"></div>
                        </div>
                        <img class="resize-image" />
                        <button type="button" class="btn btn-primary btn-crop js-crop"><?php echo __("Crop")?></button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
	echo $this->Html->script('/plugins/imageresize/js/component.js');
?>