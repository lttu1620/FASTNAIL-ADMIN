<style>
@-moz-document url-prefix() { 
    .pull-right {
        //margin-top: -38px;
    } 
    #tb_order_daily_limit {
        margin-top: 30px;
    }
    .import-btn {
        margin-top: -38px;
    }
}
</style>
<div id="flashMessage" class="alert alert-success alert-dismissable Hidden"></div>
<div class="pull-right import-btn">
    <a href="<?php echo $controller . '/import'?>"><input class="btn btn-info" type="button" value="<?php echo __('Import'); ?>"/></a>
    <input class="btn btn-info" type="button" value="<?php echo __('Save'); ?>" onclick="saveTimelyLimit();return false;" />
</div>
<table id="tb_order_daily_limit" cellpadding="0" cellspacing="0" class="table table-bordered calendar-table timely-table">
    <tbody>
    <input type="hidden" id="date" value="<?php echo $date; ?>" />
    <?php foreach ($arrTimeGrid as $hour => $minites): ?>
        <tr class="calendar-row">
        <?php foreach ($minites as $minute):?>
        <?php if ( "{$hour}:{$minute}" >= $openTime && "{$hour}:{$minute}" <= $closeTime):?>
            <td class="calendar-day">
                <div class="timely_time">
                    <?php echo "{$hour}:{$minute}";?>
                </div>
                <div class="timely-limit"><label>FN <!--予約可能数--></label><input type="text" maxlength="2" class="timely_limit numberOnly" id="<?php echo "{$hour}_{$minute}";?>" name="<?php echo "{$hour}:{$minute}";?>" value="<?php echo isset($data["{$hour}:{$minute}"]) ? $data["{$hour}:{$minute}"]['limit'] : $defaultMaxSeat;  ?>" /></div>
                <div class="timely-limit"><label>HP <!--予約可能数--></label><input type="text" maxlength="2" class="timely_hp_limit numberOnly" id="hp_limit_<?php echo "{$hour}_{$minute}";?>" name="" value="<?php echo isset($data["{$hour}:{$minute}"]) ? $data["{$hour}:{$minute}"]['hp_limit'] : $defaultHpMaxSeat;  ?>" /></div>
            </td>
        <?php else: ?>
            <td class="calendar-day-np">
                <div class="timely_time">
                    <?php //echo "{$hour}:{$minute}";?>
                </div>
            </td>
        <?php endif; ?>
        <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
