<div id="flashMessage2" class="alert alert-warning alert-dismissable Hidden"><h4><i class="icon fa fa-warning"></i>ご確認ください</h4><span></span></div>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <form method="post" onsubmit="return checkFile();" enctype="multipart/form-data" >
                    <div class="form-group">
                        <label>Excelファイルを選んでください。</label>
                        <input id="file" type="file" name="import">
                    </div>
                    <div class="form-group button-group">
                        <div class="submit">
                            <input class="btn btn-primary pull-left" type="submit" value="<?php echo __('Import') ?>">
                        </div>
                        <div class="submit">
                            <input class="btn pull-left" type="submit" onclick="return back();" value="<?php echo __('Cancel') ?>">
                        </div>
                        <div class="cls"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
checkFile = function() {
    var filename = $('#file').val();
    var ext = (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;   
    if (! (ext && /^(xlsx|xls)$/.test(ext))){
        if ($('#flashMessage').length > 0) {
            $('#flashMessage').remove();
        }
        $('#flashMessage2 span').html('認識できないファイル形式です。');
        $('#flashMessage2').removeClass('Hidden');
        return false;
    }
    return true;
}
</script>