<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Capture: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'Capture %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $meta['title'] . '｜FASTNAIL'; ?></title>
    <meta name="description" content="<?php echo $meta['description']; ?>"/>
    <meta name="keywords" content="<?php echo $meta['keywords']; ?>"/>
    <script type="text/javascript">
        var baseUrl = "<?php echo $this->Html->url('/'); ?>";
        var controller = "<?php echo strtolower($controller); ?>";
        var action = "<?php echo strtolower($action); ?>";
        var referer = "<?php echo $referer; ?>";
        var url = "<?php echo $url; ?>";
        var imgBaseUrl = baseUrl + "<?php echo Configure::read('App.imageBaseUrl'); ?>";
        var timeoutNewOrder = "<?php echo Configure::read('API.TimeoutNewOrder');?>";
        var medicalIdList = null;
    </script>
    <?php
    echo $this->Html->meta('icon', 'img/favicon.ico');
    echo $this->fetch('meta');
    echo $this->Html->css('/bootstrap/css/bootstrap.min.css');
    echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
    echo $this->Html->css('//code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css');
    echo $this->Html->css('jquery-ui.css');
    echo $this->Html->css('/dist/css/AdminLTE.css');
    echo $this->Html->css('/dist/css/skins/_all-skins.min.css');
    echo $this->Html->css('/dist/css/skins/skin-fastnail.css');
    echo $this->Html->css('bootstrap-toggle.css');
    echo $this->Html->css('/plugins/iCheck/flat/blue.css');
    echo $this->Html->css('/plugins/morris/morris.css');
    echo $this->Html->css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css');
    echo $this->Html->css('/plugins/datepicker/datepicker3.css');
    echo $this->Html->css('/plugins/daterangepicker/daterangepicker-bs3.css');
    echo $this->Html->css('/plugins/timepicker/bootstrap-timepicker.css');
    echo $this->Html->css('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
    echo $this->Html->css('/plugins/lightbox/jquery.lightbox-0.5.css');
    echo $this->Html->css('/plugins/notification/css/jquery_notification.css');
    echo $this->Html->css('fastnail.css');
    foreach ($moreCss as $css) {
        echo $this->Html->css($css);
    }
    echo $this->Html->css('dialog.css');
    echo $this->Html->css('/css/calendar/bootstrap-2-bundle.css');
    echo $this->Html->css('/css/calendar/calendar.css');
    echo $this->Html->css('custom.css');
    echo $this->Html->script('/js/calendar/calendar.js');
    echo $this->fetch('css');
    ?>
    <script type="text/javascript">
        I18n.defaultLocale = 'ja';
        I18n.locale = 'ja';
    </script>
</head>
<body id="<?php echo strtolower("{$controller}_{$action}"); ?>"
    class="fixed skin-fastnail sidebar-collapse" data-app-mode='full'
    data-device='desktop'
    data-env='production'>
<div class="wrapper">
    <?php include("header.ctp"); ?>
    <?php include("menu.ctp"); ?>
    <div class="content-wrapper" style="background-color:#ffffff;">
        <?php echo $this->Session->flash(); ?>
        <div id='workspace'>
            <div id='reservations-index'>
                <?php echo $this->element('headercalendar', array('dt'=> $dt)); ?>
                <div class='container-fluid' id='reservations-content'>
                    <div data-date='<?php echo $dt?>' data-mode='default' id='res-timetable'>
                        <?php echo $this->fetch('content'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div id= "nailAdd" class="row hidden">
                <!-- <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-red custom-small-box">
                        <div class="inner custom_inner">
                            <h3 id="orderName"></h3>
                            <p>ネイルの選択が終わりました</p>
                            <p id="p_totalAddNail" ></p>
                        </div>
                        <div class="icon custom_icon">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                </div>
                -->
            </div>
        </div>
    </div>
</div>
<div style='display: none'>
    <img alt="" height="16" src="<?php echo $this->html->url('/img/loading_gray.gif'); ?>" width="16"/>
    <img alt="" height="16" src="<?php echo $this->html->url('/img/loading.gif'); ?>" width="16"/>
    <img alt="" height="48" src="<?php echo $this->html->url('/img/loading-1.gif'); ?>" width="48"/>
</div>
<?php
echo $this->Html->script('//code.jquery.com/ui/1.11.2/jquery-ui.min.js');
echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js');
echo $this->Html->script('/plugins/morris/morris.min.js');
echo $this->Html->script('/plugins/sparkline/jquery.sparkline.min.js');
echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
echo $this->Html->script('/plugins/knob/jquery.knob.js');
echo $this->Html->script('/plugins/daterangepicker/daterangepicker.js');
echo $this->Html->script('/plugins/datepicker/bootstrap-datepicker.js');
echo $this->Html->script('/plugins/timepicker/bootstrap-timepicker.js');
echo $this->Html->script('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
echo $this->Html->script('/plugins/iCheck/icheck.min.js');
echo $this->Html->script('/plugins/slimScroll/jquery.slimscroll.min.js');
echo $this->Html->script('/plugins/fastclick/fastclick.min.js');
echo $this->Html->script('/plugins/input-mask/jquery.inputmask.js');
echo $this->Html->script('/plugins/input-mask/jquery.inputmask.date.extensions.js');
echo $this->Html->script('/plugins/input-mask/jquery.inputmask.extensions.js');
echo $this->Html->script('/plugins/notification/js/jquery_notification_v.1.js');
echo $this->Html->script('/dist/js/app.min.js');
echo $this->Html->script('bootstrap-toggle.js');
echo $this->Html->script('/plugins/lightbox/jquery.lightbox-0.5.js');
echo $this->Html->script('autocomplete.js');
foreach ($moreScript as $script) {
    echo $this->Html->script($script);
}
echo $this->Html->script('jquery.cookie.js');
echo $this->Html->script('common.js');
echo $this->fetch('script');
?>
<script>
    // reload page when user clicked back button
    /*
    jQuery(document).ready(function ($) {
        if (window.history && window.history.pushState) {
            $(window).on('popstate', function () {
                var hashLocation = location.hash;
                var hashSplit = hashLocation.split("#!/");
                var hashName = hashSplit[1];
                if (hashName !== '') {
                    var hash = window.location.hash;
                    if (hash === '') {
                        //alert('Back button was pressed.');
                        //location.reload();
                    }
                }
            });
        }
    });
    */
    $(function () {
		//reload calendar automatically
    	setInterval(
    	     function () {
    	    	 	var disable_seat_cookie = getCookie('DisableSeatCookie');
 					var enable_seat_cookie = getCookie('EnableSeatCookie');
 					var is_edit_seat = 0;
        	        var dt = $("#timedial").val();
    	    		var d = new Date();
		   	        var action_query = '';
		   	        if(disable_seat_cookie =='on' || enable_seat_cookie =='on') is_edit_seat = 1;
			 		if((controller=='orders' && action == "seat") ||  (controller=='pages' && action == "index")){
				 		if((controller=='orders' && action == "seat")){
				 			action_query = "?is_seat=1"
					 	}
			 		}else{
				 		return false;
				 	}
			 		min =  d.getMinutes();
		   	        second =  d.getSeconds();
		   	        if((min == 0 || min == 15 || min == 30 || min == 45) && second == 0){
		   	        	$.ajax({
	    	                type: "POST",
	    	                url: baseUrl + 'ajax/loadcalendar'+ action_query,
	    	                data: {date : dt, controller:controller, action:action, is_edit_seat: is_edit_seat},
	    	                success: function (response) {
	    	                   $("#workspace").replaceWith(response);
	    	                }
	    	            });
			   	   }
        	    },
    	        1000
    	    );

        function myDateFormatter(dateObject) {
            var d = new Date(dateObject);
            var day = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = year + "-" + month + "-" + day;

            return date;
        };
        /*******************set 本日 button for each screen *****************************/
        var d = new Date();
        var strDate = myDateFormatter(d);
        var date1 = new Date(strDate).getTime();
        var t = "<?php echo isset($dt) ? $dt : date('Y-m-d'); ?>";
        var date2 = new Date(t).getTime();
        var url = baseUrl;
        //var pathName = window.location.pathname.split("/");
        //var controllerName = pathName[pathName.length - 2];
        //var actionName = pathName[pathName.length - 1];
        //console.log(controller+'/'+action);
        if (controller == 'orders' && action == 'lists') {
            url += "orders/lists";
        } else if (controller == 'orders' && action == 'calendar') {
            url += "orders/calendar";
        } else if (controller == 'ordertimelylimits' && action == 'index') {
            url += "ordertimelylimits";
        } else if (controller == 'orders' && action == 'seat') {
            url += "orders/seat";
        }else if (controller == 'pages' && action == 'index') {
            url += "orders/staff";
        }

        /*
        if (date1 > date2) {
            $('.label-info').replaceWith("<b class='label label-important'>過去</b><b id='todayId'><a href='" + url + "' data-remote='true'>本日</a></b>");
            $("b").remove(".invisible");
        }
        else if (date1 < date2) {
            $('.label-info').replaceWith("<b id='todayId'><a href='" + url + "' data-remote='true'>本日</a></b>");
        } else {
            $('#todayId').replaceWith("<b class='label label-info' style='margin-left: -17px;'>本日</b>");
        }
        */
        /*******************set 本日 button for each screen ******************************/
        $('#timedial-cal-btn').click(function () {
            $('.mobidate').mobiscroll('show');
            return false;
        });
        $("body").on("click", "#btn-tab li a", function () {
            $(this).addClass("active");
            //location.reload();
            window.location.href = $(this).attr('href');
            $(this).addClass("active");
        });
        $("body").on("click", "#todayId", function () {
            window.location.href = url;
        });
        $("body").on("click", "#refresh", function () {
            location.reload();
        });
    });

    /*******************new  booking btn *****************************/
    $("body").on("click", "#new-reservation", function () {
        console.log('run functiion here');
        var curDate = $('#timedial').val();
        //curDate += ' 11:00:00';
        curDate = new Date(curDate).getTime();
        var stylist_ids = [''];
        var action_query = '';
		if(controller=='orders' && action == "seat"){
			action_query = "?is_seat=1"
		}
        var url = baseUrl + 'ajax/bookingdialog'+action_query;
        var data = {
            reservation: {
                start_at_epoch: curDate / 1000,
                stylist_ids: stylist_ids,
                duration: -1,
            }
        };
        var method = 'POST';
        showBookingDialog(url, data, method);
        return false;
    });
    /*******************new  booking btn *****************************/
    $.widget.bridge('uibutton', $.ui.button);
</script>
<?php echo $this->element('ga'); ?>
</body>
</html>