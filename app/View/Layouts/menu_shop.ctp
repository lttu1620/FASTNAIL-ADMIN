<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="overflow: hidden; width: auto; height: 845px;">           
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <!-- Booking Calender -->
            <li class="">
                <a href="<?php echo($this->Html->url('/')); ?>">
                    <i class="fa fa-calendar"></i> <span><?php echo __('Booking Calender')?></span>
                </a>
            </li>
            <?php if (!$AppUI->shop_is_franchise) : ?>
                <!-- User Manager -->
                <li<?php if ($controller=='users') echo " class=\"active\""?>>
                    <a href="<?php echo($this->Html->url('/users'))?>"><i class="fa fa-user"></i> <span><?php echo __('Registedusers').__('Management')
                    ?></span></a>
                </li>
            <?php endif; ?>
            <!-- Order Manager -->
            <li<?php if ($controller=='orders') echo " class=\"active\""?>>
              <a href="<?php echo($this->Html->url('/orders'))?>"><i class="fa fa-book"></i> <span><?php echo __('Orders').__('Management')?></span></a>
             </li>
            <?php if (!$AppUI->shop_is_franchise) : ?>
                <!-- Master -->
                <li class="treeview <?php if (in_array($controller, array(
                           'areas','prefectures', 'shopgroups', 'shops', 'nailists', 'bullions','colors','nails','devices','colorjells','ramejells',  'designs','flowers',  'genres', 'holograms', 'materials', 'paints','powders',  'scenes', 'shells','stones','keywords','tags','items','admins' )) && !($controller == 'admins' && $action == 'password') ) echo "active"?>">
                    <a href="#">
                        <i class="fa fa-gears"></i> <span><?php echo __('Master')?></span><i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="treeview <?php if (in_array($controller, array('areas','prefectures', 'shopgroups', 'devices', 'shops', 'nailists'))) echo "active"?>">
                            <a href="#">
                                <i class="fa fa-building-o"></i> <span><?php echo __('Tenant informations')?></span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li<?php if ($controller=='nailists') echo " class=\"active\""?>>
                                    <a href="<?php echo($this->Html->url('/nailists'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Nailists').__('Management')?></span></a></li>
                                <li<?php if ($controller=='devices') echo " class=\"active\""?>>
                                    <a href="<?php echo($this->Html->url('/devices'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Devices').__('Management')?></span></a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
            <?php endif; ?>
        </ul>
        <div style="margin-bottom:100px;"></div>
    </section>
</aside>