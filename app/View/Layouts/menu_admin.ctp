<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="overflow: hidden; width: auto; height: 845px;">           
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <!-- Booking Calender -->
            <li class="">
                <a href="<?php echo($this->Html->url('/')); ?>">
                    <i class="fa fa-calendar"></i> <span><?php echo __('Booking Calender')?></span>
                </a>
            </li>
            <!-- User Manager -->
            <li<?php if ($controller=='users') echo " class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/users'))?>"><i class="fa fa-user"></i> <span><?php echo __('Registedusers').__('Management')
                ?></span></a>
            </li>
            <!-- Order Manager -->
            <li<?php if ($controller=='orders' && $action != 'export') echo " class=\"active\""?>>
              <a href="<?php echo($this->Html->url('/orders'))?>"><i class="fa fa-book"></i> <span><?php echo __('Orders').__('Management')?></span></a>
            </li>
            <li <?php if ($controller=='contacts') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/contacts'))?>">
                    <i class="fa fa-envelope-o"></i> <span><?php echo __('Contact Manager')?></span>
                </a>
            </li>
            <!-- Export Order -->
            <li class="treeview <?php if ($controller == 'orders' && $action == 'export') echo "active" ?>">
                <a href="#">
                    <i class="fa fa-download"></i> <span><?php echo __('Data export') ?></span><i
                        class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller == 'orders' && $action == 'export') echo " class=\"active\"" ?>>
                        <a href="<?php echo($this->Html->url('/orders/export')) ?>"><i
                                class="fa fa-file-excel-o"></i> <?php echo __('Order export') ?></a>
                    </li>
                </ul>
            </li>
            <!-- Master -->
            <li class="treeview <?php if (in_array($controller, array(
                       'areas','prefectures', 'shopgroups', 'shops', 'nailists', 'bullions','colors','nails','devices','colorjells','ramejells',  'designs','flowers',  'genres', 'holograms', 'materials', 'paints','powders',  'scenes', 'shells','stones','keywords','tags','items','admins','beauties','userbeautyviewlogs','campaigns','pointitems','userboughtitems')) && !($controller == 'admins' && $action == 'password') ) echo "active"?>">
                <a href="#">
                    <i class="fa fa-gears"></i> <span><?php echo __('Master')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview <?php if (in_array($controller, array('areas','prefectures', 'shopgroups', 'devices', 'shops', 'nailists'))) echo "active"?>">
                        <a href="#">
                            <i class="fa fa-building-o"></i> <span><?php echo __('Tenant informations')?></span><i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li<?php if ($controller=='areas') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/areas'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Areas').__('Management')?></a></li>
                            <li<?php if ($controller=='prefectures') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/prefectures'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Prefectures').__('Management')?></a></li>
                            <li<?php if ($controller=='shopgroups') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/shopgroups'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Shopgroups').__('Management')?></span></a></li>
                            <li<?php if ($controller=='shops') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/shops'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Shops').__('Management')?></span></a></li>
                            <li<?php if ($controller=='nailists') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/nailists'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Nailists').__('Management')?></span></a></li>
                            <li<?php if ($controller=='devices') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/devices'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Devices').__('Management')?></span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview <?php if (in_array($controller, array('nails','bullions','colors','colorjells','ramejells','designs','flowers','genres','holograms','materials','paints','powders','scenes','shells','stones','keywords','tags'))  )  echo "active"?>">
                        <a href="#">
                            <i class="fa fa-suitcase"></i> <span><?php echo __('Nail Productions').__('Management')?></span><i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li<?php if ($controller == 'nails') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/nails'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Nails').__('Management')?></span></a></li>
                            <li<?php if ($controller=='genres') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/genres'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Genres').__('Management')?></span></a></li>
                            <li<?php if ($controller=='scenes') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/scenes'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Scenes').__('Management')?></a></li>
                            <li<?php if ($controller=='keywords') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/keywords'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Keywords').__('Management')?></span></a></li>
                            <li<?php if ($controller=='tags') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/tags'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Tags').__('Management')?></span></a></li>
                            <li<?php if ($controller=='bullions') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/bullions'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Bullions').__('Management')?></a></li>
                            <li<?php if ($controller=='colors') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/colors'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Colors').__('Management')?></a></li>
                            <li<?php if ($controller=='colorjells') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/colorjells'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Colorjells').__('Management')?></a></li>
                            <li<?php if ($controller=='ramejells') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/ramejells'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Ramejells').__('Management')?></a></li>
                            <li<?php if ($controller=='designs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/designs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Designs').__('Management')?></a></li>
                            <li<?php if ($controller=='flowers') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/flowers'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Flowers').__('Management')?></a></li>
                            <li<?php if ($controller=='holograms') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/holograms'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Holograms').__('Management')?></span></a></li>
                            <li<?php if ($controller=='paints') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/paints'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Paints').__('Management')?></span></a></li>
                            <li<?php if ($controller=='powders') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/powders'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Powders').__('Management')?></span></a></li>
                            <li<?php if ($controller=='shells') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/shells'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Shells').__('Management')?></span></a></li>
                            <li<?php if ($controller=='stones') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/stones'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Stones').__('Management')?></span></a></li>
                            <li<?php if ($controller=='materials') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/materials'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Materials').__('Management')?></span></a></li>
                            <li<?php if ($controller=='services') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/services'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Service list')?></span></a></li>
                        </ul>
                    </li>
                    <li<?php if ($controller == 'items') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/items'))?>"><i class="fa fa-coffee"></i> <span><?php echo __('Items').__('Management')?></span></a>
                    </li>
                    <li class="treeview <?php if (in_array($controller, array('beauties','campaigns','pointitems','userbeautyviewlogs','userboughtitems'))) echo "active"?>">
                        <a href="#">
                            <i class="fa fa-mobile" style="font-size: 18px"></i><span>アプリ関連</span><i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li<?php if ($controller=='beauties') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/beauties'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Beauty list')?></span></a>
                            </li>
                            <li<?php if ($controller=='campaigns') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/campaigns'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Campaign list')?></span></a>
                            </li>
                            <li<?php if ($controller=='pointitems') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/pointitems'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Item point List')?></span></a>
                            </li>
                            <li<?php if ($controller=='userbeautyviewlogs') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/userbeautyviewlogs'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('User beauty view log List')?></span></a>
                            </li>
                            <li<?php if ($controller=='userboughtitems') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/userboughtitems'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('User bought item List')?></span></a>
                            </li>
                        </ul>
                    </li>
                    <li<?php if ($controller == 'admins' && $action != 'password') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/admins'))?>"><i class="fa fa-wrench"></i> <span><?php echo __('Admins').__('Management')?></span></a>
                    </li>
                </ul>
            </li>
           <li class="treeview <?php if (in_array($controller,array(
                   'system',
                   'surveylogs',
                   'userpointlogs',
                   'recommendreactionlogs',
                   'userselectednaillogs',
                   'mailsendlogs',
                   'orderupdatetimebarlogs',
                   'runbatch',
                   'deletecache',
                   'ps',
               )
               ) || ($controller == 'admins' && $action == 'password')
           ) echo "active"?>">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span><?php echo __('System') ?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview <?php if (in_array($controller, array(
                                                    'userpointlogs',
                                                    'recommendreactionlogs',
                                                    'userselectednaillogs',
                                                    'surveylogs',
                                                    'mailsendlogs',
                                                    'orderupdatetimebarlogs'
                                                )
                                            )
                                        ) echo "active"?>">
                        <a href="#"><i class="fa fa-files-o"></i> <span><?php echo __('Logs')?></span><i class="fa fa-angle-left pull-right"></i>                </a>
                        <ul class="treeview-menu">
                            <li<?php if ($controller=='surveylogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/surveylogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Survey log list')?></a></li>
                            <li<?php if ($controller=='mailsendlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/mailsendlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Mail Sent Logs')?></a></li>
                            <li<?php if ($controller=='recommendreactionlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/recommendreactionlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Recommend reaction logs')?></a></li>
                            <li<?php if ($controller=='userselectednaillogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/userselectednaillogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('User selected nail logs')?></a></li>
                            <li<?php if ($controller=='userpointlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/userpointlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('User Point Logs')?></a></li>
                            <li<?php if ($controller=='orderupdatetimebarlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/orderupdatetimebarlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Order update timebar log List')?></a></li>
                        </ul>
                    </li>
                    <li class="treeview <?php if (  in_array($controller, array(
                        'runbatch','settings',
                        'deletecache','ps'))
                                ) echo "active"?>">
                        <a href="#"><i class="fa  fa-bolt"></i> <span><?php echo __('System Actions')?></span><i class="fa fa-angle-left pull-right"></i>                </a>
                        <ul class="treeview-menu">
                            <li<?php if ($action=='runbatch') echo " class=\"active\""?>><a onclick="return runbatch();" href="#"><i class="fa fa-angle-double-right"></i> <?php echo __('Start batch')?></a></li>
                            <li<?php if ($action=='deletecache') echo " class=\"active\""?>><a href="<?php echo $this->Html->url('/system/deletecache')?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Delete cache')?></a></li>
                            <li<?php if ($action=='ps') echo " class=\"active\""?>><a href="<?php echo $this->Html->url('/system/ps')?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Process')?></a></li>
                        </ul>

                    <!-- Settings -->
                    <li class="treeview <?php if ($controller=='settings' || ($controller == 'admins' && $action == 'password')) echo "active"?>">
                        <a href="#">
                            <i class="fa fa-gear"></i> <span><?php echo __('Settings')?></span><i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'global') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/settings?type=global'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Global settings')?></a>
                            </li>
                            <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'user') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/settings?type=user'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Default user settings')?></a>
                            </li>
                            <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'admin') echo " class=\"active\""?>>
                                <a href="<?php echo($this->Html->url('/settings?type=admin'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Default admin settings')?></a>
                            </li>
                            <li<?php if ($controller == 'admins' && $action == 'password') echo " class=\"active\""?>>
                                <a href="<?php echo $this->Html->Url("/admins/password") ?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Change password'); ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
           </li>
        </ul>
        <div style="margin-bottom:100px;"></div>
    </section>
</aside>
