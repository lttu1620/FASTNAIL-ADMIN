<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'FASTNAIL: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'FASTNAIL %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>	
    <title><?php echo $meta['title'] . '｜FASTNAIL';?></title>
    <meta name="description" content="<?php echo $meta['description']; ?>" />          
    <meta name="keywords" content="<?php echo $meta['keywords']; ?>" /> 
	<?php
		echo $this->Html->meta('icon','img/favicon.ico');                
        
        echo $this->Html->css('/bootstrap/css/bootstrap.min.css');
        echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
        echo $this->Html->css('//code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css');
        echo $this->Html->css('jquery-ui.css');
        echo $this->Html->css('/dist/css/AdminLTE.css');
        echo $this->Html->css('bootstrap-toggle.css');
        echo $this->Html->css('/dist/css/skins/_all-skins.min.css');        
        echo $this->Html->css('/dist/css/skins/skin-fastnail.css');
        echo $this->Html->css('/plugins/iCheck/flat/blue.css');
        echo $this->Html->css('/plugins/morris/morris.css');
        echo $this->Html->css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css');
        echo $this->Html->css('/plugins/datepicker/datepicker3.css');
        echo $this->Html->css('/plugins/daterangepicker/daterangepicker-bs3.css');
        echo $this->Html->css('/plugins/timepicker/bootstrap-timepicker.css');
        echo $this->Html->css('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
        echo $this->Html->css('/plugins/lightbox/jquery.lightbox-0.5.css');
        echo $this->Html->css('/plugins/notification/css/jquery_notification.css');
        echo $this->Html->css('fastnail.css');
        foreach ($moreCss as $css) {
            echo $this->Html->css($css);
        }
        echo $this->Html->css('custom.css'); 
        echo $this->Html->script('/plugins/jQuery/jQuery-2.1.3.min.js');
        echo $this->Html->script('/bootstrap/js/bootstrap.min.js');
	?>
    <script type="text/javascript">
        var baseUrl = "<?php echo $this->Html->url('/'); ?>";
        var controller = "<?php echo $controller; ?>";
        var action = "<?php echo $action; ?>";
        var referer = "<?php echo $referer; ?>";        
        var url = "<?php echo $url; ?>";
        var imgBaseUrl = baseUrl+ "<?php echo Configure::read('App.imageBaseUrl'); ?>";
        var timeoutNewOrder = <?php echo Configure::read('API.TimeoutNewOrder');?>
    </script>
</head>
<body id="<?php echo "{$controller}_{$action}"; ?>" class="fixed skin-fastnail sidebar-collapse" data-app-mode='full' data-device='desktop' data-env='production'>  
    <div class="wrapper">
        <?php include("header.ctp"); ?>  
        <?php include("menu.ctp"); ?>        
         <div class="content-wrapper" > 
            <section class="content-header"> 
                <?php if (!empty($breadcrumb)) : ?>
                <?php echo $this->Breadcrumb->render($breadcrumb, $breadcrumbTitle); ?>  
                <?php endif ?>
            </section>       
            <section class="content" id="<?php echo $controller . '_' . $action; ?>">                
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>                
            </section>
         </div>
    </div> 
    <?php
        echo $this->Html->script('//code.jquery.com/ui/1.11.2/jquery-ui.min.js');
		echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js');
        echo $this->Html->script('/plugins/morris/morris.min.js');
		echo $this->Html->script('/plugins/sparkline/jquery.sparkline.min.js');
		echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
		echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
		echo $this->Html->script('/plugins/knob/jquery.knob.js'); 
        echo $this->Html->script('/plugins/daterangepicker/daterangepicker.js'); 
        echo $this->Html->script('/plugins/datepicker/bootstrap-datepicker.js'); 
        echo $this->Html->script('/plugins/timepicker/bootstrap-timepicker.js');
        echo $this->Html->script('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); 
        echo $this->Html->script('/plugins/iCheck/icheck.min.js'); 
        echo $this->Html->script('/plugins/slimScroll/jquery.slimscroll.min.js'); 
        echo $this->Html->script('/plugins/fastclick/fastclick.min.js'); 
        echo $this->Html->script('/dist/js/app.min.js'); 
        echo $this->Html->script('bootstrap-toggle.js');        
        echo $this->Html->script('/plugins/lightbox/jquery.lightbox-0.5.js');
        echo $this->Html->script('/plugins/notification/js/jquery_notification_v.1.js');
        echo $this->Html->script('autocomplete.js');  	        
        foreach ($moreScript as $script) {
            echo $this->Html->script($script);
        }
        echo $this->Html->script('common.js');        	
        echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
        echo $this->element('ga');
	?>    
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
</body>
</html>