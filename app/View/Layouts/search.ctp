
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php echo $meta['title'] . '｜FASTNAIL'; ?></title>
        <meta name="description" content="<?php echo $meta['description']; ?>" />
        <meta name="keywords" content="<?php echo $meta['keywords']; ?>" />
        <?php
            echo $this->Html->meta('icon','img/favicon.ico');
            echo $this->Html->css('/bootstrap/css/bootstrap.min.css');
            echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
            echo $this->Html->css('//code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css');
            echo $this->Html->css('jquery-ui.css');
            echo $this->Html->css('/dist/css/AdminLTE.css');
            echo $this->Html->css('bootstrap-toggle.css');
            echo $this->Html->css('/dist/css/skins/_all-skins.min.css');
            echo $this->Html->css('/dist/css/skins/skin-fastnail.css');
            echo $this->Html->css('/plugins/iCheck/flat/blue.css');
            echo $this->Html->css('/plugins/morris/morris.css');
            echo $this->Html->css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css');
            echo $this->Html->css('/plugins/datepicker/datepicker3.css');
            echo $this->Html->css('/plugins/daterangepicker/daterangepicker-bs3.css');
            echo $this->Html->css('/plugins/timepicker/bootstrap-timepicker.css');
            echo $this->Html->css('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
            echo $this->Html->css('/plugins/lightbox/jquery.lightbox-0.5.css');
            echo $this->Html->css('fastnail.css');
            echo $this->Html->css('table-solution');
            echo $this->Html->css('table-solution-assets');

            foreach ($moreCss as $css) {
                echo $this->Html->css($css);
            }
            echo $this->Html->css('custom.css');
            echo $this->Html->script('/plugins/jQuery/jQuery-2.1.3.min.js');
            echo $this->Html->script('/bootstrap/js/bootstrap.min.js');
        ?>
        <script type="text/javascript">
            var baseUrl = "<?php echo $this->html->url('/'); ?>";
            var timeoutNewOrder = "<?php echo Configure::read('API.TimeoutNewOrder');?>";
        </script>

    </head>
    <body id="<?php echo "{$controller}_{$action}"; ?>" class="fixed skin-fastnail sidebar-collapse" data-app-mode='full' data-device='desktop' data-env='production'>
        <div class="wrapper">
            <div class='modal modal-large fade' data-keyboard='false' id='ajax-modal' tabindex='-1'></div>
            <div id='messages'>
            </div>

            <?php include("header.ctp"); ?>
            <?php include("menu.ctp"); ?>
            <?php echo $this->fetch('content'); ?>
        </div>
        <?php
            echo $this->Html->script('//code.jquery.com/ui/1.11.2/jquery-ui.min.js');
        ?>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <?php
            echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js');
            echo $this->Html->script('/plugins/morris/morris.min.js');
            echo $this->Html->script('/plugins/sparkline/jquery.sparkline.min.js');
            echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
            echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
            echo $this->Html->script('/plugins/knob/jquery.knob.js');
            echo $this->Html->script('/plugins/daterangepicker/daterangepicker.js');
            echo $this->Html->script('/plugins/datepicker/bootstrap-datepicker.js');
            echo $this->Html->script('/plugins/timepicker/bootstrap-timepicker.js');
            echo $this->Html->script('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
            echo $this->Html->script('/plugins/iCheck/icheck.min.js');
            echo $this->Html->script('/plugins/slimScroll/jquery.slimscroll.min.js');
            echo $this->Html->script('/plugins/fastclick/fastclick.min.js');
            echo $this->Html->script('/dist/js/app.min.js');
            echo $this->Html->script('bootstrap-toggle.js');
            //echo $this->Html->script('/dist/js/demo.js');
            echo $this->Html->script('/plugins/lightbox/jquery.lightbox-0.5.js');
            echo $this->Html->script('autocomplete.js');
            foreach ($moreScript as $script) {
                echo $this->Html->script($script);
            }
            echo $this->Html->script('jquery.cookie.js');
            echo $this->Html->script('common.js');

            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');
        ?>
    </body>
</html>
