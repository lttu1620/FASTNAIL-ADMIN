<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja">
    <head>
        <title><?php echo $meta['title'];?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width; initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" type="text/css" href="css/reset.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>

        <?php echo $this->fetch('content'); ?>                     

    </body>
</html>