<header class="main-header">
    <a href="<?php echo($this->Html->url('/')); ?>" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        <?php echo $this->Html->image('logo-ascii.png'); ?>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu">
					<a href="<?php if ($this->layout == 'full'):?> #res-timetable<?php else:?><?php echo($this->Html->url('/orders/seat')); ?>#res-timetable<?php endif?>" class="dropdown-toggle" aria-expanded="false">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-success" id="alert-no-nailist" style="margin-top: -6px;"></span> 
					</a>
				</li>
                <?php if (!empty($AppUI->id)) : ?>
                    <?php if (!empty($listShops) && !empty($AppUI->shop_id)): ?>
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-building"></i><span> <?php echo $AppUI->shop_name; ?> <i class="caret"></i></span>
                            </a>                        
                            <ul class="dropdown-menu">
                                <!-- <li class="header">You have 10 notifications</li> -->
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <?php foreach ($listShops as $shop): ?>
                                            <li>
                                                <a href="<?php echo ($this->Html->url("/session/{$shop['id']}")); ?>">
                                                    <i class="fa fa-users text-aqua"></i><?php echo $shop['name'] ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>									
                                    </ul>
                                </li>
                            </ul>                       
                        </li>	
                    <?php endif; ?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i><span> <?php echo $AppUI->display_name; ?> <i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->                        
                            <li class="user-header bg-light-blue">
                                <?php if (!empty($AppUI->display_image)) : ?>
                                    <?php echo $this->Html->image($this->Common->thumb($AppUI->display_image), array('class' => 'img-circle')) ?>                                
                                <?php endif ?>
                                <p>
                                    <?php echo $AppUI->display_name ?>						
                                    <small><?php echo __('Member since') . date('Y-m', $AppUI->created) ?></small>
                                </p>
                            </li>                       
                            <!-- Menu Body -->
                            <li class="user-body">

                            </li>
                            <!-- Menu Footer-->                        
                            <li class="user-footer">    

                                <div class="pull-right">
                                    <a href="<?php echo $this->Html->Url("/pages/logout") ?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i>&nbsp;<?php echo __('Sign out'); ?></a>
                                </div>                            
                            </li>
                        </ul>
                    </li>

                <?php endif; ?>
            </ul>
        </div>
        <?php if (!empty($AppUI->shop_id)): ?>
            <div class="navbar-custom-menu" style="float: left;">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->

                    <!-- Notifications: style can be found in dropdown.less -->

                    <!-- Tasks: style can be found in dropdown.less -->

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="">
                        <a href="<?php echo($this->Html->url('/orders/seat')); ?>">
                            <i class="fa fa-calendar"></i><span> 空き枠チャート</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo($this->Html->url('/orders/staff')); ?>">
                            <i class="fa fa-calendar"></i><span> 指名チャート</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo($this->Html->url('/orders/lists')); ?>">
                            <i class="fa fa-list-alt"></i> <span>台帳</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo($this->Html->url('/shops/customer')) ?>"><i class="fa fa-user"></i> <span><?php echo __('User Manager') ?></span></a>
                    </li>
                    <?php if (!$AppUI->shop_is_franchise) : ?>
                    <li class="">
                        <a href="<?php echo($this->Html->url('/holidays')) ?>"><i class="fa fa-heart"></i> <span><?php echo __('Holiday') ?></span></a>
                    </li>
                    <?php endif ?>
                </ul>
            </div>		
        <?php endif; ?>
    </nav>
</header> 