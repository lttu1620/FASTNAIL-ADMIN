<!--   
<div class="row"> 
     <section class="col-lg-12  ui-sortable">        
        <div class="nav-tabs-custom" style="cursor: move;">
            <?php echo $profileTab; ?>  
        </div>
    </section>  
    <div class="col-xs-12"> 
        
        <div class="box">
        <?php        
            echo $this->SimpleTable->render($table);
        ?>
        </div>
    </div>
</div>
-->
<div class="mailbox row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-4">                        
                        <?php echo $profileTab; ?>                          
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <div class="box">
                        <?php        
                            echo $this->SimpleTable->render($table);
                        ?>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>