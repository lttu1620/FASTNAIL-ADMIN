<div class="mailbox row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-4">                        
                        <?php echo $profileTab; ?>                          
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <div class="box">
                        <?php        
                            echo $this->SimpleTable->render($table);
                            echo $this->Paginate->render($total, $limit);
                        ?>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>