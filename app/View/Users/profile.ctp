<div class="row">
    <div class="col-md-6">    
        <div class="box box-primary">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('User profile') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div> 
            <div class="box-body">                
                <?php
                    echo $this->SimpleForm->render($userForm);
                ?>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($userFacebookForm)) : ?>
<div class="row">
    <div class="col-md-6">    
        <div class="box box-primary collapsed-box">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('Facebook profile') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-plus"></i></button>                
                </div>
            </div>
            <div class="box-body" style="display:none">                
                <?php
                    echo $this->SimpleForm->render($userFacebookForm);
                ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (!empty($userRecruiterForm)) : ?>
<div class="row">
    <div class="col-md-6">    
        <div class="box box-primary collapsed-box"> 
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('Recruiter profile') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-plus"></i></button>                
                </div>
            </div>
            <div class="box-body" style="display:none">                
                <?php
                    echo $this->SimpleForm->render($userRecruiterForm);
                ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (!empty($userCompanyForm)) : ?>
<div class="row">
    <div class="col-md-6">    
        <div class="box box-primary collapsed-box">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('Company profile') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-plus"></i></button>                
                </div>
            </div>
            <div class="box-body" style="display:none">                
                <?php
                    echo $this->SimpleForm->render($userCompanyForm);
                ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>