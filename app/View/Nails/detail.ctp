<div class="row">
	    <div class="col-md-8">    
        <div class="box box-primary">
			<div class="box-header with-border">
              <i class="fa fa-info"></i>
              <h3 class="box-title">ネイル情報</h3>
            </div>   
            <div class="box-body">                
				<dl class="dl-horizontal">
                    <dt><?php echo __('ID')?></dt>
                    <dd><?php echo $Nail["id"];?></dd>
                    <dt><?php echo __('Show section')?></dt>
                    <dd><?php echo $Nail["show_section"];?></dd>
                    <dt><?php echo __('Hf section')?></dt>
                    <dd><?php echo Configure::read('Config.hfSection')[$Nail["hf_section"]];?></dd>
                    <dt><?php echo __('Menu section')?></dt>
                    <dd><?php echo $Nail["menu_section"];?></dd>
                    <dt><?php echo __('Price')?></dt>
                    <dd><?php echo sprintf('¥%s', $Nail["price"]);?></dd>
                    <dt><?php echo __('Tax price')?></dt>
                    <dd><?php echo sprintf('¥%s', $Nail["tax_price"]);?></dd>
                    <dt><?php echo __('Time')?></dt>
                    <dd><?php echo $Nail["time"];?></dd>
                    <dt><?php echo __('Print')?></dt>
                    <dd><?php echo $Nail["print"];?></dd>
                    <dt><?php echo __('Limited')?></dt>
                    <dd><?php echo $Nail["limited"];?></dd>   
                    <dt><?php echo __('Rank')?></dt>
                    <dd><?php echo $Nail["rank"];?></dd>
                    <dt><?php echo __('HP coupon')?></dt>
                    <dd><?php echo $Nail["hp_coupon"];?></dd>   
                    <dt><?php echo __('Coupon memo')?></dt>
                    <dd><?php echo $Nail["coupon_memo"];?></dd>  
     
                  
<?php
            $arrAtt = array(
                'bullions'           
                ,'colors'             
                ,'color_jells'       
                ,'designs'          
                ,'flowers'          
                ,'genres'           
                ,'holograms'      
                ,'keywords'       
                ,'paints'          
                ,'powders'       
                ,'rame_jells'      
                ,'scenes'           
                ,'shells'           
                ,'stones'         
                ,'tags'                
                ,'keywords') ;
                foreach  ($arrAtt as $value) {
                ?>  
                    <dt><?php echo __(ucfirst($value))?></dt>
                    <dd><?php
                            $strvalue =''; 
                            foreach  ($listAttributes[$value] as $value1) {
                                if (!empty($value1['checked'])) {                                    
                                    $strvalue .= $value1['name'] . '<br/>'; 
                                }
                            }                       
                            echo !empty($strvalue) ? $strvalue : '-';
                        ?></dd>  
                    <?php } ?>
                </dl>
            </div>
        </div>
    </div>   
    <div class="col-md-4">    
        <div class="box box-danger">   
            <div class="box-body">
				<?php echo $this->Html->image($Nail ["image_url"]); ?>
            </div>
        </div>
    </div>
    <?php if (!empty($Nail['id'])) : ?>
        <div class="col-md-8">    
            <div class="submit">
                <a class="btn btn-primary" href="<?php echo($this->Html->url('/nails/update/' . $Nail['id'])) ?>"><?php echo __('Update nail') ?></a>
            </div>
        </div>
    <?php endif; ?>
</div>