<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
            <?php
                echo $this->SimpleForm->render($updateForm);
            ?>
            </div>
        </div>
     </div>
</div>

<script>
    $("#shop_id").change(function () {
        var id = $('#id').val();
        var shop_id = $(this).val();
        var data = {
            cart_id: id,
            shop_id: shop_id
        };
        $.ajax({
            type: "POST",
            url: baseUrl + 'ajax/shownailistsofshop',
            data: data,
            success: function (response) {
                if (response) {
                    $('#nailist_id').html(response);
                } else {
                    $('#nailist_id').html('');
                }
            }
        });
        return false;
    });
</script>