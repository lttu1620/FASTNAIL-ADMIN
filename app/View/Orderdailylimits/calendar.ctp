<table class="calendar_control" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="40%" align="left">
            <a href="?date=<?php echo $info['firstDayOfLastMonth']; ?>">
                « <?php echo $info['lastMonth']; ?>月<?php echo $info['yearOfLastMonth']; ?>
            </a>
        </td>
        <td width="20%" align="center">
            <strong>
                <?php echo $info['thisMonth']; ?>月<?php echo $info['thisYear']; ?>
            </strong>
        </td>
        <td width="40%" align="right">
            <a href="?date=<?php echo $info['firstDayOfNextMonth']; ?>">
                <?php echo $info['nextMonth']; ?>月<?php echo $info['yearOfNextMonth']; ?> »
            </a>
        </td>
    </tr>
</table>
<table id="tb_order_daily_limit" cellpadding="0" cellspacing="0" class="table table-bordered calendar-table">
    <tbody>
    <tr class="calendar-row">
        <th class="calendar-day-head">日</th>
        <th class="calendar-day-head">月</th>
        <th class="calendar-day-head">火</th>
        <th class="calendar-day-head">水</th>
        <th class="calendar-day-head">木</th>
        <th class="calendar-day-head">金</th>
        <th class="calendar-day-head">土</th>
    </tr>
    <?php foreach ($data as $key => $item) {
        if ($key % 7 == 0) { ?>
            <tr class="calendar-row">
        <?php } ?>

        <td class=" <?php
            if ($item['dateInfo']['isThisMonth']) echo 'calendar-day';
                else echo 'calendar-day-np';
            if ($item['dateInfo']['isSunday']) echo ' sun';
            if ($item['dateInfo']['isToday']) echo ' today'; ?>">
            <div class="day-number"><?php echo $item['dateInfo']['day'];?></div>
            <input type="hidden" class="day-date" value="<?php echo $item['dateInfo']['date'] ?>"/>
            <div id="<?php echo $item['dateInfo']['date'] ?>" class="day-limit">
                <?php if (!empty($item['dateData']['limit'])) echo $item['dateData']['limit'];?>
            </div>
        </td>
        <?php if (($key + 1) % 7 == 0) { ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
<!-- Modal -->
<div id="OrderDailyModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btnclose" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only"><?php echo __('Close') ?></span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Daily limit order'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="total_tax_price"> <?php echo __('Limit quantity') ?></label>
                    <input type="text" id="limit" class="form-control" name="limit" maxlength="11">
                </div>
                <!-- /.input group -->
                <div class="cls"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnclose" ><?php echo __('Cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btnModalSave"><?php echo __('Save'); ?></button>
                <input type="hidden" id="shopId" value="<?php echo $shopId ?>"/>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            $("div#OrderDailyModal button[class*='btnclose']").click(function(){
                $(this).closest("div#OrderDailyModal").hide(100);
            });
        });
    </script>
</div>
<script type="text/javascript">
    $(function (){
        $("#tb_order_daily_limit td").click(function (){
            // Get date and show popup
            if(!$(this).hasClass('calendar-day')){
                return false;
            }
            var limit = $.trim($(this).find('div.day-limit').text());
            var _day_date = $(this).find('.day-date').val();
            var _pattern   = /(\d{4})\-(\d{1,2})\-(\d{1,2})/;
            var _replace    = "$1-$2-" + _day_date.substr(-2, 2);
            var _effectDate  = _day_date.replace(_pattern, _replace);
            $("div#OrderDailyModal #myModalLabel").text(" <?php echo __('Daily limit order'); ?> " +( _effectDate.replace(_pattern, '$1年$2月$3日')));
            $('div#OrderDailyModal').attr('data-date', _day_date);
            $('div#OrderDailyModal').show(100);
            $('#limit').focus().val(limit);
        });
        $("div#OrderDailyModal button[id*='btnModalSave']").click(function (){
            var url = baseUrl + 'ajax/orderdailylimit';
            var data = {
                shop_id: $('#shopId').val(),
                limit: $('#limit').val(),
                date: $('#OrderDailyModal').attr('data-date')
            };
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (response) {
                    if (response) {
                        alert(response);
                    }
                }
            });
            if ($("#limit").val() == 0) {
                $("#" + data.date).text("");
            } else {
                $("#" + data.date).text(parseInt($("#limit").val()));
            }
            $(this).closest("div#OrderDailyModal").hide(100);
            return false;
        });
        $('#limit').keypress(function(event) {
            // Backspace, tab, enter, end, home, left, right
            // We don't support the del key in Opera because del == . == 46.
            var controlKeys = [8, 9, 13, 35, 36, 37, 39];
            // IE doesn't support indexOf
            var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
            // Some browsers just don't raise events for control keys. Easy.
            // e.g. Safari backspace.
            if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
                (48 <= event.which && event.which <= 57) || // Always 1 through 9
                (48 == event.which && $(this).attr("value")) || // No 0 first digit
                isControlKey) { // Opera assigns values for control keys.
                return;
            } else {
                event.preventDefault();
            }
        });
    });

</script>