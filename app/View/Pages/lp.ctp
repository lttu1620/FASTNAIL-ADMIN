
<div class="box box-default">
    <div class="box-header with-border">
      <i class="fa fa-link"></i>
      <h3 class="box-title">リンク</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <a href="<?php echo $this->html->url('/login'); ?>" class="text-center">ログイン</a>
    </div><!-- /.box-body -->
</div>