<?php

echo $this->element('calendar', array(
        'data' => $data,
        'dt' => $dt,
        'staffRecord' => $staffRecord,
        'deviceRecord' => $deviceRecord,
        'shop_open_time' => $shop_open_time,
        'shop_close_time' => $shop_close_time,
        'AppUI' => $AppUI
    )
);
