<div class="row">
    <div class="col-xs-120">  
        <div class="box box-primary collapsed-box">  
            <div data-original-title="Header tooltip" title="" data-toggle="tooltip" class="box-header">
                <h3 class="box-title"><?php echo __('Search') ?></h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-primary btn-xs search-collapse"><i class="fa fa-plus"></i></button>                
                </div>
            </div>  
            <div class="box-body search-body" style="display:none;">          
                <?php
                    echo $this->SimpleForm->render($searchForm);
                ?>                              
            </div>   
        </div>
    </div>
</div>
<!-- Main row -->

<div class="row">
    <div class="col-xs-120">   
        <div class="box box-primary">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('News feed favorite chart') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div> 
            <div class="box-body">                
                <div id="newsfeedFavorite" style="min-height:500px;">
                    <?php echo $newsfeedFavorite; ?> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-120">   
        <div class="box box-primary">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('News feed share chart') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div> 
            <div class="box-body">                
                <div id="newsfeedShare" style="min-height:500px;">
                    <?php echo $newsfeedShare; ?> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-120">   
        <div class="box box-primary">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('News feed like chart') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div> 
            <div class="box-body">                
                <div id="newsfeedLike" style="min-height:500px;">
                    <?php echo $newsfeedLike; ?> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-120">   
        <div class="box box-primary">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('News feed view chart') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div> 
            <div class="box-body">                
                <div id="newsfeedView" style="min-height:500px;"
                     ><?php echo $newsfeedView; ?> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-120">   
        <div class="box box-primary">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('News feed read chart') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div> 
            <div class="box-body">                
                <div id="newsfeedRead" style="min-height:500px;">
                    <?php echo $newsfeedRead; ?> 
                </div>
            </div>
        </div>
    </div>
</div>