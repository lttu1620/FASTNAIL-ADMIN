<table class="calendar_control" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="40%" align="left">
            <a href="?date=<?php echo $info['firstDayOfLastMonth']; ?>">
                « <?php echo $info['lastMonth']; ?>月<?php echo $info['yearOfLastMonth']; ?>
            </a>
        </td>
        <td width="20%" class="center">
            <center><strong>
                <?php echo $info['thisMonth']; ?>月<?php echo $info['thisYear']; ?>
            </strong></center>
        </td>
        <td width="40%" align="right">
            <a href="?date=<?php echo $info['firstDayOfNextMonth']; ?>" style="float:right;">
                <?php echo $info['nextMonth']; ?>月<?php echo $info['yearOfNextMonth']; ?> »
            </a>
        </td>
    </tr>
</table>
<table id="holiday_table" cellpadding="0" cellspacing="0" class="table table-bordered calendar-table">
    <tbody>
    <tr class="calendar-row">
        <th class="calendar-day-head">日</th>
        <th class="calendar-day-head">月</th>
        <th class="calendar-day-head">火</th>
        <th class="calendar-day-head">水</th>
        <th class="calendar-day-head">木</th>
        <th class="calendar-day-head">金</th>
        <th class="calendar-day-head">土</th>
    </tr>
    <?php foreach ($data as $key => $item) {
        if ($key % 7 == 0) { ?>
            <tr class="calendar-row">
        <?php } ?>

        <td class=" <?php
        if ($item['dateInfo']['isThisMonth'])
            echo 'calendar-day';
        else
            echo 'calendar-day-np';
        if ($item['dateInfo']['isSunday']) echo ' sun';
        if ($item['dateInfo']['isToday'] && $item['dateInfo']['isHoliday']) echo ' holiday-today';
        elseif ($item['dateInfo']['isToday']) echo ' today';
        if ($item['dateInfo']['isHoliday']) echo ' holiday'; ?> <?php echo $item['dateInfo']['date']; ?>">
            <button type="button" class="close btnclose delete-holiday
            <?php if (!$item['dateInfo']['isHoliday']) echo 'hidden'; ?>" data-dismiss="modal">
                <input type="hidden" id="delete-date" value="<?php echo $item['dateInfo']['date']; ?>"/>
                <input type="hidden" id="holiday" value="<?php echo $item['dateInfo']['isHoliday'] ? 1 : 0;?>"/>
                <span aria-hidden="true">&times;</span>
                <span class="sr-only"><?php echo __('Close') ?></span>
            </button>
            <div class="day-number">
                <a href="#">
                    <?php echo $item['dateInfo']['day']; ?>
                </a>
            </div>
            <input type="hidden" id="day-date" value="<?php echo $item['dateInfo']['date']; ?>"/>
            <div class="day-data">
                <?php if (isset($item['dateData']) && !empty($item['dateData'])) : ?>
                    <?php echo $item['dateData']['name']; ?>
                <?php endif; ?>
            </div>
        </td>
        <?php if (($key + 1) % 7 == 0) { ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
<!-- Modal -->
<div id="HolidayModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btnclose" data-dismiss="modal" style="margin-top: 0px;">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only"><?php echo __('Close') ?></span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Holiday'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="holiday_name"> <?php echo __('Holiday Name') ?></label>
                    <input type="text" id="name" class="form-control" name="name">
                </div>
                <div class="cls"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnclose" ><?php echo __('Cancel'); ?></button>
                <button type="button" class="btn btn-primary" id="btnModalSave"><?php echo __('Save'); ?></button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            $("div#HolidayModal button[class*='btnclose']").click(function(){
                $(this).closest("div#HolidayModal").hide(100);
            });
        });
    </script>
</div>
<script type="text/javascript">
    $(function (){
        $("#holiday_table td").click(function(){
            var name = $.trim($(this).find('div.day-data').text());
            var is_holiday = $('#holiday').val();
            var _day_date = $(this).find('#day-date').val();
            var _current_date = $("#res-timetable").attr('data-date');
            var _pattern   = /(\d{4})\-(\d{1,2})\-(\d{1,2})/;
            var _replace    = "$1-$2-"+ $.trim($(this).find('div.day-number').text());
            var _effectDate  = _current_date.replace(_pattern, _replace);
            $("div#HolidayModal #myModalLabel").text(" <?php echo __('Holiday'); ?> " +( _effectDate.replace(_pattern, '$1年$2月$3日')));
            $('div#HolidayModal').attr('data-date', _day_date).show(100);
            $('#name').focus().val(name);
        });
        $("button[class*='delete-holiday']").click(function(){
            if ($(this).find('#holiday').val() == '0') {
                return false;
            }
            if (confirm('本当によろしいですか？') == false) {
                return false;
            }
            $(this).siblings('.day-data').empty();
            $(this).addClass('hidden');
            $(this).parent().removeClass('holiday');
            var thisDate = $(this).parent();
            if (thisDate.hasClass('holiday-today')) {
                thisDate.addClass('today');
                thisDate.removeClass('holiday-today');
            }

            var url = baseUrl + 'ajax/holiday';
            var data = {
                disable: 1,
                date: $(this).find('#delete-date').val()
            };
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (response) {
                }
            });
            return false;
        });
        $("div#HolidayModal button[id*='btnModalSave']").click(function (){
            var url = baseUrl + 'ajax/holiday';
            var name = $('#name').val();
            var date = $('#HolidayModal').attr('data-date');
            var data = {
                disable: 0,
                name: name,
                date: date
            };
            $(this).closest("div#HolidayModal").hide(100, function(){
                var thisDate = $("."+date);
                thisDate.addClass('holiday');
                if (thisDate.hasClass('today')) {
                    thisDate.addClass('holiday-today');
                    thisDate.removeClass('today');
                }
                $("."+date+" .day-data").text(name);
                $("."+date+" button").removeClass('hidden');
                $("."+date+" button input[id*=holiday]").val(1);
            });
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (response) {
                }
            });
            return false;
        });
    });

</script>