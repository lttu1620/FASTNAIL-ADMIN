<div id='main' role='main'>
    <div id='workspace'>
        <div id='customers-index'>
            <div class='navbar navbar-static-top subnav' id='customers-subnav'>
                <div class='navbar-inner'>
                    <h1>顧客</h1>
                    <div class='pull-right'>
                        <!-- <a href="javascript:void(0)" class="btn btn-success" id="btn-create-user" data-modal="true" data-remote="true">顧客を作成する</a> -->
                    </div>
                    <div class='subnav-form' id='cust-query'>
                        <form action="<?php echo($this->Html->url('/shops/customer')); ?>" role="form" enctype="multipart/form-data" id="ShopCustomerForm" method="get" accept-charset="utf-8">
                            <span class="search-customer">
                                <select name="member" class="form-control">
                                    <option value="1" <?php if (!empty($param['member'])) echo 'selected' ?>><?php echo __('Member');?></option>
                                    <option value="0" <?php if (empty($param['member'])) echo 'selected' ?>><?php echo __('Guest');?></option>
                                </select>
                            </span>
                        <input class='form-control search-query' name="keyword"  id="cust-q" value="<?php echo !empty($param['keyword'])  ? $param['keyword'] : ''; ?>"  placeholder='名前、電話番号、またはEメール' type="text">
                        <b><?php echo __('Date'); ?></b>
                        <input name="date_from" class="date_picker form-control input-date mobidate" value="<?php echo !empty($param['date_from'])  ? $param['date_from'] : ''; ?>" placeholder='日付から' type="text">
                        <span class="x16-sprite x16-tilde" height="16" width="16"></span>
                        <input name="date_to" class="date_picker form-control input-date mobidat" value="<?php echo !empty($param['date_to'])  ? $param['date_to'] : ''; ?>"  placeholder='日付まで' type="text">
                            <div class='btn-group'>
                            <input value="<?php echo __('Search');?>"  type="submit" class="btn">

                        </div>
                        <span class='x16-loader invisible' id='cust-loading'></span>
                        </form>
                    </div>
                </div>
            </div>
            <div class='container-fluid' id='customers-list'>
                <?php
                    echo $this->SimpleTable->render($table);
                    echo $this->Paginate->render($total, $limit);
                 ?>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function(){
        $('.table').each(function() {

            $(this).removeClass('table-hover');
            $(this).addClass('table-bordered');
            $(this).addClass('table-striped');
        });
        
        //prepare for dialog
        $('body').append("<div id=\"dialog\" ></div>");
        //show dialog function
        showCustomerDialog = function(cus_id){
            
            $.ajax({
			    type: 'GET',
			    url: baseUrl + 'ajax/customerdialog?cus_id=' + cus_id,
			    data: {},
			    success: function (response) {
			        $("#dialog").html(response);
			        $("#dialog").dialog({
			            minWidth: 1000,
			            maxWidth: 1000,
			            minHeight: 825,
			            maxHeight: 825,
			            modal: true,
			            resizable: false,
                        draggable: false
                        
			        });
			    }
			});
        };
        
        $('#btn-create-user').click(function(){
			showCustomerDialog(0);
        });
    });
</script>