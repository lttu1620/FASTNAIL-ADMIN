<div id='timebar-container'></div>
<table class='table table-bordered timetable-table unselectable timebars-loading' id='res-timetable-table'>
<!--Time label -->
<thead>
    <tr>
    <?php
        $open_hour = $shop_open_time['hours'];
        $open_minute = $shop_open_time['minutes'];
        $close_hour = $shop_close_time['hours'];
        $close_minute = $shop_close_time['minutes'];
        $close_time = $shop_close_time[0];
        $name = !empty($max_seat) ? '空き枠' : 'ネイリスト';
        echo "<th class='timetable-column-stylist'>{$name}</th>";   
        for ($hour = $open_hour; $hour <= $close_hour; $hour++) {
            $m2 = '00';
            $start_m = 0;
            $end_m = 30;
            if ($hour == $open_hour) {
                $m2 = str_pad($open_minute, 2, '0', STR_PAD_LEFT);
                $start_m = $open_minute;         
            } elseif ($hour == $close_hour) {
                $end_m = $close_minute - 16;        
            }
            if ($end_m > 0) {
                echo "<th class='th-hour th-slot'>{$hour}<small>:{$m2}</small></th>";
                for ($m = $start_m; $m <= $end_m; $m+=15) {        
                    echo "<th class='th-slot'></th>";
                }
            }
        }   
    ?>
    </tr>
</thead>

<!--Seat & Device list-->
<tbody>
<?php
$timetableRow = array();
if (!empty($staffRecord)) {
    foreach ($staffRecord as $value) {
        $timetableRow['n_' . $value['id']] = array(
            'type' => 'nailist', 
            'value' => $value['id'], 
            'name' => $value['name'],
        );
    }
} elseif (!empty($max_real_seat)) {
    for ($seatId = 1; $seatId <= $max_real_seat; $seatId++) {
        $timetableRow['s_' . $seatId] = array(
            'type' => 'seat', 
            'value' => $seatId, 
            'name' => '枠 ' . $seatId, 
        );
    }
}
if (!empty($deviceRecord)) {
    foreach ($deviceRecord as $value) {
        $timetableRow['d_' . $value['id']] = array(
            'type' => 'device', 
            'value' => $value['id'], 
            'name' => $value['name'],
        );
    }
}

$is_max_fn = false;
$is_max_hp = false;
$is_first_device = false;
$num_seat = 0;
if(empty($max_seat_fn)){
	$max_seat_fn = 0;
}
if(empty($max_seat_hp)){
	$max_seat_hp = 0;
}
if(empty($max_real_seat)){
	$max_real_seat = 0;
}

$style1 = " style='border-bottom: 2px solid rgb(245, 150, 43);' ";
$style2 = " style='border-top: 2px solid rgb(245, 150, 43);' ";
foreach ($timetableRow as $key => $row) {
	$style = "";
    echo "<tr class='timetable-row' data-sty='{$key}'>";
    if ($row['type'] == 'seat' && $is_max_fn == false && $max_seat_fn > 0) {
        $num_seat++;
        if ($num_seat == $max_seat_fn) {
            $is_max_fn = true;
            $style = $style1;
            echo "<td class='timetable-column-stylist' rowspan='1' {$style}>{$row['name']}</td>";
        } else {
            echo "<td class='timetable-column-stylist' rowspan='1'>{$row['name']}</td>";
        }
    } elseif ($row['type'] == 'seat' && $is_max_hp == false && $max_seat_hp > 0) {
        $num_seat++;
        if ($num_seat == ($max_seat_fn + $max_seat_hp)) {
            $is_max_hp = true;
            $style = $style1;
            echo "<td class='timetable-column-stylist' rowspan='1' {$style}>{$row['name']}</td>";
        } else {
            echo "<td class='timetable-column-stylist' rowspan='1'>{$row['name']}</td>";
        }
    } elseif (($row['type'] == 'device' && $is_first_device == false ) && (!empty($staffRecord) || ($max_real_seat > $max_seat_fn + $max_seat_hp))) {
        $is_first_device = true;
        $style = $style2;
        echo "<td class='timetable-column-stylist' rowspan='1' {$style}>{$row['name']}</td>";
    } else {
        echo "<td class='timetable-column-stylist' rowspan='1'>{$row['name']}</td>";
    }
    for ($hour = $open_hour; $hour <= $close_hour; $hour++) {
        $start_m = 0;
        $end_m = 45;
        if ($hour == $open_hour) {
            $start_m = $open_minute;
        } elseif ($hour == $close_hour) {
            $end_m = $close_minute - 1;
        }
        for ($m = $start_m; $m <= $end_m; $m+=15) {
            $h2 = str_pad($hour, 2, '0', STR_PAD_LEFT);
            $m2 = str_pad($m, 2, '0', STR_PAD_LEFT);  
            $class = ($m == 0 ? 'slot' : 'mid slot'); 
            if ($row['type'] == 'seat') {
                $seatId = $row['value'];
                if (!isset($order_timely_limit[$h2 . ':' . $m2])) {
                    $order_timely_limit[$h2 . ':' . $m2]['limit'] = $AppUI->shop_max_seat;
                    $order_timely_limit[$h2 . ':' . $m2]['hp_limit'] = $AppUI->shop_hp_max_seat;
                }
                //if ($AppUI->shop_reservation_interval == 30 && ($m == 15 || $m == 45)) {
                    //$order_timely_limit[$h2 . ':' . $m2]['limit'] = $order_timely_limit[$h2 . ':' . str_pad($m-15, 2, '0', STR_PAD_LEFT)]['limit'];
                    //$order_timely_limit[$h2 . ':' . $m2]['hp_limit'] = $order_timely_limit[$h2 . ':' . str_pad($m-15, 2, '0', STR_PAD_LEFT)]['hp_limit'];
                //}  
                if ($seatId > $max_seat) {
                    $class .= ' extra-seat';
                } else {
                    if ($order_timely_limit[$h2 . ':' . $m2]['limit'] < $max_seat_fn && in_array($seatId, range($order_timely_limit[$h2 . ':' . $m2]['limit'] + 1, $max_seat_fn))) {
                        // disable seat of FN
                        if(!empty($is_edit_seat)){
                        	$class .= ' faked-disabled';
                        }else{
                        	$class .= ' disabled';
                            // commented by thailh, allow booking on disabled area
                        	//$class = str_replace('slot', '', $class);
                        }
                    } elseif ($order_timely_limit[$h2 . ':' . $m2]['hp_limit'] < $max_seat_hp && in_array($seatId, range($max_seat_fn + $order_timely_limit[$h2 . ':' . $m2]['hp_limit'] + 1, $max_seat))) {
                        // disable seat of HP
                    	if(!empty($is_edit_seat)){
                    		$class .= ' faked-disabled';
                    	}else{
                    		$class .= ' disabled';
                            // commented by thailh, allow booking on disabled area
                    		// $class = str_replace('slot', '', $class);
                    	}
                    }
                }                
            }
            if (strtotime($dt." {$h2}:{$m2}:00") <= $close_time) {
           		echo "<td class='{$class}' data-t='" . strtotime($dt." {$h2}:{$m2}:00") . "' {$style}></td>";
            }
        }
    }
    echo "</tr>";
}                
?>
</tbody>
</table>

<script type="text/javascript">           
    <?php
    $start_at_epoch_time = str_pad($open_hour, 2, '0', STR_PAD_LEFT) . ":" . str_pad($open_minute, 2, '0', STR_PAD_LEFT) . ":00";
    $last_order_at_epoch_time = str_pad(($close_hour - 1), 2, '0', STR_PAD_LEFT) . ":00:00";
    $end_at_epoch_time = str_pad($close_hour, 2, '0', STR_PAD_LEFT) . ":" . str_pad($close_minute, 2, '0', STR_PAD_LEFT) . ":00";
    ?>
    window.reservations = <?=$data?>; 
    window.blockages = [];
    window.sheet_set = {
        "is_override":null,
        "is_closed":null,
        "all_day":{
            "start_at_epoch":<?= strtotime($dt." {$start_at_epoch_time}") ?>,
            "last_order_at_epoch":<?= strtotime($dt." {$last_order_at_epoch_time}") ?>,
            "end_at_epoch":<?= strtotime($dt." {$end_at_epoch_time}") ?>
        },
        "breakfast":null,
        "lunch":null,
        "tea":null,
        "dinner":null,
        "night":null
    };
    window.i18n_confirm_time_change = "開始時間が変更されます。よろしいですか？";
    window.reservation_overbook = true;  
    $(function(){
        setTimeout(function(){Timetable.draw()}, 50);    
    });  
</script>