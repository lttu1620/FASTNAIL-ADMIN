
<div id='timebar-container'></div>
<table class='table table-bordered timetable-table unselectable timebars-loading' id='res-timetable-table'>
<thead>
<tr>
<th class='timetable-column-stylist'>
ネイリスト
</th>
<!--Label time-->
<!--Open time label-->
<?php
    $open_hour = $shop_open_time['hours'];
    $open_minute = $shop_open_time['minutes'];
    $close_hour = $shop_close_time['hours'];
    $close_minute = $shop_close_time['minutes'];
    if(empty($open_minute)):
?>
    <th class='th-hour th-slot'>
    <?php echo $open_hour?><small>:00</small>
    </th>
    <th class='th-slot'></th>
    <th class='th-slot'></th>
    <th class='th-slot'></th>
<?php else:?>
    <th class='th-hour th-slot'>
    <?php echo $open_hour?><small><?php echo ":{$open_minute}"?></small>
    </th>
    <?php if($open_minute == 15):?>
        <th class='th-slot'></th>
        <th class='th-slot'></th>
    <?php elseif($open_minute == 30):?>
        <th class='th-slot'></th>
    <?php else:?>
    <?php endif;?>
<?php endif;?>
<!--End open time label-->        
<!--Mid time label-->
<?php for($hour = $open_hour + 1; $hour < $close_hour - 1 ; $hour++):?>
    <th class='th-hour th-slot'>
    <?php echo $hour ?><small>:00</small>
    </th>
    <th class='th-slot'></th>
    <th class='th-slot'></th>
    <th class='th-slot'></th>  
<?php endfor;?>   
<!--End mid time label-->
<!--Close time label-->
<?php 
$close_h = $close_hour - 1;
if(empty($close_minute)):?>
    <th class='th-hour th-slot'>
    <?php echo ($close_h)?><small>:00</small>
    </th>
    <th class='th-slot'></th>
    <th class='th-slot'></th>
    <th class='th-slot'></th>
<?php else:?>
    <th class='th-hour th-slot'>
    <?php echo ($close_h)?><small>:00</small>
    </th>
    <?php if($close_minute == 15):?>
    <?php elseif($close_minute == 30):?>
        <th class='th-slot'></th>
    <?php else:?>
        <th class='th-slot'></th>
        <th class='th-slot'></th>
    <?php endif;?>
<?php endif;?>
<!--End close time label-->
</tr>
</thead>
<!--End label time-->
<tbody>
<!--Staff list-->    
<?php foreach ($staffRecord as $value): ?>
<tr class='timetable-row' data-sty='n_<?=$value['id'];?>'>
<td class='timetable-column-stylist' rowspan='1'><?=$value['name'];?></td>
<!--Open time range-->
<?php
    $start_h = str_pad($open_hour,2,'0',STR_PAD_LEFT);
    if(empty($open_minute)):?>
    <td class='slot' data-t='<?= strtotime($dt." {$start_h}:00:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:15:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:30:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:45:00") ?>'>
    </td>
<?php else:?>
    <?php if($open_minute == 15):?>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:15:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:30:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:45:00") ?>'>
        </td>
    <?php elseif($open_minute == 30):?>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:30:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:45:00") ?>'>
        </td>
    <?php else:?>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:45:00") ?>'>
    </td>
    <?php endif;?>
<?php endif;?>
<!--End open time range-->
<!--Mid time range-->
<?php for($hour = $open_hour + 1; $hour < $close_hour - 1 ; $hour++):?>
    <?php $mid_h = str_pad($hour,2,'0',STR_PAD_LEFT)?>
    <td class='slot' data-t='<?= strtotime($dt." {$mid_h}:00:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$mid_h}:15:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$mid_h}:30:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$mid_h}:45:00") ?>'>
    </td>
<?php endfor;?>   
<!--End mid time range-->
<!--Close time range-->
<?php
    $end_h = str_pad($close_hour - 1 ,2,'0',STR_PAD_LEFT);
    if(empty($close_minute)):?>
    <td class='slot' data-t='<?= strtotime($dt." {$end_h}:00:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:15:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:30:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:45:00") ?>'>
    </td>
<?php else:?>
    <?php if($close_minute == 15):?>
        <td class='slot' data-t='<?= strtotime($dt." {$end_h}:00:00") ?>'>
        </td>
       
    <?php elseif($close_minute == 30):?>
        <td class='slot' data-t='<?= strtotime($dt." {$end_h}:00:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:15:00") ?>'>
        </td>
    <?php else:?>
        <td class='slot' data-t='<?= strtotime($dt." {$end_h}:00:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:15:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:30:00") ?>'>
        </td>
    <?php endif;?>
<?php endif;?>    
<!--End close time range-->
</tr>
<?php endforeach; ?> 

<!--Device list--> 
<?php foreach ($deviceRecord as $value): ?>
<tr class='timetable-row' data-sty='d_<?=$value['id'];?>'>
<td class='timetable-column-stylist' rowspan='1'><?=$value['name'];?></td>
<!--Open time range-->
<?php
    $start_h = str_pad($open_hour,2,'0',STR_PAD_LEFT);
    if(empty($open_minute)):?>
    <td class='slot' data-t='<?= strtotime($dt." {$start_h}:00:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:15:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:30:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:45:00") ?>'>
    </td>
<?php else:?>
    <?php if($open_minute == 15):?>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:15:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:30:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:45:00") ?>'>
        </td>
    <?php elseif($open_minute == 30):?>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:30:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:45:00") ?>'>
        </td>
    <?php else:?>
        <td class='mid slot' data-t='<?= strtotime($dt." {$start_h}:45:00") ?>'>
    </td>
    <?php endif;?>
<?php endif;?>
<!--End open time range-->
<!--Mid time range-->
<?php for($hour = $open_hour + 1; $hour < $close_hour - 1 ; $hour++):?>
    <?php $mid_h = str_pad($hour,2,'0',STR_PAD_LEFT)?>
    <td class='slot' data-t='<?= strtotime($dt." {$mid_h}:00:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$mid_h}:15:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$mid_h}:30:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$mid_h}:45:00") ?>'>
    </td>
<?php endfor;?>   
<!--End mid time range-->
<!--Close time range-->
<?php
    $end_h = str_pad($close_hour - 1,2,'0',STR_PAD_LEFT);
    if(empty($close_minute)):?>
    <td class='slot' data-t='<?= strtotime($dt." {$end_h}:00:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:15:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:30:00") ?>'>
    </td>
    <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:45:00") ?>'>
    </td>
<?php else:?>
    <?php if($close_minute == 15):?>
        <td class='slot' data-t='<?= strtotime($dt." {$end_h}:00:00") ?>'>
        </td>
    <?php elseif($close_minute == 30):?>
        <td class='slot' data-t='<?= strtotime($dt." {$end_h}:00:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:15:00") ?>'>
        </td>
    <?php else:?>
        <td class='slot' data-t='<?= strtotime($dt." {$end_h}:00:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:15:00") ?>'>
        </td>
        <td class='mid slot' data-t='<?= strtotime($dt." {$end_h}:30:00") ?>'>
        </td>
    <?php endif;?>
<?php endif;?>    
<!--End close time range-->
</tr>
<?php endforeach; ?> 
</tbody>
</table>
</div>
<script type="text/javascript">           
<?php
   $start_at_epoch_time =  str_pad($open_hour,2,'0',STR_PAD_LEFT).":".str_pad($open_minute,2,'0',STR_PAD_LEFT).":00";
   $last_order_at_epoch_time =  str_pad(($close_hour - 1),2,'0',STR_PAD_LEFT).":00:00";
   $end_at_epoch_time = str_pad($close_hour,2,'0',STR_PAD_LEFT).":".str_pad($close_minute,2,'0',STR_PAD_LEFT).":00";
?>
  window.reservations = <?=$data?>; 
  window.blockages    = [];
  window.sheet_set    = {"is_override":null,"is_closed":null,"all_day":{"start_at_epoch":<?= strtotime($dt." {$start_at_epoch_time}") ?>,"last_order_at_epoch":<?= strtotime($dt." {$last_order_at_epoch_time}") ?>,"end_at_epoch":<?= strtotime($dt." {$end_at_epoch_time}") ?>},"breakfast":null,"lunch":null,"tea":null,"dinner":null,"night":null};
  
  window.i18n_confirm_time_change = "開始時間が変更されます。よろしいですか？";
  window.reservation_overbook = true;  
  $(function(){
        setTimeout(function(){Timetable.draw()}, 50);    
  });  
</script>