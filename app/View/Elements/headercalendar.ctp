<?php 
    $calTime = strtotime($dt);
    $curTime = strtotime(date("Y-m-d"));   
 ?>
 <div id='messages'></div>
<div class='navbar navbar-static-top subnav' id='reservations-subnav'> 
    <div class='navbar-inner'> 
        <h1>予約</h1>
        <div class='timedial' id='reservations-timedial'> 
            <input class='mobidate hidden' id='timedial' value='<?php echo $dt ?>'> 
            <img alt='' class='cal-btn' height='32' id='timedial-cal-btn' src='<?php echo Router::url('/img/calendar_icon.png') ?>' width='32' /> 
            <?php if($curTime > $calTime){ ?>     
                <b class='label label-important'>過去</b> 
                <b id='todayId'><a href='<?php echo Configure::read('ADM.Host') ?>' data-remote='true'>本日</a></b>
            <?php } elseif($curTime < $calTime) { ?>
                <b class='invisible label label-important'>過去</b> 
                <b id='todayId'><a href='<?php echo Configure::read('Host')?>' data-remote='true'>本日</a></b> 
            <?php } else { ?>
                <b class='invisible label label-important'>過去</b> 
                <b class='label label-info' style='margin-left: 0px;'>本日</b> 
            <?php } ?>
            <span class='timedial-loading x16-loader invisible'></span> 
        </div> 
        <div class='pull-right calendar-order-status'> 
        	<?php if ($controller == 'orders' && $action == 'seat'): ?>
        		<a href="#" class="btn" data-remote="true" id="seat-status-normal">通常</a>
                <a href="#" class="btn" data-remote="true" id="seat-status-disable">B作成 </a>
                <a href="#" class="btn" data-remote="true" id="seat-status-enable">B削除</a>
        	<?php endif;?>
            <a href='#' class='btn btn-success' data-modal='true' data-remote='true' id='new-reservation'>予約を作成する</a>
        </div>
        <ul class='nav nav-tabs' id='btn-tab'> 
            <li class="<?php if ($controller == 'orders' && $action == 'seat') echo 'active'; ?>">
                <a href="<?php echo Router::url('/orders/seat?date=' . $dt); ?>" data-remote="true">
                    <span class="x16-sprite x16-timetable_gray" height="16" width="16"></span> <?php echo __('Seats chart') ?>
                </a>
            </li>
            <li class="<?php if ($controller == 'pages' && $action == 'index') echo 'active'; ?>">
                <a href="<?php echo Router::url('/orders/staff?date=' . $dt); ?>" data-remote="true">
                    <span class="x16-sprite x16-timetable_gray" height="16" width="16"></span> 指名チャート
                </a>
            </li>
            <li class="<?php if ($controller == 'orders' && $action == 'lists') echo 'active'; ?>">
                <a href="<?php echo Router::url('/orders/lists?date=' . $dt); ?>" data-remote="true">
                    <span class="x16-sprite x16-list_cyan" height="16" width="16"></span> 台帳
                </a>
            </li>
            <li class="<?php if ($controller == 'orders' && $action == 'calendar') echo 'active'; ?>">
                <a href="<?php echo Router::url('/orders/calendar?date=' . $dt); ?>" data-remote="true">
                    <span class="x16-sprite x16-calendar_cyan" height="16" width="16"></span> カレンダー
                </a>
            </li>
            <li class="<?php if ($controller == 'ordertimelylimits' && $action == 'index') echo 'active'; ?>">
                <a href="<?php echo Router::url('/ordertimelylimits?date=' . $dt); ?>" data-remote="true">
                    <span class="x16-sprite x16-calendar_cyan" height="16" width="16"></span> 時間別予約可能数
                </a>
            </li>
        </ul> 
    </div>         
</div>
<script type='text/javascript'> 
$('.mobidate').mobiscroll().date({
    mode:    'scroller', 
    height:  30, 
    display: 'inline', 
    theme:   'tsmobi tsmobi-mini', 
    rows:    1, 
    dayNamesShort:   I18n.t('date.abbr_day_names'), 
    monthNamesShort: I18n.t('date.abbr_month_names').slice(1), 
    yearText:   I18n.t('datetime.prompts.year'), 
    monthText:  I18n.t('datetime.prompts.month'), 
    dayText:    I18n.t('datetime.prompts.day'), 
    dateFormat: 'yy-mm-dd', 
    dateOrder:  I18n.asian_locale() ? 'yyMd D' : 'yyMD d', 
    endYear:   new Date().getFullYear() + 3, 
    startYear: new Date().getFullYear() - 5, 
    width:      50,
    onChange: function(newDate, inst) { 
        if (newDate != oldDate) { 
            $('.timedial-loading').removeClass('invisible'); 
            Reservations.set_date(newDate); 
        } 
    } 
}); 
var oldDate = $('#timedial').val(); 
$('.mobidate').mobiscroll().calendar({ 
      anchor: $('#timedial-cal-btn'), 
      display: 'bubble', 
      theme: 'tsmobi no-btns', 
      navigation: 'month', 
      controls: ['calendar'], 
      dayNamesShort:   I18n.t('date.abbr_day_names'), 
      monthNames: I18n.t('date.month_names').slice(1), 
      yearText:   I18n.t('datetime.prompts.year'), 
      monthText:  I18n.t('datetime.prompts.month'), 
      dayText:    I18n.t('datetime.prompts.day'), 
      dateFormat: 'yy-mm-dd', 
      onDayChange: function(day, inst) {
         newDate=DateUtil.formatDate(day.date); 
         if (newDate != $(this).val()) { 
            $('.timedial-loading').removeClass('invisible'); 
            $(this).val(newDate); 
            Reservations.set_date($(this).val()); 
         } 
         $(this).mobiscroll('hide'); 
     } 
}); 
$(function(){ 
    
    $('#timedial-cal-btn').click(function(){ 
        $('.mobidate').mobiscroll('show'); 
        return false; 
    }); 
    
    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
          if ((new Date().getTime() - start) > milliseconds){
            break;
          }
        }
    }

    function clickStatusBtn() {
        $(".calendar-order-status a").each(function(index) {
            var id = $(this).attr('id');
            if ($(this).hasClass('active')) {
                if (id == 'seat-status-normal') {
                    $('#seat-status-disable').removeClass('btn-warning');
                    $('#seat-status-enable').removeClass('btn-info');
                    $(this).addClass('btn-success');
                } else if (id == 'seat-status-disable') {
                    $('#seat-status-normal').removeClass('btn-success');
                    $('#seat-status-enable').removeClass('btn-info');
                    $(this).addClass('btn-warning');
                } else if (id == 'seat-status-enable') {
                    $('#seat-status-normal').removeClass('btn-success');
                    $('#seat-status-disable').removeClass('btn-warning');
                    $(this).addClass('btn-info');
                }
            }            
        });
    }
    
	function onLoadHeaderCalendar(){
		
		var delete_seat_cookie = '<?php echo !empty($is_edit_seat) ? 1: 0?>';
		var controller = '<?php echo !empty($controller) ? $controller : ''?>';
		var action = '<?php echo !empty($action) ? $action : ''?>';

		if(delete_seat_cookie == 0 || (controller!='orders' && action!='seat')){		
			setCookie('DisableSeatCookie', '', -1);
			setCookie('EnableSeatCookie', '', -1);
		}
		
		var disable_seat_cookie = getCookie('DisableSeatCookie');
		var enable_seat_cookie = getCookie('EnableSeatCookie');
		if(disable_seat_cookie == 'on'){//disable
			$('#seat-status-disable').addClass('active');
            $('#seat-status-normal').removeClass('active');
            $('#seat-status-enable').removeClass('active');            
		}else if(enable_seat_cookie == 'on'){//enable
			$('#seat-status-enable').addClass('active');
            $('#seat-status-normal').removeClass('active');
            $('#seat-status-disable').removeClass('active');
		}else{//default
			$('#seat-status-normal').addClass('active');
            $('#seat-status-disable').removeClass('active');
            $('#seat-status-enable').removeClass('active');
		}
        clickStatusBtn();
	}

	onLoadHeaderCalendar();
    
    $('#seat-status-normal').click(function(){
        if ($(this).hasClass('active')) {
            return false;
        }
		$(this).addClass('active');
        $('#seat-status-disable').removeClass('active');
        $('#seat-status-enable').removeClass('active');
        if(getCookie('EnableSeatCookie') != '') {
            setCookie('EnableSeatCookie', '', -1);
        }
        if(getCookie('DisableSeatCookie') != '') {
            setCookie('DisableSeatCookie', '', -1);
        }
        clickStatusBtn();
    }); 
    
    $('#seat-status-disable').click(function(){
        if ($(this).hasClass('active')) {
            return false;
        }
		var is_edit_seat = 1; 
        $(this).addClass('active');
        $('#seat-status-normal').removeClass('active');
        $('#seat-status-enable').removeClass('active');
        if(getCookie('EnableSeatCookie') != '') {
            setCookie('EnableSeatCookie', '', -1);
        }
        setCookie('DisableSeatCookie', 'on', 1);   
        clickStatusBtn();
		/*$(".modal-backdrop").css('display','');
		$(".modal-scrollable").css('display', ''); 
		$.ajax({
				type: 'POST',
				url: baseUrl + 'ajax/loadcalendar?is_seat=1',
				data: {
					date : $('#timedial').val(),
					controller : controller,
					action: action,
					is_edit_seat: is_edit_seat
				},
				success: function (response) {
					$("#workspace").replaceWith(response); 
					$("ul#btn-tab li").removeClass('active');
					if(action == "seat"){
						$("ul#btn-tab li").eq(0).addClass('active');
					}else {
						$("ul#btn-tab li").eq(1).addClass('active');
					}	
					$(".modal-backdrop").css('display','none');
					$(".modal-scrollable").css('display', 'none'); 
				}
		}); */
    }); 

	$('#seat-status-enable').click(function(){
        if ($(this).hasClass('active')) {
            return false;
        }
		var is_edit_seat = 1; 
        $(this).addClass('active');
        $('#seat-status-normal').removeClass('active');
        $('#seat-status-disable').removeClass('active');
        if(getCookie('DisableSeatCookie') != '') {
            setCookie('DisableSeatCookie', '', -1);
        }
        setCookie('EnableSeatCookie', 'on', 1);
        clickStatusBtn();
		/*$(".modal-backdrop").css('display','');
		$(".modal-scrollable").css('display', ''); 
		$.ajax({
				type: 'POST',
				url: baseUrl + 'ajax/loadcalendar?is_seat=1',
				data: {
					date : $('#timedial').val(),
					controller : controller,
					action: action,
					is_edit_seat: is_edit_seat
				},
				success: function (response) {
					$("#workspace").replaceWith(response); 
					$("ul#btn-tab li").removeClass('active');
					if(action == "seat"){
						$("ul#btn-tab li").eq(0).addClass('active');
					}else {
						$("ul#btn-tab li").eq(1).addClass('active');
					}	
					$(".modal-backdrop").css('display','none');
					$(".modal-scrollable").css('display', 'none'); 
				}
		}); */
    }); 
    
}); 
</script> 