<div id='timebar-container'></div>
<table class='table table-bordered timetable-table unselectable timebars-loading' id='res-timetable-table'>
<!--Time label -->
<thead>
    <tr>
    <?php
        $open_hour = $shop_open_time['hours'];
        $open_minute = $shop_open_time['minutes'];
        $close_hour = $shop_close_time['hours'];
        $close_minute = $shop_close_time['minutes'];
        $close_time = $shop_close_time[0];
        echo "<th class='timetable-column-stylist'>空き枠</th>";   
        for ($hour = $open_hour; $hour <= $close_hour; $hour++) {
            $m2 = '00';
            $start_m = 0;
            $end_m = 30;
            if ($hour == $open_hour) {
                $m2 = str_pad($open_minute, 2, '0', STR_PAD_LEFT);
                $start_m = $open_minute;         
            } elseif ($hour == $close_hour) {
                $end_m = $close_minute - 16;        
            }
            if ($end_m > 0) {
                echo "<th class='th-hour th-slot'>{$hour}<small>:{$m2}</small></th>";
                for ($m = $start_m; $m <= $end_m; $m+=15) {        
                    echo "<th class='th-slot'></th>";
                }
            }
        }   
    ?>
    </tr>
</thead>

<!--Seat & Device list-->
<tbody>
<?php
$timetableRow = array();
for ($seatId = 1; $seatId <= $AppUI->total_max_seat; $seatId++) {
    $timetableRow['s_' . $seatId] = array(
        'type' => 'seat', 
        'value' => $seatId, 
        'name' => '枠 ' . $seatId, 
    );
}
foreach ($deviceRecord as $value) {
    $timetableRow['d_' . $value['id']] = array(
        'type' => 'device', 
        'value' => $value['id'], 
        'name' => $value['name'],
    );
}
foreach ($timetableRow as $key => $row) {
    echo "<tr class='timetable-row' data-sty='{$key}'>";
    echo "<td class='timetable-column-stylist' rowspan='1'>{$row['name']}</td>";
    for ($hour = $open_hour; $hour <= $close_hour; $hour++) {
        $start_m = 0;
        $end_m = 45;
        if ($hour == $open_hour) {
            $start_m = $open_minute;
        } elseif ($hour == $close_hour) {
            $end_m = $close_minute - 1;
        }
        for ($m = $start_m; $m <= $end_m; $m+=15) {
            $h2 = str_pad($hour, 2, '0', STR_PAD_LEFT);
            $m2 = str_pad($m, 2, '0', STR_PAD_LEFT);  
            $class = ($m == 0 ? 'slot' : 'mid slot'); 
            if ($row['type'] == 'seat') {
                $seatId = $row['value'];
                if (!isset($order_timely_limit[$h2 . ':' . $m2])) {
                    $order_timely_limit[$h2 . ':' . $m2]['limit'] = $AppUI->shop_max_seat;
                    $order_timely_limit[$h2 . ':' . $m2]['hp_limit'] = $AppUI->shop_hp_max_seat;
                }            
                if ($seatId > $order_timely_limit[$h2 . ':' . $m2]['limit']) {
                    $class .= ' disabled';
                }
            }
            if (strtotime($dt." {$h2}:{$m2}:00") <= $close_time) {
                echo "<td class='{$class}' data-t='" . strtotime($dt." {$h2}:{$m2}:00") . "'></td>";
            }
        }
    }
    echo "</tr>";
}                
?>
</tbody>
</table>

<script type="text/javascript">           
    <?php
    $start_at_epoch_time = str_pad($open_hour, 2, '0', STR_PAD_LEFT) . ":" . str_pad($open_minute, 2, '0', STR_PAD_LEFT) . ":00";
    $last_order_at_epoch_time = str_pad(($close_hour - 1), 2, '0', STR_PAD_LEFT) . ":00:00";
    $end_at_epoch_time = str_pad($close_hour, 2, '0', STR_PAD_LEFT) . ":" . str_pad($close_minute, 2, '0', STR_PAD_LEFT) . ":00";
    ?>
    window.reservations = <?=$data?>; 
    window.blockages = [];
    window.sheet_set = {
        "is_override":null,
        "is_closed":null,
        "all_day":{
            "start_at_epoch":<?= strtotime($dt." {$start_at_epoch_time}") ?>,
            "last_order_at_epoch":<?= strtotime($dt." {$last_order_at_epoch_time}") ?>,
            "end_at_epoch":<?= strtotime($dt." {$end_at_epoch_time}") ?>
        },
        "breakfast":null,
        "lunch":null,
        "tea":null,
        "dinner":null,
        "night":null
    };
    window.i18n_confirm_time_change = "開始時間が変更されます。よろしいですか？";
    window.reservation_overbook = true;  
    $(function(){
        setTimeout(function(){Timetable.draw()}, 50);    
    });  
</script>