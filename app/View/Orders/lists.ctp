<div class='navbar navbar-static-top subnav' id='customers-subnav'>
    <div class='navbar-inner'>
        <div class='pull-right'>
            <!-- <a href="javascript:void(0)" class="btn btn-success" id="btn-create-user" data-modal="true" data-remote="true">顧客を作成する</a> -->
        </div>
        <div class='subnav-form' id='cust-query'>
            <form action="<?php echo($this->Html->url('/orders/lists')); ?>" role="form" enctype="multipart/form-data" id="ShopCustomerForm" method="get" accept-charset="utf-8">
                <span class="search-customer">
                    <select name="nailist_id" class="form-control">
                        <option value><?php echo __('All nailists'); ?></option>
                        <option value=-1 <?php echo($nailist_id == -1 ? 'selected' : '')?>><?php echo __('No nailist'); ?></option>
                        <?php foreach ($nailists as $item):?>
                            <option value="<?php echo $item['id'];?>" <?php echo($item['id'] == $nailist_id ? 'selected' : '')?>><?php echo $item['name']; ?></option>
                        <?php endforeach;?>
                    </select>
                </span>
                <input class='form-control search-query' name="keyword"  id="cust-q" value="<?php echo $keyword; ?>" placeholder='名前、電話番号、またはEメール' type="text">
                <input name="date" value="<?php echo $hidden_date;?>" type="hidden">
                <div class='btn-group'>
                    <input value="<?php echo __('Search'); ?>"  type="submit" class="btn">
                </div>
                <span class='x16-loader invisible' id='cust-loading'></span>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
        <?php
            echo $this->SimpleTable->render($table);
        ?>        
        <script type="text/javascript">
            // Check tr end of status ==>
            var _status = 0;
            var _Oldstatus = 0;
            var _showbreak = 0;
            $.each($('form table.table tr:not(:first-child)'), function(ind, obj){ //:not(:last-child)
                _status = $(obj).find('a.o_status').attr('data-class');
                _showbreak = $(obj).find('a.o_status').attr('show-break');
                _Oldstatus = ind == 0 ?  _status : _Oldstatus ;
                if((_status != _Oldstatus) && (_showbreak == 1)){
                    $(obj).addClass('breakRow');
                    _Oldstatus = _status;
                }
            });
        </script>
        </div>
    </div>
    <a style="margin-left:15px;" href="<?php echo $this->html->url('/orders/listcsv'). "?{$exportUrl}"?>" target="_blank" class="btn btn-primary"><?php echo __('csv'); ?></a>
    <a href="<?php echo $this->html->url('/orders/listxls') ?>?<?php echo $exportUrl ?>" target="_blank" class="btn btn-primary"><?php echo __('xls'); ?></a>
    <a href="<?php echo $this->html->url('/orders/listxlsx') ?>?<?php echo $exportUrl ?>" target="_blank" class="btn btn-primary"><?php echo __('xlsx'); ?></a>  
</div>