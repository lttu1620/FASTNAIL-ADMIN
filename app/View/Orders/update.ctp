<div class="row">
    <div class="col-xs-120 ">
        <div class="box box-primary">
            <div class="box-body">
            <?php
                echo $this->SimpleForm->render($updateForm);
            ?>
            </div>
        </div>
     </div>
</div>

<?php if (!empty($nailTable['dataset'])) : ?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo __('Nail information'); ?></h3>
            </div>
            <?php
                echo $this->SimpleTable->render($nailTable);
            ?>
        </div>
    </div>
</div>
<?php endif ?>

<br />

<?php if (!empty($itemTable['dataset'])) : ?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo __('Item information'); ?></h3>
            </div>
            <?php
                echo $this->SimpleTable->render($itemTable);
            ?>
        </div>
    </div>
</div>
<?php endif ?>