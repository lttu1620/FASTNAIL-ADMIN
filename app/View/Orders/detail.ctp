<div class="row">
    <!-- User information -->
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('User information'); ?></h3>
            </div>
            <div class="box-body">
                <dl class="dl-horizontal">
                    <?php
                    $order['status'] = 0;
                    $user_information = array(
                        'user_id'
                        , 'user_code'
                        , 'user_name'
                        , 'kana'
                        , 'sex'
                        , 'birthday'
                        , 'email'
                        , 'phone'
                        , 'address'
                            );
                    $properties = array_keys($order);
                    foreach ($user_information as $user) {
                        if (in_array($user, $properties) && (isset($order[$user]) && $order[$user] != '')) {
                            switch ($user) {
                                case 'user_id':
                                    ?>
                                    <dt><?php echo __('User ID') ?></dt>
                                    <?php break;
                                case 'user_code':
                                    ?>
                                    <dt><?php echo __('Code') ?></dt>
                                    <?php break;
                                case 'user_name':
                                    ?>
                                    <dt><?php echo __('User name') ?></dt>
                                    <?php break;
                                case 'kana':
                                    ?>
                                    <dt><?php echo __('Kana') ?></dt>
                                    <?php break;
                                case 'sex':
                                    ?>
                                    <dt><?php echo __('Gender') ?></dt>
                                    <?php break;
                                case 'birthday':
                                    ?>
                                    <dt><?php echo __('Birthday') ?></dt>
                                    <?php break;
                                case 'email':
                                    ?>
                                    <dt><?php echo __('Email') ?></dt>
                                    <?php break;
                                case 'phone':
                                    ?>
                                    <dt><?php echo __('Phone') ?></dt>
                                        <?php break;
                                    case 'address':
                                        ?>
                                    <dt><?php echo __('Address') ?></dt>
                                        <?php
                                        break;
                                }
                                ?>
                            <dd><?php
                    if ($user == 'sex') {
                        $order[$user] = Configure::read('Config.searchGender')[$order[$user]];
                    } elseif ($user == 'birthday') {
                        $order[$user] = $this->Common->dateFormat($order[$user]);
                    }
                    echo $order[$user];
                    ?>
                            </dd>
    <?php }
}
?>
            </div>
        </div>
    </div>
    <!-- Order information -->
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Order information'); ?></h3>
                <?php if(!empty($order['id'])):?>
                <span style="float: right;"><a class="btn btn-primary" href="javascript:void(0);" onclick="javascript:openMediacal(<?php echo $order['id'];?>);">カルテ表示</a></span>
                <?php endif;?>
            </div>
            <div class="box-body">
                <dl class="dl-horizontal">
                    <?php
                    $order_information = array(
                        'id'
                        , 'total_price'
                        , 'total_tax_price'
                        , 'total_sell_price'
                        , 'pay_type'
                        , 'reservation_type'
                        , 'reservation_date'
                        , 'order_time'
                        , 'nail_off_time'
                        , 'servce_time'
                        , 'off_nails'
                        , 'hf_section'
                        , 'nail_length'
                        , 'nail_add_length'
                        , 'nail_type'
                        , 'problem'
                        , 'request'
                        , 'service'
                        , 'is_mail'
                        , 'is_designate'
                        , 'is_next_reservation'
                        , 'welfare_id'
                        , 'visit_section'
                        , 'visit_element'
                        , 'shop_name'
                        , 'seat_id'
                        , 'nailist'
                        , 'device'
                        , 'rating'
                        , 'is_cancel'
                        , 'is_autocancel'
                        , 'shop_counter_code'
                    );
                    foreach ($order_information as $information) {
                        if (in_array($information, $properties) && (isset($order[$information]) && $order[$information] != '')) {

                            switch ($information) {
                                case 'id':
                                    ?>
                                    <dt><?php echo __('Order ID') ?></dt>
                                    <?php break;
                                case 'total_price':
                                    ?>
                                    <dt><?php echo __('Total price') ?></dt>
                                    <?php break;
                                case 'total_tax_price':
                                    ?>
                                    <dt><?php echo __('Total tax price') ?></dt>
                                    <?php break;
                                case 'total_sell_price':
                                    ?>
                                    <dt><?php echo __('Sell price') ?></dt>
                                    <?php break;
                                case 'pay_type':
                                    ?>
                                    <dt><?php echo __('Pay type') ?></dt>
                                    <?php break;
                                case 'reservation_type':
                                    ?>
                                    <dt><?php echo __('Reservation type') ?></dt>
                                    <?php break;
                                case 'reservation_date':
                                    ?>
                                    <dt><?php echo __('Reservation date') ?></dt>
                                    <?php break;
                                case 'order_time':
                                    ?>
                                    <dt><?php echo __('Order time') ?></dt>
                                    <?php break;
                                case 'nail_off_time':
                                    ?>
                                    <dt><?php echo __('Nailoff time') ?></dt>
                                    <?php break;
                                case 'servce_time':
                                    ?>
                                    <dt><?php echo __('Service time') ?></dt>
                                    <?php break;
                                case 'off_nails':
                                    ?>
                                    <dt><?php echo __('Off nails') ?></dt>
                                    <?php break;
                                case 'hf_section':
                                    ?>
                                    <dt><?php echo __('Hf section') ?></dt>
                                    <?php break;
                                case 'nail_length':
                                    ?>
                                    <dt><?php echo __('Nail length') ?></dt>
                                    <?php break;
                                case 'nail_add_length':
                                    ?>
                                    <dt><?php echo __('Nail add length') ?></dt>
                                    <?php break;
                                case 'nail_type':
                                    ?>
                                    <dt><?php echo __('Nail type') ?></dt>
                                    <?php break;
                                case 'problem':
                                    ?>
                                    <dt><?php echo __('Problem') ?></dt>
                                    <?php break;
                                case 'is_mail':
                                    ?>
                                    <dt><?php echo __('Is mail') ?></dt>
                                    <?php break;
                                case 'is_designate':
                                    ?>
                                    <dt><?php echo __('Is designate') ?></dt>
                                    <?php break;
                                case 'is_next_reservation':
                                    ?>
                                    <dt><?php echo __('Is next reservation') ?></dt>
                                        <?php break;
                                    case 'is_next_designate':
                                        ?>
                                    <dt><?php echo __('Is next designate') ?></dt>
                                        <?php break;
                                    case 'visit_section':
                                        ?>
                                    <dt><?php echo __('Visit section') ?></dt>
                                        <?php break;
                                    case 'visit_element':
                                        ?>
                                    <dt><?php echo __('Visit element') ?></dt>
                                        <?php break;
                                    case 'shop_name':
                                        ?>
                                    <dt><?php echo __('Shop name') ?></dt>
                                        <?php break;
                                    case 'nailist':
                                        ?>
                                    <dt><?php echo __('Nailists') ?></dt>
                                        <?php break;
                                    case 'device':
                                        ?>
                                    <dt><?php echo __('Devices') ?></dt>
                                        <?php break;
                                    case 'rating':
                                        ?>
                                    <dt><?php echo __('rating') ?></dt>
                                        <?php break;
                                    case 'is_cancel':
                                        ?>
                                    <dt><?php echo __('Cancel order');  $order['status'] = $order['is_cancel'];?></dt>
                                        <?php break;
                                    case 'welfare_id':
                                        ?>
                                    <dt><?php echo __('welfare') ?></dt>
                                        <?php
                                        break;
                                    case 'is_autocancel':
                                        ?>
                                    <dt><?php echo __('Autocancel') ?></dt>
                                        <?php
                                        break;
                                    case 'seat_id':
                                        ?>
                                    <dt><?php echo __('Seat ID') ?></dt>
                                        <?php
                                        break;
                                    case 'request':
                                        ?>
                                    <dt><?php echo __('Request') ?></dt>
                                        <?php
                                        break;
                                    case 'service':
                                        ?>
                                    <dt><?php echo __('Service') ?></dt>
                                        <?php
                                        break;
                                    case 'shop_counter_code':
                                        ?>
                                    <dt><?php echo '整理番号' ?></dt>
                                        <?php
                                        break;
                                }
                                ?>
                            <dd <?php echo ($information == 'rating' || $information == 'is_cancel' ? 'style=color:red;' : '') ?>><?php
                                switch ($information) {
                                    case 'off_nails':
                                        if($order[$information] == 0){
                                            $order[$information] = '-';
                                        }else{
                                            $order[$information] = Configure::read('Config.NailOff')[$order[$information]];
                                        }
                                        break;
                                    case 'is_mail':
                                    case 'is_designate':
                                    case 'is_next_reservation':
                                    case 'is_next_designate':
                                        $order[$information] = Configure::read('Config.BooleanValue')[$order[$information]];
                                        break;
                                    case 'hf_section':
                                        $order[$information] = Configure::read('Config.hfSection')[$order[$information]];
                                        break;
                                    case 'nail_type':
                                        if($order[$information] == 0){
                                            $order[$information] = '-';
                                        }else{
                                            $order[$information] = Configure::read('Config.NailType')[$order[$information]];
                                        }
                                        break;
                                    case 'pay_type':
                                        $order[$information] = Configure::read('Config.PayType')[$order[$information]];
                                        break;
                                    case 'visit_section':
                                        $order[$information] = Configure::read('Config.VisitSection')[$order[$information]];
                                        break;
                                    case 'visit_element':
                                        $order[$information] = Configure::read('Config.VisitElement')[$order[$information]];
                                        break;
                                    case 'reservation_type':
                                        $order[$information] = Configure::read('Config.ReservationType')[$order[$information]];
                                        break;
                                    case 'reservation_date':
                                        $order[$information] = $this->Common->dateFormat($order[$information]);
                                        break;
                                    case 'rating':
                                        $order[$information] = str_replace('rating_1', '★', $order[$information]);
                                        $order[$information] = str_replace('rating_2', '★★', $order[$information]);
                                        $order[$information] = str_replace('rating_3', '★★★', $order[$information]);
                                        $order[$information] = str_replace('rating_4', '★★★★', $order[$information]);
                                        $order[$information] = str_replace('rating_5', '★★★★★', $order[$information]);
                                        break;
                                    case 'nail_length':
                                        if($order[$information] == 0){
                                            $order[$information] = '-';
                                        }else{
                                            $order[$information] = Configure::read('Config.NailLength')[$order[$information]];
                                        }
                                        break;
                                    case 'nail_add_length':
                                        $order[$information] = Configure::read('Config.FingerItemLength')[$order[$information]];
                                        break;
                                    case 'total_price':
                                    case 'total_tax_price':
                                    case 'total_sell_price':
                                        $order[$information] = $this->Common->moneyFormat($order[$information]);
                                        break;
                                    case 'is_cancel':
                                    case 'is_autocancel':
                                        $order[$information] = Configure::read('Config.BooleanValue')[$order[$information]];
                                        break;
                                    case 'service':
                                        if($order[$information] == 0){
                                            $order[$information] = '-';
                                        }else{
                                            $order[$information] = Configure::read('Config.OrderService')[$order[$information]];
                                        }
                                        break;
                                }
                                echo$order[$information];
                                ?>
                            </dd>
                <?php }
            }
            ?>
            </div>
        </div>
    </div>

    <!-- Nail information -->
<?php if (!empty($order['order_nails'])) { ?>
        <div class="col-xs-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Nail information'); ?></h3>
                </div>
        <?php
        echo $this->SimpleTable->render($order_nails);
        ?>
            </div>
        </div>
<?php } ?>

    <!-- Item information -->
<?php if (!empty($order['order_items'])) { ?>
        <div class="col-xs-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Item information'); ?></h3>
                </div>
    <?php
    echo $this->SimpleTable->render($order_items);
    ?>
            </div>
        </div>
<?php } ?>

    <!-- Log information -->
<?php if (!empty($order['order_nailist_logs'])) { ?>
        <div class="col-xs-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Log information'); ?></h3>
                </div>
    <?php
    echo $this->SimpleTable->render($order_nailist_logs);
    ?>
            </div>
        </div>
<?php } ?>
<?php if (!empty($order['id'])) : ?>
        <div class="col-md-8">
            <div class="submit">
                <a class="btn btn-primary" href="<?php echo($this->Html->url('/orders/update/' . $order['id'])) ?>"><?php echo __('Update order') ?></a>
            </div>
            <div class="submit"><input data-id="<?php echo $order['id']; ?>" data-status="<?php echo $order['status']; ?>" value="<?php echo $order['status'] == 0 ? __('Cancel order') : __('Restore order'); ?>" class="btn btn-primary btn-cancelOrder" type="submit"></div>
        </div>
<?php endif; ?>
</div>