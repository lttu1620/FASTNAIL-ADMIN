<table class="calendar_control" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="40%" align="left">
            <a href="?date=<?php echo $info['firstDayOfLastMonth']; ?>">
                « <?php echo $info['lastMonth']; ?>月<?php echo $info['yearOfLastMonth']; ?>
            </a>
        </td>
        <td width="20%" align="center">
            <center><strong>
                <?php echo $info['thisMonth']; ?>月<?php echo $info['thisYear']; ?>
            </strong></center>
        </td>
        <td width="40%" align="right">
            <a href="?date=<?php echo $info['firstDayOfNextMonth']; ?>" style="float:right;">
                <?php echo $info['nextMonth']; ?>月<?php echo $info['yearOfNextMonth']; ?> »
            </a>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" class="table table-bordered calendar-table">
    <tbody>
    <tr class="calendar-row">
        <th class="calendar-day-head">日</th>
        <th class="calendar-day-head">月</th>
        <th class="calendar-day-head">火</th>
        <th class="calendar-day-head">水</th>
        <th class="calendar-day-head">木</th>
        <th class="calendar-day-head">金</th>
        <th class="calendar-day-head">土</th>
    </tr>
    <?php foreach ($data as $key => $item) {
        if ($key % 7 == 0) { ?>
            <tr class="calendar-row">
        <?php } ?>

        <td class=" <?php
            if ($item['dateInfo']['isThisMonth']) 
                echo 'calendar-day';
            else 
                echo 'calendar-day-np';
            if ($item['dateInfo']['isSunday']) echo ' sun';
            if ($item['dateInfo']['isToday']) echo ' today'; ?>">
            <div class="day-number">
                <a href="<?php echo $this->Html->url('/orders/lists') . '?date-' . $item['dateInfo']['date'] ?>">
                <?php echo $item['dateInfo']['day'] ?>
                </a>                
            </div>             
            <?php if (isset($item['dateData'])) {
                for ($i = 0; $i < 3; $i++) {
                    if (isset($item['dateData'][$i])) { ?>
                        <div class="day-data">
                            <a target="_blank" href="<?php echo $this->Html->url('/orders/detail') . '/' . $item['dateData'][$i]['id'] ?>">
                            <?php
                                $name = !empty($item['dateData'][$i]['user_name']) ? $item['dateData'][$i]['user_name'] : '#' . $item['dateData'][$i]['id'];
                                //$name = mb_strlen($name) > 5 ? mb_substr($name, 0, 5) . '...' : $name;
                                echo date('H:i', strtotime($item['dateData'][$i]['date'])) . ' ' . $name;
                            ?>
                            </a>
                        </div>
                    <?php }
                }
                if (count($item['dateData']) > 3) { ?>
                    <div class="day-data">
                        <a href="lists?date=<?php echo $item['dateInfo']['date'] ?>"><?php echo __('...')?></a>
                    </div>
                <?php }
            } ?>
        </td>
        <?php if (($key + 1) % 7 == 0) { ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>