<?php

echo $this->element('calendar', array(
        'data' => $data,
        'dt' => $dt,
        'deviceRecord' => $deviceRecord,
        'max_seat' => $max_seat,
        'shop_open_time' => $shop_open_time,
        'shop_close_time' => $shop_close_time,
        'AppUI' => $AppUI
    )
);
