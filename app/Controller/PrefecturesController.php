<?php

App::uses('AppController', 'Controller');

/**
 * Prefectures Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class PrefecturesController extends AppController {

    /**
     * Construct
     * 
     * @author VuLTH 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author VuLTH 
     * @return void
     */
    public function index() {
        
        include ('Prefectures/index.php');
    }
    
    /**
     * update action
     * 
     * @author VuLTH 
     */
    public function update($id = 0) {
        include ('Prefectures/update.php');
    }
}
