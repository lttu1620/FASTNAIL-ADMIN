<?php

$modelName = $this->Claim->name;
$model = $this->{$modelName};
$data = array();

$listShop = MasterData::shops_all();
$listShop = $this->Common->arrayKeyValue($listShop, 'id', 'name');

$listNailist = MasterData::nailists_all();
$listNailist = $this->Common->arrayKeyValue($listNailist, 'id', 'name');

$listUser = MasterData::users_all();
$listUser = $this->Common->arrayKeyValue($listUser, 'id', 'first_name');

$pageTitle = __('Add Claim');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_claims_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Claim unavailable", __METHOD__, $param);
        throw new NotFoundException("Claim unavailable", __METHOD__, $param);
    }
    $pageTitle = __('Edit Claim');
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));
// create Update form 
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id'    => 'id',
        'type'  => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id'      => 'user_id',
        'label'   => __('User'),
        'options' => $listUser,
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'       => 'nailist_id',
        'label'    => __('Nailist'),
        'options'  => $listNailist,
        'empty'    => Configure::read('Config.StrChooseOne'),
        'required' => true
    ))
    ->addElement(array(
        'id'      => 'shop_id',
        'label'   => __('Shop'),
        'options' => $listShop,
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'    => 'order_id',
        'label' => __('Order Id'),
        'type'  => 'textarea'
    ))
    ->addElement(array(
        'id'    => 'content',
        'type'  => 'textarea',
        'label' => __('Content'),
        'rows'  => 12
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type'    => 'submit',
        'value'   => __('Cancel'),
        'class'   => 'btn btn-primary pull-left',
        'onclick' => 'return back();'
    ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        $id = Api::call(Configure::read('API.url_claims_addupdate'), $data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not update claim", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
    } else {
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}