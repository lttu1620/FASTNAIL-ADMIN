<?php

$modelName = $this->Userboughtitem->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('User bought item List');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'    => 'user_customid',
            'type'  => 'text',
            'label' => __('User ID'),
        )
    )
    ->addElement(
        array(
            'id'    => 'user_name',
            'type'  => 'text',
            'label' => __('User name'),
        )
    )
    ->addElement(
        array(
            'id'    => 'point_item_customid',
            'type'  => 'text',
            'label' => __('Point item ID'),
        )
    )
    ->addElement(
        array(
            'id'    => 'point_item_name',
            'type'  => 'text',
            'label' => __('Point item name'),
        )
    )
    ->addElement(
        array(
            'id'      => 'status',
            'label'   => __('Status'),
            'options' => Configure::read('Config.BoughtItemType'),
            'empty'   => __('All'),
        )
    )
    ->addElement(
        array(
            'id'      => 'sort',
            'label'   => __('Sort'),
            'options' => array(
                'id-asc'       => __('ID Asc'),
                'id-desc'      => __('ID Desc'),
                'created-asc'  => __('Created Asc'),
                'created-desc' => __('Created Desc'),
            ),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );

// call api to query data
$param = $this->getParams(
    array(
        'page'   => 1,
        'limit'  => Configure::read('Config.pageSize'),
        'status' => 0,
    )
);
if (isset($param['user_customid'])) {
    $param['user_id'] = $param['user_customid'];
}
if (isset($param['point_item_customid'])) {
    $param['point_item_id'] = $param['point_item_customid'];
}
list($total, $data) = Api::call(Configure::read('API.url_user_bought_item_list'), $param, false, array(0, array()));
$this->set('total', $total);
$this->set('limit', $param['limit']);
// create data table
$this->SimpleTable
    ->setDataset($data)
    ->addColumn(
        array(
            'id'    => 'id',
            'title' => __('ID'),
            'width' => 30,
        )
    )
    ->addColumn(
        array(
            'id'    => 'user_id',
            'title' => __('User ID'),
            'empty' => '',
            'width' => 110,
        )
    )
    ->addColumn(
        array(
            'id'    => 'user_name',
            'title' => __('User name'),
            'empty' => '',
            'width' => 120,
        )
    )
    ->addColumn(
        array(
            'id'    => 'point_item_name',
            'title' => __('Point item name'),
            'empty' => '',
        )
    )
    /*
    ->addColumn(
        array(
            'id'    => 'quantity',
            'title' => __('Quantity'),
            'empty' => '',
            'width' => 60,
            'type'  => 'number',
        )
    )
    ->addColumn(
        array(
            'id'    => 'point',
            'title' => __('Point'),
            'empty' => '',
            'width' => 100,
            'type'  => 'number',
        )
    )
    * 
    */
    ->addColumn(
        array(
            'id'    => 'total_point_text',
            'title' => __('Total point'),
            'empty' => '',
            'width' => 140,
           
        )
    )
    ->addColumn(
        array(
            'id'    => 'read_date',
            'title' => __('Scan QR'),
            'type'  => 'date',
            'width' => 140,
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'completed',
            'title' => '完了日',
            'type'  => 'date',
            'width' => 140,
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'canceled',
            'title' => 'キャンセル日',
            'type'  => 'date',
            'width' => 140,
            'empty' => '',
        )
    )    
    ->addColumn(
        array(
            'id'    => 'created',
            'title' => __('Created'),
            'type'  => 'date',
            'width' => 140,
        )
    );

if (in_array($param['status'], array(0, 1, 4))) { // all, Bought items, Scaned qr items, Not scan qr items
    $this->SimpleTable->addColumn(
        array(
            'id'    => 'is_completed',
            'title' => __('Approve bought item'),
            'rules' => array(
                '0' => '<a onclick="approve_bought_item({id}, {set_completed});return false;" width="90" href="#"><span class="label label-primary">'.__(
                        'Approve bought item'
                    ).'</span></a>',
                '1' => '',
            ),
            'width' => 50,
        )
    );
}

if (in_array($param['status'], array(0, 1, 4, 5))) { // all, Bought items, Scaned qr items, Not scan qr items
    $this->SimpleTable->addColumn(
        array(
            'id'    => 'is_canceled',
            'title' => __('Cancel'),
            'rules' => array(
                '0' => '<a onclick="cancel_bought_item({id}, {set_canceled});return false;" width="90" href="#"><span class="label label-primary">'.__(
                        'Cancel'
                    ).'</span></a>',
                '1' => '',
            ),
            'width' => 90,
        )
    );
}