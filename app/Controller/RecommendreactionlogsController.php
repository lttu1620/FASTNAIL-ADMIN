<?php

App::uses('AppController', 'Controller');

/**
 * ItemsController class of Recommend Reaction Log Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class RecommendreactionlogsController extends AppController
{
    /**
     * Initializes components for ItemsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Recommend Reaction Log.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Recommendreactionlogs/index.php');
    }
}
