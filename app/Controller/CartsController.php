<?php

App::uses('AppController', 'Controller');

/**
 * Orders Controller
 *
 * @package Controller
 * @created 2015-04-03
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class CartsController extends AppController {

    /**
     * Construct
     *
     * @author Tran Xuan Khoa
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     *
     * @author Tran Xuan Khoa
     * @return void
     */
    public function index() {
        include ('Carts/index.php');
    }

    /**
     * update action
     *
     * @author Tran Xuan Khoa
     */
    public function update($cart_id = 0) {
        include ('Carts/update.php');
    }

}
