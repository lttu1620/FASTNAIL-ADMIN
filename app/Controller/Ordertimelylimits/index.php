<?php
$this->layout = 'full';
// set date for calendar
if (isset($this->request->query['date'])) {
    $dt = $this->request->query['date'];
} else {
    $dt = date('Y-m-d');
}
$this->set('dt', $dt);
// Create breadcrumb
$pageTitle = __('Order timely limit');
$date = date('Y-m-d');
if (isset($_REQUEST['date'])) {
    $dt = \DateTime::createFromFormat('Y-m-d', $_REQUEST['date']);
    if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
        return $this->redirect("/{$this->controller}?date={$date}");
    } else {
        $date = $_REQUEST['date'];
    }
}
$shopId = !empty($this->AppUI->shop_id) ? $this->AppUI->shop_id : 0;

$shopDetail = $arrTimeGrid = array();
if ($shopId) {
    $shopDetail = Api::Call(Configure::read('API.url_shops_detail'), array('id' =>  $shopId));
    if (!(isset($shopDetail['status']) &&  $shopDetail['status'] == 400)) {
        $openH = floor($shopDetail['open_time'] / (60 * 60));
        $openM = ($shopDetail['open_time'] - $openH * 60 * 60) /60;
        $openTime = str_pad($openH, 2, 0, STR_PAD_LEFT) . ':' . str_pad($openM, 2, 0, STR_PAD_LEFT);

        $closeH = floor($shopDetail['close_time'] / (60 * 60));
        $closeM = ($shopDetail['close_time'] - $closeH * 60 * 60) /60;
        $closeTime = str_pad($closeH, 2, 0, STR_PAD_LEFT) . ':' . str_pad($closeM, 2, 0, STR_PAD_LEFT);

        $this->set(compact('openTime', 'closeTime'));

        $arrH = range ($openH, $closeH);
        foreach ($arrH as $h) {
            $arrMinutes = range(0, 45, $shopDetail['reservation_interval']);
            foreach ($arrMinutes as $k => $m) {
                $arrMinutes[$k] = str_pad($m, 2, 0, STR_PAD_LEFT);
            }
            $arrTimeGrid[str_pad($h, 2, 0, STR_PAD_LEFT)] = $arrMinutes;
        }

    } else {
        echo 'Shop doesn\'t exist';
        exit();
    }
}

list($count, $data) = Api::Call(Configure::read('API.url_order_timely_limit_list'),
    array(
        'shop_id' => $shopId,
        'date'    => $date
    ));

$this->set(compact('shopId', 'arrTimeGrid', 'data', 'date'));
$this->set('defaultMaxSeat', $shopDetail['max_seat']);
$this->set('defaultHpMaxSeat', $shopDetail['hp_max_seat']);
$this->Common->handleException(Api::getError());
$this->setPageTitle($pageTitle);
$this->Breadcrumb
    ->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));

$postData = array();
if ($this->request->is('post') && !empty($_POST['data'])) {
    $postData = $_POST['data'];    
    $id = Api::Call(Configure::read('API.url_order_timely_limit_multiupdate'), array('shop_id' =>  $shopId, 'data_json' => json_encode($postData)));
    if (!empty($id) && !Api::getError()) {
        //$this->Common->setFlashSuccessMessage(__('Data saved successfully'));
        echo json_encode(array('msg' => __('Data saved successfully')));
        exit;
    } else {
        return $this->Common->setFlashErrorMessage(Api::getError());       
        exit;
    }
}

