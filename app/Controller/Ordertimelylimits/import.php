<?php
error_reporting(0);
App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel' . DS . 'PHPExcel.php'));
App::import('Vendor', 'PHPExcel_IOFactory', array('file' => 'PHPExcel' . DS . 'IOFactory.php'));
App::import('Vendor', 'PHPExcel_Shared_Date', array('file' => 'PHPExcel' . DS . 'Shared' . DS . 'Date.php'));
$pageTitle = __('Import');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
$shopId = !empty($this->AppUI->shop_id) ? $this->AppUI->shop_id : 0;
if ($this->request->is('post') 
    && isset($_FILES['import']) 
    && $_FILES['import']['tmp_name']) {
    if (!$_FILES['import']['error']) {              
        $data = array();       
        try {         
            $inputFile = $_FILES['import']['tmp_name']; 
            $inputFileType = PHPExcel_IOFactory::identify($inputFile); 
            $objReader = PHPExcel_IOFactory::createReader($inputFileType); 
            $objPHPExcel = $objReader->load($inputFile);            
            foreach ($objPHPExcel->getWorksheetIterator() as $index => $sheet) { 
                if ($index >= 1) {
                    //continue;
                }
                $rowTime = array();
                $rowData = array();
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();               
                $shopId = $sheet->getCell('B2')->getOldCalculatedValue();
                if (empty($shopId)) {
                    $shopId = $sheet->getCell('B2')->getValue();
                }                
                $shopName = $sheet->getCell('B3')->getOldCalculatedValue(); 
                if (empty($shopName)) {
                    $shopName = $sheet->getCell('B3')->getValue();
                }
                if (empty($shopId)) {
                    $shopId = $sheet->getCell('B2')->getValue();
                }
                if (empty($shopId)) {
                    return $this->Common->setFlashErrorMessage('アカウントに関連付けられた店舗のみ空き枠を設定できます。');
                }
                if ($shopId != $this->AppUI->shop_id) {
                    return $this->Common->setFlashErrorMessage('アカウントに関連付けられた店舗のみ空き枠を設定できます。');
                }
                $date = $sheet->getCell('B4')->getOldCalculatedValue();
                if (empty($date)) {                   
                    $date = $sheet->getCell('B4')->getValue();                   
                }
                if (empty($date)) {                   
                    return $this->Common->setFlashErrorMessage('Date is invalid');
                }
                for ($row = 1; $row <= $highestRow; $row++) {
                    if (!empty($rowTime)) {
                        $rowData = array_merge($rowData, $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, true));
                    }
                    if ($sheet->getCellByColumnAndRow('A', $row)->getValue() == '合計予約枠数') {
                        $rowTime = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, true)[0];
                    }
                }            
                
                // parse data for insert/update       
                $date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($date)); 
                for ($i = 2; $i < count($rowTime); $i++) {
                    $item['time'] = $date . ' ' . $rowTime[$i];
                    if (isset($rowData[0][$i])) {
                        $item['limit'] = $rowData[0][$i];
                    }
                    if (isset($rowData[1][$i])) {
                        $item['hp_limit'] = $rowData[1][$i];
                    }   
                    if (!is_numeric($item['limit'])) {
                        $item['limit'] = 0;
                    }
                    if (!is_numeric($item['hp_limit'])) {
                        $item['hp_limit'] = 0;
                    }
                    $data[] = $item;                    
                    if ($i > 2 && (strtotime($item['time']) - strtotime($date . ' ' . $rowTime[$i - 1])) == 30*60) {
                        $item['time'] = date('Y-m-d H:i', strtotime($item['time']) - 15*60);
                        $item['limit'] = $rowData[0][$i - 1];
                        $item['hp_limit'] = $rowData[1][$i - 1];
                        if (!is_numeric($item['limit'])) {
                            $item['limit'] = 0;
                        }
                        if (!is_numeric($item['hp_limit'])) {
                            $item['hp_limit'] = 0;
                        }
                        $data[] = $item; 
                    }                  
                }
            }
           
            // can not read data
            if (empty($data)) {
                $this->Common->setFlashErrorMessage('Excelファイルが破損しています。');
                return $this->redirect("/{$this->controller}/import");
            }
            $id = Api::Call(
                Configure::read('API.url_order_timely_limit_multi_update'),
                array(
                    'shop_id' => intval($shopId),
                    'data_json' => json_encode($data)
                )
            );
            if (!empty($id) && !Api::getError()) {
                $this->Common->setFlashSuccessMessage(__('Data imported successfully'));
                return $this->redirect("/{$this->controller}/import");
            } else {               
                $this->Common->setFlashErrorMessage(Api::getError());
                return $this->redirect("/{$this->controller}/import");
            }                       
        } catch (Exception $e) {
            $this->Common->setFlashErrorMessage($e->getMessage());
            return $this->redirect("/{$this->controller}/import");
        }
    } else { d($e, 1);
        $this->Common->setFlashErrorMessage($_FILES['import']['error']);
        return $this->redirect("/{$this->controller}/import");
    }
}