<?php

App::uses('AppController', 'Controller');

/**
 * Nailists Controller
 * 
 * @package Controller
 * @created 2015-03-25
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class NailistsController extends AppController {

    /**
     * Construct
     * 
     * @author diennvt 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author diennvt 
     * @return void
     */
    public function index() {
        include ('Nailists/index.php');
    }

    /**
     * update action
     * 
     * @author diennvt 
     */
    public function update($nailist_id = 0) {
        include ('Nailists/update.php');
    }
}
