<?php

App::uses('AppController', 'Controller');

/**
 * OrderupdatetimebarlogsController class of User Beauty View Log Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderupdatetimebarlogsController extends AppController
{
    /**
     * Initializes components for OrderupdatetimebarlogsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Orderupdatetimebarlogs.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Orderupdatetimebarlogs/index.php');
    }
}
