<?php

$modelName = $this->User->name;

$param = $this->getParams(array('page' => 1, 'limit' => 10000));
list($total, $data) = Api::call(Configure::read('API.url_users_list'), $param, false, array(0, array()));

$fileName = 'users-' . date('YmdHis') . '.csv';
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename={$fileName}");
header("Pragma: no-cache");
header("Expires: 0");
$genders = Configure::read('Config.searchGender');
$result = __('ID') . ','
        . __('Code') . ','
        . __('Email') . ','
        . __('Name') . ','
        . __('Kana') . ','
        . __('Gender') . ','
        . __('Phone') . ','
        . __('Birthday') . ','
        . __('Created') . "\n";
foreach ($data as $value) {
    $birthday = $this->Common->dateFormat($value['birthday']) != false ? $this->Common->dateFormat($value['birthday']) : '';
    $created = $this->Common->dateFormat($value['created']) != false ? $this->Common->dateFormat($value['created']) : '';
    $gender = isset($genders[$value['sex']]) ? $genders[$value['sex']] : '';
    $result.= $value['id'] . ','
            . $value['code'] . ','
            . $value['email'] . ','
            . $value['name'] . ','
            . $value['kana'] . ','
            . $gender . ','
            . $value['phone'] . ','
            . $birthday . ','
            . $created . "\n";
}
echo $result;
exit;