<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();

// Create breadcrumb 
$isUpdate = false;
if (!empty($user_id)) {
    $pageTitle = 'ユーザ情報変更';
    $isUpdate = true;
    $param['id'] = $user_id;
    $data[$modelName] = Api::Call(Configure::read('API.url_users_detail'), $param);
    $this->Common->handleException(Api::getError());
} else {
    $pageTitle = __('Register user');
    $order_id = $this->request->query('order_id');
    if (!empty($order_id)) {
        $isUpdate = true;
        $param = array('id' => $order_id);
        $order_detail = Api::Call(Configure::read('API.url_orders_detail'), $param);
        $data[$modelName] = array(
            'code' => $order_detail['user_code'],
            'name' => $order_detail['user_name'],
            'kana' => $order_detail['kana'],
            'sex' => $order_detail['sex'],
            'phone' => $order_detail['phone'],
            'email' => $order_detail['email'],            
            'birthday' => $order_detail['birthday'],            
            'prefecture_id' => $order_detail['prefecture_id'],            
            'address1' => $order_detail['address1'],            
            'address2' => $order_detail['address2'],            
            'visit_element' => $order_detail['visit_element'],
        );
    }
}
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/users',
            'name' => __('User list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));
$this->setPageTitle($pageTitle);

//Get all prefectures.
$listPrefectures = $this->Common->arrayKeyValue(MasterData::prefectures_all(), 'id', 'name');
$this->UpdateForm
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
    ));
if ($isUpdate) {
    $this->UpdateForm->addElement(array(
        'id' => 'code',
        'label' => __('Code'), 
        'readonly' => 'readonly'
    ));
}
$this->UpdateForm       
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'kana',
            'label' => __('Name kana'),
            'required' => true
        ))
         ->addElement(array(
            'id' => 'sex',
            'label' => __('Gender'),
            'options' => Configure::read('Config.searchGender'),
            'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true
        ))       
        ->addElement(array(
            'id'        => 'phone',
            'label'     => __('Phone'),
            'phone'     => true,
            'required'  => true
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email'),
            'required' => true,
            'autocomplete' => 'off'
        ))
        ->addElement(array(
            'id' => 'birthday',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Birthday')
        ))
        ->addElement(array(
            'id' => 'prefecture_id',
            'label' => __('Prefecture name'),
            'options' => $listPrefectures,
            'empty' => Configure::read('Config.StrChooseOne'),                
        ))
        ->addElement(array(
            'id' => 'address1',
            'label' => __('Address1')
        ))
        ->addElement(array(
            'id' => 'address2',
            'label' => __('Address2')
        ))
        ->addElement(array(
            'id' => 'is_magazine',
            'label' => __('Magazine'),
            'options' => Configure::read('Config.BooleanValue'),
            'selected' => 0
        ))
        ->addElement(array(
            'id' => 'visit_element',
            'label' => __('Visit element'),
            'options' => Configure::read('Config.VisitElement'),
            'empty' => Configure::read('Config.StrChooseOne')
        ));
if (!$isUpdate) {
    $this->UpdateForm->addElement(array(
                'id' => 'password',
                'type' => 'password',
                'label' => __('Password'),
                'autocomplete' => 'off',
                'required' => true
            ))
            ->addElement(array(
                'id' => 'password_confirm',
                'type' => 'password',
                'label' => __('Confirm Password'),
                'autocomplete' => 'off',
                'required' => true
    ));
}
$this->UpdateForm->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));
// Process when submit form
if ($this->request->is('post')) {
    $error = array(
        'email' => array(1011 => __('Email address have registered already'))
    );
    $data = $this->getData($modelName);

    $data[$modelName]['phone']= str_replace(array('_',' '),"",$data[$modelName]['phone']);
    if ($model->validateUserInsertUpdate($data)) {
        //$data[$modelName]['phone'] = $tempTel;
        if(!empty($order_id)){
            $data[$modelName]['order_id'] = $order_id;
        }
        $id = Api::call(Configure::read('API.url_users_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            if(!empty($order_id)){
                $redirect = "/ajax/medicalchartdialog?id={$order_id}";
            }else{
                $redirect = "/{$this->controller}/update/{$id}";
            }
            return $this->redirect($redirect);
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error    
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
