<?php

$modelName = $this->User->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('User list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
//get list shop
$listShop = $this->Common->arrayKeyValue(MasterData::shops_all(), 'id', 'name');
// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'    => 'id',
        'label' => __('User ID')
    ))
    ->addElement(array(
        'id'    => 'code',
        'label' => __('Code')
    ))
    ->addElement(array(
        'id'    => 'name',
        'label' => __('User name')
    ))
    ->addElement(array(
        'id'    => 'kana',
        'label' => __('Name kana')
    ))
    ->addElement(array(
        'id'    => 'email',
        'label' => __('User email'),
        'empty' => ''
    ))
    ->addElement(array(
        'id'    => 'phone',
        'label' => __('Phone'),
        'empty' => ''
    ))
    ->addElement(array(
        'id'      => 'shop_id',
        'label'   => __('User shop name'),
        'options' => $listShop,
        'empty'   => __('All'),
    ))
    ->addElement(array(
        'id'       => 'reservation_date',
        'label'    => __('Reservation date'),
        'type'     => 'text',
        'calendar' => true,
        'empty'    => '',
    ))
    ->addElement(array(
        'id'      => 'sex_id',
        'label'   => __('Gender'),
        'options' => Configure::read('Config.searchGender'),
        'empty'   => __('All')
    ))
    ->addElement(array(
        'id'      => 'disable',
        'label'   => __('User status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty'   => __('All')
    ))
     ->addElement(array(
        'id'      => 'disable_by_user',
        'label'   => __('Disable by user'),
        'options' => Configure::read('Config.searchStatus'),
        'empty'   => __('All')
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'id-asc'     => __('ID Asc'),
            'id-desc'    => __('ID Desc'),
            'name-asc'   => __('Name Asc'),
            'name-desc'  => __('Name Desc'),
            'email-asc'  => __('Email Asc'),
            'email-desc' => __('Email Desc'),
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'      => 'limit',
        'label'   => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_users_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'item',
        'name'  => 'items[]',
        'type'  => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id'    => 'id',
        'type'  => 'link',
        'title' => __('ID'),
        'href'  => '/' . $this->controller . '/update/{id}',
        'width' => 50
    ))
    ->addColumn(array(
        'id'    => 'code',
        'title' => __('Code'),
        'empty' => '',
        'width' => 100,
        'hidden' => true
    ))
    ->addColumn(array(
        'id'    => 'name',
        'type'  => 'link',
        'href'  => '/' . $this->controller . '/update/{id}',
        'title' => __('User name'),
        'width' => 250,
        'empty' => '',
    ))
    ->addColumn(array(
        'id'    => 'is_web',
        'title' => __('端末'),
        'width' => 150,
        'empty' => '',
        'rules'  => array(
            '0' => '',
            '1' => '<div class="mt5"><i class="fa fa-desktop" style="font-size:11px"></i></div>'
        ),
    ))
    ->addColumn(array(
        'id'    => 'is_ios',
        'title' => __('IOS'),
        'empty' => '',
        'rules'  => array(
            '0' => '',
            '1' => '<i class="fa fa-apple"></i>'
        ),
       'hidden'=> true
    ))
    ->addColumn(array(
        'id'    => 'is_android',
        'title' => __('Android'),
        'empty' => '',
        'rules'  => array(
            '0' => '',
            '1' => '<i class="fa fa-android"></i>'
        ),
        'hidden'=> true
    ))
    ->addColumn(array(
        'id'     => 'kana',
        'type'   => 'link',
        'href'   => '/' . $this->controller . '/update/{id}',
        'title'  => __('Name kana'),
        'width'  => 250,
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'    => 'sex',
        'title' => __('Gender'),
        'rules' => Configure::read('Config.searchGender'),
        'empty' => '',
        'width' => 80
    ))
    ->addColumn(array(
        'id'    => 'email',
        'title' => __('User email'),
        'empty' => '',
        'width' => 120,
    ))
    ->addColumn(array(
        'id'    => 'phone',
        'title' => __('Phone'),
        'empty' => '',
        'width' => 120,
    ))
    ->addColumn(array(
        'id'    => 'shop_name',
        'title' => __('User shop name'),
        'empty' => '',
        'width' => 200,
    ))
    ->addColumn(array(
        'id'    => 'point',
        'title' => __('所持ポイント'),
        'type'   => 'link',
        'href'   => '/userpointlogs?user_customid={id}',
        'empty' => '0',
        'width' => 150,
    ))
    ->addColumn(array(
        'id'    => 'created',
        'type'  => 'date',
        'title' => __('Created'),
        'width' => 200,
    ))
    ->addColumn(array(
        'type'     => 'link',
        'th_title' => __('Password'),
        'title'    => __('Change'),
        'href'     => '/' . $this->controller . '/password/{id}',
        'button'   => true,
        'width'    => 100,
    ))
    ->addColumn(array(
        'type' => 'link',
        'title' => __('Setting'),
        'href' => '/usersettings/index/{id}',
        'button' => true,
        'width' => '80'
    ))
    ->addColumn(array(
        'id'     => 'disable',
        'type'   => 'checkbox',
        'title'  => __('User status'),
        'toggle' => true,
        'rules'  => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty'  => 0,
        'width'  => 90,
    ))
    ->setDataset($data)
    ->setMergeColumn(array(
        'name' => array(
            array(
                'field'  => 'code',
                'before' => __('Code') . " : ",
                'after'  => ''
            ),
            array(
                'field'  => 'kana',
                'before' => __('Name kana') . " : ",
                'after'  => ''
            ),
            /*array(
                'field'  => 'phone',
                'before' => __('Phone') . " : ",
                'after'  => ''
            ),
            array(
                'field'  => 'created',
                'before' => __('Created') . " : ",
                'after'  => ''
            ),*/
        )
    ))
    
     ->setMergeColumn(array(
        'is_web' => array(
            array(
                'field'  => 'is_ios',
            ),
            array(
                'field'  => 'is_android',
            ),
        
        )
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
$exportUrl = '';
$param1 = $param;
unset($param1['page']);
unset($param1['limit']);

foreach ($param1 as $key => $value) {
    if (is_array($value)) {
        foreach ($value as $v) {
            $exportUrl .= $key . '%5B%5D=' . $v . '&';
        }
    } else {
        $exportUrl .= $key . '=' . $value . '&';
    }
}
$this->set('exportUrl', $exportUrl);
