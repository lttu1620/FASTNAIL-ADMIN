<?php

$modelName = $this->User->name;

// create breadcrumb
$pageTitle = __('Following company list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle
        ));

$userInfo = Api::Call(Configure::read('API.url_users_detail'), array('id' => $id, 'full' => 1), false, array());
$this->Common->handleException(Api::getError());
$this->set('profileTab', $this->Common->renderProfileTab($userInfo));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['user_id'] = $id;
$result = Api::call(Configure::read('API.url_mobile_followcompanies_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$data = $result['data'];
$this->set('total', $result['total']);
$this->set('limit', $param['limit']);
$this->SimpleTable
        ->addColumn(array(
            'id' => 'thumbnail_img',
            'type' => 'image',
            'title' => __('Image'),
            'src' => '{thumbnail_img}',
            'image_type' => 'company',
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'name',
            'type' => 'link',
            'title' => __('Name'),
            'href' => '/companies/update/{id}',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'kana',
            'title' => __('Kana'),
            'width' => '250',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'address',
            'title' => __('Address'),
            'empty' => '',
            'hidden' => true,
        ))
        ->addColumn(array(
            'id' => 'category_name',
            'title' => __('Category name'),
            'empty' => '',
            'width' => '120',
        ))
        ->addColumn(array(
            'id' => 'follow_count',
            'type' => 'link',
            'title' => __('Followers'),
            'href' => '/' . ($this->AppUI->is_admin == 1 ? 'followcompanies/index/{id}' : $this->controller . '/follower/{id}'),
            'width' => '100',
            'button' => true,
            'empty' => '0'
        ))
        ->addColumn(array(
            'id' => 'created',
            'title' => __('Created'),
            'type' => 'date',
            'empty' => '',
            'width' => '120',
        ))
        ->setDataset($data)
        ->setMergeColumn(array(
            'name' => array(
                array(
                    'field' => 'address',
                    'before' => __('Address') . " : ",
                ),
            )
        ));
