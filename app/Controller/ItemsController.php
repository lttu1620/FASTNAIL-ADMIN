<?php

App::uses('AppController', 'Controller');

/**
 * ItemsController class of Items Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ItemsController extends AppController
{
    /**
     * Initializes components for ItemsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Items.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Items/index.php');
    }

    /**
     * Handles user interaction of view update Items.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Items. Default value is 0.
     * @return void
     */
    public function update($id = 0)
    {
        include('Items/update.php');
    }
}
