<?php

$modelName = $this->Mailsendlog->name;

//Process disable/enable
$this->doGeneralAction($modelName);

//get list shop
$listShop = MasterData::shops_all();
$listShop = $this->Common->arrayKeyValue($listShop, 'id', 'name');

//get list nailist
$listNailist = MasterData::nailists_all();
$listNailist = $this->Common->arrayKeyValue($listNailist, 'id', 'name');

// create breadcrumb
$pageTitle = __('Mail Sent Logs');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'      => 'shop_id',
        'label'   => __('Shop'),
        'options' => $listShop,
        'empty'   => __('All'),
    ))
    ->addElement(array(
        'id'      => 'nailist_id',
        'label'   => __('Nailist'),
        'options' => $listNailist,
        'empty'   => __('All')
    ))
    ->addElement(array(
        'id'    => 'user_id',
        'type'  => 'text',
        'label' => __('User ID'),
    ))
    ->addElement(array(
        'id'    => 'title',
        'type'  => 'text',
        'label' => __('Title'),
    ))
    ->addElement(array(
        'id'    => 'to_email',
        'type'  => 'text',
        'label' => __('To email'),
    ))
    ->addElement(array(
        'id'    => 'content',
        'type'  => 'text',
        'label' => __('Content'),
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'id-asc'       => __('ID Asc'),
            'id-desc'      => __('ID Desc'),
            'user_id-asc'  => __('User ID Asc'),
            'user_id-desc' => __('User ID Desc'),
            'shop_id-asc'  => __('Shop Asc'),
            'shop_id-desc' => __('Shop Desc'),
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'      => 'status',
        'label'   => __('Status'),
        'options' => Configure::read('Config.statusSendLog'),
    ))
    ->addElement(array(
        'id'      => 'limit',
        'label'   => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array(
        'user_id' => !empty($param['user_customid']) ? $param['user_customid'] : 0,
        'page'    => 1,
        'limit'   => Configure::read('Config.pageSize'))
);
list($total, $data) = Api::call(Configure::read('API.url_mailsendlogs_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'id',
        'title' => __('ID'),
        'width' => '30'
    ))
    ->addColumn(array(
        'id'    => 'user',
        'title' => __('User'),
        'width' => 200,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'     => 'user_id',
        'title'  => __('ID'),
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'user_first_name',
        'title'  => __('First name'),
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'user_last_name',
        'title'  => __('Last name'),
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'user_email',
        'title'  => __('Email'),
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'user_phone',
        'title'  => __('Phone'),
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'    => 'shop_name',
        'title' => __('Shop'),
        'width' => 180,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'nailist_name',
        'title' => __('Nailist'),
        'width' => 180,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'log',
        'title' => __('Log'),
        'empty' => ''
    ))
    ->addColumn(array(
        'id'     => 'title',
        'title'  => __('Title'),
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'content',
        'title'  => __('Content'),
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'created',
        'title'  => __('Created'),
        'type'   => 'date',
        'hidden' => true
    ))
    ->setMergeColumn(array(
        'user' => array(
            array(
                'field'  => 'user_id',
                'before' => __('ID') . " : ",
                'after'  => ''
            ),
            array(
                'field'  => 'user_first_name',
                'before' => __('First name') . " : ",
                'after'  => ''
            ),
            array(
                'field'  => 'user_last_name',
                'before' => __('Last name') . " : ",
                'after'  => ''
            ),
            array(
                'field'  => 'user_email',
                'before' => __('Email') . " : ",
                'after'  => ''
            ),
            array(
                'field'  => 'user_phone',
                'before' => __('Phone') . " : ",
                'after'  => ''
            )
        ),
        'log'  => array(
            array(
                'field'  => 'title',
                'before' => __('Title') . " : ",
                'after'  => ''
            ),
            array(
                'field'  => 'content',
                'before' => __('Content') . " : ",
                'after'  => ''
            ),
            array(
                'field'  => 'created',
                'before' => __('Created') . " : ",
                'after'  => ''
            )
        )
    ))
    ->setDataset($data);
