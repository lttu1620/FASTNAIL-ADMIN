<?php

App::uses('AppController', 'Controller');

/**
 * ItemsController class of Item Points Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PointitemsController extends AppController
{
    /**
     * Initializes components for ItemsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Items.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Pointitems/index.php');
    }

    /**
     * Handles user interaction of view update Items.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Items. Default value is 0.
     * @return void
     */
    public function update($id = 0)
    {
        include('Pointitems/update.php');
    }
}
