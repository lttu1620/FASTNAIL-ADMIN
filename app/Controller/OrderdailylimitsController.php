<?php

App::uses('AppController', 'Controller');

/**
 * Orders Controller
 *
 * @package Controller
 * @created 2015-04-21
 * @version 1.0
 * @author Tuan cao
 * @copyright Oceanize INC
 */
class OrderdailylimitsController extends AppController {

    var $components=array('PhpExcel');
    /**
     * Construct
     *
     * @author Tuan cao
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     *
     * @author Tuan cao
     * @return void
     */
    public function index() {
        include ('Orderdailylimits/index.php');
    }

    /**
     * update action
     *
     * @author Tuan cao
     * @param $date int datetime value of month
     * @return void
     */
    public function calendar($date = 0) {
        include ('Orderdailylimits/calendar.php');
    }
}
