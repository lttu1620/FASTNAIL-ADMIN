<?php

App::uses('AppController', 'Controller');

/**
 * UserboughtitemsController class of User Bought Item Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserboughtitemsController extends AppController
{
    /**
     * Initializes components for UserboughtitemsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Userboughtitems.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Userboughtitems/index.php');
    }
}
