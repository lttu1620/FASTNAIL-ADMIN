<?php

$modelName = $this->Orderupdatetimebarlog->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Order update timebar log List');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'    => 'admin_customid',
            'type'  => 'text',
            'label' => __('Admin ID'),
        )
    )
    ->addElement(
        array(
            'id'    => 'order_customid',
            'type'  => 'text',
            'label' => __('Order ID'),
        )
    )
    ->addElement(
        array(
            'id'      => 'sort',
            'label'   => __('Sort'),
            'options' => array(
                'id-asc'       => __('ID Asc'),
                'id-desc'      => __('ID Desc'),
                'created-asc'  => __('Created Asc'),
                'created-desc' => __('Created Desc'),
            ),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );

// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
if (isset($param['user_customid'])) {
    $param['admin_id'] = $param['admin_customid'];
}
if (isset($param['beauty_customid'])) {
    $param['beauty_id'] = $param['beauty_customid'];
}
list($total, $data) = Api::call(Configure::read('API.url_order_update_timebar_log_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);
// create data table
$this->SimpleTable
    ->addColumn(
        array(
            'id'    => 'id',
            'title' => __('ID'),
            'width' => 50,
        )
    )
    ->addColumn(
        array(
            'id'    => 'order_id',
            'title' => __('Order ID'),
            'empty' => '',
            'width' => 60,
        )
    )
    ->addColumn(
        array(
            'id'    => 'admin_id',
            'title' => __('Admin ID'),
            'empty' => '',
            'width' => 60,
        )
    )
    ->addColumn(
        array(
            'id'    => 'admin_name',
            'title' => __('Name'),
            'empty' => '',
            'width' => 120,
        )
    )
    ->addColumn(
        array(
            'id'    => 'before_seat_id',
            'title' => __('Before seat ID'),
            'empty' => '',
            'width' => 60,
        )
    )
    ->addColumn(
        array(
            'id'    => 'before_order_start_time',
            'title' => __('Before order start time'),
            'type'  => 'date',
            'empty' => '',
            'width' => 140,
        )
    )
    ->addColumn(
        array(
            'id'    => 'before_order_end_time',
            'title' => __('Before order end time'),
            'type'  => 'date',
            'empty' => '',
            'width' => 140,
        )
    )
    ->addColumn(
        array(
            'id'    => 'after_seat_id',
            'title' => __('After seat ID'),
            'empty' => '',
            'width' => 60,
        )
    )
    ->addColumn(
        array(
            'id'    => 'after_order_start_time',
            'title' => __('After order start time'),
            'type'  => 'date',
            'empty' => '',
            'width' => 140,
        )
    )
    ->addColumn(
        array(
            'id'    => 'after_order_end_time',
            'title' => __('After order end time'),
            'type'  => 'date',
            'empty' => '',
            'width' => 140,
        )
    )
    ->addColumn(
        array(
            'id'    => 'created',
            'title' => __('Created'),
            'type'  => 'date',
            'width' => 140,
        )
    )
    ->setDataset($data);
