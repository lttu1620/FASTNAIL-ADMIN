<?php

App::uses('AppController', 'Controller');

/**
 * HolidaysController class of Holidays Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class HolidaysController extends AppController
{
    /**
     * Initializes components for HolidaysController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Holidays.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Holidays/index.php');
    }
}
