<?php
$modelName = $this->Bannerrecommend->name;
// Create breadcrumb
$pageTitle = __('Campaign list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'id',
        'label' => __('ID'),
    ))
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name')
    ))
    ->addElement(array(
    		'id'       => 'start_date',
    		'type'     => 'text',
    		'calendar' => true,
    		'label'    => __('Date from'),
    ))
    ->addElement(array(
    		'id'       => 'end_date',
    		'type'     => 'text',
    		'calendar' => true,
    		'label'    => __('Date to'),
    ))
    ->addElement(array(
    		'id' => 'tag_name',
    		'label' => __('Tag name')
    ))
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty' => __('All')
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'id-asc' => __('ID Asc'),
            'id-desc' => __('ID Desc'),
            'name-asc' => __('Name Asc'),
            'name-desc' => __('Name Desc'),
            'tag_name-asc' =>__('Tag name Asc'),
            'tag_name-desc' =>__('Tag name Desc'),
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_bannerrecommends_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id' => 'item',
        'name' => 'items[]',
        'type' => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
     ->addColumn(array(
        'id'    => 'image_url',
        'type'  => 'image',
        'title' => __('Image'),
        'src'   => '{image_url}',
        'width' => '60'
    ))
    ->addColumn(array(
        'id' => 'id',
        'type' => 'link',
        'title' => __('ID'),
        'href' => '/' . $this->controller . '/update/{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id' => 'name',
        'title' => __('Name'),
        'width' => '150'
    ))
    ->addColumn(array(
        'id' => 'tag_name',
        'title' => __('Tag name'),
        'width' => 250,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'start_date',
        'type'  => 'dateonly',
        'title' => __('Start date'),
        'width' => 150,
    ))
    ->addColumn(array(
    		'id'    => 'end_date',
    		'type'  => 'dateonly',
    		'title' => __('End date'),
    		'width' => 150,
    ))
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'checkbox',
        'title' => __('Status'),
        'toggle' => true,
        'rules' => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty' => 0,
        'width' => 90,
    ))
    ->setDataset($data)
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
