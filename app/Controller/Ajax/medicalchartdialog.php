<?php
    $modelName = $this->Order->name;
    $model = $this->{$modelName};

    //Read configs
    $arrayConfigs = array (
        'showSection',
        'VisitElement',
        'ReservationType',
        'MailOption',
        'NailOff',
        'OnOff',
        'hfSection',
        'NailLength',
        'NailType',
        'FingerItemLength',
        'Finger'
    );
    foreach ($arrayConfigs as $cfg) {
        $this->set('list' . ucfirst($cfg), Configure::read('Config.' . $cfg));
    }

    $items = MasterData::items_all();
    $itemSection = Configure::read('Config.ItemSection');
    $listItems = array();
    foreach ($itemSection as $sectionId => $sectionName) {
        $listItems[$sectionId]['nane'] = $sectionName;
        $listItems[$sectionId]['item'] = $this->Common->arrayFilter($items, 'item_section', $sectionId);
    }    
    
    $errors = array();    
   
    $request = $this->getParams();
    $this->set('curMedId',!empty($request['curMedId']) ? $request['curMedId'] : '');
    $this->set('nailTab',!empty($request['tab']) ? $request['tab'] : '');
    if(!empty($request['orderId']) && !empty($request['undo'])){
    	$orderId = $request['orderId'];
        $this->set('is_undo', $request['undo']);
    }

    //Get order detail
    $orderDetail = Api::call(Configure::read('API.url_orders_detail'), array(
        'id' => $orderId, 
        'get_nail' => 1, 
        'get_item' => 1, 
        'get_latest_order' => 1, 
        'get_service' => 1, 
        'get_shop' => 1, 
        'get_order_history' => 1,
        'get_suborder' => 1));
    //d($orderDetail,1);
    if (isset($orderDetail['status']) && $orderDetail['status'] == 400) {
        echo __($orderDetail['error'][0]['message']);
        exit;
    }
    $arrItemIds = array_column($orderDetail['items'], 'item_id');

    //Get nailists' name
    $orderNailists = array_column(Api::call(Configure::read('API.url_orderlogs_nailist_logs_all'), array('order_id' => $orderId)), 'nailist_name', 'type');
    $data[$modelName] = $orderDetail;

    if ($this->request->is('post')) {
        //Posted parameters from form
        $postData = $this->getData($modelName);

        //Fields not to update
        $notUpdateFields = array_diff(array_keys($data[$modelName]), array_keys($postData[$modelName]));
        $data = array_replace_recursive($data, $postData);

        //Remove fields that are not for updating
        $updateData = $data;
        foreach ($notUpdateFields as $unsetField) {
            unset($updateData[$modelName][$unsetField]);
        }
        $updateData[$modelName]['id'] = $orderId;

        $updateData[$modelName]['items_id'] = !empty($updateData[$modelName]['items_id']) ? implode(',', $updateData[$modelName]['items_id']) : '';
        $updateData[$modelName]['items_quantity'] = !empty($updateData[$modelName]['items_quantity']) ? implode(',', $updateData[$modelName]['items_quantity']) : '';

        if (!isset($postData[$modelName]['problem'])) {
            $updateData[$modelName]['problem'] = '';
        }

        if ($model->validateOrderInsertUpdate($updateData)) {
            Api::call(Configure::read('API.url_orders_adminaddupdate'), $updateData[$modelName]);
            if (Api::getError()) {
                $errors += Api::getError();
            } else {
                //Successfull
                //$this->set('succedMsg',  __('Update successful'));
                echo json_encode(array('succedMsg' => __('Update successful')));
                exit;
            }
        } else {
            $errors +=  $model->validationErrors;
        }
        if (!empty($errors)) {
            echo json_encode($errors);
            exit;
        }
    }

    //get nail detail
    $data['Nail'] = array();
    if (!empty($orderDetail['nails'])) {
        foreach ($orderDetail['nails'] as $key => $nail) {
            $data['Nail'][$key] = Api::call(
                    Configure::read('API.url_nails_detail'), array(
                    'id' => $nail['nail_id'],
                    'order_id' => $orderId,
                    'get_attribute_for_view' => 1
                    )
            );
        }
    }
    $data['nailists'] = MasterData::nailists_all($orderDetail['shop_id']);
    //$data['nailists']= Api::call(Configure::read('API.url_orderlogs_allforupdate'), array('order_id' => $orderId, 'shop_id' => $orderDetail['shop_id']));
    $data['order_nailist']= Api::call(Configure::read('API.url_orderlogs_nailist'), array('order_id' => $orderId));
    $data['nailist_log'] = Api::call(Configure::read('API.url_orderlogs_nailist_logs_all'), array('order_id' => $orderId));
    $adminOrderTimeout = Configure::read('Config.AdminOrder.Timeout');
    $title = $data['Order']['user_name'];
    $displayName = array();
    if (!empty($data['Order']['user_name'])) {
        $displayName[] = $data['Order']['user_name'];
    }
    if (!empty($data['Order']['kana'])) {
        $displayName[] = $data['Order']['kana'];
    }
    $data['Order']['display_user_name'] = implode(' ', $displayName);
    
    $displayVisitSection = '';
    if (empty($data['Order']['latest_order'])) {
        $data['Order']['visit_section'] = 1;
    }            
    if (isset($data['Order']['visit_section']) && $data['Order']['visit_section'] == 1) {
        $displayVisitSection = '初めて';
    } elseif (!empty($data['Order']['latest_order'])) {
        $latestDate = $data['Order']['latest_order']['reservation_date'];
        if (strtotime('-3 week', $data['Order']['reservation_date']) <= $latestDate) {
            $displayVisitSection = "3週間以内"; // in 3weeks
        } elseif (strtotime('-1 month', $data['Order']['reservation_date']) <= $latestDate) {
            $displayVisitSection = "1ヶ月以内"; // in 1 month
        } elseif (strtotime('-2 month', $data['Order']['reservation_date']) <= $latestDate) {
            $displayVisitSection = "2ヶ月以内";// in 2months
        } else {
            $displayVisitSection = "2ヶ月以上"; // over 2months
        }
    }

    $listNailOffPrice = Configure::read('Config.NailOffPrice');
    $fingerItemPrice = Configure::read('Config.FingerItemPrice');
    $itemTax = Configure::read('Config.ItemTax');

    if ($data['Order']['hf_section'] == 1 && $data['Order']['off_nails'] == 2){ //ソフトジェル
        $listNailOffPrice[2] = '無料';
    }
    $this->set(compact('data', 'errors', 'listItems', 'orderNailists',
            'arrItemIds', 'adminOrderTimeout', 'title', 'displayVisitSection',
            'fingerItemPrice', 'listNailOffPrice','itemTax'));
