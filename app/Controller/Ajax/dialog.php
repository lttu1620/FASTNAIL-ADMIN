<?php
$this->layout='bookingform';
// get data
if (empty($this->request->query['data'])) {
    AppLog::info("Parameters unavailable", __METHOD__, null);
    $this->request->query['data'] = '';
}
$data = json_decode(base64_decode($this->request->query['data']), true);

// return if data is empty
if(empty($data)){
    $info = array();
}else{
    //get available nailist
    $nailist = MasterData::nailists_all($this->AppUI->shop_id);
    $nailist = $this->Common->generateArrPrefixValue($nailist, 'id', 'n_', array('id', 'name'));
    //get available devicelist
    $devicelist = MasterData::devices_all($this->AppUI->shop_id);
    $devicelist = $this->Common->generateArrPrefixValue($devicelist, 'id', 'd_', array('id', 'name'));
    // generate available list
    $available_list = array_merge($nailist, $devicelist);

    //if have order_id -> update
    if (!empty($data['reservation']['order_id'])) {
        $order_id = $data['reservation']['order_id'];
        $param['order_id'] = $order_id;
        $order = Api::call(Configure::read('API.url_orders_detailforcalendar'), $param);
        if (Api::getError()) {
            AppLog::info("API.url_orders_detailforcalendar", __METHOD__, $param);
            return $this->Common->setFlashErrorMessage(Api::getError());
        }
        $reservation_date = date('Y-m-d H:i', $order['reservation_date']);
        $reservation_date_timestamp = $order['reservation_date'];
        $reservation_duration = $order['order_end_date'] - $order['order_start_date'];

        //generate selected list
        $nailist_arr = $this->Common->arrayFilter($order['nailists'][0], 'disable', 0);
        $selected_nailist_ids = $this->Common->arrayValues($nailist_arr, 'nailist_id');
        foreach($selected_nailist_ids as &$n_id){
            $n_id = 'n_'.$n_id;
        }
        $device_arr = $this->Common->arrayFilter($order['devices'][0], 'disable', 0);
        $selected_device_ids = $this->Common->arrayValues($device_arr, 'device_id');
        foreach($selected_device_ids as &$d_id){
            $d_id = 'd_'.$d_id;
        }
        $selected_ids = array_merge($selected_nailist_ids, $selected_device_ids);
        $visit_section = $order['visit_section'];
        $visit_element = $order['visit_element'];  
    } else { // add new
        //get data from dasboard
        $reservation = $data['reservation'];

        if (!is_numeric($reservation['start_at_epoch'])) {
            $reservation['start_at_epoch'] = time();
        }
        $reservation_date = date('Y-m-d H:i', $reservation['start_at_epoch']);
        $reservation_date_timestamp = $reservation['start_at_epoch'];
        $reservation_duration = !empty($reservation['duration']) ? $reservation['duration'] : 900;
        $selected_ids = $reservation['stylist_ids'];
        $visit_section = 0;
        $visit_element = 0;  
    }

    //prepare data for booking dialog
    $info = array(
        'reservation_date' => $reservation_date, //date('Y-m-d H:i:s O'),
        'startAtTimeStamp' => $reservation_date_timestamp,
        'reservation_duration' => Configure::read('Config.BookingDuration'),
        'duration' => $reservation_duration,
        'reservation_element' => Configure::read('Config.VisitElement'),
        'reservation_section' => Configure::read('Config.showSection'),
        'available_list' => $available_list,
        'selected_ids' => $selected_ids,
        'visit_section' => $visit_section,
        'visit_element' => $visit_element,
        'shop_id' => $this->AppUI->shop_id,
        'cart_id' => '', // ???    
        'user_id' => 0,
        'order_id' => !empty($order_id) ? $order_id : '',
        'user_name' => !empty($order['user_name']) ? $order['user_name'] : '',
        'phone' => !empty($order['phone']) ? $order['phone']: '',
        'email' => !empty($order['email']) ? $order['email']  : '',
        'sex' => !empty($order['sex']) ? $order['sex'] : ''
    );
}



$this->set('info', $info);




