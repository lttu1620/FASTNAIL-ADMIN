<?php

//get list of nalist
$listNailist = Api::Call(Configure::read('API.url_nailists_all'), array('shop_id' => $this->data['shop_id']));

$listOrderNailist = array();
$modelName = '';
if (!empty($this->data['order_id'])) {
    //get list of nalist of order
    $modelName = '[Order]';
    $listOrderNailist = Api::Call(
        Configure::read('API.url_orderlogs_all'),
        array('order_id' => $this->data['order_id']),
        false,
        array()
    );
} else if (!empty($this->data['cart_id'])) {
    //get list of nalist of cart
    $modelName = '[Cart]';
    $listOrderNailist = Api::Call(
        Configure::read('API.url_cartnailists_all'),
        array('cart_id' => $this->data['cart_id']),
        false,
        array()
    );
}

$selectedArr = $this->Common->arrayValues($listOrderNailist, 'nailist_id');
$response = '';

$type = !empty($this->data['type']) ?  $this->data['type'] : 'checkbox';
if ($type == 'selectbox') {
    $response .= '<select name="nailist_customid" class="form-control" id="nailist_customid">';
    $response .= '<option value="">すべて</option>';
}

$index = 0;
foreach ($listNailist as $row) {
    $index ++;
    $checked = '';
    if (in_array($row['id'], $selectedArr)) {
        $checked = 'checked="checked"';
    }
    if ($type == 'selectbox') {
        $response .= '<option '
            . $checked . ' value="' . $row['id']  . '"><label for="nailist_customid' . $index . '" class="selected">'
            . $row['name'] . '</label></option>';
    } else {
        $response .= '<div class="form-control"><input type="checkbox" name="data' . $modelName . '[nailist_customid][]"'
            . $checked . ' value="'
            . $row['id'] . '" id="nailist_customid' . $index . '"><label for="nailist_customid' . $index . '" class="selected">'
            . $row['name'] . '</label></div>';
    }
}

if ($type == 'selectbox') {
    $response .= '</select>';
}

echo $response; exit;