<?php
$param = $this->getParams();
if (isset($param['start_date']) && $param['start_date']==1) $param['start_date'] = time();
if (isset($param['is_order']) && $param['is_order']==1) $param['off_start_date'] = time();
if (isset($param['is_order']) && $param['is_order']==0) $param['off_end_date'] = time();
if (isset($param['is_service']) && $param['is_service']==1) $param['sr_start_date'] = time();
if (isset($param['is_service']) && $param['is_service']==0) $param['sr_end_date'] = time();
if (isset($param['f_is_order']) && $param['f_is_order']==1) $param['f_off_start_date'] = time();
if (isset($param['f_is_order']) && $param['f_is_order']==0) $param['f_off_end_date'] = time();
if (isset($param['f_is_service']) && $param['f_is_service']==1) $param['f_sr_start_date'] = time();
if (isset($param['f_is_service']) && $param['f_is_service']==0) $param['f_sr_end_date'] = time();
if (isset($param['is_consult']) && $param['is_consult']==1) $param['consult_start_date'] = time();
if (isset($param['is_consult']) && $param['is_consult']== 0) $param['consult_end_date'] = time();
$param['admin_id'] = $this->AppUI->id;
$param['get_undo'] = 1;
AppLog::info("DATA For Stopwatch", __METHOD__, $param);
$result=Api::call(Configure::read('API.url_orders_stopwatch'), $param);
AppLog::info("Stopwatch result", __METHOD__, $result);
if(is_array($result) && isset($result['order_id'])){
	AppLog::info("Write sesstion", __METHOD__, $result);
	$this->Session->write('StopWatchUndo_'.$result['order_id'], $result);
}
echo date('H:i', time());
exit; 