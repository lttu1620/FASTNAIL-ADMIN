<?php

$param['id'] = isset($_POST['order_id']) ? $_POST['order_id'] : '';
$param['disable'] = 1;
$status = Api::call(Configure::read('API.url_orders_disable'), $param);
if (Api::getError()) {
	AppLog::info("API.url_orders_disable", __METHOD__, $param);
	return $this->Common->setFlashErrorMessage(Api::getError());
}
$request_date = $_POST['request_date'];
if($status){
    echo json_encode(array('message' => 'OK', 'request_date' => $request_date));
}else{
    echo json_encode(array('message' => 'Failed', 'request_date' => $request_date));
}
exit;
