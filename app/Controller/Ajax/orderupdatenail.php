<?php
if (!empty($_REQUEST['order_id'])) {
    $orderDetail = Api::call(Configure::read('API.url_orders_detail'), array('id' => $_REQUEST['order_id'],
        'get_nail' => 1));

    if (!empty($orderDetail['nails'][0]) && (isset($_REQUEST['nail_id']) && ($orderDetail['nails'][0]['nail_id'] != $_REQUEST['nail_id']))) {
        $response = array();
        $response = $orderDetail['nails'][0];
        $response['status'] = 'OK';
        $response['user_code'] = !empty($orderDetail['user_code']) ? $orderDetail['user_code'] : '';
        $response['user_name'] = !empty($orderDetail['user_name']) ? $orderDetail['user_name'] : '';
        $response['kana'] = !empty($orderDetail['kana']) ? $orderDetail['kana'] : '';
        $response['user_id'] = !empty($orderDetail['user_id']) ? $orderDetail['user_id'] : '';

        $listNailOff = Configure::read('Config.NailOff');

        $response['nailOffText'] = $listNailOff[2] . ' 無料';

        $displayName = array();
        if (!empty($response['user_name'])) {
            $displayName[] = $response['user_name'];
        }
        if (!empty($response['kana'])) {
            $displayName[] = $response['kana'];
        }

        $response['display_user_name'] = implode(' ', $displayName);

        echo json_encode($response);
        exit;
    }elseif(!empty($orderDetail['nails'][1]) && (isset($_REQUEST['f_nail_id']) && ($orderDetail['nails'][1]['nail_id'] != $_REQUEST['f_nail_id']))) {
        $response = array();
        $response = $orderDetail['nails'][1];
        $response['status'] = 'OK';
        $response['user_code'] = !empty($orderDetail['user_code']) ? $orderDetail['user_code'] : '';
        $response['user_name'] = !empty($orderDetail['user_name']) ? $orderDetail['user_name'] : '';
        $response['kana'] = !empty($orderDetail['kana']) ? $orderDetail['kana'] : '';
        $response['user_id'] = !empty($orderDetail['user_id']) ? $orderDetail['user_id'] : '';

        $listNailOff = Configure::read('Config.NailOff');

        $response['nailOffText'] = $listNailOff[2] . ' 無料';

        $displayName = array();
        if (!empty($response['user_name'])) {
            $displayName[] = $response['user_name'];
        }
        if (!empty($response['kana'])) {
            $displayName[] = $response['kana'];
        }

        $response['display_user_name'] = implode(' ', $displayName);

        echo json_encode($response);
        exit;
    }
}

echo json_encode(array('status' => 'NG'));