<?php
//get list of device
$listDevice = Api::Call(Configure::read('API.url_devices_all'), 
                array('shop_id' => $this->data['shop_id']),
                false,
                array());


$modelName = '';
$listOrderDevice = array();
if (!empty($this->data['order_id'])) {
    $modelName = '[Order]';
    $listOrderDevice = Api::Call(Configure::read('API.url_order_devices_all'), 
                array('order_id' => $this->data['order_id']),
                false,
                array());    
}

$selectedArr = $this->Common->arrayValues($listOrderDevice, 'id');

$response = '';
$type = !empty($this->data['type']) ?  $this->data['type'] : 'checkbox';
if ($type == 'selectbox') {
    $response .= '<select name="device_customid" class="form-control" id="device_customid">';
    $response .= '<option value="">すべて</option>';
}

$index = 0;
foreach ($listDevice as $row) {
    $index ++;
    $checked = '';
    if (in_array($row['id'], $selectedArr)) {
        $checked = 'checked="checked"';
    }
    if ($type == 'selectbox') {
        $response .= '<option '
            . $checked . ' value="' . $row['id']  . '"><label for="device_customid' . $index . '" class="selected">'
            . $row['name'] . '</label></option>';
    } else {
        $response .= '<div class="form-control"><input type="checkbox" name="data' . $modelName . '[nailist_customid][]"'
            . $checked . ' value="'
            . $row['id'] . '" id="nailist_customid' . $index . '"><label for="nailist_customid' . $index . '" class="selected">'
            . $row['name'] . '</label></div>';
    }
}

if ($type == 'selectbox') {
    $response .= '</select>';
}

echo $response; exit;