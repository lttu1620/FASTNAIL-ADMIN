<?php

$param = $this->getParams();
$result = Api::call(Configure::read('API.url_user_bought_item_cancel'), $param);
if (Api::getError()) {
    AppLog::info("Cancel beauty failed", __METHOD__, $param);
    $error = array(
        'user_bought_item_id' => array(
            1010 => __("Not yet scan QR or has been completed")
        )
    );
    $this->Common->setFlashErrorMessage(Api::getError(), $error);
} else {
    $this->Common->setFlashSuccessMessage(__("Data updated successfully"));
}
exit;