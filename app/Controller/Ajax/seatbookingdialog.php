<?php

//get list of nalist
$listNailist = Api::Call(Configure::read('API.url_nailists_all'), array('shop_id' => $this->AppUI->shop_id));
$order_id = $this->data['order_id'];
// Get informtion of Order
$o_detail = Api::Call(Configure::read('API.url_orders_detailforcalendar'), array('order_id' => $order_id));
if (Api::getError()) {
	AppLog::info("API.url_orders_detailforcalendar", __METHOD__, $param);
	return $this->Common->setFlashErrorMessage(Api::getError());
}
$this->set(compact('listNailist','o_detail'));

$max_seat = 0;
if (!isset($this->AppUI->total_max_seat)) {
	$shop = Api::call(Configure::read('API.url_shops_detail'), array('id' => $this->AppUI->shop_id));
	if (empty(Api::getError()) && !empty($shop)) {
		$max_seat = $shop['max_seat'] + $shop['hp_max_seat'];
	}    
} else {
	$max_seat = $this->AppUI->total_max_seat;
}
$this->set(compact('max_seat'));
