<?php
$param = array(
    'disable' => $_POST['disable'],
    'date' => $_POST['date']
);

if ($param['disable'] == 1) {
    $result = Api::call(Configure::read('API.url_holiday_disable'), $param);
} else {
    $param['name'] = $_POST['name'];
    $result = Api::call(Configure::read('API.url_holiday_add_update'), $param);
}

if (Api::getError()) {
    AppLog::info("Set or disable holiday failed", __METHOD__, $param);
    return $this->Common->setFlashErrorMessage(Api::getError());
}

exit;