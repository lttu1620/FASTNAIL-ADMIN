<?php
if (empty($this->AppUI->id) || empty($this->AppUI->shop_id)) {
    return false;
}
$param = $this->getParams(array(
        'admin_id' => $this->AppUI->id,
        'shop_id' => $this->AppUI->shop_id,
    )
);
if(isset($param['drawedOrderIds'])) {
    $param['drawedOrderIds'] = json_encode($param['drawedOrderIds']);
}
list($totalNewNoNailist, $count_add_nail, $new_order) = Api::call(Configure::read('API.url_check_neworder'), $param, false, array(0, array(), 0));
$html = '';
$htmlAddNail = '';
if (!empty($new_order)) {
    $html = '<a href="' . Router::url('/orders/seat') . '" >' . sprintf(__('There are %s new orders, check now'), $new_order) . '</a>' ; 
}
if (!empty($count_add_nail)) {
    $htmlAddNail = '<a href="' . Router::url('/orders/seat') . '" style="color: #D5000D !important">' . sprintf(__('%s件のネイル選択が終了しました。'), count($count_add_nail)) . '</a>' ; 
}
echo json_encode(array(
    'totalNewNoNailist' => $totalNewNoNailist,
    'notifyHtml' => $html,
    'notifyHtmlAddNail' => $htmlAddNail,
    'totalAddNail' => $count_add_nail
));
exit;