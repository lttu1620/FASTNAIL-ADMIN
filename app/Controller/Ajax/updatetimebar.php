<?php
if (isset($this->request->query['order_id'])) {
    $order_id = $this->request->query['order_id'];
    $param['order_id'] = $order_id;
}
$param['shop_id'] = $this->AppUI->shop_id;

if (isset($this->request->data['start_at_epoch'])) {
    $dt = $this->request->data['start_at_epoch'];
    $param['start_at_epoch'] = $dt;
}
if (isset($this->request->data['duration'])) {
    $duration = $this->request->data['duration'];
    $param['duration'] = $duration;
}
if (isset($this->request->data['stylist_ids'])) {
    $stylist_ids = $this->request->data['stylist_ids'];
    $param['stylist_ids'] = implode(',', $stylist_ids);
}

if (isset($this->request->query['is_seat'])) {
    $param['is_seat'] = 1;
}

if(isset($this->request->data['confirm_yes']))
{
	$param['confirm_yes'] = 1;
}

if(isset($this->request->data['confirm_yes']))
{
	$param['confirm_yes'] = 1;
}

$param['shop_open_time'] = $this->AppUI->shop_open_time;
$param['shop_close_time'] = $this->AppUI->shop_close_time;
$param['shop_max_seat'] = $this->AppUI->shop_max_seat;
$param['last_update_admin_id'] = $this->AppUI->id;

$record = Api::call(Configure::read('API.url_update_timebar'), $param, false, array());

$this->Common->handleException(Api::getError());
$this->set('data', $record);
