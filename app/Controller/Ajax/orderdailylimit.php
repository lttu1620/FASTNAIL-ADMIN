<?php
$param = array(
    'shop_id' => $_POST['shop_id'],
    'limit' => $_POST['limit'],
    'date' => $_POST['date']
);
$result = Api::call(Configure::read('API.url_order_daily_limit_add_update'), $param);

if (Api::getError()) {
    AppLog::info("API.url_order_daily_limit_add_update failed", __METHOD__, $param);
    return $this->Common->setFlashErrorMessage(Api::getError());
}

return $result;