<?php
if (empty($this->AppUI->shop_id)) {
    exit;
}
$param['shop_id'] = $this->AppUI->shop_id;
if (!empty($this->request->data['date'])) {
    $dt = $this->request->data['date'];
} else {
    $dt = date('Y-m-d');
}
$param['date'] = $dt;
$staffRecord = array();
$controller = !empty($this->request->data['controller']) ? $this->request->data['controller'] : '';
$action = !empty($this->request->data['action']) ? $this->request->data['action'] : '';
$is_edit_seat = !empty($this->request->data['is_edit_seat']) ? $this->request->data['is_edit_seat'] : '';
if (!empty($this->request->query['is_seat'])) {
	$param['is_seat'] = 1;	
    $result = Api::call(Configure::read('API.url_orders_date_calendar_data'), $param, false, array());
    $this->Common->handleException(Api::getError());
    
    $shop = $result['shop'];
    $data = $result['calendar_json'];
    $max_real_seat = !empty($result['max_real_seat']) ? $result['max_real_seat'] : ($shop['max_seat'] + $shop['hp_max_seat']);
    $order_timely_limit = $result['order_timely_limit'];
    $max_seat = $shop['max_seat'] + $shop['hp_max_seat'];
    $max_seat_fn = $shop['max_seat'];
    $max_seat_hp = $shop['hp_max_seat'];
    $shop_open_time = $shop['open_time'];
    $shop_close_time = $shop['close_time'];
    
    // Get max_seat 
    /*
    $max_seat = 0;
    $max_seat_fn = 0;
    $max_seat_hp = 0;
    if (!isset($this->AppUI->total_max_seat)) {
        $shop = Api::call(Configure::read('API.url_shops_detail'), array('id' => $this->AppUI->shop_id));
        if (empty(Api::getError()) && !empty($shop)) {
            $max_seat = $shop['max_seat'] + $shop['hp_max_seat'];
            $max_seat_fn = $shop['max_seat'];
            $max_seat_hp = $shop['hp_max_seat'];
        }
    } else {
        $max_seat = $this->AppUI->total_max_seat;
        $max_seat_fn = $this->AppUI->shop_max_seat;
        $max_seat_hp = $this->AppUI->shop_hp_max_seat;
    }

    $max_real_seat = 0;
    $count = Api::call(Configure::read('API.url_orders_getmaxseatrealtime'),  array(
            'shop_id' => $this->AppUI->shop_id,
            'date' => $dt
        ));
    if (empty(Api::getError()) && !empty($count)) {
        $max_real_seat = $count;
    } else {
        $max_real_seat = $this->AppUI->shop_max_seat + $this->AppUI->shop_hp_max_seat;
    }

    list($count, $order_timely_limit) = Api::Call(
        Configure::read('API.url_order_timely_limit_list'),
        array(
            'shop_id' => $this->AppUI->shop_id,
            'date' => $dt,           
        )
    );   
    * 
    */
} else {
    $staffRecord = MasterData::nailists_all($this->AppUI->shop_id);
    $data = Api::call(Configure::read('API.url_orders_date_calendar'), $param, false, array());
    $this->Common->handleException(Api::getError());
    $shop_open_time = $this->AppUI->shop_open_time;
    $shop_close_time = $this->AppUI->shop_close_time;
}
$deviceRecord = MasterData::devices_all($this->AppUI->shop_id);
$this->set('shop_open_time', getdate(strtotime($dt) + $shop_open_time));
$this->set('shop_close_time', getdate(strtotime($dt) + $shop_close_time));
$this->set(compact(
        'staffRecord', 
        'deviceRecord', 
        'max_seat',
        'max_real_seat', 
        'max_seat_fn', 
        'max_seat_hp', 
        'order_timely_limit', 
        'data', 
        'dt', 
        'controller', 
        'action', 
        'is_edit_seat'
    )
);
