<?php
$date='';
if (!empty($this->request->data['reservation'])) {
	$reservation = $this->request->data['reservation'];
	$time_from = date('Y-m-d H:i:s', $reservation['start_at_epoch']);
	if(empty($reservation['duration'])){
		$reservation['duration'] = 900;
	}
	$time_to = date('Y-m-d H:i:s', $reservation['start_at_epoch'] + $reservation['duration']);
	$shop_max_seat = $this->AppUI->shop_max_seat;
	$date = date('Y-m-d', $reservation['start_at_epoch']);
	$param = array(
			'shop_id' => $this->AppUI->shop_id,
			'time_from' => $time_from,
			'time_to' => $time_to,
	);
	
	$seat_fn_ids = array();
	$seat_hp_ids = array();
	foreach($reservation['stylist_ids'] as $id){
		$ex_id = explode('_', $id);
		if($ex_id[0] == 's'){
			if($ex_id[1] <= $shop_max_seat){
				$seat_fn_ids[] = $ex_id[1]; 
			}else{
				$seat_hp_ids[] = $ex_id[1];
			}	
		}
	}
	
	if(!empty($seat_fn_ids)){
		$max = max($seat_fn_ids);
		$param['decrease_fn'] = count($seat_fn_ids);
		$param['to_row_fn'] = $max;
	}
	
	if(!empty($seat_hp_ids)){
		$max = max($seat_hp_ids);
		$hp_max = $max - $shop_max_seat;
		for($i=1; $i<=$hp_max; $i++){
			$arr_hp[]=$i;
		}
		$param['decrease_hp'] = count($seat_hp_ids);
		$param['to_row_hp'] = max($arr_hp);
	}
	
	
	$result = Api::call(Configure::read('API.url_ordertimelylimits_decreaselimit'), $param);
	if(is_bool($result) && $result == true){	
		echo json_encode(array('msg' => 'OK', 'date' => $date));
		exit;
	}
}
echo json_encode(array('msg' => 'FAILED', 'date' => $date));
exit;



