<?php
//$this->layout = 'bookingform';
$modelName = $this->User->name;
$model = $this->{$modelName};

//make list of year, month, day
$listYears = range(date("Y") - 100, date("Y"));
$listMonths = range(1, 12);
$listDays = range(1, 31);

$listPrefecture = $this->Common->arrayKeyValue(MasterData::prefectures_all(), 'id', 'name');
$this->set(compact('listYears', 'listMonths', 'listDays', 'listPrefecture'));

//In case of updating
if(!empty($this->request->query['cus_id'])){
    $param['id'] = $this->request->query['cus_id'];
    $data['User'] = Api::call(Configure::read('API.url_users_detail'), $param);
    $data['birthday_year'] = date('Y', $data['User']['birthday']);
    $data['birthday_month'] = date('m', $data['User']['birthday']);
    $data['birthday_day'] = date('d', $data['User']['birthday']);
//    $phoneSeparated = explode('-', $data['User']['phone']);
//    if(count($phoneSeparated) == 3){
//        list($data['User']['phone_1'], $data['User']['phone_2'], $data['User']['phone_3']) = $phoneSeparated;
//    }
    $data['User']['phone_1'] = substr($data['User']['phone'], 0, 3);
    $data['User']['phone_2'] = substr($data['User']['phone'], 3, 4);
    $data['User']['phone_3'] = substr($data['User']['phone'], 7, 4);

    $this->set('data', $data);
    $this->set('id', $param['id']);
}else{
    $this->set('data', $this->data);
    if(isset($this->data['User']['id'])){
        $this->set('id', $this->data['User']['id']);
    }
}

$errors = array();
if ($this->request->is('post')) {
    $param = $this->getData($modelName);

    //Validate phone
    $arrPhoneLen = array(
        'phone_1' => 3,
        'phone_2' => 4,
        'phone_3' => 4
    );

    if (!empty($param[$modelName]['phone_1']) || !empty($param[$modelName]['phone_2'])
        || !empty($param[$modelName]['phone_3'])) {
        $index = 1;
        foreach ($arrPhoneLen as $key => $len) {
            if (strlen($param[$modelName][$key]) < $len || !is_numeric($param[$modelName][$key])) {
                $errors[$key][] = __("Phone {$index} must contain {$len} numerics");
            }
            $index++;
        }
    } else {
        $errors['phone_1'][0] = __("Phone can not empty");
    }

    //birthday has three parts year, month and day
    $param[$modelName]['birthday'] = "{$this->data['birthday_year']}-"
        . str_pad($this->data['birthday_month'], 2, 0, STR_PAD_LEFT) . '-'
        . str_pad($this->data['birthday_day'], 2, 0, STR_PAD_LEFT);

    if ($model->validateRegister($param) && empty($errors)) { //Validate parameters
        //Call API to register
        $param[$modelName]['phone'] = "{$param[$modelName]['phone_1']}{$param[$modelName]['phone_2']}{$param[$modelName]['phone_3']}";
        $id = Api::call(Configure::read('API.url_users_addupdate'), $param[$modelName]);
        if (Api::getError()) {
           if (isset(Api::getError()['email'])) {
                $errors = array(
                    'email' => array(
                        1011 => __('Email address is already registered'),
                        1021 => __('Email address is already registered and waiting activation'),
                    )
                );
            }
        } else {
            //Successfull
            $this->set('msg',  __('Registration successful'));
            $this->set('id', $id);
            $param['id'] = $id;
            $data['User'] = Api::call(Configure::read('API.url_users_detail'), $param);
            $data['birthday_year'] = date('Y', $data['User']['birthday']);
            $data['birthday_month'] = date('m', $data['User']['birthday']);
            $data['birthday_day'] = date('d', $data['User']['birthday']);
            //list($data['User']['phone_1'], $data['User']['phone_2'], $data['User']['phone_3']) = explode('-', $data['User']['phone']);
            $data['User']['phone_1'] = substr($data['User']['phone'], 0, 3);
            $data['User']['phone_2'] = substr($data['User']['phone'], 3, 4);
            $data['User']['phone_3'] = substr($data['User']['phone'], 7, 4);
            $this->set('data', $data);
        }
    } else {
        //validation errors
        $errors +=  $model->validationErrors;
    }
}
//pass error messages to view
$this->set('errors', $errors);

