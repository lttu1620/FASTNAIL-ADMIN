<?php

$query = $this->getParams();
$q = explode("-*-", $query ['q']);
$param = array();
foreach ($q as $dt) {
    $p = explode('=', $dt);
    if ($p[1] != '') {
        $param [$p[0]] = $p[1];
    }
} 

if (empty($param)) {
    echo json_encode(array('count' => 0,
        'customers' => array()
    ));
    exit;
}
$param['limit'] = 10;
$param['is_franchise'] = !empty($this->AppUI->shop_is_franchise) ? 1 : 0;
$param['shop_id'] = !empty($this->AppUI->shop_id) ?  $this->AppUI->shop_id : 0;
$result = Api::call(Configure::read('API.url_users_searchinfo'), $param, false, array(0, array()));
if (Api::getError()) {
    AppLog::info("API.url_users_searchinfo", __METHOD__, $param);
    return $this->Common->setFlashErrorMessage(Api::getError());
}
$data = array(
    'count' => count($result[1]),
    'customers' => $result[1]
);
echo json_encode($data);
exit;
