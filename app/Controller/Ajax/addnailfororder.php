<?php
if (!empty($_POST['order_id']) && !empty($_POST['nail_id'])) {//add nail for order
    $params = array(
        'order_id' => $_POST['order_id'],
        'nail_id' => $_POST['nail_id'],
        'update_order_prices' => 1);

    $res = Api::call(Configure::read('API.url_ordernails_add'), $params);
    if (Api::getError()) {
        AppLog::info("API.url_ordernails_add failed", __METHOD__, $params);
        $apiErrors = Api::getError();
        $arrErrors = array();
        foreach ($apiErrors as $field => $errs) {
            foreach ($errs as $code => $err) {
                $arrErrors[] = __($err);
            }
        }
        print_r(json_encode(array('status' => 'NG', 'message' =>  implode("\n", $arrErrors))));
    }else{
        $nail = Api::call(Configure::read('API.url_nails_detail'), array('id' => $_POST['nail_id']));
        print_r(json_encode(
            array(
                'status' => 'OK',
                'image_url' => $nail['image_url'],
                'photo_cd' => $nail['photo_cd'])));
    }
}
exit;
