<?php
$param = $this->getParams();
$undo_data = $this->Session->read('StopWatchUndo_'.$param['order_id']);
if(!empty($undo_data)){
	//AppLog::info("DATA For UndoStopwatch_".$param['order_id'], __METHOD__, $undo_data);
	$result=Api::call(Configure::read('API.url_orders_undostopwatch'), $undo_data);
	//AppLog::info("UndoStopwatch result", __METHOD__, $result);
	if(is_array($result) && isset($result['before']['order_id'])){
		//AppLog::info("UndoStopwatch result OK", __METHOD__, array());
		$this->Session->delete('StopWatchUndo_'.$param['order_id']);
		$result['msg'] = 'OK';
		echo json_encode($result);
		exit;
	}	
}
echo json_encode(array('msg' => 'FAILED'));
exit; 