<?php
$params = $this->getParams();
if ($this->request->is('post') 
    && isset($params['order_id'])
    && isset($params['attribute_id'])
    && isset($params['attribute_value'])) {
    $params[$params['attribute_id']] = $params['attribute_value'];    
    $params['get_atttibute'] = 1;    
    $data = Api::Call(Configure::read('API.url_order_update_attribute'), $params);
    $error = 0;
    if (Api::getError()) {
        $message = "<div class=\"alert alert-success alert-dismissable\"><h4><i class=\"icon fa fa-warning\"></i>" . __("System error, please try again") . "</h4></div>";      
        $data = '';
        $error = 1;
    } else {
        $message = "<div class=\"alert alert-success alert-dismissable\"><h4><i class=\"icon fa fa-check\"></i>" . __('Data saved successfully') . "</h4></div>"; 
        $data = !empty($data) ? implode(', ', $this->Common->arrayValues($data, 'name')) : '';
    }   
    echo json_encode(array(
        'message' => $message,
        'data' => $data,
        'error' => $error,
    ));
    exit;
}
$listAttributes = Api::Call(Configure::read('API.url_nails_attribute'), $params);
$this->UpdateForm
    ->addElement(array(
            'id' => 'attribute_id',
            'label' => '素材カテゴリ',
            'options' => array(                
                'color_jell_id' => 'カラージェル',
                'rame_jell_id' => 'ラメジェル',
                'stone_id' => 'ストーン',
                'bullion_id' => 'ブリオン',
                'hologram_id' => 'ホログラム',
                'shell_id' => 'シエル',
                'flower_id' => '押花',
                'powder_id' => 'パウダー',
                'paint_id' => 'ペイント',
            ),            
            'value' => $params['attribute_id'],
            'onchange' => "showGroupAttr({$params['order_id']})"
        )
    );
if (!empty($listAttributes)) {
    foreach ($listAttributes as $name => $dt) {
        $checkArr = $this->Common->arrayFilter($dt, 'checked', '1');
        $selectedArr = $this->Common->arrayValues($checkArr, 'id');
        $options = $this->Common->arrayKeyValue($dt, 'id', 'name');
        $style = '';
        $atrrId = substr($name, 0, strlen($name) - 1) . '_id';
        if ($atrrId !== $params['attribute_id']) {
            $style = "style='display:none;'";
        }
        $this->UpdateForm
            ->addElement(array(
                'id' => $name,
                'type' => 'select',
                'multiple' => 'checkbox',
                'label' => __(ucfirst($name)),
                'options' => $options,
                'selected' => $selectedArr,
                'before' => "<div class='group-attribute' id='attribute_{$atrrId}' {$style}>",
                'between' => "<div class='group-checkbox'>",
                'after' => "</div><div class=\"cls\"></div></div>",
            )
        );
    }
}
$this->UpdateForm
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
        'onclick' => "return saveAttribute({$params['order_id']}, {$params['nail_id']});",
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => '閉じる',
        'class' => 'btn pull-left',
        'data-dismiss' => 'modal',
    	'after' => " <div class='result_sumary'> データの保存ができました。</div>"
    ));

$this->set('updateForm', $this->UpdateForm);