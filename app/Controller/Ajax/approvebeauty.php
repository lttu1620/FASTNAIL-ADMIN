<?php

$param = $this->getParams();
$result = Api::call(Configure::read('API.url_approve_beauty'), $param);
if (Api::getError()) {
    AppLog::info("Approve beauty failed", __METHOD__, $param);
    $error = array(
        'update' => array(
            1021 => __("System error, please try again")
        )
    );
    $this->Common->setFlashErrorMessage(Api::getError(), $error);
} else {
    $this->Common->setFlashSuccessMessage(__("Data updated successfully"));
}
exit;