<?php
if (!empty($_GET['id'])) {
    $itemDetail = Api::Call(Configure::read('API.url_items_detail'), array('id' => $_GET['id']));
    $response = "<li>
        <div class=\"title fLeft\"><span class=\"delButton\"><a href=\"javascript:void(0);\" onclick=\"javascript:removeItem({$itemDetail['id']})\" class=\"mR5\">X</a></span>{$itemDetail['name']}</div>
        <div class=\"info fRight orderItem\">
            <span class=\"priceAll\"><span class=\"item_price\" id=\"{$itemDetail['id']}\">" . ($itemDetail['price']) . "</span>円</span><span class=\"f11px\">(税抜) ×</span>
            <span class=\"mL5\">
                <input type=\"text\" name=\"quality\" class=\"W50 ordered_item tbQty qty-input onlyNumber\" id=\"item_{$itemDetail['id']}\" value=\"1\"><span>本</span>
                <script>
                    jQuery(function($){
                        $('#item_{$itemDetail['id']}').keyboard({
                            layout: 'custom',
                            customLayout: {
                                'default' : [
                                    '{clear} {bksp}',
                                    '7 8 9',
                                    '4 5 6',
                                    '1 2 3',
                                    '0 {a} {c}'
                                ]
                            },
                            restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
                            preventPaste : true,  // prevent ctrl-v and right click
                            autoAccept : true
                        });
                    });
                </script>
                <span class=\"arrowBlock\">
                    <a href=\"javascript: upQtyClick({$itemDetail['id']});\" class=\"upQty\"></a>
                    <a href=\"javascript: downQtyClick({$itemDetail['id']});\" class=\"downQty\"></a>
                </span>
            </span>
        </div>
    </li>";

    echo $response;
}
exit;