<?php

$modelName = $this->Ajax->name;
if (!empty($comment_id) && !empty($user_id)) {
    $param['news_comment_id'] = $comment_id;
    $param['user_id'] = $user_id;
    $result = '';
    if ($_POST['status'] == 0) {
	// call api 'add comment like'
	$result = Api::call(Configure::read('API.url_newscommentlikes_add'), $param);
	if (Api::getError()) {
	    AppLog::info("API.url_newscommentlikes_add failed", __METHOD__, $param);
	    return $this->Common->setFlashErrorMessage(Api::getError());
	}
    } else {
	// call api disable comment
	$result = Api::call(Configure::read('API.url_newscommentlikes_disable'), $param);
	if (Api::getError()) {
	    AppLog::info("API.url_newscommentlikes_disable failed", __METHOD__, $param);
	    return $this->Common->setFlashErrorMessage(Api::getError());
	}
    }

    // get comments of feed
    $request = array(
	'page' => $_POST['page'],
	'limit' => $_POST['limit'],
	'user_id' => $user_id,
	'news_feed_id' => $_POST['feedId'],
	'sort' => 'created-desc'
    );
    list($total, $comments) = Api::call(Configure::read('API.url_newscomments_list'), $request);
    if (Api::getError()) {
	AppLog::info("API.url_newscomments_list failed", __METHOD__, $request);
	return $this->Common->setFlashErrorMessage(Api::getError());
    }
    $html = "";
    if (!empty($comments)) {
	foreach ($comments as $cm) {
	    $html.= "<div class=\"item news-feed-item-detail\">";
	    $image = ($cm['is_company'] == 1) ? $cm['thumbnail_img'] : $cm['image_url'];
	    $html.="<img src=\"{$this->Common->thumb($image, '60x60')}\" alt=\"user image\" class=\"online\"/>";
	    $html.="<a href=\"javascript:void(0);\" class=\"co-likes\" onclick=\"likeComment({$cm['id']},{$user_id},{$_POST['feedId']},{$cm['is_like']},{$_POST['page']},{$_POST['limit']});\">";
	    if ($cm['is_like'] == 0) {
		$html.="<i class=\"fa fa-likes-o\"></i>";
	    } else {
		$html.="<i class=\"fa fa-likes\"></i>";
	    }
	    $html.="{$cm['like_count']}</a>";
	    $html.="<p class=\"message\">";
	    $html.="<a href=\"#\" class=\"name\">{$cm['name']}</a>{$cm['comment']}";
	    $html.="</p></div>";
	}
    }
    echo $html;
} else {
    echo "";
}
exit;
