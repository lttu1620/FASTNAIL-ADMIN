<?php

if (!empty($this->request->data['userlogs'])) {
    $userlogs = $this->request->data['userlogs'];
    if (!empty($userlogs['memo'])) {
        $id = Api::call(Configure::read('API.url_userlogs_addupdate'), $userlogs, false, array(0, array()));
        $this->Common->handleException(Api::getError());
        if (!empty($id)) {
        	//cal api
        	$param['order_id'] = $userlogs['order_id'];
        	$param['limit'] = $userlogs['cur_limit'];
        	$param['page'] = 1;
        	$listLog = Api::call(Configure::read('API.url_userlogs_list'), $param, false, array(0, array()));
        	$strLogHtml = '';
        	foreach($listLog[1] as $log){
        		$strLogHtml.='<div class="sectionSub radius5 border">
								<div class="avatar"><img src="'.$log['nailist_image'].'" ></div>
								<div class="textNote">'.nl2br($log['memo']).'</div>
								<div class="name">
									<div>'.$log['shop_name'].'</div>
									<div>'.$log['nailist_name'].'</div>
								</div>
								<div class="time">'.date("Y.m.d", $log['created']).'</div>
							</div>';
        	}
        	echo $strLogHtml."---".$param['limit']."---".$listLog[0];
            exit;
        }
    }
}
echo 'Failed';
exit;


