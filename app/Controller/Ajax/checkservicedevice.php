<?php
if(!empty($_POST['services'])){
	$param = $_POST['services'];
    //in case of service time == 0
    if($param['order_start_date'] == $param['order_end_date']) {
        $param['order_end_date'] += !empty($this->AppUI->shop_is_plus) ? Configure::read('Config.DefaulPlusShopTime') : Configure::read('Config.DefaulNormalShopTime');
    }
	$result = Api::call(Configure::read('API.url_devices_all'), $param);
	if (Api::getError()) {
		AppLog::info("API.url_devices_all", __METHOD__, $param);
		return $this->Common->setFlashErrorMessage(Api::getError());
	}
}

if(!empty($result)){
    echo json_encode(array('message' => 'OK', 'device' => $result[0]));
}else{
    echo json_encode(array('message' => 'Busy', 'device' => array()));
}
exit;
