<?php
$params = $this->getParams();
/*if ($this->request->is('post') 
    && isset($params['order_id'])
    && isset($params['item_id'])
    && isset($params['item_value'])) {
    $params[$params['item_id']] = $params['item_value'];    
    $params['get_atttibute'] = 1;    
    $data = Api::Call(Configure::read('API.url_order_update_attribute'), $params);
    $error = 0;
    if (Api::getError()) {
        $message = "<div class=\"alert alert-success alert-dismissable\"><h4><i class=\"icon fa fa-warning\"></i>" . __("System error, please try again") . "</h4></div>";      
        $data = '';
        $error = 1;
    } else {
        $message = "<div class=\"alert alert-success alert-dismissable\"><h4><i class=\"icon fa fa-check\"></i>" . __('Data saved successfully') . "</h4></div>"; 
        $data = !empty($data) ? implode(', ', $this->Common->arrayValues($data, 'name')) : '';
    }   
    echo json_encode(array(
        'message' => $message,
        'data' => $data,
        'error' => $error,
    ));
    exit;
}
*/
$itemSection = Configure::read('Config.ItemSection');
unset($itemSection[5]);
$this->UpdateForm
    ->addElement(array(
            'id'        => 'attribute_id',
            'label'     => '素材カテゴリ',
            'options'   => $itemSection,
            'value'     => $params['item_id'],
            'onchange'  => "showGroupAttr({$params['order_id']})"
        )
    );
$items = MasterData::items_all();

$listItems = array();
foreach ($itemSection as $sectionId => $sectionName) {
    $listItems[$sectionId]['name'] = $sectionName;
    $listItems[$sectionId]['item'] = $this->Common->arrayFilter($items, 'item_section', $sectionId);
} 
if (!empty($listItems)) {
    foreach ($listItems as $id => $dt) {       
        $selectedArr = isset($params['checked'])? $params['checked']: array();        
        $options =$this->Common->arrayKeyValue($dt['item'], 'id', 'name');
        $style = '';
        $atrrId = $id;
        if ($atrrId != $params['item_id']) {
            $style = "style='display:none;'";
        }
        $this->UpdateForm
            ->addElement(array(
                'id' => 'section_' . $id,                
                'type' => 'select',
                'multiple' => 'checkbox',
                'label' => $dt['name'],
                'options' => $options,
                'selected' => $selectedArr,
                 'before' => "<div class='group-attribute' id='attribute_{$atrrId}' {$style}>",
                'between' => "<div class='group-checkbox'>",
                'after' => "</div><div class=\"cls\"></div></div>",
            )
        );
    }
}
$this->UpdateForm
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
        'onclick' => "return saveItems({$params['order_id']});",
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => '閉じる',
        'class' => 'btn pull-left',
        'data-dismiss' => 'modal',
        'after' => " <div class='result_sumary'> 保存しました。</div>"
    ));

$this->set('updateForm', $this->UpdateForm);