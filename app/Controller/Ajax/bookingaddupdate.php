<?php
$modelName = $this->Order->name;
$model = $this->{$modelName};
//get submit data
if (isset($_POST['reservation'])) {
    //validate user information
    $validateData = $_POST['reservation']['customer'];
    //if have email but user_name is empty then show validation message
    if(!empty($_POST['reservation']['customer']['email']) && empty($_POST['reservation']['customer']['user_name'])){
        echo json_encode(array('message' => 'Validate', 'request_date' => array('user_name' => '名字または名前を入力してください')));
        exit;
    }
    // remove empty fields
    foreach($validateData as $key => $value){
        if(empty($value)){
            unset($validateData[$key]);
        }
        if($key == 'phone'){
        	$validateData[$key] = str_replace(array('_',' '),"",$value);
        }
    }
    // just validate not empty fields
    if(count($validateData) > 0 && !$model->validateOrderInsertUpdateCalendar($validateData)){
        $errors =  $model->validationErrors;
        echo json_encode(array('message' => 'Validate', 'request_date' => $errors));
        exit;
    }
    //create nailist and device ids
    $nailist_ids = "";
    $device_ids = "";
    if(!empty($_POST['reservation']['stylist_seats_attributes'])){
    	foreach ($_POST['reservation']['stylist_seats_attributes'] as $stylist) {
    		if (!empty($stylist['stylist_id']) && $stylist['_destroy'] == 'false') {
    			$nailist_ids.= substr($stylist['stylist_id'],2) . ",";
    		}
    	}
    	$nailist_ids = rtrim($nailist_ids, ',');
    }
    if(!empty($_POST['reservation']['device_seats_attributes'])){
    	foreach ($_POST['reservation']['device_seats_attributes'] as $device) {
    		if (!empty($device['device_id']) && $device['_destroy'] == 'false') {
    			$device_ids.= substr($device['device_id'],2) . ",";
    		}
    	}
    	$device_ids = rtrim($device_ids, ',');
    }
    
    //create services ids
    $services_ids = !empty($_POST['reservation']['services']['ids']) ? implode(",", $_POST['reservation']['services']['ids']) : "";
    
    //create end date
    $duration = !empty($_POST['reservation']['duration']) ? $_POST['reservation']['duration'] : $_POST['reservation']['duration_disable'];
    if($duration == 0){
    	$duration =  !empty($this->AppUI->shop_is_plus) ? Configure::read('Config.DefaulPlusShopTime') : Configure::read('Config.DefaulNormalShopTime');
    } 
    $order_end_date = date( 'Y-m-d H:i', strtotime($_POST['reservation']['start_at']) + $duration) ;

    //prepare data for api
    $param = array(
        'id' => !empty($_POST['reservation']['order_id']) ? $_POST['reservation']['order_id'] : '',
        'shop_id' => $this->AppUI->shop_id,
        'admin_id' => $this->AppUI->id,
        'last_update_admin_id' => $this->AppUI->id,
        'reservation_date' => $_POST['reservation']['start_at'],
        'order_start_date' => $_POST['reservation']['start_at'],
        'order_end_date' => $order_end_date,
        'visit_section' => !empty($_POST['reservation']['visit_section']) ? $_POST['reservation']['visit_section'] : '0',
        'visit_element' => !empty($_POST['reservation']['visit_element']) ? $_POST['reservation']['visit_element'] : '0',
    	'is_seat' => !empty($_POST['reservation']['is_seat']) ? $_POST['reservation']['is_seat'] : '0', 	
    	'request' => !empty($_POST['reservation']['request']) ? $_POST['reservation']['request'] : '',
        'reservation_type' => !empty($_POST['reservation']['type']) ? $_POST['reservation']['type'] : '0',
        'nailist_id' => $nailist_ids,
        'devices_id' => $device_ids,
        'user_name' => $_POST['reservation']['customer']['user_name'],
        'sex' => $_POST['reservation']['customer']['sex'],
        'email' => $_POST['reservation']['customer']['email'],
        'phone' => str_replace(array('_',' '),"",$_POST['reservation']['customer']['phone']),
        'user_id' => $_POST['reservation']['user_id'],
        'shop_open_time' => $this->AppUI->shop_open_time,
        'shop_close_time' => $this->AppUI->shop_close_time,
    	'service_id' =>	$services_ids,
    	'estimate_time' => $_POST['reservation']['estimate_duration'],
        'default_duration_shop' => !empty($this->AppUI->shop_is_plus) ? Configure::read('Config.DefaulPlusShopTime') : Configure::read('Config.DefaulNormalShopTime')
    );

    $order_id = Api::call(Configure::read('API.url_orders_update_calendar'), $param);
    if (Api::getError()) {
        AppLog::info("API.url_orders_update", __METHOD__, $param);
        return $this->Common->setFlashErrorMessage(Api::getError());
    }
    $request_date = date( 'Y-m-d', strtotime($_POST['reservation']['start_at']));
    if(is_numeric($order_id) && $order_id > 0){
        echo json_encode(array('message' => 'OK', 'request_date' => $request_date));
    }elseif($order_id == -1){
        echo json_encode(array('message' => 'Duplicated', 'request_date' => $request_date));
    }else{//$order_id == -2
        echo json_encode(array('message' => 'OutOfTimeRange', 'request_date' => $request_date));
    }
}else{
    echo json_encode(array('message' => 'Failed', 'request_date' => $request_date));
}
exit;



