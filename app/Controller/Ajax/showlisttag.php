<?php
$modelName = $this->Ajax->name;
$tags = MasterData::tags_all();
if ($tags) {
	$html = "<select id=\"select-tag-id-{$_POST['feedId']}\" onchange=\"addfeedtag({$_POST['feedId']},this.value)\">";
	$html .= "<option value=\"\">".__("-- Select tag --")."</option>";
	foreach($tags as $tag){
		$html .= "<option value=\"{$tag['id']}-{$tag['color']}\">{$tag['name']}</option>";
	}
	$html .= "</select>";
	
	echo $html;
}else{
	echo 'empty';
}
exit;
