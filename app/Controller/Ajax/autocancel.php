<?php
$param = $this->data;
$param['autocancel']= $param['value'];
unset($param['value']);
$result = Api::call("{$param['controller']}/autocancel", $param);
if (empty($result) && !Api::getError()) {
    AppLog::warning("Can not update", __METHOD__, $param);
    echo __("System error, please try again");
}
exit;