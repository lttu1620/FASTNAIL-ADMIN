<?php
$param['order_id'] = $order_id;
$param['limit'] = Configure::read('Config.LogLimit');
$param['page'] = 1;
// get nailist for shop
$listNailist = MasterData::nailists_all($this->AppUI->shop_id);
//get order detail
$order = Api::call(Configure::read('API.url_orders_detail'), array('id' => $order_id), false, array(0, array()));
$this->Common->handleException(Api::getError());
//get log list
$listLog = Api::call(Configure::read('API.url_userlogs_list'), $param, false, array(0, array()));
//set data for view
$this->set('listLog', $listLog[1]);
$this->set('total', $listLog[0]);
$this->set('user_id', !empty($order['user_id']) ? $order['user_id'] : 0);
$this->set('listNailist', !empty($listNailist) ? $listNailist : array());
$this->set('order_id', $order_id);
$this->set('cur_limit', $param['limit']);



