<?php
$param = $this->data;
$param['is_admin']= $param['value'];
unset($param['value']);
$result = Api::call("{$param['controller']}/isAdmin", $param);
if (empty($result) && !Api::getError()) {
    AppLog::warning("Can not update", __METHOD__, $param);
    echo __("System error, please try again");
}
exit;