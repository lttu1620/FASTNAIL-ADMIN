<?php

App::uses('AppController', 'Controller');

/**
 * AjaxController class of Ajax Controller
 *
 * @package	Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class AjaxController extends AppController {

    public $uses = array('User', 'Order');

    /** @var string $layout layout name for view */
    public $layout = "ajax";

    /**
     * Handles user interaction of view fblogin Ajax.
     *
     * @return void
     */
    public function fblogin() {
        include ('Ajax/fblogin.php');
    }

    /**
     * Handles user interaction of view fblogintoken Ajax.
     *
     * @return void
     */
    public function fblogintoken() {
        include ('Ajax/fblogintoken.php');
    }

    /**
     * Handles user interaction of view university Ajax.
     *
     * @param integer $id ID value of university default value is 0.
     *
     * @return void
     */
    public function university($id = 0) {
        include ('Ajax/university.php');
    }

    /**
     * Handles user interaction of view likecomment Ajax.
     *
     * @param integer $comment_id ID value of news_comments, default value is 0.
     * @param integer $user_id ID value of user, default value is 0.
     *
     * @return void
     */
    public function likecomment($comment_id = 0, $user_id = 0) {
        include ('Ajax/likecomment.php');
    }

    /**
     * Handles user interaction of view newscommentlikes Ajax.
     *
     * @param integer $id ID value of news_comments default value is 0.
     *
     * @return void
     */
    public function newscommentlikes($id = 0) {
        include ('Ajax/newscommentlikes.php');
    }

    /**
     * Handles user interaction of view autocompleteuniversity Ajax.
     *
     * @return void
     */
    public function autocompleteuniversity() {
        $search = $this->getParam('q', '');
        $data = MasterData::universities_all_key_value();
        $search = strtolower($search);

        $result = array();
        foreach ($data as $key => $value) {
            if (strpos(strtolower($value), $search) !== false) {
                $result[] = array(
                    'id' => $key,
                    'label' => $value
                );
            }
        }
        echo $this->getParam('callback') . '(' . json_encode($result) . ')';

        exit;
    }

    /**
     * Handles user interaction of view autocomplete Ajax.
     *
     * @return void
     */
    public function autocomplete() {
        $search = $this->getParam('q', '');
        $data = array(
            array('id' => 1, 'label' => 'Thai'),
            array('id' => 2, 'label' => 'Quan'),
            array('id' => 3, 'label' => 'Manh'),
            array('id' => 4, 'label' => 'Kino'),
            array('id' => 5, 'label' => 'Dien'),
            array('id' => 6, 'label' => 'Tu'),
            array('id' => 7, 'label' => 'Tuan'),
            array('id' => 8, 'label' => 'Truong'),
            array('id' => 9, 'label' => 'Huy'),
            array('id' => 10, 'label' => 'Thang'),
            array('id' => 11, 'label' => 'Hieu'),
        );
        $search = strtolower($search);
        $result = array();
        foreach ($data as $item) {
            if (strpos(strtolower($item['label']), $search) !== false) {
                $result[] = $item;
            }
        }
        echo $this->getParam('callback') . '(' . json_encode($result) . ')';
        exit;
    }

    public function dialog() {
        include ('Ajax/dialog.php');
    }

    /**
     * Handles user interaction of view deletefeedtag Ajax.
     *
     * @return void
     */
    public function deletefeedtag() {
        include ('Ajax/deletefeedtag.php');
    }

    /**
     * Handles user interaction of view showlisttag Ajax.
     *
     * @return void
     */
    public function showlisttag() {
        include ('Ajax/showlisttag.php');
    }

    /**
     * Handles user interaction of view addfeedtag Ajax.
     *
     * @return void
     */
    public function addfeedtag() {
        include ('Ajax/addfeedtag.php');
    }

    public function cropimagedialog() {

    }

    /**
     * Handles user interaction of view cropimage Ajax.
     *
     * @return void
     */
    public function cropimage() {
        include ('Ajax/cropimage.php');
    }

    /**
     * Handles user interaction of view resendregisteremail Ajax.
     *
     * @return void
     */
    public function resendregisteremail() {
        include ('Ajax/resendregisteremail.php');
    }

    /**
     * Handles user interaction of view resendforgetpassword Ajax.
     *
     * @return void
     */
    public function resendforgetpassword() {
        include ('Ajax/resendforgetpassword.php');
    }

    /**
     * Handles user interaction of view resendregistercompany Ajax.
     *
     * @return void
     */
    public function resendregistercompany() {
        include ('Ajax/resendregistercompany.php');
    }

    /**
     * Processing batch.
     *
     * @return void
     */
    public function runbatch() {
        include ('Ajax/runbatch.php');
    }

    /**
     * Handles user interaction of view disable Ajax.
     *
     * @return void
     */
    public function disable() {
        include ('Ajax/disable.php');
    }

    /**
     * Handles user interaction of view admin Ajax.
     *
     * @return void
     */
    public function admin() {
        include ('Ajax/admin.php');
    }

    /**
     * Handles user interaction of view approved Ajax.
     *
     * @return void
     */
    public function approved() {
        include ('Ajax/approved.php');
    }

    /**
     * Handles user interaction of view booking dialog Ajax.
     *
     * @return void
     */
    public function bookingdialog() {
        include ('Ajax/bookingdialog.php');
    }

    /**
     * Handles user interaction of view booking dialog Ajax.
     *
     * @return void
     */
    public function bookingaddupdate() {
        include ('Ajax/bookingaddupdate.php');

    }

    /**
     * Handles user interaction of view booking dialog Ajax.
     *
     * @return void
     */
    public function bookingdelete() {
        include ('Ajax/bookingdelete.php');
    }

    /**
     * Get nailists lit when changing shop
     *
     * @return string html
     */
    public function shownailistsofshop() {
        include ('Ajax/shownailistsofshop.php');
    }

    /**
     * Load calendar lit when changing shop
     *
     * @return string html
     */
    public function loadcalendar() {
        include ('Ajax/calendar.php');
    }

    /**
     * Load calendar lit when changing shop
     *
     * @return string html
     */
    public function updatetimebar() {
        include ('Ajax/updatetimebar.php');
    }

    /**
     * customers_json action
     *
     * @author diennvt
     * @return string json format
     */
    public function customers_json() {
    	include ('Ajax/customerjson.php');
    }

    /**
     * Handles user interaction of view custumer dialog Ajax.
     *
     * @return void
     */
    public function customerdialog() {
        include ('Ajax/customerdialog.php');
    }

    /**
     * Check new order for calendar
     *
     * @return string html
     */
    public function checkneworder() {
        include ('Ajax/checkneworder.php');
    }

    /**
     * Handles user interaction of view approved Ajax.
     *
     * @return void
     */
    public function plus() {
        include ('Ajax/plus.php');
    }

    /**
     * Handles user interaction of view custumer dialog Ajax.
     *
     * @return void
     */
    public function medicalchartdialog($orderId='') {
        include ('Ajax/medicalchartdialog.php');
    }

    /**
     * Get nailists lit when changing shop
     *
     * @return string html
     */
    public function showDevicesOfShop() {
        include ('Ajax/showdevicesofshop.php');
    }

    /**
     * Handles user interaction of view approved Ajax.
     *
     * @return void
     */
    public function stopwatchdialog() {
        include ('Ajax/stopwatchdialog.php');
    }
    
    /**
     * Handles user interaction of view approved Ajax.
     *
     * @return void
     */
    public function undostopwatchdialog() {
    	include ('Ajax/undostopwatchdialog.php');
    }

    /**
     * Handles user interaction of view customer dialog Ajax.
     *
     * @return void
     */
    public function orderslog($order_id ='') {
        include ('Ajax/orderslog.php');
    }

    /**
     * Handles user interaction of view customer dialog Ajax.
     *
     * @return void
     */
    public function orderslogupdate() {
        include ('Ajax/orderslogupdate.php');
    }

    /**
     * Handles user interaction of view customer dialog Ajax.
     *
     * @return void
     */
    public function orderslogreadmore($order_id ='') {
    	include ('Ajax/orderslogreadmore.php');
    }

    /**
     * Handles user interaction of view booking dialog Ajax.
     *
     * @return void
     */
    public function bookingdialog1() {
        include ('Ajax/bookingdialog1.php');
    }

    public function orderDailyLimit() {
        include ('Ajax/orderdailylimit.php');
    }

    /**
     * customers_json action
     *
     * @author diennvt
     * @return string json format
     */
    public function stylists_by_date() {
        $str = '{"stylists":[]}';
        echo $str;
        exit;
    }

    /**
     * Get item's info action
     *
     * @author khoatx
     * @return string json format
     */
    public function getiteminfo() {
        include ('Ajax/getiteminfo.php');
    }

    /**
     * Handles user interaction of view cancel Ajax.
     *
     * @return void
     */
    public function cancel() {
    	include ('Ajax/cancel.php');
    }

    /**
     * Handles user interaction of view cancel Ajax.
     *
     * @return void
     */
    public function autocancel() {
        include ('Ajax/autocancel.php');
    }


    public function seatbookingdialog()
    {
        include ('Ajax/seatbookingdialog.php');
    }
    /**
     * Handles add a nail for an order Ajax.
     * @author khoatx
     * @return void
     */
    public function addnailfororder() {
        include ('Ajax/addnailfororder.php');
    }
    /**
     * Handles check and update nail for order
     * @author khoatx
     * @return void
     */
    public function orderupdatenail() {
        include ('Ajax/orderupdatenail.php');
    }

    /**
     * Handles check service device
     * @author diennvt
     * @return void
     */
    public function checkservicedevice() {
    	include ('Ajax/checkservicedevice.php');
    }
    
    /**
     * Handles disable chart seat
     * @author diennvt
     * @return void
     */
    public function disablechartseat() {
    	include ('Ajax/disablechartseat.php');
    }
    
    /**
     * Handles enable chart seat
     * @author diennvt
     * @return void
     */
    public function enablechartseat() {
    	include ('Ajax/enablechartseat.php');
    }
    
    /**
     * Get list order nail
     * @author Tuancd
     * @return void
     */
    public function ordernails() {
        include ('Ajax/ordernails.php');
    }

    /**
     * Get list order nail
     * @author Tuancd
     * @return void
     */
    public function orderitems() {
        include ('Ajax/orderitems.php');
    }

    /**
     * Approve beauty
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function approvebeauty()
    {
        include('Ajax/approvebeauty.php');
    }

    /**
     * Approve user bought item complete
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function completed()
    {
        include('Ajax/completed.php');
    }

    /**
     * Action for holiday
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function holiday()
    {
        include('Ajax/holiday.php');
    }

    /**
     * Approve user bought item complete
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function cancelboughtitem()
    {
        include('Ajax/cancelboughtitem.php');
    }
}
