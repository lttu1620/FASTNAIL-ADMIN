<?php

$modelName = $this->Userpointlog->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('User Point Logs');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'        => 'user_customid',
        'label'     => __('User ID'),
        'empty'     => '',
    ))
    ->addElement(array(
        'id'        => 'username',
        'label'     => __('User name'),
        'empty'     => '',
    ))
    ->addElement(array(
        'id'        => 'email',
        'label'     => __('Email'),
        'empty'     => '',
    ))
    ->addElement(array(
        'id'        => 'type',
        'label'     => __('Type'),
        'options'   => Configure::read('Config.UserPointLogType'),
        'empty'     => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'        => 'date_from',
        'type'      => 'text',
        'calendar'  => true,
        'label'     => __('Date from'),
    ))
    ->addElement(array(
        'id'        => 'date_to',
        'type'      => 'text',
        'calendar'  => true,
        'label'     => __('Date to'),
    ))
    ->addElement(array(
        'id'        => 'limit',
        'label'     => __('Limit'),
        'options'   => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'      => 'submit',
        'value'     => __('Search'),
        'class'     => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array(
        'page'    => 1,
        'limit'   => Configure::read('Config.pageSize'))
);
if(!empty($param['user_customid'])){
    $param['user_id'] = $param['user_customid'];
    unset($param['user_customid']);
}
$result = Api::call(Configure::read('API.url_userpointlogs_list'), $param, false, array('total' => 0, 'data' => array()));
$total = $result['total'];
$data = $result['data'];
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'id',
        'title' => __('ID'),
        'width' => '90'
    ))
    ->addColumn(array(
        'id'    => 'user_id',
        'title' => __('UserId'),
        'empty' => '',
        'width' => 150,
    ))
    ->addColumn(array(
        'id'    => 'name',
        'title' => __('User name'),
        'empty' => '',
        'width' => 250,
    ))
    ->addColumn(array(
        'id'    => 'type',
        'title' => __('Title'),
        'empty' => '',
        'rules' => array(
            '1' => Configure::read('Config.UserPointLogType')[1],
            '2' => Configure::read('Config.UserPointLogType')[2],    
            '3' => Configure::read('Config.UserPointLogType')[3],    
            '4' => Configure::read('Config.UserPointLogType')[4],    
            '5' => Configure::read('Config.UserPointLogType')[5],    
            '7' => Configure::read('Config.UserPointLogType')[7],
            '8' => Configure::read('Config.UserPointLogType')[8],
            '99' => Configure::read('Config.UserPointLogType')[99],    
        ),
        'width' => 250,
    ))
    ->addColumn(array(
        'id'     => 'got_point',
        'title'  => __('Got point'),
        'empty'  => '-',
        'width' => 100,
    ))
    ->addColumn(array(
        'id'     => 'used_point',
        'title'  => __('Used point'),
        'empty'  => '-',
        'width' => 100,
    ))
    ->addColumn(array(
        'id'     => 'created',
        'title'  => __('Created'),
        'type'   => 'date',
        'width' => 200,
    ))
    ->addColumn(array(
        'id'     => 'expire_date',
        'title'  => __('Expired date'),
        'type'   => 'date',
        'width' => 200,
    ))
    ->setDataset($data);
