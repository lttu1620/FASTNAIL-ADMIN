<?php

$modelName = $this->Pointitem->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Item point List');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get') 
    ->addElement(array(
        'id'    => 'id',     
        'label' => __('ID')
    )) 
    ->addElement(array(
        'id'    => 'name',
        'type'  => 'text',
        'label' => __('Name')
    ))   
    ->addElement(array(
        'id'       => 'start_date',
        'type'     => 'text',
        'calendar' => true,
        'label'    => __('Start date'),
    ))
    ->addElement(array(
        'id'       => 'end_date',
        'type'     => 'text',
        'calendar' => true,
        'label'    => __('End date'),
    ))
    ->addElement(array(
        'id'      => 'disable',
        'label'   => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty'   => Configure::read('Config.StrAll')
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'name-asc'     => __('Name Asc'),
            'name-desc'    => __('Name Desc'),
            'created-asc'  => __('Created Asc'),
            'created-desc' => __('Created Desc')
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'      => 'limit',
        'label'   => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_point_items_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

// create data table
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'item',
        'name'  => 'items[]',
        'type'  => 'checkbox',
        'value' => '{id}',
        'width' => 20
    ))
    ->addColumn(array(
        'id'    => 'id',
        'type'  => 'link',
        'title' => __('ID'),
        'href'  => '/' . $this->controller . '/update/{id}',
        'width' => 50
    ))
    ->addColumn(array(
        'id'    => 'image_url',
        'type'  => 'image',
        'title' => __('Image'),
        'src'   => '{image_url}',
        'width' => '60'
    ))   
    ->addColumn(array(
        'id'    => 'name',        
        'title' => __('Name'),       
    ))
    ->addColumn(array(
        'id'    => 'point',       
        'type'    => 'number',       
        'title' => __('Point'), 
        'width' => 100,
    ))
    ->addColumn(array(
        'id'    => 'limit_quantity',       
        'title' => __('Limit quantity'),       
        'empty' => '',     
        'width' => 100,
    ))
    ->addColumn(array(
        'id'    => 'start_date',
        'title' => __('Start date'),
        'type'  => 'date',
        'empty' => '',
        'width' => 140
    ))
    ->addColumn(array(
        'id'    => 'end_date',
        'title' => __('End date'),
        'type'  => 'date',
        'empty' => '',
        'width' => 140
    ))
    ->addColumn(array(
        'id'    => 'created',
        'title' => __('Created'),
        'type'  => 'date',
        'width' => 140
    ))
    ->addColumn(array(
        'id'     => 'disable',
        'type'   => 'checkbox',
        'title'  => __('Status'),
        'toggle' => true,
        'rules'  => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty'  => 0,
        'width'  => 90,
    ))
    ->setDataset($data)
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'id'    => 'btn-disable',
        'type'  => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'id'    => 'btn-enable',
        'type'  => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
