<?php

$modelName = $this->Pointitem->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Item Point');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_point_items_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Item unavailable", __METHOD__, $param);
        throw new NotFoundException("Item Point unavailable", __METHOD__, $param);
    }
    $pageTitle = __('Edit Item Point');
}
$this->setPageTitle($pageTitle);
$listItem = $this->Common->arrayKeyValue(MasterData::items_all(), 'id', 'name');
$listService = $this->Common->arrayKeyValue(MasterData::services_all(), 'id', 'name');

// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle
        ));
// create Update form 
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'description',
            'label' => __('Description'),
            'type' => 'text',
            'rows' => 6,
            'required' => true
        ))
        ->addElement(array(
                'id'         => 'image_url',
                'type'       => 'file',
                'image'      => true,
                'label'      => __('Image url'),
                'allowEmpty' => true,
                'class'      => 'upload',
                'crop'       => array(
                    //'field' => 'image_url',
                ),
            )
        )
        ->addElement(array(
            'id' => 'point',
            'label' => __('Point'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'limit_quantity',
            'label' => __('Limit quantity'),           
        ))
        ->addElement(array(
            'id' => 'start_date',            
            'calendar' => true,
            'label' => __('Start date'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'end_date',           
            'calendar' => true,
            'label' => __('End date'),
            'required' => true
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    $error = array();
    if ($model->validateInsertUpdate($data)) {
        $id = Api::call(Configure::read('API.url_point_items_add_update'), $data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not update item point", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
    } else {
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}