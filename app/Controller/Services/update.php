<?php

$modelName = $this->Service->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add service');
if (!empty($id)) {
    $pageTitle = __('Edit service');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_services_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/services',
            'name' => __('Service list'),
        ))
        ->add(array(
            'name' => $pageTitle,
        ));
// create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('ID'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'time',
            'label' => __('Time'),
            'options' => $this->Common->getServiceTime(),
            'empty' => Configure::read('Config.StrChooseOne'),            
        ))
        ->addElement(array(
            'id' => 'device_type',
            'label' => __('Device type'),
            'options' => Configure::read('Config.deviceType'),
            'empty' => Configure::read('Config.StrChooseOne'),           
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

// process when submit form
if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertOrUpdate($data)) { 
        $id = Api::call(Configure::read('API.url_services_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            AppCache::delete(Configure::read('services_all')->key);
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}