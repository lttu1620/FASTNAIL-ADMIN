<?php

$modelName = $this->Item->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Item');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_items_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Item unavailable", __METHOD__, $param);
        throw new NotFoundException("Item unavailable", __METHOD__, $param);
    }
    $pageTitle = __('Edit Item');
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));
// create Update form 
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id'    => 'id',
        'type'  => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id'    => 'item_cd',
        'label' => __('Item Cd'),
        'required' => true
    ))
    ->addElement(array(
        'id' => 'item_section',
        'label' => __('Item Section'),
        'options' => Configure::read('Config.ItemSection'),
        'required' => true
    ))
    ->addElement(array(
        'id'    => 'name',
        'label' => __('Name'),
        'required' => true
    ))
    ->addElement(array(
        'id'    => 'price',
        'label' => __('Price'),
        'required' => true
    ))
    ->addElement(array(
        'id' => 'tax_price',
        'label' => __('Tax price'),        
        'required' => true
    ))
    ->addElement(array(
        'id'    => 'is_sale',
        'label' => __('Sale'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'    => 'is_discount',
        'label' => __('Discount'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'    => 'is_tax',
        'label' => __('Tax'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'    => 'priority',
        'label' => __('Priority'),
        'required' => true
    ))
    ->addElement(array(
        'id'    => 'is_discount_combi',
        'label' => __('Discount Combine'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'    => 'receipt_coupon',
        'label' => __('Receipt Coupon'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'    => 'discount_price',
        'label' => __('Discount Price')
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type'    => 'submit',
        'value'   => __('Cancel'),
        'class'   => 'btn pull-left',
        'onclick' => 'return back();'
    ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        $id = Api::call(Configure::read('API.url_items_addupdate'), $data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not update item", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            AppCache::delete(Configure::read('items_all')->key);
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
    } else {
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}