<?php

$modelName = $this->Item->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Item List');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'item_section',
        'label' => __('Item Section'),
        'options' => Configure::read('Config.ItemSection'),      
        'empty'   => Configure::read('Config.StrAll')
    ))
    ->addElement(array(
        'id'    => 'item_cd',
        'type'  => 'text',
        'label' => __('Item Cd'),
    ))
    ->addElement(array(
        'id'    => 'name',
        'type'  => 'text',
        'label' => __('Name')
    ))    
    ->addElement(array(
        'id'      => 'disable',
        'label'   => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty'   => Configure::read('Config.StrAll')
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'name-asc'     => __('Name Asc'),
            'name-desc'    => __('Name Desc'),
            'created-asc'  => __('Created Asc'),
            'created-desc' => __('Created Desc')
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'      => 'limit',
        'label'   => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_items_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

// create data table
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'item',
        'name'  => 'items[]',
        'type'  => 'checkbox',
        'value' => '{id}',
        'width' => 20
    ))
    ->addColumn(array(
        'id'    => 'id',
        'type'  => 'link',
        'title' => __('ID'),
        'href'  => '/' . $this->controller . '/update/{id}',
        'width' => 50
    ))
    ->addColumn(array(
        'id'    => 'item_section',
        'title' => __('Item section'),
        'rules' => Configure::read('Config.ItemSection'),
        'width' => '100',
    ))
    ->addColumn(array(
        'id'    => 'item_cd',
        'title' => __('Code'),
        'width' => '60',
    ))    
    ->addColumn(array(
        'id'    => 'name',
        'type'  => 'link',
        'title' => __('Name'),
        'href'  => '/' . $this->controller . '/update/{id}',        
    ))
    ->addColumn(array(
        'id'    => 'price',
        'title' => __('Price'),
        'type'  => 'number',
        'width' => 120,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'tax_price',
        'title' => __('Tax price'),
        'type'  => 'number',
        'width' => 120,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'sell_price',
        'title' => __('Sell price'),
        'type'  => 'number',
        'width' => 120,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'is_sale',
        'title' => __('Sale'),
        'width' => 80,
        'rules' => array(
            1 => __('Yes'),
            0 => __('No'),
        ),
        'empty' => '0'
    ))
    ->addColumn(array(
        'id'    => 'is_discount',
        'title' => __('Discount'),
        'width' => 80,
        'rules' => array(
            1 => __('Yes'),
            0 => __('No'),
        ),
        'empty' => '0'
    ))
    ->addColumn(array(
        'id'    => 'is_tax',
        'title' => __('Tax'),
        'rules' => array(
            1 => __('Yes'),
            0 => __('No'),
        ),
        'width' => 80,
        'empty' => '0'
    ))
    ->addColumn(array(
        'id'    => 'priority',
        'title' => __('Priority'),
        'width' => 80,
        'empty' => '',
    ))
    ->addColumn(array(
        'id'    => 'created',
        'title' => __('Created'),
        'type'  => 'date',
        'width' => 120
    ))
    ->addColumn(array(
        'id'     => 'disable',
        'type'   => 'checkbox',
        'title'  => __('Status'),
        'toggle' => true,
        'rules'  => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty'  => 0,
        'width'  => 90,
    ))
    ->setDataset($data)
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'id'    => 'btn-disable',
        'type'  => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'id'    => 'btn-enable',
        'type'  => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
