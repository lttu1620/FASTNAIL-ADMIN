<?php

$modelName = $this->Surveylog->name;

//Process disable/enable
$this->doGeneralAction($modelName);
$listUsers = MasterData::users_all();
$listUsers = $this->Common->arrayKeyValue($listUsers, 'id', 'first_name');
$listSurveys = MasterData::surveys_all();
$listSurveys = $this->Common->arrayKeyValue($listSurveys, 'id', 'title');
//var_dump($listAreas);exit;
// create breadcrumb
$pageTitle = __('Survey log list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
                'id' => 'user_id',
                'label' => __('User'),
                'options' => $listUsers,
                'empty' => Configure::read('Config.StrChooseOne'),
               // 'autocomplete_combobox' => true,
    ))
    ->addElement(array(
                'id' => 'survey_id',
                'label' => __('Survey'),
                'options' => $listSurveys,
                'empty' => Configure::read('Config.StrChooseOne'),
               // 'autocomplete_combobox' => true,
    ))
    ->addElement(array(
        'id'       => 'date_from',
        'label'    => __('From date'),
        'type'     => 'text',
        'calendar' => true
    ))
    ->addElement(array(
        'id'       => 'date_to',
        'label'    => __('To date'),
        'type'     => 'text',
        'calendar' => true
    ))
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty' => __('All')
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'id-asc' => __('ID Asc'),
            'id-desc' => __('ID Desc'),
            'name-asc' => __(' name Asc'),
            'name-desc' => __(' name Desc')
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_surveylogs_list'), $param, false, array(0, array()));

$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id' => 'id',
        'title' => __('ID'),
        'width' => '50'
    ))
    ->addColumn(array(
        'id' => 'title',
        'title' => __('Title'),
        'width' => 250,
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'created',
        'type' => 'date',
        'title' => __('Created'),
        'width' => 250
    ))
    ->setDataset($data);
