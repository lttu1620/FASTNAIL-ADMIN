<?php

$modelName = $this->Tag->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Tag');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_tags_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Tag unavailable", __METHOD__, $param);
        throw new NotFoundException("Tag unavailable", __METHOD__, $param);
    }
    // set tile update 
    $pageTitle = __('Edit Tag');
}
$this->setPageTitle($pageTitle);
// Create Breadcrumb
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/tags',
            'name' => __('Tag list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// Create Update form 
$this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('name'),
            'autocomplete' => 'off',
            'required' => true
        ))
        ->addElement(array(
            'id' => 'color',
            'label' => __('Color'),
            // 'options' => Configure::read('Config.searchColor'),
            // 'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

if ($this->request->is('post')) {
    // if case add new tag
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_tags_addupdate'), $model->data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not add new", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
        }
        $this->redirect($this->request->here(false));
    } else {
        AppLog::info("Can not add new", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}