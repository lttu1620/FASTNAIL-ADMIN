<?php

$modelName = $this->Attribute->name;

//get real controller
$controllerName = explode("/", $this->request->url)[0];
$this->request->params['controller'] = $controllerName;
$this->set('controller', $controllerName);

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle =   __(ucfirst($controllerName).' list');
$this->setPageTitle($pageTitle);
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name')
    ))
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty' => __('All')
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'id-asc' => __('ID Asc'),
            'id-desc' => __('ID Desc'),
            'name-asc' => __('Name Asc'),
            'name-desc' => __('Name Desc'),
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call("{$controllerName}/list", $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id' => 'item',
        'name' => 'items[]',
        'type' => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id' => 'id',
        'type' => 'link',
        'title' => __('ID'),
        'href' => '/' . $controllerName . '/update/{id}',
        'width' => '30'
    ))
    ->addColumn(array(
        'id' => 'name',
        'type' => 'link',
        'title' => __('Name'),
        'href' => '/' . $controllerName . '/update/{id}',       
        'empty' => ''
    ));
if (in_array($controllerName, array('genres', 'designs', 'scenes', 'colors'))) {
    $this->SimpleTable
        ->addColumn(array(
            'id'    => 'icon',
            'type'  => 'image',
            'title' => __('PC Icon'),
            'src'   => '{icon}',
            'width' => '120',
        ))
        ->addColumn(array(
            'id'    => 'mobile_icon',
            'type'  => 'image',
            'title' => __('Mobile icon'),
            'src'   => '{mobile_icon}',
            'width' => '120'
        ))
        ->addColumn(array(
            'id'    => 'active_icon',
            'type'  => 'image',
            'title' => __('PC Active icon'),
            'src'   => '{active_icon}',
            'width' => '120'
        ))
        ->addColumn(array(
            'id'    => 'mobile_active_icon',
            'type'  => 'image',
            'title' => __('Mobile active icon'),
            'src'   => '{mobile_active_icon}',
            'width' => '120'
        ))
        ->addColumn(array(
            'id'    => 'header_icon',
            'type'  => 'image',
            'title' => __('PC Header icon'),
            'src'   => '{header_icon}',
            'width' => '120'
        ))
        ->addColumn(array(
            'id'    => 'mobile_header_icon',
            'type'  => 'image',
            'title' => __('Mobile header icon'),
            'src'   => '{mobile_header_icon}',
            'width' => '120'
        ));
}
/*
if (in_array($controllerName, array('colors'))) {
    $this->SimpleTable
        ->addColumn(array(
            'id' => 'css_class',
            'title' => __('Css class'),
            'width' => 200,
            'empty' => ''
        ));
}
* 
*/
if (in_array($controllerName, array('colorjells'))) {
    $this->SimpleTable->addColumn(array(
        'id'    => 'icon',
        'type'  => 'image',
        'title' => __('Icon'),
        'src'   => '{icon}',
        'width' => '64'
    ));
}
$this->SimpleTable    
    ->addColumn(array(
        'id' => 'created',
        'type' => 'date',
        'title' => __('Created'),
        'width' => 120,
    ))
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'checkbox',
        'title' => __('Status'),
        'toggle' => true,
        'rules' => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty' => 0,
        'width' => 90,
    ))
    ->setDataset($data)
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
