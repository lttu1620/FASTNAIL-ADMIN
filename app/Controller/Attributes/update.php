<?php

$modelName = $this->Attribute->name;
$model = $this->{$modelName};

$controllerName = explode("/", $this->request->url)[0];
$this->request->params['controller'] = $controllerName;
$this->set('controller', $controllerName);

// Create breadcrumb 
$data = array();
if (isset($id) && $id != 0) {
    $pageTitle = __('Update ' . $controllerName);    
    $param['id'] = $id;
    $data[$modelName] = Api::Call("{$controllerName}/detail", $param);
    $this->Common->handleException(Api::getError());
} else {
    $pageTitle = __('Register ' . $controllerName);
}
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/' . $controllerName,
            'name' => __(ucfirst($controllerName) . ' list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));
$this->setPageTitle($pageTitle);
$form = $this->UpdateForm->reset()
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

if (in_array($controllerName, array('genres', 'designs', 'scenes', 'colors'))) {
    $form
        ->addElement(array(
            'id'    => 'icon',
            'type'  => 'file',
            'image' => true,
            'label' => __('Icon'),
        ))
        ->addElement(array(
            'id'    => 'active_icon',
            'type'  => 'file',
            'image' => true,
            'label' => __('Active icon'),
        ))
        ->addElement(array(
            'id'    => 'header_icon',
            'type'  => 'file',
            'image' => true,
            'label' => __('Header icon'),
        ))
        ->addElement(array(
            'id'    => 'mobile_icon',
            'type'  => 'file',
            'image' => true,
            'label' => __('Mobile icon'),
        ))
        ->addElement(array(
            'id'    => 'mobile_active_icon',
            'type'  => 'file',
            'image' => true,
            'label' => __('Mobile active icon'),
        ))
        ->addElement(array(
            'id'    => 'mobile_header_icon',
            'type'  => 'file',
            'image' => true,
            'label' => __('Mobile header icon'),
        ));
}
if (in_array($controllerName, array('colorjells'))) {
    $form->addElement(array(
        'id' => 'icon',
        'type' => 'file',
        'image' => true,
        'label' => __('Icon'),        
    ));    
}
/*
if (in_array($controllerName, array('colors'))) {
    $form->addElement(array(
        'id' => 'css_class',
        'label' => __('Css class'),        
    ));
}
 * 
 */
// Process when submit form
if ($this->request->is('post')) {
    $error = array();
    $data = $this->getData($modelName);
    if ($model->validateAttributeInsertUpdate($data)) {
        if (!empty($_FILES['data']['name'][$modelName]['icon'])) {
            $icon = $this->Image->uploadImage("{$modelName}.icon");
            $data[$modelName]['icon'] = $icon;
        } elseif (isset($data[$modelName]['icon'])) {
            unset($data[$modelName]['icon']);
        }
        if (!empty($_FILES['data']['name'][$modelName]['active_icon'])) {
            $activeIcon = $this->Image->uploadImage("{$modelName}.active_icon");
            $data[$modelName]['active_icon'] = $activeIcon;
        } elseif (isset($data[$modelName]['active_icon'])) {
            unset($data[$modelName]['active_icon']);
        }
        if (!empty($_FILES['data']['name'][$modelName]['header_icon'])) {
            $headerIcon = $this->Image->uploadImage("{$modelName}.header_icon");
            $data[$modelName]['header_icon'] = $headerIcon;
        } elseif (isset($data[$modelName]['header_icon'])) {
            unset($data[$modelName]['header_icon']);
        }
        if (!empty($_FILES['data']['name'][$modelName]['mobile_icon'])) {
            $mobileIcon = $this->Image->uploadImage("{$modelName}.mobile_icon");
            $data[$modelName]['mobile_icon'] = $mobileIcon;
        } elseif (isset($data[$modelName]['mobile_icon'])) {
            unset($data[$modelName]['mobile_icon']);
        }
        if (!empty($_FILES['data']['name'][$modelName]['mobile_active_icon'])) {
            $mobileActiveIcon = $this->Image->uploadImage("{$modelName}.mobile_active_icon");
            $data[$modelName]['mobile_active_icon'] = $mobileActiveIcon;
        } elseif (isset($data[$modelName]['mobile_active_icon'])) {
            unset($data[$modelName]['mobile_active_icon']);
        }
        if (!empty($_FILES['data']['name'][$modelName]['mobile_header_icon'])) {
            $mobileHeaderIcon = $this->Image->uploadImage("{$modelName}.mobile_header_icon");
            $data[$modelName]['mobile_header_icon'] = $mobileHeaderIcon;
        } elseif (isset($data[$modelName]['mobile_header_icon'])) {
            unset($data[$modelName]['mobile_header_icon']);
        }
        $id = Api::call("{$controllerName}/addupdate", $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$controllerName}/update/{$id}");
        }
        return $this->Common->setFlashErrorMessage(Api::getError(), $error);
    }
    // if validation error from api, write log and set validation error
    AppLog::info("Can not update", __METHOD__, $this->data);
    // show validation error    
    $this->Common->setFlashErrorMessage($model->validationErrors);
}