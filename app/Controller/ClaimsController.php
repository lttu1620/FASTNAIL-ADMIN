<?php

App::uses('AppController', 'Controller');

/**
 * ClaimsController class of Claims Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ClaimsController extends AppController
{
    /**
     * Initializes components for ClaimsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Claims.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Claims/index.php');
    }

    /**
     * Handles user interaction of view update Claims.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Claims. Default value is 0.
     * @return void
     */
    public function update($id = 0)
    {
        include('Claims/update.php');
    }
}
