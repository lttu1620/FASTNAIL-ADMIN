<?php

App::uses('AppController', 'Controller');

/**
 * Shops Controller
 *
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class ShopsController extends AppController {

    /**
     * Construct
     *
     * @author VuLTH
     * @return void
     */
    public function __construct($request = null, $response = null) {
        //echo 'vu';
        parent::__construct($request, $response);
    }

    /**
     * index action
     *
     * @author VuLTH
     * @return void
     */
    public function index() {

        include ('Shops/index.php');
    }

    /**
     * update action
     *
     * @author VuLTH
     */
    public function update($shop_id = 0) {
        include ('Shops/update.php');
    }

    /**
     * update action
     *
     * @author KhoaTX
     */
    public function customer() {
        include ('Shops/customer.php');
    }
}
