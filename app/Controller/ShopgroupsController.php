<?php

App::uses('AppController', 'Controller');

/**
 * ShopGroups Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class ShopgroupsController extends AppController {

    /**
     * Construct
     * 
     * @author VuLTH 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        //echo 'vu';
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author VuLTH 
     * @return void
     */
    public function index() {
        
        include ('Shopgroups/index.php');
    }
    
    /**
     * update action
     * 
     * @author VuLTH 
     */
    public function update($id = 0) {
        include ('Shopgroups/update.php');
    }
}
