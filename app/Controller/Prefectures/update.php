<?php

$modelName = $this->Prefecture->name;
$model = $this->{$modelName};
// get list area
$listAreas = MasterData::areas_all();
$listAreas = $this->Common->arrayKeyValue($listAreas, 'id', 'name');

$data = array();
$pageTitle = __('Add Prefecture');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_prefectures_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Item unavailable", __METHOD__, $param);
        throw new NotFoundException("Item unavailable", __METHOD__, $param);
    }
    $pageTitle = __('Edit Prefecture');
}
$this->setPageTitle($pageTitle);

$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));

$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id'    => 'id',
        'type'  => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id'       => 'area_id',
        'label'    => __('Area'),
        'options'  => $listAreas,
        'empty'    => Configure::read('Config.StrChooseOne'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'name',
        'label'    => __('Name'),
        'required' => true
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type'    => 'submit',
        'value'   => __('Cancel'),
        'class'   => 'btn pull-left',
        'onclick' => 'return back();'
    ));

// Process when submit form
if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        //$data = $this->getData($modelName);
        $id = Api::call(Configure::read('API.url_prefectures_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error    
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
