<?php

$modelName = $this->Prefecture->name;

//Process disable/enable
$this->doGeneralAction($modelName);
$listAreas = MasterData::areas_all();
$listAreas = $this->Common->arrayKeyValue($listAreas, 'id', 'name');
//var_dump($listAreas);exit;
// create breadcrumb
$pageTitle = __('Prefecture list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name')
    ))
    ->addElement(array(
        'id' => 'area_id',
        'label' => __('Area'),
        'options' => $listAreas,
        'empty' => __('All')       
    ))
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty' => __('All')
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'id-asc' => __('ID Asc'),
            'id-desc' => __('ID Desc'),
            'name-asc' => __('Name Asc'),
            'name-desc' => __('Name Desc')
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_prefectures_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id' => 'item',
        'name' => 'items[]',
        'type' => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id' => 'id',
        'type' => 'link',
        'title' => __('ID'),
        'href' => '/' . $this->controller . '/update/{id}',
        'width' => '30'
    ))
    ->addColumn(array(
        'id' => 'name',
        'type' => 'link',
        'href' => '/' . $this->controller . '/update/{id}',
        'title' => __('Name'),       
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'area_name',
        'type' => 'link',
        'href' => '/areas/update/{area_id}',
        'title' => __('Area'),
        'width' => 300,
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'created',
        'type' => 'date',
        'title' => __('Created'),
        'width' => 150
    ))
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'checkbox',
        'title' => __('Status'),
        'toggle' => true,
        'rules' => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty' => 0,
        'width' => 90,
    ))
    ->setDataset($data)
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
