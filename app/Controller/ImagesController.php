<?php

App::uses('AppController', 'Controller');

/**
 * ImagesController class of Images Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class ImagesController extends AppController {

   /** @var string $layout layout Name */
   public $layout = "other";

    /**
     * Initializes components for HelpcontentsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    public function upload() {
        $param['id']    = $this->data['id'];
        $objectInfo     = $this->data['objectInfo'];
        if(!empty($objectInfo)){
            // upload image base64
            $url = $this->Image->uploadImageBase64($this->data['imageData']);
            switch ($objectInfo) {
                case 'nail_id':
                $param['photo_url'] = $url;
                    // update image for nail
                $id = Api::call(Configure::read('API.url_nails_addupdate'), $param);
                if (empty($id) || Api::getError()) {
                   echo json_encode(false);
                   exit;
               }
               break;

               case 'nailist_id':
                    // update image for naillist            
               $param['image_url'] = $url;
               $id = Api::call(Configure::read('API.url_nailists_addupdate'), $param);
               if (empty($id) || Api::getError()) {
                   echo json_encode(false);
                   exit;
               }
               break;
               
               case 'beauty_id':
               	// update image for beauty
               	$param['image_url'] = $url;
               	$id = Api::call(Configure::read('API.url_beauties_addupdate'), $param);
               	if (empty($id) || Api::getError()) {
               		echo json_encode(false);
               		exit;
               	}
               	break;

                case 'bannerrecommend_id':
                // update image for beauty
                $param['image_url'] = $url;
                $id = Api::call(Configure::read('API.url_bannerrecommends_addupdate'), $param);
                if (empty($id) || Api::getError()) {
                  echo json_encode(false);
                  exit;
                }
                break;
                
                case 'campaign_id':
                // update image for beauty
                $param['image_url'] = $url;
                $id = Api::call(Configure::read('API.url_campaigns_addupdate'), $param);
                if (empty($id) || Api::getError()) {
                  echo json_encode(false);
                  exit;
                }
                break;
           }
            // Return new image url 
           echo json_encode($url);
       }else{
            echo json_encode(false);
        }      
    }

    public function get_image() {
        $getInfo = getimagesize($this->request->query['img']);
        header('Content-type: ' . $getInfo['mime']);
        readfile($this->request->query['img']);
    }
}
