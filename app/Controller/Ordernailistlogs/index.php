<?php

$modelName = $this->Ordernailistlog->name;

// Process disable/enable
$this->doGeneralAction($modelName);
// create breadcrumb
$pageTitle = __('Order nailist logs list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));
$this->setPageTitle($pageTitle);
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$data = Api::call(Configure::read('API.url_orderlogs_nailist_logs_all'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->SimpleTable->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'nailist_image_url',
            'type' => 'image',
            'title' => __('Image'),
            'src' => '{nailist_image_url}',
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'nailist_name',
            'title' => __('Nailist name'),
        ))
        ->addColumn(array(
            'id' => 'type',
            'title' => __('Type name'),
            'rules' =>Configure::read('Config.OrderNailistLogType'),
            'empty' => '',
            'width' => '250'
        ))
        ->addColumn(array(
            'id' => 'admin_name',
            'title' => __('Admin name'),
            'width' => '250'
        ))
        ->addColumn(array(
            'id' => 'created',
            'title' => __('Created'),
            'type' => 'date',
            'width' => 120
        ))
        ->setDataset($data);
