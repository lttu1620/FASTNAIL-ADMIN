<?php

$modelName = $this->Nail->name;
$model = $this->{$modelName};

// Create breadcrumb 
$is_update = false;
if (isset($nail_id) && $nail_id != 0) {
    $pageTitle = __('Nail detail')."&nbsp;ID:".h($nail_id);
    $is_update = true;
    $param['id'] = $nail_id;
    $param['nail_id'] = $nail_id;
    $data[$modelName] = Api::Call(Configure::read('API.url_nails_detail'), $param);
    $this->Common->handleException(Api::getError());
} else {
    $pageTitle = __('Add nail');
    $param['nail_id'] = -1;
}

// get list attributes
$data['listAttributes'] = Api::Call(Configure::read('API.url_nails_attribute'), $param);
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/nails',
        'name' => __('Nail list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));
$this->setPageTitle($pageTitle);
$this->set($data) ;

