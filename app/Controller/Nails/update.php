<?php

$modelName = $this->Nail->name;
$model = $this->{$modelName};

$data[$modelName] = array();
// Create breadcrumb 
if (isset($nail_id) && $nail_id != 0) {
    $pageTitle = __('Update nail');
    $param['id'] = $nail_id;
    $param['nail_id'] = $nail_id;
    $data[$modelName] = Api::Call(Configure::read('API.url_nails_detail'), $param);
    $this->Common->handleException(Api::getError());
} else {
    $pageTitle = __('Add nail');
    $param['nail_id'] = -1;
}

// get list attributes
$listAttributes = Api::Call(Configure::read('API.url_nails_attribute'), $param);
$attributes = array(
    'bullions',
    'colors',
    'color_jells',
    'designs',
    'flowers',
    'genres',
    'holograms',
    'keywords',
    'paints',
    'powders',
    'rame_jells',
    'scenes',
    'shells',
    'stones',
    'tags'
);

//Keep attribute data when missing required fields
if ($this->request->is('post')) {
    $dt = $this->getData($modelName);
    foreach ($attributes as $attr) {
        if(!empty($dt[$modelName][$attr])) {
            foreach($dt[$modelName][$attr] as $attr_id) {
                foreach($listAttributes[$attr] as $k => $attr_item) {
                    if($attr_item['id'] == $attr_id) {
                        $listAttributes[$attr][$k]['checked'] = '1';
                        break;
                    }
                }
            }
        }
    }
}

// get list price
//$listPrice = MasterData::prices_all();

$this->Breadcrumb->SetTitle($pageTitle)
    ->add(
        array(
            'link' => $this->request->base.'/nails',
            'name' => __('Nail list'),
        )
    )
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$this->setPageTitle($pageTitle);
// Create Update form nail
$this->UpdateForm->reset()
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(
        array(
            'id'    => 'id',
            'type'  => 'hidden',
            'label' => __('id'),
        )
    )
    ->addElement(
        array(
            'id'       => 'photo_cd',
            'label'    => __('Photo code'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'show_section',
            'label'    => __('Show section'),
            'options'  => Configure::read('Config.showSection'),
            'empty'    => __('-- Select one --'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'hf_section',
            'label'    => __('Hf section'),
            'options'  => Configure::read('Config.hfSection'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'menu_section',
            'label'    => __('Menu section'),
            'options'  => Configure::read('Config.BooleanValue'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'price',
            'label'    => __('Price'),
            'type'  => 'text',
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'    => 'tax_price',
            'label' => __('Tax price'),
            'type'  => 'text',
        )
    )
    ->addElement(
        array(
            'id'       => 'time',
            'label'    => __('Nail time'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'print',
            'label'    => __('Print'),
            'options'  => Configure::read('Config.BooleanValue'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'limited',
            'label'    => __('Limited'),
            'options'  => Configure::read('Config.BooleanValue'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'rank',
            'label'    => __('Rank'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'      => 'hp_coupon',
            'label'   => __('Hp coupon'),
            'options' => Configure::read('Config.BooleanValue'),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'    => 'coupon_memo',
            'label' => __('Coupon memo'),
            'type'  => 'textarea',
            'rows'  => '10',
        )
    )
    ->addElement(
        array(
            'id'       => 'coupon_type',
            'label'    => __('Coupon Type'),
            'options'  => Configure::read('Config.CouponType'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'    => 'attention',
            'label' => __('Attention'),
            'type'  => 'textarea',
            'rows'  => '10',
        )
    )
    ->addElement(
        array(
            'id'         => 'photo_url',
            'type'       => 'file',
            'image'      => true,
            'label'      => __('Photo url'),
            'allowEmpty' => true,
            'class'      => 'upload',
            'crop'       => array(
                'nail_id' => $nail_id,
                'field'   => 'photo_url',
            ),
        )
    );
if (!empty($listAttributes)) {
    foreach ($listAttributes as $name => $dt) {
        $checkArr = $this->Common->arrayFilter($dt, 'checked', '1');
        $selectedArr = $this->Common->arrayValues($checkArr, 'id');
        $options = $this->Common->arrayKeyValue($dt, 'id', 'name');
        $this->UpdateForm
            ->addElement(
                array(
                    'id'       => $name,
                    'type'     => 'select',
                    'multiple' => 'checkbox',
                    'label'    => __(ucfirst($name)),
                    'options'  => $options,
                    'selected' => $selectedArr,
                    'between'  => "<div class='group-checkbox'>",
                    'after'    => "</div><div class=\"cls\"></div>",
                )
            );
    }
}
$this->UpdateForm
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        )
    )
    ->addElement(
        array(
            'type'    => 'submit',
            'value'   => __('Cancel'),
            'class'   => 'btn pull-left',
            'onclick' => 'return back();',
        )
    );

// Process when submit form
if ($this->request->is('post')) {
    $error = array();
    $data = $this->getData($modelName);
    if ($model->validateNailInsertUpdate($data)) {
        foreach ($attributes as $attr_name) {
            $dt = isset($data[$modelName][$attr_name]) ? $data[$modelName][$attr_name] : array();
            $attribute_name_id = substr($attr_name, 0, -1).'_id';
            $data[$modelName][$attribute_name_id] = !empty($dt) ? implode(',', $dt) : '';
            unset($data[$modelName][$attr_name]);
        }
        // Processing upload Image
        if (!empty($_FILES['data']['name'][$modelName]['photo_url'])) {
            $image_url = $this->Image->uploadImage("{$modelName}.photo_url");
            $data[$modelName]['photo_url'] = $image_url;
        } elseif (!empty($model->data[$modelName]['photo_url']['remove'])) {
            $data[$modelName]['photo_url'] = '';
        } else {
            unset($data[$modelName]['photo_url']);
        }

        $id = Api::call(Configure::read('API.url_nails_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error    
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
