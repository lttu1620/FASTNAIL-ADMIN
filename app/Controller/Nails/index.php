<?php

$modelName = $this->Nail->name;

//Process disable/enable
$this->doGeneralAction($modelName);
// get list price
$listPrice = MasterData::prices_all();
// create breadcrumb
$pageTitle = __('Nail list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'    => 'id',
        'type'  => 'text',
        'label' => __('ID'),
        'empty' => ''
    ))
    ->addElement(array(
        'id'    => 'photo_cd',
        'type'  => 'text',
        'label' => __('Photo Cd'),
        'empty' => ''
    ))
    ->addElement(array(
        'id'      => 'price',
        'label'   => __('Price'),
        'type'    => 'select',
        'options' => $listPrice,
        'empty'   => __('All'),
    ))    
    ->addElement(array(
        'id'      => 'time',
        'label'   => __('Time'),
        'type'    => 'select',
        'options' => Configure::read('Config.BookingDuration'),
        'empty'   => __('All'),
    ))
    ->addElement(array(
        'id'    => 'rank',
        'label' => __('Rank'),
        'empty' => ''
    ))
    ->addElement(array(
        'id'      => 'print',
        'label'   => __('Print'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty'   => __('All'),
    ))    
    ->addElement(array(
        'id'      => 'hp_coupon',
        'label'   => __('HP coupon'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty'   => __('All'),
    ))
    ->addElement(array(
        'id'      => 'coupon_type',
        'label'   => __('Coupon Type'),
        'type'    => 'select',
        'options' => Configure::read('Config.CouponType'),
        'empty'   => __('All'),
    ))
    /*
    ->addElement(array(
        'id'      => 'show_section',
        'label'   => __('Show section'),
        'options' => Configure::read('Config.showSection'),
        'empty'   => __('All'),
    ))
    ->addElement(array(
        'id'      => 'hf_section',
        'label'   => __('Hf section'),
        'options' => Configure::read('Config.hfSection'),
        'empty'   => __('All'),
    ))
    ->addElement(array(
        'id'      => 'limited',
        'label'   => __('Limited'),
        'options' => Configure::read('Config.BooleanValue'),
        'empty'   => __('All'),
    ))
    * 
    */
    ->addElement(array(
        'id'      => 'disable',
        'label'   => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty'   => __('All')
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'id-asc'         => __('ID Asc'),
            'id-desc'        => __('ID Desc'),
            'price-asc'      => __('Price Asc'),
            'price-desc'     => __('Price Desc'),
            'time-asc'       => __('Time Asc'),
            'time-desc'      => __('Time Desc'),
            'rank-asc'       => __('Rank Asc'),
            'rank-desc'      => __('Rank Desc'),
            'hp_coupon-asc'  => __('Hp coupon Asc'),
            'hp_coupon-desc' => __('Hp coupon Desc'),
            'created-asc'    => __('Created Asc'),
            'created-desc'   => __('Created Desc'),
            'photo_cd-asc'    => __('Photo Cd Asc'),
            'photo_cd-desc'   => __('Photo Cd Desc'),
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'      => 'limit',
        'label'   => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_nails_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'item',
        'name'  => 'items[]',
        'type'  => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id'    => 'id',
        'type'  => 'link',
        'title' => __('ID'),
        'href'  => '/' . $this->controller . '/detail/{id}',
        'width' => '30'
    ))
    ->addColumn(array(
        'id'    => 'image_url',
        'type'  => 'image',
        'title' => __('Image'),
        'src'   => '{image_url}',
        'width' => '60'
    ))
    ->addColumn(array(
        'id'    => 'photo_cd',        
        'title' => __('Photo Cd'),       
        'width' => '100'
    ))
    ->addColumn(array(
        'id'    => 'price',
        'title' => __('Price'),
        'type'  => 'number',
        'width' => 120,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'tax_price',
        'title' => __('Tax price'),
        'type'  => 'number',
        'width' => 120,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'sell_price',
        'title' => __('Sell price'),
        'type'  => 'number',
        'width' => 120,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'time',
        'title' => __('Time'),
        'type'  => 'number',
        'width' => 60,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'rank',
        'title' => __('Rank'),
        'width' => 60,
        'empty' => '0'
    ))
    ->addColumn(array(
        'id'    => 'favorite_count',
        'title' => __('Favorite count'),
        'type'  => 'number',
        'width' => 100,
        'empty' => '0'
    ))   
    ->addColumn(array(
        'id'    => 'hf_section',
        'title' => __('Hf section'),
        'width' => 140,
        'rules' => array(
            1 => 'H',
            2 => 'F',
        ),
        'empty' => '0'
    ))
    /*
    ->addColumn(array(
        'id'    => 'show_section',
        'title' => __('Show section'),
        'width' => 100,
        'empty' => '0'
    ))
    ->addColumn(array(
        'id'    => 'menu_section',
        'title' => __('Menu section'),
        'width' => 100,
        'empty' => '0'
    ))
    ->addColumn(array(
        'id'    => 'limited',
        'title' => __('Limited'),
        'width' => 80,
        'empty' => '0'
    ))
    * 
    */
    ->addColumn(array(
        'id'    => 'print',
        'title' => __('Print'),
        'width' => 80,
        'rules' => array(
            1 => __('Yes'),
            0 => __('No'),
        ),
        'empty' => '0'
    ))
    ->addColumn(array(
        'id'    => 'hp_coupon',
        'title' => __('HP coupon'),
        'width' => 100,
        'rules' => array(
            1 => __('Yes'),
            0 => __('No'),
        ),
        'empty' => '0'
    ))
    ->addColumn(array(
        'id'    => 'coupon_type',
        'title' => __('Coupon Type'),
        'width' => 100,
        'rules' => Configure::read('Config.CouponType'),
        'empty' => '0'
    ))
    ->addColumn(array(
        'id'    => 'created',
        'title' => __('Created'),
        'type'  => 'date',       
    ))
    ->addColumn(array(        
        'th_title' => __('Edit'),
        'type'  => 'link',
        'title' => '<i class="fa fa-edit"></i>',
        'href'  => '/' . $this->controller . '/update/{id}',
        'width' => '50'
    ))
    ->addColumn(array(
        'id'     => 'disable',
        'type'   => 'checkbox',
        'title'  => __('Status'),
        'toggle' => true,
        'rules'  => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty'  => 0,
        'width'  => 90,
    ))
    ->setDataset($data)
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
