<?php

App::uses('AppController', 'Controller');

/**
 * Ordernailistlogs of controller
 * @package Controller
 * @created 2015-05-04
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class OrdernailistlogsController extends AppController {
    
    /**
     * Construct
     * 
     * @author truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Action: index
     * 
     * @author truongnn
     * @return void
     */
    public function index() {
        include ('Ordernailistlogs/index.php');
    }
}
