<?php

App::uses('AppController', 'Controller');

/**
 * Userselectednaillogs Controller
 * 
 * @package Controller
 * @created 2015-05-28
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class UserselectednaillogsController extends AppController {

    /**
     * Construct
     * 
     * @author truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author truongnn 
     * @return void
     */
    public function index() {
        
        include ('Userselectednaillogs/index.php');
    }
}
