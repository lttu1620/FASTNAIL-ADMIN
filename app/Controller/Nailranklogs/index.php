<?php

$modelName = $this->Nailranklog->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Nail Rank Logs');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'        => 'nail_id',
        'label'     => __('Nail ID'),
        'empty'     => '',
    ))
    ->addElement(array(
        'id'        => 'date_from',
        'type'      => 'text',
        'calendar'  => true,
        'label'     => __('Date from'),
    ))
    ->addElement(array(
        'id'        => 'date_to',
        'type'      => 'text',
        'calendar'  => true,
        'label'     => __('Date to'),
    ))
    ->addElement(array(
        'id'        => 'limit',
        'label'     => __('Limit'),
        'options'   => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'      => 'submit',
        'value'     => __('Search'),
        'class'     => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array(
        'page'    => 1,
        'limit'   => Configure::read('Config.pageSize'))
);
list($total, $data) = Api::call(Configure::read('API.url_nailranklogs_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'id',
        'title' => __('ID'),
        'width' => '90'
    ))
    ->addColumn(array(
        'id'    => 'name',
        'title' => __('Admin name'),
        'empty' => ''
    ))
    ->addColumn(array(
        'id'     => 'photo_cd',
        'title'  => __('Photo cd'),
        'empty'  => '',
    ))
    ->addColumn(array(
        'id'     => 'photo_url',
        'title'  => __('Photo url'),
        'type'   => 'image',
        'empty'  => '',
        'width'  => '120'
    ))
    ->addColumn(array(
        'id'     => 'rank',
        'title'  => __('Rank'),
        'empty'  => '',
        'width' => 200,
    ))
    ->addColumn(array(
        'id'     => 'old_rank',
        'title'  => __('Old rank'),
        'empty'  => '',
        'width' => 200,
    ))
    ->addColumn(array(
        'id'     => 'created',
        'title'  => __('Created'),
        'type'   => 'date',
        'width' => 200,
    ))
    ->setDataset($data);
