<?php

App::uses('AppController', 'Controller');

/**
 * Nails Controller
 * 
 * @package Controller
 * @created 2015-03-23
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class NailsController extends AppController {

    /**
     * Construct
     * 
     * @author diennvt 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author diennvt 
     * @return void
     */
    public function index() {
        include ('Nails/index.php');
    }
    /**
     * detail action
     * 
     * @author diennvt 
     * @return void
     */
    public function detail($nail_id = 0) {
        include ('Nails/detail.php');
    }

    /**
     * update action
     * 
     * @author diennvt 
     */
    public function update($nail_id = 0) {
        include ('Nails/update.php');
    }
}
