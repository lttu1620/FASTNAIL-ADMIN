<?php

App::uses('AppController', 'Controller');

/**
 * Bannerrecommends Controller
 * 
 * @package Controller
 * @created 2015-07-06
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class BannerrecommendsController extends AppController {

    /**
     * Construct
     * 
     * @author diennvt 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author diennvt 
     * @return void
     */
    public function index() {
        include ('Bannerrecommends/index.php');
    }

    /**
     * update action
     * 
     * @author diennvt 
     */
    public function update($id = 0) {
        include ('Bannerrecommends/update.php');
    }
}
