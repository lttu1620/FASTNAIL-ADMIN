<?php

$modelName = $this->Userbeautyviewlog->name;

// create breadcrumb
$pageTitle = __('User beauty report');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'    => 'beauty_customid',
            'type'  => 'text',
            'label' => __('Beauty ID'),
        )
    )
   ->addElement(array(
        'id'       => 'date_from',
        'type'     => 'text',
        'calendar' => true,
        'label'    => __('Date from'),
    ))
    ->addElement(array(
        'id'       => 'date_to',
        'type'     => 'text',
        'calendar' => true,
        'label'    => __('Date to'),
    ))
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );
// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));

if (isset($param['beauty_customid'])) {
    $param['beauty_id'] = $param['beauty_customid'];
}
$param['get_beauty_report_all'] = 1;
list($total, $data) = Api::call(Configure::read('API.url_user_beauty_view_log_get_cvr'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);
// create data table
$this->SimpleTable      
    ->addColumn(
        array(
            'id'    => 'beauty_id',
            'title' => __('Beauty ID'),
            'empty' => '',
            'width' => 140,
        )
    )
    ->addColumn(
        array(
            'id'    => 'pv',
            'title' => __('PV'),            
            'empty' => '',
            'width' => 250,
        )
    )   
    ->addColumn(
        array(
            'id'    => 'uu',
            'title' => __('UU'),            
            'empty' => '',
            'width' => 250,
        )
    )   
    ->addColumn(
        array(
            'id'    => 'cvr',
            'title' => __('CVR'),            
            'empty' => '',
            'width' => 250,
        )
    )   
    ->addColumn(
        array(
            'id'    => 'date',
            'title' => __('Date'),
            'type'  => 'date',
            'width' => 250,
        )
    )
    
    ->setDataset($data);
