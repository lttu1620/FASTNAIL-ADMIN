<?php

$modelName = $this->Userbeautyviewlog->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('User beauty view log List');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'    => 'user_name',
            'type'  => 'text',
            'label' => '顧客',
        )
    )
    ->addElement(
        array(
            'id'    => 'beauty_customid',
            'type'  => 'text',
            'label' => __('Beauty ID'),
        )
    )
    ->addElement(
        array(
            'id'      => 'sort',
            'label'   => __('Sort'),
            'options' => array(
                'id-asc'       => __('ID Asc'),
                'id-desc'      => __('ID Desc'),
                'created-asc'  => __('Created Asc'),
                'created-desc' => __('Created Desc'),
            ),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );

// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
if (isset($param['user_customid'])) {
    $param['user_id'] = $param['user_customid'];
}
if (isset($param['beauty_customid'])) {
    $param['beauty_id'] = $param['beauty_customid'];
}
list($total, $data) = Api::call(Configure::read('API.url_user_beauty_view_log_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);
// create data table
$this->SimpleTable      
    ->addColumn(
        array(
            'id'    => 'image_url',
            'title' => __('Image url'),
            'src'   => '{image_url}',
            'type'  => 'image',
            'empty' => '',
            'width' => 250,           
        )
    )
    ->addColumn(
        array(
            'id'    => 'point',
            'title' => __('Point'),
            'empty' => '',
            'width' => 100,
            'type' => 'number',            
            'hidden' => true
        )
    )
    ->addColumn(
        array(     
            'id'    => 'mission',
            'title' => 'ミッション',
            'empty' => '',            
        )
    )        
    ->addColumn(
        array(
            'id'    => 'beauty_id',
            'title' => __('Beauty ID'),
            'empty' => '',
            'width' => 140,
            'hidden' => true
        )
    )
    ->addColumn(
        array(
            'id'    => 'title',
            'title' => __('Title'),
            'empty' => '',
            'width' => 150,
            'hidden' => true
        )
    )
    ->addColumn(
        array(
            'id'    => 'description',
            'title' => '内容',
            'empty' => '',
            'hidden' => true
        )      
    )
    /*
    ->addColumn(
        array(
            'id'    => 'user_id',
            'title' => __('User ID'),
            'empty' => '',
            'width' => 140,
        )
    )
    * 
    */
    ->addColumn(
        array(
            'id'    => 'name',
            'title' => '顧客',            
            'empty' => '',
            'width' => 150,     
        )
    )   
    ->addColumn(
        array(
            'id'    => 'created',
            'title' => __('Created'),
            'type'  => 'date',
            'width' => 140,
        )
    )
    ->addColumn(
        array(
            'id'     => 'disable',
            'type'   => 'link',
            'button' => true,
            'title'  => __('Approve'),
            'rules'  => array(
                '1' => __('Approve'),
            ),
            'onclick' => 'approve_beauty({user_id},{beauty_id});return false;',
            'empty'  => 0,
            'width'  => 90,
        )
    )
    ->setMergeColumn(array(
        'mission' => array(
            array(
                'field'  => 'beauty_id',
                'before' => __('Beauty ID') . ": ",
                'after'  => ''
            ),
            array(
                'field'  => 'title',
                'before' => __('Title') . ": ",
                'after'  => ''
            ),
            array(
                'field'  => 'description',
                'before' =>  "内容: ",
                'after'  => ''
            ),
            array(
                'field'  => 'point',
                'before' =>  __('Point') . ": ",
                'after'  => ''
            )
        ),       
    ))  
    ->setDataset($data);
