<?php

$modelName = $this->Material->name;
$model = $this->{$modelName};

// Create breadcrumb 
$is_update = false;
if (isset($material_id) && $material_id != 0) {
    $pageTitle = __('Update material');
    $is_update = true;
    $param['id'] = $material_id;
    $data[$modelName] = Api::Call(Configure::read('API.url_materials_detail'), $param);
    $this->Common->handleException(Api::getError());
} else {
    $pageTitle = __('Register material');
}
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/materials',
        'name' => __('Material list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));
$this->setPageTitle($pageTitle);
if ($is_update) { //Update material
    // Create Update form material
    $this->UpdateForm->reset()
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'material_section',
            'label' => __('Material section'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'material_cd',
            'label' => __('Material cd'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
    ));
} else { // Add new material
    $this->UpdateForm->setModelName($modelName)
        ->addElement(array(
            'id' => 'material_section',
            'label' => __('Material section'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'material_cd',
            'label' => __('Material cd'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
    ));
}

// Process when submit form
if ($this->request->is('post')) {
    $error = array();
    $data = $this->getData($modelName);
    if ($model->validateMaterialInsertUpdate($data)) {
        $id = Api::call(Configure::read('API.url_materials_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$material_id}");
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error    
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
