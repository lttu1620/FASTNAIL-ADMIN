<?php

$modelName = $this->Campaign->name;
$model = $this->{$modelName};

// Create breadcrumb 
$data[$modelName] = array();
$pageTitle = __('Add Campaign');
if (isset($id) && $id != 0) {
    $is_update = true;
    $param['id'] = $id;
    $data[$modelName] = Api::Call(Configure::read('API.url_campaigns_detail'), $param);
    $this->Common->handleException(Api::getError());
    $pageTitle = __('Edit Campaign');
}

$this->Breadcrumb
    ->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/campaigns',
            'name' => __('Campaign list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));
$this->setPageTitle($pageTitle);
$tags = MasterData::tags_all();
$tags = $this->Common->arrayKeyValue($tags, 'name', 'name');
$this->UpdateForm->reset()
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'tag_name',
            'label' => __('Tag name'),
            'options' => $tags
        ))
        ->addElement(array(
        		'id'       => 'start_date',
        		'type'     => 'text',
        		'calendar' => true,
        		'label'    => __('Date from'),
        ))
        ->addElement(array(
        		'id'       => 'end_date',
        		'type'     => 'text',
        		'calendar' => true,
        		'label'    => __('Date to'),
        ))
       ->addElement(array(
            'id' => 'image_url',
            'type' => 'file',
            'image' => true,
            'label' => __('Image'),
            'allowEmpty' => true,
            'class' => 'upload',
            'crop' => array(
                'field' => 'image_url',
            )
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

// Process when submit form
if ($this->request->is('post')) {
    $error = array();
    $data = $this->getData($modelName);
 
    if ($model->validateCampaignInsertUpdate($data)) {
        // Processing upload Image
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $image_url = $this->Image->uploadImage("{$modelName}.image_url");
            $data[$modelName]['image_url'] = $image_url;
        } elseif (!empty($model->data[$modelName]['image_url']['remove'])) {
        	$data[$modelName]['image_url'] = '';
        } else {
            unset($data[$modelName]['image_url']);
        }
        $id = Api::call(Configure::read('API.url_campaigns_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error    
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
