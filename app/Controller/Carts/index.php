<?php

$modelName = $this->Cart->name;

//Process disable/enable
$this->doGeneralAction($modelName);

//get list shop
$listShop = MasterData::shops_all();
$listShop = $this->Common->arrayKeyValue($listShop, 'id', 'name');

//get list nailist
$listNailist = MasterData::nailists_all();
$listNailist = $this->Common->arrayKeyValue($listNailist, 'id', 'name');

// create breadcrumb
$pageTitle = __('Cart list');
$this->setPageTitle($pageTitle);
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));
// Create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'shop_id',
            'label' => __('Shop'),
            'options' => $listShop,
            'empty' => __('All'),
        ))
        ->addElement(array(
            'id' => 'nailist_id',
            'label' => __('Nailist'),
            'options' => $listNailist,
            'empty' => __('All'),
        ))
        ->addElement(array(
            'id' => 'date_from',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date from'),
        ))
        ->addElement(array(
            'id' => 'date_to',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date to'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'id-asc' => __('ID Asc'),
                'id-desc' => __('ID Desc'),
                'shop_id-asc' => __('Shop Asc'),
                'shop_id-desc' => __('Shop Desc'),
                'total_price-asc' =>__('Price Asc'),
                'total_price-desc' =>__('Price Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_carts_list'), $param, false, array(0, array()));

//get list of name of nailists
foreach ($data as $index => $row) {
    $data[$index]['nailists'] = implode('<br />', array_column($row['nailists'], 'name'));
}

$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
        ->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'width' => '20'
        ))
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'total_price',
            'title' => __('Total price'),
            'width' => 50
        ))
        ->addColumn(array(
            'id' => 'reservation_date',
            'title' => __('Reservation date'),
            'type' => 'date',
            'width' => 50,
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'user_name',
            'title' => __('Username'),
            'width' => 50,
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'shop_name',
            'title' => __('Shop'),
            'width' => 150,
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'nailists',
            'title' => __('Nailist'),
            'width' => 150,
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Status'),
            'toggle' => true,
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),
            'empty' => 0,
            'width' => 20,
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));
