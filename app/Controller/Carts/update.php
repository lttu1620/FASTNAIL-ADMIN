<?php

$modelName = $this->Cart->name;
$model = $this->{$modelName};

// Create breadcrumb
$pageTitle = __('Update cart');
$param['id'] = $cart_id;

$data[$modelName] = Api::Call(Configure::read('API.url_carts_detail'), $param);
$this->Common->handleException(Api::getError());

//get list shop
$listShop = MasterData::shops_all();
$listShop = $this->Common->arrayKeyValue($listShop, 'id', 'name');

//get list shop
list($cnt, $listNailist) = Api::Call(Configure::read('API.url_nailists_list'), array('shop_id' => $data[$modelName]['shop_id']));
$this->setPageTitle($pageTitle);
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/carts',
            'name' => __('Cart list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// Create Update form order
$this->UpdateForm->reset()
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'shop_id',
            'label' => __('Shop'),
            'options' => $listShop,
            'empty' => __('All'),
        ))
        //hidden fields
        ->addElement(array('id' => 'id',
            'type' => 'hidden',
        ));

        //get list attributes
        $listAttributes = Api::Call(Configure::read('API.url_cartnailists_all'), array ('cart_id' => $cart_id, 'disable' => 0));

        $selectedArr = $this->Common->arrayValues($listAttributes[0], 'nailist_id');

        $options = $this->Common->arrayKeyValue($listNailist, 'id', 'name');
        $this->UpdateForm->addElement(array(
            'id' => 'nailist_id',
            'type' => 'select',
            'multiple' => 'checkbox',
            'label' => __('Nailist'),
            'options' => $options,
            'selected' => $selectedArr,
            'between' => "<div class='group-checkbox' id='nailist_id'>",
            'after' => "</div><div class=\"cls\"></div>",
        ))

        ->addElement(array(
            'id' => 'reservation_date',
            'label' => __('Reservation date'),
            'calendar' => true
        ))

        ->addElement(array(
            'id' => 'total_price',
            'type' => 'text',
            'label' => __('Total price'),
        ))

        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

//Get nail's orders list
$ordernailParam['order_id'] = $cart_id;
$orderNails = Api::call(Configure::read('API.url_ordernails_all'), $ordernailParam, false, array(0, array()));
$this->Common->handleException(Api::getError());

$nailTable = $this->SimpleTable
        ->setModelName('Ordernail')
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'nail_id',
            'title' => __('Nail ID'),
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'price',
            'title' => __('Price'),
            'width' => 50,
            'empty' => ''
        ))
        ->setDataset($orderNails)
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Status'),
            'toggle' => true,
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),
            'empty' => 0,
            'width' => 20,
            'class' => 'toggle-event ordernails'
        ));

$this->set('nailTable', $nailTable->get());

//Get item's orders list
$orderitemParam['order_id'] = $cart_id;
$orderItems = Api::call(Configure::read('API.url_orderitems_all'), $orderitemParam, false, array(0, array()));
$this->Common->handleException(Api::getError());

$itemTable = $this->SimpleTable
        ->reset()
        ->setModelName('Orderitem')
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'item_id',
            'title' => __('Item ID'),
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'item_name',
            'title' => __('Item Name'),
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'item_price',
            'title' => __('Item Price'),
            'width' => 50,
            'empty' => ''
        ))
        ->setDataset($orderItems)
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Status'),
            'toggle' => true,
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),
            'empty' => 0,
            'width' => 20,
            'class' => 'toggle-event orderitems'
        ));

$this->set('itemTable', $itemTable->get());

// Process when submit form
if ($this->request->is('post')) {
    $error = array();

    $postData = $this->getData($modelName);
    $data = array_replace_recursive($data, $postData);

    if ($model->validateCartInsertUpdate($data)) {
        if (!empty($data[$modelName]['nailist_id'])) {
            $data[$modelName]['nailist_id'] = implode(',', $data[$modelName]['nailist_id']);
        }
        $id = Api::call(Configure::read('API.url_carts_addupdate'), $data[$modelName]);

        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$cart_id}");
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
