<?php

App::uses('AppController', 'Controller');

/**
 * Orders Controller
 *
 * @package Controller
 * @created 2015-03-26
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class OrdersController extends AppController
{

    var $components = array('PhpExcel');

    /**
     * Construct
     *
     * @author Tran Xuan Khoa
     * @return void
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * index action
     *
     * @author Tran Xuan Khoa
     * @return void
     */
    public function index()
    {
        include('Orders/index.php');
    }

    /**
     * update action
     *
     * @author Tran Xuan Khoa
     */
    public function update($order_id = 0)
    {
        include('Orders/update.php');
    }

    /**
     * list action
     *
     * @author Tran Xuan Khoa
     * @return void
     */
    public function lists()
    {
        include('Orders/lists.php');
    }

    /**
     * update action
     *
     * @author VuLTH
     */
    public function calendar($date = 0)
    {
        include('Orders/calendar.php');
    }

    /**
     * update action
     *
     * @author VuLTH
     */
    public function csv()
    {
        include('Orders/csv.php');
    }

    /**
     * update action
     *
     * @author VuLTH
     */
    public function xls()
    {
        include('Orders/xls.php');
    }

    /**
     * update action
     *
     * @author VuLTH
     */
    public function xlsx()
    {
        include('Orders/xlsx.php');
    }

    /**
     * update action
     *
     * @author VuLTH
     */
    public function listcsv()
    {
        include('Orders/listcsv.php');
    }

    /**
     * update action
     *
     * @author VuLTH
     */
    public function listxls()
    {
        include('Orders/listxls.php');
    }

    /**
     * update action
     *
     * @author VuLTH
     */
    public function listxlsx()
    {
        include('Orders/listxlsx.php');
    }

    /**
     * detail action
     *
     * @author truongnn
     * @return void
     */
    public function detail($order_id = 0)
    {
        include('Orders/detail.php');
    }

    /**
     * Action: doUpdateStatisticAction
     *
     * @author Truongnn
     * @return void
     */
    public function doCancelAction($modelName)
    {
        $data = $this->request->data;
        if ($this->request->is('post')) {
            $id = empty($data['actionId']) ? 0 : $data['actionId'];
            if (!empty($id)) {
                $param = array(
                    'id'     => $id,
                    'cancel' => $data['action'] == 0 ? 1 : 0,
                );
                if (!Api::call(Configure::read('API.url_orders_cancel'), $param)) {
                    AppLog::warning("Can not update", __METHOD__, $data);
                    $this->Common->setFlashErrorMessage(__("Can not update"));
                }
                $this->Common->setFlashSuccessMessage(__("Data updated successfully"));
                return $this->redirect($this->request->here(false));
            }
        }
    }

    /**
     * Seat Action
     *
     * @return void
     */
    public function seat()
    {
        include('Orders/seat.php');
    }

    /**
     * Export Action
     *
     * @return void
     */
    public function export()
    {
        include('Orders/export.php');
    }

    /**
     * Export Action
     *
     * @return void
     */
    public function exportOrder()
    {
        include('Orders/exportorder.php');
    }
}
