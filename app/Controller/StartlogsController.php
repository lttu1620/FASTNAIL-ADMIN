<?php

/**
 * Startlog controller
 * 
 * @package Controller
 * @created 2015-01-09
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class StartlogsController extends AppController {

    public $uses = array('User','Startlog');

    /**
     * Construct
     * 
     * @author Truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     * 
     * @author Truongnn 
     * @return void
     */
    public function index() {
        include ('Startlogs/index.php');
    }

}
