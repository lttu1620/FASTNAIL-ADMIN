<?php

$this->layout = 'full';
$modelName = $this->Order->name;

$param = $this->getParams(array(
        'shop_id' => !empty($this->AppUI->shop_id) ? $this->AppUI->shop_id : 0,
        'date' => date('Y-m-d'),
        'sort' => 'reservation_date-ASC'
    )
);

$sortExplode = explode('-', !empty($param['sort']) ? $param['sort'] : '');

$data = Api::call(Configure::read('API.url_orders_allbydate'), $param, false, array()); 
$this->Common->handleException(Api::getError());

$this->SimpleTable
        ->setDataset($data)
        ->addColumn(array(
            'type' => 'link',
            'title' => __('medical chart'),
            'href' => '#',
            'onClick' => 'openMediacal({id});return false;',
            'target' => '_blank',
            'button' => true,
            'width' => '60'
        ))        
        ->addColumn(array(
            'id' => 'reservation_date',
            'title' => __('Time'),
            'type' => 'time',
            'width' => '40',
            'sort' => true,
            'sort-default' => true
        ))
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/detail/{id}',
            'width' => '30',
            'sort' => true
        ))
        ->addColumn(array(
            'id' => 'user_name',
            'title' => __('Name/Kana'),
            'width' => '100',
            'empty' => '',
            'sort' => true
        ))
        ->addColumn(array(
            'id' => 'kana',
            'title' => __('Kana'),
            'width' => '100',
            'empty' => '',
            'hidden' => true
        ))
        ->addColumn(array(
            'id' => 'status',
            'title' => __('Status'),
            'type' => 'link',            
            'href' => '#',
            'onClick' => 'javascript:void(0);',
            'class' => 'o_status',
            'show-break' => (!empty($sortExplode[0]) && $sortExplode[0] == 'status') ? '1' : '0' ,
            'data-class' => '{status}',
            'button' => true,
            'width' => '100',
            'rules' => Configure::read('Config.StatusOrder'),
            'sort' => true

        ))
        ->addColumn(array(
            'id' => 'phone',
            'title' => __('Phone'),
            'width' => '120',
            'empty' => '',
            'sort' => true
        ))
        ->addColumn(array(
            'th_title' => __('Edit'),
            'type' => 'link',
            'title' => '<i class="fa fa-edit"></i>',
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '50'
        ))->setMergeColumn(array(
            'user_name'  => array(
                array(
                    'field'  => 'kana',
                    'before'  => '(',
                    'after'  => ')'
                )
        )));
// export data
$exportUrl = '';
$param1 = $param;
unset($param1['page']);
unset($param1['limit']);
foreach ($param1 as $key => $value) {
    if (is_array($value)) {
        foreach ($value as $v) {
            $exportUrl .= $key . '%5B%5D=' . $v . '&';
        }
    } else {
        $exportUrl .= $key . '=' . $value . '&';
    }
}
$this->set('exportUrl', $exportUrl);

$listNailist = Api::Call(Configure::read('API.url_nailists_all'), array('shop_id' => $this->AppUI->shop_id));
$this->set('nailists', $listNailist);
$this->set('hidden_date', $param['date']);
$this->set('keyword', empty($param['keyword']) ? '' : $param['keyword']);
$this->set('nailist_id', empty($param['nailist_id']) ? '' : $param['nailist_id']);
$this->set('dt', $this->getParam('date', date('Y-m-d')));