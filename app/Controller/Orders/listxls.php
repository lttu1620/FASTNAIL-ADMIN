<?php

$modelName = $this->Order->name;

$param = $this->getParams(array('page' => 1, 'limit' => 10000));
$data = Api::call(Configure::read('API.url_orders_allbydate'), $param, false, array());
$this->Common->handleException(Api::getError());

//get list of name of nailists
foreach ($data as $index => &$row) {
    if (!empty($row['nailists'])) {
        $row['nailists'] = implode("\n", array_column($row['nailists'], 'name'));
    } else {
        $row['nailists'] = '';
    }
}
$this->PhpExcel->createWorksheet()
        ->setDefaultFont('Calibri', 12);
// define table cells
$table = array(
    array('label' => __('ID')),
    array('label' => __('Time')),
    array('label' => __('Customer name')),
    array('label' => __('Email')),
    array('label' => __('Phone')),
    array('label' => __('Staff'), 'width' => 50, 'wrap' => true),
    array('label' => __('Author')),
    array('label' => __('Created')),
);
// add heading with different font and bold text
$this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));

// add data
foreach ($data as $value) {
    $value['created'] = $this->Common->dateFormat($value['created']) != false ? $this->Common->dateFormat($value['created']) : '';
    $this->PhpExcel->addTableRow(array(
        $value['id'],
        date('H:i', $value['reservation_date']),
        $value['user_name'],
        $value['email'],       
        $value['phone'],
        $value['nailists'],
        $value['admin_name'],
        $value['created']
    ));
}
// close table and output
$fileName = 'orders-' . date('Ymd', strtotime($param['date'])) . '-' . date('YmdHis') . '.xls';
$this->PhpExcel->addTableFooter()
        ->output($fileName, 'Excel5');
