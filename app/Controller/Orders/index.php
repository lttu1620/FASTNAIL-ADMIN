<?php

$modelName = $this->Order->name;

//Process disable/enable
$this->doGeneralAction($modelName);

//get list shop
$listShop = MasterData::shops_all();
$listShop = $this->Common->arrayKeyValue($listShop, 'id', 'name');
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));

//get list nailist, device
if (empty($param['shop_id'])) {
    $listNailist = MasterData::nailists_all();
    $listDevice = MasterData::devices_all();
} else {
    $listNailist = Api::Call(Configure::read('API.url_nailists_all'), array('shop_id' => $param['shop_id']));
    $listDevice = Api::Call(Configure::read('API.url_devices_all'), array('shop_id' => $param['shop_id']));
}
$listNailist = $this->Common->arrayKeyValue($listNailist, 'id', 'name');
$listDevice = $this->Common->arrayKeyValue($listDevice, 'id', 'name');

//get list item
$listItem = $this->Common->arrayKeyValue(MasterData::items_all(), 'id', 'name');

// create breadcrumb
$pageTitle = __('Order list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'    => 'order_customid',
        'type'  => 'text',
        'label' => __('Order ID'),
    ));
    if(!$this->AppUI->shop_is_franchise){
        $this->SearchForm->setModelName($modelName)->addElement(array(
            'id'      => 'shop_id',
            'label'   => __('Shops'),
            'options' => $listShop,
            'empty'   => __('All'),
        ));
    }
    $this->SearchForm->setModelName($modelName)->addElement(array(
        'id'       => 'nailist_id',
        'type'     => 'select',
        //'multiple' => 'checkbox',
        'label'    => __('Nailists'),
        'options'  => $listNailist,
        'empty'    => __('All'),
        'selected' => empty($param['nailist_customid']) ? '' : $param['nailist_customid'],
        'between'  => "<div class='selectbox' id='nailist_id'>",
        'after'    => "</div><div class=\"cls\"></div>",
    ))
    ->addElement(array(
        'id'    => 'email',
        'type'  => 'text',
        'label' => __('Email'),
    ))
    ->addElement(array(
        'id'    => 'user_name',
        'type'  => 'text',
        'label' => __('Customer name'),
    ))
    ->addElement(array(
        'id'    => 'phone',
        'type'  => 'text',
        'label' => __('Phone'),
    ))
    ->addElement(array(
        'id'       => 'device_id',
        'type'     => 'select',
        //'multiple' => 'checkbox',
        'label'    => __('Device'),
        'options'  => $listDevice,
        'empty'    => __('All'),
        'selected' => empty($param['device_customid']) ? '' : $param['device_customid'],
        'between'  => "<div class='selectbox' id='device_id'>",
        'after'    => "</div><div class=\"cls\"></div>",
    ))
    ->addElement(array(
        'id'      => 'item_id',
        'label'   => __('Items'),
        'options' => $listItem,
        'empty'   => __('All')
    ))
    ->addElement(array(
        'id'       => 'date_from',
        'type'     => 'text',
        'calendar' => true,
        'label'    => __('Date from'),
    ))
    ->addElement(array(
        'id'       => 'date_to',
        'type'     => 'text',
        'calendar' => true,
        'label'    => __('Date to'),
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'id-asc'                => __('ID Asc'),
            'id-desc'               => __('ID Desc'),
            'user_name-asc'         => __('Username Asc'),
            'user_name-desc'        => __('Username Desc'),
            'email-asc'             => __('Email Asc'),
            'email-desc'            => __('Email Desc'),
            'reservation_date-asc'  => __('Reservation date Asc'),
            'reservation_date-desc' => __('Reservation date Desc'),
            'nail_length-asc'       => __('Nail Length Asc'),
            'nail_length-desc'      => __('Nail Length Desc'),
            'created-asc'           => __('Created Asc'),
            'created-desc'          => __('Created Desc')
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'      => 'limit',
        'label'   => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
    		'id' => 'is_cancel',
    		'label' => __('Cancel'),
    		'options' => Configure::read('Config.BooleanValue'),
    		'empty' => Configure::read('Config.StrAll'),
    ))
    ->addElement(array(
        'id'    => 'hp_order_code',
        'type'  => 'text',
        'label' => __('Hp Order Code'),
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));
    
if ($this->AppUI->shop_is_franchise){
    $param['shop_id'] = $this->AppUI->shop_id;
}
list($total, $data) = Api::call(Configure::read('API.url_orders_list'), $param, false, array(0, array()));

$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'item',
        'name'  => 'items[]',
        'type'  => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id'    => 'id',
        'type'  => 'link',
        'title' => __('ID'),
        'href'  => '/' . $this->controller . '/detail/{id}',
        'width' => '70'
    ))
    ->addColumn(array(
        'id'    => 'user_name',
        'title' => __('Customer name'),
        'width' => 150,
        'empty' => ''
    ))
    /*
    ->addColumn(array(
        'id'    => 'reservation_date',
        'title' => __('Reservation date'),
        'type'  => 'datetime', // Ex: 2015年04月17日
        'width' => 150,
        'empty' => ''
    ))
    * 
    */
    ->addColumn(array(
        'id'    => 'total_sell_price',
        'title' => __('Sell price'),
        'width' => 200,
        'type' => 'number',
        'empty' => 0,
    ))
    ->addColumn(array(
        'id'     => 'total_price',
        'title'  => __('Price'),
        'type'  => 'number',        
        'empty'  => 0,
        'hidden' => true
    ))
    ->addColumn(array(
        'id'    => 'total_tax_price',
        'title' => __('Tax price'),
        'type' => 'number',
        'empty' => 0,    
        'hidden' => true
    ))
    ->addColumn(array(
        'id'    => 'order_date',
        'title' => __('Order time'),
        'width' => 220,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'     => 'order_start_date',
        'title'  => __('Order start time'),
        'type'   => 'datetime',
        'width'  => 150,
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'order_end_date',
        'title'  => __('Order end time'),
        'type'   => 'datetime',
        'width'  => 150,
        'empty'  => '',
        'hidden' => true
    ))
    /*
    ->addColumn(array(
        'id'    => 'order_off_date',
        'title' => __('Nailoff time'),
        'width' => 200,
        'empty' => ''
    ))    
    ->addColumn(array(
        'id'     => 'off_start_date',
        'title'  => __('Order off start time'),
        'type'   => 'datetime',
        'width'  => 150,
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'off_end_date',
        'title'  => __('Order off end time'),
        'type'   => 'datetime',
        'width'  => 150,
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'    => 'sr_date',
        'title' => __('Service time'),
        'width' => 200,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'     => 'sr_start_date',
        'title'  => __('Service start time'),
        'type'   => 'datetime',
        'width'  => 150,
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'sr_end_date',
        'title'  => __('Service end time'),
        'type'   => 'datetime',
        'width'  => 150,
        'empty'  => '',
        'hidden' => true
    ))
     * 
     */
    ->addColumn(array(
        'id'    => 'shop_name',
        'title' => __('Shops'),
        'width' => 100,
        'empty' => ''
    ));
if (!empty($param['nailist_customid'])) {
    $this->SimpleTable
        ->addColumn(array(
            'id'    => 'nailist_name',
            'title' => __('Nailist'),
            'width' => 100,
            'empty' => ''
        ));
}
if (!empty($param['device_customid'])) {
    $this->SimpleTable
        ->addColumn(array(
            'id'    => 'device_name',
            'title' => __('Device'),
            'width' => 100,
            'empty' => ''
        ));
}
$this->SimpleTable
    ->addColumn(array(
        'id'     => 'email',
        'title'  => __('Email'),
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'phone',
        'title'  => __('Phone'),
        'empty'  => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'hp_order_code',
        'title'  => __('Hp Order Code'),
        'empty'  => '',
        'width' => 100,
    ))
    ->addColumn(array(
        'id'    => 'created',
        'title' => __('Created'),
        'type'  => 'dateonly', // Ex: 2015年04月17日
        'width' => 120
    ))
    /*
    ->addColumn(array(
        'id'    => 'log',
        'title' => __('Log'),
        'type'  => 'link',
        'target'  => '_blank',
        'button'  => true,
        'href' => '/ordernailistlogs?order_id={id}',
        'width' => 50
    ))  
     * 
     */
    ->addColumn(array(        
        'th_title' => __('Edit'),
        'type'  => 'link',
        'title' => '<i class="fa fa-edit"></i>',
        'href'  => '/' . $this->controller . '/update/{id}',
        'width' => '50'
    ))  
    /*
    ->addColumn(array(
    		'id'     => 'is_cancel',
    		'type'   => 'checkbox',
    		'title'  => __('Cancel'),
    		'toggle' => true,
    		'class'=>'cancel',
    		'rules'  => array(
    				'0' => '',
    				'1' => 'checked'
    		),
    		'empty'  => 0,
    		'width'  => 90,
    ))
    * 
    */
    ->addColumn(array(
        'id'     => 'disable',
        'type'   => 'checkbox',
        'title'  => __('Status'),
        'toggle' => true,
        'rules'  => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty'  => 0,
        'width'  => 80,
    ))
    ->setDataset($data)
    ->setMergeColumn(array(
        'user_name'  => array(
            array(
                'field'  => 'email',
                'before' => __('Email') . ": ",
                'after'  => ''
            ),
            array(
                'field'  => 'phone',
                'before' => __('Phone') . ": ",
                'after'  => ''
            )
        ),
        'total_sell_price' => array(
            array(
                'field'  => 'total_price',
                'before' => __('Price') . ": ",
                'after'  => ''
            ),
            array(
                'field'  => 'total_tax_price',
                'before' => __('Tax price') . ": ",
                'after'  => ''
            )
        ),
        'order_date' => array(
            array(
                'field'  => 'order_start_date',
                'before' => __('Start') . ": ",
                'after'  => ''
            ),
            array(
                'field'  => 'order_end_date',
                'before' => __('End') . ": ",
                'after'  => ''
            )
        ),
        'order_off_date' => array(
            array(
                'field'  => 'off_start_date',
                'before' => __('Start') . ": ",
                'after'  => ''
            ),
            array(
                'field'  => 'off_end_date',
                'before' => __('End') . ": ",
                'after'  => ''
            )
        ),
        'sr_date'    => array(
            array(
                'field'  => 'sr_start_date',
                'before' => __('Start') . ": ",
                'after'  => ''
            ),
            array(
                'field'  => 'sr_end_date',
                'before' => __('End') . ": ",
                'after'  => ''
            )
        )
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
$exportUrl = '';
$param1 = $param;
unset($param1['page']);
unset($param1['limit']);

foreach ($param1 as $key => $value) {
    if (is_array($value)) {
        foreach ($value as $v) {
            $exportUrl .= $key . '%5B%5D=' . $v . '&';
        }
    } else {
        $exportUrl .= $key . '=' . $value . '&';
    }
}
$this->set('exportUrl', $exportUrl);
?>
