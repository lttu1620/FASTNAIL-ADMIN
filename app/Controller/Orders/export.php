<?php

$modelName = $this->Order->name;
$model = $this->{$modelName};

// Create breadcrumb
$pageTitle = __('Order export');
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(
        array(
            'link' => $this->request->base.'/orders',
            'name' => __('Order list'),
        )
    )
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$this->setPageTitle($pageTitle);
// Create Update form order
$this->UpdateForm->reset()
    ->setModelName($modelName)
    ->addElement(
        array(
            'id'    => 'user_id',
            'label' => __('User ID'),
            'type'  => 'text',
        )
    )
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => array(
                100     => '100',
                500     => '500',
                1000    => '1,000',
                5000    => '5,000',
                10000   => '10,000',
                50000   => '50,000',
                100000  => '100,000',
                500000  => '500,000',
            ),
        )
    )
    ->addElement(
        array(
            'id'       => 'date_from',
            'type'     => 'text',
            'calendar' => true,
            'label'    => __('Date from'),
        )
    )
    ->addElement(
        array(
            'id'       => 'date_to',
            'type'     => 'text',
            'calendar' => true,
            'label'    => __('Date to'),
        )
    )
    /*->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'id-asc'                => __('ID Asc'),
            'id-desc'               => __('ID Desc'),
            'user_name-asc'         => __('Username Asc'),
            'user_name-desc'        => __('Username Desc'),
            'email-asc'             => __('Email Asc'),
            'email-desc'            => __('Email Desc'),
            'reservation_date-asc'  => __('Reservation date Asc'),
            'reservation_date-desc' => __('Reservation date Desc'),
            'nail_length-asc'       => __('Nail Length Asc'),
            'nail_length-desc'      => __('Nail Length Desc'),
            'created-asc'           => __('Created Asc'),
            'created-desc'          => __('Created Desc')
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))*/
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        )
    )
    ->addElement(
        array(
            'type'    => 'submit',
            'value'   => __('Cancel'),
            'class'   => 'btn pull-left',
            'onclick' => 'return back();',
        )
    );

// Process when submit form
if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    $option = '';
    foreach ($data[$modelName] as $key => $value) {
        if ($key != 'model_name' && $value != '') {
            $option .= $key.'='.$value.'&';
        }
    }
    $option = !empty($option) ? '?'.$option : $option;
    return $this->redirect('/orders/exportorder'.$option);
}
