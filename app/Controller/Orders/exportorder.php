<?php

$modelName = $this->Order->name;

@ini_set('memory_limit', '-1');
$param = $this->getParams(array('page' => 1, 'limit' => 100));
$param['limit'] = $param['limit'] > 500000 ? 500000 : $param['limit'];
list($total, $data) = Api::call(Configure::read('API.url_orders_export'), $param, false, array('total' => 0, 'data' => array()));

$fileName = 'order-export-' . date('YmdHis') . '.csv';
header('Content-Encoding: UTF-8');
header("Content-type: text/csv; charset=UTF-8");
header("Content-Disposition: attachment; filename={$fileName}");
echo "\xEF\xBB\xBF";
header("Pragma: no-cache");
header("Expires: 0");

$export = 'Dates'.',' .date('Ymd', time())."\n"
    .'予約番号'.',' // orders.id
    .'ステータス'.',' // orders.status
    .'会員ID'.',' // users.code
    .'利用店舗番号'.',' // shops.id
    .'利用店舗名'.',' // shops.name
    .'氏名'.',' // orders.name
    .'氏名（フリガナ）'.',' // orders.kana
    .'性別'.',' // orders.sex
    .'電話番号'.',' // orders.phone
    .'メールアドレス'.',' // orders.email
    .'メルマガ可・不可'.',' // users.is_magazine
    .'生年月日'.',' // users.birthday
    .'都道府県'.',' // prefectures.name where users.prefecture_id
    .'住所'.',' // users.address1
    .'建物名'.',' // users.address2
    .'予約日'.',' // from_unixtime(orders.reservesion_date) <- only display date
    .'予約時間'.',' // from_unixtime(orders.reservesion_date) <- only display time
    .'来店日'.',' // from_unixtime(orders.start_date) <- only display date
    .'来店時間'.',' // from_unixtime(orders.start_date) <- only display time
    .'サイトに来たきっかけ'.',' // users.visit_element where orders.users_id
    .'予約方法'.',' // orders.reservation_type (same text as admin)
    .'来店経路'.',' // orders.visit_element (same text as admin)
    .'予約メモ'.',' // orders.reservation_request
    .'前回来店日'.',' // orders.reservation_date (last time if nodata display "-")
    .'前々回来店日'.',' // orders.reservation_date (2 times ago if nodata display "-")
    .'3ヶ月以内の来店回数'.',' // ( select count(id) from orders where user_id = @id and between reservation_date and reservation_date - 3 months and disable = 0)
    .'6ヶ月以内の来店回数'.',' // ( select count(id) from orders where user_id = @id and between reservation_date and reservation_date - 6 months and disable = 0)
    .'12ヶ月以内の来店回数'.',' //  ( select count(id) from orders where user_id = @id and between reservation_date and reservation_date - 12 months and disable = 0)
    .'キャンセル日'.',' // orders.disable_by_user
    .'無断キャンセル回数'.',' // ( select count(id) from orders where user_id = @id and disable = 1 and disable_by_user = 0)
    .'メニュー'.',' // orders.purpose (same text as admin) if multiple data separate with slash "/"
    .'整理番号'.',' // orders. shop_counter_code
    .'予約回数'.',' // ( select count (id) from orders where user_id =@id )
    .'合計売上(税抜)'.',' // total price without tax
    .'写真番号'.',' // order_nails.photo_cd
    .'デザイン金額'.',' // order_nails.price without tax
    .'オプション金額'.',' // sum(price) from order_services where order_id =@id and type=オフ＆リペア
    .'物販金額'.',' // sum(price) from order_services where order_id =@id and type =物販
    .'割引'.',' // sum(price) from order_services where order_id =@id and type=特典
    .'Rポイント'.',' //orders.r_point
    .'保有FNポイント'.',' // users.fn_point
    .'FN利用ポイント'.',' // orders.fn_point
    .'新規/リピーター'.',' // (select count(id) from orders where reservation_date <= @reservation_date ) -> if 1: 新規 / over 2:リピーター
    .'デザイン事前決定予約かどうか'.',' // orders.reservation_method=1  -> はい
    .'日時のみ予約かどうか'.',' // orders.reservation_method=2 いいえ
    .'コンサル担当ID'.',' // nailists.id (consultant)
    .'コンサル担当者名'.',' // nailists.name  (consultant)
    .'オフ担当ID'.',' // nailists.id (nail off)
    .'オフ担当者名'.',' // nailists.name (nail off)
    .'アテンド担当ID'.',' // nailists.id (attend)
    .'アテンド担当者名'.',' // nailists.name (attend)
    .'予約時滞在時間'.',' // (orders.sr_end_date - orders.start_date)  <- change to minutes
    .'受付時間'.',' // from_unixtime(orders.start_date) <- only display time
    .'コンサル開始'.',' // from_unixtime(orders.consult_start_date)
    .'コンサル終了'.',' // from_unixtime(orders.consult_end_date)
    .'オフ開始'.',' //  from_unixtime(orders.off_start_date)
    .'オフ終了'.',' // from_unixtime(orders.off_end_date)
    .'アテンド開始'.',' //  from_unixtime(orders.sr_start_date)
    .'アテンド終了'.',' //  from_unixtime(orders.sr_end_date)
    .'最終ログイン日時' // last user login date
    ."\n";
foreach ($data as $item) {
    foreach ($item as &$i) {
        if ($i === null || $i == '') $i = '-';
    }
    $export .=
        $item['id'].','
        .Configure::read('Config.StatusOrderExport.'.$item['status'], '-').','
        .$item['code'].','
        .$item['shop_id'].','
        .$item['shop_name'].','
        .$item['user_name'].','
        .$item['kana'].','
        .$item['sex'].','
        .$item['phone'].','
        .$item['email'].','
        .$item['is_magazine'].','
        .$item['birthday'].','
        .$item['prefecture_name'].','
        .$item['address1'].','
        .$item['address2'].','
        .$item['reservation_date'].','
        .$item['reservation_time'].','
        .$item['start_date'].','
        .$item['start_time'].','
        .Configure::read('Config.VisitElement.'.$item['user_visit_element'], '-').','
        .Configure::read('Config.ReservationType.'.$item['reservation_type'], '-').','
        .Configure::read('Config.VisitElement.'.$item['order_visit_element'], '-').','
        .$item['request'].','
        .$item['last_time'].','
        .$item['second_last_time'].','
        .$item['last_3_months'].','
        .$item['last_6_months'].','
        .$item['last_12_months'].','
        .Configure::read('Config.BooleanValue.'.$item['is_cancel'], '-').','
        .$item['auto_cancel'].','
        .Configure::read('Config.OrderPurpose.'.$item['purpose'], '-').','
        .$item['shop_counter_code'].','
        .$item['total_order'].','
        .$item['total_price'].','
        .$item['photo_cd'].','
        .$item['order_nail_price'].','
        .$item['item_section_1_2'].','
        .$item['item_section_3'].','
        .$item['item_section_4'].','
        .$item['r_point'].','
        .$item['user_point'].','
        .$item['fn_point'].','
        .Configure::read('Config.NewRepeat.'.$item['new_or_repeat'], '-').','
        .Configure::read('Config.BooleanValue.'.$item['order_by_design'], '-').','
        .Configure::read('Config.BooleanValue.'.$item['order_by_day'], '-').','
        .$item['consultant_nailist_id'].','
        .$item['consultant_nailist_name'].','
        .$item['nail_off_nailist_id'].','
        .$item['nail_off_nailist_name'].','
        .$item['attend_nailist_id'].','
        .$item['attend_nailist_name'].','
        .$item['all_time'].','
        .$item['start_time'].','
        .$item['consult_start_date'].','
        .$item['consult_end_date'].','
        .$item['off_start_date'].','
        .$item['off_end_date'].','
        .$item['sr_start_date'].','
        .$item['sr_end_date'].','
        .$item['last_login']
        ."\n"
    ;
}
echo $export;
exit;