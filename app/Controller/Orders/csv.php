<?php

$modelName = $this->Order->name;

$param = $this->getParams(array('page' => 1, 'limit' => 10000));
list($total, $data) = Api::call(Configure::read('API.url_orders_list'), $param, false, array(0, array()));

$fileName = 'orders-' . date('YmdHis') . '.csv';
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename={$fileName}");
header("Pragma: no-cache");
header("Expires: 0");

$result = __('ID') . ','
        . __('Sales Code') . ','
        . __('Reservation date') . ','
        . __('Username') . ','
        . __('Shop') . ','
        . __('Nailist') . ','
        . __('Nail length') . ','
        . __('Address') . ','
        . __('Email') . ','
        . __('Phone') . ','
        . __('Created') . "\n";

foreach ($data as $value) {
    $nailist_name = '';
    if (isset($param['nailist_customid']) && !empty($param['nailist_customid'])) {
        $nailist_name = $value['nailist_name'];
    }
    $reservation_date = $this->Common->dateFormat($value['reservation_date']) != false ? $this->Common->dateFormat($value['reservation_date']) : '';
    $created = $this->Common->dateFormat($value['created']) != false ? $this->Common->dateFormat($value['created']) : '';
    $result.= $value['id'] . ',' 
            . $value['sales_code'] . ',' 
            . $reservation_date . ',' 
            . $value['user_name'] . ','
            . $value['shop_name'] . ',' 
            . $nailist_name . ',' 
            . $value['nail_length'] . ',' 
            . $value['address1'] . ',' 
            . $value['email'] . ',' 
            . $value['phone'] . ',' 
            . $created . "\n";
}
echo $result;
exit;