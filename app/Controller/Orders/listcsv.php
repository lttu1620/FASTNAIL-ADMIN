<?php

$modelName = $this->Order->name;

$param = $this->getParams(array('page' => 1, 'limit' => 10000));
$data = Api::call(Configure::read('API.url_orders_allbydate'), $param, false, array());
$this->Common->handleException(Api::getError());

//get list of name of nailists
foreach ($data as $index => &$row) {
    if (!empty($row['nailists'])) {
        $row['nailists'] = implode('|', array_column($row['nailists'], 'name'));
    } else {
        $row['nailists'] = '';
    }
}

$fileName = 'orders-' . date('Ymd', strtotime($param['date'])) . '-' . date('YmdHis') . '.csv';
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename={$fileName}");
header("Pragma: no-cache");
header("Expires: 0");
$result = __('ID') . ','
        . __('Time') . ','
        . __('Customer name') . ','
        . __('Email') . ','
        . __('Phone') . ','
        . __('Staff') . ','
        . __('Author') . ','       
        . __('Created') . "\n";
foreach ($data as $value) {
    $value['created'] = $this->Common->dateFormat($value['created']) != false ? $this->Common->dateFormat($value['created']) : '';
    $result.= $value['id'] . ','
            . date('H:i', $value['reservation_date']) . ',' 
            . $value['user_name'] . ','
            . $value['email'] . ',' 
            . $value['phone'] . ',' 
            . $value['nailists'] . ',' 
            . $value['admin_name'] . ',' 
            . $value['created'] . "\n";
}
echo $result;
exit;
