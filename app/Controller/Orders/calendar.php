<?php

$this->layout = 'full';
// set date for calendar
if (isset($this->request->query['date'])) {
    $dt = $this->request->query['date'];
} else {
    $dt = date('Y-m-d');
}
$this->set('dt', $dt);

$date = date('Y-m-d');
if ($this->getParam('date')) {
    $dt = \DateTime::createFromFormat('Y-m-d', $_REQUEST['date']);
    if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
        return $this->redirect("/{$this->controller}/calendar?date={$date}");
    } else {
        $date = $_REQUEST['date'];
    }
}
$data = Api::Call(Configure::read('API.url_orders_month_calendar'), array(
            'shop_id' => !empty($this->AppUI->shop_id) ? $this->AppUI->shop_id : '',
            'date' => $date
        ));
$this->Common->handleException(Api::getError());
$this->set('data', $data['calendarData']);
$this->set('info', $data['calendarInfo']);
