<?php
$modelName = $this->Order->name;

// Process update statistics
$this->doCancelAction($modelName);

 $pageTitle = '';
// Create breadcrumb
 $order_id = empty($order_id) ? 0 : $order_id;
$pageTitle = __('Order detail')."&nbsp;ID: ".h($order_id);
$param['id'] = $order_id;
$data = Api::Call(Configure::read('API.url_orders_detail_for_view'), $param);
$this->Common->handleException(Api::getError());
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/orders',
        'name' => __('Order list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));
$this->setPageTitle($pageTitle);
$this->set('order', $data) ;
$order_nails = $this->SimpleTable
    ->addColumn(array(
        'id'    => 'id',
        'title' => __('ID'),
        'width' => 100
    ))
    ->addColumn(array(
        'id'    => 'image_url',
        'type'  => 'image',
        'title' => __('Image'),
        'src'   => '{image_url}',
        'width' => '60'
    ))
    ->addColumn(array(
        'id'    => 'photo_cd',
        'title' => __('Photo Cd'),
        'width' => 100,
        'empty' => '',
    ))
    ->addColumn(array(
        'id'     => 'price',
        'title'  => __('Price'),
        'type'  => 'number',
        'width'  => 150,
        'empty'  => '',
    ))
    ->addColumn(array(
        'id'    => 'tax_price',
        'title' => __('Tax price'),
        'type' => 'number',
        'empty' => '',
        'width' => 150
    ))
    ->addColumn(array(
        'id'    => 'sell_price',
        'title' => __('Sell price'),
        'type'  => 'number',
        'width' => 150
    ))    
    ->addColumn(array(
        'id'    => 'hp_coupon',
        'title' => __('HP coupon'),
        'rules' => Configure::read('Config.BooleanValue'),        
    ))
    ->setDataset($data['order_nails']);

$this->set('order_nails', $order_nails->get());

$order_items = $this->SimpleTable
    ->reset()     
    ->addColumn(array(
        'id'    => 'id',
        'title' => __('ID'),
        'width' => 100
    ))
    ->addColumn(array(
        'id'    => 'item_cd',
        'title' => __('Code'),
        'width' => 80,
        'empty' => '',
    ))
    ->addColumn(array(
        'id'     => 'item_name',
        'title'  => __('Item name'),
        'empty'  => '',
    ))
    ->addColumn(array(
        'id'    => 'item_quantity',
        'title' => __('Item quantity'),
        'type'  => 'number',
        'empty' => '',
        'width' => 120,
    ))
    ->addColumn(array(
        'id'    => 'item_price',
        'title' => __('Price'),
        'type' => 'number',
        'empty' => '',
        'width' => 150
    ))
    ->addColumn(array(
        'id'    => 'item_tax_price',
        'title' => __('Tax price'),
        'type' => 'number',
        'empty' => '',
        'width' => 150
    ))    
    ->addColumn(array(
        'id'    => 'sell_price',
        'title' => __('Sell price'),
        'type'  => 'number',
        'empty' => '',
        'width' => 150,
    ))
    ->setDataset($data['order_items']);

$this->set('order_items', $order_items->get());

$order_nailist_logs = $this->SimpleTable
    ->reset()
    ->addColumn(array(
        'id'    => 'nailist_image_url',
        'type'  => 'image',
        'title' => __('Image'),
        'src'   => '{nailist_image_url}',
        'width' => '60'
    ))
    ->addColumn(array(
        'id'    => 'nailist_name',
        'title' => __('Nailist name'),
        'empty' => '',
    ))
    ->addColumn(array(
        'id'    => 'created',
        'title' => __('Created'),
        'type'   => 'date',
        'width'  => 150,
        'empty'  => '',
    ))
    ->setDataset($data['order_nailist_logs']);

$this->set('order_nailist_logs', $order_nailist_logs->get());
