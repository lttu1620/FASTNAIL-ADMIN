<?php

if (!empty($this->AppUI->shop_id)) {
    $this->layout = 'full';
    $param['shop_id'] = $this->AppUI->shop_id;
    if (isset($this->request->query['date'])) {
        $dt = $this->request->query['date'];
    } else {
        $dt = date('Y-m-d');
    }
    $param['date'] = $dt;
    $param['is_seat'] = 1;
    $result = Api::call(Configure::read('API.url_orders_date_calendar_data'), $param, false, array());
    $this->Common->handleException(Api::getError());
    
    $shop = $result['shop'];
    $data = $result['calendar_json'];
    $max_real_seat = !empty($result['max_real_seat']) ? $result['max_real_seat'] : ($shop['max_seat'] + $shop['hp_max_seat']);
    $order_timely_limit = $result['order_timely_limit'];
    $max_seat = $shop['max_seat'] + $shop['hp_max_seat'];
    $max_seat_fn = $shop['max_seat'];
    $max_seat_hp = $shop['hp_max_seat'];
    
    /*
    $data = Api::call(Configure::read('API.url_orders_date_calendar'), $param, false, array());
    $this->Common->handleException(Api::getError());
   
    $deviceRecord = MasterData::devices_all($this->AppUI->shop_id);
    $shop_open_time = !empty($this->AppUI->shop_open_time) ? $this->AppUI->shop_open_time : Configure::read('Config.DefaultShopOpenTime');
    $shop_close_time = !empty($this->AppUI->shop_close_time) ? $this->AppUI->shop_close_time : Configure::read('Config.DefaultShopCloseTime');
    
    list($count, $order_timely_limit) = Api::Call(
        Configure::read('API.url_order_timely_limit_list'),
        array(
            'shop_id' => $this->AppUI->shop_id,
            'date' => $dt
        )
    );    
    // Get max_seat 
    $max_seat = 0;
    $max_seat_fn=0;
    $max_seat_hp=0;
    if (!isset($this->AppUI->total_max_seat)) {
        $shop = Api::call(Configure::read('API.url_shops_detail'), array('id' => $this->AppUI->shop_id));
        if (empty(Api::getError()) && !empty($shop)) {
            $max_seat = $shop['max_seat'] + $shop['hp_max_seat'];
            $max_seat_fn = $shop['max_seat'];
            $max_seat_hp = $shop['hp_max_seat'];
        }    
    } else {
        $max_seat = $this->AppUI->total_max_seat;
        $max_seat_fn = $this->AppUI->shop_max_seat;
        $max_seat_hp = $this->AppUI->shop_hp_max_seat;
    }
  
    $max_real_seat = 0;
    $count = Api::call(Configure::read('API.url_orders_getmaxseatrealtime'),  $param);
    if (empty(Api::getError()) && !empty($count)) {
        $max_real_seat = $count;
    }else{
        $max_real_seat = $this->AppUI->shop_max_seat + $this->AppUI->shop_hp_max_seat;
    }
    */
    
    $deviceRecord = MasterData::devices_all($this->AppUI->shop_id);
    $this->set(compact(
        'data', 
        'dt', 
        'deviceRecord', 
        'max_seat',
        'max_real_seat', 
        'max_seat_fn', 
        'max_seat_hp', 
        'order_timely_limit'
    ));
    $this->set('shop_open_time', getdate(strtotime($dt) + $shop['open_time']));
    $this->set('shop_close_time', getdate(strtotime($dt) + $shop['close_time']));
} else {
    $this->layout = 'template';
    $this->viewPath = 'Pages';
    $this->view = 'dashboard';
}