<?php

$modelName = $this->Order->name;

$param = $this->getParams(array('page' => 1, 'limit' => 10000));
list($total, $data) = Api::call(Configure::read('API.url_orders_list'), $param, false, array(0, array()));
$this->PhpExcel->createWorksheet()
        ->setDefaultFont('Calibri', 12);
$table = array(
    array('label' => __('ID')),
    array('label' => __('Sales Code')),
    array('label' => __('Reservation date')),
    array('label' => __('Username')),
    array('label' => __('Shop'), 'width' => 50, 'wrap' => true),
    array('label' => __('Nailist')),
    array('label' => __('Nail length')),
    array('label' => __('Address')),
    array('label' => __('Email')),
    array('label' => __('Phone')),
    array('label' => __('Created'))
);
// add heading with different font and bold text
$this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));

// add data
foreach ($data as $value) {
    $nailist_name = '';
    if (isset($param['nailist_customid']) && !empty($param['nailist_customid'])) {
        $nailist_name = $value['nailist_name'];
    }
    $reservation_date = $this->Common->dateFormat($value['reservation_date']) != false ? $this->Common->dateFormat($value['reservation_date']) : '';
    $created = $this->Common->dateFormat($value['created']) != false ? $this->Common->dateFormat($value['created']) : '';
    $this->PhpExcel->addTableRow(array(
        $value['id'],
        $value['sales_code'],
        $reservation_date,
        $value['user_name'],
        $value['shop_name'],
        $nailist_name,
        $value['nail_length'],
        $value['address1'],
        $value['email'],
        $value['phone'],
        $created
    ));
}

// close table and output
$fileName = 'orders-' . date('YmdHis') . '.xlsx';
$this->PhpExcel->addTableFooter()->output($fileName);
