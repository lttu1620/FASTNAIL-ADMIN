<?php

$modelName = $this->Order->name;
$model = $this->{$modelName};

// Create breadcrumb
$pageTitle = __('Update order');
$param['id'] = $order_id;
$data[$modelName] = Api::Call(Configure::read('API.url_orders_detail'), $param);
$this->Common->handleException(Api::getError());

// Convert date to format(yyyy-MM-dd HH:mm:ss)
if (!empty($data[$modelName]['reservation_date'])) {
    $data[$modelName]['reservation_date'] = date("Y-m-d H:i:s", $data[$modelName]['reservation_date']);
}
if (!empty($data[$modelName]['start_date'])) {
    $data[$modelName]['start_date'] = date("Y-m-d H:i", $data[$modelName]['start_date']);
}
if (!empty($data[$modelName]['order_start_date'])) {
    $data[$modelName]['order_start_date'] = date("Y-m-d H:i:s", $data[$modelName]['order_start_date']);
}
if (!empty($data[$modelName]['order_end_date'])) {
    $data[$modelName]['order_end_date'] = date("Y-m-d H:i:s", $data[$modelName]['order_end_date']);
}
if (!empty($data[$modelName]['off_start_date'])) {
    $data[$modelName]['off_start_date'] = date("Y-m-d H:i:s", $data[$modelName]['off_start_date']);
}
if (!empty($data[$modelName]['off_end_date'])) {
    $data[$modelName]['off_end_date'] = date("Y-m-d H:i:s", $data[$modelName]['off_end_date']);
}
if (!empty($data[$modelName]['sr_start_date'])) {
    $data[$modelName]['sr_start_date'] = date("Y-m-d H:i:s", $data[$modelName]['sr_start_date']);
}
if (!empty($data[$modelName]['sr_end_date'])) {
    $data[$modelName]['sr_end_date'] = date("Y-m-d H:i:s", $data[$modelName]['sr_end_date']);
}

//get list shop
$listShop = $this->Common->arrayKeyValue(MasterData::shops_all(), 'id', 'name');

//get list nailist
$listNailist = Api::Call(Configure::read('API.url_nailists_all'), array('shop_id' => $data[$modelName]['shop_id']));

//get list device
$listDevice = Api::Call(Configure::read('API.url_devices_all'), array('shop_id' => $data[$modelName]['shop_id']));

//get list prefecture
$listPrefecture = $this->Common->arrayKeyValue(MasterData::prefectures_all(), 'id', 'name');

//get list order nailist
$listOrderNailist = Api::Call(Configure::read('API.url_orderlogs_all'), array('order_id' => $order_id, 'disable' => 0), false, array());
$selectedNailistArr = $this->Common->arrayValues($listOrderNailist, 'nailist_id');

//get list order device
$listOrderDevice = Api::Call(Configure::read('API.url_order_devices_all'), array('order_id' => $order_id), false, array());
$selectedDeviceArr = $this->Common->arrayValues($listOrderDevice, 'device_id');

$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/orders',
        'name' => __('Order list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));
$this->setPageTitle($pageTitle);
// Create Update form order
$this->UpdateForm->reset()
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id'       => 'id',
        'label'    => __('ID'),
        'readonly' => true
    ))
    ->addElement(array(
        'id'    => 'total_price',
        'type'  => 'text',
        'label' => __('Total price'),
        'readonly' => true,
    ))
    ->addElement(array(
        'id'    => 'total_tax_price',
        'type'  => 'text',
        'label' => __('Total tax price'),
        'readonly' => true,
    ))
    ->addElement(array(
        'id'       => 'service',
        'label'    => __('Service'),
        'options'  => Configure::read('Config.OrderService'),
        'empty'    => Configure::read('Config.StrChooseOne'),
    ))
     ->addElement(array(
        'id'    => 'request',
        'type'  => 'text',
        'label' => __('Request'),
    ))
    ->addElement(array(
        'id'       => 'pay_type',
        'label'    => __('Pay type'),
        'options'  => Configure::read('Config.PayType'),
        'empty'    => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'       => 'is_paid',
        'label'    => __('Is paid'),
        'options'  => Configure::read('Config.booleanValue'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'start_date',
        'type'     => 'text',
        'readonly' => true,
        'label'    => __('Start date')
    ))
    ->addElement(array(
        'id'       => 'reservation_type',
        'label'    => __('Reservation type'),
        'options'  => Configure::read('Config.ReservationType'),
        'empty'    => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'       => 'reservation_date',
        'type'     => 'text',
        'label'    => false,
        'required' => true,
        'class'    => 'form-control datetime_picker',
        'datetime' => true,
        'before'   => '<label for="reservation_date">' . __('Reservation date') . '</label> <div  class="input-group date reservation_date"> ',
        'after'    => '<span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span></div>'
    ))
    ->addElement(array(
        'id'       => 'order_start_date',
        'type'     => 'text',
        'label'    => false,
        'datetime' => true,
        'class'    => 'form-control datetime_picker',
        'before'   => '<label for="reservation_date">' . __('Order start date') . '</label> <div  class="input-group date order_start_date"> ',
        'after'    => '<span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span></div>'
    ))
    ->addElement(array(
        'id'       => 'order_end_date',
        'type'     => 'text',
        'class'    => 'form-control datetime_picker',
        'label'    => false,
        'datetime' => true,
        'class'    => 'form-control datetime_picker',
        'before'   => '<label for="reservation_date">' . __('Order end date') . '</label> <div  class="input-group date order_end_date"> ',
        'after'    => '<span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span></div>'
    ))
    ->addElement(array(
        'id'       => 'off_start_date',
        'type'     => 'text',
        'label'    => false,
        'datetime' => true,
        'class'    => 'form-control datetime_picker',
        'before'   => '<label for="reservation_date">' . __('Off start date') . '</label> <div  class="input-group date off_start_date"> ',
        'after'    => '<span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span></div>'
    ))
    ->addElement(array(
        'id'       => 'off_end_date',
        'type'     => 'text',
        'label'    => false,
        'datetime' => true,
        'class'    => 'form-control datetime_picker',
        'before'   => '<label for="reservation_date">' . __('Off end date') . '</label> <div  class="input-group date off_end_date"> ',
        'after'    => '<span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span></div>'
    ))
    ->addElement(array(
        'id'       => 'sr_start_date',
        'type'     => 'text',
        'label'    => false,
        'datetime' => true,
        'class'    => 'form-control datetime_picker',
        'before'   => '<label for="reservation_date">' . __('Service start date') . '</label> <div  class="input-group date sr_start_date"> ',
        'after'    => '<span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span></div>'
    ))
    ->addElement(array(
        'id'       => 'sr_end_date',
        'type'     => 'text',
        'label'    => false,
        'datetime' => true,
        'class'    => 'form-control datetime_picker',
        'before'   => '<label for="reservation_date">' . __('Service end date') . '</label> <div  class="input-group date sr_end_date"> ',
        'after'    => '<span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span></div>'
    ))
    ->addElement(array(
        'id'      => 'off_nails',
        'label'   => __('Off nails'),
        'options' => Configure::read('Config.NailOff')
    ))
    ->addElement(array(
        'id'       => 'visit_section',
        'label'    => __('Visit section'),
        'options'  => Configure::read('Config.VisitSection'),
        'empty'    => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'       => 'visit_element',
        'label'    => __('Visit element'),
        'options'  => Configure::read('Config.VisitElement'),
        'empty'    => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'       => 'hf_section',
        'label'    => __('Hf section'),
        'options'  => Configure::read('Config.hfSection'),
        'empty'    => Configure::read('Config.StrChooseOne'),
        'readonly' => true,
    ))
    ->addElement(array(
        'id'      => 'nail_length',
        'label'   => __('Nail length'),
        'options' => Configure::read('Config.NailLength'),
        'empty'   => Configure::read('Config.StrChooseOne'),        
    ))
    ->addElement(array(
        'id'      => 'nail_type',
        'label'   => __('Nail type'),
        'options' => Configure::read('Config.NailType'),
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'      => 'nail_add_length',
        'label'   => __('Nail add length'),
        'options' => Configure::read('Config.FingerItemLength'),
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'    => 'problem',
        'label' => __('Problem'),
        'type'  => 'texarea',
        'rows'  => 5
    ))
    ->addElement(array(
        'id'    => 'rating_1',
        'type'  => 'text',
        'label' => __('Rating 1'),
    ))
    ->addElement(array(
        'id'    => 'rating_2',
        'type'  => 'text',
        'label' => __('Rating 2'),
    ))
    ->addElement(array(
        'id'    => 'rating_3',
        'type'  => 'text',
        'label' => __('Rating 3'),
    ))
    ->addElement(array(
        'id'    => 'rating_4',
        'type'  => 'text',
        'label' => __('Rating 4'),
    ))
    ->addElement(array(
        'id'    => 'rating_5',
        'type'  => 'text',
        'label' => __('Rating 5'),
    ))
    ->addElement(array(
        'id'       => 'is_mail',
        'label'    => __('Is mail'),
        'options'  => Configure::read('Config.booleanValue'),
        'required' => true
    ))
    ->addElement(array(
        'id'      => 'is_designate',
        'label'   => __('Is designate'),
        'options' => Configure::read('Config.booleanValue')
    ))
    ->addElement(array(
        'id'      => 'is_next_reservation',
        'label'   => __('Is next reservation'),
        'options' => Configure::read('Config.booleanValue')
    ))
    ->addElement(array(
        'id'      => 'is_next_designate',
        'label'   => __('Is next designate'),
        'options' => Configure::read('Config.booleanValue')
    ))
    ->addElement(array(
        'id'    => 'welfare_id',
        'type'  => 'text',
        'label' => __('Welfare ID'),
    ))
    ->addElement(array(
        'id'    => 'seat_id',
        'type'  => 'text',
        'label' => __('Seat'),
        'readonly' => true
    ))
    ->addElement(array(
        'id'       => 'user_id',
        'label'    => __('User ID'),
        'type'     => 'text',
        'readonly' => true
    ))
    ->addElement(array(
        'id'       => 'user_code',
        'label'    => __('User code'),       
        'readonly' => true,
    ))
    ->addElement(array(
        'id'    => 'user_name',
        'label' => __('User name')
    ))
    ->addElement(array(
        'id'    => 'kana',
        'label' => __('Kana')
    ))
    ->addElement(array(
        'id'       => 'sex',
        'label'    => __('Gender'),
        'options'  => Configure::read('Config.searchGender'),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'       => 'birthday',
        'type'     => 'text',
        'calendar' => true,
        'label'    => __('Birthday'),
        'class'    => 'form-control date_picker'
    ))
    ->addElement(array(
        'id'       => 'email',
        'type'     => 'text',
        'label'    => __('Email'),        
    ))   
    ->addElement(array(
        'id' => 'phone',
        'label' => __('Phone'),
        'phone' => true,        
        'data-format' => '2'        
    ))
    ->addElement(array(
        'id'      => 'prefecture_id',
        'label'   => __('Prefecture'),
        'options' => $listPrefecture,
        'empty'   => __('---'),
    ))
    ->addElement(array(
        'id'    => 'address1',
        'type'  => 'text',
        'label' => __('Address 1'),
    ))
    ->addElement(array(
        'id'    => 'address2',
        'type'  => 'text',
        'label' => __('Address 2'),
    ))    
    ->addElement(array(
        'id'       => '',
        'label'    => __('Shops'),
        'options'  => $listShop,
        'value'    => !empty($data[$modelName]['shop_id']) ? $data[$modelName]['shop_id'] : 0, 
        'empty'    => __('All'),      
        'disabled' => 'disabled',
    ))
    ->addElement(array(
        'id'    => 'shop_id',
        'type'  => 'hidden',
    ))    
    ->addElement(array(
        'id'       => 'nailist_id',
        'type'     => 'select',
        'multiple' => 'checkbox',
        'label'    => __('Nailists'),
        'options'  => $this->Common->arrayKeyValue($listNailist, 'id', 'name'),
        'selected' => $selectedNailistArr,
        'between'  => "<div class='group-checkbox' id='nailist_id'>",
        'after'    => "</div><div class=\"cls\"></div>",
    ))
    ->addElement(array(
        'id'       => 'device_id',
        'type'     => 'selectbox',
        'multiple' => 'checkbox',
        'label'    => __('Device'),
        'options'  => $this->Common->arrayKeyValue($listDevice, 'id', 'name'),
        'selected' => $selectedDeviceArr,
        'between'  => "<div class='group-checkbox' id='device_id'>",
        'after'    => "</div><div class=\"cls\"></div>",
    ))   
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type'    => 'submit',
        'value'   => __('Cancel'),
        'class'   => 'btn pull-left',
        'onclick' => 'return back();'
    ));

//Get nail's orders list
$orderNailParam['order_id'] = $order_id;
$orderNails = Api::call(Configure::read('API.url_ordernails_all'), $orderNailParam, false, array(0, array()));
$this->Common->handleException(Api::getError());

$nailTable = $this->SimpleTable
    ->setModelName('Ordernail')    
    ->addColumn(array(
        'id'    => 'nail_id',
        'title' => __('ID'),
        'width' => '60'
    ))
    ->addColumn(array(
        'id'    => 'image_url',
        'type'  => 'image',
        'title' => __('Image'),
        'src'   => '{nail_image_url}',
        'width' => '60'
    ))
    ->addColumn(array(
        'id'    => 'nail_photo_cd',
        'title' => __('Photo Cd'),     
        'empty' => '',       
    ))
    ->addColumn(array(
        'id'    => 'nail_price',
        'title' => __('Price'),
        'type'  => 'number',
        'width' => '120',
        'empty' => ''
    ))    
    ->setDataset($orderNails);

$this->set('nailTable', $nailTable->get());

//Get item's orders list
$orderItemParam['order_id'] = $order_id;
$orderItems = Api::call(Configure::read('API.url_orderitems_all'), $orderItemParam, false, array(0, array()));
$this->Common->handleException(Api::getError());

$itemTable = $this->SimpleTable
    ->reset()
    ->setModelName('Orderitem')
    ->addColumn(array(
        'id'    => 'id',
        'type'  => 'link',
        'title' => __('ID'),
        'href'  => '/' . $this->controller . '/update/{id}',
        'width' => '30'
    ))
    ->addColumn(array(
        'id'    => 'item_id',
        'title' => __('Item ID'),
        'width' => '100'
    ))
    ->addColumn(array(
        'id'    => 'item_name',
        'title' => __('Item Name'),        
    ))
    ->addColumn(array(
        'id'    => 'item_price',
        'title' => __('Item Price'),
        'width' => '100',
        'empty' => ''
    ))   
    ->setDataset($orderItems);

$this->set('itemTable', $itemTable->get());

// Process when submit form
if ($this->request->is('post')) {    
    $data = $this->getData($modelName);     
    if ($model->validateOrderInsertUpdate($data)) {
        if (!empty($data[$modelName]['nailist_id'])) {
            $data[$modelName]['nailist_id'] = implode(',', $data[$modelName]['nailist_id']);
        }
        if (!empty($data[$modelName]['device_id'])) {
            $data[$modelName]['devices_id'] = implode(',', $data[$modelName]['device_id']);
        }
        $id = Api::call(Configure::read('API.url_orders_update'), $data[$modelName]);        
        if (!empty($id) && !Api::getError()) { 
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$order_id}");
        } else {
            $error = array(
                'devices_id' => array(1011 => '既にある予約と、時間がかぶっています'),
                'nailist_id' => array(1011 => '既にある予約と、時間がかぶっています')
            );
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        AppLog::info("Can not update", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
