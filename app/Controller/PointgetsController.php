<?php

App::uses('AppController', 'Controller');

/**
 * PointGetsController class of Point Gets Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PointgetsController extends AppController
{
    /**
     * Initializes components for PointGetsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index PointGets.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Pointgets/index.php');
    }

    /**
     * Handles user interaction of view update PointGets.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of PointGets. Default value is 0.
     * @return void
     */
    public function update($id = 0)
    {
        include('Pointgets/update.php');
    }
}
