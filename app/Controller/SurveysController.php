<?php

App::uses('AppController', 'Controller');

/**
 * SurveysController class of Surveys Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class SurveysController extends AppController
{
    /**
     * Initializes components for SurveysController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Surveys.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Surveys/index.php');
    }

    /**
     * Handles user interaction of view update Surveys.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Surveys. Default value is 0.
     * @return void
     */
    public function update($id = 0)
    {
        include('Surveys/update.php');
    }
}
