<?php

App::uses('AppController', 'Controller');

/**
 * System Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class SystemController extends AppController
{
    /**
     * Construct
     * 
     * @author tuancd 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * deletecache action
     * 
     * @author tuancd
     * @return void 
     */
    public function deletecache() {
        if (defined('CACHE_PATH')) {
            $files = array();
            $files = array_merge($files, glob(CACHE_PATH . '*')); // remove cached data
            $files = array_merge($files, glob(CACHE_PATH . 'css' . DS . '*')); // remove cached css
            $files = array_merge($files, glob(CACHE_PATH . 'js' . DS . '*'));  // remove cached js           
            $files = array_merge($files, glob(CACHE_PATH . 'models' . DS . '*'));  // remove cached models           
            $files = array_merge($files, glob(CACHE_PATH . 'persistent' . DS . '*'));  // remove cached persistent           
            foreach ($files as $f) {
                if (is_file($f)) {
                    @unlink($f);
                }
            }         
        }         
        Api::call(Configure::read('API.url_system_deletecache'));
        @file_get_contents(Configure::read('FE.Host'). "system/deletecache");        
        $this->Common->setFlashSuccessMessage(__('Cacche deleted successfully'));
        return $this->redirect($this->request->referer());
    }

     /**
     * ps action
     * 
     * @author tuancd
     * @return void 
     */
    public function ps()
    {
        include('Systems/ps.php');
    }
}
