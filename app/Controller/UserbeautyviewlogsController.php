<?php

App::uses('AppController', 'Controller');

/**
 * UserbeautyviewlogsController class of User Beauty View Log Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserbeautyviewlogsController extends AppController
{
    /**
     * Initializes components for UserbeautyviewlogsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Userbeautyviewlogs.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Userbeautyviewlogs/index.php');
    }
    
    /**
     * Handles user interaction of view report Userbeautyviewlogs.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function report()
    {
        include('Userbeautyviewlogs/report.php');
    }
}
