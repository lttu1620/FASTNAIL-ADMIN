<?php

$modelName = $this->Shop->name;
$model = $this->{$modelName};

$listShopGroups = $this->Common->arrayKeyValue(MasterData::shopgroups_all(), 'id', 'name');
$listAreas = $this->Common->arrayKeyValue(MasterData::areas_all(), 'id', 'name');
$listAdmin = $this->Common->arrayKeyValue(MasterData::admins_all(0, 0), 'id', 'name');
// Create breadcrumb 
$data[$modelName] = array();
if (isset($shop_id) && $shop_id != 0) {
    $pageTitle = __('Update shop');
    $param['id'] = $shop_id;
    $data[$modelName] = Api::Call(Configure::read('API.url_shops_detail'), $param);
    $this->Common->handleException(Api::getError());
} else {
    $pageTitle = __('Add shop');
}
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/shops',
        'name' => __('Shop list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));
$this->setPageTitle($pageTitle);
// Create Update form shop
$this->UpdateForm->reset()
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id'    => 'id',
        'type'  => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id'       => 'name',
        'label'    => __('Name'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'area_id',
        'label'    => __('Area'),
        'options'  => $listAreas,
        'empty'    => Configure::read('Config.StrChooseOne'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'shop_group_id',
        'label'    => __('Shop group'),
        'options'  => $listShopGroups,
        'empty'    => Configure::read('Config.StrChooseOne'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'region',
        'label'    => __('Region'),
        'options'  => Configure::read('Config.ShopRegion'),
        'empty'    => Configure::read('Config.StrChooseOne'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'admin_id',
        'label'    => __('Login account'),
        'options'  => $listAdmin,
        'empty'    => Configure::read('Config.StrChooseOne'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'priority',
        'label'    => __('Priority'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'reservation_interval',
        'label'    => __('Reservation interval'),
        'options'  => Configure::read('Config.ReservationInterval'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'is_plus',
        'label'    => __('Is plus'),
        'options'  => Configure::read('Config.BooleanValue'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'is_designate',
        'label'    => __('Is Designate'),
        'options'  => Configure::read('Config.BooleanValue'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'is_franchise',
        'label'    => __('Is Franchise'),
        'options'  => Configure::read('Config.BooleanValue'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'open_time',
        'label'    => __('Open time'),
        'options'  => $this->Common->getTime(),
        'empty'    => '',
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'close_time',
        'label'    => __('Close time'),
        'options'  => $this->Common->getTime(),
        'empty'    => '',
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'phone',
        'label'    => __('Phone'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'address',
        'label'    => __('Address'),
        'required' => true
    ))    
    ->addElement(array(
        'id'       => 'max_seat',
        'label'    => __('Max seat'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'hp_max_seat',
        'label'    => __('HP max seat'),
        'required' => true
    ))
    ->addElement(
        array(
            'id'         => 'image_url',
            'type'       => 'file',
            'image'      => true,
            'label'      => __('Image'),
            'allowEmpty' => true,
        )
    )
    ->addElement(array(
        'id'       => 'map_url',
        'label'    => '地図のURL'
    ))
    ->addElement(array(
        'id'       => 'zipcode',
        'label'    => '郵便番号',        
    ))
    ->addElement(array(
        'id'       => 'workingtime',
        'label'    => '営業時間',
        'type' => 'textarea',
        'rows' => '3',       
    ))
    ->addElement(array(
        'id'       => 'offday',
        'label'    => '定休日',       
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type'    => 'submit',
        'value'   => __('Cancel'),
        'class'   => 'btn pull-left',
        'onclick' => 'return back();'
    ));

// Process when submit form
if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        $data = $this->getData($modelName);
        if (isset($data[$modelName]['admin_id'])) {
            $data[$modelName]['admin_shop_id'] = $data[$modelName]['admin_id'];
            unset($data[$modelName]['admin_id']);
        }
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $data[$modelName]['image_url'] = $this->Image->uploadImage("{$modelName}.image_url");
        } elseif (isset($data[$modelName]['image_url'])) {
            unset($data[$modelName]['image_url']);
        }
        $id = Api::call(Configure::read('API.url_shops_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            AppCache::delete(Configure::read('shops_all')->key);
            if (!empty($shop_id) && $this->AppUI->shop_id ) {
                $this->AppUI->shop_group_id = $data[$modelName]['shop_group_id'];
                $this->AppUI->shop_area_id = $data[$modelName]['area_id'];              
                $this->AppUI->shop_reservation_interval = $data[$modelName]['reservation_interval'];
                $this->AppUI->shop_id = $data[$modelName]['id'];
                $this->AppUI->shop_name = $data[$modelName]['name'];
                $this->AppUI->shop_phone = $data[$modelName]['phone'];
                $this->AppUI->shop_address = $data[$modelName]['address'];
                $this->AppUI->shop_is_designate = $data[$modelName]['is_designate'];
                $this->AppUI->shop_open_time = $data[$modelName]['open_time'];
                $this->AppUI->shop_close_time = $data[$modelName]['close_time'];
                $this->AppUI->shop_is_plus = $data[$modelName]['is_plus'];               
                $this->AppUI->shop_max_seat = $data[$modelName]['max_seat'];
                $this->AppUI->shop_hp_max_seat = $data[$modelName]['hp_max_seat'];
                $this->AppUI->shop_zipcode = $data[$modelName]['zipcode'];
                $this->AppUI->shop_workingtime = $data[$modelName]['workingtime'];
                $this->AppUI->shop_offday = $data[$modelName]['offday'];
                $this->AppUI->total_max_seat = $data[$modelName]['max_seat'] + $data[$modelName]['hp_max_seat'];
                $this->createLoginSession((array)$this->AppUI);
            }
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            AppCache::delete(Configure::read('shops_all')->key);
            return $this->redirect("/{$this->controller}/update/{$id}");
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error    
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
