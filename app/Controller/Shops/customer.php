<?php

$this->layout = 'search';
$modelName = $this->Shop->name;
$pageTitle = '顧客管理';
$this->setPageTitle($pageTitle);
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'   => 'keyword',
            'type' => 'text'
        )
    )
    ->addElement(
        array(
            'id'       => 'date_from',
            'type'     => 'text',
            'calendar' => true,
            'label'    => __('Date From'),
        )
    )
    ->addElement(
        array(
            'id'       => 'date_to',
            'type'     => 'text',
            'calendar' => true,
            'label'    => __('Date To'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        )
    );

$param = $this->getParams(
    array(
        'page'    => 1,
        'limit'   => Configure::read('Config.pageSize'),
        'shop_id' => !empty($this->AppUI->shop_id) ? $this->AppUI->shop_id : 0,
    )
);
$param['member'] = $this->getParam('member', '1');
list($total, $data) = Api::call(Configure::read('API.url_shops_customer'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());

$this->set('total', $total);
$this->set('limit', $param['limit']);

if (!empty($param['member'])) {
    $this->SimpleTable
        ->addColumn(
            array(
                'id'    => 'id',
                'title' => __('ID'),
                'width' => '30',
                'empty' => ''
            )
        );
}
$this->SimpleTable
    ->addColumn(
        array(
            'id'    => 'user_name',
            'title' => __('Username'),
            'width' => '200',
            'empty' => ''
        )
    )
    ->addColumn(
        array(
            'id'    => 'kana',
            'title' => __('Kana'),
            'width' => '200',
            'empty' => ''
        )
    )
    ->addColumn(
        array(
            'id'    => 'sex',
            'title' => __('Gender'),
            'width' => '50',
            'rules' => array(
                '1' => __('Male'),
                '2' => __('Female')
            ),
            'empty' => ''
        )
    )
    ->addColumn(
        array(
            'id'    => 'email',
            'title' => __('Email'),
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'phone',
            'title' => __('Phone'),
            'width' => '120',
            'empty' => ''
        )
    )
    ->addColumn(
        array(
            'id'    => 'as_claim_id',
            'title' => __('Is claim'),
            'width' => '120',
            'empty' => ''
        )
    )
    ->addColumn(
        array(
            'id'    => 'shop_name',
            'title' => __('Shop name'),
            'width' => '150',
            'empty' => ''
        )
    )
    ->addColumn(
        array(
            'id'    => 'nailists',
            'title' => __('前回サービス担当者'),
            'empty' => '',
            'width' => '150',
        )
    )
    ->addColumn(
        array(
            'id'    => 'reservation_date',
            'title' => __('Reservation date'),
            'type'  => 'date',
            'empty' => '',
            'width' => '150',
        )
    )
    ->setDataset($data)
    ->addColumn(
        array(
            'type'    => 'link',
            'title'   => __('medical chart'),
            'href'    => '#',
            'onClick' => 'openMediacal({order_id});return false;',
            'target'  => '_blank',
            'button'  => true,
            'width'   => '60'
        )
    );

$this->set(compact('listShop', 'param'));
