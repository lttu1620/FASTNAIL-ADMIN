<?php
$modelName = $this->Shop->name;

//Process disable/enable
$this->doGeneralAction($modelName);

$listShopGroups = $this->Common->arrayKeyValue(MasterData::shopgroups_all(), 'id', 'name');
$listAreas = $this->Common->arrayKeyValue(MasterData::areas_all(), 'id', 'name');

// create breadcrumb
$pageTitle = __('Shop list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(
        array(
            'id'      => 'area_id',
            'label'   => __('Area'),
            'options' => $listAreas,
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'shop_group_id',
            'label'   => __('Shop group'),
            'options' => $listShopGroups,
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'region',
            'label'   => __('Region'),
            'options' => Configure::read('Config.ShopRegion'),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'    => 'name',
            'label' => __('Name'),
        )
    )
    ->addElement(
        array(
            'id'    => 'address',
            'label' => __('Address'),
        )
    )
    ->addElement(
        array(
            'id'      => 'open_time',
            'label'   => __('Open time'),
            'options' => $this->Common->getTime(),
            'empty'   => '',
        )
    )
    ->addElement(
        array(
            'id'      => 'close_time',
            'label'   => __('Close time'),
            'options' => $this->Common->getTime(),
            'empty'   => '',
        )
    )
    ->addElement(
        array(
            'id'      => 'disable',
            'label'   => __('Status'),
            'options' => Configure::read('Config.searchStatus'),
            'empty'   => __('All'),
        )
    )
    ->addElement(
        array(
            'id'      => 'is_plus',
            'label'   => __('Is plus'),
            'options' => Configure::read('Config.searchStatus'),
            'empty'   => __('All'),
        )
    )
    ->addElement(
        array(
            'id'    => 'max_seat',
            'label' => __('Max seat'),
            'empty' => '',
        )
    )
    ->addElement(
        array(
            'id'    => 'hp_max_seat',
            'label' => __('HP max seat'),
            'empty' => '',
        )
    )
    /*
    ->addElement(array(
        'id'      => 'zipcode',
        'label'   => __('Zipcode'),
        'empty'   => ''
    ))
    ->addElement(array(
        'id'      => 'workingtime',
        'label'   => __('Working time'),
        'empty'   => ''
    ))
    ->addElement(array(
        'id'      => 'offday',
        'label'   => __('Offday'),
        'empty'   => ''
    ))*/
    ->addElement(
        array(
            'id'      => 'sort',
            'label'   => __('Sort'),
            'options' => array(
                'id-asc'       => __('ID Asc'),
                'id-desc'      => __('ID Desc'),
                'name-asc'     => __(' name Asc'),
                'name-desc'    => __(' name Desc'),
                'address-asc'  => __('Address  Asc'),
                'address-desc' => __('Address Desc'),
            ),
            'empty'   => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'      => 'limit',
            'label'   => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        )
    );

$param = $this->getParams(
    array(
        'page'  => 1,
        'limit' => Configure::read('Config.pageSize'),
        'sort'  => 'id-asc',
    )
);
list($total, $data) = Api::call(Configure::read('API.url_shops_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(
        array(
            'id'    => 'item',
            'name'  => 'items[]',
            'type'  => 'checkbox',
            'value' => '{id}',
            'width' => '20',
        )
    )
    ->addColumn(
        array(
            'id'    => 'id',
            'type'  => 'link',
            'title' => __('ID'),
            'href'  => '/'.$this->controller.'/update/{id}',
            'width' => '30',
        )
    )
    ->addColumn(
        array(
            'id'    => 'image_url',
            'type'  => 'image',
            'title' => __('Image'),
            'src'   => '{image_url}',
            'width' => 60,
        )
    )
    ->addColumn(
        array(
            'id'    => 'name',
            'type'  => 'link',
            'href'  => '/'.$this->controller.'/update/{id}',
            'title' => __('Name'),
            'empty' => '',
            'width' => 200,
        )
    )
    ->addColumn(
        array(
            'id'     => 'open_time',
            'title'  => __('Open time'),
            'rules'  => $this->Common->getTime(),
            'empty'  => '0',
            'hidden' => true,
        )
    )
    ->addColumn(
        array(
            'id'     => 'close_time',
            'title'  => __('Close time'),
            'rules'  => $this->Common->getTime(),
            'empty'  => '0',
            'hidden' => true,
        )
    )
    ->addColumn(
        array(
            'id'    => 'address',
            'title' => __('Address').'/'.__('Phone'),
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'     => 'phone',
            'title'  => __('Phone'),
            'width'  => 150,
            'empty'  => '',
            'hidden' => true,
        )
    )
    ->addColumn(
        array(
            'id'    => 'reservation_interval',
            'title' => __('Reservation interval'),
            'width' => 80,
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'admin_name',
            'title' => __('Admin name'),
            'width' => 120,
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'area_name',
            'title' => __('Area'),
            'width' => 120,
            'empty' => '',
        )
    )
    ->addColumn(
        array(
            'id'    => 'shop_group_name',
            'title' => __('Shop group name'),
            'width' => 120,
            'empty' => '',
        )
    )
    /*
    ->addColumn(array(
        'id'    => 'created',
        'type'  => 'date',
        'title' => __('Created'),
        'width' => 120,
        'empty' => ''
    ))*/
    ->addColumn(
        array(
            'id'     => 'max_seat',
            'title'  => __('Max seat'),
            'before' => "FN: ",
            'width'  => 110,
            'empty'  => '',
        )
    )
    ->addColumn(
        array(
            'id'     => 'hp_max_seat',
            'title'  => __('HP max seat'),
            'width'  => 110,
            'empty'  => '',
            'hidden' => true,
        )
    )
//    ->addColumn(array(
//        'id'    => 'zipcode',
//        'title' => __('Zipcode'),
//        'width' => 110,
//        'empty' => '',
//    ))
//    ->addColumn(array(
//        'id'    => 'workingtime',
//        'title' => __('Working time'),
//        'width' => 110,
//        'empty' => '',
//    ))
//    ->addColumn(array(
//        'id'    => 'offday',
//        'title' => __('Offday'),
//        'width' => 110,
//        'empty' => '',
//    ))
    ->addColumn(
        array(
            'id'     => 'is_plus',
            'type'   => 'checkbox',
            'title'  => __('Is plus'),
            'toggle' => true,
            'class'  => 'plus',
            'rules'  => array(
                '0' => '',
                '1' => 'checked',
            ),
            'empty'  => '0',
            'width'  => 90,
        )
    )
    ->addColumn(
        array(
            'id'     => 'disable',
            'type'   => 'checkbox',
            'title'  => __('Status'),
            'toggle' => true,
            'rules'  => array(
                '0' => 'checked',
                '1' => '',
            ),
            'empty'  => 0,
            'width'  => 90,
        )
    )
    ->setDataset($data)
    ->setMergeColumn(
        array(
            'name'     => array(
                array(
                    'field'  => 'open_time',
                    'before' => __('Open time').": ",
                    'after'  => '',
                ),
                array(
                    'field'  => 'close_time',
                    'before' => __('Close time').": ",
                    'after'  => '',
                ),
            ),
            'max_seat' => array(
                array(
                    'field'  => 'hp_max_seat',
                    'before' => "HP: ",
                    'after'  => '',
                ),
            ),
            'address'  => array(
                array(
                    'field'  => 'phone',
                    'before' => __('Phone').':',
                    'after'  => '',
                ),
            ),
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        )
    )
    ->addButton(
        array(
            'type'  => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        )
    );
