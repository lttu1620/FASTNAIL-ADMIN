<?php

App::uses('AppController', 'Controller');

/**
 * Surveylogs Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class SurveylogsController extends AppController {

    /**
     * Construct
     * 
     * @author VuLTH 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author VuLTH 
     * @return void
     */
    public function index() {
        
        include ('Surveylogs/index.php');
    }
}
