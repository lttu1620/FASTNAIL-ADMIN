<?php

$modelName = $this->Nailist->name;

//Process disable/enable
$this->doGeneralAction($modelName);

//Get list shop
$listShop = MasterData::shops_all();
$listShop = $this->Common->arrayKeyValue($listShop, 'id', 'name');

// Create breadcrumb
$pageTitle = __('Nailist list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'id',
        'label' => __('ID'),
    ))
    ->addElement(array(
        'id' => 'code',
        'label' => __('Code')
    ))
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name')
    ))
    ->addElement(array(
        'id' => 'shop_id',
        'label' => __('Shop'),
        'options' => $listShop,
        'empty' => __('All'),
    ))
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty' => __('All')
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'id-asc' => __('ID Asc'),
            'id-desc' => __('ID Desc'),
            'code-asc' => __('Code Asc'),
            'code-desc' => __('Code Desc'),
            'name-asc' =>__('Name Asc'),
            'name-desc' =>__('Name Desc'),
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_nailists_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id' => 'item',
        'name' => 'items[]',
        'type' => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
     ->addColumn(array(
        'id'    => 'nailist_image_url',
        'type'  => 'image',
        'title' => __('Image'),
        'src'   => '{nailist_image_url}',
        'width' => '60'
    ))
    ->addColumn(array(
        'id' => 'id',
        'type' => 'link',
        'title' => __('ID'),
        'href' => '/' . $this->controller . '/update/{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id' => 'code',
        'title' => __('Code'),
        'width' => '150'
    ))
    ->addColumn(array(
        'id' => 'name',
        'title' => __('Name'),
        'width' => 250,
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'shop_name',
        'title' => __('Shop'),
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'checkbox',
        'title' => __('Status'),
        'toggle' => true,
        'rules' => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty' => 0,
        'width' => 90,
    ))
    ->setDataset($data)
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
