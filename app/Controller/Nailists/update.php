<?php

$modelName = $this->Nailist->name;
$model = $this->{$modelName};

// Create breadcrumb 
$data[$modelName] = array();
if (isset($nailist_id) && $nailist_id != 0) {
    $pageTitle = __('Update nailist');
    $is_update = true;
    $param['id'] = $nailist_id;
    $param['nailist_id'] = $nailist_id;
    $data[$modelName] = Api::Call(Configure::read('API.url_nailists_detail'), $param);
    $this->Common->handleException(Api::getError());
} else {
    $pageTitle = __('Add nailist');
    $param['nailist_id'] = -1;
}
$this->Breadcrumb
    ->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/nailists',
            'name' => __('Nailist list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));
$this->setPageTitle($pageTitle);
$this->UpdateForm->reset()
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'code',
            'label' => __('Code'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'shop_id',
            'label' => __('Shop'),
            'options' => $this->Common->arrayKeyValue(MasterData::shops_all(), 'id', 'name'),
            'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'image_url',
            'type' => 'file',
            'image' => true,
            'label' => __('Image'),
            'allowEmpty' => true,
            'class' => 'upload',
            'crop' => array(
                'nail_id' => $nailist_id,
                'field' => 'image_url',
            )
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

// Process when submit form
if ($this->request->is('post')) {
    $error = array();
    $data = $this->getData($modelName);
 
    if ($model->validateNailistInsertUpdate($data)) {
        // Processing upload Image
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $image_url = $this->Image->uploadImage("{$modelName}.image_url");
            $data[$modelName]['image_url'] = $image_url;
        } elseif (!empty($model->data[$modelName]['image_url']['remove'])) {
            $data[$modelName]['image_url'] = '';
        } else {
            unset($data[$modelName]['image_url']);
        }
        $id = Api::call(Configure::read('API.url_nailists_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {            
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            AppCache::delete(Configure::read('nailists_all')->key);
            return $this->redirect("/{$this->controller}/update/{$id}");
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error    
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}
