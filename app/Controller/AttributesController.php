<?php

App::uses('AppController', 'Controller');

/**
 * Attributes Controller
 * 
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class AttributesController extends AppController {

    /**
     * Construct
     * 
     * @author diennvt 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author diennvt 
     * @return void
     */
    public function index() {
        include ('Attributes/index.php');
    }
    
    /**
     * update action
     * 
     * @author diennvt 
     * @return void
     */
    public function update($id = 0) {
        include ('Attributes/update.php');
    }
    
    
}
