<?php
$this->layout = 'full';
// set date for calendar
if (isset($this->request->query['date'])) {
    $dt = $this->request->query['date'];
} else {
    $dt = date('Y-m-d');
}
$this->set('dt', $dt);
// Create breadcrumb
$pageTitle = __('Order daily limit calendar');
$date = date('Y-m-d');
if (isset($_REQUEST['date'])) {
    $dt = \DateTime::createFromFormat('Y-m-d', $_REQUEST['date']);
    if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
        return $this->redirect("/{$this->controller}/calendar?date={$date}");
    } else {
        $date = $_REQUEST['date'];
    }
}
$shopId = !empty($this->AppUI->shop_id) ? $this->AppUI->shop_id : 0;
$data = Api::Call(Configure::read('API.url_order_daily_limit_list'), array(
    'shop_id' => $shopId,
    'date'    => $date
));
$this->set('data', $data['calendarData']);
$this->set('info', $data['calendarInfo']);
$this->set('shopId', $shopId);
$this->Common->handleException(Api::getError());
$this->setPageTitle($pageTitle);
$this->Breadcrumb
    ->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));