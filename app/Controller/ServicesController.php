<?php

App::uses('AppController', 'Controller');

/**
 * ServicesController class of Services Controller
 *
 * @package Controller
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class ServicesController extends AppController {

    /**
     * Initializes components for ServicesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

   /**
     * Handles user interaction of view index Services.
     * 
     * @return void
     */
    public function index() {
        include ('Services/index.php');
    }

    /**
     * Handles user interaction of view update Services.
     * 
     * @param integer $id ID value of Services. Default value is 0.
     * 
     * @return void
     */
    public function update($id = 0) {
        include ('Services/update.php');
    }

}
