<?php

App::uses('AppController', 'Controller');

/**
 * Orders Controller
 *
 * @package Controller
 * @created 2015-04-01
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class MailsendlogsController extends AppController {

    /**
     * Construct
     *
     * @author Tran Xuan Khoa
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     *
     * @author Tran Xuan Khoa
     * @return void
     */
    public function index() {
        include ('Mailsendlogs/index.php');
    }

}
