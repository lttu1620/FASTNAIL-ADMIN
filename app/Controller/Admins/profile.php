<?php

$modelName = $this->Admin->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Change profile');
if (empty($id)) {
    $id = $this->AppUI->id;
}
$data[$modelName]['name'] = $this->AppUI->name;
$this->setPageTitle($pageTitle);
// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => '/update/' . $this->request->url,
        'name' => $pageTitle
    ));
$this->UpdateForm->setModelName($modelName);
if (!empty($this->AppUI->shop_id)) {     
    $data[$modelName]['shop_group_id'] = $this->AppUI->shop_group_id;
    $data[$modelName]['shop_id'] = $this->AppUI->shop_id;
    $data[$modelName]['shop_name'] = $this->AppUI->shop_name;
    $data[$modelName]['shop_phone'] = $this->AppUI->shop_phone;
    $data[$modelName]['shop_address'] = $this->AppUI->shop_address;
    $data[$modelName]['shop_is_designate'] = $this->AppUI->shop_is_designate;
    $data[$modelName]['shop_reservation_interval'] = $this->AppUI->shop_reservation_interval;
    $data[$modelName]['shop_prefecture_id'] = $this->AppUI->shop_prefecture_id;
}
$listPrefectures = $this->Common->arrayKeyValue(MasterData::prefectures_all(), 'id', 'name');
$listShopGroups = $this->Common->arrayKeyValue(MasterData::shopgroups_all(), 'id', 'name');
$this->UpdateForm
        ->setData($data)       
        ->addElement(array(
            'id' => 'name',
            'label' => __('Admin name'),
        ));
if (!empty($this->AppUI->shop_id)) {
    $this->UpdateForm
        ->addElement(array(
            'id' => 'shop_prefecture_id',
            'label' => __('Shop prefecture'),
            'options' => $listPrefectures,
            'empty' => Configure::read('Config.StrChooseOne'),                
        ))
        ->addElement(array(
            'id' => 'shop_group_id',
            'label' => __('Shop group'),
            'options' => $listShopGroups,
            'empty' => Configure::read('Config.StrChooseOne'),          
        ))
        ->addElement(array(
            'id' => 'shop_id',
            'type' => 'hidden',            
        ))
        ->addElement(array(
            'id' => 'shop_name',
            'label' => __('Shop name'),
        ))
        ->addElement(array(
            'id' => 'shop_phone',
            'label' => __('Shop phone'),
        ))
        ->addElement(array(
            'id' => 'shop_address',
            'label' => __('Shop address'),
        ))
        ->addElement(array(
            'id' => 'shop_reservation_interval',
            'label' => __('Reservation interval'),
            'options' => array(15=>'15',30=>'30',45=>'45',60=>'60'),
            'empty' => __('All')
        ))        
        ->addElement(array(
            'id' => 'shop_is_designate',
            'label' => __('Shop Is Designate'),
            'options' => Configure::read('Config.BooleanValue'),
            'empty' => __('All')
        ));
}
$this->UpdateForm
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

if ($this->request->is('post')) {
    if ($model->validate_profile($this->getData($modelName))) {
        $param = array(
            'id' => $this->AppUI->id,
            'name' => $model->data[$modelName]['name'],
            'login_id' => $this->AppUI->login_id
        );
        Api::call(Configure::read('API.url_admins_addupdate'), $param);
        if (Api::getError()) {
            AppLog::info("Can not update ", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->AppUI->name = $model->data[$modelName]['name'];
            // update shop
            if (!empty($this->AppUI->shop_id)) {
                $data = $this->getData($modelName);                
                $shop['Shop']['id']=$model->data[$modelName]['shop_id'];
                $shop['Shop']['shop_group_id']=$model->data[$modelName]['shop_group_id'];
                $shop['Shop']['name']=$model->data[$modelName]['shop_name'];
                $shop['Shop']['phone']=$model->data[$modelName]['shop_phone'];
                $shop['Shop']['address']=$model->data[$modelName]['shop_address'];
                $shop['Shop']['is_designate']=$model->data[$modelName]['shop_is_designate'];
                $shop['Shop']['reservation_interval']=$model->data[$modelName]['shop_reservation_interval'];
                $shop['Shop']['prefecture_id']=$model->data[$modelName]['shop_prefecture_id'];
                if ($this->Shop->validateInsertUpdate($shop)) {
                    $id = Api::call(Configure::read('API.url_shops_addupdate'), $shop['Shop']);
                    if (!empty($id) && !Api::getError()) {
                        $this->AppUI->shop_group_id = $shop['Shop']['shop_group_id'];
                        $this->AppUI->shop_name = $shop['Shop']['name'];
                        $this->AppUI->shop_phone = $shop['Shop']['phone'];
                        $this->AppUI->shop_address = $shop['Shop']['address'];
                        $this->AppUI->shop_is_designate = $shop['Shop']['is_designate'];
                        $this->AppUI->shop_reservation_interval = $shop['Shop']['reservation_interval'];
                        $this->AppUI->shop_prefecture_id = $shop['Shop']['prefecture_id'];
                        $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
                        AppCache::delete(Configure::read('shops_all')->key);
                    } else {
                        return $this->Common->setFlashErrorMessage(Api::getError(), $error);
                    }
                } else {
                    $this->Common->setFlashErrorMessage(__('Input shop data'));
                }
            }
            else $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            AppCache::delete(Configure::read('admins_all')->key);
        }
        $this->redirect($this->request->here(false));
    } else {
        AppLog::info("Can not update your profile", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}