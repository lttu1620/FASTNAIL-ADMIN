<?php

$modelName = $this->Recommendreactionlog->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Recommend reaction log List');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->addElement(array(
        'id'    => 'user_name',
        'type'  => 'text',
        'label' => __('Username'),
    ))
    ->addElement(array(
        'id'    => 'order_id',
        'type'  => 'text',
        'label' => __('Order ID')
    ))
    ->addElement(array(
        'id'      => 'action_type',
        'label'   => __('Action type'),
        'options' => Configure::read('Config.actionType'),
        'empty'   => Configure::read('Config.StrAll')
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'user_name-asc'     => __('Name Asc'),
            'user_name-desc'    => __('Name Desc'),
            'created-asc'  => __('Created Asc'),
            'created-desc' => __('Created Desc')
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'      => 'limit',
        'label'   => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_recommend_reaction_log_list'), $param, false, array(0, array()));
$this->set('total', $total);
$this->set('limit', $param['limit']);

// create data table
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'id',
        'title' => __('ID')
    ))
    ->addColumn(array(
        'id'    => 'user_name',
        'type'  => 'link',
        'title' => __('Username'),
        'href'  => '/users/update/{user_id}',
    ))
    ->addColumn(array(
        'id'    => 'order_id',
        'type'  => 'link',
        'title' => __('Order ID'),
        'href'  => '/orders/detail/{order_id}'
    ))
    ->addColumn(array(
        'id'    => 'action_type',
        'title' => __('Action type'),
        'rules' => Configure::read('Config.actionType'),
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'created',
        'title' => __('Created'),
        'type'  => 'date',
        'width' => 120
    ))
    ->setDataset($data);
