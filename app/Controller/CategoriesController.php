<?php

App::uses('AppController', 'Controller');

/**
 * CategoriesController class of Categories Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class CategoriesController extends AppController {

    /**
     * Initializes components for CategoriesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Categories.
     *     
     * @return void
     */
    public function index() {
        include ('Categories/index.php');
    }

    /**
     * Handles user interaction of view update Categories.
     * 
     * @param integer $id ID value of Categories. Default value is 0.
     * 
     * @return void
     */
    public function update($id = 0) {
        include ('Categories/update.php');
    }

    /**
     * Handles user interaction of view follower Categories.
     *     
     * @param integer $id ID value of Categories. Default value is 0.
     * 
     * @return void
     */
    public function follower($id = 0) {
        include ('Categories/follower.php');
    }

}
