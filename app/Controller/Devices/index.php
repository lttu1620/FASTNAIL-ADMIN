<?php

$modelName = $this->Device->name;

//Process disable/enable
$this->doGeneralAction($modelName);

//get list shop
$listShop = $this->Common->arrayKeyValue(MasterData::shops_all(), 'id', 'name');

// create breadcrumb
$pageTitle = __('Device list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'shop_id',
            'label' => __('Shop'),
            'options' => $listShop,
            'empty' => __('All'),
        ))
        ->addElement(array(
            'id' => 'name',
            'type' => 'text',
            'label' => __('Name'),
        ))
        ->addElement(array(
            'id' => 'device_type',
            'label' => __('Device type'),
            'options' => Configure::read('Config.deviceType'),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'id-asc' => __('ID Asc'),
                'shop-id-desc' => __('Shop ID Desc'),
                'name-asc' => __('Name Asc'),
                'name-desc' => __('Name Desc'),
                'created-asc' => __('Created Asc'),
                'created-desc' => __('Created Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_devices_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
        ->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'width' => '10'
        ))
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'name',
            'title' => __('Name'),
        ))
        ->addColumn(array(
            'id' => 'shop_name',
            'title' => __('Shop name'),
        ))
        ->addColumn(array(
            'id' => 'device_type',
            'title' => __('Device type'),
            'rules' => Configure::read('Config.deviceType'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'created',
            'type' => 'date',
            'title' => __('Created'),
            'width' => 120,
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Status'),
            'toggle' => true,
            'rules' => array(
                '0' => 'checked',
                '1' => ''
            ),
            'empty' => 0,
            'width' => 100,
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));
