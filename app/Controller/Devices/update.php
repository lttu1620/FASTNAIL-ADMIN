<?php

$modelName = $this->Device->name;
$model = $this->{$modelName};

//get list shop
$listShop = MasterData::shops_all();
$listShop = $this->Common->arrayKeyValue($listShop, 'id', 'name');

$data = array();
$pageTitle = __('Add Device');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_devices_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Device unavailable", __METHOD__, $param);
        throw new NotFoundException("Device unavailable", __METHOD__, $param);
    }
    // set tile update 
    $pageTitle = __('Edit Device');
}
$this->setPageTitle($pageTitle);
// Create Breadcrumb
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/devices',
            'name' => __('Device list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// Create Update form 
$this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'autocomplete' => 'off',
            'required' => true
        ))
        ->addElement(array(
            'id' => 'shop_id',
            'label' => __('Shop'),
            'options' => $listShop,
            'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'device_type',
            'label' => __('Device type'),
            'options' => Configure::read('Config.deviceType'),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

if ($this->request->is('post')) {
    // if case add new tag
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_devices_addupdate'), $model->data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not add new", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            AppCache::delete(Configure::read('devices_all')->key);
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
    } else {
        AppLog::info("Can not add new", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}