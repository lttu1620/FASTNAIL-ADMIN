<?php

$modelName = $this->Pointget->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Point get');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_point_get_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Point get unavailable", __METHOD__, $param);
        throw new NotFoundException("Point get unavailable", __METHOD__, $param);
    }
    $pageTitle = __('Edit Point get');
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
// create Update form 
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(
        array(
            'id'    => 'id',
            'type'  => 'hidden',
            'label' => __('id'),
        )
    )
    ->addElement(
        array(
            'id'    => 'name',
            'type'  => 'text',
            'label' => __('Name'),
        )
    )
    ->addElement(
        array(
            'id'    => 'point',
            'type'  => 'text',
            'label' => __('Point'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        )
    )
    ->addElement(
        array(
            'type'    => 'submit',
            'value'   => __('Cancel'),
            'class'   => 'btn btn-primary pull-left',
            'onclick' => 'return back();',
        )
    );

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        $id = Api::call(Configure::read('API.url_point_get_addupdate'), $data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not update point get", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
    } else {
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}