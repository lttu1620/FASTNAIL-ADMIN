<?php

App::uses('AppController', 'Controller');

/**
 * Orders Controller
 *
 * @package Controller
 * @created 2015-05-13
 * @version 1.0
 * @author KhoaTX
 * @copyright Oceanize INC
 */
class OrdertimelylimitsController extends AppController {

    var $components=array('PhpExcel');
    /**
     * Construct
     *
     * @author KhoaTX
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     *
     * @author KhoaTX
     * @return void
     */
    public function index() {
        include ('Ordertimelylimits/index.php');
    }

    /**
     * Import data
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function import()
    {
        include('Ordertimelylimits/import.php');
    }

}
