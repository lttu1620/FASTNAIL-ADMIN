<?php

App::uses('AppController', 'Controller');

/**
 * Materials Controller
 * 
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class MaterialsController extends AppController {

    /**
     * Construct
     * 
     * @author diennvt 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author diennvt 
     * @return void
     */
    public function index() {
        include ('Materials/index.php');
    }

    /**
     * update action
     * 
     * @author diennvt 
     */
    public function update($material_id = 0) {
        include ('Materials/update.php');
    }
}
