<?php
if (!empty($this->AppUI->shop_id)) {
    $this->layout = 'full';
    $param['shop_id'] = $this->AppUI->shop_id;
    if (isset($this->request->query['date'])) {
        $dt = $this->request->query['date'];
    } else {
        $dt = date('Y-m-d');
    }
    $param['date'] = $dt;
    $data = Api::call(Configure::read('API.url_orders_date_calendar'), $param, false, array());
    $this->Common->handleException(Api::getError());
    $this->set('data', $data);
    $this->set('dt', $dt);

    $staffRecord = MasterData::nailists_all($this->AppUI->shop_id);
    $deviceRecord = MasterData::devices_all($this->AppUI->shop_id);
    $shop_open_time = !empty($this->AppUI->shop_open_time) ? $this->AppUI->shop_open_time : Configure::read('Config.DefaultShopOpenTime');
    $shop_close_time = !empty($this->AppUI->shop_close_time) ? $this->AppUI->shop_close_time : Configure::read('Config.DefaultShopCloseTime');
    $this->set('staffRecord', $staffRecord);
    $this->set('deviceRecord', $deviceRecord);
    $this->set('shop_open_time', getdate(strtotime($dt) + $shop_open_time));
    $this->set('shop_close_time', getdate(strtotime($dt) + $shop_close_time));
} else {
    $this->layout = 'template';
    $this->view = 'dashboard';
}