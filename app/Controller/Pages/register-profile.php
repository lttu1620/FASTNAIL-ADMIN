<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};

$data = array();
if (!empty($this->AppUI->name) && !empty($this->AppUI->name)) {
    $data[$modelName]['name'] = $this->AppUI->name;
}
if (!empty($this->AppUI->nickname)) {
    $data[$modelName]['nickname'] = $this->AppUI->nickname;
}
if (!empty($this->AppUI->sex_id)) {
    $data[$modelName]['sex_id'] = $this->AppUI->sex_id;
}
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->setAttribute('type', 'post')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),           
            'required' => true
        ))
        ->addElement(array(
            'id' => 'nickname',
            'label' => __('Nickname'),            
            'required' => true
        ))
        ->addElement(array(
            'id' => 'sex_id',
            'label' => __('Gender'),
            'options' => Configure::read('Config.searchGender'),
            'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true           
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Update profile'),
            'class' => 'btn bg-capture-red btn-block',
            'style' => 'width:320px;',
        ));

if ($this->request->is('post')) {    
    if ($model->validateRegisterProfile($this->getData($modelName))) {
        $param['id'] = $this->AppUI->id;
        $param['name'] = $model->data[$modelName]['name'];                
        $param['nickname'] = $model->data[$modelName]['nickname'];                
        $param['sex_id'] = $model->data[$modelName]['sex_id'];
        $userId = Api::call(Configure::read('API.url_users_addupdate'), $param);
        if ($userId) {
            $this->AppUI->name = $param['name'];
            $this->AppUI->nickname = $param['nickname'];
            $this->AppUI->sex_id = $param['sex_id'];
            $this->AppUI->redirect_register_profile = 0;
            $this->AppUI->redirect_register_company = 1;
            $this->Auth->login($this->AppUI);           
            $this->Common->setFlashSuccessMessage(__('Profile updated successfully'));
            return $this->redirect('/register/company');
        }
    }
    return $this->Common->setFlashErrorMessage($model->validationErrors);   
}