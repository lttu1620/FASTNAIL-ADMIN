<?php
$this->Cookie->httpOnly = true;
if ($this->Auth->logout()) {
    AppCache::delete(Configure::read('shops_all')->key);
    AppCache::delete(Configure::read('nailists_all')->key);
    AppCache::delete(Configure::read('devices_all')->key);
    return $this->redirect('/login');
}