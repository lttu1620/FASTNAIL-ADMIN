<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};
// Check remember userName and password
$this->Cookie->httpOnly = true;
if (empty($this->AppUI->id)) {
    if ($this->Cookie->read('remember_admin_cookie')) {
        $loginCookie = $this->Cookie->read('remember_admin_cookie');
    }
}
$loginForm = $this->UpdateForm->reset();
$loginForm->setModelName($modelName)
        ->setAttribute('type', 'post')
        ->addElement(array(
            'id' => 'login',
            'label' => false,
            'value' => empty($loginCookie['login']) ? '' : $loginCookie['login'],
            'placeholder' => __('Login ID')
        ))
        ->addElement(array(
            'id' => 'password',
            'type' => 'password',
            'label' => false,
            'value' => empty($loginCookie['admin_password']) ? '' : $loginCookie['admin_password'],
            'placeholder' => __('Password')
        ))
        ->addElement(array(
            'id' => 'remembera',
            'type' => 'checkbox',
            'value' => '1',
            'checked' => empty($loginCookie['remembera']) ? false : $loginCookie['remembera'] == 1,
            'after' => "<label for='remembera' > <span class='nomarlText'> &nbsp;" . __('Remember me') . "</span></lable>",
            'label' => false,
            'class' => ''
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Login'),
            'class' => 'btn bg-capture-red btn-block',
            'style' => 'width:320px;',
        ));
$this->set('loginForm', $loginForm->get());
if ($this->request->is('post')) {
    if ($model->validateLogin($this->getData($modelName))) {
        $param['password'] = $model->data[$modelName]['password'];
        if (!empty($model->data[$modelName]['login'])) {
            $this->dispatchEvent('Admin.beforeLogin', $param);
            $param['login_id'] = $model->data[$modelName]['login'];
            if ($this->startAdminLogin($param)) {
                $this->dispatchEvent('Admin.afterLogin', $param);
                // did they select the remember me checkbox?
                if (isset($model->data[$modelName]['remembera']) && $model->data[$modelName]['remembera'] == 1) {
                    $model->data[$modelName]['admin_password'] = $model->data[$modelName]['password'];
                    unset($model->data[$modelName]['password']);
                    $this->Cookie->write('remember_admin_cookie', $model->data[$modelName], true, '2 weeks');
                }
                return $this->redirect($this->Auth->redirect());
            }
        }
    }
    AppLog::info("Login fail", __METHOD__, $this->data);
    $this->Common->setFlashErrorMessage(__('Invalid Login ID or password. Please try again'));
}