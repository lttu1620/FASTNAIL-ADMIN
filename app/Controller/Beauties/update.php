<?php

$modelName = $this->Beauty->name;
$model = $this->{$modelName};

$data = array();
$pageTitle = __('Add Beauty');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_beauties_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Beauty unavailable", __METHOD__, $param);
        //throw new NotFoundException("Beauty unavailable", __METHOD__, $param);
        return $this->Common->setFlashErrorMessage('Beauty unavailable', $error);
    }
    // set tile update
    $pageTitle = __('Edit Beauty');
}
$this->setPageTitle($pageTitle);
// Create Breadcrumb
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(
        array(
            'link' => $this->request->base.'/beauties',
            'name' => __('Beauty list'),
        )
    )
    ->add(
        array(
            'name' => $pageTitle,
        )
    );

$prefectures = MasterData::prefectures_all();
// Create Update form 
$this->UpdateForm
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(
        array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        )
    )
    ->addElement(
        array(
            'id' => 'title',
            'label' => __('Title'),
            'required' => true,
        )
    )
    ->addElement(array(
        'id' => 'description',
        'type' => 'textarea',
        'escapse' => false,
        'rows' => '3',
        'label' => __('Description'),
    ))   
    ->addElement(
        array(
            'id' => 'image_url',
            'type' => 'file',
            'image' => true,
            'label' => __('Image url'),
            'allowEmpty' => true,
            'class' => 'upload',
            'crop' => array(
                //'field' => 'image_url',
            ),
        )
    )
    ->addElement(
        array(
            'id' => 'mission_type',
            'label' => __('Mission type'),
            'options' => Configure::read('Config.missionType'),
            'empty' => Configure::read('Config.StrChooseOne'),
            'onchange' => "
                if ($(this).val() == 0) {
                    $('#point').val('');
                    $('#point').attr('disabled', 'disabled'); 
                } else {    
                    $('#point').removeAttr('disabled');
                }
            ",
            'required' => true,            
        )
    )
    ->addElement(
        array(
            'id' => 'point',
            'label' => __('Point'),
            'type' => 'number',
            'disabled' => !empty($data[$modelName]['mission_type']) ? false : true,
        )
    )
    ->addElement(
        array(
            'id' => 'start_date',
            'label' => __('Start date'),
            'calendar' => true,
        )
    )
    ->addElement(
        array(
            'id' => 'end_date',
            'label' => __('End date'),
            'calendar' => true,
        )
    )    
    ->addElement(
        array(
            'id' => 'segment_sex',
            'label' => __('Segment sex'),
            'options' => Configure::read('Config.searchGender'),
        )
    )
    ->addElement(
        array(
            'id' => 'segment_device',
            'label' => __('Segment device'),
            'options' => Configure::read('Config.segmentDevice'),
            'empty' => Configure::read('Config.StrChooseOne'),
        )
    )
    ->addElement(
        array(
            'id'    => 'segment_prefecture',
            'label' => __('Segment prefecture'),
            'after' => '<br/>' .  __('Prefecture ID') . ':' . $this->Common->arrayField($prefectures, 'id', true)
        )
    )
    ->addElement(
        array(
            'id'    => 'segment_user_generation',
            'label' => '年齢',
            'after' => '<br/>年齢: ' . implode(',',range(1, 50)) . '<br/>' . implode(',',range(51, 99))
        )
    )
    ->addElement(
        array(
            'id'    => 'detail_page_url',
            'label' => __('Detail page url'),
        )
    )
    ->addElement(
        array(
            'id'    => 'distribution_limit',
            'label' => __('Distribution limit'),
        )
    )
    ->addElement(
        array(
            'id'    => 'display_no',
            'label' => __('Display No'),
        )
    )
    ->addElement(
        array(
            'id' => 'token',
            'label' => __('Survey token'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        )
    )
    ->addElement(
        array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();',
        )
    );

if ($this->request->is('post')) {
    $error = array();
    $data = $this->getData($modelName);

    if ($model->validateInsertUpdate($data)) {
        // Processing upload Image
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $image_url = $this->Image->uploadImage("{$modelName}.image_url");
            $data[$modelName]['image_url'] = $image_url;
        } elseif (!empty($model->data[$modelName]['image_url']['remove'])) {
            $data[$modelName]['image_url'] = '';
        } else {
            unset($data[$modelName]['image_url']);
        }
        $id = Api::call(Configure::read('API.url_beauties_addupdate'), $data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        // show validation error    
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}