<?php

$modelName = $this->Survey->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Survey');
if (!empty($id)) {
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_surveys_detail'), $param);
    if (empty($data[$modelName])) {
        AppLog::info("Survey unavailable", __METHOD__, $param);
        throw new NotFoundException("Survey unavailable", __METHOD__, $param);
    }
    $pageTitle = __('Edit Survey');
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));
// create Update form 
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id'    => 'id',
        'type'  => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id'       => 'order_id',
        'label'    => __('Order Id'),
        'type'     => 'text',
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'title',
        'label'    => __('Title'),
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'url',
        'label'    => __('Url'),
        'type'     => 'url',
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'started',
        'label'    => __('Started'),
        'type'     => 'text',
        'calendar' => true,
        'required' => true
    ))
    ->addElement(array(
        'id'       => 'finished',
        'label'    => __('Finished'),
        'type'     => 'text',
        'calendar' => true,
        'required' => true
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type'    => 'submit',
        'value'   => __('Cancel'),
        'class'   => 'btn pull-left',
        'onclick' => 'return back();'
    ));

if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        $id = Api::call(Configure::read('API.url_surveys_addupdate'), $data[$modelName]);
        if (Api::getError()) {
            AppLog::info("Can not update survey", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
    } else {
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}