<?php

$modelName = $this->Survey->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Survey List');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'    => 'order_id',
        'label' => __('Order Id'),
    ))
    ->addElement(array(
        'id'    => 'title',
        'label' => __('Title')
    ))
    ->addElement(array(
        'id'       => 'date_from',
        'label'    => __('From date'),
        'type'     => 'text',
        'calendar' => true
    ))
    ->addElement(array(
        'id'       => 'date_to',
        'label'    => __('To date'),
        'type'     => 'text',
        'calendar' => true
    ))
    ->addElement(array(
        'id'      => 'disable',
        'label'   => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty'   => Configure::read('Config.StrAll')
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'name-asc'     => __('Name Asc'),
            'name-desc'    => __('Name Desc'),
            'created-asc'  => __('Created Asc'),
            'created-desc' => __('Created Desc')
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'      => 'limit',
        'label'   => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_surveys_list'), $param, false, array(0, array()));
$this->set('total', $total);
$this->set('limit', $param['limit']);

// create data table
$this->SimpleTable->addColumn(array(
    'id'    => 'item',
    'name'  => 'items[]',
    'type'  => 'checkbox',
    'value' => '{id}',
    'width' => 20
))
    ->addColumn(array(
        'id'    => 'id',
        'type'  => 'link',
        'title' => __('ID'),
        'href'  => '/' . $this->controller . '/update/{id}',
        'width' => 50
    ))
    ->addColumn(array(
        'id'    => 'order_id',
        'title' => __('Order Id'),
        'width' => '100',
    ))
    ->addColumn(array(
        'id'    => 'title',
        'title' => __('Title')
    ))
    ->addColumn(array(
        'id'    => 'url',
        'type'  => 'url',
        'title' => __('Url'),
        'width' => 350
    ))
    ->addColumn(array(
        'id'    => 'started',
        'title' => __('Started'),
        'type' => 'date',
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'finished',
        'title' => __('Finished'),
        'type' => 'date',
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'created',
        'title' => __('Created'),
        'type'  => 'date',
        'width' => 150
    ))
    ->addColumn(array(
        'id'     => 'disable',
        'type'   => 'checkbox',
        'title'  => __('Status'),
        'toggle' => true,
        'rules'  => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty'  => 0,
        'width'  => 90,
    ))
    ->setDataset($data)
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'id'    => 'btn-disable',
        'type'  => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'id'    => 'btn-enable',
        'type'  => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
