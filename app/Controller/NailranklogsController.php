<?php

App::uses('AppController', 'Controller');

/**
 * Nailists Controller
 * 
 * @package Controller
 * @created 2015-03-25
 * @version 1.0
 * @author Tuancd
 * @copyright Oceanize INC
 */
class NailranklogsController extends AppController {

    /**
     * Construct
     * 
     * @author diennvt 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * index action
     * 
     * @author Tuancd 
     * @return void
     */
    public function index() {
        include ('Nailranklogs/index.php');
    }
}
