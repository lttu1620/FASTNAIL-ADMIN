<?php

$modelName = $this->Contact->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add contact');
if (!empty($id)) {
    $pageTitle = __('Edit contact');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_contacts_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/contacts',
            'name' => __('Contact list'),
        ))
        ->add(array(
            'name' => $pageTitle,
        ));
//System will send mail if the value is_send_mail equal true, otherwise not send mail.
// create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('ID'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'required' => true
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email'),
            'required' => true
        ))
       
        ->addElement(array(
            'id' => 'subject',
            'label' => __('Subject'),
            'options' => Configure::read('Config.contacts.subject'),
            'empty' => Configure::read('Config.StrChooseOne'),
            'required' => true
        ))
    
        ->addElement(array(
            'id' => 'content',
            'type' => 'textarea',
            'escapse' => false,
            'rows' => '5',
            'label' => __('Content'),
            'required' => true
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => (!empty($id) ? __('Save') : __('Send')),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn pull-left',
            'onclick' => 'return back();'
        ));

// process when submit form
if ($this->request->is('post')) {
    if ($model->validate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_contacts_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfully'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}